# Entity Discovery

These files use Named Entity Recognition to identify the peoples names in text.

## Usage

Start mongodb.

Start the script by running:

```
python entity-discovery.py <spacy model path> <text files folder> <mongodb db name> <mongodb db collection>
```

As an example, using the models from the [trained models]() repository:

```
python src/discovery/entity-discovery.py ../acerpi-ner/models/ufrgs_third ../acerpi-data/ufrgs/txt/ ufrgs records
```