import re
import sys
import spacy
from pathlib import Path
from collections import namedtuple
from pymongo import MongoClient

SIAPE_REGEX = re.compile('[S|s][I|i][A|a][P|p][E|e][^0-9.]{1,3}([0-9]{6,8})')
MAX_LENGTH = 120
AFTER_SPLIT_INDEX = 1

Record = namedtuple('record', 'id name siape document')
Document = namedtuple('Document', 'name')


def normalize(value: str):
    return value.strip().lower().replace('\n', ' ').replace('  ', ' ')


def save_if_needed(records, collection):
    if len(records) == 1000:
        collection.insert_many(records)
        #print("Saved 1000 records to database")
        records.clear()


def try_find_closest(text, entity_name):
    text_following = text.split(entity_name)[AFTER_SPLIT_INDEX][0:MAX_LENGTH]
    result = SIAPE_REGEX.findall(text_following)
    if len(result) > 0:
        return result[0]
    else:
        return None


def siape_from(text, entity_name):
    result = SIAPE_REGEX.findall(text)
    if len(result) <= 1:
        return result
    else:
        closest = try_find_closest(text, entity_name)
        if closest:
            return [closest]
        else:
            # print('Could not find closest, appending all found in document.')
            return result


def run(model, folder, db, collection):
    current_id = 0
    nlp = spacy.load(model)
    records = []

    for file in Path(folder).iterdir():
        with open(file, 'r') as reader:
            text = reader.read()
            document = Document(file.stem)._asdict()
            discovered = nlp(text)
            for entity in discovered.ents:
                siape = siape_from(text, entity.text)
                record = Record(current_id, entity.text, siape, document)._asdict()
                records.append(record)
                save_if_needed(records, db[collection])
                current_id += 1


if __name__ == '__main__':
    model_path = sys.argv[1]
    files_folder = sys.argv[2]
    db_name = sys.argv[3]
    collection_name = sys.argv[4]

    client = MongoClient()
    database = client[db_name]

    run(model_path, files_folder, database, collection_name)

