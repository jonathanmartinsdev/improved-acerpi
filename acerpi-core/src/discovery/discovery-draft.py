import sys
import spacy
from pathlib import Path
from recordclass import recordclass


RealWorldEntity = recordclass('RealWorldEntity', 'name occurrences')


def normalize(value: str):
    return value.strip().replace('\n', ' ').replace('  ', ' ')


def run(model, folder):
    current_id = 0
    nlp = spacy.load(model)
    real_world_entities = {}

    for file in Path(folder).iterdir():
        # print(str(file))
        with open(file, 'r') as reader:
            text = reader.read()
            doc = nlp(text)
            for entity in doc.ents:
                name = normalize(entity.text)
                default = RealWorldEntity(name, [])
                real_world_entity = real_world_entities.get(name, default)
                real_world_entity.occurrences.append(str(current_id))
                real_world_entities[name] = real_world_entity
                current_id += 1

    return real_world_entities


if __name__ == '__main__':
    model_path = sys.argv[1]
    files_folder = sys.argv[2]
    result = run(model_path, files_folder)

    for value in result:
        print(value, result[value].occurrences)