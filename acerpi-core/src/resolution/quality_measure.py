

class QualityMeasure:
    def __init__(self, total_pairs_correct):
        self.total_pairs = total_pairs_correct
        self.true_positives = 0
        self.false_positives = 0
        self.true_negatives = 0
        self.false_negatives = 0

    def compute_true_positive(self):
        self.true_positives += 1

    def compute_false_positive(self):
        self.false_positives += 1

    def compute_measures(self):
        print(f'True Positives: {self.true_positives}')
        print(f'False Positives: {self.false_positives}')
        precision = self.true_positives / (self.false_positives + self.true_positives)
        print("Precision: " + str(precision))

        self.false_negatives = self.total_pairs - self.true_positives
        print(f'False negatives: {self.false_negatives}')
        recall = self.true_positives / (self.false_negatives + self.true_positives)
        print("Recall: " + str(recall))

        fMeasure = (2 * precision * recall)/(precision + recall)
        print("F-measure: " + str(fMeasure))