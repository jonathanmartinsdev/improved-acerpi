# Entity Resolution

These files use Entity Resolution to match records discovered.

## Entity Resolver

### Usage

Start mongodb.

Start the script by running:

```
python entity-resolver.py <mongodb db name> <mongodb records collection> <mongodb entity collection>
```

As an example, from the repository root, run:

```
python src/resolution/entity-resolver.py ufrgs records entities
```

## Evaluation

Evaluation happens by pairwise comparison. The steps for that are as follows:
1. Pairs are created as a combination(cluster, 2) for each cluster from the gold standard and clustered records.
2. For every par in a resolved cluster:
    1. If the pair exists in the gold standard pairs, it is considered as a true positive.
    2. If the pair does not exist in the gold standard pairs, it is considered a false positive.
3. The false negatives are calculated as the number of gold standard pairs minus the number of correctly clustered pairs.
4. Precision, Recall and F1 are calculated as the respective formulae.

The gold-standard file is composed of one cluster per line, and record ids from each cluster separated by ", " (comma and space characters).

### Usage

Start mongodb.

Start the script by running:

```
python resolution-evaluation.py <path to gold standard file> <mongodb db name> <mongodb entity collection>
```

As an example, from the repository root, run:

```
python src/resolution/resolution-evaluation.py src/resolution/ufrgs-correct.csv ufrgs entities
```