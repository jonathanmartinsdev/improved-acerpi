import sys
from itertools import combinations
from pymongo import MongoClient

class QualityMeasure:
    def __init__(self, total_pairs_correct):
        self.total_pairs = total_pairs_correct
        self.true_positives = 0
        self.false_positives = 0
        self.true_negatives = 0
        self.false_negatives = 0

    def compute_true_positive(self):
        self.true_positives += 1

    def compute_false_positive(self):
        self.false_positives += 1

    def compute_measures(self):
        print(f'True Positives: {self.true_positives}')
        print(f'False Positives: {self.false_positives}')
        precision = self.true_positives / (self.false_positives + self.true_positives)
        print("Precision: " + str(precision))

        self.false_negatives = self.total_pairs - self.true_positives
        print(f'False negatives: {self.false_negatives}')
        recall = self.true_positives / (self.false_negatives + self.true_positives)
        print("Recall: " + str(recall))

        fMeasure = (2 * precision * recall)/(precision + recall)
        print("F-measure: " + str(fMeasure))


def clustered_together(pair, collection):
    actual_cluster = collection.find({'records': {'$all': [(pair[0]), (pair[1])]}})
    return len(list(actual_cluster)) == 1


def set_of_records(gold_std):
    return set([item for t in gold_std for item in t])


def run(correct_clusters, mongo_collection):
    gold_std = [item for sublist in create_gold(correct_clusters) for item in sublist]
    metrics = QualityMeasure(len(gold_std))

    for entity in mongo_collection.find({'records': {'$in': list(set_of_records(gold_std))}}):
        actual_pairs = to_pairs(entity['records'])
        for pair in actual_pairs:
            if pair in gold_std or (pair[1], pair[0]) in gold_std:
                metrics.compute_true_positive()
            else:
                metrics.compute_false_positive()
    metrics.compute_measures()


def create_gold(correct_clusters):
    gold = []
    for cluster in correct_clusters:
        gold.append(to_pairs(cluster))
    return gold


def to_pairs(cluster):
    return [x for x in combinations(cluster, 2)]


def parse(filename):
    result = []
    with open(filename, 'r') as f:
        f.readline()
        for line in f.readlines():
            tmp = line.split(',')
            result.append([int(x.strip()) for x in tmp])
    return result


if __name__ == '__main__':
    correct_clusters_file = sys.argv[1]
    db_name = sys.argv[2]
    db_collection = sys.argv[3]

    client = MongoClient()
    database = client[db_name]

    run(parse(correct_clusters_file), database[db_collection])