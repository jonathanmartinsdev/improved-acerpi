Documento gerado sob autenticação Nº KNT.817.923.FP4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5315                  de  15/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir da data deste ato, como segue:
Transformar:
 -  Gerente Administrativo da Secretaria de Relações Internacionais,  Código FG3, em Diretor do
Centro Interdisciplinar em Sociedade, Ambiente e Desenvolvimento - CISADE, Código FG3.
Processo nº 23078.029269/2015-64.
CARLOS ALEXANDRE NETTO
Reitor
