Documento gerado sob autenticação Nº WPJ.148.408.FAH, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3275                  de  03/05/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a PEDRO MARTIM SILVA
VEBER, matrícula SIAPE nº 0358242, no cargo de Pedreiro, nível de classificação B, nível de capacitação I,
padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de trabalho, com exercício
no Setor de Infraestrutura, Espaço Físico, Patrimônio e Recursos Humanos da Gerência Administrativa da
Faculdade de Ciências Econômicas, com proventos integrais. Processo 23078.007323/2016-00.
CARLOS ALEXANDRE NETTO
Reitor
