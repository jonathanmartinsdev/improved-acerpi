Documento gerado sob autenticação Nº GXM.076.435.CB7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7227                  de  07/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de EDUARDO BITTENCOURT, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia Civil da Escola de Engenharia, com a finalidade de
proferir  palestra  na "International  Conference on Computational  Plasticity",  em Barcelona,  Espanha,  no
período compreendido entre 04/09/2017 e 08/09/2017, incluído trânsito, com ônus limitado. Solicitação nº
30109.
RUI VICENTE OPPERMANN
Reitor
