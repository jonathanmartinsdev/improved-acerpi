Documento gerado sob autenticação Nº BKL.434.536.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1787                  de  22/02/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CLAUDIO VESCIA ZANINI, Professor do Magistério Superior,
lotado e em exercício no Departamento de Línguas Modernas do Instituto de Letras, com a finalidade de
participar da "2nd Global Conference: The Changing Faces of Evil" junto à Progressive Connexions, em Praga,
Repúbica Tcheca, no período compreendido entre 08/03/2019 e 11/03/2019, incluído trânsito, com ônus
limitado. Solicitação nº 62323.
RUI VICENTE OPPERMANN
Reitor
