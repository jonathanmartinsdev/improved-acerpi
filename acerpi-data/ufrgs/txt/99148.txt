Documento gerado sob autenticação Nº WCV.847.179.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9118                  de  08/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar,  a partir  da data de publicação no Diário Oficial  da União,  o ocupante do cargo de
Economista  -  701026,  do Nível  de Classificação EIII,  do Quadro de Pessoal  desta  Universidade,  FELIPE
VENDRUSCOLO DA SILVA, matrícula SIAPE 2080670, da função de Diretor da Divisão de Bolsas do Depto de
Benefícios e Assistência Estudantil da PRAE, Código SRH 1538, Código FG-2, para a qual foi designado pela
Portaria  nº  404/2019,  de 11/01/2019,  publicada no Diário  Oficial  da União de 14/01/2019.  Processo nº
23078.526577/2019-67.
RUI VICENTE OPPERMANN
Reitor.
