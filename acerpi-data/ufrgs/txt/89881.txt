Documento gerado sob autenticação Nº ELC.419.998.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3324                  de  16/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de JAIRTON DUPONT, Professor do Magistério Superior, lotado e
em exercício no Departamento de Química Orgânica do Instituto de Química, com a finalidade de proferir
palestra  no  "8th  International  Congress  on  Ionic  Liquids  (COIL-8)",  em  Pequim,  China,  no  período
compreendido entre 11/05/2019 e 18/05/2019, incluído trânsito, com ônus limitado. Solicitação nº 72619.
RUI VICENTE OPPERMANN
Reitor
