Documento gerado sob autenticação Nº UGB.832.164.C0L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10510                  de  17/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à ocupante do cargo de Bibliotecário-documentalista, MICHELE DIAS MEDEIROS, lotada
no Instituto de Psicologia e com exercício na Biblioteca do Instituto de Psicologia, Licença por Motivo de
Afastamento de Cônjuge, sem remuneração, nos termos do artigo 84, "caput", da Lei n° 8.112, de 11 de
dezembro de 1990, com a redação alterada pela Lei n° 9.527/97, a partir de 01 de fevereiro de 2018, por
prazo  indeterminado,  mediante  comprovação  anual  da  manutenção  dos  respectivos  fatos  geradores.
Processo n° 23078.520450/2017-72.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
