Documento gerado sob autenticação Nº FWB.584.631.DCR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4994                  de  11/06/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CLARICE MISOCZKY DE OLIVEIRA,  Professor do Magistério
Superior,  lotada  e  em exercício  no  Departamento  de  Urbanismo da  Faculdade de  Arquitetura,  com a
finalidade de participar da "11th International Critical Management Studies Conference", em Milton Keynes,
Inglaterra, no período compreendido entre 25 e 30/06/2019, com ônus UFRGS (Pró-Reitoria de Pesquisa:
diárias) e realizar visita à Oxford Brookes University, em Oxford, Inglaterra, no período compreendido entre
01 e 07/07/2019, incluído trânsito, com ônus limitado. Solicitação nº 84016.
RUI VICENTE OPPERMANN
Reitor
