Documento gerado sob autenticação Nº UAD.636.968.5KF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             790                  de  23/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria nº 511 de 16/01/2018 que designou, temporariamente, LAURA WUNSCH (Siape:
1654277)  para  substituir   LOVOIS  DE  ANDRADE  MIGUEL  (Siape:  1230208  ),  Processo  SEI  Nº
23078.500961/2019-30
 
onde se lê:
 
"...  em seu afastamento por motivo de férias,  no período de 16/01/2019 a 20/01/2019, com o
decorrente pagamento das vantagens por 5 dias...;
 
          leia-se:
 
"...   em seu afastamento por motivo de férias,  nos dias 16 e 17/01/2019 ,  com o decorrente
pagamento das vantagens por 2 dias..."; ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
