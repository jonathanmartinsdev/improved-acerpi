Documento gerado sob autenticação Nº UYS.673.534.7L6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6358                  de  17/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de GERHARD ERNST OVERBECK, Professor do Magistério Superior,
lotado no Departamento de Botânica do Instituto de Biociências e com exercício no Programa de Pós-
Graduação em Botânica,  com a  finalidade de  participar  de  reunião  na  Intergovernmental  Platform on
Biodiversity and Ecosystem Services, em Cartagena, Colômbia, no período compreendido entre 03/08/2017 e
10/08/2017, incluído trânsito, com ônus limitado. Solicitação nº 29418.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
