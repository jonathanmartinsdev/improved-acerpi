Documento gerado sob autenticação Nº AMI.472.645.DTT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7596                  de  24/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  13/09/2018,   referente  ao  interstício  de
17/05/2016 a 12/09/2018, para a servidora LUCIANE DA SILVA FURNO, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 0358719,  lotada  na  Pró-Reitoria de Graduação, passando do Nível
de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.514801/2018-97:
Formação Integral de Servidores da UFRGS III  CH: 88 Carga horária utilizada: 81 hora(s) / Carga horária
excedente: 7 hora(s) (26/04/2016 a 13/09/2018)
ESAF - Nova Regra Ortográfica CH: 19 (02/09/2013 a 30/09/2013)
UNIASSELVI - Inglês Básico I CH: 50 (02/06/2017 a 18/08/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
