Documento gerado sob autenticação Nº OAD.426.363.VP8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3274                  de  15/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por decisão judicial  proferida no processo nº 5002028-77.2019.4.04.7100,  da 10ª Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo II da Portaria nº 1874, de
13/07/2006, da servidora CARMEN REGINA SOARES DE ALMEIDA , matrícula SIAPE n° 0351612, aposentada
no cargo de  Assistente em Administração - 701200, do Nível I para o Nível III,  a contar de 01/01/2006,
conforme o Processo nº 23078.508982/2019-01.
 
RUI VICENTE OPPERMANN
Reitor
