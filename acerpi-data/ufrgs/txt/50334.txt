Documento gerado sob autenticação Nº VGJ.888.599.ASV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1142                  de  06/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar  na Escola  de Educação Física,  Fisioterapia  e  Dança,  com exercício  no Núcleo de Gestão
Organizacional da Gerência Administrativa da Escola de Educação Física, Fisioterapia e Dança, CÁTIA SORAIA
JESUS, nomeada conforme Portaria Nº 11303/2017 de 19 de dezembro de 2017, publicada no Diário Oficial
da União no dia 20 de dezembro de 2017, em efetivo exercício desde 24 de janeiro de 2018, ocupante do
cargo de ASSISTENTE EM ADMINISTRAÇÃO, Ambiente Organizacional Administrativo, classe D, nível I, padrão
101, no Quadro de Pessoal desta Universidade. 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
