Documento gerado sob autenticação Nº UYK.480.535.BP8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1169                  de  06/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor CÁSSIO CASTALDI ARAÚJO
BLAZ, ocupante do cargo de Analista de Tecnologia da Informação-701062, lotado no Centro Nacional de
Supercomputação, SIAPE 2093203, para 52% (cinquenta e dois por cento), a contar de 15/12/2017, tendo em
vista  a  conclusão  do  curso  de  Mestrado  em  Ciência  da  Computação,  conforme  o  Processo  nº
23078.523906/2017-56.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
