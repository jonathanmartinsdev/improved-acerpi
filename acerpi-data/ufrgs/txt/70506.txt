Documento gerado sob autenticação Nº VON.482.113.F13, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7968                  de  05/10/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CHRISTINE WETZEL, Professor do Magistério Superior, lotada e
em exercício no Departamento de Assistência e Orientação Profissional da Escola de Enfermagem, com a
finalidade de participar do "XVI Coloquio Panamericano de Investigación en Enfermería", em Havana, Cuba,
no período compreendido entre 03/11/2018 e 10/11/2018, incluído trânsito, com ônus limitado. Solicitação nº
59240.
RUI VICENTE OPPERMANN
Reitor
