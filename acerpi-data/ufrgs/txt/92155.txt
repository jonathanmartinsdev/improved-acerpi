Documento gerado sob autenticação Nº IAE.970.757.JCF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4739                  de  03/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de LÉLIO ANTÔNIO TEIXEIRA BRITO,  Professor do Magistério
Superior,  lotado  no  Departamento  de  Engenharia  Civil  da  Escola  de  Engenharia  e  com  exercício  no
Laboratório de Pavimentação, com a finalidade de realizar visita à University of California Davis, em Davis,
Estados Unidos,  no período compreendido entre 04/06/2019 e 10/06/2019,  incluído trânsito,  com ônus
limitado. Solicitação nº 84609.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
