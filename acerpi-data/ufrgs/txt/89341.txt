Documento gerado sob autenticação Nº GIE.126.992.FED, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2977                  de  04/04/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  07/03/2019,
correspondente ao grau Insalubridade Máxima, ao servidor AFONSO REGULY, Identificação Única 12952958,
Professor do Magistério Superior, com exercício no Programa de Pós-Graduação em Engenharia de Minas,
Metalúrgica e de Materiais da Escola de Engenharia, observando-se o disposto na Lei nº 8.112, de 11 de
dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas
consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.505258/2019-18, Código
SRH n° 23804.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
