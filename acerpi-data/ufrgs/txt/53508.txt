Documento gerado sob autenticação Nº LVH.297.176.PL4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2947                  de  23/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°48602,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ADMINISTRADOR, do Quadro de
Pessoal desta Universidade, RAFAELA REMIÃO PERES DA SILVA (Siape: 2406633 ),  para substituir   ANDREIA
DE ESPINDOLA LOPES (Siape: 2121748 ), Chefe do Setor de Patrimônio Histórico da SUINFRA, Código FG-1, em
seu afastamento por motivo de Laudo Médico do titular da Função, no período de 09/04/2018 a 23/04/2018,
com o decorrente pagamento das vantagens por 15 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
