Documento gerado sob autenticação Nº GNU.478.338.F13, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7965                  de  05/10/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
          Autorizar o afastamento no País de MARINA DOS REIS,  ocupante do cargo de Assistente em
Administração,  com lotação na Faculdade de Educação e exercício  na Secretaria  do Programa de Pós-
Graduação em Educação, com a finalidade de realizar estudos em nível de Mestrado, junto à Universidade
Federal do Rio Grande do Sul, no período compreendido entre 11/10/2018 e 10/10/2019, com ônus limitado.
Processo 23078.520318/2018-41.
RUI VICENTE OPPERMANN
Reitor.
