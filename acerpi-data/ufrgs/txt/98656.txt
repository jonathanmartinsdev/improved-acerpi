Documento gerado sob autenticação Nº UBR.620.479.C5C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8782                  de  27/09/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar GUILHERME VARGAS DE LIMA, Matrícula SIAPE 1251308, ocupante do cargo de Assistente
em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe
do Setor Administrativo e de Comunicação da Gerência Administrativa da Faculdade de Farmácia, Código FG-
7, Código SRH 1405, com vigência a partir de 01/10/2019. Processo nº 23078.526206/2019-85.
JANE FRAGA TUTIKIAN
Vice-Reitora.
