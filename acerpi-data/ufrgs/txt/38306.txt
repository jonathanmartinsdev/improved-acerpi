Documento gerado sob autenticação Nº ZZU.144.619.ER0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4273                  de  16/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  CARLOS  ALBERTO  SARAIVA  GONCALVES,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Bioquímica do Instituto de Ciências Básicas
da  Saúde,  com  a  finalidade  de  participar  do  "First  International  Workshop  of  Cuban  Network  of
Neuroimmunology", em Havana, Cuba, no período compreendido entre 09/06/2017 e 20/06/2017, incluído
trânsito, com ônus limitado. Solicitação nº 27996.
RUI VICENTE OPPERMANN
Reitor
