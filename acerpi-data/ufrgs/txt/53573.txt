Documento gerado sob autenticação Nº COA.078.035.OLJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3029                  de  25/04/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias 
RESOLVE
Dispensar, a partir de 02/04/2018, o ocupante do cargo de Assistente em Administração - 701200,
do Nível de Classificação DII, do Quadro de Pessoal desta Universidade, GUSTAVO PEREIRA, matrícula SIAPE
2267363 da função de Secretário Administrativo do Instituto Latino-Americano de Estudos Avançados, Código
SRH 1327, Código FG-4, para a qual foi designado pela Portaria nº 5852/2017 de 04/07/2017, publicada no
Diário Oficial da União de 06/07/2017. Processo nº 23078.507497/2018-21.
PHILIPPE OLIVIER ALEXANDRE NAVAUX
Decano do Conselho Universitário, no exercício da Reitoria.
