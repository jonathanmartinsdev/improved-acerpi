Documento gerado sob autenticação Nº MFY.218.851.2DQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8582                  de  19/09/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, HENRIQUE SAIDEL, matrícula SIAPE n° 3013820, lotado no Departamento de Arte
Dramática  do  Instituto  de  Artes,  como  Chefe  Substituto  do  Depto  de  Arte  Dramática  do  Instituto  de
Artes, com vigência a partir de 23/09/2019 até 23/09/2020, a fim de completar o mandato da professora
CRISTIANE  WERLANG,  sem  prejuízo  e  cumulativamente  com  a  função  de  Coordenador  Substituto  da
COMGRAD em Arte Dramática. Processo nº 23078.525381/2019-55.
JANE FRAGA TUTIKIAN
Vice-Reitora.
