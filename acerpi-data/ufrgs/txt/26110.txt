Documento gerado sob autenticação Nº BMZ.009.797.LV6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6112                  de  17/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de NICOLAS BRUNO MAILLARD, Professor do Magistério Superior,
lotado no Departamento de Informática Aplicada do Instituto de Informática e com exercício na Secretaria de
Relações Internacionais, com a finalidade de, como  representante da UFRGS, participar da "28th Annual EAIE
Conference  and  Exhibition",  em  Liverpool,  Inglaterra,  no  período  compreendido  entre  11/09/2016  e
17/09/2016, incluído trânsito, com ônus UFRGS (diárias e passagens). Solicitação nº 22301.
CARLOS ALEXANDRE NETTO
Reitor
