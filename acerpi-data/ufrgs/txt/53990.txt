Documento gerado sob autenticação Nº GLV.871.524.9V3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3313                  de  07/05/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  27/04/2018,   referente  ao  interstício  de
08/04/2016 a 26/04/2018, para o servidor THIAGO RAMOS DA ROSA,  ocupante do cargo de Técnico de
Tecnologia da Informação - 701226, matrícula SIAPE 1053469,  lotado  no  Centro de Processamento de
Dados, passando do Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de
Capacitação  D  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.509935/2018-96:
NELE - Inglês para TI - Módulo 2016/1 CH: 30 (06/04/2016 a 29/06/2016)
NELE - Inglês para TI - Módulo 2016/2 CH: 30 (31/08/2016 a 14/12/2016)
RNP - Segurança de Redes e Sistemas CH: 40 Carga horária utilizada: 30 hora(s) / Carga horária excedente: 10
hora(s) (16/04/2018 a 20/04/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
