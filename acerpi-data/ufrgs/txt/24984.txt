Documento gerado sob autenticação Nº TSV.271.361.2JQ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5504                  de  22/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme o Laudo Médico n°39838,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, JOAO LUIZ BECKER (Siape: 0353045 ),  para substituir  
WALTER MEUCCI NIQUE (Siape: 0355827 ), Coordenador do PPG em Administração, Código FUC, em seu
afastamento por motivo de Laudo Médico do titular da Função, no período de 13/06/2016 a 16/06/2016, com
o decorrente pagamento das vantagens por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
