Documento gerado sob autenticação Nº OBT.417.121.U5Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6788                  de  30/08/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.504879/2018-01,
23078.507252/2016-32,  do  contrato  nº  053/2017,  da  Lei  10.520/02  e  ainda  da  Lei  8.666/93,
 
            RESOLVE:
 
           Aplicar a sanção administrativa de MULTA de 5% (cinco por cento) sobre o valor da parcela que lhe deu
causa,  no  montante  de  R$  560,82  (quinhentos  e  sessenta  reais  e  oitenta  e  dois  centavos),  conforme
demonstrativo de cálculo constante no documento SEI nº 1175239 prevista no inciso II, item 03 da cláusula
décima  primeira  do  Contrato,  à  Empresa  W.S.  COMERCIO  DE  REFRIGERAÇÃO  E  EQUIPAMENTOS
INDUSTRIAIS  LTDA-ME,  CNPJ  n.º  13.624.180/0001-24,  pelo  atraso  na  execução dos  serviços  conforme
atestado pela DAL nos documentos SEI nº 0962288 e 1014742, bem como pelo NUDECON no documento SEI
nº 1172765 do processo 23078.504879/2018-01.
 
            Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
