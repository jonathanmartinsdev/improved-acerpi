Documento gerado sob autenticação Nº BRM.080.015.OH2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6872                  de  31/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°84856,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ALEXANDRE SANFELICE BAZANELLA (Siape: 1208018
),  para substituir   JOAO MANOEL GOMES DA SILVA JUNIOR (Siape: 1031115 ), Coordenador do PPG em
Engenharia Elétrica, Código FUC, em seu afastamento do país, no período de 01/08/2019 a 18/08/2019, com o
decorrente pagamento das vantagens por 18 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
