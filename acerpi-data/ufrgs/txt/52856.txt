Documento gerado sob autenticação Nº XJD.530.542.3IV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2592                  de  10/04/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
e,  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.516228/2016-94  e  nº
23078.520203/2017-76, da Lei 10.520/02, do Contrato nº 072/PROPLAN/NUDECON/2017 e da Lei 8.666/93,
          RESOLVE:
 
         Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 01 da cláusula décima primeira do
referido Contrato, à Empresa SUMAIA RODRIGUES ZAHRAN REDIN - EPP, CNPJ 07.225.848/0001-68, pelo
atraso de 01 (uma) hora na execução do serviço referente ao contrato em epígrafe, conforme atestado pela
SUINFRA nos documentos SEI nº 0786782 e 0867950, bem como pelo NUDECON no documento SEI 0940568
do processo administrativo 23078.520203/2017-76.
 
           Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
