Documento gerado sob autenticação Nº WZP.478.480.104, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6068                  de  17/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder prorrogação da jornada de trabalho reduzida, com remuneração proporcional à servidora
ANDRESSA  GIROTTO  VARGAS,  matrícula  SIAPE  n°  2383911,  ocupante  do  cargo  de  Assistente  em
Administração - 701200, lotada na Pró-Reitoria de Graduação e com exercício na Divisão de Emissão, Registro
e Revalidação de Diploma do Departamento de Consultoria em Registro Discentes da PROGRAD, alterando a
jornada de trabalho de oito  horas  diárias  e  quarenta  horas  semanais  para  seis  horas  diárias  e  trinta
semanais, nos termos dos artigos 5º ao 7º da Medida Provisória nº 2.174-28, de 24 de agosto de 2001, no
período de 21 de julho de 2019 a 30 de setembro de 2019, e conforme o Processo nº 23078.507137/2019-19.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
