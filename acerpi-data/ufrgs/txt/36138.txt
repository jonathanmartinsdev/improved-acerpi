Documento gerado sob autenticação Nº GNU.194.810.0QV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2698                  de  28/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  GABRIELA  TREVISOL  OURIQUES,  Matrícula  SIAPE  1858972,  ocupante  do  cargo  de
Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a
função de Chefe do Setor de Processo Seletivo da Divisão de Concursos Públicos do DDGP/PROGESP, Código
SRH 1096, código FG-4, com vigência a partir da data de publicação no Diário Oficial da União. Processo nº
23078.004733/2017-71.
JANE FRAGA TUTIKIAN
Vice-Reitora.
