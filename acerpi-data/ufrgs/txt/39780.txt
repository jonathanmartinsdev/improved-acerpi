Documento gerado sob autenticação Nº FPV.476.710.0OA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5359                  de  21/06/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de LEANDRO FARINA, Professor do Magistério Superior, lotado e
em exercício no Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística, com a
finalidade de realizar visita à  Western Norway University of Applied Sciences, em Bergen, Noruega, no
período compreendido entre 21/07/2017 e 25/08/2017, incluído trânsito, com ônus limitado. Solicitação nº
28742.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
