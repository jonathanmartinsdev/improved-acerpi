Documento gerado sob autenticação Nº YNH.941.637.5B8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1111                  de  31/01/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°49053,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MORGANA BAZZAN DESSUY (Siape: 2028836 ),  para
substituir   ADRIANO DE ARAÚJO GOMES (Siape: 1305743 ),  Coordenador da Comissão de Extensão do
Instituto de Química,  em seu afastamento por motivo de férias, no período de 03/02/2020 a 07/02/2020.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
