Documento gerado sob autenticação Nº GBD.265.298.M6R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2842                  de  01/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARCELO SOARES LUBASZEWSKI,  Professor do Magistério
Superior, lotado no Departamento de Engenharia Elétrica da Escola de Engenharia e com exercício no Parque
Científico e Tecnológico da Universidade Federal do Rio Grande do Sul, com a finalidade de realizar visita à
Uppsala Universitet,  em Uppsala,  Suécia,  e à Technological  University of Ostrava, em Ostrava, Repúbica
Tcheca, no período compreendido entre 22/04/2019 e 01/05/2019, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Planejamento e Administração/Projeto LISTO). Solicitação nº 72902.
RUI VICENTE OPPERMANN
Reitor
