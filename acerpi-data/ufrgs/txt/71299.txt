Documento gerado sob autenticação Nº RJU.635.530.6L0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8546                  de  24/10/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  JOANA  BOSAK  DE  FIGUEIREDO,  matrícula  SIAPE  n°  3314130,  lotada  e  em  exercício  no
Departamento de Artes Visuais do Instituto de Artes, da classe C  de Professor Adjunto, nível 02, para a classe
C   de  Professor  Adjunto,  nível  03,  referente  ao  interstício  de  25/11/2015  a  24/11/2017,  com vigência
financeira a partir de 11/09/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.507960/2018-35.
JANE FRAGA TUTIKIAN
Vice-Reitora.
