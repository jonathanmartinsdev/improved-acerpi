Documento gerado sob autenticação Nº MIL.234.913.QAK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6719                  de  25/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°33637,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA, do Quadro de Pessoal desta Universidade, JOSE ALBERTO CARDOSO DO PRADO (Siape: 0353162 ),
 para substituir   JOSE LUIZ MACHADO RIETJENS (Siape: 0351125 ), Coordenador do Setor de Transporte do
Grupo Frota da Faculdade de Agronomia, Código FG-4, em seu afastamento por motivo de férias, no período
de 24/07/2017 a 31/07/2017.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
