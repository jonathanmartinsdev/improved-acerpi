Documento gerado sob autenticação Nº PRU.381.618.MAG, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1586                  de  02/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°22828,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, ROSAURA DENISE CARBONE RIBAS (Siape:
0356367 ),   para substituir    ANA VERA FINARDI RODRIGUES (Siape:  0357700 ),  Chefe da Biblioteca de
Veterinária, Código FG-5, em seu afastamento por motivo de férias, no período de 15/02/2016 a 23/02/2016,
com o decorrente pagamento das vantagens por 9 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
