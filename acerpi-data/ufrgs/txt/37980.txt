Documento gerado sob autenticação Nº QIM.277.151.V44, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4024                  de  09/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme solicitação de férias nº 30366
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de Assistente em Administração, do
Quadro de Pessoal desta Universidade, DANIELA FONSECA DA SILVA (Siape: 2217366),   para substituir
FABIANA HITOMI TANABE (Siape: 2111379 ), Chefe do Refeitório do CLN do Núcleo de Assistência ao Aluno d
Diretoria Adm do Campus Litoral Norte, Código SRH 1463, em seu afastamento por motivo de férias, no
período de 02/05/2017 a 11/05/2017.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
