Documento gerado sob autenticação Nº JYS.465.495.63A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7038                  de  06/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
ALTERAR o percentual  de Incentivo à Qualificação concedido ao servidor LUCIANO MACHADO
LIMA,  ocupante do cargo de Assistente em Administração-701200, lotado na Faculdade de Odontologia,
SIAPE 0357479, para 30% (trinta por cento), a contar de 22/07/2019, tendo em vista a conclusão do curso de
Especialização em Educação Ambiental e Sustentabilidade, conforme o Processo nº 23078.519043/2019-84.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
