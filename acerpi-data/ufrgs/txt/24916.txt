Documento gerado sob autenticação Nº ZFN.243.896.UON, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5459                  de  20/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas e considerando o
disposto no artigo 3º da Portaria nº 404, do Ministro de Estado da Educação, publicada no DOU de 07 de
maio de 2009,
RESOLVE
Efetivar  a requisição, pelo prazo de 01 (um) ano da seguinte servidora, pertencente ao Quadro de
Pessoal desta Universidade, na forma abaixo indicada:
Nome: JULIANA RIBEIRO AZEVEDO
Matrícula: 1600590
Cargo: Assistente em Administração
           Para: Defensoria Pública da União
Amparo legal:artigo 4º da Lei nº 9.020, de 30 de março de 1995
Ônus: órgão requisitado
Processo: 23078.028723/2015-60
Art. 1°. Caberá ao órgão requisitante efetivar a apresentação da servidora ao seu órgão de origem
ao término da requisição.
Art.  2°.  Cumpre ao requisitante comunicar,  mensalmente,  ao órgão ou entidade requisitado a
freqüência da servidora.
Art. 3°. Esta portaria entra em vigor na data de sua publicação.
CARLOS ALEXANDRE NETTO
Reitor
