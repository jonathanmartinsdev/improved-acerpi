Documento gerado sob autenticação Nº GMB.069.763.SHS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1826                  de  02/03/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a LUCIA COUTO TERRA,
matrícula SIAPE nº 0353617, no cargo de Professor da Carreira do Magistério do Ensino Básico, Técnico e
Tecnológico,  classe  D,  nível  4,  do  Quadro desta  Universidade,  no  regime de  dedicação exclusiva,  com
exercício  no Departamento de Ciências  Exatas e  da Natureza do Colégio de Aplicação,  com proventos
integrais. Processo 23078.000584/2018-52.
RUI VICENTE OPPERMANN
Reitor.
