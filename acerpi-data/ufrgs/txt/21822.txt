Documento gerado sob autenticação Nº AAH.056.050.IHC, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3375                  de  06/05/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Remover, a partir de 06 de maio de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
LUCI MARI CASTRO LEITE JORGE, ocupante do cargo de Instrumentador Cirúrgico, Ambiente Organizacional
Ciências da Saúde, Código 701207, Classe D, Nível de Capacitação III, Padrão de Vencimento 13, SIAPE nº.
1103276 da Pró-Reitoria de Gestão de Pessoas para a lotação Hospital de Clínicas Veterinárias, com novo
exercício no Centro Cirúrgico da CGA.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
