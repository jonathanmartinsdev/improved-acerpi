Documento gerado sob autenticação Nº PRD.118.586.2M7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4138                  de  07/06/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme o Laudo Médico n°39149,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ADMINISTRADOR, do Quadro de
Pessoal desta Universidade, CRISTIANE BASSO (Siape: 1744267 ),  para substituir   ELIANA VENTORINI (Siape:
1688979 ), Coordenador da Coordenadoria de Concursos, Mobilidade e Acompanhamento, vinc. ao DDGP da
PROGESP, Código CD-4, em seu afastamento por motivo de Laudo Médico do titular da Função, no período
de 06/05/2016 a 09/05/2016, com o decorrente pagamento das vantagens por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
