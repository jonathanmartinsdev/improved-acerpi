Documento gerado sob autenticação Nº SVX.438.945.5I1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7376                  de  15/08/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir de 19/08/2019, como segue:
 
Criar:  Coordenador  do Escritório  de  Interação e  Transferência  de  Tecnologia  da  Secretaria  de
Desenvolvimento Tecnológico, Código SRH 1542, Código FG-2.
 
 
Processo nº 23078.521519/2019-47.
 
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
