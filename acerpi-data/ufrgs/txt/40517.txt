Documento gerado sob autenticação Nº FBH.020.460.LRO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5882                  de  06/07/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CARLOS HOPPEN, Professor do Magistério Superior, lotado no
Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística e com exercício no
Programa de Pós-Graduação em Matemática Aplicada, com a finalidade de participar da "18th International
Conference on Random Structures and Algorithms", em Gniezno, Polônia, no período compreendido entre
05/08/2017 e 11/08/2017, incluído trânsito, com ônus CNPq (Projeto nº 448.754/2014-2). Solicitação nº 28267.
RUI VICENTE OPPERMANN
Reitor
