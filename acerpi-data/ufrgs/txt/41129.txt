Documento gerado sob autenticação Nº DFU.683.362.MSH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6305                  de  17/07/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°35981,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TÉCNICO EM CONTABILIDADE,
do Quadro de Pessoal desta Universidade, JULIANA DE LIMA VIEIRA MACHADO (Siape: 2156700 ),  para
substituir   GUILHERME LEHNEMANN RAMOS (Siape: 1707835 ),  Chefe do Setor Financeiro da SUINFRA,
Código FG-1, em seu afastamento por motivo de férias, no período de 17/07/2017 a 26/07/2017, com o
decorrente pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
