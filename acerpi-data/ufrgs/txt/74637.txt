Documento gerado sob autenticação Nº KCR.159.732.NAO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             184                  de  07/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Homologar o parecer que aprova a servidora técnico-administrativa JULIANA GRACIELI FONTANA,
ocupante  do  cargo  de  Assistente  em  Administração,  no  estágio  probatório  cumprido  no  período  de
05/01/2016 até 05/01/2019, fazendo jus, a partir desta data, à estabilidade no serviço público federal.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
