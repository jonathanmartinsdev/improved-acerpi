Documento gerado sob autenticação Nº ZVA.115.706.AVI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             211                  de  06/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 3958, de 29 de maio de 2018
RESOLVE
Nomear
            
      TAKEYOSHI IMASATO
      
      CAMILA FOCKINK
      
      NÁDIA ARAUJO COSTA DORNELES
      
      ANDREA FARIAS WILDE (Suplente)
      
para comporem o Núcleo de Gestão de Desempenho da Escola de Administração, por um mandato
até 1º de outubro de 2020, com a finalidade de organizar a implementação e a execução do Processo de
Avaliação de Desempenho dos Servidores Técnico-administrativos da UFRGS, observando os termos da Lei
11.091, de 12/01/2005, e das Decisões 939/2008, 328/2010 e 417/2014 do Conselho Universitário.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas.
