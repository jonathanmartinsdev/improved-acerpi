Documento gerado sob autenticação Nº OEC.485.829.JQV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             115                  de  04/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°46855,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA, do Quadro de Pessoal desta Universidade, EDSON LUIS SOUZA DE ARAUJO (Siape: 0356950 ),  para
substituir   SILVIA REGINA CENTENO (Siape: 1038180 ), Secretário do PPG em Biologia Celular e Molecular,
Código FG-7, em seu afastamento por motivo de férias, no período de 07/01/2019 a 18/01/2019.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
