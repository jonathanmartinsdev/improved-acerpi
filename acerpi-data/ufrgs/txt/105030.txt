Documento gerado sob autenticação Nº HSO.796.697.2TJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1386                  de  07/02/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Retificar, por decisão judicial proferida no processo nº 5049427-39.2018.4.04.7100, a Portaria n°
3819/2015, de 18/05/2015 que concedeu progressão funcional, por avaliação, no Quadro desta Universidade,
ao  Professor  RENAR JOAO BENDER,  com exercício  no  Departamento  de  Horticultura  e  Silvicultura  da
Faculdade de Agronomia da classe de Professor Associado, nível 02, para a classe de Professor Associado,
nível 03, referente ao interstício de 11/01/2011 a 10/01/2013, com vigência financeira a partir de 12 de maio
de 2015. Processo nº 23078.201897/2015-83.
 
 
Onde se lê:
...com vigência financeira a partir de 12 de maio de 2015...
 
Leia-se:
...com vigência financeira a partir de 11 de janeiro de 2013, ficando ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora.
