Documento gerado sob autenticação Nº CME.281.620.9F5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5897                  de  12/07/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ALFREDO ALEJANDRO GUGLIANO,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Ciência Política do Instituto de Filosofia e Ciências
Humanas, com a finalidade de participar de reunião junto à Universidad Nacional Autônoma de México e do
"X  Congresso  ALACIP",  na  Cidade  do  México  e  Monterrey,  México,  no  período  compreendido  entre
25/07/2019 e 04/08/2019 (duas diárias internacionais), incluído trânsito, com ônus CAPES/PROAP. Solicitação
nº 85281.
RUI VICENTE OPPERMANN
Reitor
