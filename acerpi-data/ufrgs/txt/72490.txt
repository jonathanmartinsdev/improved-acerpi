Documento gerado sob autenticação Nº HTJ.111.798.ACK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9445                  de  22/11/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor FERNANDO DORNELLES, matrícula SIAPE n° 2855225, lotado e em exercício no Departamento de
Hidromecânica e Hidrologia do Instituto de Pesquisas Hidráulicas, da classe C  de Professor Adjunto, nível 03,
para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 11/10/2016 a 10/10/2018, com
vigência financeira a partir de 18/10/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de
2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.521334/2018-51.
RUI VICENTE OPPERMANN
Reitor.
