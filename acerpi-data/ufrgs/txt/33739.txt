Documento gerado sob autenticação Nº RCR.453.743.AL6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             990                  de  30/01/2017
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7793, de 03 de outubro de 2016
RESOLVE
Prorrogar por mais 15 (quinze) dias o prazo estabelecido pela Portaria nº 444 de 16 de janeiro de
2016,  a  fim de  que  a  Comissão  dê  continuidade  à  elaboração  de  Termo de  Referência  para  licitar  a
contratação de serviços de manutenção de instalações elétricas, conforme consta no processo SEI UFRGS nº
23078.500579/2017-64.
 
ANDREA PINTO LOGUERCIO
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
