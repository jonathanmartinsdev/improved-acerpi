Documento gerado sob autenticação Nº WZU.429.220.E5I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2753                  de  29/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  ROSIMERI  AQUINO  DA  SILVA,  matrícula  SIAPE  n°  1795689,  lotada  e  em  exercício  no
Departamento de Ensino e Currículo da Faculdade de Educação, da classe C  de Professor Adjunto, nível 03,
para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 06/07/2014 a 05/07/2016, com
vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro
de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.510426/2016-44.
JANE FRAGA TUTIKIAN
Vice-Reitora.
