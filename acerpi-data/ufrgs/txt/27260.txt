Documento gerado sob autenticação Nº XUO.995.301.8PA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7025                  de  08/09/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
             Designar o ocupante do cargo de Professor do Magistério Superior LEANDRO DO AMARAL
DORNELES DE DORNELES, matrícula SIAPE 1530873, Coordenador Substituto da Comissão de Graduação de
Direito,  para  substituir,  em  caráter  "pró-tempore",  o  titular  desta  função  em  seus  afastamentos  ou
impedimentos regulamentares, a partir da data deste ato. Processo nº 23078.019509/2016-01.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
