Documento gerado sob autenticação Nº BKQ.228.592.AHK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2067                  de  01/03/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5042869-51.2018.4.04.7100,  da  2ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, da servidora ANTONIETA LUZ DA SILVA,  matrícula SIAPE n° 0351177, aposentada no cargo
de Técnico em Secretariado - 701275, para o nível IV, conforme o Processo nº 23078.504332/2019-89.
RUI VICENTE OPPERMANN
Reitor
