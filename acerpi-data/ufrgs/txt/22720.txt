Documento gerado sob autenticação Nº JIP.588.321.SUN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3925                  de  30/05/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de DENISE TOLFO SILVEIRA, Professor do Magistério Superior,
lotada e em exercício no Departamento de Enfermagem Médico-Cirúrgica da Escola de Enfermagem, com a
finalidade de participar do "13th International Congress in Nursing Informatics",  em Genebra, Suíça,  no
período compreendido entre 24/06/2016 e 30/06/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa - diárias). Solicitação nº 18765.
CARLOS ALEXANDRE NETTO
Reitor
