Documento gerado sob autenticação Nº TIV.445.889.CC4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1436                  de  16/02/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Tornar insubsistente a Portaria nº 986 de 01/02/2018 que designou temporariamente CHRISTINE
WETZEL (Siape: 0357621 ), para substituir EGLE REJANE KOHLRAUSCH (Siape: 1251601 ), Chefe do Depto de
Assistência e Orientação Profissional da Escola de Enfermagem,Processo SEI nº23078.501958/2018-52
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
