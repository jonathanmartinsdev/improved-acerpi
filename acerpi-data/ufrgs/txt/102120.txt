Documento gerado sob autenticação Nº BUT.957.184.LT0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11056                  de  11/12/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de NATHAN WILLIG LIMA,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Física do Instituto de Física, com a finalidade de atuar como
Professor Visitante junto à University of Copenhagen, em Copenhage, Dinamarca, no período compreendido
entre 02/01/2020 e 31/03/2020, com ônus CAPES/PRINT/UFRGS. Processo nº 23078.532314/2019-97.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
