Documento gerado sob autenticação Nº AOZ.242.695.GFA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7732                  de  27/09/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, MARCELINO DE SOUZA, matrícula SIAPE n° 1346009, lotado no Departamento
de  Economia  e  Relações  Internacionais  da  Faculdade  de  Ciências  Econômicas,  para  exercer  a  função
de Coordenador do PPG em Agronegócios, código SRH 1107, código FUC, com vigência a partir de 07/10/2018
até 06/10/2020. Processo nº 23078.525586/2018-50.
JANE FRAGA TUTIKIAN
Vice-Reitora.
