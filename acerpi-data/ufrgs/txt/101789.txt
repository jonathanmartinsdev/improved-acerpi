Documento gerado sob autenticação Nº ZQV.478.639.GVB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10835                  de  05/12/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar  no Instituto  de Ciências  Básicas  da Saúde,  com exercício  no Setor  de Infraestrutura  da
Gerência Adm. do Instituto de Ciências Básicas da Saúde, Ambiente Organizacional Administrativo, YURY
ROCHA CHAVARÉ, nomeado conforme Portaria Nº 9787/2019 de 30 de outubro de 2019, publicada no Diário
Oficial  da União no dia 31 de outubro de 2019,  em efetivo exercício desde 28 de novembro de 2019,
ocupante do cargo de ASSISTENTE EM ADMINISTRAÇÃO, classe D, nível I, padrão 101, no Quadro de Pessoal
desta Universidade. Processo n° 23078.532914/2019-55.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
