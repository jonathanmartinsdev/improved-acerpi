Documento gerado sob autenticação Nº EGZ.876.087.M3N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6561                  de  21/07/2017
Constituição  de  Comissão  de  Sindicância
Investigativa da UFRGS
O PRÓ-REITOR DE ASSUNTOS ESTUDANTIS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL,  no uso de suas atribuições,  considerando o disposto na Portaria nº  7625,  de 29 de
setembro de 2016
RESOLVE
Prorrogar Sindicância Investigativa a ser processada pela Comissão integrada pelas servidoras 
ISABEL DE ABRANTES TIMM, ocupante do cargo de Assistente em Administração, lotada na Pró-Reitoria de
Assuntos Estudantis e com exercício na Gerência Administrativa da PRAE, CAROLINA TAGLIANI RIBEIRO,
ocupante do cargo de Engenheiro-área, lotada na Pró-Reitoria de Assuntos Estudantis e com exercício na
Gerência Administrativa da PRAE, para sob a Presidência da primeira, no prazo de trinta dias, concluir os
trabalhos no processo nº 23078.011955/2017-41
ELTON LUIS BERNARDI CAMPANARO
Pró-Reitor de Assuntos Estudantis em exercício
