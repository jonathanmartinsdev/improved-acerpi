Documento gerado sob autenticação Nº OHT.643.251.5G4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5644                  de  29/06/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar no Instituto de Filosofia e Ciências Humanas, com exercício no Núcleo Acadêmico de Pós-
Graduação do Instituto de Filosofia e Ciências Humanas, DIMITRI MORAES HATSCHA, nomeado conforme
Portaria Nº 4087/2017 de 10 de maio de 2017, publicada no Diário Oficial da União no dia 11 de maio de
2017,  em  efetivo  exercício  desde  19  de  junho  de  2017,  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  Ambiente Organizacional  Administrativo,  classe D,  nível  I,  padrão 101,  no Quadro de
Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
