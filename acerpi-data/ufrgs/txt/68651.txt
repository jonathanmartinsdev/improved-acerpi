Documento gerado sob autenticação Nº CWZ.674.990.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6562                  de  22/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LILIANE BASSO BARICHELLO, Professor do Magistério Superior,
lotada e  em exercício  no Departamento de Matemática  Pura e  Aplicada do Instituto de Matemática  e
Estatística, com a finalidade de participar da "4th International Conference on Physics and Technology of
Reactors  and  Applications",  em  Marrakesh,  Marrocos,  no  período  compreendido  entre  15/09/2018  e
21/09/2018, incluído trânsito, com ônus limitado. Solicitação nº 58157.
RUI VICENTE OPPERMANN
Reitor
