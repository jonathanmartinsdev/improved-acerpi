Documento gerado sob autenticação Nº PUO.441.300.L2C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10676                  de  23/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°37352,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, DAVI NUNES DE ALCANTARA (Siape: 2069268 ),
 para substituir   MARCIO ALEXANDRE RODRIGUEZ DE RODRIGUES (Siape: 1758968 ), Coordenador do Núcleo
Acadêmico da Gerência Administrativa do Instituto de Pesquisas Hidráulicas (IPH),  Código FG-5, em seu
afastamento por motivo de férias, no período de 27/11/2017 a 01/12/2017, com o decorrente pagamento das
vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
