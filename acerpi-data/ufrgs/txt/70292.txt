Documento gerado sob autenticação Nº CTE.566.165.ST6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7785                  de  28/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LEONIDAS GARCIA SOARES, Professor do Magistério Superior,
lotado e em exercício no Departamento de Design e Expressão Gráfica da Faculdade de Arquitetura, com a
finalidade de participar da "5th CIDAG - International Conference in Design and Graphic Arts", em Lisboa,
Portugal, no período compreendido entre 23/10/2018 e 27/10/2018, incluído trânsito, com ônus limitado.
Solicitação nº 58947.
RUI VICENTE OPPERMANN
Reitor
