Documento gerado sob autenticação Nº EOW.543.790.2D3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             565                  de  18/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  ROMMULO  VIEIRA  CONCEIÇÃO,  matrícula  SIAPE  n°  1298184,  lotado  no  Departamento  de
Geologia do Instituto de Geociências e com exercício no Programa de Pós-Graduação em Geociências, da
classe D  de Professor Associado, nível 04, para a classe E  de Professor Titular,  referente ao interstício de
02/08/2016 a 07/12/2018, com vigência financeira a partir de 08/12/2018, de acordo com o que dispõe a Lei
12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº
23078.535069/2018-99.
JANE FRAGA TUTIKIAN
Vice-Reitora.
