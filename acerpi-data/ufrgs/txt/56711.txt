Documento gerado sob autenticação Nº QBT.524.373.7TJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5349                  de  19/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  18/07/2018,   referente  ao  interstício  de
18/01/2017 a 17/07/2018, para a servidora KAREN WERLANG LUNKES, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2081430,  lotada  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV,
em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.516336/2018-29:
Formação Integral de Servidores da UFRGS III CH: 73 (13/04/2016 a 19/06/2018)
Fundação Bradesco - Postura e Imagem Profissional CH: 59 (27/04/2016 a 29/04/2016)
Fundação  Bradesco  -  Comunicação  Escrita  CH:  91  Carga  horária  utilizada:  18  hora(s)  /  Carga  horária
excedente: 73 hora(s) (25/04/2016 a 10/05/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
