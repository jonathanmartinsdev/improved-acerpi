Documento gerado sob autenticação Nº EHC.458.416.MDC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6579                  de  23/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar RENATA CARDOSO DE SOUZA, Matrícula SIAPE 3062373, ocupante do cargo de Assistente
em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe
da Divisão Financeira da Gerência Administrativa do Instituto de Biociências, código SRH 599, código FG-7,
com vigência a partir de 27/08/2018. Processo nº 23078.521417/2018-41.
JANE FRAGA TUTIKIAN
Vice-Reitora.
