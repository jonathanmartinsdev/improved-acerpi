Documento gerado sob autenticação Nº WFP.073.845.9JC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2907                  de  19/04/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar ENRIQUE MUNARETTI, matrícula SIAPE n° 1565836, ocupante do cargo de Professor do
Magistério  Superior,  classe  Adjunto,  lotado  no  Departamento  de  Engenharia  de  Minas  da  Escola  de
Engenharia, do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Chefe do Depto de
Engenharia de Minas da Escola de Engenharia,  Código SRH 208,  código FG-1,  com vigência a partir  da
publicação no Diário Oficial da União até 29/06/2018, a fim de completar o mandato do Professor Carlos
Otavio Petter, conforme artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.502313/2018-37.
RUI VICENTE OPPERMANN
Reitor.
