Documento gerado sob autenticação Nº LTO.782.087.8AM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4890                  de  09/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°47535,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  MARILIA  LAZAROTTO  (Siape:  2091234  ),   para
substituir   GILMAR SCHAFER (Siape: 2322630 ), Chefe do Depto de Horticultura e Silvicultura da Faculdade de
Agronomia, Código FG-1,  em seu afastamento no país,  no período de 12/07/2018 a 13/07/2018, com o
decorrente pagamento das vantagens por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
