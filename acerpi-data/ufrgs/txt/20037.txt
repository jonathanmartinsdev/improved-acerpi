Documento gerado sob autenticação Nº NCW.187.546.I2B, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2399                  de  01/04/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE:
Designar MATEUS DALMORO, CPF nº 775156094, Matrícula SIAPE 2057116, ocupante do cargo de
Administrador, Código 701001, do Quadro de Pessoal desta Universidade, para exercer a função de Diretor
da Divisão de Qualificação e Aperfeiçoamento da Escola de Desenvolvimento de Servidores da UFRGS, Código
SRH 1382, Código FG-1, com vigência a partir de 06/04/2016. Processo nº 23078.006738/2016-58.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
