Documento gerado sob autenticação Nº KLQ.710.760.SP7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             498                  de  16/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério do Ensino Básico, Técnico e Tecnológico,
classe D, do Quadro de Pessoal desta Universidade, ANA CRISTINA CYPRIANO PEREIRA, matrícula SIAPE n°
2174464, lotada no Colégio de Aplicação, para exercer a função de Coordenadora da COMGRAD em Relações
Públicas, Código SRH 1503, código FUC, com vigência a partir de 01/02/2019 até 31/01/2021. Processo nº
23078.500161/2019-19.
JANE FRAGA TUTIKIAN
Vice-Reitora.
