Documento gerado sob autenticação Nº HSG.990.573.GL6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10740                  de  24/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar SUSAN BONEBERG DE SOUZA, Matrícula SIAPE 2146495, ocupante do cargo de Técnico
em Nutrição e Dietética, Código 701252, do Quadro de Pessoal desta Universidade, para exercer a função de
Chefe do Refeitório Universitário do Campus Litoral Norte, código SRH 1425, código FG-3, com vigência a
partir da data de publicação no Diário Oficial da União. Processo nº 23078.520255/2017-42.
RUI VICENTE OPPERMANN
Reitor.
