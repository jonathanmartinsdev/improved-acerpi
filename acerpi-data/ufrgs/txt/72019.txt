Documento gerado sob autenticação Nº KNU.242.013.DGR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9108                  de  09/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°52659,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, JOSE ANTONIO LUMERTZ (Siape: 1076887 ),  para
substituir    MARIA  IVANICE  VENDRUSCOLO (Siape:  2577132  ),  Coordenador  da  COMGRAD de  Ciências
Atuariais, Código FUC, em seu afastamento por motivo de Laudo Médico do titular da Função, no período de
28/10/2018 a 26/11/2018, com o decorrente pagamento das vantagens por 30 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
