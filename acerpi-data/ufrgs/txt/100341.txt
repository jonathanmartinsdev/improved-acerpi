Documento gerado sob autenticação Nº GDU.879.022.C19, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9888                  de  01/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de FERNANDO GONÇALVES AMARAL,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Engenharia de Produção e Transportes da Escola de
Engenharia, com a finalidade de realizar visita às École centrales, em Marseille, Lille, Lyon, Nantes e Paris,
França,  no  período  compreendido  entre  05/11/2019  e  14/11/2019,  incluído  trânsito,  com  ônus
CAPES/BRAFITEC.  Solicitação  nº  87456.
RUI VICENTE OPPERMANN
Reitor
