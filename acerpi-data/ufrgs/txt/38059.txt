Documento gerado sob autenticação Nº JJC.721.812.7NB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4097                  de  10/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora CRISTINA DA SILVA SILVEIRA FUMACO, ocupante do cargo de  Revisor de
Textos Braille - 701211, lotada na Pró-Reitoria de Gestão de Pessoas, SIAPE 2370389, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 21/02/2017, tendo em vista a conclusão do
curso de Graduação em Pedagogia - Licenciatura, conforme o Processo nº 23078.002563/2017-91.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
