Documento gerado sob autenticação Nº NIC.612.940.LFR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             85                  de  03/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de PATRICE SCHUCH, Professor do Magistério Superior, lotada e
em  exercício  no  Departamento  de  Antropologia  do  Instituto  de  Filosofia  e  Ciências  Humanas,  com  a
finalidade de realizar visita à University College London, em Londres, Inglaterra, no período compreendido
entre 27/01/2020 e 11/02/2020, incluído trânsito, com ônus limitado. Solicitação nº 89515.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
