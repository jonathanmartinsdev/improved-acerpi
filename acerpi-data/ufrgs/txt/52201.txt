Documento gerado sob autenticação Nº ZFC.037.177.IGL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2197                  de  23/03/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  JORGE  ALBERTO  QUILLFELDT,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Biofísica do Instituto de Biociências, com a finalidade de
participar do "Nintieth Annual Meeting  Midwestern Psychological Association", em Chicago, IL, da "2018
International Conference on Learning and Memory", em Huntington Beach, CA e  realizar visita ao National
Aeronautics and Space Administration - NASA, em Houston, Tx, Estados Unidos, no período compreendido
entre 11/04/2018 e 23/04/2018, incluído trânsito, com ônus limitado. Solicitação nº 34489.
RUI VICENTE OPPERMANN
Reitor
