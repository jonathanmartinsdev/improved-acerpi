Documento gerado sob autenticação Nº DHA.780.905.NHE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             892                  de  28/01/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°89664,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, FERNANDA DA SILVA MOMO (Siape: 1056390 ),  para
substituir   ARIEL BEHR (Siape: 2770215 ), Chefe do Depto de Ciências Contábeis e Atuariais da FCE, Código
FG-1, em seu afastamento no país, no período de 26/01/2020 a 28/01/2020, com o decorrente pagamento
das vantagens por 3 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
