Documento gerado sob autenticação Nº MVE.266.235.AP3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5811                  de  04/07/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de JOSE VICENTE TAVARES DOS SANTOS, Professor do Magistério
Superior, lotado no Departamento de Sociologia do Instituto de Filosofia e Ciências Humanas e com exercício
no Instituto Latino-Americano de Estudos Avançados, com a finalidade de participar da "Annual Conference
of  the  South  African  Sociological  Association",  em  Mafikeng,  República  da  África  do  Sul,  no  período
compreendido entre 01/07/2017 e 07/07/2017, incluído trânsito, com ônus limitado. Solicitação nº 29209.
RUI VICENTE OPPERMANN
Reitor
