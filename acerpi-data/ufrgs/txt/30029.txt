Documento gerado sob autenticação Nº ONM.755.602.06R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8832                  de  01/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 64 (sessenta e quatro) dias de licença para capacitação, nos termos do artigo 87 da Lei nº
8.112, de 11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a
servidora EVELYSE RAMOS ITAQUI HERNANDEZ, com exercício na Coordenação Acadêmica da SEAD, a ser
usufruída no período de 11/11/2016 a 13/01/2017, referente ao quinquênio de 10/03/2011 a 09/03/2016, a
fim de participar do curso "As TIC aplicadas no Ensino Superior", conforme Processo nº 23078.023401/2016-
13.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
