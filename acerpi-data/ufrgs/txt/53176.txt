Documento gerado sob autenticação Nº VMD.737.907.NK5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2771                  de  13/04/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar o afastamento no país de LUIZ ALBERTO GRIJO,  ocupante do cargo de Professor do
Magistério Superior,  lotado no Departamento de História do Instituto de Filosofia e Ciências Humanas e com
exercício no Programa de Pós-Graduação em História,  com a finalidade de ministrar disciplina junto ao
Instituto Federal de Educação, Ciência e Tecnologia do Piauí, em Teresina, Brasil, no período compreendido
entre 13/05/2018 e 27/05/2018, incluído trânsito, com ônus limitado. Solicitação n° 44973.
JANE FRAGA TUTIKIAN
Vice-Reitora.
