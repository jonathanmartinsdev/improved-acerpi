Documento gerado sob autenticação Nº UKQ.853.997.MSA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4895                  de  05/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/07/2016,   referente  ao  interstício  de
05/01/2015 a 04/07/2016, para o servidor GABRIEL FARIA MARTINS, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2185980,  lotado  na  Escola de Administração, passando do Nível
de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.010088/2015-64:
Formação Integral de Servidores da UFRGS I CH: 34 (28/04/2015 a 18/05/2016)
Fundação  Bradesco  -  Comunicação  Escrita  CH:  91  Carga  horária  utilizada:  56  hora(s)  /  Carga  horária
excedente: 35 hora(s) (26/02/2015 a 15/04/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
