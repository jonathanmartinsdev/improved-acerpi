Documento gerado sob autenticação Nº SGA.940.819.H28, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9703                  de  30/11/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar,  a partir  da data de publicação no Diário Oficial  da União,  o ocupante do cargo de
Professor do Magistério Superior, classe Titular, do Quadro de Pessoal desta Universidade, JOSE VICENTE
TAVARES DOS SANTOS, matrícula SIAPE n° 0353328,  da função de Coordenador do PPG em Segurança
Cidadã, Código SRH 1526, código FUC, para a qual foi designado pela Portaria 8503/2017, de 11/09/2017,
publicada no Diário Oficial da União do dia 14/09/2017. Processo nº 23078.530991/2018-90.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
