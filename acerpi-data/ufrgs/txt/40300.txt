Documento gerado sob autenticação Nº NZF.925.236.6UD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5660                  de  29/06/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar no Instituto de Química, com exercício no Centro de Gestão de Tratamento de Resíduos
Químicos, EDUARDO RIBEIRO RICKROT, nomeado conforme Portaria Nº 4526/2017 de 22 de maio de 2017,
publicada no Diário Oficial da União no dia 23 de maio de 2017, em efetivo exercício desde 12 de junho de
2017, ocupante do cargo de TÉCNICO DE LABORATÓRIO ÁREA, Ambiente Organizacional Ciências Exatas e da
Natureza, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
