Documento gerado sob autenticação Nº MQU.523.675.FNK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3539                  de  11/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora SIMONE KRAUSE FERRAO,
ocupante do cargo de Técnico de Laboratório Área-701244, lotada na Faculdade de Farmácia, SIAPE 1037871,
para 52% (cinquenta e dois por cento), a contar de 31/03/2016, tendo em vista a conclusão do curso de
Mestrado em Biociências e Reabilitação, conforme o Processo nº 23078.007138/2016-15.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
