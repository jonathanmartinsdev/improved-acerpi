Documento gerado sob autenticação Nº SCF.960.119.GLG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2162                  de  08/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  ao  servidor  JEFERSON LUIS  DE SOUZA DIAS,  ocupante  do  cargo  de   Operador  de
Máquina Copiadora - 701454, lotado na Faculdade de Biblioteconomia e Comunicação, SIAPE 0358789, o
percentual de 25% (vinte e cinco por cento) de Incentivo à Qualificação, a contar de 02/01/2019, tendo em
vista a conclusão do curso de Curso Superior de Tecnologia em Gestão Pública, conforme o Processo nº
23078.528235/2018-09.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
