Documento gerado sob autenticação Nº ZUS.132.702.21Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9797                  de  23/10/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar PEDRO LUIS OLIVEIRA VAGNER, Matrícula SIAPE 1878669, ocupante do cargo de Técnico
de Tecnologia da Informação, Código 701226, do Quadro de Pessoal desta Universidade, para exercer a
função de Chefe do Setor de Informática da FABICO, código SRH 640, código FG-7, com vigência a partir da
data de publicação no Diário Oficial da União. Processo nº 23078.008383/2016-31.
RUI VICENTE OPPERMANN
Reitor.
