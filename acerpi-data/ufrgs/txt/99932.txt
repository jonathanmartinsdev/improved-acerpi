Documento gerado sob autenticação Nº HFU.332.016.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9635                  de  23/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora VALENTINA COUTINHO BALDOTO GAVA CHAKR,  matrícula SIAPE n° 1440834,  lotada e em
exercício no Departamento de Pediatria da Faculdade de Medicina, da classe C  de Professor Adjunto, nível
01, para a classe C  de Professor Adjunto, nível 02, referente ao interstício de 09/10/2017 a 08/10/2019, com
vigência financeira a partir de 09/10/2019, conforme decisão judicial proferida no processo nº 5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.523568/2019-14.
RUI VICENTE OPPERMANN
Reitor.
