Documento gerado sob autenticação Nº DUO.190.719.P0M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             544                  de  14/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
 Retificar as Portarias  nº  11567/2019, de 28/12/2019 e 499/2020, de 13/01/2020, que designou,
temporariamente, ADRIANA MORE PACHECO (Siape: 1736538) para substituir JOAO CARLOS OLIVA (Siape:
1081416) na função de Coordenador da Comissão de Extensão da ESEF, Processo SEI Nº 23078.500242/2020-
52
 
onde se lê:
 
"...no período de 02/01/2020 a 10/01/2020...";
 
  leia-se:
 
"...no período de 02/01/2020 a 06/01/2020", ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
