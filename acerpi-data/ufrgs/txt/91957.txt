Documento gerado sob autenticação Nº ZFV.321.537.Q7P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
DEPARTAMENTO DE DESENVOLVIMENTO E GESTÃO DE PESSOAS
ESCOLA DE DESENVOLVIMENTO DE SERVIDORES DA UFRGS
Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
(51) 3308-3015/3219/3914/4596 - edufrgs@progesp.ufrgs.br
Processo n°: 23078.508437/2019-15
Servidor: ANTONIO CARLOS BICCA RANGEL
Cargo: Vigilante
Lotação: Coordenadoria de Segurança da UFRGS
Ambiente Organizacional: Infraestrutura
Nível de Classificação e Nível de Capacitação: D III
PARECER N° 743/2019
Trata  este  expediente  da  retificação  do  Parecer  n°  537/2019,  de  15/04/2019,  que  concerne
a concessão de Horário Especial para servidor estudante a ANTONIO CARLOS BICCA RANGEL, Vigilante, com
exercício no Setor de Vigilância Campus Centro, Saúde e Olímpico da Coordenadoria de Segurança da UFRGS.
Processo nº 23078.508437/2019-15.
 
Onde se lê:
"para o período de 20/02/2019 a 12/04/2019",
leia-se:
"para o período de 20/02/2019 a 31/05/2019".
Em 28/05/2019.
BIANCA SPODE BELTRAME
Divisão de Análise e Orientação do Desenvolvimento na Carreira
De acordo. Ao requerente, para ciência e conclusão do processo.
Em 28/05/2019.
KAREN WERLANG LUNKES
Diretora da Divisão de Análise e Orientação do Desenvolvimento na Carreira
