Documento gerado sob autenticação Nº INN.782.606.IC2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9539                  de  29/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas  atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, EDUARDO CARDOSO, matrícula SIAPE n° 2620161, lotado no Departamento de
Design e Expressão Gráfica da Faculdade de Arquitetura, para exercer a função de Coordenador da Ponto
UFRGS vinculado ao Departamento Administrativo e de Registro da Extensão (DARE) da Pró-Reitoria de
Extensão (PROREXT), Código SRH 1515, código FG-1, com vigência a partir da data de publicação no Diário
Oficial da União. Processo nº 23078.024651/2016-62.
RUI VICENTE OPPERMANN
Reitor
