Documento gerado sob autenticação Nº ZFR.948.538.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6018                  de  08/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de Luciana Pazini Papi, Professor do Magistério Superior, lotada e
em exercício no Departamento de Ciências Administrativas da Escola de Administração, com a finalidade de
participar do "IX Congreso Internacional Gobierno, Administración y Políticas Públicas", em Madri, Espanha,
no período compreendido entre 23/09/2018 e 27/09/2018, incluído trânsito, com ônus UFRGS (Pró-Reitoria
de Pesquisa - diárias). Solicitação nº 47106.
RUI VICENTE OPPERMANN
Reitor
