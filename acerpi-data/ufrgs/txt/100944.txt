Documento gerado sob autenticação Nº TEW.968.496.IU9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/4
PORTARIA Nº             10329                  de  14/11/2019
O PRÓ-REITOR DE GRADUAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de
suas atribuições, considerando o disposto na Portaria nº 7626, de 29 de setembro de 2016
               Resolve:
 
Publicar as Datas Acadêmicas para o primeiro e segundo semestres de 2020.
Até 29/11/2019 às
23h59min
SOLICITAÇÃO DE VAGAS AOS DEPARTAMENTOS: Período para as Comissões de Graduação enviarem seus
pedidos de vagas aos Departamentos com vistas a 2020/1.
Até 29/11/2019 às
23h59min
DEFINIÇÃO DOS PLANOS DE ENSINO: Período para os docentes definirem as alterações ou incluírem novos
Planos de Ensino com vistas a 2020/1.
Até 13/12/2019 às
23h59min
HOMOLOGAÇÃO DOS PLANOS DE ENSINO: Período para os Departamentos e as Comissões de Graduação
homologarem as alterações ou novos Planos de Ensino com vistas a 2020/1.
05/11/2019 a
03/01/2020
TROCA DE CURRÍCULO ou HABILITAÇÃO e FORMAÇÃO DIVERSIFICADA COMPLEMENTAR (FDC): Data limite
para solicitações de Troca de Currículo ou Habilitação com vistas à 2020/1. (Portaria º 9366/2018).
14/11/2019 às 8h a
03/01/2020 às
23h59min
OFERECIMENTO DAS ATIVIDADES DE ENSINO: Período para Oferecimento de Disciplinas/Turmas para as
Atividades de 2020/1.
14/11/2019 às 8h a
03/01/2020 às
23h59min
SOLICITAÇÃO DE ESPAÇO FÍSICO para as Atividades de Ensino oferecidas em 2020/1 no Módulo Turmas.
03/12/2019 às 8h a
03/01/2020 às
23h59min
SOLICITAÇÃO DE VAGAS DE MONITORIA: período para as COMGRADs e Departamentos encaminharem as
suas solicitações de vaga para monitoria para 2020/1.
15/12/2019 a
14/01/2020
PERMANÊNCIA: período para solicitações de Permanência nos Cursos que possuem este instituto conforme a
Resolução 11/2013 do CEPE, com vistas à 2020/1. (Portaria nº 9366/2018).
13/01/2020 a
05/02/2020 Período aconselhado para realização de PLES.
15/01/2020 às 12h
ANÁLISE DO PEDIDO DE PERMANÊNCIA, TROCA DE CURRÍCULO, HABILITAÇÃO E FORMAÇÃO
DIVERSIFICADA COMPLEMENTAR (FDC): Prazo limite para COMGRAD analisar os pedidos de permanência,
troca de currículo, habilitação e FDC. (Portaria n° 9366/2018).
10/01/2020 às 23h59 ESPAÇO FÍSICO: data limite para os gerentes de Espaço Físico atenderem ou negarem os pedidos dealocação de espaço físico.
13/01/2020 às 8h a
17/01/2020 às
23h59min
AJUSTE DO OFERECIMENTO: Período para as correções de oferecimento que se fizerem necessárias para
2020/1, incluindo Espaço Físico.
15/01/2020 às 8h a
05/02/2020 às
23h59min
CADASTRO DAS FAIXAS-HORÁRIAS PARA CALOUROS 2020/1: Período para as Comgrads cadastrarem as
faixas-horárias para matrícula de calouros 2020/1.
20/01/2020 às 8h a
22/01/2020 às 18h 
RESERVA DE VAGAS PARA CALOUROS: Período para a COMGRAD reservar, no Portal da COMGRAD, as vagas
destinadas aos calouros.
03/02/2020 a partir das
4h a 06/02/2020 às 12h
ENCOMENDA DE MATRÍCULA PARA ALUNOS VETERANOS: Período para os alunos veteranos, aptos à
matrícula de 2020/1, solicitarem disciplinas, via Portal do Aluno. (Portaria nº 9889/2019).
Documento gerado sob autenticação Nº TEW.968.496.IU9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/4
07/02/2020 às 14h a
11/02/2020 às 14h
AMPLIAÇÃO DE VAGAS: Período para os Departamentos ampliarem vagas, cancelarem ou incluírem novas
disciplinas/turmas para as demandas solicitadas na Encomenda de Matrícula.
14/02/2020 às 4h DIVULGAÇÃO DO RESULTADO DA ENCOMENDA DE MATRÍCULA: Divulgação dos resultados da matrícula dealunos veteranos no Portal do Aluno. (Portaria nº 9889/2019).
14/02/2020 a partir das
12h até 17/02/2020 às
14h
AJUSTE DE MATRÍCULA PARA ALUNOS VETERANOS: Período para alunos veteranos solicitarem, via Portal do
Aluno, inclusão de matrícula de novas disciplinas/turmas, substituições, exclusão de matrícula em disciplinas,
sem prejuízo no ordenamento de matrícula, ou  outros ajustes no resultado da Encomenda para 2020/1.
(Portaria nº 9889/2019).
19/02/2020 a partir das
14h até 21/02/2020 às
18h.
AMPLIAÇÃO DE VAGAS: Período para os Departamentos ampliarem vagas, cancelarem ou incluírem novas
disciplinas/turmas para as demandas solicitadas no Ajuste.
Até 28/02/2020
DIVULGAÇÃO DE FAIXA HORÁRIA DA MATRÍCULA DOS CALOUROS DE 2020/1: Divulgação no site da UFRGS,
www.ufrgs.br, da faixa horária da matrícula presencial dos calouros, classificados no Concurso Vestibular CV
2020 e SISU/2020, para ingresso no 1º período letivo de 2020. (Portaria nº 9889/2019)
29/02/2020 às 4h DIVULGAÇÃO DO RESULTADO FINAL DA MATRÍCULA DOS ALUNOS VETERANOS: Divulgação dos resultadosfinais da matrícula de alunos veteranos no Portal do Aluno. (Portaria n° 9889/2019).
02/03/2020 a
04/03/2020
MATRÍCULA PRESENCIAL DOS CALOUROS 2020/1: Período para realização da matrícula dos candidatos
classificados do Concurso Vestibular e SiSU 2020 (Portaria nº 9889/2019), Ingresso Especial Estudante Indígena
e estudante Programa Estudante Convênio - Graduação (PEC-G) com ingresso no 1º Período Letivo de 2020.
05/03/2020 INÍCIO DAS AULAS do 1º Período Letivo de 2020. (Portaria n° 9889/2019).
05/03/2020 INÍCIO DAS ATIVIDADES DE MONITORIA em 2020/1
De 05/03/2020 às 4h a
11/03/2020 às
23h59min
EXCLUSÃO DE MATRÍCULA: Período para alunos efetuarem, via Portal do Aluno, Exclusão de Matrícula em
atividades de ensino de 2020/1, sem prejuízo no Ordenamento de Matrícula. (Portaria nº 9889/2019).
06/03/2020 a
11/03/2020
LIBERAÇÃO DAS VAGAS RESERVADAS PARA CALOUROS não utilizadas na matrícula presencial. Data limite
para as Comissões de Graduação liberarem as vagas não utilizadas na matrícula presencial de calouros para
serem utilizadas na matrícula de inclusão.
De 12/03/2020 às 4h a
13/03/2020 às
23h59min
MATRÍCULA DE INCLUSÃO: período para matrícula em vagas remanescentes, via Portal do Aluno. (Portaria nº
9889/2019).
16/03/2020 até às
23h59min DIVULGAÇÃO DO RESULTADO DA MATRÍCULA DE INCLUSÃO no Portal do Aluno. (Portaria nº 9889/2019).
23/03/2020 a
15/06/2020
SOLICITAÇÃO DE COLAÇÃO DE GRAU: período para os alunos formandos em 2020/1 solicitarem colação de
grau pelo Portal do Aluno.
23 a 25/03/2020 ALUNO ESPECIAL: Período para requerer matrícula como aluno especial, com vistas ao 1º Período Letivo de2020, através do site www.prograd.ufrgs.br
20/03/2020 ALTERAÇÃO CURRICULAR: Data limite para recebimento, pela Câmara de Graduação, de processos advindosdas COMGRADs para 2020/2.
13/04/2020 às 8h até
15/05/2020 às
23h59min
SOLICITAÇÃO DE VAGAS AOS DEPARTAMENTOS: Período para as COMGRADs enviarem seus pedidos de
vagas aos Departamentos com vistas a 2020/2.
13/04/2020 às 8h até
29/05/2020 às
23h59min
DEFINIÇÃO DOS PLANOS DE ENSINO: Período para os docentes definirem as alterações ou incluírem novos
Planos de Ensino com vistas a 2020/2.
13/04/2020 às 8h até
19/06/2020 às
23h59min
HOMOLOGAÇÃO DOS PLANOS DE ENSINO: Período para os Departamentos e as COMGRADs homologarem
as alterações ou novos Planos de Ensino com vistas a 2020/2.
13/04/2020 às 8h até
26/06/2020 às
23h59min
OFERECIMENTO DAS ATIVIDADES DE ENSINO: Período para Oferecimento de Disciplinas/Turmas para as
Atividades de 2020/2.
13/04/2020 às 8h até
26/06/2020 às
23h59min
SOLICITAÇÃO DE ESPAÇO FÍSICO para as Atividades de Ensino oferecidas em 2020/2 no Módulo Turmas.
01/06/2020 às 8h a
26/06/2020 às
23h59min
SOLICITAÇÃO DE VAGAS DE MONITORIA: período para as COMGRADs e Departamentos encaminharem as
suas solicitações de vaga para monitoria para 2020/2.
01/06/2020 às 8h a
07/07/2020 às 12h
TROCA DE CURRÍCULO ou HABILITAÇÃO e FORMAÇÃO DIVERSIFICADA COMPLEMENTAR (FDC): Período
para solicitações dos alunos de Troca de Currículo ou Habilitação e FDC com vistas a 2020/2.
01/06/2020
TRANCAMENTO DE MATRÍCULA: Data limite para efetuar Trancamento de Matrícula do semestre 2020/1,
tanto para alunos regularmente matriculados quanto para alunos sem matrícula em 2020/1, via Portal do
Aluno. (Portaria nº 9889/2019).
12/06/2020 a
27/07/2020
AVALIAÇÃO DOCENTE 2020/1: Período para Avaliação Docente pelos Discentes, através do Portal do aluno.
(Portaria nº 9889/2019).
22/06/2020 às 8h a
10/07/2020 às
23h59min
CADASTRO DAS FAIXAS-HORÁRIAS PARA CALOUROS 2020/2: Período para as Comgrads cadastrarem as
faixas-horárias para matrícula de calouros 2020/2.
29/06/2020 a
17/07/2020 às 12h
PERMANÊNCIA: Período para solicitações pelos alunos de Permanência no Curso, diretamente no Portal, com
vistas a 2020/2.
Documento gerado sob autenticação Nº TEW.968.496.IU9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
3/4
03/07/2020 às 8h a
08/07/2020 às
23h59min
AJUSTE DO OFERECIMENTO: Período para as correções de oferecimento que se fizerem necessárias para
2020/2.
13/07/2020 às 8h a
15/07/2020 às
23h59min
RESERVA DE VAGAS PARA CALOUROS: Período para a COMGRAD reservar, no Portal da COMGRAD, as vagas
destinadas aos calouros.
15/07/2020 às 23h59 ESPAÇO FÍSICO: data limite para os gerentes de Espaço Físico atenderem ou negarem os pedidos dealocação de espaço físico.
15/07/2020 CANCELAMENTO JUSTIFICADO DE MATRÍCULA: Data limite para efetuar o Cancelamento justificado deMatrícula das disciplinas do Período Letivo 2020/1. (Portaria nº 9889/2019).
15/07/2020 TÉRMINO DAS AULAS do 1º Período Letivo de 2020. (Portaria nº 9889/2019).
16/07/2020 APROPRIAÇÃO DOS CONCEITOS: Data limite para apropriação dos conceitos das atividades de ensino, juntoao Portal, pelos Professores. (Portaria nº 9889/2019).
17/07/2020 Divulgação dos CONCEITOS FINAIS do 1º Período Letivo de 2020. (Portaria nº 9889/2019).
17/07/2020 às
23h59min
ANÁLISE DO PEDIDO DE PERMANÊNCIA, TROCA DE CURRÍCULO, HABILITAÇÃO E FDC: Prazo limite para
COMGRAD analisar os pedidos de permanência.
Até 17/07/2020 AFASTAMENTO POR DIPLOMAÇÃO: Período aconselhável para as COMGRADs efetuarem o afastamento pordiplomação dos alunos aptos à colação de grau em 2020/1.
21/07/2020 a partir das
4h a 23/07/2020 às
23h59min
ENCOMENDA DE MATRÍCULA PARA ALUNOS VETERANOS: Período para alunos veteranos, aptos à matrícula
de 2020/2, solicitarem disciplinas, via Portal do Aluno. (Portaria nº 9889/2019).
22/07/2020 REVISÃO DE CONCEITOS: Data limite para solicitações de revisão de conceitos 2020/1, junto aosDepartamentos. (Portaria nº 9889/2019).
27/07/2020 a partir das
12h a 29/07/2020 até às
14h
AMPLIAÇÃO DE VAGAS: Período para os Departamentos ampliarem vagas, cancelarem ou incluírem novas
disciplinas/turmas para as demandas solicitadas na Encomenda de Matrícula.
30/07/2020 às 4h DIVULGAÇÃO DO RESULTADO DA ENCOMENDA DE MATRÍCULA: Divulgação dos resultados da matrícula dealunos veteranos no Portal do Aluno. (Portaria nº 9889/2019).
30/07/2020 das 8h às
23h59min
AJUSTE DE MATRÍCULA PARA ALUNOS VETERANOS: Período para alunos veteranos solicitarem, via Portal
do Aluno, inclusão de matrícula de novas disciplinas/turmas, substituições, exclusão de matrícula em
disciplinas, sem prejuízo no ordenamento de matrícula, ou  outros ajustes no resultado da Encomenda
para 2020/2. (Portaria nº 9889/2019).
De 31/07/2020 a
25/09/2020 Período destinado às COLAÇÕES DE GRAU do 1º Período Letivo de 2020. (Portaria nº 9889/2019).
Até 31/07/2020
DIVULGAÇÃO DE FAIXA HORÁRIA da Matrícula dos Calouros de 2020/2: Divulgação no site da UFRGS,
www.ufrgs.br, da faixa horária da matrícula presencial dos calouros classificados no Concurso Vestibular e no
SiSU 2020, para ingresso em 2020/2. (Portaria nº 9889/2019).
31/07/2020 a partir das
10h até 03/08/2020 às
23h59min
AMPLIAÇÃO DE VAGAS: Período para os Departamentos e COMGRADs que ofertam disciplinas ampliarem
vagas, cancelarem ou incluírem novas disciplinas/turmas para as demandas solicitadas no Ajuste.
03 a 05/08/2020
MATRÍCULA PRESENCIAL DOS CALOUROS 2020/2: Período para realização da matrícula dos candidatos
classificados do Concurso Vestibular e SiSU 2020 (Portaria nº 9889/2019), Ingresso Especial Estudante Indígena
e estudante Programa Estudante Convênio - Graduação (PEC-G) com ingresso no 2º Período Letivo de 2020.
05/08/2020 às 4h DIVULGAÇÃO DO RESULTADO FINAL DA MATRÍCULA DOS ALUNOS VETERANOS: Divulgação dosresultados finais da matrícula de alunos veteranos no Portal do Aluno. (Portaria n° 9889/2019).
06/08/2020 INÍCIO DAS AULAS do 2º Período Letivo de 2020. (Portaria n° 9889/2019).
06/08/2020 INÍCIO DAS ATIVIDADES DE MONITORIA em 2020/2.
De 06/08/2020 às 4h a
12/08/2020 às
23h59min
EXCLUSÃO DE MATRÍCULA: Período para alunos efetuarem, via Portal do Aluno, Exclusão de Matrícula
em atividades de ensino de 2020/2, sem prejuízo no Ordenamento de Matrícula. (Portaria nº 9889/2019).
06/08/2020 a
12/08/2020
LIBERAÇÃO DAS VAGAS RESERVADAS PARA CALOUROS não utilizadas na matrícula presencial. Data
limite para as Comissões de Graduação liberarem as vagas não utilizadas na matrícula presencial de
calouros para serem utilizadas na matrícula de inclusão.
De 13/08/2020 às 4h a
14/08/2020 às
23h59min
MATRÍCULA DE INCLUSÃO: período para matrícula em vagas remanescentes, via Portal do Aluno.
(Portaria nº 9889/2019).
17/08/2020 até às
23h59min DIVULGAÇÃO DO RESULTADO DA MATRÍCULA DE INCLUSÃO no Portal do Aluno. (Portaria nº 9889/2019).
24/08/2020 a
20/11/2020
SOLICITAÇÃO DE COLAÇÃO DE GRAU: período para os alunos formandos em 2020/2 solicitarem colação de
grau pelo Portal do Aluno.
24/08/2020 a
26/08/2020
ALUNO ESPECIAL: Período para requerer matrícula como aluno especial, com vistas a 2020/2, através do site
www.prograd.ufrgs.br
07/08/2020 ALTERAÇÃO CURRICULAR: Data limite para recebimento, pela Câmara de Graduação, de processos advindosdas Comissões de Graduação para 2021/1.
14 a 18/09/2020 SALÃO UFRGS/ SEMANA ACADÊMICA (Portaria nº 9889/2019)
Documento gerado sob autenticação Nº TEW.968.496.IU9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
4/4
05/11/2020 a
15/12/2020
TROCA DE CURRÍCULO ou HABILITAÇÃO e FDC: Data limite para solicitações de Troca de Currículo ou
Habilitação e FDC com vistas a 2021/1.
08/11/2020
TRANCAMENTO DE MATRÍCULA: Data limite para efetuar Trancamento de Matrícula do semestre 2020/2,
tanto para alunos regularmente matriculados quanto para alunos sem matrícula em 2020/2, via Portal do
Aluno. (Portaria nº 9889/2019).
20/11/2020 a
11/01/2021
AVALIAÇÃO DOCENTE 2020/2: Período para Avaliação Docente pelos Discentes, através do Portal do Aluno
(Portaria nº 9889/2019).
14/12/2020 a
12/01/2021 PERMANÊNCIA: período para solicitações de Permanência no Curso.
23/12/2020 CANCELAMENTO JUSTIFICADO DE MATRÍCULA: Data limite para efetuar o Cancelamento Justificado deMatrícula das disciplinas do período 2019/2. (Portaria nº 9889/2019).
23/12/2020 TÉRMINO DAS AULAS do 2º Período Letivo de 2020. (Portaria nº 9889/2019).
Até 28/12/2020 APROPRIAÇÃO DOS CONCEITOS: Data limite para apropriação dos conceitos das atividades de ensino, juntoao Portal, pelos Professores. (Portaria nº 9889/2019).
29/12/2020 Divulgação dos CONCEITOS FINAIS do 2º Período Letivo de 2020. (Portaria nº 9889/2019).
05/01/2021 REVISÃO DE CONCEITOS: Data limite para solicitações de revisão de conceitos 2020/2, junto aosDepartamentos. (Portaria nº 9889/2019).
Até 08/01/2021 AFASTAMENTO POR DIPLOMAÇÃO: Período aconselhável para as COMGRADs efetuarem o afastamento pordiplomação dos alunos aptos à colação de grau em 2020/2.
13/01/2021 às 12h ANÁLISE DO PEDIDO DE PERMANÊNCIA, TROCA DE CURRÍCULO, HABILITAÇÃO E FDC: Prazo limite paraCOMGRAD analisar os pedidos de permanência.
18/01/2021 a
29/03/2021 Período destinado às COLAÇÕES DE GRAU do 2º Período Letivo de 2020. (Portaria nº 9889/2019).
OBSERVAÇÃO:
As datas específicas referentes às monitorias acadêmicas serão divulgadas em calendário
próprio.
 
VLADIMIR PINHEIRO DO NASCIMENTO
Pró-Reitor de Graduação
