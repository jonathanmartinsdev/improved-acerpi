Documento gerado sob autenticação Nº TVE.759.936.0O5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9544                  de  27/11/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Autorizar o afastamento no País de LEANDRO CESAR DE GODOY, ocupante do cargo de Professor
do Magistério Superior,  lotado e em exercício no Departamento de Zootecnia da Faculdade de Agronomia, 
com a finalidade de ministrar disciplina junto à Universidade Nilton Lins, em Manaus, Brasil, no período
compreendido entre 30/11/2018 e 22/12/2018, incluído trânsito, com ônus limitado. Solicitação n° 61445.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
