Documento gerado sob autenticação Nº PJQ.493.440.86F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             998                  de  30/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, ANTONIO BARROS DE BRITO JUNIOR, matrícula SIAPE n° 1813768, lotado no
Departamento de Linguística, Filologia e Teoria Literária, como Chefe Substituto do Depto de Lingüística,
Filologia e Teoria Literária do Instituto de Letras, para substituir automaticamente o titular desta função em
seus  afastamentos  ou  impedimentos  regulamentares,  com  vigência  a  partir  da  data  deste  ato  e  até
20/12/2018. Processo nº 23078.204382/2016-16.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
