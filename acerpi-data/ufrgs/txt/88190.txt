Documento gerado sob autenticação Nº USC.464.395.UNU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2327                  de  14/03/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  LUCIANA  LAUREANO  PAIVA,  matrícula  SIAPE  n°  1844915,  lotada  e  em  exercício  no
Departamento de Educação Física, Fisioterapia e Dança da Escola de Educação Física, Fisioterapia e Dança, da
classe C  de Professor Adjunto, nível 04, para a classe D  de Professor Associado, nível 01, referente ao
interstício de 10/02/2017 a 09/02/2019, com vigência financeira a partir de 10/02/2019, de acordo com o que
dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN.
Processo nº 23078.501075/2019-23.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
