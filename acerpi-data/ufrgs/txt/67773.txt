Documento gerado sob autenticação Nº MOF.949.817.6FE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6019                  de  08/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LUDYMILA SCHULZ BARROSO, Nutricionista-habilitação, lotada
na Pró-Reitoria de Assuntos Estudantis e com exercício na Divisão de Alimentação, com a finalidade de
participar  do  "Congreso  Internacional  Sociología  de  la  Alimentación",  em  Gijón,  Espanha,  no  período
compreendido entre 25/09/2018 e 29/09/2018, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Gestão de
Pessoas - diárias e passagens). Solicitação nº 34760.
RUI VICENTE OPPERMANN
Reitor
