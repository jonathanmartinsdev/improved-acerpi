Documento gerado sob autenticação Nº QTD.490.643.FBT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8648                  de  26/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  pensão  com  natureza  vitalícia,  a  partir  de  21  de  março  de  2018,  a  SOILA  MARIA
CALDEIRA XAVIER,  nos termos dos artigos 215 e 217, inciso III da Lei n.º 8.112, de 11 de dezembro de
1990, combinado com o artigo 40 da Constituição Federal de 1988, alterado pela Emenda Constitucional 41
de 2003, regulamentado pela Lei nº 10.887/2004, artigo 2º, I,  em decorrência do falecimento de DARCY
MADRUGA,  matrícula SIAPE n° 0351889, aposentado no cargo de Porteiro do quadro de pessoal desta
Universidade. Processo n.º 23078.507649/2018-96.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
