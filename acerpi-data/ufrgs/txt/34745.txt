Documento gerado sob autenticação Nº ZGN.228.803.ACG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1765                  de  22/02/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SANDRA MARIA LUCIA PEREIRA GONÇALVES, Professor do
Magistério  Superior,  lotada  e  em  exercício  no  Departamento  de  Comunicação  da  Faculdade  de
Biblioteconomia e Comunicação, com a finalidade de participar do "VIII Congresso Internacional CSO'2017 -
Criadores  Sobre  outras  Obras",  em  Lisboa,  Portugal,  no  período  compreendido  entre  05/04/2017  e
14/04/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 26279.
RUI VICENTE OPPERMANN
Reitor
