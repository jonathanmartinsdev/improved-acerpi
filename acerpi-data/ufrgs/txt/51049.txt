Documento gerado sob autenticação Nº SYW.400.281.NRS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1467                  de  19/02/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°42014,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, MARIANE MOREIRA DA SILVA (Siape: 2260285
),  para substituir   JORGE UBIRAJARA BRANDO NOGUEIRA (Siape: 2057112 ), Coordenador do Núcleo de
Financas, Infraest, Patrim e Supriment Gerência Adm do Instituto de Letras, Código FG-7, em seu afastamento
por motivo de férias, no período de 19/02/2018 a 02/03/2018, com o decorrente pagamento das vantagens
por 12 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
