Documento gerado sob autenticação Nº XCV.807.118.NTM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3460                  de  11/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/05/2018,   referente  ao  interstício  de
06/03/2005 a 10/05/2018, para o servidor JOAO FRANCISCO DORNELES FONTOURA, ocupante do cargo de
Pedreiro - 701649, matrícula SIAPE 1111695,  lotado  na  Superintendência de Infraestrutura, passando do
Nível de Classificação/Nível de Capacitação B I, para o Nível de Classificação/Nível de Capacitação B II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.511130/2018-11:
CENED - Segurança do Trabalho, Higiene e Ergonomia CH: 100 Carga horária utilizada: 40 hora(s) / Carga
horária excedente: 60 hora(s) (05/01/2016 a 05/02/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
