Documento gerado sob autenticação Nº KHG.224.882.NAA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3870                  de  25/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  EROS MOREIRA DE CARVALHO,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Filosofia do Instituto de Filosofia e Ciências Humanas,
com a finalidade de participar da "Reconciving Cognition Confeerence", em Antuérpia, Bélgica, no período
compreendido entre 25/06/2018 e 30/06/2018, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa -
diárias). Solicitação nº 45547.
RUI VICENTE OPPERMANN
Reitor
