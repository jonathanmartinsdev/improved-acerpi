Documento gerado sob autenticação Nº VRJ.564.493.7EG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1590                  de  14/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  à  servidora  ANDREA  HELENA
RODRIGUES COLOMBO, ocupante do cargo de Assistente em Administração-701200, lotada no Instituto de
Informática, SIAPE 2270622, para 30% (trinta por cento), a contar de 10/12/2018, tendo em vista a conclusão
do  curso  de  MBA  em  Administração  Pública  e  Gerência  de  Cidades,  conforme  o  Processo  nº
23078.534105/2018-05.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
