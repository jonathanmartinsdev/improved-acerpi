Documento gerado sob autenticação Nº SWA.778.700.GR4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4038                  de  09/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta  Universidade,  CLAUDIA  LIMA  MARQUES,  matrícula  SIAPE  n°  0357139,  lotada  no
Departamento de Direito Público e Filosofia do Direito da Faculdade de Direito, para exercer a função de
Coordenadora do PPG em Direito, Código SRH 1154, código FUC, com vigência a partir da data de publicação
no Diário Oficial da União, pelo período de 02 anos, por ter sido reeleita. Processo nº 23078.000180/2017-88.
JANE FRAGA TUTIKIAN
Vice-Reitora.
