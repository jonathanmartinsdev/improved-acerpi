Documento gerado sob autenticação Nº JCW.395.456.2GM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8990                  de  04/10/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, , e conforme a Solicitação de Férias n°51449,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, TATIANA DA SILVA DUARTE (Siape: 2201492 ),  para
substituir   ROBERTO LUIS WEILER (Siape: 2254351 ), Coordenador da Comissão de Extensão da Faculdade de
Agronomia, Código XX, em seu afastamento por motivo de férias, no período de 07/10/2019 a 11/10/2019.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
