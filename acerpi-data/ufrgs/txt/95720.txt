Documento gerado sob autenticação Nº LDY.583.633.G3F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7027                  de  05/08/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  MARIA  DE  FATIMA  APARECIDA  SARAIVA  BITENCOURT,
Professor do Magistério Superior,  lotada e em exercício no Departamento de Geologia do Instituto de
Geociências, com a finalidade de realizar visita à University of Tromso - The Arctic University of Norway, em
Tromso, Noruega, no período compreendido entre 23/08/2019 e 11/09/2019, incluído trânsito, com ônus
limitado. Solicitação nº 85872.
RUI VICENTE OPPERMANN
Reitor
