Documento gerado sob autenticação Nº YMT.013.047.965, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10110                  de  13/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar  ALESSANDRA  LUCIA  BOCHIO,  matrícula  SIAPE  n°  2382600,  ocupante  do  cargo  de
Professor do Magistério Superior, classe Adjunto A, lotada no Departamento de Artes Visuais do Instituto de
Artes, do Quadro de Pessoal da Universidade, para exercer a função de Chefe Substituta do Depto de Artes
Visuais do Instituto de Artes, com vigência a partir de 18/12/2018 até 12/01/2020, a fim de completar o
mandato  da  professora  MARINA  BORTOLUZ  POLIDORO,  conforme  artigo  92  do  Estatuto  da  mesma
Universidade. Processo nº 23078.534334/2018-11.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
