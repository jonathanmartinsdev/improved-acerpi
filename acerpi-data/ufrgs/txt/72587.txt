Documento gerado sob autenticação Nº LYY.457.927.96D, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9523                  de  23/11/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  JAQUELINE  MALLMANN  HAAS,  matrícula  SIAPE  n°  1970425,  lotada  no
Departamento  Interdisciplinar  do  Campus  Litoral  Norte,  como  Coordenadora  Substituta  do  PPG  em
Dinâmicas Regionais e Desenvolvimento do Campus Litoral Norte, com vigência a partir de 02/12/2018 até
01/12/2020. Processo nº 23078.532170/2018-98.
RUI VICENTE OPPERMANN
Reitor.
