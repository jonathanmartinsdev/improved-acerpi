Documento gerado sob autenticação Nº NDC.329.943.017, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8059                  de  04/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta Universidade,  VIVIAN FISCHER,  matrícula SIAPE n° 2211297,  lotada no Departamento de
Zootecnia da Faculdade de Agronomia, para exercer a função de Chefe do Depto de Zootecnia da Faculdade
de Agronomia, Código SRH 200, código FG-1, com vigência a partir de 25/09/2019 até 24/09/2021. Processo nº
23078.523754/2019-53.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
