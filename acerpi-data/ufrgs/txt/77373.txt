Documento gerado sob autenticação Nº YJK.966.139.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1925                  de  26/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5075839-07.2018.4.04.7100,  da  1ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, do servidor PEDRO BISSIGO, matrícula SIAPE n° 0358190, ativo no cargo de Eletricista - 701427,
para o nível III, conforme o Processo nº 23078.503924/2019-83.
 
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
 
Conceder Progressão por Capacitação, passando do Nível de Classificação/Nível de Capacitação C III,
para o Nível de Classificação/Nível de Capacitação C IV, a contar de 28/01/2009.
RUI VICENTE OPPERMANN
Reitor
