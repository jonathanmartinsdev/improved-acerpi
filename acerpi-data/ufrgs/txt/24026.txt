Documento gerado sob autenticação Nº JIP.557.155.D4E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4803                  de  30/06/2016
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de
2013
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  21/06/2016,   referente  ao  interstício  de
05/05/2014  a  20/06/2016,  para  a  servidora  LUCIA  MARIA  PUGGINA  MORAES,  ocupante  do  cargo  de
Assistente em Administração - 701200, matrícula SIAPE 0358771,  lotada  no  Instituto de Artes, passando
do Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.013553/2016-08:
ENAP - Ética e Serviço Público CH: 20 (31/05/2016 a 20/06/2016)
ENAP  -  Gestão  da  Informação  e  Documentação  -  Conceitos  básicos  em  Gestão  Documental  CH:  20
(03/05/2016 a 23/05/2016)
UNINTER - Técnicas de Negociação CH: 40 (01/07/2015 a 31/08/2015)
Cursos Online SP - Programação Neurolinguística CH: 40 (03/04/2016 a 17/04/2016)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
