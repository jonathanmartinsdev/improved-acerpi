Documento gerado sob autenticação Nº CVN.057.372.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3569                  de  26/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, à
Professora DANIELE CARON, matrícula SIAPE 1018101, lotada na Faculdade de Arquitetura e com exercício
no Departamento de Design e Expressão Gráfica, da classe A  de Professor Assistente A, nível 01, para a
classe C  de Professor Adjunto, nível 01, com vigência financeira a partir de 23/04/2019, de acordo com o que
dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554, de 20 de junho de
2013 do Ministério da Educação e a Decisão nº 331/2017. Processo nº 23078.507185/2019-07.
RUI VICENTE OPPERMANN
Reitor.
