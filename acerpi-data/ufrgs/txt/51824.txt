Documento gerado sob autenticação Nº OLX.549.598.G7L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1988                  de  15/03/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CARLOS OTAVIO CORSO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Cirurgia da Faculdade de Medicina, com a finalidade de participar
do "Coloquio Medicina Traslacional: problemas emergentes de salud en América Latina", em Pucón,
Chile,  no  período compreendido  entre  22/03/2018  e  25/03/2018,  incluído  trânsito,  com ônus  limitado.
Solicitação nº 34206.
RUI VICENTE OPPERMANN
Reitor
