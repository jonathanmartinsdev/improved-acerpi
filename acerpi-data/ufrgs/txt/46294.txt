Documento gerado sob autenticação Nº IEY.818.789.17E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9835                  de  24/10/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de GONCALO NUNO CORTE REAL FERRAZ DE OLIVEIRA, Professor
do Magistério Superior, lotado e em exercício no Departamento de Ecologia do Instituto de Biociências, com
a finalidade de ministrar disciplina no "VI Curso Internacional de Anillamiento de Aves y Análisis de Muda", no
SELVA:  Investigación  para  la  Conservación  en  el  Neotrópico,  em  Riohacha,  Colômbia,  no  período
compreendido entre 13/12/2017 e 22/12/2017, incluído trânsito, com ônus UFRGS (diárias e passagens).
Solicitação nº 31855.
RUI VICENTE OPPERMANN
Reitor
