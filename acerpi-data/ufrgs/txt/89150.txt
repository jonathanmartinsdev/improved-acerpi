Documento gerado sob autenticação Nº OTD.277.898.M6R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2853                  de  01/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a WALTER
MEUCCI  NIQUE,  matrícula  SIAPE nº  0355827,  no  cargo de  Professor  Titular  da  Carreira  do Magistério
Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no Departamento
de Ciências Administrativas da Escola de Administração, com proventos integrais e incorporando a vantagem
pessoal de que trata a Lei nº 9.624, de 2 de abril de 1998, que assegurou o disposto no artigo 3º da Lei nº
8.911, de 11 de julho de 1994. Processo 23078.531539/2018-45.
RUI VICENTE OPPERMANN
Reitor.
