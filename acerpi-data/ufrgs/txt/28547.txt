Documento gerado sob autenticação Nº DDM.187.806.OD5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7788                  de  03/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a partir  da data de publicação no Diário Oficial  da União,  a ocupante do cargo de
Pedagogo-área - 701058, do Nível de Classificação EII, do Quadro de Pessoal desta Universidade, ANA LUCIA
BORGES ANDRADE,  matrícula  SIAPE 2144533 da função de Diretora da Divisão de Ensino,  Pesquisa  e
Extensão, da Asses da Diret Acad do Campus Litoral Norte, Código SRH 1415, Código FG-3, para a qual foi
designada pela Portaria nº 9465/14 de 09/12/2014, publicada no Diário Oficial da União de 16/12/2014.
Processo nº 23078.022222/2016-51.
JANE FRAGA TUTIKIAN
Vice-Reitora
