Documento gerado sob autenticação Nº XEV.579.125.H17, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             268                  de  05/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar insubsistente a Portaria nº 11533 de 27/12/2017 que nomeou IARA SOUZA PEREIRA (Siape:
0352730 ), para substituir ANDREIA DE ESPINDOLA LOPES (Siape: 2121748 ), Chefe do Setor de Patrimônio
Histórico da SUINFRA, Código FG-1, em seu afastamento por motivo de férias, no período de 02/01/2018 a
05/01/2018. Processo SEI nº 23078.524588/2017-41
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
