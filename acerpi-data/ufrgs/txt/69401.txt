Documento gerado sob autenticação Nº ZVV.936.008.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7139                  de  10/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de EDSON PRESTES E SILVA JUNIOR,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Informática Teórica do Instituto de Informática, com a
finalidade  de  realizar   visita   ao  Instituto  Politécnico  de  Castelo  Branco,  Lisboa,  Portugal,  no  período
compreendido entre 29/09/2018 e 09/10/2018, incluído trânsito, com ônus limitado. Solicitação nº 59097.
RUI VICENTE OPPERMANN
Reitor
