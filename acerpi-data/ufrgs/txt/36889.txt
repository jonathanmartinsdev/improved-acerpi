Documento gerado sob autenticação Nº TFL.232.581.5NP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3231                  de  13/04/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  a  ocupante  do  cargo  de  Professor  do  Magistério  Superior,  classe  Assistente,  desta
Universidade, IVANA DE SOUZA KARL, matrícula SIAPE n° 1251634, lotada no Departamento de Enfermagem
Materno-Infantil  da  Escola  de  Enfermagem,  para  exercer  a  função  de  Coordenadora  da  Comissão  de
Extensão da Escola de Enfermagem, Código SRH 720, com vigência a partir de 22/04/2017 e até 21/04/2019,
ficando o pagamento de gratificação condicionado à disponibilidade de uma função gratificada. Processo nº
23078.004064/2017-38.
JANE FRAGA TUTIKIAN
Vice-Reitora.
