Documento gerado sob autenticação Nº ABV.492.025.1M4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             606                  de  17/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar insubsistente a Portaria nº 7075 de 06/08/2019, que designou temporariamente FELIPE
AMORIM BERUTTI (Siape: 3028247), para substituir  DANIELA DIETZ VIANA (Siape: 2360646 ), Coordenador
da COMGRAD do Curso Bacharelado Interdisciplin em Ciências e Tec do Campus Litoral Norte, Código SRH
1461, FUCC, em sua Prorrogação de Licença Gestante,no período de 20 a 25 de janeiro de 2020. Processo SEI
Nº 23078.516018/2019-49
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
