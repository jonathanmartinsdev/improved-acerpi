Documento gerado sob autenticação Nº NAI.855.447.R5K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8733                  de  26/10/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Declarar que a aposentadoria concedida a ERIKA LORI MENNELLA, matrícula SIAPE nº 352254,
através da portaria nº 5485, de 5 de dezembro de 1996, e publicada no Diário Oficial da União do dia 10
subsequente, passa a ser com as vantagens deferidas pelo artigo 192, inciso II, da Lei nº 8112, de 11 de
dezembro de 1990, cumulativamente com a vantagem pessoal de que trata a Lei nº 9624, de 2 de abril de
1998, que assegurou o disposto na Lei nº 8911, de 11 de julho de 1994.
RUI VICENTE OPPERMANN
Reitor
