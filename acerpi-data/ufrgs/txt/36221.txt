Documento gerado sob autenticação Nº OFF.436.136.E5I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2756                  de  29/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor FERNANDO AUGUSTO MARINHO DE FRANCA GUALDA DANTAS, matrícula SIAPE n° 1260996,
lotado e em exercício no Departamento de Música do Instituto de Artes, da classe C  de Professor Adjunto,
nível 02, para a classe C  de Professor Adjunto, nível 03, referente ao interstício de 11/02/2013 a 10/02/2015,
com vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-
CONSUN. Processo nº 23078.508438/2016-17.
JANE FRAGA TUTIKIAN
Vice-Reitora.
