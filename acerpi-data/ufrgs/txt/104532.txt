Documento gerado sob autenticação Nº DZE.423.398.FRI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             994                  de  30/01/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°54295,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, BRUNO CASSEL NETO (Siape: 0358868 ),  para
substituir   RAFAEL ROESLER (Siape: 2190466 ), Pró-Reitor de Pesquisa, Código CD-2, em seu afastamento por
motivo de férias, no período de 31/01/2020 a 24/02/2020, com o decorrente pagamento das vantagens por
25 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
