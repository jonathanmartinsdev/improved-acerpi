Documento gerado sob autenticação Nº MLT.783.202.UE3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3807                  de  23/05/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Lotar no Campus Litoral Norte, com exercício no Divisão de Estrutura e Logística do Núcleo de Apoio
ao Ensino da Direção Administrativa do Campus Litoral Norte,  Marta Lucia de Freitas Vasco,  nomeada
conforme Portaria Nº 2.406 de 01 de abril de 2016, publicada no Diário Oficial da União no dia 05 de abril de
2016, em efetivo exercício desde 17 de maio de 2016, ocupante do cargo de Assistente em Administração,
Ambiente  Organizacional  Administrativo,  Classe  D,  nível  I,  padrão  I,  no  Quadro  de  Pessoal  desta
Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
