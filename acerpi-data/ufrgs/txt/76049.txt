Documento gerado sob autenticação Nº FFT.188.904.REG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1175                  de  01/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/01/2019,   referente  ao  interstício  de
26/07/2017 a 25/01/2019, para a servidora JÉSSICA DAIANE THOMÉ,  ocupante do cargo de Técnico de
Laboratório Área - 701244, matrícula SIAPE 2145123,  lotada  na  Faculdade de Farmácia, passando do Nível
de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.500037/2019-53:
Formação Integral de Servidores da UFRGS I CH: 37 (17/05/2017 a 17/12/2018)
SOF - ENAP - Ética e serviço Público CH: 20 (08/07/2018 a 29/07/2018)
BRADESCO - Língua Portuguesa sem Complicações CH: 20 Carga horária utilizada: 3 hora(s) / Carga horária
excedente: 17 hora(s) (11/12/2018 a 12/12/2018)
ENAP - Gestão Estratégica de Pessoas e Planos de Carreira CH: 20 (25/08/2018 a 15/09/2018)
ENAP  -  Gestão  da  Informação  e  Documentação  -  Conceitos  básicos  em  Gestão  Documental  CH:  20
(08/07/2018 a 29/07/2018)
ENAP - Educação em Direitos Humanos CH: 30 (08/07/2018 a 05/08/2018)
ENAP - Direitos Humanos: Uma Declaração Universal CH: 20 (08/07/2018 a 29/07/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
