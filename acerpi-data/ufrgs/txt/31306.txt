Documento gerado sob autenticação Nº WSG.301.059.D5H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9808                  de  12/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°26640,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TÉCNICO EM NUTRIÇÃO E
DIETÉTICA, do Quadro de Pessoal desta Universidade, KAMILA SANTOS LOPES (Siape: 2136284 ),   para
substituir   FABIANA HITOMI TANABE (Siape: 2111379 ), Chefe do Refeitório do CLN do Núcleo de Assistência
ao Aluno d Diretoria Adm do Campus Litoral Norte, em seu afastamento por motivo de férias, no período de
12/12/2016 a 31/12/2016.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
