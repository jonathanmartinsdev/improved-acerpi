Documento gerado sob autenticação Nº KJE.500.232.4O7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             519                  de  17/01/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  LUIZ  AUGUSTO  ESTRELLA  FARIA,  matrícula  SIAPE  n°  0359382,  lotado  no
Departamento  de  Economia  e  Relações  Internacionais  da  Faculdade  de  Ciências  Econômicas,  como
Coordenador Substituto do PPG em Estudos Estratégicos Internacionais, para substituir automaticamente o
titular desta função em seus afastamentos ou impedimentos regulamentares no período de 20/01/2017 e até
19/01/2019. Processo nº 23078.025918/2016-39.
JANE FRAGA TUTIKIAN
Vice-Reitora
