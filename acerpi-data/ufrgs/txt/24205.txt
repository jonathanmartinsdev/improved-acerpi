Documento gerado sob autenticação Nº RXR.683.065.16N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4931                  de  06/07/2016
  Designar  Presidente  do  Comitê  de
Segurança  da  Informação  da  UFRGS.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
tendo em vista o que consta no Ofício nº 001/2015-CSI
RESOLVE:
    Designar o servidor LEANDRO MÁRCIO BERTHOLDO como Presidente do Comitê de Segurança da
Informação da Universidade Federal do Rio Grande do Sul, instituído pela  Portaria nº 3.587 de 11 de maio de
2015.
CARLOS ALEXANDRE NETTO,
Reitor.
