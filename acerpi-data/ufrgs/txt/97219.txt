Documento gerado sob autenticação Nº PGM.759.258.I3Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7818                  de  28/08/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar o afastamento no País de THIAGO HENRIQUE BRAGATO BARROS, ocupante do cargo
de Professor do Magistério Superior, lotado e em exercício no Departamento de Ciência da Informação da
Faculdade de Biblioteconomia e Comunicação, com a finalidade de participar da "VI Reunião Brasileira de
Ensino e Pesquisa em Arquivologia (REPARQ)" e do "Congresso Brasileiro em organização do conhecimento
(ISKO-BRASIL)", em Belém, Brasil, no período compreendido entre 26/08/2019 e 13/09/2019, incluído trânsito,
com ônus para a Universidade Federal do Pará e CAPES. Solicitação n° 86151.
JANE FRAGA TUTIKIAN
Vice-Reitora.
