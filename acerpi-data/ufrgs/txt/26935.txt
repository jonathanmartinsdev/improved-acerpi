Documento gerado sob autenticação Nº MKO.085.027.8O5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6726                  de  31/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/08/2016,   referente  ao  interstício  de
26/02/2015 a 25/08/2016,  para o servidor ANDERSON GONÇALVES ASSUNÇÃO,  ocupante do cargo de
Auxiliar em Administração - 701405,  matrícula SIAPE 2056808,  lotado  na  Pró-Reitoria de Gestão de
Pessoas, passando do Nível de Classificação/Nível de Capacitação C II, para o Nível de Classificação/Nível de
Capacitação  C  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.018887/2016-60:
Formação Integral de Servidores da UFRGS IV CH: 119 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 29 hora(s) (30/09/2013 a 29/03/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
