Documento gerado sob autenticação Nº OVN.171.871.4EC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5920                  de  09/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  à  servidora  MICHELE  CARDOSO  SCHAFER,  ocupante  do  cargo  de   Assistente  em
Administração - 701200, lotada na Pró-Reitoria de Graduação, SIAPE 2326953, o percentual de 30% (trinta por
cento)  de  Incentivo  à  Qualificação,  a  contar  de  14/07/2016,  tendo  em vista  a  conclusão  do  curso  de
Especialização em Controladoria e Finanças, conforme o Processo nº 23078.014318/2016-45.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
