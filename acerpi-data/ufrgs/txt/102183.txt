Documento gerado sob autenticação Nº VAO.963.655.1J1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11088                  de  12/12/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal  desta  Universidade,  LUCIANE  MARIA  PILOTTO,  matrícula  SIAPE  n°  3061360,  lotada  no
Departamento de Odontologia Preventiva e Social, para exercer a função de Coordenadora Substituta da
Comissão de Extensão da Faculdade de Odontologia, com vigência a partir da data deste ato, pelo período de
2 anos, nos termos do art. 25 do Decreto nº 9.739/2019, em concordância com a norma do item 3.4 do
Manual de Estruturas Organizacionais do Poder Executivo Federal/SIORG, não recebendo remuneração de
qualquer natureza por essa função. Processo nº 23078.531091/2019-41.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
