Documento gerado sob autenticação Nº JYC.229.401.QMU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3199                  de  13/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  à  servidora  SANDRA  DE  BRITO
STEFANI, ocupante do cargo de Auxiliar de Enfermagem-701411, lotada na Faculdade de Odontologia, SIAPE
1743609, para 30% (trinta por cento),  a contar de 31/01/2017, tendo em vista a conclusão do curso de
Especialização em Saúde do Trabalhador, conforme o Processo nº 23078.001640/2017-95.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
