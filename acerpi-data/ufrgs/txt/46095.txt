Documento gerado sob autenticação Nº LXX.474.344.49K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9663                  de  18/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ANDRE MOREIRA CUNHA, Professor do Magistério Superior,
lotado e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de Ciências
Econômicas, com a finalidade de participar de reunião na Université Paris 13, em Paris, França, no período
compreendido entre 18/11/2017 e 27/11/2017, incluído trânsito, com ônus limitado. Solicitação nº 30914.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
