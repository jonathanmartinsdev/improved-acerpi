Documento gerado sob autenticação Nº HEM.774.684.I19, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6694                  de  24/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, à
Professora LUCIANA MORTEO EBOLI, matrícula SIAPE 2578956, lotada e em exercício no Departamento de
Arte Dramática do Instituto de Artes, da classe A  de Professor Adjunto A, nível 02, para a classe C  de
Professor Adjunto, nível 01, com vigência financeira a partir da data de publicação da portaria, de acordo com
o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554, de 20 de
junho  de  2013  do  Ministério  da  Educação  e  a  Decisão  nº  401/2013  -  CONSUN.  Processo  nº
23078.511200/2017-41.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
