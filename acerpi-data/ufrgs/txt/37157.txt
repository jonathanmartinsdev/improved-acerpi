Documento gerado sob autenticação Nº VMO.126.046.ONO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3411                  de  25/04/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  LUCIO ALBINO AMARO DA SILVA,  Matrícula  SIAPE  2346610,  ocupante  do  cargo  de
Engenheiro-área, Código 701031, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe
do Setor  de  Projetos  de  Edificações,  Código  SRH 406,  Código  FG-5,  com vigência  a  partir  da  data  de
publicação no Diário Oficial da União. Processo nº 23078.006622/2017-08.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
