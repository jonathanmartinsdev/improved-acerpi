Documento gerado sob autenticação Nº TRP.290.292.I8T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5990                  de  08/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, SILVIA LIMA DE AQUINO, matrícula SIAPE n° 1770398, lotada no Departamento
Interdisciplinar do Campus Litoral Norte, para exercer a função de Coordenadora da COMGRAD do Curso de
Licenciatura em Ciências Sociais  EaD,  Código SRH 1531,  código FUC,  com vigência a  partir  da data de
publicação no Diário Oficial da União, pelo período de 02 anos. Processo nº 23078.519656/2018-31.
JANE FRAGA TUTIKIAN
Vice-Reitora.
