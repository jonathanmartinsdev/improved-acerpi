Documento gerado sob autenticação Nº QQZ.977.858.PI1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8536                  de  21/10/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir da data deste ato, como segue:
 
 
Transformar:
 - Vice-Superintendente de Obras da SUINFRA, Código SRH 910, Código CD-4, em Assessor do Pró-
Reitor de Planejamento e Administração, Código SRH 910, Código CD-4.
 
Processo nº 23078.023525/2016-91.
 
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
