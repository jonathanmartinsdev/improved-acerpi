Documento gerado sob autenticação Nº HGK.942.621.C19, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9718                  de  25/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar,  por Decisão Judicial  proferida no processo nº 5045969-14.2018.4.04.7100, da 24ª Vara
Federal de Porto Alegre, a Portaria n° 2316/2016, de 28/03/2016 que concedeu promoção funcional, no
Quadro desta Universidade,  à Professora MARIA ELLY HERZ GENRO,  matrícula SIAPE nº 2336550,  com
exercício no Departamento de Estudos Básicos da Faculdade de Educação da classe C de Professor Adjunto,
nível  04,  para  a  classe  D  de  Professor  Associado,  nível  01,   referente  ao  interstício  de  30/01/2014  a
29/01/2016, com vigência financeira a partir de 30/03/2016. Processo nº 23078.520433/2018-16.
 
 
Onde se lê:
... com vigência financeira a partir de 30/03/2016...
leia-se:
... com vigência financeira a partir de 30/01/2016, ficando ratificados os demais termos.
 
RUI VICENTE OPPERMANN
Reitor.
