Documento gerado sob autenticação Nº HKH.335.938.POC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3267                  de  04/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do país  de EDISON PIGNATON DE FREITAS,  Professor  do Magistério
Superior, lotado e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a
finalidade de realizar visita à Shanghai Dianji University, em Shanghai, China, no período compreendido entre
26/05/2018 e 10/06/2018, incluído trânsito, com ônus limitado. Solicitação nº 45851.
RUI VICENTE OPPERMANN
Reitor
