Documento gerado sob autenticação Nº PVJ.677.517.7L6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6361                  de  17/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar a Portaria n° 5441/2017, de 22/06/2017, que concedeu autorização para afastamento do
País a JOÃO RICARDO VIEIRA IGANCI, Professor do Magistério Superior, com exercício no Departamento de
Botânica do Instituto de Biociências
Onde se lê: com ônus CNPq,
leia-se: com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias), ficando ratificados os demais termos.
Solicitação nº 29703.
JANE FRAGA TUTIKIAN,
Vice-Reitora, no exercício da Reitoria.
