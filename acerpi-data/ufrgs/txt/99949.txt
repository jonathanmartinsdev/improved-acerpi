Documento gerado sob autenticação Nº TYF.343.558.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9644                  de  23/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  JUSSARA CARNEVALE DE ALMEIDA,  matrícula  SIAPE n°  2420042,  lotada e  em exercício  no
Departamento de Nutrição da Faculdade de Medicina, da classe D  de Professor Associado, nível 01, para a
classe D  de Professor Associado, nível 02, referente ao interstício de 15/08/2016 a 14/08/2018, com vigência
financeira  a  partir  de  15/08/2018,  conforme  decisão  judicial  proferida  no  processo  nº  5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.525904/2019-63.
RUI VICENTE OPPERMANN
Reitor.
