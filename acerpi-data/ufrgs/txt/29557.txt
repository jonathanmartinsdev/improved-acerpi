Documento gerado sob autenticação Nº GYH.222.653.SCC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8507                  de  20/10/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora REGINA ANTUNES TEIXEIRA DOS SANTOS, matrícula SIAPE n° 1960620, lotada e em exercício no
Departamento de Música do Instituto de Artes, da classe C  de Professor Adjunto, nível 02, para a classe C  de
Professor Adjunto, nível 03, referente ao interstício de 04/08/2014 a 03/08/2016, com vigência financeira a
partir de 04/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações  e  a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.507344/2016-12.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
