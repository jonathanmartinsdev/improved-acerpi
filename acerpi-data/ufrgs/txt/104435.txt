Documento gerado sob autenticação Nº SDB.213.155.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             962                  de  29/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal  desta  Universidade,  SIMONE  DE  AZEVEDO  ZANETTE,  matrícula  SIAPE  n°  1276054,  lotada  no
Departamento  de  Cirurgia  da  Faculdade de  Medicina,  como Coordenadora  Substituta  da  Comissão de
Pesquisa da Faculdade de Medicina, com vigência a partir da data desto ato, pelo período de 2 anos, nos
termos do art. 25 do Decreto nº 9.739/2019, em concordância com a norma do item 3.4 do Manual de
Estruturas Organizacionais do Poder Executivo Federal/SIORG, não recebendo remuneração de qualquer
natureza por essa função. Processo nº 23078.535194/2019-80.
RUI VICENTE OPPERMANN
Reitor.
