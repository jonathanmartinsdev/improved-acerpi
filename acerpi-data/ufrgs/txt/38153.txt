Documento gerado sob autenticação Nº LLX.980.599.NAL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4146                  de  11/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  10/02/2017,
correspondente ao grau Insalubridade Máxima, ao servidor RAFAEL MENEZES NUNES, Identificação Única
21905053, Professor do Magistério Superior, com exercício no Departamento de Metalurgia da Escola de
Engenharia, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei
8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Insalubres conforme
Laudo Pericial  constante  no Processo nº  23078.504570/2017-22,  Código  SRH n°  23113 e  Código  SIAPE
2017001667.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
