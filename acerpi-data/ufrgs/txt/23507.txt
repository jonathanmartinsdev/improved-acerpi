Documento gerado sob autenticação Nº RJX.352.318.9I7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4553                  de  22/06/2016
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de
2013
RESOLVE
Conceder à servidora FABIANA HITOMI TANABE, ocupante do cargo de  Nutricionista-habilitação -
701055, lotada no Campus Litoral Norte, SIAPE 2111379, o percentual de 30% (trinta por cento) de Incentivo à
Qualificação, a contar de 22/01/2016, tendo em vista a conclusão do curso de Especialização em Psicologia
em Nutrição: Intervenções no Comportamento Alimentar, conforme o Processo nº 23078.001245/2016-21.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
