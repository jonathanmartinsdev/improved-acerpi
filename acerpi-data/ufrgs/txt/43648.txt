Documento gerado sob autenticação Nº KYE.884.443.IK8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8050                  de  28/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora FERNANDA FRANCISCATTO DE AGUIAR, ocupante do cargo de  Assistente
em Administração - 701200, lotada na Superintendência de Infraestrutura, SIAPE 2412304, o percentual de
15% (quinze por cento) de Incentivo à Qualificação, a contar de 01/08/2017, tendo em vista a conclusão do
curso de Medicina Veterinária, conforme o Processo nº 23078.014611/2017-93.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
