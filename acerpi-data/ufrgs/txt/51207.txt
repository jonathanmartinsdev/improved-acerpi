Documento gerado sob autenticação Nº YVD.300.348.EQ6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1597                  de  22/02/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias,
RESOLVE
Designar THAIS PICCOLI FACCO, Matrícula SIAPE 2332151, ocupante do cargo de Assistente em
Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de Diretora
da Divisão de Apoio Administrativo da GA da PROPLAN, código SRH 529, código FG-7, com vigência a partir da
data de publicação no Diário Oficial da União. Processo nº 23078.510353/2017-71.
RICARDO DEMETRIO DE SOUZA PETERSEN
Decano do Conselho Universitário, no exercício da Reitoria.
