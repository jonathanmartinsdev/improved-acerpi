Documento gerado sob autenticação Nº AUW.237.369.QC3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8591                  de  19/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  à  servidora  TALITA  AMARAL  ORSOLINI,  ocupante  do  cargo  de  Assistente  em
Administração - 701200, lotada na Faculdade de Medicina, SIAPE 1078580, o percentual de 15% (quinze por
cento)  de  Incentivo  à  Qualificação,  a  contar  de  04/09/2019,  tendo  em vista  a  conclusão  do  curso  de
Comunicação Social - Bacharelado, conforme o Processo nº 23078.524206/2019-41.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
