Documento gerado sob autenticação Nº WZG.227.407.DNK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5054                  de  07/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  CAMILA  QUEVEDO  CARVALHO,  Matrícula  SIAPE  2274097,  ocupante  do  cargo  de
Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a
função de Diretora Adjunta da Divisão de Cursos Lato Sensu do DAPG da PROPG, Código SRH 399, Código FG-
4, com vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.010506/2017-85.
JANE FRAGA TUTIKIAN
Vice-Reitora.
