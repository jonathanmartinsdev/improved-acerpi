Documento gerado sob autenticação Nº HUX.658.853.8K6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4682                  de  25/05/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de MARCELO SCHERER PERLIN, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ciências Administrativas da Escola de Administração, com a
finalidade de realizar visita  à Goethe-Universität Frankfurt am Main, em Frankfurt, Alemanha, no período
compreendido entre 25/06/2017 e 27/07/2017, incluído trânsito, com ônus limitado. Solicitação nº 27030.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
