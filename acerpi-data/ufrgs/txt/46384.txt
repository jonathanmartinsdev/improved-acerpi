Documento gerado sob autenticação Nº JAA.397.283.QUL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9867                  de  25/10/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor OLY CAMPOS CORLETA, matrícula SIAPE n° 3174460, lotado e em exercício no Departamento de
Cirurgia da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 03, para a classe C  de Professor
Adjunto, nível 04, referente ao interstício de 22/07/2015 a 21/07/2017, com vigência financeira a partir de
22/07/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e
a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.513557/2017-64.
JANE FRAGA TUTIKIAN
Vice-Reitora.
