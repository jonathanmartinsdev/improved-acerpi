Documento gerado sob autenticação Nº PVR.001.660.FE1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8487                  de  18/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de EDUARDO KELLER SAADI, Professor do Magistério Superior,
lotado e em exercício no Departamento de Cirurgia da Faculdade de Medicina, com a finalidade de participar
do  "33rd  EACTS  Annual  Meeting",  em Lisboa,  Portugal,  no  período  compreendido  entre  01/10/2019  e
06/10/2019, incluído trânsito, com ônus limitado. Solicitação nº 86899.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
