Documento gerado sob autenticação Nº UFD.908.010.S6T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1160                  de  03/02/2020
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
ALTERAR o percentual  de Incentivo à  Qualificação concedido ao servidor  MATHEUS DOS REIS
LOHMANN, ocupante do cargo de Analista de Tecnologia da Informação-701062, lotado na Pró-Reitoria de
Planejamento  e  Administração,  SIAPE  2111321,  para  52%  (cinquenta  e  dois  por  cento),  a  contar  de
02/12/2019, tendo em vista a conclusão do curso de Mestrado Acadêmico em Engenharia de Produção,
conforme o Processo nº 23078.533385/2019-15.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
