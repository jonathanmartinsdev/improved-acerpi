Documento gerado sob autenticação Nº NEP.103.438.ABL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6096                  de  11/07/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar CATHARINA SIMONI DA COSTA,  matrícula SIAPE n° 0353528,  ocupante do cargo de
Professor do Magistério Superior, classe Adjunto, lotada no Departamento de Odontologia Conservadora, do
Quadro de Pessoal da Universidade, para exercer a função de Chefe Substituta do Depto de Odontologia
Conservadora da Faculdade de Odontologia, com vigência a partir da data deste ato e até 16/03/2018, a fim
de completar o mandato  do Professor LUIS CARLOS DA FONTOURA FRASCA, conforme artigo 92 do Estatuto
da mesma Universidade. Processo nº 23078.011941/2017-27.
RUI VICENTE OPPERMANN
Reitor.
