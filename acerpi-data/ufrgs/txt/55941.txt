Documento gerado sob autenticação Nº SMN.059.451.VDP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4776                  de  04/07/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Autorizar o afastamento no país de João Matheus Jury Giraldi, ocupante do cargo de Professor do
Magistério Superior,  lotado e em exercício no Departamento de Matemática Pura e Aplicada do Instituto de
Matemática e Estatística,  com a finalidade de participar do "2018 International Congress of Mathematicians",
no Rio de Janeiro, Brasil, no período compreendido entre 31/07/2018 e 10/08/2018, incluído trânsito, com
ônus limitado. Solicitação n° 47426.
RUI VICENTE OPPERMANN
Reitor.
