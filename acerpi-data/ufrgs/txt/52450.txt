Documento gerado sob autenticação Nº CDM.359.657.2HF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2309                  de  27/03/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  a  ocupante  do  cargo  de  Professor  do  Magistério  Superior,  classe  Adjunto,  desta
Universidade, ROSA ANGELA CHIEZA, matrícula SIAPE n° 1901537, lotada no Departamento de Economia e
Relações Internacionais da Faculdade de Ciências Econômicas, para exercer a função de Coordenadora da
Comissão de Extensão da FCE, Código SRH 774, com vigência a partir deste ato, pelo período de 02 anos, por
ter  sido  reeleita,  ficando  o  pagamento  de  gratificação  condicionado  à  disponibilidade  de  uma função
gratificada. Processo nº 23078.504378/2018-17.
JANE FRAGA TUTIKIAN
Vice-Reitora.
