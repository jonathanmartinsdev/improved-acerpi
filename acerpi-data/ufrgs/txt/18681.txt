Documento gerado sob autenticação Nº NIF.019.993.082, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1797                  de  09/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de Outubro de 2012
RESOLVE:
Conceder  Progressão  por  Capacitação,  a  contar  de  17/02/2016,   referente  ao  interstício  de
01/03/2005 a 16/02/2016, para a servidora ENILDA TERESINHA COUTO DA COSTA SILVA, ocupante do cargo
de Servente de Limpeza - 701823, matrícula SIAPE 0358156,  lotada  no  Instituto de Pesquisas Hidráulicas,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  A  I,  para  o  Nível  de  Classificação/Nível  de
Capacitação  A  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.200345/2016-39:
Micros & Methodos - Windows CH: 40 Carga horária utilizada: 20 hora(s) / Carga horária excedente: 20 hora(s)
(01/11/2015 a 31/12/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
