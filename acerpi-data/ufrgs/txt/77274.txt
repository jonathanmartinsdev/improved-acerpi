Documento gerado sob autenticação Nº NRJ.608.694.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1857                  de  25/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar SHEILA IRRIBAREM DE MELLO BOTT,  Matrícula SIAPE 1083070, ocupante do cargo de
Bibliotecário-documentalista,  Código 701010,  do Quadro de Pessoal  desta Universidade,  para exercer  a
função de Chefe da Biblioteca de Artes, Código SRH 441, código FG-5, com vigência a partir da data de
publicação no Diário Oficial da União. Processo nº 23078.503917/2019-81.
RUI VICENTE OPPERMANN
Reitor.
