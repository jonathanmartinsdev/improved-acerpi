Documento gerado sob autenticação Nº YRT.984.925.J9H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             342                  de  09/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  09/01/2020,   referente  ao  interstício  de
22/04/2016 a 08/01/2020, para o servidor FERNANDO ANTONIO RANGEL LOPES DAUDT, ocupante do cargo
de Odontologo - 701063, matrícula SIAPE 2446921,  lotado  na  Faculdade de Odontologia, passando do Nível
de Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.500565/2020-46:
CENED - Biossegurança Hospitalar CH: 180 Carga horária utilizada: 163 hora(s) / Carga horária excedente: 17
hora(s) (26/11/2019 a 21/12/2019)
PPGODO - Bioestatística Avançada CH: 17 (17/08/2015 a 01/09/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
