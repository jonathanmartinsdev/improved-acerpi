Documento gerado sob autenticação Nº ABF.274.652.72M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6414                  de  17/08/2018
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
 
Designar os servidores Angélica Elisa Giacomel Schaefer, Diretora do Departamento de Benefícios
e Assistência  Estudantil,  Felipe Vendruscolo da Silva,  Diretor  da Divisão de Bolsas  e  Auxílios,  Jéssica
Froehlich Flores,  Coordenadora de Execução Financeira e Orçamentária, e Liciê Helena Ribeiro Nardi,
Assistente Social com exercício na Divisão de Seleção e Acompanhamento Pedagógico, Psicológico e Social,
para, sob a presidência da primeira, comporem Comissão de Análise com o objetivo de apurar a renda média
mensal per capita dos candidatos ao Programa de Benefícios e Moradia Estudantil da PRAE.
 
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
