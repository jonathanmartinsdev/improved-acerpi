Documento gerado sob autenticação Nº PJG.061.658.2EP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6472                  de  25/08/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal desta Universidade, RENATO LEVIEN, matrícula SIAPE n° 0358815, lotado no Departamento de Solos
da Faculdade de Agronomia, como Diretor Substituto da Estação Experimental Agronômica, para substituir
automaticamente  o  titular  desta  função  em  seus  afastamentos  ou  impedimentos  regulamentares  na
vigência do presente mandato. Processo nº 23078.017800/2016-37.
CARLOS ALEXANDRE NETTO
Reitor
