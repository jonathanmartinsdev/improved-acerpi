Documento gerado sob autenticação Nº SEW.193.914.4RG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8749                  de  26/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar RENATO PIERETTI DUARTE,  Matrícula SIAPE 0358836, ocupante do cargo de Vigilante,
Código  701269,  do  Quadro  de  Pessoal  desta  Universidade,  para  exercer  a  função  de  Secretário  da
Coordenadoria de Segurança, Código SRH 325, código FG-4, com vigência a partir da data de publicação no
Diário Oficial da União. Processo nº 23078.023291/2016-81.
JANE FRAGA TUTIKIAN
Vice-Reitora
