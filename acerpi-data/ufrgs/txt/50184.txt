Documento gerado sob autenticação Nº IOF.731.684.KQR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             846                  de  30/01/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de CARLOS EDUARDO SCHONERWALD DA SILVA, Professor do
Magistério Superior,  lotado e em exercício no Departamento de Economia e Relações Internacionais da
Faculdade de Ciências Econômicas, com a finalidade de participar de conferência junto ao Levy Economics
Institute  of  Bard  College,  em  Boston,  Estados  Unidos,  no  período  compreendido  entre  28/02/2018  e
05/03/2018, incluído trânsito, com ônus limitado. Solicitação nº 33776.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
