Documento gerado sob autenticação Nº TWX.666.280.LKT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             448                  de  12/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°51746,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, LUCIANO MACHADO LIMA (Siape: 0357479 ),
 para substituir   MARCOS FREIRE MACHADO (Siape: 1683409 ), Coordenador do Núcleo de Infraestrutura da
Gerência Administrativa da Faculdade de Odontologia, Código FG-7, em seu afastamento por motivo de
férias, no período de 13/01/2020 a 24/01/2020, com o decorrente pagamento das vantagens por 12 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
