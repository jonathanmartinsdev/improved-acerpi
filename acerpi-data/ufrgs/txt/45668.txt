Documento gerado sob autenticação Nº EYT.866.856.91U, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9344                  de  09/10/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Retificar  a  Portaria  nº  8576/2017,  de  13/09/2017,  que  designou,  temporariamente,  MAGALI
MENDES DE MENEZES (Siape: 2307961) para substituir  CESAR VALMOR MACHADO LOPES (Siape: 1247066 ),
Processo SEI nº 23078.517184/2017-09
 
onde se lê:
"...no período de 23/09/2017 a 05/10/2017, com o decorrente pagamento das vantagens por
13 dias".
         
           leia-se:
          "... no período de 23/09/2017 a 03/10/2017, com o decorrente pagamento das vantagens por
11 dias..."; ficando ratificados os demais termos.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
