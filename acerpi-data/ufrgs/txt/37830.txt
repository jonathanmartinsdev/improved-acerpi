Documento gerado sob autenticação Nº LWK.224.331.OQ8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3925                  de  05/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  LUCAS  DE  OLIVEIRA  ALVARES,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Biofísica do Instituto de Biociências, com a finalidade de
participar do "40th Annual Meeting of the  Japan Neuroscience Society", em Chiba, com ônus UFRGS (Pró-
Reitoria de Pesquisa - diárias) e realizar visita  à University of Tokyo, em Tokyo, Japão, com ônus limitado, no
período compreendido entre 12/07/2017 e 25/07/2017, incluído trânsito. Solicitação nº 27442.
RUI VICENTE OPPERMANN
Reitor
