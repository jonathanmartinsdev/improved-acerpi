Documento gerado sob autenticação Nº CVF.106.810.K8E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2192                  de  23/03/2018
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
              Retifica a portaria número 4138/2017, que nomeou a Diretoria Executiva eleita no Centro Acadêmico
André da Rocha, que passa a encerrar o mandato em 31 de dezembro de 2017.
 
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
