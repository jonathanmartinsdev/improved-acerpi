Documento gerado sob autenticação Nº HFX.793.718.ODM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7488                  de  22/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5253, de 01 de outubro de 2012
RESOLVE
Retificar  a  Portaria  n°  5982/2016,  de  10/08/2016,  publicada  no  Diário  Oficial  da  União  de
12/08/2016, que nomeou em caráter efetivo ALINE BEDNARSKI LUMMERTZ, para o cargo de Assistente em
Administração. Processo nº 23078.014002/2015-72.
,
 
Onde se lê:
N° 5.98 Nomear, em caráter efetivo...
leia-se:
N° 5.982 Nomear, em caráter efetivo...
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
