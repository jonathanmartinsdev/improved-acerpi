Documento gerado sob autenticação Nº VFN.469.515.00Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3490                  de  25/04/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
             Tornar sem efeito a Portaria nº 2673 de 15 de junho de 2010, que criou o Comitê Gestor de Tecnologia
da Informação da UFRGS.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
