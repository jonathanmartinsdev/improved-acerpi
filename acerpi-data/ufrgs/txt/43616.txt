Documento gerado sob autenticação Nº OGQ.816.308.6DT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8082                  de  29/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 90 (noventa) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de
11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
REGIANE JUCHEN MACHADO ACCORSI, com exercício no Núcleo Acadêmico de Pós-Graduação do Instituto de
Filosofia  e  Ciências  Humanas,  a  ser  usufruída  no  período  de  01/10/2017  a  29/12/2017,  referente  ao
quinquênio de 11/03/2008 a 10/03/2013, a fim de realizar atividades de pesquisa e redação da dissertação de
Mestrado  do  Programa  de  Pós-Graduação  em  Políticas  Públicas,  na  UFRGS;  conforme  Processo  nº
23078.201845/2017-79.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
