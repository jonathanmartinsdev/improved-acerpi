Documento gerado sob autenticação Nº IEV.286.886.P57, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8898                  de  04/11/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  HELENA  ARAUJO  RODRIGUES  KANAAN,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Artes Visuais do Instituto de Artes, com a
finalidade de participar de encontro junto ao Instituto Cultural Paraguayo-Alemán - Instituto Goethe, em
Assunção, Paraguai, no período compreendido entre 19/11/2016 e 27/11/2016, incluído trânsito, com ônus
limitado. Solicitação nº 23986.
RUI VICENTE OPPERMANN
Reitor
