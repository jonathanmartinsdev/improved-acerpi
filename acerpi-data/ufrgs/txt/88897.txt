Documento gerado sob autenticação Nº DRA.181.547.O4E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2693                  de  27/03/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/03/2019,   referente  ao  interstício  de
10/08/2017 a 25/03/2019,  para a servidora BRUNA MOLINA LEAL,  ocupante do cargo de Técnico em
Assuntos Educacionais - 701079, matrícula SIAPE 2416384,  lotada  no  Instituto de Biociências, passando do
Nível de Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.507690/2019-43:
Formação Integral de Servidores da UFRGS V CH: 121 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 1 hora(s) (25/09/2017 a 12/12/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
