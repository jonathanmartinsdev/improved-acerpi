Documento gerado sob autenticação Nº KFK.341.402.VG9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4347                  de  13/06/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar  LUIZ  DARIO  TEIXEIRA  RIBEIRO,  CPF  n°  13256025072,  matrícula  SIAPE  n°  0353704,
ocupante do cargo de Professor do Magistério Superior,  classe Associado,  lotado no Departamento de
História do Instituto de Filosofia e Ciências Humanas, do Quadro de Pessoal da Universidade Federal do Rio
Grande do Sul, como Coordenador da COMGRAD de História, Código SRH 1184, código FUC, com vigência a
partir da data de publicação no Diário Oficial da União e até 30/01/2017, a fim de completar o mandato de
TEMISTOCLES AMERICO CORREA CEZAR, conforme artigo 92 do Estatuto da mesma Universidade. Processo nº
23078.201951/2016-71.
RUI VICENTE OPPERMANN
Vice-Reitor
