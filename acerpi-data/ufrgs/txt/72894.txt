Documento gerado sob autenticação Nº ZOX.376.514.QVE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9729                  de  03/12/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  FERNANDO MARCELO PEREIRA,  matrícula  SIAPE n°  1813048,  ocupante  do cargo de
Professor do Magistério Superior, classe Associado, lotado no Departamento de Engenharia Mecânica da
Escola  de  Engenharia,  do  Quadro  de  Pessoal  da  Universidade  Federal  do  Rio  Grande  do  Sul,  como
Coordenador do PPG em Engenharia Mecânica, Código SRH 1160, código FUC, com vigência a partir da data
de  publicação  no  Diário  Oficial  da  União  até  30/06/2020,  a  fim  de  completar  o  mandato  do
professor EDUARDO ANDRE PERONDI, conforme artigo 92 do Estatuto da mesma Universidade. Processo nº
23078.528305/2018-11.
JANE FRAGA TUTIKIAN
Vice-Reitora.
