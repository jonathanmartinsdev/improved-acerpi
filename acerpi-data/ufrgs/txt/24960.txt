Documento gerado sob autenticação Nº GNU.264.002.2JQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5500                  de  22/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5253, de 01 de outubro de 2012
RESOLVE
Tornar sem efeito a Portaria nº 4419/2016, de 16/06/2016, publicada no Diário Oficial da União de
20/06/2016,  que  nomeou  em  caráter  efetivo,  ZACARIAS  EDUARDO  FABRIM,  para  o  cargo  de
Engenheiro/Área: Mecânica, tendo em vista que não tomou posse no prazo previsto em lei conforme prevê o
artigo 13, parágrafos 1º e 6º da Lei 8.112, de 11 de dezembro de 1990. Processo nº 23078.020128/13-15.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
