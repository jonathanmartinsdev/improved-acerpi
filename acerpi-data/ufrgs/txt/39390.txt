Documento gerado sob autenticação Nº GJH.756.516.I76, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5093                  de  09/06/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de GUSTAVO KUHN PFEIFER, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ciências Morfológicas do Instituto de Ciências Básicas da Saúde,
com a finalidade de participar de curso na  Showa University Northern Yokohama Hospital, em Yokohama,
Japão,  no período compreendido entre  29/06/2017 e  03/10/2017,  incluído trânsito,  com ônus limitado.
Solicitação nº 28714.
RUI VICENTE OPPERMANN
Reitor
