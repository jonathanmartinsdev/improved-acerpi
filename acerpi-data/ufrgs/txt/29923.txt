Documento gerado sob autenticação Nº MFT.154.556.RJB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8773                  de  27/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  IDA  VANESSA  DOEDERLEIN  SCHWARTZ,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Genética do Instituto de Biociências, com a
finalidade de participar de reunião junto ao Shire Human Genetic Therapies, em Barcelona, Espanha, no
período compreendido entre 27/10/2016 e 29/10/2016, incluído trânsito, com ônus limitado. Solicitação nº
24356.
RUI VICENTE OPPERMANN
Reitor
