Documento gerado sob autenticação Nº PBK.389.593.10B, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4650                  de  24/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  LUCIANO  STURMER  DE  FRAGA,  matrícula  SIAPE  n°  3350374,  lotado  no
Departamento de Fisiologia do Instituto de Ciências Básicas da Saúde, como Chefe Substituto do Depto de
Fisiologia  do  ICBS,  para  substituir  automaticamente  o  titular  desta  função  em  seus  afastamentos  ou
impedimentos regulamentares na vigência do presente mandato. Processo nº 23078.007818/2017-10.
JANE FRAGA TUTIKIAN
Vice-Reitora.
