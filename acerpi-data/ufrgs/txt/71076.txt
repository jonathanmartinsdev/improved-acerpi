Documento gerado sob autenticação Nº PAM.500.365.UOV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8382                  de  18/10/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de PAULO EDISON BELO REYES, Professor do Magistério Superior,
lotado e em exercício no Departamento de Urbanismo da Faculdade de Arquitetura, com a finalidade de
participar do "Seminário em Design", em Lisboa, Portugal, no período compreendido entre 08/11/2018 e
13/11/2018, e da "Politics and Image Conference", em Coimbra, Portugal, no período compreendido entre
14/11/2018 e 19/11/2018, sendo de 15/11/2018 a 17/ 11/2018 com ônus CAPES/PROEX e o restante com ônus
limitado. Solicitação nº 59981.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
