Documento gerado sob autenticação Nº FFO.405.113.E88, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10229                  de  07/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  06/11/2017,   referente  ao  interstício  de
06/05/2016 a 05/11/2017, para a servidora BARBARA IEGER VIANNA, ocupante do cargo de Bibliotecário-
documentalista - 701010, matrícula SIAPE 2164049,  lotada  no  Instituto de Química, passando do Nível de
Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.517427/2017-09:
Formação Integral de Servidores da UFRGS I CH: 30 (09/08/2017 a 11/09/2017)
UFSM - Oficina Ubuntu GNU/Linux Versão 16.04 LTS CH: 30 (15/05/2017 a 11/06/2017)
UFSM - Elaboração de E-Book CH: 30 (10/04/2017 a 08/05/2017)
PUCPR - Curso de MARC 21 - Formato Autoridade CH: 60 (19/10/2016 a 21/10/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
