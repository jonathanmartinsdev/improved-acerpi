Documento gerado sob autenticação Nº MYY.320.533.RAQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             26                  de  03/01/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  15/12/2016,   referente  ao  interstício  de
02/09/2010 a 14/12/2016,  para a  servidora MARILIA SPINELLI  JACOBY CUNDA,  ocupante do cargo de
Psicólogo-área - 701060, matrícula SIAPE 1683108,  lotada  no  Instituto de Psicologia, passando do Nível de
Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.025507/2016-43:
Formação Integral de Servidores da UFRGS I CH: 39 (23/04/2009 a 20/04/2010)
UNIFESP -  SUPERA -  Sistema para detecção do Uso abusivo e dependência de substâncias psicoativas:
Encaminhamento, intervenção breve, reinserção social e acompanhamento. CH: 150 Carga horária utilizada:
111 hora(s) / Carga horária excedente: 39 hora(s) (15/08/2016 a 22/11/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
