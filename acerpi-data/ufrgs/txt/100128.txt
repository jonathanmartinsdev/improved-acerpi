Documento gerado sob autenticação Nº ZHY.591.601.DPG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9928                  de  04/11/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.527565/2019-50  e
23078.523971/2018-62, da Lei nº 10.520/02, do Contrato nº 005/2019 e ainda, da Lei nº 8.666/93,
               RESOLVE:
 
            Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no inciso I do item 13.1 do Termo de
Referência, à Empresa LIDERANÇA LIMPEZA E CONSERVAÇÃO LTDA, CNPJ nº 00.482.840/0001-38, pelos
seguintes descumprimentos contratuais: entrega de documentação em atraso e atraso no pagamento de
salários,  conforme  atestado  pela  fiscalização  (Doc.  SEI  nº  1828572  e  1849685),  bem  como  pelo
DICON/NUDECON (Doc. SEI nº 1853333) do processo administrativo 23078.527565/2019-50.
 
             Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
