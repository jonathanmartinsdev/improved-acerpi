Documento gerado sob autenticação Nº WBN.862.874.PID, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6357                  de  24/08/2016
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de
2013
RESOLVE
Homologar  o  parecer  que  aprova  o  servidor  técnico-administrativo  LOURENCO BRITO FELIN,
ocupante do cargo de Assistente Social,  no estágio probatório cumprido no período de 14/08/2013 até
14/08/2016, fazendo jus, a partir desta data, à estabilidade no serviço público federal.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
