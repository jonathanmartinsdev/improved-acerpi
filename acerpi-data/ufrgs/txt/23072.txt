Documento gerado sob autenticação Nº CTN.238.593.T9Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4153                  de  08/06/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Remover, a partir de 7 de junho de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da Lei
n° 8.112, de 11 de dezembro de1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997, Cesar
Daniel de Assis Rolim, ocupante do cargo de Técnico em Assuntos Educacionais, Ambiente Organizacional
Administrativo,  Código 701079,  Classe  E,  Nível  de  Capacitação III,  Padrão de Vencimento 03,  SIAPE nº.
1904145 da Secretaria de Educação a Distância para a lotação Biblioteca Central, com novo exercício na
Biblioteca Central.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
