Documento gerado sob autenticação Nº XIF.151.940.C8I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 1403/2019-PROGESP Porto Alegre, 04 de setembro de 2019.
Senhora Pró-Reitora,
Encaminhamos a servidora KYRIE MACHADO DA ROSA, ocupante do cargo Assistente Social, para
exercício nessa Unidade no dia 04 de setembro de 2019.
Solicitamos que nos sejam encaminhadas, no prazo máximo de 03 dias, as seguintes informações:
- atividades a serem desenvolvidas pela servidora;
- local de exercício (divisão, setor), observando a hierarquia dos órgãos registrada no SRH;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal.
Informamos que este encaminhamento se faz para reposição de vaga, em virtude da remoção da
servidora Karen Luana Wasen.
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
Ilma. Sra.
Professora SUZI ALVES CAMEY,
Pró-Reitora de Assuntos Estudantis,
N/Universidade.
 
