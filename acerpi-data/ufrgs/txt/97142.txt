Documento gerado sob autenticação Nº NGQ.845.444.FGI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7753                  de  27/08/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Exonerar, a pedido, a partir de 20 de agosto de 2019, nos termos do artigo 34 "caput", da Lei n°
8.112, de 1990, BARBARA QUATRIN TIELLET DA SILVA, ocupante do cargo de Administrador, código 701001,
nível de classificação E, nível de capacitação I, padrão 05, do Quadro de Pessoal, lotada e com exercício na
Pró-Reitoria de Planejamento e Administração. Processo nº 23078.522314/2019-89.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
