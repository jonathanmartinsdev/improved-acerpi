Documento gerado sob autenticação Nº AVE.721.032.T5J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3752                  de  03/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta  Universidade,  MARCO  ANTONIO  ZACHIA  AYUB,  matrícula  SIAPE  n°  6358875,  lotado  no
Departamento de Tecnologia dos Alimentos do Instituto de Ciências e Tecnologia de Alimentos, como Chefe
Substituto do Depto de Tecnologia de Alimentos do ICTA, para substituir automaticamente o titular desta
função em seus afastamentos ou impedimentos regulamentares na vigência do presente mandato. Processo
nº 23078.510924/2019-30.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
