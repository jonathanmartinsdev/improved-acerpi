Documento gerado sob autenticação Nº TUM.338.836.24J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             729                  de  25/01/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  FERNANDO  HEPP  PULGATI,  matrícula  SIAPE  nº  1573549,  lotado  no
Departamento de Estatística do Instituto de Matemática e Estatística, para exercer a função de Presidente da
CPPD, Código SRH 221, código FG-1, com vigência a partir da data de publicação no Diário Oficial da União,
pelo período de 01 ano. Processo n° 23078.022763/2016-89.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
