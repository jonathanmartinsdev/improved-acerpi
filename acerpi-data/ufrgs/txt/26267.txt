Documento gerado sob autenticação Nº XPQ.778.780.LIS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6233                  de  19/08/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARIA GABRIELA CURUBETO GODOY, matrícula SIAPE n° 1472771, lotada e em exercício no
Departamento de Assistência e Orientação Profissional da Escola de Enfermagem, da classe C  de Professor
Adjunto, nível 01, para a classe C  de Professor Adjunto, nível 02, referente ao interstício de 26/02/2013 a
25/02/2015, com vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de
28 de dezembro de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº
401/2013-CONSUN. Processo nº 23078.503643/2016-88.
RUI VICENTE OPPERMANN
Vice-Reitor
