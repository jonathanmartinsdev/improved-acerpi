Documento gerado sob autenticação Nº QNC.182.193.8KV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8294                  de  04/09/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de DANIEL SERGIO PRESTA GARCIA,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Engenharia de Produção e Transportes da Escola de
Engenharia,  com a finalidade de ministrar curso na Universidad Nacional del  Nordeste,  em Resistencia,
Argentina, no período compreendido entre 07/09/2017 e 09/09/2017, incluído trânsito, com ônus limitado.
Solicitação nº 30590.
RUI VICENTE OPPERMANN
Reitor
