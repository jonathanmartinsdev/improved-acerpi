Documento gerado sob autenticação Nº HUU.040.222.JJN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1265                  de  09/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°39439,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO EM MANUTENCAO DE
ÁUDIO/VÍDEO, do Quadro de Pessoal desta Universidade, JOAO MANOEL TESSARO (Siape: 1869082 ),  para
substituir   LUIZ FERNANDO BARBOSA AGUIRRE (Siape: 0356210 ), Coordenador do Núcleo de Infraestrutura
da Gerência Administrativa da Faculdade de Arquitetura, Código FG-7, em seu afastamento por motivo de
férias, no período de 14/02/2018 a 23/02/2018, com o decorrente pagamento das vantagens por 10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
