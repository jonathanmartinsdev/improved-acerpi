Documento gerado sob autenticação Nº MFL.529.421.EMK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             322                  de  09/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito a partir de 1° de janeiro de 2020 a Portaria nº 9670/2019, de 24/10/2019, que
nomeou o Núcleo de Gestão de Desempenho do Centro de Estudos Costeiros, Limnológicos e Marinhos. 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
