Documento gerado sob autenticação Nº VUV.781.045.CB7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7343                  de  09/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  ALY  FERREIRA  FLORES  FILHO,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Engenharia Elétrica da Escola de Engenharia, com a
finalidade de participar do "18th International Symposium on Electromagnetic Fields in Mechatronics", em
Lodz,  Polônia,  no  período  compreendido  entre  08/09/2017  e  17/09/2017,  incluído  trânsito,  com  ônus
limitado. Solicitação nº 29692.
RUI VICENTE OPPERMANN
Reitor
