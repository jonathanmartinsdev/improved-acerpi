Documento gerado sob autenticação Nº KRV.338.513.IA9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3731                  de  02/05/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  01/05/2019,   referente  ao  interstício  de
01/11/2017 a 30/04/2019, para a servidora ELIANE ZABIELA, ocupante do cargo de Técnico em Assuntos
Educacionais - 701079, matrícula SIAPE 1094152,  lotada  na  Pró-Reitoria de Planejamento e Administração,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  E  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  E  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.509900/2019-38:
FGV - Programa de Capacitaçao e Desenvolvimento da Rede de Planejamento e Modernização do FNDE CH:
40 (25/05/2006 a 06/09/2006)
Curso de Capacitação em Informação,  Acessibilidade e  Direitos  Humanos -  CAPADHIA,  para Servidores
Públicos  Federais  CH:  160  Carga  horária  utilizada:  110  hora(s)  /  Carga  horária  excedente:  50  hora(s)
(30/09/2013 a 31/03/2014)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
