Documento gerado sob autenticação Nº RWB.939.154.9JM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5403                  de  21/06/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor DIOGO LOSCH DE OLIVEIRA, matrícula SIAPE n° 1824083, lotado e em exercício no Departamento
de Bioquímica do Instituto de Ciências Básicas da Saúde, da classe C  de Professor Adjunto, nível 03, para a
classe C  de Professor Adjunto, nível 04, referente ao interstício de 25/03/2015 a 24/03/2017, com vigência
financeira a partir de 25/03/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo
nº 23078.503872/2017-83.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
