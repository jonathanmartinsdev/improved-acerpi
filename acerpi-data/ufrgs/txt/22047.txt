Documento gerado sob autenticação Nº JGM.800.847.1RQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             4682                  de  24/06/2016
Nomeia  a  Representação  Discente  eleita
no  Diretório  Acadêmico  da  Economia,
Contábeis  e  Atuariais
O PRÓ-REITOR DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7149, de 05 de dezembro de 2012
RESOLVE
Nomear  a  Representação  Discente  eleita  para  compor  o  Diretório  Acadêmico  da  Economia,
Contábeis e Atuariais, com mandato de 01 (um) ano, a contar de 01 de janeiro de 2016, atendendo ao
disposto  nos  artigos  175  do  Regimento  Geral  da  Universidade  e  79  do  Estatuto  da  Universidade  e
considerando o processo nº 23078.008942/2016-11, conforme segue:
COORDENADORES                                                                                              
Gabriela da Silva Zílio                                                                                                          
Caio Bruno Bassi Olhier                                                                                                       
Ana Clara Cossetin Ferreira de Lima                                                                                     
 
SECRETÁRIA-GERAL
Jéssica dos Santos Guedes                                                                                                   
 
2º SECRETÁRIO
Gustavo Menezes Silveira                                                                                                     
 
TESOUREIRO-GERAL
Rafael Geliski                                                                                                                       
 
2º TESOUREIRO
Gabriel Vinicius Vieira                                                                                                           
 
COMISSÃO ACADÊMICA                        
Rafael Geliski                                                                                                                      
Walter de Vargas                                                                                                                   
Caio Bruno Bassi Olhier                                                                                                         
Documento gerado sob autenticação Nº JGM.800.847.1RQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Gustavo Menezes Silveira                                                                                                      
Francisco de Carvalho Santana                                                                                               
Stephania Ribeiro Vellinho                                                                                                       
Jessica dos Santos Guedes                                                                                                   
Leonardo Oliveira Fiad                                                                                                            
 Guilhermo Muller de Oliveira                                                                                                  
 
COMISSÃO DE MOVIMENTO ESTUDANTIL
Artur Peluso Waissman                                                                                                           
Elbio Maier Ozório                                                                                                                  
Cauê Assis Braz                                                                                                                     
Lucas Costa Nogueira                                                                                                          
Alexsander Castro Prestes                                                                                                   
Ricardo Minelli Bockmann                                                                                                  
Gabriela da Silva Zílio                                                                                                         
Rodolfo Fuchs dos Santos                                                                                                   
 
COMISSÃO DE COMUNICAÇÃO
Victor Gasperin Ribas                                                                                                          
Miguel Vargas Wainstein                                                                                                     
Ana Clara Cossetin Ferreira de Lima                                                                                   
Caroline Gisele Dutra                                                                                                           
Andressa Klagenberg                                                                                                           
Maurício Nunes Victorino                                                                                                    
Roger de Oliveira Borges Julio                                                                                            
Gabriel Vinicius Vieira                                                                                                         
 
 
                                                                                                                                                         
 
ANGELO RONALDO PEREIRA DA SILVA
Pró-Reitor de Assuntos Estudantis
