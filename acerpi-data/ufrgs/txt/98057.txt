Documento gerado sob autenticação Nº YBR.999.580.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8330                  de  12/09/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir de 16/09/2019, como segue:
 
Transformar: Chefe do Setor de Projetos de Infraestrutura vinculado ao Departamento de Projetos e
Planejamento da SUINFRA, Código FG-2, Código SRH 1045, em Chefe do Setor de Projetos de Edificações,
Código FG-2, Código SRH 1045.
 
Processo nº 23078.524808/2019-06.
 
 
RUI VICENTE OPPERMANN
Reitor.
