Documento gerado sob autenticação Nº DID.937.164.3VA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2362                  de  16/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a pedido, a partir de 10/03/2017, a ocupante do cargo de Psicólogo-área - 701060, do
Nível  de Classificação EII,  do Quadro de Pessoal  desta Universidade,  GRACE VALI FREITAG TANIKADO,
matrícula SIAPE 1645302, da função de Diretora da Divisão de Seleção e Acompanhamento Pedagógico,
Psicológico e Social do DBAS da PRAE, Código SRH 996, Código FG-4, para a qual foi designada pela Portaria
nº  3029/15  de  20/04/2015,  publicada  no  Diário  Oficial  da  União  de  24/04/2015.  Processo  nº
23078.003506/2017-29.
JANE FRAGA TUTIKIAN
Vice-Reitora.
