Documento gerado sob autenticação Nº ZGD.547.698.JTD, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2185                  de  23/03/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°38933,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de CONTADOR, do Quadro de
Pessoal desta Universidade, VALERIA GOMES DA SILVA (Siape: 1683301 ),  para substituir   ANGELA TERESA
JUNG (Siape: 1728518 ), Chefe da Seção de Escrituração da Despesa/DAD/DCF/PROPLAN, Código FG-5, em
seu afastamento por motivo de férias, no período de 02/04/2018 a 16/04/2018, com o decorrente pagamento
das vantagens por 15 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
