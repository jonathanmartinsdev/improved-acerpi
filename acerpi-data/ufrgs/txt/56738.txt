Documento gerado sob autenticação Nº KPK.451.385.J7C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5392                  de  19/07/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar a Portaria nº 5025, de 12/07/2018, que concedeu autorização para afastamento do país a
LUIZ CARLOS PINTO DA SILVA FILHO,  Professor  do Magistério  Superior,  lotado no Departamento de
Engenharia Civil da Escola de Engenharia e com exercício na Escola de Engenharia.
Onde se lê: no período compreendido entre 12/08/2018 e 22/08/2018,
leia-se: no período compreendido entre 10/08/2018 e 26/08/2018, ficando ratificados os demais
termos. Solicitação nº 47799.
RUI VICENTE OPPERMANN
Reitor
