Documento gerado sob autenticação Nº AJE.247.322.DO4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6221                  de  21/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°49760,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do  Quadro  de  Pessoal  desta  Universidade,  MARCOS  ANTONIO  FERNANDES  (Siape:
0356226  ),   para  substituir    JAQUELINE  DA  SILVA  OLIVEIRA  (Siape:  2321843  ),  Secretário  da  Estação
Experimental Agronômica, Código FG-7, em seu afastamento por motivo de férias, no período de 22/07/2019
a 30/07/2019, com o decorrente pagamento das vantagens por 9 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
