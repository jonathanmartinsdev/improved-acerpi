Documento gerado sob autenticação Nº MHK.006.547.MM6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9818                  de  24/10/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  15/10/2017,   referente  ao  interstício  de
15/04/2016 a 14/10/2017, para a servidora BEATRIZ REGINA KLING TROTT, ocupante do cargo de Técnico
em Assuntos Educacionais - 701079,  matrícula SIAPE 2015856,  lotada  na  Pró-Reitoria de Graduação,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  E  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  E  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.513675/2017-72:
Formação Integral de Servidores da UFRGS VII CH: 197 Carga horária utilizada: 180 hora(s) / Carga horária
excedente: 17 hora(s) (11/08/2015 a 11/08/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
