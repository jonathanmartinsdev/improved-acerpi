Documento gerado sob autenticação Nº VXT.429.799.I0C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4318                  de  17/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a pedido, a partir de 01/06/2017, o ocupante do cargo de Técnico de Laboratório Área -
701244, do Nível de Classificação DIII, do Quadro de Pessoal desta Universidade, ANTÔNIO LOPES, matrícula
SIAPE 0379209, da função de Coordenador do Núcleo de Administração e Recursos Humanos da Gerência
Administrativa do Instituto de Artes, Código SRH 544, Código FG-7, para a qual foi designado pela Portaria nº
9610/2015  de  08/12/2015,  publicada  no  Diário  Oficial  da  União  de  10/12/2015.  Processo  nº
23078.008331/2017-46.
JANE FRAGA TUTIKIAN
Vice-Reitora.
