Documento gerado sob autenticação Nº ZCJ.951.959.LT0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11050                  de  11/12/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias, considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Autorizar o afastamento do País de FLAVIO PECHANSKY, Professor do Magistério Superior, lotado e
em exercício no Departamento de Psiquiatria e Medicina Legal da Faculdade de Medicina, com a finalidade
de  participar  de  reuniões  junto  à  Organização  Mundial  da  Saúde,  em  Genebra,  Suíça,  no  período
compreendido entre 15/12/2019 e 20/12/2019, incluído trânsito, com ônus limitado. Solicitação nº 89194.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
