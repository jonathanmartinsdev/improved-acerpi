Documento gerado sob autenticação Nº PZT.917.500.AVL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3004                  de  06/04/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SABRINA PEREIRA DE ABREU, Professor do Magistério Superior,
lotada e em exercício no Departamento de Letras Clássicas e Vernáculas do Instituto de Letras,  com a
finalidade de  participar  do  "I  Congreso Internacional  de  Lexicología,  Lexicogreafía  y  Terminología",  em
Córdoba, Argentina, no período compreendido entre 02/07/2017 e 06/07/2017, incluído trânsito, com ônus
UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 26773.
RUI VICENTE OPPERMANN
Reitor
