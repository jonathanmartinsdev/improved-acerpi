Documento gerado sob autenticação Nº UWW.462.786.0B3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3604                  de  26/04/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder prorrogação de afastamento parcial,  no período de 31/05/2019 a 30/05/2020, para a
servidora  VIRGINIA DORNELLES  BAUM,  ocupante  do  cargo  de  Técnico  em Assuntos  Educacionais  -
701079, matrícula SIAPE 1755913,  lotada  no  Colégio de Aplicação, para cursar o Doutorado em Educação,
na PUCRS; conforme o Processo nº 23078.516998/2017-18.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
