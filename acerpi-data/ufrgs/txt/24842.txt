Documento gerado sob autenticação Nº QEI.109.555.UON, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5440                  de  20/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ROSANE AZEVEDO NEVES DA SILVA,  matrícula SIAPE n° 1354635, lotada e em exercício no
Departamento de Psicologia Social  e  Institucional  do Instituto de Psicologia,  da classe D  de Professor
Associado, nível 03, para a classe D  de Professor Associado, nível 04, referente ao interstício de 01/07/2014 a
30/06/2016, com vigência financeira a partir  de 18/07/2016, de acordo com o que dispõe a Decisão nº
197/2006-CONSUN, alterada pela Decisão nº 401/2013-CONSUN. Processo nº 23078.504271/2016-15.
CARLOS ALEXANDRE NETTO
Reitor
