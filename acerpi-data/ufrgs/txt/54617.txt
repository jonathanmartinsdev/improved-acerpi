Documento gerado sob autenticação Nº POY.636.007.Q98, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3774                  de  23/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de FLAVIO SANSON FOGLIATTO, Professor do Magistério Superior,
lotado no Departamento de Engenharia de Produção e Transportes da Escola de Engenharia e com exercício
no Programa de Pós-Graduação em Engenharia de Produção, com a finalidade de participar da "Design
Research  Society  Conference",  em  Limerick,  Irlanda,  no  período  compreendido  entre  23/06/2018  e
30/06/2018, incluído trânsito, com ônus limitado. Solicitação nº 34320.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
