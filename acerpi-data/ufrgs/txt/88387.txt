Documento gerado sob autenticação Nº VOR.677.290.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2466                  de  19/03/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  CECILIA  GRAVINA  DA  ROCHA,  matrícula  SIAPE  n°  1978172,  lotada  e  em  exercício  no
Departamento de Engenharia Civil da Escola de Engenharia, da classe C  de Professor Adjunto, nível 03, para
a classe C  de Professor Adjunto, nível 04, referente ao interstício de 13/11/2016 a 12/11/2018, com vigência
financeira a partir de 20/02/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.533944/2018-06.
RUI VICENTE OPPERMANN
Reitor.
