Documento gerado sob autenticação Nº SBA.219.757.88O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6071                  de  15/08/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor MARCO ANTONIO TREVIZANI MARTINS, matrícula SIAPE n° 1774258, lotado e em exercício no
Departamento de Odontologia Conservadora, da classe C  de Professor Adjunto, nível 03, para a classe C  de
Professor Adjunto, nível 04, referente ao interstício de 29/04/2014 a 28/04/2016, com vigência financeira a
partir de 04/08/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela Decisão nº
401/2013-CONSUN. Processo nº 23078.502478/2016-47.
CARLOS ALEXANDRE NETTO
Reitor
