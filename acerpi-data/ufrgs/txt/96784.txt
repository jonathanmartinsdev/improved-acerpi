Documento gerado sob autenticação Nº YCH.133.901.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7665                  de  23/08/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar, a partir de 23 de agosto de 2019,  o ocupante do cargo de Vigilante - 701269, do Nível de
Classificação DII, do Quadro de Pessoal desta Universidade, JULIO CESAR DA SILVEIRA DE SOUZA, matrícula
SIAPE 0991466 da função de Diretor da Divisão de Almoxarifado Central do DPA da PROPLAN, Código SRH
343, Código FG-4, para a qual foi designado pela Portaria nº 2923/2017 de 03/04/2017, publicada no Diário
Oficial  da  União  de  27/03/2018,  por  ter  sido  designado  para  outra  função  gratificada.Processo  nº
23078.519931/2019-05.
RUI VICENTE OPPERMANN
Reitor.
