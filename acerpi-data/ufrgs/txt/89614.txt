Documento gerado sob autenticação Nº UEF.948.333.GQS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3138                  de  10/04/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°46740,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de MÉDICO VETERINÁRIO, do
Quadro de Pessoal desta Universidade, LUCIANO CAVALHEIRO MELO (Siape: 1649735 ),  para substituir  
CARLOS AFONSO DE CASTRO BECK (Siape: 2028820 ), Coordenador da Clínica e Cirurgia de Grandes Animais
do HCV, Código FG-2, em seu afastamento por motivo de férias, no período de 10/04/2019 a 19/04/2019, com
o decorrente pagamento das vantagens por 10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
