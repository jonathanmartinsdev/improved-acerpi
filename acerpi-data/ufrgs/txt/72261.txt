Documento gerado sob autenticação Nº RUK.625.138.KUN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9266                  de  16/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°60723,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do Quadro de Pessoal  desta  Universidade,  LEONARDO DA SILVA PETTENON (Siape:
1860609 ),  para substituir   VERA REGINA DA CUNHA (Siape: 0355713 ), Coordenador do Núcleo de Assuntos
Disciplinares  da  PROGESP,  Código  FG-1,  em  seu  afastamento  no  país,  no  período  de  19/11/2018  a
24/11/2018, com o decorrente pagamento das vantagens por 6 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
