Documento gerado sob autenticação Nº KFS.113.919.B7G, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7633                  de  15/08/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°30208,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MARIA LUIZA CORREA DA CAMARA ROSA (Siape:
4686885 ),   para substituir    ANTONIO PEDRO VIERO (Siape: 0359408 ),  Coordenador da COMGRAD de
Geologia,  Código  FUC,  em  seu  afastamento  no  país,  no  período  de  07/08/2017  a  12/08/2017,  com  o
decorrente pagamento das vantagens por 6 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
