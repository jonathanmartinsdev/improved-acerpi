Documento gerado sob autenticação Nº MNK.656.276.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6548                  de  22/08/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5045297-06.2018.4.04.7100,  da  4ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, do servidor JAIR LUIZ FERNANDES ROLIM , matrícula SIAPE n° 0356787, ativo no cargo de 
Vigilante - 701269, para o nível IV, conforme o Processo nº 23078.521437/2018-11.
RUI VICENTE OPPERMANN
Reitor
