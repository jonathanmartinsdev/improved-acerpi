Documento gerado sob autenticação Nº CWD.500.105.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2430                  de  18/03/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de RICARDO MACHADO XAVIER, Professor do Magistério Superior,
lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a finalidade de
participar  de reunião junto à EULAR,  em Viena,  Áustria,  no período compreendido entre 21/03/2019 e
23/03/2019, incluído trânsito, com ônus limitado. Solicitação nº 72641.
RUI VICENTE OPPERMANN
Reitor
