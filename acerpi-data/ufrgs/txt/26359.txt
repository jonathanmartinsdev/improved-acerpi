Documento gerado sob autenticação Nº BMS.072.161.9TS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6260                  de  19/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  MAURICIO MOREIRA E  SILVA BERNARDES,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Design e Expressão Gráfica da Faculdade de
Arquitetura, com a finalidade de realizar visita ao Illinois Institute of technology, em Chicago, Estados Unidos,
no período compreendido entre 15/09/2016 e 02/10/2016, incluído trânsito, com ônus limitado. Solicitação nº
22330.
CARLOS ALEXANDRE NETTO
Reitor
