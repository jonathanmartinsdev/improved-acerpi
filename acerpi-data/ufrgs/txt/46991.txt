Documento gerado sob autenticação Nº ZUC.495.560.GAJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10356                  de  09/11/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar na Faculdade de Biblioteconomia e Comunicação, com exercício no Setor de Infraestrutura da
FABICO, LUCAN JADOVSKI CARVALHO, nomeado conforme Portaria Nº 8763/2017 de 19 de setembro de
2017, publicada no Diário Oficial da União no dia 25 de setembro de 2017, em efetivo exercício desde 06 de
novembro  de  2017,  ocupante  do  cargo  de  ADMINISTRADOR  DE  EDIFÍCIOS,  Ambiente  Organizacional
Administrativo, classe C, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
