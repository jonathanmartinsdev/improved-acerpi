Documento gerado sob autenticação Nº GPA.728.296.9F0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1895                  de  07/03/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a MAURO
PINHEIRO VIEGAS, matrícula SIAPE nº 0351750, no cargo de Auxiliar de Agropecuária, nível de classificação B,
nível de capacitação I, padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho, com exercício na Clínica de Grandes Animais do Hospital de Clínicas Veterinárias, com proventos
integrais. Processo 23078.000426/2018-01.
RUI VICENTE OPPERMANN
Reitor.
