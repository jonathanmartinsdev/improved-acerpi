Documento gerado sob autenticação Nº RRD.392.110.99N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7399                  de  19/09/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar na Faculdade de Farmácia, com exercício no Setor de Toxicologia, ALINE DE LIMA NOGARE,
nomeada conforme Portaria Nº 2538/2018 de 07 de abril de 2018, publicada no Diário Oficial da União no dia
10 de abril de 2018, em efetivo exercício desde 18 de setembro de 2018, ocupante do cargo de Farmacêutico,
Ambiente Organizacional  Ciências da Saúde,  classe E,  nível  I,  padrão 101,  no Quadro de Pessoal  desta
Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
