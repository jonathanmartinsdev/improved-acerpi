Documento gerado sob autenticação Nº KGL.979.080.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8612                  de  23/09/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Declarar que a aposentadoria concedida a ALICE MARIA DE BONA, matrícula SIAPE nº 0351032,
através da portaria  nº  1516,  de 10 de junho de 2002,  publicada no Diário Oficial  da União do dia 17
subsequente, passa a ser no cargo de Professor do Ensino Básico, Técnico e Tecnológico, classe D IV, nível 4,
conforme  determinação  judicial  contida  no  processo  nº  5056007-22.2017.4.04.7100.  Processo
23078.524345/2019-74.
RUI VICENTE OPPERMANN
Reitor.
