Documento gerado sob autenticação Nº SRY.350.761.1HO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             104                  de  04/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°40136,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de LOCUTOR, do Quadro de Pessoal
desta Universidade, CLAUDIA MARIA ROCCA RIBEIRO (Siape: 6358722 ),  para substituir   LUIZ SPEROTTO
TEIXEIRA  (Siape:  0357115  ),  Diretor  Técnico  do  Centro  de  Teledifusão  Educativa,  Código  FG-1,  em seu
afastamento por motivo de férias, no período de 08/01/2018 a 01/02/2018, com o decorrente pagamento das
vantagens por 25 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
