Documento gerado sob autenticação Nº DZM.114.566.NMN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4152                  de  14/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Homologar  o  parecer  que  aprova  o  servidor  técnico-administrativo  VERIDIANO  KOEFFENDER
MOREIRA,  ocupante do cargo de Técnico em Assuntos Educacionais, no estágio probatório cumprido no
período de 12/05/2016 até 12/05/2019, fazendo jus, a partir desta data, à estabilidade no serviço público
federal.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
