Documento gerado sob autenticação Nº YVQ.511.081.E6Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8241                  de  14/10/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°28439,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ANALISTA DE TECNOLOGIA DA
INFORMAÇÃO, do Quadro de Pessoal desta Universidade, ALCIDES VAZ DA SILVA (Siape: 1676200 ),  para
substituir   MARCIA CARLOTTO (Siape: 0356945 ), Diretor da Central de Serviços de Tecnologia da Informação,
vinculada ao CPD, Código CD-4, em seu afastamento por motivo de férias, no período de 05/10/2016 a
11/10/2016, com o decorrente pagamento das vantagens por 7 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
