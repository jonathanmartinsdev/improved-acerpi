Documento gerado sob autenticação Nº RVQ.982.272.UDF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9345                  de  15/10/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Designar temporariamente, nos termos do § 3° do artigo 92 do Estatuto da Universidade Federal do
Rio Grande do Sul, a ocupante do cargo de Professor do Magistério Superior, do Quadro de Pessoal desta
Universidade, ROSA MARIA CASTILHOS FERNANDES, matrícula SIAPE 2926491, para substituir JUSSARA MARIA
ROSA MENDES, matrícula SIAPE 1768485, Coordenadora do PPG em Política Social e Serviço Social, Código
SRH 1490, código FUC, no período de 15/10/2019 até 19/10/2019, com o pagamento das vantagens por
5 dias. Processo nº 23078.528165/2019-61.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
