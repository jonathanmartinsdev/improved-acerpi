Documento gerado sob autenticação Nº HAP.250.760.DJC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6482                  de  19/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Declarar vago, a partir de 27 de junho de 2017, o Cargo de Assistente em Administração, Código
701200, Nível de Classificação D, Nível de Capacitação I, Padrão 01, do Quadro de Pessoal, em decorrência de
posse  em  outro  cargo  inacumulável,  de  EWERTON  BREGALDA  com  lotação  na  Superintendência  de
Infraestrutura. Processo nº 23078.012297/2017-12.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
