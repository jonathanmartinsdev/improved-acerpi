Documento gerado sob autenticação Nº QSK.364.898.LME, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10779                  de  03/12/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar, a partir de 11/11/2019, a ocupante do cargo de Técnico em Secretariado - 701275, do
Nível  de Classificação DIII,  do Quadro de Pessoal  desta Universidade,  RONISE FERREIRA DOS SANTOS,
matrícula  SIAPE 1125011,  da função de Coordenadora do Núcleo de Infraestrutura e  Espaço Físico da
Gerência Administrativa do Instituto de Artes, Código SRH 344, Código FG-4, para a qual foi designada pela
Portaria nº 6968/2017, de 01/08/2017, publicada no Diário Oficial  da União de 04/08/2017. Processo nº
23078.529741/2018-15.
RUI VICENTE OPPERMANN
Reitor.
