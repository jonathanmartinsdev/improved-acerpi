Documento gerado sob autenticação Nº RXW.576.968.P0T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2404                  de  17/03/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, PEDRO WALMSLEY FREJLICH, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 15/2017 de 23 de Fevereiro de 2017 homologado em 24 de
fevereiro de 2017 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei
12.772,  de 28 de dezembro de 2012,  com redação dada pela Lei  12.863,  de 24 de setembro de 2013,
publicada  no  Diário  Oficial  da  União  de  25  de  setembro  de  2013,  para  o  cargo  de  PROFESSOR  DO
MAGISTÉRIO SUPERIOR do Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor
Adjunto A, Nível I,  do Quadro de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação
Exclusiva), junto ao Departamento Interdisciplinar do Campus Litoral Norte, em vaga decorrente de Criação
de Cargos,  código nº  928826,  ocorrida em 10 de baril  de 2014,  conforme  PORTARIA Nº  321/2014 de
09/04/2014, publicada no Diário Oficial da União de 10/04/2014. Processo nº. 23078.508532/2016-68.
RUI VICENTE OPPERMANN
Reitor.
