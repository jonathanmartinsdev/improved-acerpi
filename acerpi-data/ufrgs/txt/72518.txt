Documento gerado sob autenticação Nº DDM.402.537.96D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9508                  de  23/11/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5008438-59.2016.4.04.7100,  da  8ª  Vara
Federal de Porto Alegre,  a Portaria n° 5005/2015, de 25/06/2015 que concedeu progressão funcional, por
avaliação, no Quadro desta Universidade, à Professora SIMONE CRISTINA BAGGIO GNOATTO,  matrícula
SIAPE 2342894, com exercício no Departamento de Produção de Matéria Prima da Faculdade de Farmácia da
classe C de Professor Adjunto, nível 03, para a classe C de Professor Adjunto, nível 04, referente ao interstício
de  19/03/2013  a  18/03/2015,  com  vigência  financeira  a  partir  de  23  de  junho  de  2015.  Processo  nº
23078.008269/2015-21.
 
 
Onde se lê:
...com vigência financeira a partir de 23 de junho de 2015...
 
Leia-se:
...com vigência financeira a partir de 18 de março de 2015...,  ficando ratificados os demais
termos.
RUI VICENTE OPPERMANN
Reitor.
