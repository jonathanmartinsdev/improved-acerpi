Documento gerado sob autenticação Nº XUY.079.086.T0G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             133                  de  04/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°53502,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal  desta Universidade,  VINICIUS FERRO (Siape:  1157295 ),   para
substituir   ELIANA VENTORINI (Siape: 1688979 ), Coordenador da Coordenadoria de Concursos, Mobilidade e
Acompanhamento, vinc. ao DDGP da PROGESP, Código CD-4, em seu afastamento por motivo de Laudo
Médico do titular da Função, no período de 12/12/2018 a 14/12/2018, com o decorrente pagamento das
vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
