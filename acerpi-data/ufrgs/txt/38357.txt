Documento gerado sob autenticação Nº POB.464.316.I0C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4331                  de  17/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 10/05/2017, a ocupante do cargo de Assistente em Administração - 701200,
do Nível de Classificação DII, do Quadro de Pessoal desta Universidade, BRUNA FIGUEIREDO RIEDIGER,
matrícula  SIAPE  2260046,  da  função  de  Coordenadora  do  Núcleo  de  Recursos  Humanos  da  Gerência
Administrativa da Faculdade de Medicina, Código SRH 557, Código FG-7, para a qual foi designada pela
Portaria nº  1997/2017 de 03/03/2017,  publicada no Diário Oficial  da União de 06/03/2017.  Processo nº
23078.007751/2017-13.
JANE FRAGA TUTIKIAN
Vice-Reitora.
