Documento gerado sob autenticação Nº HWN.175.959.BPM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             116                  de  04/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°52859,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ENGENHEIRO-ÁREA, do Quadro
de Pessoal desta Universidade, HENRIQUE GOMES DOS SANTOS (Siape: 2000029 ),  para substituir   ANDRÉ
RODRIGUES DE RODRIGUES (Siape:  2309825 ),  Chefe do Setor  de Oficinas de Produção e Manutenção
Mecânica - SUINFRA, Código FG-2, em seu afastamento por motivo de férias, no período de 06/01/2020 a
10/01/2020, com o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
