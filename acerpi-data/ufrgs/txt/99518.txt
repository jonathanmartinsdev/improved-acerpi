Documento gerado sob autenticação Nº KMQ.371.469.731, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9312                  de  14/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de DANIEL CANAVESE DE OLIVEIRA,  Professor do Magistério
Superior,  lotado e em exercício no Departamento de Saúde Coletiva da Escola de Enfermagem, com a
finalidade de participar do "SVRI Forum 2019", na Cidade do Cabo, África do Sul, no período compreendido
entre 20/10/2019 e 26/10/2019, incluído trânsito, com ônus CAPES/PROAP. Solicitação nº 85483.
RUI VICENTE OPPERMANN
Reitor
