Documento gerado sob autenticação Nº LRP.988.303.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8619                  de  23/09/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5056285-52.2019.4.04.7100,  da  6ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, da servidora CIRILA DOS SANTOS FERREIRA DA CRUZ, matrícula SIAPE n° 0357556, ativa no
cargo de Recepcionista - 701459, do nível II para o nível IV, a contar de 01/01/2006, conforme o Processo nº
23078.525472/2019-91.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
RUI VICENTE OPPERMANN
Reitor
