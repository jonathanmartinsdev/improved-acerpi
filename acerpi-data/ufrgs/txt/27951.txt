Documento gerado sob autenticação Nº ZHL.660.829.0SK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7458                  de  22/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder afastamento parcial,  no período de 26/09/2016 a 31/12/2016, para a servidora LIGIA
ANTONELA DA SILVA PETRUCCI,  ocupante  do cargo de  Produtor  Cultural  -  701061,  matrícula  SIAPE
0357582,  lotada  na  Pró-Reitoria de Extensão, para cursar o Mestrado em Artes Cênicas, conforme o
Processo nº 23078.018765/2016-73.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
