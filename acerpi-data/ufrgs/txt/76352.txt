Documento gerado sob autenticação Nº LAB.363.110.065, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1312                  de  05/02/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Declarar vaga, a partir de 01/02/2019, a função de Gerente Administrativa da FCE, Código SRH
1392, Código FG-1, desta Universidade, tendo em vista a aposentadoria de KATIA REZER MENGER, matrícula
SIAPE n° 0356562, conforme Portaria n° 877/2019, de 25 de janeiro de 2019, publicada no Diário Oficial da
União do dia 01/02/2019. Processo nº 23078.502655/2019-38.
 
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
