Documento gerado sob autenticação Nº NEC.244.891.ON0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10639                  de  22/11/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de Professor do Magistério
Superior,  do Quadro de Pessoal desta Universidade, CRISTIANE BAUERMANN LEITAO (Siape: 1293127),
 para substituir automaticamente TICIANA DA COSTA RODRIGUES (Siape: 1278403 ), Coordenadora do PPG
em Ciências Médicas: Endocrinologia, Código SRH 1149, FUCC, em sua Licença Adotante, no período de
17/10/2017  a  14/11/2017,  com  o  decorrente  pagamento  das  vantagens  por  29  dias.  Processo  SEI  nº
23078.518999/2017-05
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
