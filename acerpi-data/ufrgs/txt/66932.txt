Documento gerado sob autenticação Nº RIR.035.711.J7C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5508                  de  24/07/2018
  D e s i g n a  r e p r e s e n t a n t e s  d e s t a
Universidade  no  âmbito  do  Fórum
Estadual  Permanente  de  Apoio  à
Formação  Docente  dos  Profissionais  do
Magistério  da  Educação  Básica  do  Rio
Grande do Sul - FEPAD-RS.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
            Art. 1º Designar os Professores MARCUS VINICIUS DE AZEVEDO BASSO e ROSANE ARAGON,
respectivamente como titular e suplente, para comporem a representação desta Universidade, no âmbito do
Fórum Estadual Permanente de Apoio à Formação Docente dos Profissionais do Magistério da Educação
Básica do Rio Grande do Sul - FEPAD-RS, a partir da presente data.
 
               Art. 2º Revogar a Portaria nº 2259 de 14 de março de 2017.
RUI VICENTE OPPERMANN,
Reitor.
