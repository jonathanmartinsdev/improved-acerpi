Documento gerado sob autenticação Nº LQX.695.831.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5781                  de  10/07/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Autorizar o afastamento no País de ALCIDES VIEIRA COSTA, ocupante do cargo de Professor do
Magistério Superior,  lotado e em exercício no Departamento de Educação Física, Fisioterapia e Dança da
Escola de Educação Física, Fisioterapia e Dança,  com a finalidade de participar de encontros e curso junto à
empresa Guajiru Kite Center, em Guajiru, Brasil, no período compreendido entre 13/07/2019 e 31/07/2019,
incluído trânsito, com ônus limitado. Solicitação n° 85401.
RUI VICENTE OPPERMANN
Reitor.
