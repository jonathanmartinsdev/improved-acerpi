Documento gerado sob autenticação Nº FJY.044.655.T0H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7395                  de  15/08/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir da data deste ato, como segue:
 
Transformar: Coordenador de Apoio Acadêmico da Gerência Administrativa do IFCH, Código SRH
1433, FG-4,  em Coordenador do Núcleo Acadêmico Administrativo de Graduação do IFCH, Código SRH 1433,
FG-4.  
 
Processo nº 23078.521622/2019-97.
 
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
