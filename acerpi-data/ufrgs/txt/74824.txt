Documento gerado sob autenticação Nº NQT.408.541.6MP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             380                  de  11/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, RITA DO CARMO FERREIRA LAIPELT, matrícula SIAPE n° 2714174, lotada no
Departamento de Ciência da Informação da Faculdade de Biblioteconomia e Comunicação, para exercer a
função de Coordenadora da COMGRAD de Biblioteconomia, Código SRH 1199, código FUC, com vigência a
partir de 27/01/2019 até 26/01/2021, por ter sido reeleita. Processo nº 23078.500162/2019-63.
JANE FRAGA TUTIKIAN
Vice-Reitora.
