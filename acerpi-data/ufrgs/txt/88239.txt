Documento gerado sob autenticação Nº LEB.936.391.J36, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2353                  de  14/03/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar, a partir de 15/03/2019, o ocupante do cargo de Assistente em Administração - 701200,
do Nível de Classificação DII, do Quadro de Pessoal desta Universidade, OTÁVIO LUÍS DA SILVA BARRADAS,
matrícula SIAPE 2317068, da função de Chefe da Divisão Acadêmica da Gerência Administrativa do Instituto
de Biociências,  Código SRH 608, Código FG-7,  para a qual foi  designado pela Portaria nº 8614/2018 de
25/10/2018, publicada no Diário Oficial da União de 26/10/2018. Processo nº 23078.505846/2019-51.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
