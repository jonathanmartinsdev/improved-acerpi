Documento gerado sob autenticação Nº ENI.418.877.PS9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3760                  de  20/05/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MARCELO  TEIXEIRA  PACHECO,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Plantas de Lavoura da Faculdade de Agronomia, com a
finalidade de participar da "10th International Oat Conference", em São Petersburgo, Rússia, no período
compreendido entre 09/07/2016 e 18/07/2016, incluído trânsito, com ônus UFRGS (Faculdade de Agronomia -
diárias e passagens). Solicitação nº 19630.
CARLOS ALEXANDRE NETTO
Reitor
