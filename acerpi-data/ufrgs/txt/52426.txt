Documento gerado sob autenticação Nº JGE.628.656.B9C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2321                  de  27/03/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  17/03/2018,   referente  ao  interstício  de
17/09/2016 a 16/03/2018, para a servidora CAREN JOSEANA HENZ, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2217368,  lotada  no  Instituto de Informática, passando do Nível
de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.505159/2018-55:
Formação Integral de Servidores da UFRGS III  CH: 72 Carga horária utilizada: 61 hora(s) / Carga horária
excedente: 11 hora(s) (06/04/2017 a 30/11/2017)
ILB - Ética e Administração Pública CH: 24 (16/09/2015 a 15/10/2015)
ILB - Direito Administrativo para gerentes no setor público CH: 35 (15/10/2015 a 03/12/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
