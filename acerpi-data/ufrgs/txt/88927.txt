Documento gerado sob autenticação Nº VDJ.750.512.LHO, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2729                  de  28/03/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  CAMILO  DA  ROSA  SIMÕES,  matrícula  SIAPE  n°  2276997,  lotado  no
Departamento de Música do Instituto de Artes, para exercer a função de Chefe do Depto de Música do
Instituto  de  Artes,  Código  SRH  137,  código  FG-1,  com  vigência  a  partir  de  29/04/2019  até
28/04/2021.  Processo  nº  23078.507700/2019-41.
JANE FRAGA TUTIKIAN
Vice-Reitora.
