Documento gerado sob autenticação Nº DYW.817.798.POG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5241                  de  21/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar  CAMILA  DE  MEDEIROS  PADILHA,  Matrícula  SIAPE  2397771,  ocupante  do  cargo  de
Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a
função de Diretora da Divisão de Análise Funcional do DAP da PROGESP, Código SRH 1373, Código FG-1, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.516042/2019-88.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
