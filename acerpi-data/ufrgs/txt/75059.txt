Documento gerado sob autenticação Nº PQK.139.967.0IL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             457                  de  15/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder ao servidor LUIZ DANIEL RODRIGUES DINARTE,  ocupante do cargo de  Tradutor e
Intérprete de Linguagem de Sinais - 701266, lotado na Pró-Reitoria de Gestão de Pessoas, SIAPE 2060235, o
percentual de 75% (setenta e cinco por cento) de Incentivo à Qualificação, a contar de 03/12/2018, tendo em
vista a conclusão do curso de Doutorado em Educação, conforme o Processo nº 23078.533440/2018-88.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
