Documento gerado sob autenticação Nº PTY.521.283.H3N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4826                  de  01/07/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Nomear, em caráter efetivo, CAROLINE PIETA DIAS, em virtude de habilitação em Concurso Público
de Provas e Títulos, conforme Edital Nº 53/2016 de 3 de Junho de 2016, homologado em 06 de junho de 2016
e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de 28 de
dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do
Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro
de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento
de Educação Física da Escola de Educação Física, Fisioterapia e Dança, em vaga decorrente de posse em outro
cargo inacumulável do Professor Alvaro Reischak de Oliveira, código nº 274177, ocorrida em 18 de Outubro
de 2012, conforme Portaria nº.6327 de 01 de novembro de 2012, publicada no Diário Oficial da União de 06
de novembro de 2012. Processo nº. 23078.000067/2015-31.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
