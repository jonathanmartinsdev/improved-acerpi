Documento gerado sob autenticação Nº FBZ.097.227.1EA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8936                  de  25/09/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder ao ocupante do cargo de Professor do Magistério Superior,  FLAVIO VASCONCELLOS
COMIM, lotado e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de
Ciências Econômicas, Licença para o Trato de Assuntos Particulares, a partir de 02 de outubro de 2017, pelo
prazo de três anos, nos termos do artigo 91 da Lei n° 8.112, de 11 de dezembro de 1990, alterado pela
Medida Provisória n° 2.225-45, de 4 de setembro de 2001, e conforme previsto no artigo 3°, inciso II, da
Portaria SEGRT/MP n° 35, de 01 de março de 2016. Processo n° 23078.017454/2017-78.
RUI VICENTE OPPERMANN
Reitor.
