Documento gerado sob autenticação Nº ACV.414.652.U5Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6785                  de  30/08/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.026518/12-73  e
23078.510320/2018-11, do Contrato nº 022/2014, da Lei 10.520/02 e ainda da Lei 8.666/93,
RESOLVE:
 
Aplicar  à  Empresa  LINCE  SEGURANÇA  PATRIMONIAL  LTDA,  CNPJ  n.º  10.364.152/0002-08,  conforme
atestado pela GERTE no Ofício 772/2018 (doc. SEI nº 0988665) e pelo NUDECON, no doc. SEI nº 1143851, do
Processo 23078.510320/2018-11, as seguintes sanções administrativas:
 
1. ADVERTÊNCIA, prevista na alínea "a" c/c o parágrafo terceiro da cláusula décima segunda do
contrato, pelo descumprimento quanto à irregularidade do vínculo empregatício (1ª ocorrência);
2.  MULTA,  no montante de R$ 1.535,23 (Mil,  quinhentos e  trinta e  cinco reais  e  vinte e  três
centavos),  conforme demonstrativo de cálculo  (doc.  SEI  nº  1171844),  prevista  na alínea 'b'  c/c
parágrafo 3º da Cláusula décima-segunda, pelos descumprimentos: falta de uso de uniformes/EPIs;
falta  de  documentação  (carta  de  apresentação);  não  cumprimento  de  carga  horária;  falta  de
cobertura de postos; falta de equipamentos; falta de disponibilização de documentação; falta de
metodologia  de  trabalho  e  execução  insatisfatória  de  atividades  específicas;  irregularidade  de
atividades específicas; por não se tratarem de 1ª ocorrência.
 
 
Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
