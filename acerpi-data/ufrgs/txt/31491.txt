Documento gerado sob autenticação Nº KAS.568.393.LAR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9943                  de  15/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°31107,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MARIO REIS ALVARES DA SILVA (Siape: 1253587 ),
 para substituir   CARLOS FERNANDO DE MAGALHAES FRANCISCONI (Siape: 0351529 ), Coordenador do PPG:
Ciências  em Gastroenterologia,  Código FUC,  em seu afastamento por  motivo de férias,  no período de
20/12/2016 a 29/12/2016, com o decorrente pagamento das vantagens por 10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
