Documento gerado sob autenticação Nº LAP.186.636.4E2, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2219                  de  23/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE:
Nomear, em caráter efetivo, no cargo abaixo, na vaga referente ao respectivo código, em virtude de
habilitação em Concurso Público, homologado em 26 de julho de 2013 e de acordo com o artigo 9º, item I, da
Lei nº. 8.112, de 11 de dezembro de 1990, com as alterações dadas pela Lei nº. 11.091, de 12 de janeiro de
2005, publicada no Diário Oficial da União de 13 de janeiro de 2005, Decreto nº. 7.232, de 19 de julho de
2010, publicado no Diário Oficial da União de 20 de julho de 2010, Lei 12.772, de 28 de dezembro de 2012,
publicada no Diário Oficial da União de 31 de dezembro de 2012. Processo nº. 23078.020126/13-81.
Cargo 34 - Auxiliar em Administração - Classe C Padrão I
DANIEL PIRES MARTINS - Vaga SIAPE 275502
Processo nº. 23078.020126/13-81.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
