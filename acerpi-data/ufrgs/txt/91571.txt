Documento gerado sob autenticação Nº BFD.240.708.05D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4339                  de  20/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do País  de JULIO CELSO BORELLO VARGAS,  Professor do Magistério
Superior,  lotado e  em exercício  no  Departamento  de  Urbanismo da  Faculdade de  Arquitetura,  com a
finalidade de participar de reunião e conferência junto à Oxford Brookes University, em Oxford, Inglaterra, no
período compreendido entre 08/06/2019 e 16/06/2019, incluído trânsito, com ônus limitado. Solicitação nº
83149.
RUI VICENTE OPPERMANN
Reitor
