Documento gerado sob autenticação Nº ZIO.628.351.CLG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7764                  de  27/08/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, FERNANDO ERTHAL, matrícula SIAPE n° 2328398, lotado no Departamento de
Paleontologia e Estratigrafia do Instituto de Geociências, como Diretor Substituto do Centro de Investigação
do Gondwana,  para  substituir  automaticamente  o  titular  desta  função a  partir  da  data  deste  ato  até
17/07/2021. Processo nº 23078.517954/2019-77.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
