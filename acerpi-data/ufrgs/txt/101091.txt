Documento gerado sob autenticação Nº ZYW.642.442.6DN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10387                  de  18/11/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor DENNIS RUSSOWSKY, matrícula SIAPE n° 1216910, lotado e em exercício no Departamento de
Química Orgânica do Instituto de Química, da classe C   de Professor Adjunto, nível 03, para a classe   C de
Professor Adjunto, nível 04, referente ao interstício de 01/01/2004 a 31/12/2007, com vigência financeira a
partir de 01/01/2008, conforme decisão judicial proferida no processo nº 5054491-30.2018.4.04.7100, da 1ª
Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.529123/2019-48.
JANE FRAGA TUTIKIAN
Vice-Reitora.
