Documento gerado sob autenticação Nº SQA.113.077.Q33, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1626                  de  19/02/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARCIA MARIA A NACHENVENG P MARGIS,  Professor do
Magistério Superior, lotada e em exercício no Departamento de Genética do Instituto de Biociências, com a
finalidade de atuar como Professor Visitante junto à Carnegie Institution For Science, em Stanford, Estados
Unidos,  no  período  de  01/03/2020  a  31/08/2020,  com  ônus  CAPES/PRINT/UFRGS.  Processo  nº
23078.501052/2020-52.
RUI VICENTE OPPERMANN
Reitor.
