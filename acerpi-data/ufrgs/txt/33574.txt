Documento gerado sob autenticação Nº AQH.594.865.OMJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             897                  de  26/01/2017
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Tornar sem efeito, a partir de 12 de janeiro de 2017,a Portaria nº 347/2017, de 12/01/2017, que
nomeou representantes discentes da APG no CONSUN (Conselho Universitário).
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
