Documento gerado sob autenticação Nº MPG.204.329.9VR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5060                  de  08/06/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 31 de maio de 2017,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
CAROLINA  LEONARDI  DE  OLIVEIRA,  ocupante  do  cargo  de  Assistente  em  Administração,  Ambiente
Organizacional Administrativo, Código 701200, Classe D, Nível de Capacitação II, Padrão de Vencimento 03,
SIAPE nº. 2059985 da Pró-Reitoria de Gestão de Pessoas para a lotação Faculdade de Educação, com novo
exercício no Núcleo de Apoio Administrativo e Departamental da Gerência Administrativa da Faculdade de
Educação.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
