Documento gerado sob autenticação Nº NVD.421.620.ACD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8112                  de  05/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5051803-61.2019.4.04.7100,  da  6ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo II da Portaria nº 1874, de
13/07/2006, da servidora LUZIA VALDETE GOULART KOEHLER, matrícula SIAPE n° 0353776, aposentada no
cargo de Bibliotecário-documentalista - 701010, do nível I para o nível II, a contar de 01/01/2006, conforme o
Processo nº 23078.523697/2019-11.
 
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
