Documento gerado sob autenticação Nº TCK.827.248.1VS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8515                  de  23/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 30 (trinta) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de 11
de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
ERISSANDRA GOMES, com exercício no Departamento de Cirurgia e Ortopedia da Faculdade de Odontologia,
a  ser  usufruída  no  período  de  01/02/2019  a  02/03/2019,  referente  ao  quinquênio  de  09/02/2009  a
08/02/2014,  a  fim  de  participar  de  capacitação  no  Grupo  de  Pesquisa  Patofisiologia  do  Sistema
Estomatognático, oferecida pelo Departamento de Fonoaudiologia da Faculdade de Pernambuco; conforme
Processo nº 23078.526242/2018-68.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
