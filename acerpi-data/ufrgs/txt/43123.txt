Documento gerado sob autenticação Nº VUE.457.786.EEC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7809                  de  21/08/2017
Reinstauração de Sindicância Investigativa da
UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Reinstaurar Sindicância Investigativa a ser processada pela Comissão integrada pelas servidoras 
ISABEL DE ABRANTES TIMM, ocupante do cargo de Assistente em Administração, lotada na Pró-Reitoria de
Assuntos Estudantis e com exercício na Gerência Administrativa da PRAE, CAROLINA TAGLIANI RIBEIRO,
ocupante do cargo de Engenheiro-área, lotada na Pró-Reitoria de Assuntos Estudantis e com exercício na
Gerência Administrativa da PRAE, para sob a Presidência da primeira, no prazo de trinta dias, concluir os
trabalhos no processo nº 23078.011955/2017-41
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
