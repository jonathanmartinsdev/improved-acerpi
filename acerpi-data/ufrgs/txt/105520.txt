Documento gerado sob autenticação Nº TZR.328.087.IRF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1847                  de  05/03/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  17/02/2020,   referente  ao  interstício  de
16/08/2018 a 16/02/2020, para a servidora ROCHELE RESENDE PORTO, ocupante do cargo de Técnico em
Assuntos Educacionais - 701079, matrícula SIAPE 1876383,  lotada  na  Pró-Reitoria de Extensão, passando
do Nível de Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.503255/2020-83:
Formação Integral de Servidores da UFRGS III CH: 62 (28/09/2018 a 13/11/2019)
ILB -  Conhecendo o novo acordo ortográfico CH: 20 Carga horária utilizada: 18 hora(s)  /  Carga horária
excedente: 2 hora(s) (26/01/2020 a 15/02/2020)
ILB - ÉTICA E ADMINISTRAÇÃO PÚBLICA CH: 40 (08/01/2020 a 28/01/2020)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
