Documento gerado sob autenticação Nº NYM.384.072.3FK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9266                  de  21/11/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar GUILHERME FRANCISCO WATERLOO RADOMSKY, matrícula SIAPE n° 1841720, ocupante
do cargo de Professor do Magistério Superior, classe Adjunto, lotado no Departamento de Sociologia do
Instituto de Filosofia e Ciências Humanas, do Quadro de Pessoal da Universidade, para exercer a função de
Coordenador Substituto do PPG em Sociologia, com vigência a partir da data deste ato até 20/05/2017, a fim
de completar  o  mandato  de  CINARA LERRER ROSENFIELD,  conforme artigo  92  do Estatuto  da  mesma
Universidade. Processo nº 23078.203928/2016-11.
JANE FRAGA TUTIKIAN
Vice-Reitora
