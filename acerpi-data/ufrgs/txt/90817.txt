Documento gerado sob autenticação Nº IVZ.655.571.HFM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3948                  de  09/05/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5007126-43.2019.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, do servidor MIGUEL MACHADO DIAS, matrícula SIAPE n° 0356294, ativo no cargo de Técnico em
Restauração  -  701260,  do  Nível  I  para  o  Nível  IV,  a  contar  de  01/01/2006,  conforme  o  Processo  nº
23078.510610/2019-37.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
RUI VICENTE OPPERMANN
Reitor
