Documento gerado sob autenticação Nº IWS.631.125.SCK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1234                  de  06/02/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°50198,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, DARIO FREDERICO PASCHE (Siape: 1371723 ),  para
substituir   KARINA DE OLIVEIRA AZZOLIN (Siape: 1900234 ), Coordenador da Comissão de Pesquisa da Escola
de Enfermagem, em seu afastamento por motivo de férias, no período de 06/02/2020 a 16/02/2020.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
