Documento gerado sob autenticação Nº IGK.971.670.965, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10103                  de  13/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ANDRE INACIO REIS,  matrícula SIAPE n° 2291203, lotado e em exercício no Departamento de
Informática Aplicada, da classe D  de Professor Associado, nível 04, para a classe E  de Professor Titular, 
referente ao interstício de 11/07/2016 a 27/11/2018, com vigência financeira a partir de 28/11/2018, de
acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº
331/2017 do CONSUN. Processo nº 23078.533987/2018-83.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
