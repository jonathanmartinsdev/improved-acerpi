Documento gerado sob autenticação Nº TEJ.752.621.P7M, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2827                  de  01/04/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/03/2019,   referente  ao  interstício  de
16/10/2015 a 25/03/2019, para o servidor HELIER BALBINOTTI DA SILVEIRA, ocupante do cargo de Técnico
de Laboratório Área - 701244, matrícula SIAPE 1912073,  lotado  no  Instituto de Ciências Básicas da Saúde,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.505793/2019-79:
Formação Integral de Servidores da UFRGS III CH: 70 (02/06/2015 a 26/03/2019)
ILB - Conhecendo o novo acordo ortográfico CH: 20 (25/02/2019 a 12/03/2019)
USP - Capacitação no Uso e Manejo de Animais de Laboratório CH: 60 (30/10/2018 a 16/01/2019)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
