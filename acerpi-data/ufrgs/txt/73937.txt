Documento gerado sob autenticação Nº JLJ.762.697.QQS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10375                  de  21/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°41680,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ANALISTA DE TECNOLOGIA DA
INFORMAÇÃO, do Quadro de Pessoal desta Universidade, RAFAEL SILVEIRA MACHADO (Siape: 2777454 ),
 para substituir   FABIANO RIOS HECK (Siape: 1677900 ),  Diretor do Depto de Sistemas de Informação
vinculado ao CPD, Código CD-4, em seu afastamento por motivo de férias, no período de 26/12/2018 a
03/01/2019, com o decorrente pagamento das vantagens por 9 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
