Documento gerado sob autenticação Nº GLF.697.171.T86, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8363                  de  18/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder pensão , a partir de 25 de outubro de 2017, a MARIEN ISABEL CHIES, nos termos dos
artigos 215 e 217, inciso II, da Lei n.º 8.112, de 11 de dezembro de 1990, combinado com o artigo 40 da
Constituição Federal de 1988, alterado pela Emenda Constitucional 41 de 2003, regulamentado pela Lei nº
10.887/2004, artigo 2º, I, em decorrência do falecimento deGUSTAVO BAPTISTA EBOLI, matrícula SIAPE n°
0358527,  aposentado  no  cargo  de  Professor  do  Magistério  Superior  do  quadro  de  pessoal  desta
Universidade. Processo n.º 23078.022149/2017-06.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
