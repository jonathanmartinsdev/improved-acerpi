Documento gerado sob autenticação Nº MQG.623.961.H65, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1984                  de  15/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°24088,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA, do Quadro de Pessoal desta Universidade, LETICIA MOREIRA (Siape: 1681003 ),  para substituir  
BRUNAH  DE  CASTRO  BRASIL  (Siape:  1737378  ),  Coordenador  do  Núcleo  Especializado  da  Gerência
Administrativa da Faculdade de Odontologia, Código FG-7, em seu afastamento por motivo de férias, no
período de 19/03/2016 a 03/04/2016, com o decorrente pagamento das vantagens por 16 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
