Documento gerado sob autenticação Nº WFR.234.432.FCL, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2678                  de  12/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar  o  ocupante  do  cargo  de  Professor  do  Magistério  Superior,  classe  Adjunto  A,  desta
Universidade, CARLOS AUGUSTO BONIFACIO LEITE, CPF n° 1440318689, matrícula SIAPE n° 2890389, lotado
no  Departamento  de  Letras  Clássicas  e  Vernáculas  do  Instituto  de  Letras,  para  exercer  a  função  de
Coordenador da Comissão de Extensão do Instituto de Letras, Código SRH 1051, com vigência a partir da data
deste ato, pelo período de 02 anos, ficando o pagamento de gratificação condicionado à disponibilidade de
uma função gratificada. Processo nº 23078.201633/2015-20.
RUI VICENTE OPPERMANN
Vice-Reitor
