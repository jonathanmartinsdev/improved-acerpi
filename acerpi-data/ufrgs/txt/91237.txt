Documento gerado sob autenticação Nº SNE.720.968.ONH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4115                  de  14/05/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°84213,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, ARAO DA SILVA MORAES (Siape: 0351279 ),
 para substituir   VLADIMIR PINHEIRO DO NASCIMENTO (Siape: 1150018 ), Pró-Reitor de Graduação, Código
CD-2, em seu afastamento no país, no período de 14/05/2019 a 16/05/2019, com o decorrente pagamento
das vantagens por 3 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
