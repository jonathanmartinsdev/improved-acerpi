Documento gerado sob autenticação Nº VYU.593.824.S01, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2985                  de  22/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°29632,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de AUXILIAR EM ADMINISTRAÇÃO,
do Quadro de Pessoal desta Universidade, ALVARO MARQUES DE AGUIAR (Siape: 1068346 ),  para substituir  
VALQUIRIA  NUNES  PERUFO  (Siape:  1757725  ),  Chefe  da  Seção  de  Pagamento  da  Despesa/Div  Exec
Fin/DCF/PROPLAN, Código FG-5, em seu afastamento por motivo de férias, no período de 11/04/2016 a
22/04/2016, com o decorrente pagamento das vantagens por 12 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
