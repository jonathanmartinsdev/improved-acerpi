Documento gerado sob autenticação Nº FXS.337.875.77H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2730                  de  29/03/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7633, de 29 de
setembro de 2016  e tendo em vista o que consta do Processo Administrativo n° 23078.200542/13-06, do
Pregão Eletrônico nº 041/2015, Lei 10.520/02 e ainda da Lei 8.666/93,
RESOLVE:
 
Aplicar a sanção administrativa de MULTA 10% (dez por cento) sobre o valor da parcela que lhe deu causa
(2015NE805175) no montante de R$ 5.309,50 (cinco mil trezentos e nove reais e cinquenta centavos), prevista
na alínea 'c' do item 105.3 do Edital de Licitação, à Empresa A & L SERVICE LTDA, CNPJ n.º 14.752.105/0001-
01, pela não realização do objeto do empenho 2015NE805175, conforme atestado pela SUINFRA às fls. 27/27-
verso, 38/38-verso, bem como pelo NUDECON à fl. 44 do processo supracitado.
 
Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
LUIS ROBERTO DA SILVA MACEDO
Pró-Reitor de Planejamento e Administração em exercício
