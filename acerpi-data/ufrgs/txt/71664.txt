Documento gerado sob autenticação Nº ZFS.598.372.BIO, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8853                  de  01/11/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  RODRIGO  DE  LEMOS  PERONI,  matrícula  SIAPE  n°  2375719,  lotado  e  em  exercício  no
Departamento de Engenharia de Minas da Escola de Engenharia, da classe D  de Professor Associado, nível
02, para a classe D  de Professor Associado, nível 03, referente ao interstício de 24/04/2016 a 23/04/2018,
com vigência financeira a partir de 22/08/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro
de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.519935/2018-02.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
