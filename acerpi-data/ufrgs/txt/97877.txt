Documento gerado sob autenticação Nº IBH.262.985.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8233                  de  09/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ANNELISE KOPP ALVES,  Professor do Magistério Superior,
lotada e em exercício  no Departamento de Engenharia  dos Materiais  da Escola de Engenharia,  com a
finalidade de participar da "International Conference on Materials Science and Engineering 2019"; e realizar
visita à CSIRO -  Plant Industry,  à The University of Melbourne e à The University of New South Wales,
em  Melbourne,  Clayton,  Newcastle  e  Sydney,  Austrália,  no  período  compreendido  entre  13/09/2019  e
27/09/2019, incluído trânsito, com ônus CAPES/PRINT/UFRGS. Solicitação nº 85802.
RUI VICENTE OPPERMANN
Reitor
