Documento gerado sob autenticação Nº RWJ.095.068.BKJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3907                  de  27/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder à servidora SILVIA DE OLIVEIRA KIST, ocupante do cargo de  Pedagogo-área - 701058,
lotada na Secretaria de Educação a Distância, SIAPE 2433801, o percentual de 52% (cinquenta e dois por
cento) de Incentivo à Qualificação, a contar de 04/04/2016, tendo em vista a conclusão do curso de Mestrado
em Educação, conforme o Processo nº 23078.007325/2016-91.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
