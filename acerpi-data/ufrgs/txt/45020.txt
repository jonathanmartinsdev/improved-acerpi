Documento gerado sob autenticação Nº YOQ.395.565.P1N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8905                  de  22/09/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar no Instituto de Informática, com exercício na Secretaria do Programa de Pós-Graduação em
Ciência da Computação, CLEIA SAIONARA HOFFMANN, nomeada conforme Portaria Nº 7788/2017 de 18 de
agosto de 2017, publicada no Diário Oficial da União no dia 21 de agosto de 2017, em efetivo exercício
desde  18  de  setembro  de  2017,  ocupante  do  cargo  de  ASSISTENTE  EM  ADMINISTRAÇÃO,  Ambiente
Organizacional Administrativo, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
