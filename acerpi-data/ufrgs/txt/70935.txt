Documento gerado sob autenticação Nº BWP.954.834.60Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8263                  de  15/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder prorrogação de afastamento parcial,  no período de 15/10/2018 a 14/10/2019, para o
servidor ANDRE ABREU MARTINS, ocupante do cargo de Técnico de Laboratório Área - 701244, matrícula
SIAPE 1677070,  lotado  no  Instituto de Geociências, para cursar Doutorado em Geociências, conforme o
Processo nº 23078.515736/2017-36.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
