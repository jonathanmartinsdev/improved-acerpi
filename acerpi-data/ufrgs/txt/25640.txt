Documento gerado sob autenticação Nº ELU.536.360.357, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5868                  de  08/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder à servidora VERONICA MACHADO ROLIM, ocupante do cargo de  Médico Veterinário -
701048, lotada na Estação Experimental Agronômica, SIAPE 2326322, o percentual de 52% (cinquenta e dois
por cento) de Incentivo à Qualificação, a contar de 18/07/2016, tendo em vista a conclusão do curso de
Mestrado em Ciências Veterinárias, conforme o Processo nº 23078.015492/2016-13.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
