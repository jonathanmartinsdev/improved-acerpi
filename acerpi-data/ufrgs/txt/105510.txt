Documento gerado sob autenticação Nº XXF.118.864.Q33, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1631                  de  19/02/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar RENATA CARDOSO DE SOUZA, Matrícula SIAPE 3062373, ocupante do cargo de Assistente
em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe
da Divisão Financeira da Gerência Administrativa do Instituto de Biociências, Código FG-5, Código SRH 481,
com vigência a partir de 20/02/2020. Processo nº 23078.502508/2020-00.
RUI VICENTE OPPERMANN
Reitor.
