Documento gerado sob autenticação Nº PYL.152.817.09N, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5464                  de  23/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Remover, a partir de 4 de maio de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da Lei
n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
NEUZA  MARIA  PACHECO  FELICIANO  WOLLMANN,  ocupante  do  cargo  de  Técnico  de  Laboratório  Área,
Ambiente Organizacional Ciências Exatas e da Natureza, Código 701244, Classe D, Nível de Capacitação IV,
Padrão de Vencimento 16, SIAPE nº 0356514, do Instituto de Biociências para a lotação Centro de Estudos
Costeiros, Limnológicos e Marinhos, mantendo o exercício no Centro de Estudos Costeiros, Limnológicos e
Marinhos, conforme Decisão nº 134/2018 do Conselho Universitário, processo nº 23078.201251/2017-68.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
