Documento gerado sob autenticação Nº TDF.011.927.CIF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             154                  de  04/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°44398,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, PATRICIA HELENA XAVIER DOS SANTOS (Siape:
0359307 ),   para  substituir    RITA DE CASSIA  DOS SANTOS CAMISOLAO (Siape:  0359355 ),  Diretor  do
Departamento de Educação e Desenvolvimento Social da PROREXT, Código CD-4, em seu afastamento por
motivo de férias, no período de 07/01/2019 a 14/01/2019, com o decorrente pagamento das vantagens por 8
dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
