Documento gerado sob autenticação Nº SHD.355.433.1JB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5759                  de  03/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°28426,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MAURO ROESE (Siape: 0359286 ),  para substituir  
ENIO PASSIANI (Siape: 2097762 ), Coordenador da Comissão de Pesquisa do IFCH, em seu afastamento no
país, no período de 26/05/2017 a 30/05/2017.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
