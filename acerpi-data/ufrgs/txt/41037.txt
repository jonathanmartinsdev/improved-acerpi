Documento gerado sob autenticação Nº OFX.070.944.UUG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6253                  de  13/07/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°35896,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ANALISTA DE TECNOLOGIA DA
INFORMAÇÃO, do Quadro de Pessoal desta Universidade, FELIPE MENDONCA SCHEEREN (Siape: 1645595 ),
 para substituir    ARTHUR BOOS JUNIOR (Siape: 0351338 ),  Diretor do Departamento de Segurança da
Informação do CPD, Código FG-1, em seu afastamento por motivo de férias, no período de 19/07/2017 a
28/07/2017, com o decorrente pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
