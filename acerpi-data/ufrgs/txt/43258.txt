Documento gerado sob autenticação Nº GDJ.319.255.K5S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7823                  de  21/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016do Magnífico
Reitor, e conforme processo nº 23078.513125/2017-53
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de Nutricionista-habilitação, do
Quadro de Pessoal desta Universidade, RITA DE CÁSSIA COSTA CORBO (Siape: 1756980), para substituir
PATRICIA SOEIRO PETROSKI (Siape: 2206365), Chefe da Seção do Restaurante Universitário Campus do Vale II
RU 06, Código FG-3, em seu afastamento por motivo de Licença Capacitação, no período de 12 a 15/08/2017
e de 26 a 31/08/2017, com o decorrente pagamento das vantagens por 10 dias.
 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
