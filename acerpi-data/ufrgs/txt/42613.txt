Documento gerado sob autenticação Nº KBD.760.957.UJM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7356                  de  09/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Ad ic iona l  de  Insa lubr idade ,  no  percentual  de  10% ,  a  part i r  de
26/06/2017,  correspondente  ao  grau  Insalubridade  Média,  à  servidora  SUZETE  BRUM  GUIMARAES,
Identificação Única 24059390, Técnico em Higiene Dental, com exercício no Núcleo Especializado da Gerencia
Administrativa  da  ODONTO,  observando-se  o  disposto  na  Lei  nº  8.112,  de  11  de  dezembro  de  1990,
combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas
Insalubres conforme Laudo Pericial constante no Processo nº 23078.511205/2017-74, Código SRH n° 23219 e
Código SIAPE 2017002964.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
