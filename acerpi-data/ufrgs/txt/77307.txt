Documento gerado sob autenticação Nº ZLY.093.777.B9C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1834                  de  23/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°49155,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TÉCNICO EM CONTABILIDADE,
do Quadro de Pessoal  desta  Universidade,  FERNANDA BORDIGNON SOARES (Siape:  2054586 ),   para
substituir   CAMILA FOCKINK (Siape: 1341594 ), Gerente Administrativo da Escola de Administração, Código
FG-1, em seu afastamento por motivo de férias, no período de 25/02/2019 a 01/03/2019, com o decorrente
pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
