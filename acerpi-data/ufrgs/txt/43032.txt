Documento gerado sob autenticação Nº PAJ.820.941.RD8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             7668                  de  15/08/2017
  Alterar a composição da Comissão de Ética
no Uso de Animais - CEUA, estabelecida na
Portaria nº 2224 de 23/03/2016 e alterada
nas Portarias nº  3952 de 01/06/2016,  nº
6767  de  01/09/2016  e  nº  2921  de
03/04/2017.
A PRÓ-REITORA DE COORDENAÇÃO ACADÊMICA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL, no uso  de suas  atribuições,
RESOLVE:
              Alterar a composição da Comissão de Ética no Uso de Animais - CEUA, designada na Portaria nº 2224
de 23 de março de 2016 e alterada pelas Portarias nº 3952 de 01 de junho de 2016, nº 6767 de 01 de
setembro de 2016 e nº 2921 de 03 de abril de 2017, que passa a vigorar com os seguintes membros:
 
I - Biólogos e Médicos Veterinários
I.I - Biólogos
Titular:     ELOISA DA SILVEIRA LOSS
Suplente:  Fernando Benetti
 
Titular:    LIVIA KMETZSCH ROSA E SILVA
Suplente: Karina Mariante Monteiro
 
Titular:    LUCAS DE OLIVEIRA ALVARES
Suplente: Gonçalo Nuno Corte Real Ferraz de Oliveira
 
I.II - Médicos Veterinários     
Titular:    LUIS GUSTAVO CORBELLINI
Suplente: Veronica Schmidt
 
Titular:    CLÁUDIO ESTEVÃO FARIAS DA CRUZ
Suplente: Paula Cristina Sieczkowski Gonzalez
 
Titular:    FERNANDA BASTOS DE MELLO
Documento gerado sob autenticação Nº PAJ.820.941.RD8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Suplente: Marcele Bettim Bandinelli
 
Titular:    MARCELE DE SOUZA MUCCILLO
Suplente: Aline Silva Gouvea
 
Titular:    MARCELO MELLER ALIEVI
Suplente: Daniel Guimarães Gerardi
 
II - Docentes e Pesquisadores em Áreas Específicas
Titular:    ALEXANDRE TAVARES DUARTE DE OLIVEIRA
Suplente: Marcelo Lazzaron Lamers
        
Titular:    TADEU MELLO E SOUZA
Suplente: Guilhian Leipnitz
 
Titular:     ELISA CRISTINA MODESTO
Suplente: André Luis Lucero Batista
 
Titular:    MANOELA DOMINGUES MARTINS
Suplente: Eduardo Jose Gaio
 
Titular:    MIRIAM ANDERS APEL
Suplente: Marcelo Dutra Arbo
 
Titular:    REGIS ADRIEL ZANETTE
Suplente: Angelo Luis Stapassoli Piato
 
Titular:    ALEXANDRE SILVA DE QUEVEDO
Suplente: Tiago André Fontoura de Melo
 
Titular:    TERESA CRISTINA TAVARES DALLA COSTA
Suplente: Aline Rigon Zimmer
 
Titular:    INES ANDRETTA
Suplente: Luciano Trevizan
 
III - Representante de Sociedade Protetora dos Animais ? Associação de Defesa Animal e Ambiental do
Campus do Vale/Bichos do Campus-UFRGS
Titular:    MARA REJANE RITTER
Suplente: Denise Regina Jesien Farias
JANE FRAGA TUTIKIAN,
Pró-Reitora de Coordenação Acadêmica.
