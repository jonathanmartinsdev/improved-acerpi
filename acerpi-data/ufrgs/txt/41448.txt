Documento gerado sob autenticação Nº SCB.005.299.KBH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6541                  de  20/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016, do Magnífico Reitor, e conforme processo nº 23078.013588/2017-10
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de Assistente em Administração, do
Quadro de Pessoal desta Universidade, LUIZ GUSTAVO SANTOS REIS DE OLIVEIRA (Siape: 2277892),  para
substituir HELENA BEATRIZ PETERSEN (Siape: 0356710 ), Assistente da Ouvidoria, Código SRH 309, FG-2, em
sua Licença para Capacitação, no período de 06/07/2017 a 10/07/2017, com o decorrente pagamento das
vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
