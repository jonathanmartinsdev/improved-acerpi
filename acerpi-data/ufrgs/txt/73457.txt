Documento gerado sob autenticação Nº DDE.016.216.965, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10112                  de  13/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar RAQUEL SALCEDO GOMES, matrícula SIAPE n° 1313435, ocupante do cargo de Professor
do Magistério Superior, classe Adjunto A, lotada no Departamento Interdisciplinar do Campus Litoral Norte,
do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Coordenadora da COMGRAD do
Curso de Licenciatura em Pedagogia EaD, Código SRH 1532, código FUC, com vigência a partir da data de
publicação no Diário Oficial da União até 12/08/2020, a fim de completar o mandato da professora CLAUDIA
GLAVAM DUARTE, conforme artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.531577/2018-
06.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
