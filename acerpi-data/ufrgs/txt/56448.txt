Documento gerado sob autenticação Nº VZC.532.800.Q1E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5150                  de  13/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar na Faculdade de Direito, a partir de 29/06/2018, EDUARDO TEIXEIRA NATALE, matrícula SIAPE
n° 1051812, redistribuído conforme Portaria nº 1.015, de 30 de maio de 2018, do Ministério da Educação,
publicada no Diário Oficial da União no dia 01 de junho de 2018, ocupante do cargo de Assistente em
Administração, Classe D, Nível III,  Padrão de Vencimento 04, no Quadro de Pessoal desta Universidade.
Portaria nº 23078.506702/2018-31.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
