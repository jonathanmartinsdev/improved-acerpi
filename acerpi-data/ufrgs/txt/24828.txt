Documento gerado sob autenticação Nº GPY.892.108.UON, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5432                  de  20/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento no país de ANDRE ABREU MARTINS, ocupante do cargo de Técnico de
Laboratório Área,  lotado no Instituto de Geociências e com exercício no Laboratório de Geologia Isotópica, 
com a finalidade de participar de curso na Universidade Federal  de Pernambuco, em Recife,  Brasil,  no
período compreendido entre 24/07/2016 e 09/08/2016, incluído trânsito, com ônus limitado. Solicitação n°
21524.
CARLOS ALEXANDRE NETTO
Reitor
