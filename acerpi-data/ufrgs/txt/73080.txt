Documento gerado sob autenticação Nº FGO.968.530.N41, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9833                  de  05/12/2018
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de EDUARDO ANDRE PERONDI, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia Mecânica da Escola de Engenharia, com a finalidade
de  ministrar  cursos  na  Universidad  Nacional  Del  Nordeste,  em  Resistencia,  Argentina,  no  período
compreendido entre 06/12/2018 e 09/12/2018, incluído trânsito, com ônus limitado. Solicitação nº 61427.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
