Documento gerado sob autenticação Nº FOI.074.843.5SV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3526                  de  25/04/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar  na  Pró-Reitoria  de  Assuntos  Estudantis,  com  exercício  na  Divisão  de  Atendimento  ao
Estudante  -  TUA  UFRGS,  Ambiente  Organizacional  Administrativo,  RENATA  DA  FONTOURA  REMIÃO,
nomeada conforme Portaria Nº 2564/2019 de 21 de março de 2019, publicada no Diário Oficial da União no
dia 26 de março de 2019, em efetivo exercício desde 23 de abril de 2019, ocupante do cargo de ASSISTENTE
EM ADMINISTRAÇÃO, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
