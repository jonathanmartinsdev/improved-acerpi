Documento gerado sob autenticação Nº XFG.991.190.NC3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10901                  de  02/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  27/11/2017,   referente  ao  interstício  de
25/05/2016  a  26/11/2017,  para  a  servidora  CINTIA  DE  LORENZO,  ocupante  do  cargo  de  Técnico  de
Laboratório Área - 701244, matrícula SIAPE 1919145,  lotada  na  Faculdade de Veterinária, passando do
Nível de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.522279/2017-36:
Formação Integral de Servidores da UFRGS IV CH: 90 Carga horária utilizada: 58 hora(s) / Carga horária
excedente: 32 hora(s) (18/03/2016 a 19/07/2017)
ILB - Excelência no Atendimento CH: 20 (02/01/2017 a 22/01/2017)
ILB - Conhecendo o novo acordo ortográfico CH: 2 (28/04/2016 a 18/05/2016)
ENAP - - Impactos da Mudança do Clima para a Gestão Municipal CH: 20 (30/05/2017 a 19/06/2017)
ENAP - Impactos da Mudança do Clima para Gestão Municipal CH: 20 (30/05/2017 a 19/06/2017)
IX ENDIVET 2016 CH: 30 (17/10/2016 a 20/10/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
