Documento gerado sob autenticação Nº DXY.413.501.26H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3127                  de  27/04/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  DANIELA  FRANCISCO  BRAUNER,  matrícula  SIAPE  n°  2271361,  lotada  e  em  exercício  no
Departamento de Ciências Administrativas da Escola de Administração, da classe A  de Professor Adjunto A,
nível  01,  para  a  classe  A   de  Professor  Adjunto  A,  nível  02,  referente  ao  interstício  de  30/12/2015  a
29/12/2017, com vigência financeira a partir de 21/03/2018, de acordo com o que dispõe a Lei 12.772 de 28
de  dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.500195/2018-22.
JANE FRAGA TUTIKIAN
Vice-Reitora.
