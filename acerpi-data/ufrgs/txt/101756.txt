Documento gerado sob autenticação Nº MMM.266.355.3LV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11288                  de  19/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual  de Incentivo à Qualificação concedido à servidora ANALICE LONGARAY
TEIXEIRA, ocupante do cargo de Bibliotecário-documentalista-701010, lotada no Colégio de Aplicação, SIAPE
1698303, para 30% (trinta por cento), a contar de 29/11/2019, tendo em vista a conclusão da Especialização
em Biblioteconomia e Gestão de Bibliotecas Escolares, conforme o Processo nº 23078.533209/2019-75.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
