Documento gerado sob autenticação Nº XYI.356.862.QEB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             600                  de  19/01/2018
  Designa  Comissão  Recursal  da  Comissão
Permanente  de  Verificação  de  Documentos  da
Condição de Pessoa com Deficiência.
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de
suas atribuições legais e estatutárias
RESOLVE:
Designar
Membros:
MARILIA BORGES HACKMANN - Diretora do Departamento de Atenção à Saúde
DIEGO LISBOA DOS SANTOS - Divisão de Saúde e Junta Médica
EDUARDO COPSTEIN - Divisão de Saúde e Junta Médica
FERNANDA SILVA MENEZES - Divisão de Saúde e Junta Médica
GABRIEL FERREIRA PHEULA - Divisão de Saúde e Junta Médica
GABRIELA MACHADO DE CASTILHOS - Divisão de Saúde e Junta Médica
JULIANA DONADUSSI NEUHAUS LIGNATI - Divisão de Saúde e Junta Médica
LUCIANE LACERDA GOMES GONÇALVES - Divisão de Saúde e Junta Médica
EDUARDO HERNANDES FERNANDES - Divisão de Segurança e Medicina do Trabalho
FÁBIO RUSCHEL - Divisão de Segurança e Medicina do Trabalho
MARCIO DALL AGNESE - Divisão de Segurança e Medicina do Trabalho
PEDRO IVO KALIL GASPAR - Divisão de Segurança e Medicina do Trabalho
VERA REGINA MARQUES - Divisão de Segurança e Medicina do Trabalho
JOSE MENNA OLIVEIRA - Divisão de Promoção da Saúde
LUCIANI MENDES DE OLIVEIRA DA FONSECA - Divisão de Promoção da Saúde
MILTON HUMBERTO SCHANES DOS SANTOS - Divisão de Promoção da Saúde
para,  sob  a  coordenação  da  primeira,  compor  a  Comissão  Recursal  da  Comissão  Permanente  de  Verificação  de
Documentos da Condição de Pessoa com Deficiência, do Concurso Vestibular 2018 desta Universidade.
JANE FRAGA TUTIKIAN,
Vice-Reitora, no exercício da Reitoria.
