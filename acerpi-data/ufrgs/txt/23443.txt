Documento gerado sob autenticação Nº VHR.919.904.EOC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4423                  de  16/06/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°20842,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, LUIS ARMANDO GANDIN (Siape: 1079411 ),  para
substituir    GILBERTO ICLE (Siape:  2228826 ),  Coordenador do PPG em Educação,  Código FUC, em seu
afastamento no país, no período de 20/06/2016 a 21/06/2016, com o decorrente pagamento das vantagens
por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
