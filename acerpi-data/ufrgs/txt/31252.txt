Documento gerado sob autenticação Nº NFF.458.167.OP7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9833                  de  12/12/2016
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
 
RESOLVE:
 
Considerando o que consta do Processo Administrativo n° 23078.202052/2015-13, do Pregão Eletrônico nº
169/2015, da Lei 10.520/02 e ainda da Lei 8.666/93,
Aplicar as sanções administrativas abaixo à Empresa MG COMERCIAL DE EQUIPAMENTOS E SERVIÇOS
EIRELI-ME, CNPJ n.º 15.135.630/0001-40, por não cumprir com o prazo e com as condições de execução,
conforme atestado pela SUINFRA às fls. 109, 112, 120 e 137, bem como pelo NUDECON à fl. 141 do processo
supracitado:
1. ADVERTÊNCIA, prevista no item 90.1. do Edital do Pregão Eletrônico supracitado;
2. MULTA de 10% (dez por cento) sobre o valor total empenhado (2015NE807697), no
montante de R$ 11.578,84 (onze mil, quinhentos e setenta e oito reais e oitenta e quatro
centavos), prevista na alínea 'b' do item 90.3.
Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
