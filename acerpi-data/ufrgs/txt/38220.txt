Documento gerado sob autenticação Nº DTG.734.021.7HC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4288                  de  17/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  à  servidora  EVELIN  STAHLHOEFER  COTTA,  ocupante  do  cargo  de   Bibliotecário-
documentalista - 701010, lotada na Escola de Administração, SIAPE 1509913, o percentual de 52% (cinquenta
e dois por cento) de Incentivo à Qualificação, a contar de 27/04/2017, tendo em vista a conclusão do curso de
Mestrado em Memória Social e Bens Culturais, conforme o Processo nº 23078.004187/2017-79.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
