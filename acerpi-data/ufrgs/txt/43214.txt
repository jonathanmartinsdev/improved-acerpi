Documento gerado sob autenticação Nº YEI.446.532.K5S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7870                  de  22/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder progressão por capacitação, a contar de 14/12/2010, em decorrência da retificação do
enquadramento  no  PCCTAE,  determinada  por  decisão  judicial  proferida  no  processo  nº  5006079-
15.2011.4.04.7100, da 3ª Vara Federal de Porto Alegre, à servidora CLAUDIA HOCHHEIM OLIVEIRA, matrícula
SIAPE n° 0756992, ativa no cargo de  Visitador Sanitário - 701268, para o nível III, em virtude de ter realizado o
seguinte curso, conforme o Processo nº 23078.001127/2017-02
               
               Pichon-Rivière Instituto de Psicologia Social - Coordenação de Grupos: Experiência Teórico-Vivencial.
CH: 144 horas (23/03/2010 a 08/12/2010).
 
 
 
MAURÍCIO VIEGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
 
