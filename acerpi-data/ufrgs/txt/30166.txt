Documento gerado sob autenticação Nº LOJ.587.418.E0T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8944                  de  07/11/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de JOHNNY FERRAZ DIAS, Professor do Magistério Superior, lotado
e em exercício no Departamento de Física do Instituto de Física, com a finalidade de participar do "VIII Taller
de colisiones inelásticas en la  matéria",  em Quintana Roo,  México e de reunião e proferir  palestra na
Universidade de Havana,  Cuba, no período compreendido entre 05/12/2016 e 16/12/2016, incluído trânsito,
com ônus CAPES/MES-CUBA. Solicitação nº 24580.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
