Documento gerado sob autenticação Nº UMU.043.133.5FI, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2095                  de  18/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de Outubro de 2012
RESOLVE:
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora MARIANA DE MATTOS
BROSE, ocupante do cargo de Técnico em Radiologia-701257, lotada no Hospital de Clínicas Veterinárias,
SIAPE 1953337, para 52% (cinquenta e dois por cento), a contar de 29/12/2015, tendo em vista a conclusão do
curso de Mestrado em Saúde e Desenvolvimento Humano, conforme o Processo nº 23078.203378/2015-50.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
