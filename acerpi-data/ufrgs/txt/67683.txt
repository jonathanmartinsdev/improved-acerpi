Documento gerado sob autenticação Nº EQB.089.358.NGV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5973                  de  08/08/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder prorrogação de afastamento parcial,  no período de 31/08/2018 a 31/01/2019, para a
servidora  LEONÉIA  HOLLERWEGER,  ocupante  do  cargo  de  Pedagogo-área  -  701058,  matrícula  SIAPE
1058263,  lotada  na  Escola de Administração, para cursar Mestrado em Educação, conforme o Processo nº
23078.511998/2017-21.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
