Documento gerado sob autenticação Nº MUT.859.884.ASV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1127                  de  06/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°41900,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ARQUIVISTA, do Quadro de
Pessoal desta Universidade, LAURA GOMES MACHADO (Siape: 2055436 ),  para substituir   MEDIANEIRA
APARECIDA PEREIRA GOULART (Siape: 2523113 ), Diretor da Divisão de Documentação, vinc. ao Depto de
Assessoria  Geral  da PROPLAN,  Código FG-4,  em seu afastamento por motivo de férias,  no período de
14/02/2018 a 23/02/2018, com o decorrente pagamento das vantagens por 10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
