Documento gerado sob autenticação Nº DCQ.115.483.AD3, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2512                  de  06/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de Paulo Ricardo Zilio Abdala, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ciências Administrativas da Escola de Administração, com a
finalidade de participar do "32nd Colloquium of the European Group for Organizational Studies", em Nápoles,
Itália, no período compreendido entre 06/07/2016 e 11/07/2016, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa - diárias). Solicitação nº 18284.
CARLOS ALEXANDRE NETTO
Reitor
