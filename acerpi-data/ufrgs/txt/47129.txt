Documento gerado sob autenticação Nº EMR.960.054.5DB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10454                  de  14/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar insubsistente a Portaria nº 10352/2017, de 09/11/2017, que designou temporariamente LUIZ
GUSTAVO SANTOS REIS DE OLIVEIRA (Siape: 2277892), para substituir PANTELIS VARVAKI RADOS (Siape:
0354721 ),  Assessor do Reitor,  Código SRH 1388, código CD-4, no período de 16/11/2017 à 13/12/2017,
gerada conforme Solicitacao de Férias nº 32340. Processo SEI nº 23078.521011/2017-87
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
