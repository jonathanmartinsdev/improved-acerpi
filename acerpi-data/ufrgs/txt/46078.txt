Documento gerado sob autenticação Nº SUU.302.727.MSF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9649                  de  17/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor CHRISTIAN COSTA KIELING, matrícula SIAPE n° 2603402, lotado e em exercício no Departamento
de Psiquiatria e Medicina Legal da Faculdade de Medicina, da classe A  de Professor Adjunto A, nível 01, para
a classe A  de Professor Adjunto A, nível 02, referente ao interstício de 03/08/2015 a 02/08/2017, com vigência
financeira a partir de 03/08/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo
nº 23078.513757/2017-17.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
