Documento gerado sob autenticação Nº GNW.972.886.JCF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4741                  de  03/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de EDSON PRESTES E SILVA JUNIOR,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Informática Teórica do Instituto de Informática, com a
finalidade de participar de reuniões junto à Organização das Nações Unidas, em Nova York, Estados Unidos,
no período compreendido entre 07/06/2019 e 13/06/2019, incluído trânsito, com ônus limitado. Solicitação nº
84711.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
