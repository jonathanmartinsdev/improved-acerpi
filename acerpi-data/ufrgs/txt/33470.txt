Documento gerado sob autenticação Nº XZY.348.194.D12, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             876                  de  26/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
I  -  Conceder afastamento pelo período de um ano a partir  de 01/02/2017,  à  servidora DEISE
MAZZARELLA GOULART FERREIRA,  ocupante do cargo de Administrador,  classe E,  nível  IV,  padrão 05,
matrícula SIAPE n° 1735397, lotada na Secretaria de Educação a Distância e com exercício na Secretaria de
Educação a Distância,  nos termos do artigo 26-A da Lei  n° 11.191 de dezembro de 2005, para prestar
colaboração técnica junto à(ao) Universidade Federal do Pampa.
II - Fica sob responsabilidade do(a) Universidade Federal do Pampa o envio da frequência da citada
servidora a esta Universidade até o 5° (quinto) dia útil do mês subsequente ao trabalhado.
Processo n° 23078.025091/2016-63.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
