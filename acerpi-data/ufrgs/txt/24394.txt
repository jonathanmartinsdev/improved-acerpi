Documento gerado sob autenticação Nº EPT.018.006.K3V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5064                  de  11/07/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de DAVID MANUEL LELINHO DA MOTTA MARQUES, Professor do
Magistério Superior, lotado no Departamento de Obras Hidráulicas do Instituto de Pesquisas Hidráulicas e
com exercício  no Programa de Pós-Graduação em Recursos  Hídricos  e  Saneamento Ambiental,  com a
finalidade de participar do "32nd Congress of the International Society of Limnology", em Turim, Itália, no
período compreendido entre 30/07/2016 e 06/08/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa - diárias). Solicitação nº 20745.
CARLOS ALEXANDRE NETTO
Reitor
