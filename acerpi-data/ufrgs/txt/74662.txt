Documento gerado sob autenticação Nº KUK.773.152.Q90, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             248                  de  09/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta  Universidade,  ANDRE LUIZ  LOPES DA SILVEIRA,  matrícula  SIAPE n°  0357406,  lotado no
Departamento de Hidromecânica e Hidrologia do Instituto de Pesquisas Hidráulicas, para exercer a função de
Coordenador do PPG em Gestão e Regulação de Recursos Hídricos-PROFAGUA, Código SRH 1517, código
FUC,  com  vigência  a  partir  de  11/01/2019  até  10/01/2021,  por  ter  sido  reeleito.  Processo  nº
23078.533418/2018-38.
JANE FRAGA TUTIKIAN
Vice-Reitora.
