Documento gerado sob autenticação Nº NPV.538.489.KKO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4595                  de  23/06/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARIA LUIZA ADAMS SANVITTO,  Professor do Magistério
Superior,  lotada e  em exercício  no Departamento de Arquitetura  da  Faculdade de Arquitetura,  com a
finalidade de participar da "3rd International Conference on Structures and Architecture",  na cidade de
Guimarães, Portugal, no período compreendido entre 26/07/2016 e 30/07/2016, incluído trânsito, com ônus
UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 20567.
CARLOS ALEXANDRE NETTO
Reitor
