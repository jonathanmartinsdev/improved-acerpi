Documento gerado sob autenticação Nº SAH.457.408.4MR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1575                  de  16/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Exonerar,  a  partir  da data de publicação no Diário  Oficial  da União,  o  ocupante do cargo de
Assistente em Administração,  274934,  D,  IV,  do Quadro de Pessoal  desta Universidade,  LUIZ FABRICIO
MARTINS DOS SANTOS, matrícula SIAPE n° 0356209, do cargo de Prefeito do Campus Saúde e Olímpico da
Vice-Superintendência de Manutenção da SUINFRA, Código SRH 1014, Código CD-4, para o qual foi nomeado
pela Portaria n° 8890, de 04/11/2016, publicada no Diário Oficial  da União de 07/11/2016, por ter sido
nomeado para outro cargo de direção. Processo nº 23078.002159/2017-17.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
