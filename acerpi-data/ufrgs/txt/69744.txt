Documento gerado sob autenticação Nº TLY.842.005.O34, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             7286                  de  14/09/2018
O PRÓ-REITOR DE GRADUAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de
suas atribuições, considerando o disposto na Portaria nº 7626, de 29 de setembro de 2016
RESOLVE
Designar
IRMA ANTONIETA GRAMKOW BUENO - Pró-Reitoria de Graduação;
ADRIANA SALDANHA FERRARI - Faculdade de Medicina;
CRISTINA MACHADO WACHTER - Pró-Reitoria de Graduação;
HAMILTON FERNANDO DOS SANTOS SANTANA - Pró-Reitoria de Graduação;
IDA MARIA DE OLIVEIRA - Secretaria de Avaliação Institucional;
JULIANA RIBEIRO AZEVEDO - Pró-Reitoria de Graduação;
LILIANE DELLA LBERA - Pró-Reitoria de Graduação;
MARCO ANTONIO TEIXEIRA VIANNA - Pró-Reitoria de Graduação;
PATRICIA COSTA AZEVEDO - Pró-Reitoria de Gestão de Pessoas;
RICARDO STRACK - Pró-Reitoria de Graduação;
SILVIO CESAR ESCOVAR PAIVA - Secretaria de Educação a Distância;
VALDONI PEREIRA BARTH - Campus Litoral Norte;
VANESSA GABRIELA SAGGIN - Colégio de Aplicação;|
para,  sob  a  presidência  da  primeira,  constituírem  a  Comissão  de  Recursos  da  Análise
Socioeconômica dos concursos Vestibular e SiSU.
VLADIMIR PINHEIRO DO NASCIMENTO
Pró-Reitor de Graduação
Documento gerado sob autenticação Nº TLY.842.005.O34, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
