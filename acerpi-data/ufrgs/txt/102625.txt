Documento gerado sob autenticação Nº ISF.255.497.TKR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11329                  de  20/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°50236,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ELOA ROSSONI (Siape: 0356892 ),  para substituir  
VICENTE CASTELO BRANCO LEITUNE (Siape: 2866781 ), Coordenador da Comissão de Pesquisa da Faculdade
de Odontologia,  em seu afastamento por motivo de férias, no período de 23/12/2019 a 03/01/2020.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
