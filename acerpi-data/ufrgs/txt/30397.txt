Documento gerado sob autenticação Nº GGB.171.091.0P8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9113                  de  16/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7612, de 29 de setembro de 2016
RESOLVE
Retificar a Portaria n° 8265/2016, de 14/10/2016, publicada no Diário Oficial da União de 17/10/2016
que nomeou em caráter efetivo,  LUCIO ALBINO AMARO DA SILVA,  para o cargo de Engenheiro/Área:
Mecânica. Processo nº 23078.020128/13-15.
,
 
Onde se lê:
Vaga SIAPE 981609
leia-se:
Vaga SIAPE 981607.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
