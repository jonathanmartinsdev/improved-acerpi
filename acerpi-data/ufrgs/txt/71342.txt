Documento gerado sob autenticação Nº YLR.479.488.5UK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8586                  de  24/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora LIVIA DONIDA BIASOTTO,
ocupante do cargo de Administrador-701001, lotada na Pró-Reitoria de Extensão, SIAPE 1759692, para 52%
(cinquenta e dois por cento), a contar de 05/06/2018, tendo em vista a conclusão do curso de Mestrado em
Planejamento Urbano e Regional, conforme o Processo nº 23078.513537/2018-74.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
