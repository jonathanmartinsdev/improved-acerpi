Documento gerado sob autenticação Nº UUK.193.644.EKF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4175                  de  11/05/2017
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7793, de 03 de outubro de 2016
RESOLVE
Designar os servidores,  MAGDA DA SILVEIRA ELKFURY, ocupante do cargo de Engenheiro-área,
lotada na Superintendência de Infraestrutura e com exercício no Setor de Fiscalização e Adequação, PAULO
HENRIQUE  DOS  SANTOS  PACHECO,  ocupante  do  cargo  de  Técnico  em  Edificações,  lotado  na
Superintendência de Infraestrutura e com exercício na Prefeitura Campus do Vale, REGINALDO DOS SANTOS
LOPES, ocupante do cargo de Engenheiro-área, lotado na Superintendência de Infraestrutura e com exercício
na Prefeitura Campus do Vale, para sob a presidência da primeira, constituirem a Comissão de Recebimento,
em atendimento à alínea "b" do inciso I do artigo 73 da Lei 8666/93, para emissão do Termo de Recebimento
Definitivo da Obra do contrato 070/2016, cujo objeto é "REFORMA E ADAPTAÇÃO DOS SANITÁRIOS DO
INSTITUTO DE INFORMÁTICA E INSTITUTO DE BIOCIÊNCIAS no Campus do Vale da UFRGS, em Porto
Alegre  RS", com prazo até 11/12/2017 quando se encerra o prazo de vigência do contrato. Processo nº
23078.200031/2016-36.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
ANDREA PINTO LOGUERCIO
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
