Documento gerado sob autenticação Nº PNF.427.007.CVH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8578                  de  24/10/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do país  de FLAVIO DANNI FUCHS,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a finalidade de
participar da "Scientific Sessions of the American Heart Association", em Chicago, Estados Unidos, no período
compreendido entre 08/11/2018 e 13/11/2018, incluído trânsito, com ônus limitado. Solicitação nº 60617.
RUI VICENTE OPPERMANN
Reitor
