Documento gerado sob autenticação Nº WSY.060.342.QC5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9977                  de  10/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  ALEX  FERNANDO  TEIXEIRA  PRIMO,  matrícula  SIAPE  n°  1278222,  lotado  e  em  exercício  no
Departamento de Comunicação da Faculdade de Biblioteconomia e Comunicação, da classe D  de Professor
Associado, nível 03, para a classe D  de Professor Associado, nível 04, referente ao interstício de 31/03/2015 a
30/03/2017, com vigência financeira a partir de 10/11/2018, de acordo com o que dispõe a Lei 12.772 de 28
de  dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.509812/2018-55.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
