Documento gerado sob autenticação Nº BKA.812.487.5J5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2249                  de  12/03/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  do  Processo  Administrativo  n°  23078.529546/2018-87  e
23078.511035/2017-28, do contrato nº 102/2017, da Lei nº 10.520/02 e ainda, da Lei nº 8.666/93,
 
              RESOLVE:
 
           Aplicar a sanção administrativa de MULTA de 5% (cinco por cento) sobre o valor da parcela que lhe deu
causa, no montante de R$ 5.104,92 (cinco mil cento e quatro reais e noventa e dois centavos), conforme
demonstrativo de cálculo (Doc. SEI nº 1475741), prevista no inciso II, item 3 da Cláusula 11ª do Contrato, à
Empresa ROMARCK GERADORES - COMÉRCIO E SERVIÇOS LTDA - ME, CNPJ n.º 04.298.489/0001-80, por
atraso no saneamento das pendências de execução dos orçamentos, conforme atestado pela fiscalização do
contrato (Doc. SEI nº 1396438 e 1412158), bem como pelo NUDECON (Doc. SEI nº 1474082) do processo
23078. 529546/2018-87.
 
               Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG. 
 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
