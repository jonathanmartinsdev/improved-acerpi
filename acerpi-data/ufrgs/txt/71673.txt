Documento gerado sob autenticação Nº WNQ.879.750.CQC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8812                  de  31/10/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 24 de outubro de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
MARCELO NOLD, ocupante do cargo de Auxiliar em Administração, Ambiente Organizacional Administrativo,
Código 701405, Classe C, Nível de Capacitação II, Padrão de Vencimento 03, SIAPE nº. 1566834, da Faculdade
de Direito para a lotação Centro de Estudos e Pesquisas Econômicas, com novo exercício na Secretaria do
Centro de Estudos e Pesquisas Econômicas.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
