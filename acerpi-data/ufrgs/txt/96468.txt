Documento gerado sob autenticação Nº QGP.102.300.4P8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7371                  de  15/08/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°51079,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  AGNES OLSCHOWSKY (Siape:  6356803  ),   para
substituir    GISELA  MARIA  SCHEBELLA  SOUTO  DE  MOURA  (Siape:  0356815  ),  Diretor  da  Escola  de
Enfermagem,  Código  CD-3,  em  seu  afastamento  por  motivo  de  férias,  no  período  de  15/08/2019  a
02/09/2019, com o decorrente pagamento das vantagens por 19 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
