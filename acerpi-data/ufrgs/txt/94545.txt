Documento gerado sob autenticação Nº JWG.252.096.GD0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6480                  de  23/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ENOI DAGO LIEDKE, matrícula SIAPE n° 0357861, lotada e em exercício no Departamento de
Comunicação da Faculdade de Biblioteconomia e Comunicação, da classe C  de Professor Adjunto, nível 03,
para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 28/02/2017 a 27/02/2019, com
vigência financeira a partir de 28/02/2019, conforme decisão judicial proferida no processo nº 5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.507436/2019-45.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
