Documento gerado sob autenticação Nº FAJ.760.507.CD7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5940                  de  15/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°48590,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  BIBLIOTECÁRIO-
DOCUMENTALISTA, do Quadro de Pessoal desta Universidade, LEONARDO DO PRADO RODRIGUES (Siape:
2156563 ),  para substituir   ISMAEL CABRAL (Siape: 2994373 ), Chefe da Biblioteca do CECLIMAR, Código FG-5,
em seu afastamento por motivo de férias,  no período de 15/07/2019 a 30/07/2019,  com o decorrente
pagamento das vantagens por 16 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
