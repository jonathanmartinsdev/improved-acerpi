Documento gerado sob autenticação Nº OWP.743.080.NKV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10919                  de  06/12/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 11/12/2019,  o ocupante do cargo de Professor do Magistério
Superior, classe Adjunto, do Quadro de Pessoal desta Universidade, LEANDRO MANENTI, matrícula SIAPE n°
2137343, da função de Coordenador Substituto da COMGRAD de Arquitetura, para a qual foi designado pela
Portaria 10038/2018, de 12/12/2018. Processo nº 23078.533821/2019-48.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
