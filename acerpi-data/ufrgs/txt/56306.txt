Documento gerado sob autenticação Nº BBO.541.391.Q1E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5157                  de  13/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/07/2018,   referente  ao  interstício  de
01/01/2017 a 10/07/2018, para a servidora AMANDA SANTOS WITT, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2053951,  lotada  na  Escola de Enfermagem, passando do Nível de
Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.517349/2018-15:
ENAP - Ética e Serviço Público CH: 20 (26/06/2018 a 17/07/2018)
ENAP - Gestão de Projetos CH: 20 Carga horária utilizada: 10 hora(s) / Carga horária excedente: 10 hora(s)
(09/07/2018 a 30/07/2018)
ENAP - Controle Social CH: 20 (29/06/2018 a 20/07/2018)
ENAP - Sistema Eletrônico de Informações - SEI CH: 20 (28/11/2017 a 18/12/2017)
ENAP - SEI! USAR CH: 20 (28/11/2017 a 18/12/2017)
ENAP - Introdução à Gestão de Processos CH: 20 (19/06/2018 a 10/07/2018)
ENAP  -  Gestão  da  Informação  e  Documentação  -  Conceitos  Básicos  em  Gestão  Documental  CH:  20
(24/04/2018 a 15/05/2018)
ENAP - Gestão de Processos CH: 20 (19/06/2018 a 10/07/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
