Documento gerado sob autenticação Nº BOI.633.993.F5N, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5209                  de  14/06/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a ROSELEI RIBEIRO PRATES,
matrícula SIAPE nº 0355156, no cargo de Telefonista, nível de classificação C, nível de capacitação I, padrão
16, do Quadro desta Universidade, no regime de quarenta horas semanais de trabalho, com exercício no
Departamento de Infraestrutura de TI do Centro de Processamento de Dados, com proventos integrais.
Processo 23078.008714/2017-14.
RUI VICENTE OPPERMANN
Reitor.
