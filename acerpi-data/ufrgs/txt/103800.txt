Documento gerado sob autenticação Nº QDH.197.023.P0M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             548                  de  14/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor WAGNER DA SILVA, ocupante do cargo de Assistente em Administração -
701200, lotado na Pró-Reitoria de Planejamento e Administração, SIAPE 3159232, o percentual de 20% (vinte
por cento) de Incentivo à Qualificação, a contar de 18/12/2019, tendo em vista a conclusão do Curso Técnico
em Contabilidade, conforme o Processo nº 23078.535041/2019-32.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
