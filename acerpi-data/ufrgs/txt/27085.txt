Documento gerado sob autenticação Nº CBB.114.307.U69, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6843                  de  02/09/2016
  Designa  Coordenadora  do  Programa
Inglês  Sem  Fronteiras,  no  âmbito  da
UFRGS,  atuando  como  representante
institucional  junto  ao  MEC  e  a  CAPES.
               O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições legais e estatutárias, considerando a solicitação do Ministério da Educação referente
à indicação de professores da UFRGS para o funcionamento do Núcleo de Idiomas da Universidade e para a
aplicação dos Exames de Língua Inglesa,
RESOLVE
            Art. 1° Designar a servidora SIMONE SARMENTO, Professora de Magistério Superior, SIAPE nº
1805369, para exercer a função de Coordenadora do Programa Idioma Sem Fronteiras, no âmbito da UFRGS,
atuando  como  representante  institucional  junto  ao  Ministério  da  Educação  e  à  Coordenação  de
Aperfeiçoamento de Pessoal - CAPES - e demais órgãos governamentais que tenham relação com o referido
Programa, assim como junto à MASTERTEST, como coordenadora de todas as ações decorrentes da função.
 
              Art 2º - Revogar a Portaria nº 6702 de 08 de setembro de 2015.
 
              Art. 3º -  Esta Portaria entra em vigor na data de sua assinatura.
RUI VICENTE OPPERMANN,
VICE-REITOR, NO EXERCÍCIO DA REITORIA.
