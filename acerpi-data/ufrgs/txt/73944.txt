Documento gerado sob autenticação Nº AVK.956.861.PH8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10393                  de  21/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, DOUGLAS GAMBA, matrícula SIAPE n° 2281542, lotado no Departamento de
Química Orgânica do Instituto de Química, como Chefe Substituto do Depto de Química Orgânica do Instituto
de Química, para substituir automaticamente o titular desta função em seus afastamentos ou impedimentos
regulamentares na vigência do presente mandato. Processo nº 23078.535274/2018-54.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
