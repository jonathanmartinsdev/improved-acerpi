Documento gerado sob autenticação Nº PDJ.297.378.J4Q, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6030                  de  10/07/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°36591,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  BIBLIOTECÁRIO-
DOCUMENTALISTA, do Quadro de Pessoal desta Universidade, ANANDA FEIX RIBEIRO (Siape: 2156868 ),
 para substituir   CAROLINA KAUTZMANN (Siape: 1446734 ), Chefe da Biblioteca da Diretoria Acadêmica do
Campus Litoral Norte, Código FG-2, em seu afastamento por motivo de férias, no período de 03/07/2017 a
21/07/2017, com o decorrente pagamento das vantagens por 19 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
