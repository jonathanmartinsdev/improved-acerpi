Documento gerado sob autenticação Nº JLL.797.280.THG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3740                  de  02/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ALAN ALVES BRITO,  matrícula SIAPE n° 2082163, lotado no Departamento de Astronomia do
Instituto de Física e com exercício no Observatório Astronômico, da classe C  de Professor Adjunto, nível 01,
para a classe C  de Professor Adjunto, nível 02, referente ao interstício de 14/01/2017 a 13/01/2019, com
vigência financeira a partir de 22/03/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de
2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.505800/2019-32.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
