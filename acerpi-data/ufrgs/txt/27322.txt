Documento gerado sob autenticação Nº GZV.911.467.4L7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6989                  de  06/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  31/01/2014,
correspondente  ao  grau  Insalubridade  Média,  à  servidora  BÁRBARA  NIEGIA  GARCIA  DE  GOULART,
Identificação  Única  17168716,  Professor  do  Magistério  Superior,  com  exercício  no  Programa  de  Pós-
Graduação em Epidemiologia da Faculdade de Medicina, observando-se o disposto na Lei nº 8.112, de 11 de
dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas
consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.038890/2014-38, Código
SRH n° 22874 e Código SIAPE 2016003285.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
