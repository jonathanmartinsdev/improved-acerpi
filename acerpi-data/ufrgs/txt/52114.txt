Documento gerado sob autenticação Nº GMI.162.242.F07, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2158                  de  21/03/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor CARLOS EDUARDO LIMA
DOS SANTOS, ocupante do cargo de Contínuo-701421, lotado no Instituto de Química, SIAPE 1031354, para
50% (cinquenta por cento), a contar de 18/12/2017, tendo em vista a conclusão do curso de Doutorado em
Ciências - Área de Concentração: Geoquímica, conforme o Processo nº 23078.524064/2017-50.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
