Documento gerado sob autenticação Nº OBN.961.763.UPC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1248                  de  06/02/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°52019,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, JULIO OTAVIO JARDIM BARCELLOS (Siape: 1034086 ),
 para substituir   AUGUSTO JAEGER JUNIOR (Siape: 1534113 ), Presidente da Câmara de Pesquisa, Código CD-
4, em seu afastamento por motivo de férias, no período de 07/02/2020 a 24/02/2020, com o decorrente
pagamento das vantagens por 18 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
