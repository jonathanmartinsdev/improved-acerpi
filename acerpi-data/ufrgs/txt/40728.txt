Documento gerado sob autenticação Nº PXP.087.540.ABL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6083                  de  11/07/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de JULIANA CHARAO MARQUES, Professor do Magistério Superior,
lotada e  em exercício  no Departamento de Geologia  do Instituto de Geociências,  com a finalidade de
participar da pré-conferência e da "14th Biennial  Meeting of the Society for Geology Applied to Mineral
Deposits",  em  Quebec,  com  ônus  UFRGS  (Pró-Reitoria  de  Pesquisa  -  diárias)  e  realizar  visita  Queen's
University, em Kingston, Ontario, Canadá, Canadá, no período compreendido entre 12/08/2017 e 25/08/2017,
incluído trânsito, com ônus limitado. Solicitação nº 29270.
RUI VICENTE OPPERMANN
Reitor
