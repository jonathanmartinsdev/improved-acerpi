Documento gerado sob autenticação Nº GIM.935.212.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6009                  de  08/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do país  de DANIEL UMPIERRE DE MORAES,  Professor  do Magistério
Superior, lotado no Departamento de Assistência e Orientação Profissional da Escola de Enfermagem e com
exercício no Departamento de Saúde Coletiva, com a finalidade de participar do "Research Transparency and
Reproducibility Training", em Los Angeles, Estados Unidos, no período compreendido entre 01/09/2018 e
09/09/2018, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 47891.
RUI VICENTE OPPERMANN
Reitor
