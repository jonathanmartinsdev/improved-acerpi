Documento gerado sob autenticação Nº ALP.337.081.59B, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3650                  de  17/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  5469,  de 04 de outubro de 2012,  do
Magnífico Reitor, e conforme processo nº23078.009350/2016-17
RESOLVE
Retificar a Portaria nº 771/2016, de 05/02/2016, que designou, temporariamente, PAULO EDISON
BELO REYES (Siape: 1985477) para substituir  PAULO EDISON BELO REYES (Siape: 1985477 ),
onde se lê
"...  em seu afastamento por motivo de férias,  no período de 25/01/2016 a 31/01/2016, com o
decorrente pagamento das vantagens por 7 dias".
leia-se:
"...  em seu afastamento por motivo de férias,  no período de 25/01/2016 a 31/01/2016, com o
decorrente pagamento das vantagens por 1 dia, devido à concomitância de substituição de função" ; ficando
ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
