Documento gerado sob autenticação Nº INV.166.554.TUP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             818                  de  23/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
                         Autorizar o afastamento no País de RITA DE CASSIA DOS SANTOS CAMISOLAO, ocupante do
cargo de Assistente em Administração, lotada na Pró-Reitoria de Extensão e com exercício no Departamento
de Educação e Desenvolvimento Social, com a finalidade de realizar estudos em nível de Mestrado junto à
Universidade Federal do Rio Grande do Sul, em Porto Alegre, Rio Grande do Sul, no período compreendido
entre 11/03/2019 e 11/10/2019, com ônus limitado. Processo 23078.500588/2019-17.
JANE FRAGA TUTIKIAN
Vice-Reitora.
