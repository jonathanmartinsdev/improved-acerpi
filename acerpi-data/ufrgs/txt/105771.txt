Documento gerado sob autenticação Nº GHR.474.747.IRF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1942                  de  05/03/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°55858,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ADMINISTRADOR, do Quadro de
Pessoal desta Universidade, GABRIELA WAGNER ESPÍNDOLA (Siape: 3125469 ),  para substituir   ROBERTA
FISCHER CASAGRANDE (Siape: 1395387 ), Coordenador do Núcleo de Planejamento e Gestão, vinculado à
DIMA do DDGP DA PROGESP,  Código FG-2,  em seu afastamento por  motivo de férias,  no período de
27/02/2020 a 06/03/2020, com o decorrente pagamento das vantagens por 9 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
