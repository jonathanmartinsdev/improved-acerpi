Documento gerado sob autenticação Nº HCJ.047.538.6F6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5636                  de  28/06/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a ILSE MARIA ZEN, matrícula
SIAPE nº 0358204, no cargo de Porteiro, nível de classificação C, nível de capacitação IV, padrão 16, do Quadro
desta Universidade, no regime de quarenta horas semanais de trabalho, com exercício na Biblioteca do
Instituto de Física, com proventos integrais. Processo 23078.201289/2017-31.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
