Documento gerado sob autenticação Nº ISA.351.596.731, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9299                  de  14/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LISIANE DE OLIVEIRA PORCIUNCULA, Professor do Magistério
Superior, lotada e em exercício no Departamento de Bioquímica do Instituto de Ciências Básicas da Saúde,
com a finalidade de participar do congresso "Neuroscience 2019", em Chicago, Estados Unidos, no período
compreendido entre 18/10/2019 e 24/10/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa:
diárias). Solicitação nº 86319.
RUI VICENTE OPPERMANN
Reitor
