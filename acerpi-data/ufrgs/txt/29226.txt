Documento gerado sob autenticação Nº RJA.597.656.E6Q, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8247                  de  14/10/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°25223,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TÉCNICO EM CONTABILIDADE,
do Quadro de Pessoal desta Universidade, CLAUDIA DARLENE SCHNEIDER (Siape: 2175598 ),  para substituir  
MONICA FRANCINE DE OLIVEIRA DARDE (Siape: 1651790 ), Coordenador do Núcleo Financeiro da Gerência
Administrativa do Instituto de Artes, Código FG-7, em seu afastamento por motivo de férias, no período de
10/10/2016 a 22/10/2016, com o decorrente pagamento das vantagens por 13 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
