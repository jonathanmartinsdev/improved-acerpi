Documento gerado sob autenticação Nº SDI.394.158.ELC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7587                  de  22/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de
2016, do Magnífico Reitor, e conforme a Solicitação de Afastamento n°84701,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, DANIELA CALLEGARO DE MENEZES (Siape: 2319336 ),
 para substituir   ROGERIO FAE (Siape: 1808981 ), Coordenador da COMGRAD de Administração, Código FUC,
em seu afastamento no país, no período de 22/08/2019 a 26/08/2019, com o decorrente pagamento das
vantagens por 5 dias.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas
