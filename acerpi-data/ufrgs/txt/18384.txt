Documento gerado sob autenticação Nº YOF.534.973.D6C, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1419                  de  25/02/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°28171,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, GABRIEL FARIA MARTINS (Siape: 2185980 ),
 para substituir   GABRIEL DUARTE DA FONSECA (Siape: 1155287 ), Chefe do Setor Acadêmico da Gerência
Administrativa da Escola de Administração,  Código FG-5,  em seu afastamento por motivo de férias,  no
período de 10/02/2016 a 16/02/2016, com o decorrente pagamento das vantagens por 7 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
