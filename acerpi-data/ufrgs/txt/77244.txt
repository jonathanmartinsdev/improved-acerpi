Documento gerado sob autenticação Nº KHB.443.241.8VL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1794                  de  22/02/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de BRUNO NUBENS BARBOSA MIRAGEM, Professor do Magistério
Superior, lotado e em exercício no Departamento de Direito Privado e Processo Civil da Faculdade de Direito,
com a finalidade de proferir palestra no "XX Congreso Argentino de Derecho del Consumidor", em Santa Fé,
Argentina, no período compreendido entre 14/03/2019 e 17/03/2019, incluído trânsito, com ônus limitado.
Solicitação nº 62269.
RUI VICENTE OPPERMANN
Reitor
