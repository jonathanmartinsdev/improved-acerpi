Documento gerado sob autenticação Nº SSO.384.587.8S4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11563                  de  28/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°53713,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, THIAGO GOMES DE CASTRO (Siape: 2321254 ),  para
substituir   GIANA BITENCOURT FRIZZO (Siape: 1716848 ), Chefe do Depto de Psicologia do Desenvolvimento
e da Personalidade do Instituto de Psicologia, Código FG-1, em seu afastamento por motivo de férias, no
período de 02/01/2020 a 23/01/2020, com o decorrente pagamento das vantagens por 22 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
