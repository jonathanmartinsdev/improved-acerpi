Documento gerado sob autenticação Nº HTT.485.628.FLM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10983                  de  10/12/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor DANILO PEDRO STREIT JR, matrícula SIAPE n° 1565281, lotado no Departamento de Zootecnia da
Faculdade de Agronomia e com exercício no Programa de Pós-Graduação em Zootecnia, da classe D  de
Professor Associado, nível 02, para a classe D  de Professor Associado, nível 03, referente ao interstício de
30/07/2017  a  29/07/2019,  com  vigência  financeira  a  partir  de  30/07/2019,  conforme  decisão  judicial
proferida no processo nº 5054491-30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o
que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do
CONSUN. Processo nº 23078.528498/2019-91.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
