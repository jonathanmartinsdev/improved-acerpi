Documento gerado sob autenticação Nº UFN.860.882.84H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3891                  de  27/05/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de Tiago Luis Reis Jalowitzki,  Geólogo, lotado no Instituto de
Geociências  e  com exercício  no  Centro  de  Estudos  em Petrologia  e  Geoquímica,  com a  finalidade de
participar do evento "Goldschmidt 2016", em Yokohama, Japão, no período compreendido entre 24/06/2016
e 01/07/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 19295.
CARLOS ALEXANDRE NETTO
Reitor
