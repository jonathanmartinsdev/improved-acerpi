Documento gerado sob autenticação Nº XXK.113.852.R5S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8696                  de  25/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor GILBERTO BECKER DA
SILVA,  ocupante do cargo de Vigilante-701269, lotado na Coordenadoria de Segurança da UFRGS, SIAPE
0358098,  para 25% (vinte e  cinco por  cento),  a  contar  de 17/09/2019,  tendo em vista  a  conclusão do
curso Superior de Tecnologia de Gestão Pública, conforme o Processo nº 23078.525459/2019-31.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
