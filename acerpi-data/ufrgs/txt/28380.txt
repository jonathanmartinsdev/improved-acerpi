Documento gerado sob autenticação Nº IMX.564.722.UGI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7745                  de  03/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7612, de 29 de setembro de 2016
RESOLVE
Nomear
                  HUGO FRIDOLINO MULLER NETO            SANDRA REGINA CELA            MAIRA COMERLATO            GABRIEL DUARTE DA FONSECA (Suplente)      
para comporem o Núcleo de Gestão de Desempenho da Escola de Administração, por um mandato
de dois anos a contar desta data, com a finalidade de organizar a implementação e a execução do Processo
de Avaliação de Desempenho dos Servidores Técnico-administrativos da UFRGS, observando os termos da
Lei 11.091, de 12/01/2005, e das Decisões 939/2008, 328/2010 e 417/2014 do Conselho Universitário.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
