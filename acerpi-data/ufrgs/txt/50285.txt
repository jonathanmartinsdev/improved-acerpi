Documento gerado sob autenticação Nº NDP.509.569.KMC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             972                  de  01/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°40444,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, KELLY GOULART PAZETTO (Siape: 2201471 ),
 para substituir   SIMONE DA COSTA SALDANHA (Siape: 0357032 ), Diretor do Depto Administrativo e de
Registro da Extensão da PROREXT, Código CD-4, em seu afastamento por motivo de férias, no período de
05/02/2018 a 09/02/2018, com o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
