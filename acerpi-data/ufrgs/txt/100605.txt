Documento gerado sob autenticação Nº FKI.154.822.FR0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10033                  de  07/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora ROBERTA FISCHER CASAGRANDE, ocupante do cargo de  Administrador -
701001, lotada na Pró-Reitoria de Gestão de Pessoas, SIAPE 1395387, o percentual de 30% (trinta por cento)
de Incentivo à Qualificação, a contar de 15/03/2019, tendo em vista a conclusão do curso de Especialização
em Inteligência Estratégica, conforme o Processo nº 23078.527848/2018-11.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
