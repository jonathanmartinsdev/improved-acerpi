Documento gerado sob autenticação Nº BTI.255.482.P57, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8887                  de  04/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar a Portaria n° 5814/2016, de 04/08/2016, que concedeu autorização para afastamento do
País a PAOLO GIULIETTI, Professor do Magistério Superior, com exercício no Departamento de Matemática
Pura e Aplicada do Instituto de Matemática e Estatística
Onde se lê: no período compreendido entre 01 de agosto de 2016 e 31 de janeiro de 2017,
leia-se: no período compreendido entre 01 de agosto e 31 de outubro de 2016, ficando ratificados
os demais termos. Processo nº 23078.506020/2016-67.
RUI VICENTE OPPERMANN
Reitor
