Documento gerado sob autenticação Nº HJE.225.435.GNI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6283                  de  14/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder afastamento parcial, no período de 24/07/2017 a 24/06/2018, para a servidora JUSSARA
SMIDT PORTO, ocupante do cargo de Técnico em Artes Gráficas - 701217, matrícula SIAPE 0357515,  lotada 
na  Secretaria de Comunicação Social, para cursar o Doutorado e Design na UFRGS; conforme o Processo nº
23078.510126/2017-46.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
