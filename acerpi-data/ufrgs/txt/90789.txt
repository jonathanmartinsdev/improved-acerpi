Documento gerado sob autenticação Nº CBL.065.111.MF7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             3890                  de  07/05/2019
Nomeação  da  Representação  Discente  dos
órgãos  colegiados  no  curso  de  Relações
Internacionais  da  Faculdade  de  Ciências
Econômicas  da  UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita no curso de Relações Internacionais na Faculdade de
Ciências Econômicas, com mandato de 01 (um) ano, a contar de 1º de janeiro de 2019, atendendo o disposto
nos artigos 175 e 178 do Regimento Geral da Universidade e 79 do Estatuto da Universidade, e considerando
o processo nº 23078.509678/2019-73, conforme segue:
 
Comissão de Graduação (COMGRAD - Relações Internacionais) 
Titular: Luiza Borges Fortes Leandro
Suplente: Maria Eduarda Variani    
                                     
Colegiado do Departamento de Economia e Relações Internacionais    
Titular: Tiago Carvalho Rodrigues    
Suplente: Geórgia Bernardina de Menezes Gomes
                                     
Plenário do Departamento de Economia e Relações Internacionais       
Titular: Luiza Borges Fortes Leandro
                                     
Conselho da Unidade (CONSUNI - Relações Internacionais)
Titular: Francisca Marques Falcetta 
Suplente: Luiza Borges Fortes Leandro       
                                     
Núcleo de Avaliação da Unidade (NAU)     
Titular: Paolla Grazielly Codignole Souza     
Suplente: Gabriel Gomes Constantino        
                                     
Comissão de Extensão (COMEX)     
Documento gerado sob autenticação Nº CBL.065.111.MF7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Titular: Artur Holzschuh Frantz
Suplente: André Lucas Silva Pereira
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
