Documento gerado sob autenticação Nº QJI.707.002.VD1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7362                  de  18/09/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar no Instituto de Física, com exercício no Setor de Eletrônica do Instituto de Física, WALTER
LEANDRO DIAS OLIVIER ALVES,  nomeado conforme Portaria  Nº  6413/2018 de 17 de agosto de 2018,
publicada no Diário Oficial da União no dia 20 de agosto de 2018, em efetivo exercício desde 17 de setembro
de  2018,  ocupante  do  cargo  de  ENGENHEIRO-ÁREA,  Ambiente  Organizacional  Ciências  Exatas  e  da
Natureza, classe E, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
