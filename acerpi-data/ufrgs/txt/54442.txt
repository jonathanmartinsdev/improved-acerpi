Documento gerado sob autenticação Nº UYU.642.547.2NO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/8
PORTARIA Nº             3657                  de  18/05/2018
  Regulamenta e estabelece critérios para o
pagamento da Gratificação por Encargo de
Curso  ou  Concurso  no  âmbito  da
Universidade  Federal  do  Rio  Grande  do
Sul.
              O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e,
considerando o Decreto 6.114/2007, que regulamenta o pagamento de Gratificação por Encargo de Curso ou
Concurso  de  que  trata  o  art.  76-A  da  Lei  nº  8.112/1990,  na  redação  dada  pela  Lei  nº  11.314/2006;
considerando  a  Portaria  do  Ministério  da  Educação  nº  1.084/2008;  considerando   o  Ofício  nº
267/2017/CGRH/DIFES/SESU/SESU-MEC,
RESOLVE:
Art. 1º Estabelecer critérios e valores, no âmbito da Universidade Federal do Rio Grande do Sul (UFRGS), para
fins de pagamento da Gratificação por Encargo de Curso ou Concurso, devida pela execução eventual de
atividades inerentes a cursos ou concursos públicos.
 
 
Art. 2º A Gratificação a que se refere o art. 1º será paga por hora trabalhada pelo servidor, observados os
valores estabelecidos no Anexo I.
 
§ 1º A hora trabalhada a que se refere o caput deste artigo corresponde a 60 (sessenta) minutos.
 
§ 2º A retribuição do servidor que executar atividades inerentes a cursos ou concursos públicos no âmbito do
Serviço Público Federal não poderá ser superior ao equivalente a cento e vinte horas anuais de trabalho,
ressalvada situação de excepcionalidade, devidamente justificada pelo órgão promotor do curso ou concurso
e previamente aprovada pela autoridade máxima da instituição de origem do servidor, que poderá autorizar
o acréscimo de até cento e vinte horas anuais de trabalho.
 
§ 3º Aplica-se o disposto neste artigo para pagamento aos servidores que atuarem como aplicadores locais
dos testes de proficiência do Programa Idiomas Sem Fronteiras, nos termos do Ofício-Circular nº 04/2013 da
Secretaria de Educação Superior.
 
 
 
 
 
Documento gerado sob autenticação Nº UYU.642.547.2NO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/8
 
Art. 3º A indicação ou a seleção de servidor para execução eventual de atividades que ensejem o pagamento
da  Gratificação  por  Encargo  de  Curso  ou  Concurso  deverá  considerar  critérios  relativos  à  formação
acadêmica e/ou à experiência profissional na área de atuação a que se propuser, pelo desempenho das
atividades descritas nos incisos I e II, do Art. 2º, do Decreto 6.114/2007.
 
 
Art.  4º O pagamento da Gratificação será efetuado após a realização da atividade e a devida instrução
processual.
 
 
Art. 5º No caso de atividades relativas a Concurso Público para a Carreira de Magistério Superior, só farão jus
ao recebimento da Gratificação, nos termos desta Portaria, os servidores que atuarem como membros em
Banca Examinadora e na logística de preparação e de realização do Concurso.
 
§ 1º Os servidores que atuarem como membros de Banca Examinadora de Concursos Públicos para a
Carreira de Magistério Superior,  farão jus ao recebimento da Gratificação para as seguintes atividades,
considerando-se os seguintes limitadores:
 
I.Correção de prova discursiva (Prova Escrita), Exame oral (Conferência, Defesa da Produção Intelectual ou
Defesa e Arguição de Memorial) e Prova Didática, Prova Prática (se houver) e Análise curricular (Exame de
Títulos e Trabalhos), observados os valores constantes da tabela "b" do Apêndice I;
 
II.Execução,  constante da tabela "c"  do Apêndice I,  observada a carga horária máxima de 2 horas por
concurso (1 hora para o ato de instalação do concurso e 1 hora para o ato de encerramento);
 
III.Aplicação, constante da tabela "d" do Apêndice I,  observada a carga horária máxima de 4 horas por
concurso, no caso da prova escrita.
 
§ 2º Os servidores que atuarem na logística de preparação e de realização de Concurso Público para a
Carreira  de  Magistério  Superior  somente  farão  jus  ao  recebimento  de  Gratificação  para  as  seguintes
atividades, considerando-se ainda, os seguintes limitadores:
I.Planejamento, constante da tabela "c" do Apêndice I, observada a carga horária máxima de 8h (oito horas)
por concurso;
 
II.Execução, constante da tabela "c" do Apêndice I, observada a carga horária máxima de 8h por concurso,
somando-se 2,5h por candidato presente em concurso sem prova prática ou 3h por candidato presente em
concurso com prova prática.
 
§ 3º No caso de mais de um servidor atuar na logística de preparação e de realização do Concurso, a soma da
carga horária total dos servidores não poderá ser superior aos limites estabelecidos nos incisos I e II do § 2º
deste artigo.
 
 
Art. 6º No caso de atividades relativas a Concurso Público para a Carreira de Magistério do Ensino Básico,
Técnico e Tecnológico, só farão jus ao recebimento da Gratificação, nos termos desta Portaria, os servidores
que atuarem como membros em Banca Examinadora e na logística de preparação e de realização do
Concurso.
 
 
 
 
Documento gerado sob autenticação Nº UYU.642.547.2NO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
3/8
§ 1º Os servidores que atuarem como membros de Banca Examinadora de Concursos Públicos para a
carreira de Magistério do Ensino Básico, Técnico e Tecnológico farão jus ao recebimento da Gratificação para
as seguintes atividades, considerando-se ainda, os seguintes limitadores:
 
I.Elaboração de questões de prova e Julgamento de recurso, observados os valores constantes da tabela
"b" do Apêndice I (para a Primeira Fase do Concurso);
 
II.Correção de prova discursiva (Prova Escrita), Exame oral (Defesa da Produção Intelectual) e Prova Didática,
Prova  Prática  (se  houver)  e  Análise  curricular  (Exame  de  Títulos  e  Trabalhos),  observados  os  valores
constantes da tabela "b" do Apêndice I;
 
III.Execução, constante da tabela "c" do Apêndice I,  observada a carga horária máxima de 2 horas por
concurso (1 hora para o ato de instalação do concurso e 1 hora para o ato de encerramento);
 
IV.Aplicação, constante da tabela "d" do Apêndice I,  observada a carga horária máxima de 4 horas por
concurso, no caso da prova escrita.
 
§ 2º Os servidores que atuarem na logística de preparação e de realização de Concursos Públicos para a
carreira  de Magistério  do Ensino Básico,  Técnico e  Tecnológico,  somente farão jus  ao recebimento de
Gratificação para as seguintes atividades, considerando-se ainda, os seguintes limitadores:
 
I.Fiscalização e Supervisão, constantes da tabela "d" do Apêndice I (para a Primeira Fase do Concurso);
 
II.Planejamento, constante da tabela "c" do Apêndice I, observada a carga horária máxima de 8h (oito horas)
por concurso;
 
III.Execução, constante da tabela "c" do Apêndice I, observada a carga horária máxima de 8h por concurso,
somando-se 2,5h por candidato presente em concurso sem prova prática ou 3h por candidato presente em
concurso com prova prática.
 
§ 3º No caso de mais de um servidor atuar na logística de preparação e de realização do Concurso, a soma da
carga horária total dos servidores não poderá ser superior aos limites estabelecidos nos incisos II e III do § 2º
deste artigo.
 
 
Art.  7º  No  caso  de  atividades  relativas  ao  procedimento  de  heteroidentificação  complementar  à
autodeclaração dos candidatos negros, para fins de preenchimento das vagas reservadas nos concursos
públicos federais, só farão jus ao recebimento da Gratificação, nos termos desta Portaria, os servidores que
atuarem como membros da Comissão de Heteroidentificação.
 
Parágrafo Único: Os servidores que atuarem como membros da Comissão de Heteroidentificação farão jus
ao recebimento da Gratificação para as seguintes atividades:
 
I.Procedimento de heteroidentificação, constantes da tabela "b" do Apêndice I, apenas aos presentes no
procedimento de heteroidentificação;
 
II.Julgamento de recurso, constantes da tabela "b" do Apêndice I, apenas para os membros que atuarem
como titulares no julgamento do recurso. 
 
 
 
 
Documento gerado sob autenticação Nº UYU.642.547.2NO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
4/8
 
Art.  8º  No  caso  de  atividades  relativas  a  Processo  de  Concessão  de  Reconhecimento  de  Saberes  e
Competências (RSC) para a Carreira de Magistério do Ensino Básico, Técnico e Tecnológico, só farão jus ao
recebimento da Gratificação, nos termos desta Portaria,  os servidores que atuarem como membros de
Comissão Especial de avaliação.
 
Parágrafo único. Os servidores que atuarem como membros de Comissão Especial de avaliação farão jus ao
recebimento da Gratificação para a atividade de Avaliação curricular, constante da tabela "b" do Apêndice I,
observada a carga horária máxima de 3 horas por avaliação.
 
 
Art. 9º No caso de atividades relativas à Aplicação dos Testes de Proficiência do Programa Idiomas sem
Fronteiras,  só  farão jus  ao recebimento da Gratificação,  nos  termos desta  Portaria,  os  servidores  que
atuarem como aplicadores locais dos testes.
 
Parágrafo  único.  Os  servidores  que  atuarem  como  aplicadores  locais,  farão  jus  ao  recebimento  da
Gratificação para as seguintes atividades:
 
I.Execução, constante da tabela "c" do Apêndice I;
 
II.Fiscalização, constante da tabela "d" do Apêndice I.
 
 
Art.  10º Para que o servidor faça jus à retribuição respectiva à atividade desempenhada, o processo de
solicitação de pagamento da Gratificação deverá ser aberto pelo Departamento organizador do concurso ou
pela Direção da Unidade no prazo de até 30 dias após o término das atividades do concurso e deverá estar
instruído com a seguinte documentação, devidamente preenchida e assinada:
 
I.Ofício de solicitação do pagamento, contendo nome completo do servidor, matrícula SIAPE, cargo ocupado,
local de lotação, local de exercício e informações sobre o concurso público (número do edital de abertura de
inscrições, área/subárea do concurso, período de realização);
 
II.Termo de Responsabilidade e Compromisso, conforme modelo constante do Apêndice II;
 
III.Declaração de Execução de Atividades, conforme modelo constante do Anexo I, na qual o servidor deverá
indicar a carga horária e as atividades anteriores que lhe tenham ensejado o pagamento da Gratificação no
ano corrente;
 
IV.Planilha de Cálculo da gratificação;
 
V.Cópia do Cronograma Final do concurso público;
 
VI.Cópia do Parecer Final do concurso público;
 
VII.Plano de Compensação de Carga Horária, no qual o servidor indicará às chefias imediata e ascendente o
seu planejamento de compensação da carga horária na qual esteve afastado para atividades relacionadas ao
concurso público.
 
Parágrafo único.  Ao término do período para compensação da carga horária informada no documento
constante do inciso VII do Art. 10º:
 
Documento gerado sob autenticação Nº UYU.642.547.2NO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
5/8
I.Cabe ao servidor que atuou no concurso juntar ao processo a declaração de compensação de carga horária
ou o comprovante da compensação extraído do Sistema de Ponto Eletrônico e enviá-lo à PROGESP;
 
II.Cabe à PROGESP anexar ao processo despacho de encerramento do mesmo e encaminhá-lo ao Arquivo
Central.
 
 
Art.  11 No caso de atividades relativas a cursos,  entendidos nos termos desta Portaria como ações de
capacitação,  farão  jus  ao  recebimento  da  Gratificação  os  servidores  que  desempenharem  atividades
pedagógicas ou logísticas.
 
§1º Dentre as atividades pedagógicas, incluem-se coordenação técnica e pedagógica, instrutoria,  tutoria,
orientação de trabalhos e elaboração de materiais didáticos, constantes da tabela "a" do Apêndice I.
 
§ 2º Dentre as atividades logísticas, incluem-se aquelas descritas na tabela "c" do Apêndice I: planejamento,
coordenação, supervisão e execução de atividades-meio relativas às ações de capacitação.
 
§  3º  A  carga horária  máxima para cada atividade será  definida considerando a  sua natureza e  a  sua
complexidade,  conforme pactuação em reunião pedagógica  com o coordenador  técnico-pedagógico ou
instrutor.
 
§ 4º Em relação a atividades internas, executadas por servidores pertencentes ao quadro de pessoal da
UFRGS, para que o servidor faça jus à retribuição respectiva, o processo de solicitação de pagamento da
Gratificação deverá ser aberto pela PROGESP, em nome do servidor, antes da realização da ação, e deverá
conter a seguinte documentação:
 
I.Documento orientativo da PROGESP, endereçado ao servidor que realizará a atividade, com explicações
acerca do preenchimento do Termo de Responsabilidade e Compromisso, da Declaração de Execução de
Atividades e do Plano de Compensação;
 
II.Termo  de  Responsabilidade  e  Compromisso,  no  qual  o  servidor  deverá  indicar  as  atividades
desempenhadas  no  ambiente  de  trabalho  (relativas  ao  cargo  ou  função  que  ocupa)  e  providenciar  a
assinatura da chefia imediata e do Diretor da Unidade concordando com sua liberação para a realização de
atividades eventuais pagas pela GECC;
 
III.Declaração de Execução de Atividades, na qual o servidor deverá indicar a carga horária e as atividades
anteriormente desempenhadas no ano corrente, que lhe tenham ensejado o pagamento da Gratificação;
 
IV.Projeto da ação de capacitação, no qual são apresentados os objetivos, conteúdos e informações sobre o
evento que justificará o pagamento da Gratificação;
 
V.Plano de Compensação de Carga Horária, no qual o servidor indicará às chefias imediatas e ascendentes o
seu planejamento de compensação da carga horária na qual esteve afastado para atividades relacionadas a
cursos;
 
VI.Termo  de  Cessão  de  Direitos  Autorais,  nos  casos  em  que  houver  pagamento  da  Gratificação  para
elaboração de material didático e de material multimídia para cursos presenciais ou a distância.
 
§ 5º Em até 5 (cinco) dias após a finalização da ação, cabe ao servidor encaminhar à EDUFRGS (Escola de
Desenvolvimento de Servidores da UFRGS) os seguintes documentos:
 
 
Documento gerado sob autenticação Nº UYU.642.547.2NO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
6/8
 
I.Listas de frequência;
 
II.Relatório final da ação de capacitação;
 
III.Formulários de avaliação de reação.
 
§ 6º Após o recebimento da documentação elencada no parágrafo 5º deste artigo, cabe a PROGESP anexar ao
processo os seguintes documentos:
 
I.Ofício de solicitação do pagamento;
 
II.Planilha de cálculo do pagamento;
 
III.Relatório de aproveitamento da ação;
 
IV.Relatório de Execução da Ação de Aperfeiçoamento.
 
§ 7º Ao término do ano de exercício em que foi realizada a atividade:
 
I.Cabe à PROGESP anexar ao processo a declaração de compensação da carga horária desempenhada e
remunerada via Gratificação por Encargo de Curso ou Concurso, a ser assinada pelo servidor e sua respectiva
chefia imediata. Após assinaturas, o servidor deverá encaminhar o processo ao Arquivo Central.
 
II.Cabe ao servidor que atuou em cursos juntar ao processo a comprovação de compensação de carga
horária ou o comprovante da compensação extraído do Sistema de Ponto Eletrônico.
 
§  8º  A  solicitação  de  pagamento  da  Gratificação  relativa  as  atividades  inerentes  a  cursos  só  serão
encaminhadas  pela  EDUFRGS  à  Divisão  de  Pagamento  e  Recolhimento  após  o  recebimento  de  toda
documentação  solicitada,  ou  seja,  após  a  completa  instrução  processual,  considerando  também  o
cronograma de lançamentos da Folha de Pagamento.
 
 
Art.  12  Em  relação  a  atividades  internas,  executadas  por  servidores  lotados  em  outras  instituições
pertencentes a administração pública federal, autárquica ou fundacional, que executarem, no âmbito da
UFRGS,  atividades  ensejadoras  da  Gratificação,  o  pagamento  se  dará  por  transferência  de  recursos
orçamentários, via Sistema Integrado de Administração Financeira (SIAFI), para a instituição de origem do
servidor, para a qual deverá ser encaminhada cópia do processo administrativo correspondente.
 
 
Art. 13 Em relação a atividades externas, executadas por servidores pertencentes ao quadro de pessoal da
UFRGS, o pagamento só será efetivado após a transferência orçamentária da instituição promotora e o
recebimento dos seguintes documentos:
 
I.Ofício informando a descentralização do recurso;
 
II.Cópia da Declaração de Execução de Atividades;
 
III.Cópia do Termo de Responsabilidade e Compromisso com assinatura das chefias imediata e ascendente;
 
Documento gerado sob autenticação Nº UYU.642.547.2NO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
7/8
IV.Cálculo do pagamento especificando a carga horária desempenhada e o valor da hora trabalhada;
 
V.Cópia de Portaria ou documento normativo que regulamente o pagamento da GECC no órgão em que a
atividade foi executada;
 
VI.Comprovante de Transferência Orçamentária.
 
Parágrafo Único.  Cabe ao servidor que desempenhou a atividade ensejadora da Gratificação solicitar a
documentação elencada no Art. 13º junto à Instituição Promotora e posteriormente encaminhar a mesma à
PROGESP, mediante abertura de processo individual,  ou seja,  um processo por servidor,  para que seja
providenciado o pagamento.
 
 
Art. 14 A conferência da correta instrução processual dos processos relativa à Gratificação de Encargo de
Curso ou Concurso será de competência:
 
I.Da PROGESP, no caso de atividades realizadas no âmbito da UFRGS;
 
II.Da Instituição Promotora, no caso de participação de servidores da UFRGS em atividades promovidas em
outros Órgãos da Administração Pública Federal, Direta, Autárquica ou Fundacional.
 
 
Art. 15 A efetivação do pagamento desta Gratificação ficará sob responsabilidade da PROGESP e ocorrerá
somente após a correta instrução processual.
 
 
Art. 16 É vedado o pagamento da Gratificação:
 
I.Para servidores em gozo de férias, afastamentos ou quaisquer licenças, remuneradas ou não, e pessoas
físicas não vinculadas à Administração Pública Federal;
 
II.Pelo desempenho de atividades inerentes às atribuições do cargo ou função ocupada pelo servidor;
 
III.Pela participação em banca examinadora de processo seletivo simplificado para contratação de professor
substituto;
 
IV.Pela realização de treinamentos em serviço ou por eventos de disseminação de conteúdos relativos às
competências das unidades organizacionais.
 
 
Art. 17 A Gratificação a que se refere esta Portaria somente poderá ser paga se as atividades ensejadoras do
seu pagamento forem exercidas sem prejuízo das atribuições do cargo de que o servidor for titular, devendo
ser objeto de compensação de carga horária quando desempenhadas durante a jornada de trabalho, na
forma do § 4º do art. 98 da Lei nº 8.112/1990.
 
§ 1º É da chefia imediata e da direção da Unidade a responsabilidade pela liberação do servidor e pelo
acompanhamento  da  compensação das  horas  a  que  se  refere  o  caput,  podendo a  PROGESP aferir  a
efetivação da compensação através do Sistema de Registro de Ponto adotado pela Universidade.
 
§ 2º O servidor que não compensar as horas trabalhadas e remuneradas via gratificação em até 12 meses,
conforme disposto no art. 98 da Lei nº 8112/1990, deverá restituir, mediante Guia de Recolhimento da União
Documento gerado sob autenticação Nº UYU.642.547.2NO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
8/8
- GRU, o valor recebido referente às horas não compensadas e ficará impedido de receber novo pagamento
da mesma espécie até que o valor seja restituído ao erário.
 
 
Art. 18 É vedada a incorporação da Gratificação a que se refere esta Portaria ao vencimento básico do
servidor, para qualquer efeito, inclusive para cálculo de proventos de aposentadoria.
 
 
Art. 19 Os casos omissos serão examinados pela PROGESP.
 
 
Art. 20 Esta Portaria entrará em vigor a partir desta data.
 
 
Art. 21 Fica revogada a Portaria nº 5027, de 07 de junho de 2017.
RUI VICENTE OPPERMANN,
Reitor.
