Documento gerado sob autenticação Nº ZZI.265.284.80P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10842                  de  28/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  FERNANDO  SALDANHA  THOME,  matrícula  SIAPE  n°  0357440,  lotado  no
Departamento de Medicina Interna da Faculdade de Medicina, como Chefe Substituto do Depto de Medicina
Interna  da  Faculdade  de  Medicina,  para  substituir  automaticamente  o  titular  desta  função  em  seus
afastamentos ou impedimentos regulamentares no período de 03/12/2017 até 02/12/2019. Processo nº
23078.522210/2017-11.
RUI VICENTE OPPERMANN
Reitor.
