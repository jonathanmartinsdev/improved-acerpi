Documento gerado sob autenticação Nº UIC.853.165.H6F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6758                  de  30/08/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ENEIDA REJANE RABELO DA SILVA, Professor do Magistério
Superior,  lotada  e  em  exercício  no  Departamento  de  Enfermagem  Médico-Cirúrgica  da  Escola  de
Enfermagem, com a finalidade de realizar  visita  à University of Michigan, Estados Unidos, no período
compreendido entre 12/09/2018 e 14/09/2018, incluído trânsito, com ônus limitado. Solicitação nº 58736.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
