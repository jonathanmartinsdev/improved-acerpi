Documento gerado sob autenticação Nº GZH.677.721.SLJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             290                  de  10/01/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°43145,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, SIMONE SARMENTO (Siape: 1805369 ),  para substituir  
LILIAM RAMOS DA SILVA (Siape: 1937112 ), Chefe do Depto de Línguas Modernas do Instituto de Letras,
Código FG-1, em seu afastamento por motivo de férias, no período de 14/01/2019 a 10/02/2019, com o
decorrente pagamento das vantagens por 28 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
