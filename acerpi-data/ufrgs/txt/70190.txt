Documento gerado sob autenticação Nº CDM.772.110.DBV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7706                  de  27/09/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, considerando o disposto na Portaria nº.7678, de 30 de setembro de 2016, e conforme a
Solicitação de Férias n°42554,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, GABRIELA PEREIRA LOPES (Siape: 1069605 ),
 para substituir   TANIRA RODRIGUES SOARES (Siape: 1758398 ), Assessor do Reitor, Código CD-4, em seu
afastamento por motivo de férias, no período de 27/09/2018 a 05/10/2018, com o decorrente pagamento das
vantagens por 9 dias.
JANE FRAGA TUTIKIAN
Vice-Reitora.
