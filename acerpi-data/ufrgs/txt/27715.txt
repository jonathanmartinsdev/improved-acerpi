Documento gerado sob autenticação Nº MCS.085.411.76I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7328                  de  15/09/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Declarar vaga, a partir de 08/09/2016, a função de Secretária do Depto de Educação Física da ESEF,
Código SRH 766, Código FG-7, desta Universidade, tendo em vista a Aposentadoria c/ proventos Integrais (Art.
3º Inc. I,  II,  III)  EC 47/05 de ISABEL CRISTINA OLIVEIRA GARCIA,  matrícula SIAPE n° 0352825, conforme
Portaria n° 6946/2016, de 6 de setembro de 2016, publicada no Diário Oficial da União do dia 08/09/2016.
Processo nº 23078.020553/2016-56.
RUI VICENTE OPPERMANN
Vice-Reitor
