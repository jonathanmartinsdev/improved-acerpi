Documento gerado sob autenticação Nº UKL.539.683.HP4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9617                  de  02/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Remover, a partir de 14 de outubro de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
CAMILA SIMONETTI, ocupante do cargo de Engenheiro-área, Ambiente Organizacional Infraestrutura, Código
701031,  Classe E,  Nível  de Capacitação IV,  Padrão de Vencimento 05,  SIAPE nº.  1733930 da Escola  de
Engenharia para a lotação Superintendência de Infraestrutura, com novo exercício na Prefeitura Campus
Centro.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
