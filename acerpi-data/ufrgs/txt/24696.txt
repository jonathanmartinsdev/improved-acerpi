Documento gerado sob autenticação Nº YAL.211.514.9SN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5542                  de  25/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, BASILIO ALBERTO SARTOR, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 69/2016 de 29 de Junho de 2016, homologado em 30 de junho
de 2016 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de
28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do
Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro
de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento
de Comunicação da Faculdade de Biblioteconomia e Comunicação, em vaga decorrente da aposentadoria da
Professora Rosa Nivea Pedroso, código nº 275808, ocorrida em 3 de Agosto de 2015, conforme Portaria
nº. 5763 de 28 de julho de 2015, publicada no Diário Oficial da União de 03 de agosto de 2015.  Processo nº.
23078.024390/2015-08.
CARLOS ALEXANDRE NETTO
Reitor
