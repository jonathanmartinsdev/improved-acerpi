Documento gerado sob autenticação Nº WLS.333.289.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7634                  de  24/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de AURORA CARNEIRO ZEN, Professor do Magistério Superior,
lotada e em exercício no Departamento de Ciências Administrativas da Escola de Administração, com a
finalidade de participar de reuniões junto à University of Amsterdam, em Amsterdam, Holanda, no período
compreendido entre 20/10/2018 e 22/10/2018, com ônus limitado e à Università di Bologna, em Bologna,
Itália, no período compreendido entre 23/10/2018 e 28/10/2018, com ônus CAPES/PROAP. Solicitação nº
59687.
RUI VICENTE OPPERMANN
Reitor
