Documento gerado sob autenticação Nº HWK.679.147.C0A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8538                  de  12/09/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a LUIS FERNANDO LEITE
RODRIGUES, matrícula SIAPE nº 0356200, no cargo de Vigilante, nível de classificação D, nível de capacitação
IV, padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de trabalho, com
exercício na Biblioteca do Instituto de Artes, com proventos integrais. Processo 23078.015767/2017-91.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
