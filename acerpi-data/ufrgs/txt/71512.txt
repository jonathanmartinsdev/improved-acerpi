Documento gerado sob autenticação Nº KWB.152.277.PLL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8720                  de  29/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  à  servidora  CAROLINE  RAMOS
MACHADO WEBER,  ocupante do cargo de Assistente em Administração-701200,  lotada no Instituto de
Informática, SIAPE 2404319, para 30% (trinta por cento), a contar de 11/09/2018, tendo em vista a conclusão
do curso de Especialização em Gestão Estratégica de Pessoas, conforme o Processo nº 23078.524244/2018-
12.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
