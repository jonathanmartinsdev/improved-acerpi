Documento gerado sob autenticação Nº GUH.998.767.LIB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 1290/2019-PROGESP Porto Alegre, 15 de agosto de 2019.
Senhora Diretora, 
Encaminhamos  a  servidora  Maria  Teresa  Anselmo  Olinto  ocupante  do  cargo  Professor  do
Magistério Superior, para exercício nessa Unidade.
Para confecção da Portaria de Lotação, solicitamos que nos sejam encaminhadas, em até 03 dias, as
seguintes informações:               
-  lotação e exercício (Departamento), observando a hierarquia dos órgãos registrados no SRH;
-  área de atuação do servidor docente;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal;
- data de efetiva apresentação do servidor na Unidade.
Informamos que este encaminhamento se faz para reposição de vaga, em virtude de vacância da
servidora Aline Petter Schneider, publicada no Diário Oficial da União em 15/08/2019.
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
Ilma Sra
Professora Lucia Maria Kliemann,
Diretora da Faculdade de Medicina
Nesta Universidade.
