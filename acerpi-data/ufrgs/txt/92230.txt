Documento gerado sob autenticação Nº YXG.138.432.98P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4790                  de  05/06/2019
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
Designar os servidores,  HENRIQUE GOMES DOS SANTOS, ocupante do cargo de Engenheiro-área,
lotado na Superintendência  de  Infraestrutura  e  com exercício  na  Oficinas  de  Produção e  Manutenção
Mecânica, ALEXANDRE DE LIMA,  ocupante do cargo de Engenheiro-área, lotado na Superintendência de
Infraestrutura e com exercício na Prefeitura Campus Saúde e Olímpico, RICARDO DEMETRIO DE SOUZA
PETERSEN, ocupante do cargo de Professor do Magistério Superior, lotado no Departamento de Educação
Física, Fisioterapia e Dança da Escola de Educação Física, Fisioterapia e Dança e com exercício na Escola de
Educação Física,  Fisioterapia e Dança,  para sob a presidência do primeiro,  constituirem a Comissão de
Recebimento, em atendimento à alínea "b" do inciso I do artigo 73 da Lei 8666/93, para emissão do Termo de
Recebimento Definitivo da Obra do contrato 055/2016, cujo objeto é "FORNECIMENTO E INSTALAÇÃO DE 2
PLATAFORMAS PARA OS PRÉDIOS DA ADMINISTRAÇÃO E GINÁSIO 1", com prazo de 30 dias quando se
encerra o prazo de vigência do contrato. Processo nº 23078.201656/2016-15.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
