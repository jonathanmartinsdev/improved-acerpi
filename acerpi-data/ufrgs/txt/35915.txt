Documento gerado sob autenticação Nº DLK.427.437.LDQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2586                  de  24/03/2017
A VICE-REITORA, NO EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de
suas atribuições legais e estatutárias no uso de suas atribuições, considerando o disposto na Portaria nº
7624, de 29 de setembro de 2016
RESOLVE
Autorizar o afastamento do país de EDUARDO CESAR TONDO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ciências dos Alimentos do Instituto de Ciências e Tecnologia de
Alimentos, com a finalidade de participar de reunião junto ao Laboratory Corporation of America, em St. Paul,
Estados Unidos,  no período compreendido entre 22/04/2017 e 27/04/2017,  incluído trânsito,  com ônus
limitado. Solicitação nº 26733.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria
