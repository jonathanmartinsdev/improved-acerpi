Documento gerado sob autenticação Nº JUA.220.372.HIM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4447                  de  22/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/05/2019,   referente  ao  interstício  de
01/03/2005 a 15/05/2019, para o servidor JULIO DA COSTA ROCHA,  ocupante do cargo de Jardineiro -
701638, matrícula SIAPE 0358033,  lotado  na  Escola de Educação Física, Fisioterapia e Dança, passando do
Nível de Classificação/Nível de Capacitação B I, para o Nível de Classificação/Nível de Capacitação B II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.512812/2019-13:
ILB - ÉTICA E ADMINISTRAÇÃO PÚBLICA CH: 40 (16/04/2019 a 06/05/2019)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
