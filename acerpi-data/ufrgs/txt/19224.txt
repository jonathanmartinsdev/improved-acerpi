Documento gerado sob autenticação Nº VPK.604.952.2LJ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1927                  de  14/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°17769,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, NELSON LUIZ SAMBAQUI GRUBER (Siape: 6354473 ),
 para substituir   ROBERTO VERDUM (Siape: 0357046 ), Coordenador do PPG em Geografia, Código FUC, em
seu afastamento  do país,  no  período de  07/03/2016 a  11/03/2016,  com o  decorrente  pagamento  das
vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
