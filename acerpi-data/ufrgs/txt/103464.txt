Documento gerado sob autenticação Nº FWO.054.570.FOE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             355                  de  10/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Tornar insubsistente a Portaria nº 259/2020, de 07/01/2020, que designou LIDIANE ALVES MENDES,
ocupante do cargo de Assistente em Administração, lotada no Instituto de Biociências e com exercício no
Divisão de Recursos Humanos da Gerência Administrativa do Instituto de Biociências, como Chefe da Divisão
Administrativa da Gerência Administrativa do Instituto de Biociências, Código FG-7, Código SRH 607.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
