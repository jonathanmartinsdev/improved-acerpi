Documento gerado sob autenticação Nº FLW.515.294.USB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10266                  de  13/11/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, ao
Professor  RODRIGO  SABALLA  DE  CARVALHO,  matrícula  SIAPE  1880288,  lotado  e  em  exercício  no
Departamento de Estudos Especializados da Faculdade de Educação, da classe A  de Professor Adjunto A,
nível 02, para a classe C  de Professor Adjunto, nível 01, com vigência financeira a partir de 11/11/2019, de
acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554,
de  20  de  junho  de  2013  do  Ministério  da  Educação  e  a  Decisão  nº  331/2017.  Processo  nº
23078.531302/2019-45.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
