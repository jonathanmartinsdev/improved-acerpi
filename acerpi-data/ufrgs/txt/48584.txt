Documento gerado sob autenticação Nº MUL.781.237.25J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11468                  de  26/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor RAFAEL ARENHALDT, matrícula SIAPE n° 2816248, lotado e em exercício no Departamento de
Estudos Especializados da Faculdade de Educação, da classe A  de Professor Adjunto A, nível 01, para a classe
A  de Professor Adjunto A,  nível  02,  referente ao interstício de 16/09/2015 a 15/09/2017, com vigência
financeira a partir de 16/09/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo
nº 23078.512635/2017-11.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
