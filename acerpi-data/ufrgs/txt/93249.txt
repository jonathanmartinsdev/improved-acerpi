Documento gerado sob autenticação Nº AGR.003.943.KK2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5405                  de  26/06/2019
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
             Retificar a Portaria nº 5354 de 25/06/2019 que nomeou a Representação Discente do Conselho
Universitário da UFRGS, conforme segue:
 
Onde se lê: 
"7º Suplente: João Pedro Junk Rocha"
 
Leia-se:
"7º Suplente: João Pedro Juk Rocha"
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
