Documento gerado sob autenticação Nº EFB.345.780.QU3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3504                  de  14/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°40524,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, GIOVANI LOCK GOMES (Siape: 0356709 ),  para
substituir   ANA LÚCIA DIAS GRAEFF (Siape: 1098970 ), Coordenador do Núcleo de Apoio Administrativo e
Departamental da GA da Faculdade de Educação, Código FG-4, em seu afastamento por motivo de férias, no
período de 21/05/2018 a 31/05/2018.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
