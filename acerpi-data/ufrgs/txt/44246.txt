Documento gerado sob autenticação Nº VPP.003.310.Q42, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8399                  de  08/09/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar na Faculdade de Veterinária, com exercício no Núcleo de Ensino de Graduação da Faculdade
de Veterinária, VANDERLEI SOUTO DOS SANTOS, nomeado conforme Portaria Nº 6875/2017 de 28 de julho
de 2017, publicada no Diário Oficial da União no dia 31 de julho de 2017, em efetivo exercício desde 21 de
agosto de 2017, ocupante do cargo de TÉCNICO EM ASSUNTOS EDUCACIONAIS, Ambiente Organizacional
Ciências  Humanas,  Jurídicas  e  Econômicas,  classe  E,  nível  I,  padrão 101,  no  Quadro  de  Pessoal  desta
Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
