Documento gerado sob autenticação Nº IDA.161.717.RNR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             7065                  de  09/09/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir da data deste ato, como segue:
Criar:  
 - Gerente Administrativo da Escola de Engenharia, Código FG-1;
 - Coordenador de Núcleos Especiais da Escola de Engenharia, Código FG-2.
Transformar: 
 - Assessor Administrativo da Escola de Engenharia, Código FG-4, em Coordenador de Gabinete da
Escola de Engenharia, Código FG-4;
-  Secretário  do  Departamento  de  Engenharia  Civil  da  Escola  de  Engenharia,  Código  FG-7,  em
Coordenador do Setor de Infraestrutura e Tecnologia da Informação da Gerência Administrativa da Escola de
Engenharia, Código FG-7;
 - Secretário do Departamento de Engenharia de Minas da Escola de Engenharia, Código FG-7, em
Coordenador do Setor de Gestão de Pessoas da Gerência Administrativa da Escola de Engenharia, Código FG-
7;
 - Secretário do Departamento de Engenharia de Produção e Transportes da Escola de Engenharia,
Código FG-7,  em Coordenador do Setor  de Apoio Acadêmico da Gerência  Administrativa  da Escola  de
Engenharia, Código FG-7;
 - Secretário do Departamento de Engenharia Elétrica da Escola de Engenharia, Código FG-7, em
Coordenador  do Setor  de Controle  e  Execução Orçamentária  da Gerência  Administrativa  da Escola  de
Engenharia, Código FG-7;
 - Secretário do Departamento de Engenharia Mecânica da Escola de Engenharia, Código FG-7, em
Coordenador do Setor de Infraestrutura da Gerência Administrativa da Escola de Engenharia, Código FG-7;
 - Secretário do Departamento de Engenharia Química da Escola de Engenharia, Código FG-7, em
Coordenador  do  Setor  de  Gestão  e  Análise  de  Informações  da  Gerência  Administrativa  da  Escola  de
Engenharia, Código FG-7;
Documento gerado sob autenticação Nº IDA.161.717.RNR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
 - Secretário do Departamento de Materiais da Escola de Engenharia, Código FG-7, em Coordenador
do Setor Administrativo do Campus Centro da Gerência Administrativa da Escola de Engenharia, Código FG-7;
 -  Secretário  do  Departamento  de  Metalurgia  da  Escola  de  Engenharia,  Código  FG-7,  em
Coordenador  do  Setor  Administrativo  do  Campus  do  Vale  da  Gerência  Administrativa  da  Escola  de
Engenharia, Código FG-7.
  Processo nº 23078.040027/2014-41.
 
CARLOS ALEXANDRE NETTO
Reitor
