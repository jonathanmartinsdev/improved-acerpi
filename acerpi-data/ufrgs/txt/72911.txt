Documento gerado sob autenticação Nº LZH.261.534.M69, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9718                  de  30/11/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor CIRILO SEPPI BRESOLIN, matrícula SIAPE n° 1054852, lotado e em exercício no Departamento de
Engenharia Mecânica da Escola de Engenharia, da classe A  de Professor Adjunto A, nível 01, para a classe A 
de Professor Adjunto A, nível 02, referente ao interstício de 14/03/2016 a 13/03/2018, com vigência financeira
a partir de 13/07/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.507975/2018-01.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
