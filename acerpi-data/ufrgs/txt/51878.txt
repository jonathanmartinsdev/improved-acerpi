Documento gerado sob autenticação Nº BKW.882.916.QOK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2002                  de  16/03/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de EDUARDO GUIMARAES BARBOZA,  Professor do Magistério
Superior,  lotado  no  Departamento  de  Paleontologia  e  Estratigrafia  do  Instituto  de  Geociências  e  com
exercício na Comissão de Graduação de Ciências Biológicas Marinha, com a finalidade de realizar trabalho de
campo junto à Universidad de la República, em Rocha, Uruguai, no período compreendido entre 08/04/2018
e 14/04/2018, incluído trânsito, com ônus limitado. Solicitação nº 34416.
RUI VICENTE OPPERMANN
Reitor
