Documento gerado sob autenticação Nº ICM.375.700.CKF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4763                  de  04/07/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.502474/2018-21  e
23078.511069/2018-01, do contrato nº 009/2018, da Lei 10.520/02 e ainda da Lei 8.666/93,
RESOLVE:
 
Aplicar a sanção administrativa de MULTA 5% (cinco por cento) sobre o valor da parcela que deu causa,
informada pela DAL no doc. SEI nº 1066131, no montante de R$ 22,32 (vinte e dois reais e trinta e dois
centavos), prevista no inciso III do item 3 da cláusula décima primeira do Contrato, à Empresa COMERCIAL
SOUZA DE ALIMENTOS LTDA,  CNPJ  n.º  06.354.555/0001-18,  pela  entrega de produtos  com qualidade
inadequada, conforme atestado pela fiscalização do contrato, nos docs. SEI nº 0997861 e 1013639, bem como
pelo NUDECON, no doc. SEI nº1041179, do processo 23078.511069/2018-01.
 
Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
