Documento gerado sob autenticação Nº NNB.153.519.T77, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6796                  de  30/08/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de Joel Luis Carbonera, Professor do Magistério Superior, lotado e
em exercício no Instituto de Informática, com a finalidade de participar da "20th International Conference on
Big Data Analytics and Knowledge Discovery", em Regensburg, Alemanha, no período compreendido entre
01/09/2018 e 08/09/2018, incluído trânsito, com ônus limitado. Solicitação nº 58945.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
