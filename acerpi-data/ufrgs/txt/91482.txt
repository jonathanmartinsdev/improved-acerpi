Documento gerado sob autenticação Nº ZAX.598.091.S1C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4290                  de  17/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Tornar insubsistente a Portaria nº 3904/2019, de 07/05/2019, que designou temporariamente JUNE
MAGDA ROSA SCHARNBERG (Siape: 0351463), para substituir ROSANE BEATRIZ ALLEGRETTI BORGES (Siape:
3367333 ), Chefe da Biblioteca de Engenharia, Código SRH 501, código FG-5, no período de 07/05/2019 à
17/05/2019, gerada conforme Solicitacao de Férias nº 46727. Processo SEI Nº 23078.511723/2019-50
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
