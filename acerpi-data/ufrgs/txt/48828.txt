Documento gerado sob autenticação Nº ZKS.002.298.88H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11675                  de  29/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar no Campus Litoral Norte, com exercício no Divisão de Atendimento ao Aluno do Campus
Litoral Norte, ROSE KELLY PIRES MARINHO, nomeada conforme Portaria Nº 10997/2017 de 06 de dezembro
de 2017, publicada no Diário Oficial da União no dia 11 de dezembro de 2017, em efetivo exercício desde 27
de dezembro de 2017, ocupante do cargo de ASSISTENTE EM ADMINISTRAÇÃO, Ambiente Organizacional
Administrativo, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
