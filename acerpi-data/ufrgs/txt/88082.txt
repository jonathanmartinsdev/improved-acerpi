Documento gerado sob autenticação Nº XBX.593.211.DUE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2292                  de  13/03/2019
  Alterar composição do Conselho Editorial
da  Editora  da  UFRGS,  estabelecida  na
Portaria 1267 de 09 de fevereiro de 2017.
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE:
Alterar a composição do Conselho Editorial da Editora da UFRGS, designada na Portaria 1267 de 09
de fevereiro de 2017, que passa a vigorar com os seguintes membros:  
 
 
      ALEX NICHE TEIXEIRA
      ALVARO ROBERTO CRESPO MERLO
      AUGUSTO JAEGER JUNIOR
      ENIO PASSIANI
      JOSE RIVAIR MACEDO
      LIA LEVY
      MARCIA IVANA DE LIMA E SILVA      
      NAIRA MARIA BALZARETTI
      PAULO CESAR RIBEIRO GOMES
      RAFAEL DE CARVALHO MATIELLO BRUNHARA
      TANIA DENISE MISKINIS SALGADO
 
JANE FRAGA TUTIKIAN,
Vice-Reitora, no exercício da Reitoria.
