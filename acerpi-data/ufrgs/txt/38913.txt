Documento gerado sob autenticação Nº BKT.433.706.MIU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4754                  de  29/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SERGIO LUIZ VIEIRA, Professor do Magistério Superior, lotado e
em exercício no Departamento de Zootecnia da Faculdade de Agronomia, com a finalidade de participar de
reunião  junto  ao  DSM  Nutritional  Products,  em  Bangkok,  Tailândia,  no  período  compreendido  entre
11/06/2017 e 16/06/2017, incluído trânsito, com ônus limitado. Solicitação nº 28068.
RUI VICENTE OPPERMANN
Reitor
