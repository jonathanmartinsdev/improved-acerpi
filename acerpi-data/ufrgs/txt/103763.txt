Documento gerado sob autenticação Nº FLW.543.211.BCD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             523                  de  13/01/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora INGRID KUCHENBECKER,  matrícula SIAPE n° 1435475, lotada no Colégio de Aplicação e com
exercício no Departamento de Comunicação do Colégio de Aplicação, da classe de Professor DIV, nível 03,
para a classe de Professor DIV, nível 04, referente ao interstício de 30/11/2017 a 29/11/2019, com vigência
financeira  a  partir  de  30/11/2019,  conforme  decisão  judicial  proferida  no  processo  nº  5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.533252/2019-31.
JANE FRAGA TUTIKIAN
Vice-Reitora.
