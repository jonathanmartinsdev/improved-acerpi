Documento gerado sob autenticação Nº CTA.310.109.JRS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9408                  de  16/10/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora LEILA RECHENBERG, matrícula SIAPE n° 2090788, lotada e em exercício no Departamento de
Odontologia Preventiva e Social, da classe B  de Professor Assistente, nível 01, para a classe B  de Professor
Assistente, nível 02, referente ao interstício de 18/02/2017 a 17/02/2019, com vigência financeira a partir de
18/02/2019, conforme decisão judicial proferida no processo nº 5054491-30.2018.4.04.7100, da 1ª Vara
Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.520610/2019-45.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
