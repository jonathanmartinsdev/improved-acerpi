Documento gerado sob autenticação Nº WAH.127.623.Q2A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1729                  de  28/02/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Retificar a Portaria n° 1629/2020, de 19/02/2020, que concedeu autorização para afastamento no
País a LAURA SOUZA FONSECA,  Professor do Magistério Superior,  com exercício no Departamento de
Estudos Especializados da Faculdade de Educação
Onde se lê:
com ônus limitado,
leia-se:
com ônus CNPQ.
Ficando ratificados os demais termos. Processo nº 23078.530960/2019-10.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
