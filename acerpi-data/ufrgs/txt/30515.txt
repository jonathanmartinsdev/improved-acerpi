Documento gerado sob autenticação Nº OCB.934.831.35O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9202                  de  18/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº23078.024179/2016-68
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de Analista de Tecnologia da
Informação, do Quadro de Pessoal desta Universidade, MANUELA KLANOVICZ FERREIRA (Siape: 1693403),
 para substituir ZAIDA HOROWITZ (Siape: 0355890 ), Chefe da Divisão de Sistemas de Pesquisa vinculada ao
Depto de Sistemas de Informação do CPD, Código SRH 950, FG-5, em sua Licença Prêmio, no período de
04/11/2016 a 02/01/2017, com o decorrente pagamento das vantagens por 60 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
