Documento gerado sob autenticação Nº MWG.199.983.T55, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5968                  de  07/07/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 28 de junho de 2017,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
RAQUEL CRISTIANE DA SILVA GUIMARAES, ocupante do cargo de Bibliotecário-documentalista,  Ambiente
Organizacional Informação, Código 701010, Classe E, Nível de Capacitação II, Padrão de Vencimento 02, SIAPE
nº. 2248251 do Instituto de Letras para a lotação Escola de Educação Física, Fisioterapia e Dança, com novo
exercício na Biblioteca da Escola de Educação Física.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
