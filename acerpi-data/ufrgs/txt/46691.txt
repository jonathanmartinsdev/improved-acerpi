Documento gerado sob autenticação Nº EAW.871.992.P3L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10128                  de  01/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a CARLA SANTOS FERREIRA,
matrícula  SIAPE  nº  0351479,  no  cargo  de  Técnico  em Contabilidade,  nível  de  classificação  D,  nível  de
capacitação IV,  padrão 16,  do Quadro desta  Universidade,  no regime de quarenta  horas  semanais  de
trabalho,  com exercício  no  Núcleo  de  Gestão  Organizacional  da  Gerência  Administrativa  da  Escola  de
Educação Física, Fisioterapia e Dança, com proventos integrais. Processo 23078.019329/2017-01.
RUI VICENTE OPPERMANN
Reitor.
