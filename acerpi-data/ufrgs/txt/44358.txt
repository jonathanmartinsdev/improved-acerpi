Documento gerado sob autenticação Nº LDJ.993.542.K6I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8738                  de  19/09/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais eestatutárias, de acordo com o processo nº 23078.009024/2016-00, considerando o disposto na Lei nº 8.112,de 11/12/1990, no Decreto Presidencial nº 1.590, de 10/08/1995, no Decreto nº 4.836, de 09/09/2003, naDecisão  nº  432  do  Conselho  Universitário,  de  27/11/2015  e  na  Portaria  3.183  de  12/04/2017  destaUniversidade
RESOLVE
Art.1º Autorizar a jornada de trabalho flexibilizada na Biblioteca da Escola de Engenharia no período
diário de atendimento ao público das 8h00min às 21h00min, pelo prazo de doze meses, conforme disposto
no Art. 11, da Portaria 3.183 de 12/04/2017. Os servidores técnico-administrativos abaixo relacionados terão
jornada de trabalho de seis horas diárias, cumprindo carga horária de trinta horas semanais:
ALEXANDRE CHOW - SIAPE: 016090888
CIRA ADRIANA MARTINS RIBEIRO - SIAPE: 016784121
DEISE MARI ARAÚJO DA SILVA - SIAPE: 21067502
GILBERTO DIPPE RODRIGUES - SIAPE: 003525287
JÚLIA ANGST COELHO - SIAPE: 018567266
KISMARA TERESINHA SILVA - SIAPE: 003568202
LETÍCIA ANGHEBEN EL AMMAR CONSONI - SIAPE: 017438470
MAGDA HELENA BEHRMANN - SIAPE: 018601138
ROSANE BEATRIZ ALLEGRETTI BORGES - SIAPE: 013673335
ROSANGELA HAIDE BRATKOWSKI - SIAPE: 014465086
ROSELI BUENO DA SILVA - SIAPE: 014466600
SABRINA DIEHL MENEZES - SIAPE: 016387805
SILVIA CATARINA ROSSI - SIAPE: 003586995
VERA LÚCIA FAGUNDES LONGARAY - SIAPE: 000501557
Art. 2º Determinar a afixação, nas suas dependências, em local visível e de grande circulação de
usuários dos serviços, bem como no site da Unidade/Órgão, de quadro, permanentemente atualizado, com
escala  nominal  dos  servidores  arrolados no Art.  1º  desta  Portaria,  constando dias  e  horários  de seus
expedientes.
RUI VICENTE OPPERMANN
Reitor.
