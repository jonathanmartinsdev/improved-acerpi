Documento gerado sob autenticação Nº JWG.745.512.FMT, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1300                  de  23/02/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de LAURA BANNACH JARDIM, Professor do Magistério Superior,
lotada e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a finalidade de
realizar visita ao Department of Cell Biology at the University Medical Center Groningen, Holanda, no período
compreendido entre 12/03/2016 e 16/03/2016, incluído trânsito,  com ônus CNPq (Bolsa de Pesquisador
Visitante Especial). Solicitação nº 18048.
CARLOS ALEXANDRE NETTO
Reitor
