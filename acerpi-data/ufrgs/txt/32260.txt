Documento gerado sob autenticação Nº EDY.441.057.H96, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             102                  de  04/01/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito a Portaria nº 9486/2016, de 29/11/2016, publicada no Diário Oficial da União de
30/11/2016 que nomeou em caráter efetivo, MARCIÉLE PEUCKERT LUCHER, para o cargo de Arquivista, de
acordo com o que preceitua o § 6º do artigo 13 da Lei nº. 8.112, de 11 de dezembro de 1990, com a redação
dada pela Lei nº. 9.527, de 10 de dezembro de 1997. Processo nº 23078.020128/13-15.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
