Documento gerado sob autenticação Nº WSV.308.753.BPQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3466                  de  10/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  KATHRIN  LERRER  ROSENFIELD,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Filosofia do Instituto de Filosofia e Ciências Humanas,
com a finalidade de participar de encontro na University of Vienna, Áustria, no período compreendido entre
04/06/2016 e 01/07/2016, incluído trânsito, com ônus limitado. Solicitação nº 18890.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
