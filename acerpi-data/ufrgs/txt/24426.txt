Documento gerado sob autenticação Nº RAN.469.219.O5E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5246                  de  14/07/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Dispensar, a pedido, a partir de 29/06/2016, o(a) ocupante do cargo de Técnico em Secretariado -
701275, do Nível de Classificação DI, do Quadro de Pessoal desta Universidade, VERA ROSANE RODRIGUES
DE OLIVEIRA, CPF nº 55459307015, matrícula SIAPE 0351519 da função de Diretora da Divisão de Moradia
Estudantil  do Departamento de Infraestrutura da PRAE,  Código SRH 967,  Código FG-4,  para a  qual  foi
designado(a) pela Portaria nº 0457/15 de 14/01/2015, publicada no Diário Oficial da União de 16/01/2015.
Processo nº 23078.014646/2016-41.
RUI VICENTE OPPERMANN
Vice-Reitor
