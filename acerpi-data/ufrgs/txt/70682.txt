Documento gerado sob autenticação Nº WZU.067.045.F13, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8068                  de  09/10/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do país  de LEANDRO AZAMBUJA REICHERT,  Professor do Magistério
Superior,  lotado  e  em  exercício  no  Departamento  de  Odontologia  Conservadora  da  Faculdade  de
Odontologia, com a finalidade de, como palestrante, participar do "Congreso de la Universidad San Martin
de  Porres", em Lima, Peru, no período compreendido entre 08/11/2018 e 10/11/2018, incluído trânsito, com
ônus limitado. Solicitação nº 60114.
RUI VICENTE OPPERMANN
Reitor
