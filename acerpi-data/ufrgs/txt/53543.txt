Documento gerado sob autenticação Nº CUU.271.804.6FV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2978                  de  24/04/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor CARLOS HOPPEN, matrícula SIAPE n° 1769606, lotado no Departamento de Matemática Pura e
Aplicada do Instituto  de Matemática  e  Estatística  e  com exercício  no Programa de Pós-Graduação em
Matemática Aplicada, da classe C  de Professor Adjunto, nível 04, para a classe D  de Professor Associado,
nível 01, referente ao interstício de 01/03/2016 a 28/02/2018, com vigência financeira a partir de 19/03/2018,
de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº
331/2017 do CONSUN. Processo nº 23078.503354/2018-41.
PHILIPPE OLIVIER ALEXANDRE NAVAUX
Decano do Conselho Universitário, no exercício da Reitoria.
