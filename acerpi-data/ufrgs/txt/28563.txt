Documento gerado sob autenticação Nº YXW.689.826.U24, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7861                  de  04/10/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a GLORIA
MARIA LEITE, matrícula SIAPE nº 1062821, no cargo de Assistente em Administração, nível de classificação D,
nível de capacitação IV, padrão 13, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho,  com  exercício  na  Seção  de  Cadastro  de  Fornecedores  da  Pró-Reitoria  de  Planejamento  e
Administração, com proventos integrais. Processo 23078.017802/2016-26.
RUI VICENTE OPPERMANN
Reitor
