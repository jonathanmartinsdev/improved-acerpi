Documento gerado sob autenticação Nº OKP.792.666.NGF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8515                  de  18/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  RONI  ANZOLCH,  matrícula  SIAPE  n°  2212312,  lotado  e  em  exercício  no  Departamento  de
Arquitetura da Faculdade de Arquitetura, da classe D  de Professor Associado, nível 01, para a classe D  de
Professor Associado, nível 02, referente ao interstício de 26/09/2017 a 25/09/2019, com vigência financeira a
partir de 26/09/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.514463/2019-74.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
