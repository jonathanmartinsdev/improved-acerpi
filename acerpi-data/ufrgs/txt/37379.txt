Documento gerado sob autenticação Nº QPY.415.357.28D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3590                  de  27/04/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias, 
RESOLVE
Dispensar, a partir de 20/04/2017, a ocupante do cargo de Recepcionista - 701459, do Nível de
Classificação CIII,  do Quadro de Pessoal  desta  Universidade,  MARIA MARGARETH LOPES FERNANDES,
matrícula SIAPE 0358793, da função de Secretária da Câmara de Pesquisa, Código SRH 1261, Código FG-1,
para a qual foi designada pela Portaria nº 0804/13, de 13/02/2013, publicada no Diário Oficial da União de
14/02/2013. Processo nº 23078.007170/2017-73.
RUI VICENTE OPPERMANN
Reitor.
