Documento gerado sob autenticação Nº BRO.699.917.NN4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8242                  de  01/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Homologar o parecer que aprova a servidora técnico-administrativa LUCIANE BRESCIANI LOPES,
ocupante do cargo de Tradutor e Intérprete de Linguagem de Sinais, no estágio probatório cumprido no
período de 19/08/2014 até 19/08/2017, fazendo jus, a partir desta data, à estabilidade no serviço público
federal.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
