Documento gerado sob autenticação Nº CLV.452.639.5B7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
DEPARTAMENTO DE DESENVOLVIMENTO E GESTÃO DE PESSOAS
ESCOLA DE DESENVOLVIMENTO DE SERVIDORES DA UFRGS
Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
(51) 3308-3015/3219/3914/4596 - edufrgs@progesp.ufrgs.br
Processo n°: 23078.528640/2018-19
Servidora: ANA CLAUDIA COSTA DOS SANTOS
Cargo: Revisor de Textos
Lotação: Gráfica Universitária
Ambiente Organizacional: Artes, Comunicação e Difusão
Nível de Classificação e Nível de Capacitação: E III
PARECER N° 1322/2018
Trata  este  expediente  da  retificação  do  Parecer  n°  1294/2018,  de  24/10/2018,  que  concede
progressão  por  capacitação  à  servidora  ANA  CLAUDIA  COSTA  DOS  SANTOS,  Revisor  de  Textos,  com
exercício na Gráfica Universitária da Secretaria de Comunicação Social. Processo nº 23078.528640/2018-19.
 
Onde se lê:
 "Tendo em vista a realização dos cursos abaixo listados:
(...)
ENAP  -  Ética  e  Serviço  Público  -  20  horas  -  31/08/2018  a  21/09/2018  -  Escola  Nacional  de
Administração Pública."
 
leia-se:
"Tendo em vista a realização dos cursos abaixo listados:
(...)
ENAP  -  Ética  e  Serviço  Público  -  20  horas  -  31/08/2018  a  21/09/2018  -  Escola  Nacional  de
Administração Pública.
PROREXT - Francês 5 - 52 horas - 27/03/2015 a 03/07/2015 - Pró-Reitoria de Extensão".
Em 29/10/2018.
BIANCA SPODE BELTRAME
Divisão de Análise e Orientação do Desenvolvimento na Carreira
De acordo.  Proceda-se  à  confecção da  portaria,  para  certificação do Pró-Reitor  de  Gestão de
Pessoas. Após, encaminhe-se à DPR, para inclusão na folha de pagamento; posteriormente à requerente,
para ciência e conclusão do processo.
Em 29/10/2018.
KAREN WERLANG LUNKES
Diretora da Divisão de Análise e Orientação do Desenvolvimento na Carreira
