Documento gerado sob autenticação Nº CUA.642.414.BGD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11127                  de  12/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ANTONIO DAVID CATTANI, Professor do Magistério Superior,
lotado e em exercício no Departamento de Sociologia do Instituto de Filosofia e Ciências Humanas, com a
finalidade de participar de encontro junto ao Collège d'Études Mondiales,  em Paris,  França, no período
compreendido entre 21/01/2018 e 26/01/2018, incluído trânsito, com ônus limitado. Solicitação nº 33379.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
