Documento gerado sob autenticação Nº KBN.298.387.QK1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1057                  de  01/02/2017
        O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO
RIO GRANDE DO SUL, no uso de suas atribuições
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta Universidade,  NILO SERGIO MEDEIROS CARDOZO, matrícula SIAPE n° 2224351,  lotado no
Departamento de Engenharia Química da Escola de Engenharia, como Coordenador Substituto do PPG em
Engenharia  Química,  para substituir  automaticamente o titular  desta função em seus afastamentos ou
impedimentos  regulamentares  no  período  de  09/02/2017  e  até  08/02/2019.  Processo  nº
23078.026356/2016-41.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no Exercício da Reitoria
