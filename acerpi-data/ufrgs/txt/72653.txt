Documento gerado sob autenticação Nº AEB.958.627.A4R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9561                  de  27/11/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°43595,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA,  do Quadro de Pessoal  desta Universidade,  ALEXSANDRO DALLEGRAVE (Siape:  1456353 ),   para
substituir   FABIANA NOGUEIRA GROSSER (Siape: 1504706 ),  Coordenador do Núcleo Técnico Científico-
Central Analítica da Gerência Adm do Instituto de Química, Código FG-7, em seu afastamento por motivo de
férias, no período de 27/11/2018 a 06/12/2018, com o decorrente pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
