Documento gerado sob autenticação Nº WGN.418.483.8A4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7216                  de  13/09/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Dispensar,  a  pedido,  a partir  de 08/09/2016,  o ocupante do cargo de Professor do Magistério
Superior, classe Associado do Quadro de Pessoal desta Universidade, RAFAEL GOMES DIONELLO, matrícula
SIAPE n° 1565975, da função de Chefe do Depto de Fitossanidade da Faculdade de Agronomia, Código SRH
202, código FG-1, para a qual foi designado pela Portaria 8974/2015, de 16/11/2015, publicada no Diário
Oficial da União do dia 18/11/2015. Processo nº 23078.019377/2016-18.
RUI VICENTE OPPERMANN
Vice-Reitor
