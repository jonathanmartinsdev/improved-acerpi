Documento gerado sob autenticação Nº ONY.148.646.94C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3234                  de  13/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar os servidores  CLAUDIO ANTONIO SEVERO GONCALVES, ocupante do cargo de Assistente
em Administração,  lotado na Superintendência  de Infraestrutura e  com exercício  no Departamento de
Logística e Suprimentos,  CRISTIANE DORNELLES REMIAO DIFINI,  ocupante do cargo de Assistente em
Administração,  lotada  na  Pró-Reitoria  de  Gestão  de  Pessoas  e  com exercício  na  Divisão  de  Análise  e
Orientação do Desenvolvimento na Carreira, GISLAINE THOMPSON DOS SANTOS, ocupante do cargo de
Enfermeiro-área, lotada na Pró-Reitoria de Gestão de Pessoas e com exercício na Divisão de Promoção da
Saúdepara comporem a Comissão de Avaliação de Estágio Probatório dos servidores técnico-administrativos,
que tem por finalidade avaliar e emitir parecer para homologação do Estágio Probatório.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
