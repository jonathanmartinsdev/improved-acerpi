Documento gerado sob autenticação Nº TDX.374.766.T6L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1768                  de  22/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  4183,  de 17 de outubro de 2008,  do
Magnífico Reitor, e conforme a Solicitação de Férias n°45422,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ENIO PASSIANI (Siape: 2097762 ),  para substituir  
MELISSA DE MATTOS PIMENTA (Siape: 1880803 ), Chefe do Depto de Sociologia do IFCH, Código FG-1, em seu
afastamento por motivo de férias, no período de 22/02/2019 a 24/02/2019, com o decorrente pagamento das
vantagens por 3 dias.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
