Documento gerado sob autenticação Nº GHT.812.558.LAN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6716                  de  25/07/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5032152-58.2010.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, estabelecido no Anexo I da Portaria n° 1874, de
13/07/2006,  da  servidora  MARIA  DE  LOURDES  DE  VILHENA  ALVES,  matrícula  SIAPE  n°  0357540,
aposentada  no  cargo  de   Auxiliar  de  Enfermagem  -  701411,  para  o  nível  II,  conforme  o  Processo
Administrativo nº 23078.013428/2017-71.
RUI VICENTE OPPERMANN
Reitor.
