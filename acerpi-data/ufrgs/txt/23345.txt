Documento gerado sob autenticação Nº PRP.618.958.ELD, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4379                  de  14/06/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Remover, a partir de 25 de maio de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
JULIA LÂNGARO BECKER, ocupante do cargo de Psicólogo-área, Ambiente Organizacional Ciências da Saúde,
Código 701060, Classe E, Nível de Capacitação IV, Padrão de Vencimento 05, SIAPE nº. 1757206 da Pró-
Reitoria de Gestão de Pessoas para a lotação Escola de Enfermagem, com novo exercício na Secretaria do
Programa de Programa de Pós-Graduação em Saúde Coletiva.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
