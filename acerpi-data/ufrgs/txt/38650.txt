Documento gerado sob autenticação Nº DMI.025.345.D67, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             4550                  de  22/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Nomear, em caráter efetivo, no cargo abaixo, na vaga referente ao respectivo código, em virtude de
habilitação em Concurso Público, conforme Edital Nº 29/2017 de 12 de Maio de 2017, homologado em 15 de
maio de 2017 e de acordo com o artigo 9º, item I, da Lei nº. 8.112, de 11 de dezembro de 1990, com as
alterações dadas pela Lei nº. 11.091, de 12 de janeiro de 2005, publicada no Diário Oficial da União de 13 de
janeiro de 2005, Decreto nº. 7.232, de 19 de julho de 2010, publicado no Diário Oficial da União de 20 de julho
de 2010, Lei 12.772, de 28 de dezembro de 2012, publicada no Diário Oficial da União de 31 de dezembro de
2012. Processo nº 23078.025182/2016-07.
 
 
AUXILIAR DE SAÚDE - Classe C - Padrão 1
 
- LUCIANI DE ALMEIDA MOREIRA - Vaga SIAPE 301415
- JULIANA PERUCA - Vaga SIAPE 673669
- VIVIANE FERNANDES SILVEIRA - Vaga SIAPE 674969
- BARBARA MATZENBACHER DUARTE FALCÃO - Vaga SIAPE 674983
- LUIZ CARLOS ABREU QUINTEIRO - Vaga SIAPE 674986
- SILVIA LORENZINI - Vaga SIAPE 674987
- CLAITON DOS SANTOS CANUTES - Vaga SIAPE 674988
 
 
 
 
RUI VICENTE OPPERMANN
Reitor
Documento gerado sob autenticação Nº DMI.025.345.D67, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
