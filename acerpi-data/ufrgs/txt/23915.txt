Documento gerado sob autenticação Nº DTO.495.158.G4B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4718                  de  27/06/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 5263, de 01 de outubro de
2012, e conforme a Solicitação de Afastamento n°20641,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, DANIELLE BERNARDES FARIAS (Siape: 2052292
),   para substituir    LENISE DI  DOMENICO COLPO (Siape:  1678532 ),  Chefe do Núcleo de Aquisição da
Biblioteca Central, Código FG-4, em seu afastamento no país, no período de 09/06/2016 a 10/06/2016, com o
decorrente pagamento das vantagens por 2 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
