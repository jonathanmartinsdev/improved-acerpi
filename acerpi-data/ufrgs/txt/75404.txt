Documento gerado sob autenticação Nº KIJ.462.062.99C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             674                  de  20/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°46993,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, MARIA CAROLINA FAGUNDES DANIEL (Siape:
2426925  ),   para  substituir    OTAVIA  TATIANA  CARDOZO  (Siape:  2222036  ),  Coordenador  do  Núcleo
Administrativo  e  Gestão  de  Pessoas  da  Gerência  Adm  do  Instituto  de  Letras,  Código  FG-4,  em  seu
afastamento por motivo de férias, no período de 21/01/2019 a 25/01/2019, com o decorrente pagamento das
vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
