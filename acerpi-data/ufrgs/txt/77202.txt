Documento gerado sob autenticação Nº AXV.594.179.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1847                  de  25/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5031789-90.2018.4.04.7100,  da  4ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo II da Portaria n° 1874, de
13/07/2006, da servidora MARIA BEATRIZ SARAIVA CARRARO , matrícula SIAPE n° 0579073, aposentado no
cargo de  Enfermeiro-área - 701029, para o nível IV, conforme o Processo nº 23078.503254/2019-03.
 
RUI VICENTE OPPERMANN
Reitor.
