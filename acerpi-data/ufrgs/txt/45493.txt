Documento gerado sob autenticação Nº WUE.980.801.154, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9236                  de  05/10/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°30388,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ANALÚCIA DANILEVICZ PEREIRA (Siape: 1893840 ),
 para substituir   EDUARDO MUNHOZ SVARTMAN (Siape: 1883418 ), Coordenador do PPG em Ciência Política,
Código FUC,  em seu afastamento no país,  no período de 12/10/2017 a 15/10/2017,  com o decorrente
pagamento das vantagens por 4 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
