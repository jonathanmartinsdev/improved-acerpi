Documento gerado sob autenticação Nº ZSG.482.779.FLM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10980                  de  10/12/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  MARIA  INES  REINERT  AZAMBUJA,  matrícula  SIAPE  n°  1149748,  lotada  e  em  exercício  no
Departamento de Medicina Social da Faculdade de Medicina, da classe   de Professor Adjunto, nível 03, para a
classe   de Professor Adjunto, nível 04, referente ao interstício de 21/12/2007 a 20/12/2009, com vigência
financeira  a  partir  de  21/12/2009,  conforme  decisão  judicial  proferida  no  processo  nº  5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.530874/2019-15.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
