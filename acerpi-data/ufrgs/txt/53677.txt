Documento gerado sob autenticação Nº PEO.195.126.RMV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3069                  de  26/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de PAOLO RECH, Professor do Magistério Superior, lotado e em
exercício no Departamento de Informática Aplicada do Instituto de Informática, com a finalidade de realizar
trabalho de campo, em Didcot, Inglaterra, no período compreendido entre 01/05/2018 e 14/05/2018, incluído
trânsito, com ônus limitado. Solicitação nº 45568.
RUI VICENTE OPPERMANN
Reitor
