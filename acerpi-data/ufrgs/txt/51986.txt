Documento gerado sob autenticação Nº WIJ.083.815.FCS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2065                  de  19/03/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir da publicação no Diário Oficial da União, o ocupante do cargo de Servente de
Obras - 701824, do Nível de Classificação AIV, do Quadro de Pessoal desta Universidade, ANDRE LUIS DE
MARISE ROSA, matrícula SIAPE 0357888 da função de Chefe da Subprefeitura do Campus Olímpico, Código
SRH 357, Código FG-4, para a qual foi designado pela Portaria nº 3412/2017 de 25/04/2017, publicada no
Diário Oficial da União de 26/04/2017. Processo nº 23078.504931/2018-11.
JANE FRAGA TUTIKIAN
Vice-Reitora.
