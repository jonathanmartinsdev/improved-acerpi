Documento gerado sob autenticação Nº UGR.858.835.J77, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7420                  de  19/09/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de PAULO SERGIO VIERO NAUD, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ginecologia e Obstetrícia da Faculdade de Medicina, com a
finalidade  de  proferir  palestra  na  "International  Conference  on  Human Genital  Infections  &  HPV",  em
Chicago, Estados Unidos, no período compreendido entre 19/10/2016 e 22/10/2016, incluído trânsito, com
ônus limitado. Solicitação nº 23228.
CARLOS ALEXANDRE NETTO
Reitor
