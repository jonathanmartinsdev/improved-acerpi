Documento gerado sob autenticação Nº NBB.225.743.BI5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6611                  de  24/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, KELLY LISSANDRA BRUCH, matrícula SIAPE n° 2201484, lotada no Departamento
de Direito Econômico e do Trabalho da Faculdade de Direito, como Diretora Substituta do Centro de Estudos
Interdisciplinares  em  Agronegócios,  para  substituir  automaticamente  o  titular  desta  função  em  seus
afastamentos  ou  impedimentos  regulamentares  na  vigência  do  presente  mandato,  sem  prejuízo  e
cumulativamente  com  a  função  de  Coordenadora  Substituta  da  COMGRAD  de  Direito.  Processo  nº
23078.518772/2019-13.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
