Documento gerado sob autenticação Nº DQS.617.990.R6V, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
DEPARTAMENTO DE DESENVOLVIMENTO E GESTÃO DE PESSOAS
ESCOLA DE DESENVOLVIMENTO DE SERVIDORES DA UFRGS
Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
(51) 3308-3015/3219/3914/4596 - edufrgs@progesp.ufrgs.br
Processo n°: 23078.514277/2019-35
Servidora: IZABEL CRISTINA DE OLIVEIRA MARTINEZ
Cargo: Técnico em Assuntos Educacionais
Lotação: Pró-Reitoria de Gestão de Pessoas
Ambiente Organizacional: Administrativo
Nível de Classificação e Nível de Capacitação: E I
PARECER N° 1487/2019
Trata  este  expediente da retificação do Parecer  n°  1334/2019,  de 14/10/2019,  que concede o
Incentivo à Qualificação a IZABEL CRISTINA DE OLIVEIRA MARTINEZ, Técnico em Assuntos Educacionais,
com  exercício  na  Divisão  de  Controle  de  Cargos  da  Pró-Reitoria  de  Gestão  de  Pessoas.  Processo  nº
23078.514277/2019-35.
 
 
Onde se lê:
"concluído em 01/08/2019 [...] a contar de 01/08/2019."
leia-se:
"concluído em 31/05/2019 [...] a contar de 31/05/2019."
 
Em 16/12/2019.
TACIANA BALDI MARTINEZ
Divisão de Análise e Orientação do Desenvolvimento na Carreira
De acordo.  Proceda-se  à  confecção da  portaria,  para  certificação do Pró-Reitor  de  Gestão de
Pessoas. Após, encaminhe-se à DPR, para inclusão na folha de pagamento; posteriormente à requerente,
para ciência e conclusão do processo.
Em 16/12/2019.
KAREN WERLANG LUNKES
Diretora da Divisão de Análise e Orientação do Desenvolvimento na Carreira
