Documento gerado sob autenticação Nº YYP.379.069.QVE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9730                  de  03/12/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 15/10/2018,  o ocupante do cargo de Professor do Magistério
Superior,  classe  Associado,  do  Quadro  de  Pessoal  desta  Universidade,  FERNANDO  MARCELO  PEREIRA,
matrícula SIAPE n° 1813048, da função de Coordenador Substituto do PPG em Engenharia Mecânica, para a
qual foi designado pela Portaria 4571/2018, de 25/06/2018. Processo nº 23078.528305/2018-11.
JANE FRAGA TUTIKIAN
Vice-Reitora.
