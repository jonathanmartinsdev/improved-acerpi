Documento gerado sob autenticação Nº KRL.084.187.OH2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6875                  de  31/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°85932,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do Quadro de Pessoal  desta Universidade,  LELIZ TICONA ARENAS (Siape:  1809390 ),   para
substituir   MARCOS JOSE LEITE SANTOS (Siape: 1805628 ), Coordenador do PPG em Ciência dos Materiais,
Código FUC,  em seu afastamento no país,  no período de 01/08/2019 a 02/08/2019,  com o decorrente
pagamento das vantagens por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
