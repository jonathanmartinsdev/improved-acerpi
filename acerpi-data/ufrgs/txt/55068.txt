Documento gerado sob autenticação Nº MQH.113.098.VDP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4189                  de  08/06/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SILVIA REGINA FERABOLLI, Professor do Magistério Superior,
lotada e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de Ciências
Econômicas, com a finalidade de participar da "Conference of the British Society for Middle Eastern Studies",
com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias); em Londres, reunião na  University of Cambridge, em
Cambridge, com ônus limitado e proferir palestra na School of Oriental and Africa Studies, em Londres,
Inglaterra,  com ônus CAPES/PROAP, no período compreendido entre 23/06/2018 e 06/07/2018, incluído
trânsito. Solicitação nº 45697.
RUI VICENTE OPPERMANN
Reitor
