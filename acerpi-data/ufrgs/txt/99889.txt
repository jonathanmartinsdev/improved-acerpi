Documento gerado sob autenticação Nº OBL.102.383.6AM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9548                  de  21/10/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar SILVIO ROBERTO RAMOS CORREA para exercer a função de Coordenador da Comissão
Interna de Supervisão do Plano de Carreira dos Cargos Técnicos-Admin em Educ, Código SRH 865, com
vigência a partir da data deste ato até 16/10/2021, a fim de completar o mandato da servidora ANGELA
FERNANDES DA SILVA. Processo nº 23078.528263/2019-07.
JANE FRAGA TUTIKIAN
Vice-Reitora.
