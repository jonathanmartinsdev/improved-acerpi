Documento gerado sob autenticação Nº OIG.873.137.M37, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4331                  de  14/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de SERGIO BAPTISTA DA SILVA, Professor do Magistério Superior,
lotado e em exercício no Departamento de Antropologia do Instituto de Filosofia e Ciências Humanas, com a
finalidade de participar do "56º Congresso Internacional de Americanistas",  em Salamanca, Espanha, no
período compreendido entre 14/07/2018 e 21/07/2018, incluído trânsito, com ônus limitado. Solicitação nº
45648.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
