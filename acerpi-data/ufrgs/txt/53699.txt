Documento gerado sob autenticação Nº PKG.251.265.A7F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3090                  de  27/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  27/04/2018,   referente  ao  interstício  de
27/10/2016 a 26/04/2018, para o servidor CARLOS ALEXANDRE WENDEL, ocupante do cargo de Técnico em
Assuntos Educacionais - 701079, matrícula SIAPE 1139896,  lotado  no  Campus Litoral Norte, passando do
Nível de Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.508788/2018-37:
Formação Integral  de Servidores da UFRGS I  CH: 35 Carga horária utilizada: 34 hora(s)  /  Carga horária
excedente: 1 hora(s) (27/04/2016 a 29/05/2017)
ILB - Deveres, proibições e responsabilidades do servidor público federal CH: 56 (08/03/2016 a 28/03/2016)
ILB - Relações Internacionais: Teoria e História CH: 60 (06/11/2017 a 26/11/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
