Documento gerado sob autenticação Nº LFP.477.335.92P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8535                  de  19/09/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°49836,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA, do Quadro de Pessoal desta Universidade, TIAGO CARRARD CENTURIAO (Siape: 1184626 ),  para
substituir    LOUIDI  LAUER ALBORNOZ (Siape:  2027001 ),  Coordenador do Núcleo de Apoio Técnico da
Gerência Administrativa do Instituto de Pesquisas Hidráulicas, Código FG-7, em seu afastamento por motivo
de férias, no período de 23/09/2019 a 30/09/2019.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
