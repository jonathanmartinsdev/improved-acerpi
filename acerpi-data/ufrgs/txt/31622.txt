Documento gerado sob autenticação Nº UHB.746.368.C77, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10041                  de  19/12/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  MAGALI  MENDES  DE  MENEZES,  matrícula  SIAPE  nº  2307961,  lotada  no
Departamento de Estudos Básicos da Faculdade de Educação, para exercer a função de Vice-Diretora da
Faculdade de Educação, Código SRH 180, código FG-1, com vigência a partir de 21/12/2016 e até 20/12/2020,
conforme o disposto nos artigos 5º e 6º do Decreto nº 1.916, de 23 de maio de 1996, que regulamenta a Lei
nº 9.192, de 21 de dezembro de 1995. Processo n° 23078.024854/2016-59.
RUI VICENTE OPPERMANN
Reitor
