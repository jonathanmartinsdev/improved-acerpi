Documento gerado sob autenticação Nº HNB.577.992.EN5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10100                  de  21/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°30421,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de SERVENTE DE LIMPEZA, do
Quadro de Pessoal desta Universidade, ANA CLAUDIA DOS SANTOS (Siape: 1107372 ),  para substituir  
MARISA CARVALHO BELLO (Siape: 0358682 ),  Secretário de Pós-Graduação da Faculdade de Agronomia,
Código FG-7, em seu afastamento por motivo de férias, no período de 28/12/2016 a 04/01/2017, com o
decorrente pagamento das vantagens por 8 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
