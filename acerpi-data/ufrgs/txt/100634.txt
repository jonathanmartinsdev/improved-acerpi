Documento gerado sob autenticação Nº TYA.128.266.S7M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10051                  de  07/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°88367,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO DO
ENSINO BÁSICO, TÉCNICO E TECNOLÓGICO, do Quadro de Pessoal desta Universidade, SOTERO SERRATE
MENGUE (Siape: 0355435 ),  para substituir   ALVARO VIGO (Siape: 0356920 ), Coordenador do PPG em
Epidemiologia, Código FUC, em seu afastamento no país, no período de 11/11/2019 a 14/11/2019, com o
decorrente pagamento das vantagens por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
