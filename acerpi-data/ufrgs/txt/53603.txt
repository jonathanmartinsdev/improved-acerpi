Documento gerado sob autenticação Nº EIT.116.111.J2R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3003                  de  25/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  19/03/2018,   referente  ao  interstício  de
16/09/2016 a 18/03/2018,  para a servidora ALEXANDRA GONÇALVES MARTINS,  ocupante do cargo de
Auxiliar em Administração - 701405,  matrícula SIAPE 2059154,  lotada  na  Pró-Reitoria de Gestão de
Pessoas, passando do Nível de Classificação/Nível de Capacitação C III, para o Nível de Classificação/Nível de
Capacitação  C  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.505620/2018-70:
ENAP - Gestão Estratégica de Pessoas e Planos de Carreira CH: 20 (05/09/2017 a 25/09/2017)
ENAP - Ética e Serviço Público CH: 20 (05/09/2017 a 25/09/2017)
ENAP - SEI! USAR CH: 20 (28/11/2017 a 18/12/2017)
ENAP - Siape Folha CH: 40 (30/05/2017 a 03/07/2017)
ENAP - Controles Institucional e Social dos Gastos Públicos CH: 20 (28/11/2017 a 18/12/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
