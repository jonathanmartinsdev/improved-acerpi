Documento gerado sob autenticação Nº GVY.352.184.5J9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4356                  de  17/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SIMONE CRISTINA BAGGIO GNOATTO, Professor do Magistério
Superior, lotada e em exercício no Departamento de Produção de Matéria Prima da Faculdade de Farmácia,
com a finalidade de realizar  visita    à  University  of  Rennes I,  França,  no período compreendido entre
17/06/2017 e 29/06/2017, incluído trânsito, com ônus limitado. Solicitação nº 27560.
RUI VICENTE OPPERMANN
Reitor
