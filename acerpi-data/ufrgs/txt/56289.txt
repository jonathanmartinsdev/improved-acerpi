Documento gerado sob autenticação Nº LPB.899.491.TJC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/4
PORTARIA Nº             5145                  de  13/07/2018
  Altera  a  composição  dos  membros
internos  do  Plenário  da  Coordenadoria
das Licenciaturas - COORLICEN.
              O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais, e
tendo em vista o Ofício Nº 034/2018 - COORLICEN e o que consta no processo nº 23078.003440/2016-96,
RESOLVE:
Art.  1º  Alterar  a  composição  dos  Membros  Internos  do  Plenário  da  Coordenadoria  das  Licenciaturas,
estabelecido  na  Portaria  nº  3917  de  28/05/2018,  no  que  tange  à  Coordenação da  Coordenadoria  das
Licenciaturas,  bem como dos  Representantes  das  Licenciaturas  em Ciências  Sociais,  Química,  História,
Geografia,  Geografia  do  Campus  Litoral  Norte,  Faculdade  de  Educação,  Representação  Discente  e
Representantes do Conselho Estadual de Educação do Rio Grande do Sul - CEED/RS, conforme estabelecido
no Art. 17 e no Art. 30 do Regimento da Coordenadoria das Licenciaturas, que passa a vigorar com os
seguintes membros:
 
Membros Internos
 
Coordenador da Coordenadoria das Licenciaturas
Simone Bicca Charczuk
 
Vice-Coordenadora da Coordenadoria das Licenciaturas
Gláucia Helena Motta Grohs
 
Representantes da Licenciatura em Dança
Flávia Pilla do Valle (titular)
Cibele Sastre (suplente)
 
Representante da Licenciatura em Teatro
João Carlos Machado (titular)
Cláudia Muller Sachs (suplente)
 
Representantes da Licenciatura em Letras
Simone Sarmento (titular)
Ingrid Finger (suplente)
Documento gerado sob autenticação Nº LPB.899.491.TJC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/4
 
Representantes da Licenciatura em Biologia
Russel Teresinha Dutra da Rosa (titular)
Eliane Kaltchuk dos Santos  (suplente)
 
Representantes da Pedagogia
Helena Dória Lucas de Oliveira (titular)
Cristiano Bedin da Costa (suplente)
 
Representantes da Licenciatura em Física
Neusa Teresinha Massoni (titular)
Magale Elisa Bruckmann (suplente)
 
Representantes da Licenciatura em Matemática
Maria Cecilia Bueno Fischer (titular)
Marcia Rodrigues Notare Meneghetti (suplente)
 
Representantes da Licenciatura em Educação Física
Clézio José dos Santos Gonçalves (titular)
Denise Grosso da Fonseca (suplente)
 
Representantes da Licenciatura em Ciências Sociais
Alexandre Silva Virgínio (titular)
Ricardo Antônio Cavalcanti Schiel (suplente)
 
Representantes da Licenciatura em Química
Camila Greff Passos (titular)
Mauricius Selvero Pazinato (suplente)
 
Representantes da Licenciatura em História
Adriana Schmidt Dias (titular)
Clarice Gontarski Speranza (suplente)
 
Representantes da Licenciatura em Geografia
Rafael da Rocha Ribeiro (titular)
Adriana Dorfmann (suplente)
 
Representantes da Licenciatura em Artes Visuais
Adriane Hernandez (titular)
Luciana Grupelli Loponte (suplente)
 
Representantes da Licenciatura em Música
Marilia Raquel Albornoz Stein (titular)
Luciane da Costa Cuervo (suplente)
 
Representantes da Licenciatura em Filosofia
Ana Rieger Schmidt (titular)
Nikolay Steffens Martins (suplente)
Documento gerado sob autenticação Nº LPB.899.491.TJC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
3/4
 
Representantes da Licenciatura em Educação do Campo - Ciências da Natureza - Litoral Norte
Cláudia Glavam Duarte (titular)
Elisete Enir Bernardi Garcia (suplente)
 
Representantes da Licenciatura em Geografia do Campus Litoral Norte
Sinthia Cristina Batista (titular)
Lucimar de Fátima dos Santos Vieira (suplente)
 
Representantes do Núcleo de Incentivo e Iniciação à Docência
Marilisa Bialvo Hoffmann (titular)
Luciane Uberti (suplente)
 
Representantes do Núcleo de Formação de Professores
Marcus Vinicius de Azevedo Basso (titular)
Sônia Mara Moreira Ogiba (suplente)
 
Representantes do Núcleo de Desenvolvimento das Licenciaturas
Rochele de Quadros Loguercio (titular)
Felipe Lohmann Arend (suplente)
 
Representantes do Colégio de Aplicação da UFRGS
Simone Vacaro Fogazzi (titular)
Maíra Suertegaray Rossato (suplente)
 
Representantes da Secretaria de Educação à Distância - SEAD
Sílvia de Oliveira Kist (titular)
Laura Wunsch (suplente)
 
Representante dos Técnicos em Assuntos Educacionais
 
Representantes da Faculdade de Educação
Liliane Ferrari Giordani (titular)
 
Representantes Discentes
Juliane Rodrigues Gonçalves (titular)
Giulia Minuzzo de Lima (suplente)
 
Representantes da Pró-Reitoria Graduação
Ricardo Strack (titular)
Nayane Rocha Manaut (suplente)
 
Representantes da Licenciatura do Campo
Leandra Anversa Fiorezi (titular)
Valéra da Cruz Viana Labrea (suplente)
 
 
 
Documento gerado sob autenticação Nº LPB.899.491.TJC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
4/4
Representantes da Licenciatura em Psicologia
Mara Lúcia Fernandes Carneiro (titular)
Amadeu de Oliveira Weinmann (suplente)
 
 
Membros  Externos
 
Representantes do Conselho Estadual de Educação do Rio Grande do Sul - CEED/RS
Sani Belfer Cardon (titular)
Gabriel Grabowski (suplente)
 
Representantes da Secretaria de Educação do Estado do Rio Grande do Sul -SEDUC/RS
José Adilson Santos Antunes (titular)
Carmem Maria França da Silva (suplente)
 
Representantes da Secretaria Municipal de Educação de Porto Alegre - SMED
Cristina Rolim Wolffenbuttel (titular)
Valéria Carvalho de Leonço (suplente)
 
Representantes do Sindicato do Ensino Privado - SINEPE/RS
Marícia da Silva Ferri (titular)
Naime Pigatto (suplente)
 
Art. 2º O mandato dos novos membros designados por esta Portaria será de 02 (dois) anos, a contar desta
data, permanecendo inalterados os demais mandatos.
 
Art. 3º O mandato da Professora Gláucia Helena Motta Grohs, como Vice-Coordenadora, será de 01 (um) ano,
a contar desta data.
 
Art. 4º O mandato dos representantes discentes será de 01 (um) ano, a contar da data.
 
RUI VICENTE OPPERMANN,
Reitor.
