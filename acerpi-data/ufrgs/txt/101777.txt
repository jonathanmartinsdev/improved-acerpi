Documento gerado sob autenticação Nº NGT.449.955.7JI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10855                  de  05/12/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta  Universidade,  CLAUDIO  WAGECK  CANAL,  matrícula  SIAPE  n°  1284015,  lotado  no
Departamento  de  Patologia  Clínica  Veterinária  da  Faculdade  de  Veterinária,  para  exercer  a  função  de
Coordenador do PPG em Ciências Veterinárias,  Código SRH 1151, código FUC, com vigência a partir  de
13/12/2019 até 12/12/2021. Processo nº 23078.532431/2019-51.
RUI VICENTE OPPERMANN
Reitor.
