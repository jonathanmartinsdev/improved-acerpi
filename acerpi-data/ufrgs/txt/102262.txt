Documento gerado sob autenticação Nº IVC.273.419.C0I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11133                  de  13/12/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de MARINA TREVISAN, Professor do Magistério Superior, lotada e
em exercício no Departamento de Astronomia do Instituto de Física, com a finalidade de realizar visita ao
Institut d'Astrophysique de Paris, em Paris, França, no período compreendido entre 13/01/2020 e 14/02/2020,
incluído trânsito, com ônus limitado. Solicitação nº 89282.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
