Documento gerado sob autenticação Nº CEI.724.559.14A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5703                  de  30/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°28998,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de QUÍMICO, do Quadro de Pessoal
desta Universidade, GREICE VANIN OLIVEIRA (Siape: 1654948 ),  para substituir   DARCI BARNECH CAMPANI
(Siape: 0351877 ), Assessor da Assessoria de Gestão Ambiental, Código CD-4, em seu afastamento do país, no
período de 26/06/2017 a 28/06/2017, com o decorrente pagamento das vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
