Documento gerado sob autenticação Nº WIQ.626.585.UJM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7328                  de  09/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°29889,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TECNOLOGO-FORMAÇÃO, do
Quadro de Pessoal desta Universidade, ROSI MARIA DA ROSA MENDES (Siape: 0357200 ),  para substituir  
VERA REGINA DA CUNHA (Siape: 0355713 ), Coordenador do Núcleo de Assuntos Disciplinares da PROGESP,
Código FG-1,  em seu afastamento no país,  no período de 01/08/2017 a 04/08/2017,  com o decorrente
pagamento das vantagens por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
