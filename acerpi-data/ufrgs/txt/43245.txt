Documento gerado sob autenticação Nº ELN.802.504.4IL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7834                  de  21/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país  de MARIA CECI ARAUJO MISOCZKY,  Professor do Magistério
Superior, lotada e em exercício no Departamento de Ciências Administrativas da Escola de Administração,
com a finalidade de participar do "XXXI  Congreso de la  Asociación Latinoamericana de Sociología",  em
Montevidéu, Uruguai, no período compreendido entre 02/12/2017 e 09/12/2017, incluído trânsito, com ônus
UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 28001.
RUI VICENTE OPPERMANN
Reitor
