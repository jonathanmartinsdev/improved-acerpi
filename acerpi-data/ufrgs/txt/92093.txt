Documento gerado sob autenticação Nº DVN.657.175.L0E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4696                  de  31/05/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°50050,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do Quadro de  Pessoal  desta  Universidade,  CINARA APARECIDA MARTINS BARBOSA
MOREIRA  (Siape:  2258032  ),   para  substituir    MARIA  REJANE  KUNZLER  (Siape:  0356275  ),  Gerente
Administrativo vinculado do CPD, Código FG-3, em seu afastamento por motivo de férias, no período de
03/06/2019 a 07/06/2019, com o decorrente pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
