Documento gerado sob autenticação Nº ZNZ.601.201.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1852                  de  25/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5067097-90.2018.4.04.7100,  da  4ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, do servidor PAULO RICARDO DA ROZA TERRA, matrícula SIAPE n° 0356544, ativo no cargo de 
Assistente em Administração - 701200, para o nível II, conforme o Processo nº 23078.503796/2019-78.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
Conceder  progressão  por  capacitação,  a  contar  de  19/11/2009,  passando  do  Nível  de
Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de  Capacitação  D  III.
Conceder  progressão  por  capacitação,  a  contar  de  28/09/2012,  passando  do  Nível  de
Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de  Capacitação  D  IV.
RUI VICENTE OPPERMANN
Reitor
