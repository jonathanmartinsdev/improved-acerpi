Documento gerado sob autenticação Nº FHH.974.214.198, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2499                  de  04/04/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Nomear,  em  caráter  efetivo,  ANA  LUIZA  DE  FREITAS  KESSLER,  em  virtude  de  habilitação  em
Concurso Público de Provas e Títulos, conforme Edital Nº 57/2017 de 8 de Novembro de 2017 homologado
em 09 de novembro de 2017 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de
1990 e Lei 12.772, de 28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de
2013, publicada no Diário Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO
MAGISTÉRIO DO ENSINO BÁSICO, TÉCNICO E TECNOLÓGICO do Plano de Carreiras e Cargos do Magistério
Federal, na Classe "A" de Professor D, Nível I,  do Quadro de Pessoal desta Universidade, em regime de
trabalho de DE (Dedicação Exclusiva), junto ao Departamento de Ciências Exatas e da Natureza do Colégio de
Aplicação,  em  vaga  decorrente  de  Criação  de  Cargos,  código  nº  238016,  ocorrida  em  04  de  abril  de
2017, conforme Portaria nº. 839/2017 de 31/03/2017, publicada no Diário Oficial da União de 04/04/2017.
Processo nº. 23078.509138/2016-47.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
