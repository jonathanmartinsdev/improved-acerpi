Documento gerado sob autenticação Nº KGS.593.476.T8G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3844                  de  25/05/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  SABRINA  CARVALHO  GOMES,  matrícula  SIAPE  n°  1746828,  lotada  e  em  exercício  no
Departamento de Odontologia Conservadora, da classe C  de Professor Adjunto, nível 04, para a classe D  de
Professor Associado, nível 01, referente ao interstício de 02/04/2016 a 01/04/2018, com vigência financeira a
partir de 02/04/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.503730/2018-05.
JANE FRAGA TUTIKIAN
Vice-Reitora.
