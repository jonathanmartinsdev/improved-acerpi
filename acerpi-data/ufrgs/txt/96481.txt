Documento gerado sob autenticação Nº PLP.102.419.FK4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7517                  de  20/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Declarar vago, a partir de 29 de julho de 2019, o Cargo de Assistente em Administração, Código
701200, Nível de Classificação D, Nível de Capacitação II, Padrão 02, do Quadro de Pessoal, em decorrência
de posse em outro cargo inacumulável,  de LUIS PAULO SILVA DA ROSA  com lotação na Faculdade de
Veterinária. Processo nº 23078.517599/2019-36.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
