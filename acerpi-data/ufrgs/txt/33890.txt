Documento gerado sob autenticação Nº AXL.824.424.Q5E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1195                  de  08/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, HENRIQUE MORRONE, matrícula SIAPE n° 1474144, lotado no Departamento de
Economia e Relações Internacionais da Faculdade de Ciências Econômicas, como Coordenador Substituto da
COMGRAD de Economia, para substituir automaticamente o titular desta função em seus afastamentos ou
impedimentos regulamentares na vigência do presente mandato. Processo nº 23078.000086/2017-29.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
