Documento gerado sob autenticação Nº EQP.265.226.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8743                  de  26/09/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Exonerar, a pedido, a partir de 24/09/2019, ALEXANDRE BRENTANO, do cargo de Procurador-Geral
Adjunto, Código SRH 907, Código CD-4, da Universidade Federal do Rio Grande do Sul,  para o qual foi
designado pela Portaria nº 4641/2018, de 27/06/2018, publicada no Diário Oficial da União de 29/06/2018.
Processo nº 23078.526127/2019-74.
RUI VICENTE OPPERMANN
Reitor.
