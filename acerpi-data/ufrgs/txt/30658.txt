Documento gerado sob autenticação Nº NNU.009.669.98H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9312                  de  22/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora CAMILA FOCKINK,  ocupante do cargo de  Assistente em Administração -
701200, lotada no Instituto de Biociências, SIAPE 2341594, o percentual de 25% (vinte e cinco por cento) de
Incentivo à Qualificação, a contar de 07/10/2016, tendo em vista a conclusão do curso de Graduação em
Administração - Bacharelado, conforme o Processo nº 23078.022759/2016-11.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
