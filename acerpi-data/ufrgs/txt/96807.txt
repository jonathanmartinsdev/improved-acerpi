Documento gerado sob autenticação Nº MYF.909.769.395, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 1321/2019-PROGESP Porto Alegre, 21 de agosto de 2019.
Senhora Diretora
               Encaminhamos o(a) servidor(a) Claudia Maria Perrone ocupante do cargo Professor do Magistério
Superior, para exercício nessa Unidade, a partir de 19/08/2019.
               Para confecção da Portaria de Lotação, solicitamos que nos sejam encaminhadas, em até 03 dias, as
seguintes informações:
-  lotação e exercício (Departamento), observando a hierarquia dos órgãos registrados no SRH;
-  área de atuação do(a) servidor(a) docente;
-  nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal;
-  data de efetiva apresentação do(a) servidor(a) nessa Unidade.
        
Informamos que este encaminhamento visa atendimento de Decisão Judicial, conforme processo nº
5027473-97.2019.4.04.7100, Parecer de Força Executória nº 035/2019/PRF4.
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
Ilma. Sra.
Professora CLARISSA MARCELI TRENTINI
Diretora do Instituto de Psicologia,
Nesta Universidade.
