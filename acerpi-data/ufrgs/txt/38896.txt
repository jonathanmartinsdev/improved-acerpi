Documento gerado sob autenticação Nº WUW.275.793.HCC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4818                  de  31/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 18/05/2017,  o ocupante do cargo de Professor do Magistério
Superior, classe Associado, do Quadro de Pessoal desta Universidade, MARCELO ANTONIO CONTERATO,
matrícula SIAPE n° 1650064, da função de Coordenador da COMGRAD em Bacharelado em Desenvolvimento
Rural, Código SRH 1188, código FUC, para a qual foi designado pela Portaria 3954/2016, de 01/06/2016,
publicada no Diário Oficial da União do dia 06/06/2016. Processo nº 23078.010379/2017-14.
JANE FRAGA TUTIKIAN
Vice-Reitora.
