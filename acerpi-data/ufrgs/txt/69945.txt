Documento gerado sob autenticação Nº QKY.674.255.VIT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7501                  de  21/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor SAMUEL GENEROSO VIEIRA, ocupante do cargo de  Técnico em Edificações -
701228, lotado na Superintendência de Infraestrutura, SIAPE 3067399, o percentual de 25% (vinte e cinco por
cento)  de  Incentivo  à  Qualificação,  a  contar  de  10/08/2018,  tendo  em vista  a  conclusão  do  curso  de
Engenharia Civil, conforme o Processo nº 23078.520581/2018-31.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
