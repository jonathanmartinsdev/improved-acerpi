Documento gerado sob autenticação Nº FSP.542.836.OMJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             2477                  de  21/03/2017
Nomeação  os  representantes  discentes
eleitos  no  Curso  de  Química.
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear os representantes discentes eleitos no Curso de Química, com mandato de 01 (um) ano, a
contar de 27 de janeiro de 2017, atendendo ao disposto nos artigos 175 do Regimento Geral da Universidade
e 79 do Estatuto da Universidade e considerando o processo nº 23078.200432/2017-77, conforme segue:
CENTRAL ANALÍTICA (CA)
Titular Cristina Missagia Santana
Suplente Tatiane Rocha Montes D'oca
 
CENTRO DE GESTÃO E TRATAMENTO DE RESÍDUOS QUÍMICOS (CGTRQ)
Titular Arthur Exner
Suplente Bruna Cunha Dias
 
COMISSÃO DE GRADUAÇÃO
Titular Francielle Pereira Pedroso
Suplente João Gabriel Machado da Silva
 
COMISSÃO DE EXTENSÃO (COMEXT)
Titular Flávia Maggioni Bernardi
Suplente Billy Nunes Cardoso
 
CONSELHO DA UNIDADE (CONSUNI)
1º Titular Alice Gaier Viário
1º Suplente Natália Kohlraush
2º Titular Tainá de Oliveira Piñero
2º Suplente Rafaela Oliveira Anderi
 
DEPARTAMENTO DE FÍSICO-QUÍMICA (DFQ)
Titular Eduardo Santos Vasconcelos
Documento gerado sob autenticação Nº FSP.542.836.OMJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Suplente Daniele Prestes Daniel
 
DEPARTAMENTO DE QUÍMICA INORGÂNICA (DQI)
Titular Eduardo Horbach Nunes
Suplente Bruno Bercini de Araújo
 
DEPARTAMENTO DE QUÍMICA ORGÂNICA (DQO)
Titular Gabriel da Silva Pereira
Suplente Letícia Bortolotto
 
NÚCLEO DE AVALIAÇÃO DA UNIDADE
Titular Miriã Terra da Costa
Suplente Ismael dos Santos Belmonte
 
                     
 
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
