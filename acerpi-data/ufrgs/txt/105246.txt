Documento gerado sob autenticação Nº GSJ.182.984.0EC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1522                  de  13/02/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°52649,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA,  do Quadro de Pessoal  desta  Universidade,  LOUIDI  LAUER ALBORNOZ (Siape:  2027001 ),   para
substituir   ALFONSO RISSO (Siape: 1187631 ), Coordenador do Curso Técnico em Hidrologia, Código FUC, em
seu afastamento por motivo de férias, no período de 14/02/2020 a 28/02/2020, com o decorrente pagamento
das vantagens por 15 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
