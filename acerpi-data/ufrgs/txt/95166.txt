Documento gerado sob autenticação Nº FZL.245.248.H7O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6686                  de  25/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 3958, de 29 de maio de
2018
RESOLVE
Tornar sem efeito a partir de 25 de julho de 2015 a Portaria de nomeação do(s) candidato(s) abaixo
relacionado(s), ocorrida através da Portaria nº 5230/2019, de 21/06/2019, publicada no Diário Oficial da união
de 24 de junho de 2019, de acordo com o que preceitua o § 6º do artigo 13º, da Lei nº 8.112, de 11 de
dezembro de 1990,  com a redação dada pela Lei  nº.  9.527,  de 10 de dezembro de 1997.  Processo nº
23078.014002/2015-72.
 
            
ENGENHEIRO -ÁREA CIVIL - Classe E - Padrão 1
 
- REGINA VIGNATTI - Vaga SIAPE 67223
 
 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas.
