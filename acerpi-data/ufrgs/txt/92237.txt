Documento gerado sob autenticação Nº LQX.973.907.O3T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4781                  de  04/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de PAULO ANTONIO ZAWISLAK, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ciências Administrativas da Escola de Administração, com a
finalidade  de  participar  da  ""R&D  Management  Conference  2019",  em  Paris,  França,  no  período
compreendido entre 17/06/2019 e 24/06/2019, incluído trânsito, com ônus CNPq (Proc. nº 450845/2019-2).
Solicitação nº 84054.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
