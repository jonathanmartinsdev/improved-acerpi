Documento gerado sob autenticação Nº HWA.384.289.4UQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3145                  de  27/04/2016
  Designar Comissão para apresentação de
Proposta de Projeto de Integração entre as
atividades desenvolvidas no Ceclimar e no
Campus Litoral Norte.
O PRÓ-REITOR DE COORDENAÇÃO ACADÊMICA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL, no uso de suas atribuições, 
RESOLVE:
Designar
      
      EDUARDO GUIMARAES BARBOZA
      LIANE LUDWIG LODER
      NORMA LUIZA WURDIG
para  compor  Comissão  para  apresentação  de  Proposta  de  Projeto  de  Integração  entre  as  atividades
desenvolvidas no Centro de Estudos Costeiros, Limnológicos e Marinhos - Ceclimar (Imbé - RS) e no Campus
Litoral Norte - CLN (Tramandaí - RS).
RUI VICENTE OPPERMANN,
Pró-Reitor de Coordenação Acadêmica.
 
