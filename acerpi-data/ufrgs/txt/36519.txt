Documento gerado sob autenticação Nº TQW.242.206.P1T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2943                  de  04/04/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor VINICIUS KARLINSKI DE BARCELLOS,  matrícula  SIAPE n°  1902655,  lotado e em exercício no
Departamento de Metalurgia da Escola de Engenharia, da classe A  de Professor Adjunto A, nível 01, para a
classe A  de Professor Adjunto A, nível 02, referente ao interstício de 18/02/2015 a 17/02/2017, com vigência
financeira a partir de 18/02/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo
nº 23078.500167/2017-24.
JANE FRAGA TUTIKIAN
Vice-Reitora.
