Documento gerado sob autenticação Nº ARK.366.281.FJC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9180                  de  04/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ARIEL BEHR, Professor do Magistério Superior, lotado e em
exercício no Departamento de Ciências Contábeis e Atuariais da Faculdade de Ciências Econômicas, com a
finalidade de ministrar curso junto à Universidad Nacional  del Sur, em Bahia Blanca, Argentina, no período
compreendido entre 29/10/2017 e 03/11/2017, incluído trânsito, com ônus CAPES/CAFP. Solicitação nº 31164.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
