Documento gerado sob autenticação Nº HPR.931.808.9V3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3323                  de  07/05/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder Progressão por Mérito Profissional, nos termos do artigo 10-A da Lei nº 11.091, de 12 de
janeiro  de  2005,  e  da  Decisão nº  939,  de  21  de  novembro de  2008,  do  Conselho Universitário  desta
Universidade, para a servidora PRISCILLA MAGRO REQUE, matrícula SIAPE 1816420, Nível de Classificação E ,
para o padrão de vencimento 06, com vigência a partir de 21 de março de 2018.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
