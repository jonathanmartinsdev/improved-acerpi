Documento gerado sob autenticação Nº SGS.383.700.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6639                  de  24/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARTHA LUCIA OLIVAR JIMENEZ,  Professor do Magistério
Superior, lotada e em exercício no Departamento de Direito Público e Filosofia do Direito da Faculdade de
Direito,  com  a  finalidade  de  ministrar  cursos  junto  à  Université  de  Rennes  2,   França,  no  período
compreendido entre 24/09/2018 e 30/09/2018, incluído trânsito, com ônus limitado. Solicitação nº 58528.
RUI VICENTE OPPERMANN
Reitor
