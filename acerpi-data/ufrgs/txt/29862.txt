Documento gerado sob autenticação Nº SUP.105.131.JGP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8843                  de  01/11/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 18/10/2016, o ocupante do cargo de Analista de Tecnologia da Informação -
701062,  do Nível  de Classificação EII,  do Quadro de Pessoal  desta  Universidade,  AFONSO COMBA DE
ARAUJO  NETO,  matrícula  SIAPE  1446592  da  função  de  Diretor  do  Departamento  de  Segurança  da
Informação do CPD, Código SRH 1451, Código FG-1, para a qual foi designado pela Portaria nº 5594/14 de
08/08/2014, publicada no Diário Oficial da União de 12/08/2014. Processo nº 23078.023422/2016-21.
JANE FRAGA TUTIKIAN
Vice-Reitora
