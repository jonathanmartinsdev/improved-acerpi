Documento gerado sob autenticação Nº GRE.756.700.NK5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1687                  de  27/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, ao
Professor  VINICIUS KARLINSKI  DE BARCELLOS,  matrícula  SIAPE 1902655,  lotado no Departamento de
Metalurgia da Escola de Engenharia e com exercício no Centro de Tecnologia, da classe A  de Professor
Adjunto A, nível 02, para a classe C  de Professor Adjunto, nível 01, com vigência financeira a partir da data de
publicação da portaria, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações, Portaria nº 554, de 20 de junho de 2013 do Ministério da Educação e a Decisão nº 401/2013 -
CONSUN. Processo nº 23078.502864/2018-09.
RUI VICENTE OPPERMANN
Reitor.
