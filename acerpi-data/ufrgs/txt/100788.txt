Documento gerado sob autenticação Nº ZXO.866.020.7JI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10175                  de  11/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  RENATO  VENTURA  BAYAN  HENRIQUES,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Sistemas Elétricos de Automação e Energia
da Escola de Engenharia, com a finalidade de ministrar curso junto à Universidad Militar Nueva Granada, em
Bogotá, Colômbia, no período compreendido entre 17/11/2019 e 23/11/2019, incluído trânsito, com ônus
limitado. Solicitação nº 88017.
RUI VICENTE OPPERMANN
Reitor
