Documento gerado sob autenticação Nº GVF.559.964.K8T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2313                  de  15/03/2017
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7793, de 03 de outubro de 2016
RESOLVE
Reinstaurar a Portaria nº 336 de 12 de janeiro de 2017 a ser processada pela Comissão integrada
pelos  servidores   GISELLE  REIS  ANTUNES,  ocupante  do  cargo  de  Engenheiro-área,  lotada  na
Superintendência de Infraestrutura e com exercício no Setor de Fiscalização e Adequação, JONES RITTA
RODRIGUES, ocupante do cargo de Técnico em Edificações, lotado na Superintendência de Infraestrutura e
com exercício na Prefeitura Campus Centro, LUIZ FRANCISCO PERRONE, ocupante do cargo de Arquiteto e
Urbanista, lotado na Superintendência de Infraestrutura e com exercício no Setor de Patrimônio Histórico,
RAQUEL BUTTOW NUNES DIAS,  ocupante do cargo de Engenheiro-área, lotada na Superintendência de
Infraestrutura e com exercício na Prefeitura Campus Centro, IGARA CESAR MIRANDA PAQUOLA, ocupante
do cargo de Arquiteto e Urbanista, lotado na Superintendência de Infraestrutura e com exercício no Setor de
Patrimônio Histórico, para, sob a presidência da primeira, no prazo de noventa dias, a partir da presente
data, elaborar Laudo Técnico Circunstanciado contendo a avaliação das condições de revestimento externo e
interno dos prédios históricos dos quarteirões 1 e 2 do Campus Centro, apontando as necessidades de
intervenção e propondo solução técnica.
ANDREA PINTO LOGUERCIO
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
