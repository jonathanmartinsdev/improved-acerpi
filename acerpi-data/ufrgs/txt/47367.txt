Documento gerado sob autenticação Nº SVH.434.673.L2C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10672                  de  23/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora SOLANGE ROSA SOUZA
GUEDES,  ocupante  do  cargo  de  Copeiro-701633,  lotada  na  Pró-Reitoria  de  Assuntos  Estudantis,  SIAPE
1028719, para 25% (vinte e cinco por cento), a contar de 13/11/2017, tendo em vista a conclusão do curso
Superior de Tecnologia em Gestão Pública, conforme o Processo nº 23078.521146/2017-42.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
