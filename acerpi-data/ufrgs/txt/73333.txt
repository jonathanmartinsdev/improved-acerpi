Documento gerado sob autenticação Nº TMX.151.891.H67, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9971                  de  10/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, nos termos do art. 1° da Lei n° 1.234, de 14 de novembro de 1950, combinado com o art.
1° e o parágrafo único do art. 7°, do Decreto 81.384, de 22 de fevereiro de 1978, alterado pelo Decreto
84.106, de 22 de outubro de 1979 e com os artigos 72 e 79, da Lei n° 8112, de 11 de dezembro de 1990, o Sr.
LUIS  CARLOS DA FONTOURA FRASCA,  matriculado no SIAPE sob nº  0355025,   ocupando o  cargo de
Professor do Magistério Superior, lotado e em exercício no Departamento de Odontologia Conservadora,
para operar direta,  obrigatória e habitualmente com Raio X ou substâncias radioativas,  fazendo jus ao
pagamento da respectiva gratificação, calculado com base no percentual de sobre o vencimento do cargo
efetivo, conforme art. 12, da Lei n° 8.270, de 17 de dezembro de 1991, combinado com o art. 68, da Lei n°
8.112,  de  11  dezembro  de  1990,  com  efeitos  financeiros  a  partir  de  01/09/2018.  Processo  nº
23078.523142/2018-80.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
