Documento gerado sob autenticação Nº SJJ.356.043.9SN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5554                  de  25/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Designar  STÉFANI  ALMEIDA  SCHNEIDER,  Matrícula  SIAPE  2230620,  ocupante  do  cargo  de
Nutricionista-habilitação, Código 701055, do Quadro de Pessoal desta Universidade, para exercer a função de
Chefe da Seção do RU1 da Divisão de Alimentação do DIE da PRAE, Código SRH 1294, código FG-3, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.015089/2016-86.
CARLOS ALEXANDRE NETTO
Reitor
