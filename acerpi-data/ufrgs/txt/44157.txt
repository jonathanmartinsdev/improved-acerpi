Documento gerado sob autenticação Nº VDM.000.841.F0V, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8365                  de  06/09/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, ROBERTO MARIO SILVEIRA ISSLER,  matrícula SIAPE n° 1027359, lotado no
Departamento de Pediatria da Faculdade de Medicina, para exercer a função de Coordenador do Programa
de Educação Médica Continuada da Faculdade de Medicina, Código SRH 474, código FG-5, com vigência a
partir  da  data  de  publicação  no  Diário  Oficial  da  União,  pelo  período  de  02  anos,  por  ter  sido
reeleito. Processo nº 23078.510623/2017-44.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
