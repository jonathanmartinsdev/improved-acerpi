Documento gerado sob autenticação Nº WCK.837.242.QE1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1011                  de  31/01/2017
           O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições,
RESOLVE
Designar MARIA TEREZA FLORES PEREIRA,  matrícula SIAPE n° 1854763, ocupante do cargo de
Professor do Magistério Superior, classe Adjunto, lotada no Departamento de Ciências Administrativas da
Escola de Administração,  do Quadro de Pessoal  da Universidade Federal  do Rio Grande do Sul,  como
Coordenadora da COMGRAD de Administração, Código SRH 1194, Código FUC, com vigência a partir da data
de publicação no Diário Oficial da União e até 19/02/2017, a fim de completar o mandato de DANIELA
CALLEGARO  DE  MENEZES,  conforme  artigo  92  do  Estatuto  da  mesma  Universidade.  Processo  nº
23078.026116/2016-46.
                                     CELSO GIANNETTI LOUREIRO CHAVES
                                              Decano, no exercício da Reitoria
