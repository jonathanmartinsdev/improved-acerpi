Documento gerado sob autenticação Nº MMU.608.514.ENU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7794                  de  18/08/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, ANNELISE KOPP ALVES, matrícula SIAPE n° 3508176, lotada no Departamento de
Engenharia dos Materiais da Escola de Engenharia, como Chefe Substituta do Depto de Engenharia dos
Materiais  da  Escola  de  Engenharia,  para  substituir  automaticamente  o  titular  desta  função  em  seus
afastamentos ou impedimentos regulamentares no período de 21/08/2017 e até 20/08/2019, por ter sido
reeleita, sem prejuízo e cumulativamente com a função de Coordenadora da COMGRAD de Engenharia de
Materiais. Processo nº 23078.511223/2017-56.
JANE FRAGA TUTIKIAN
Vice-Reitora.
