Documento gerado sob autenticação Nº BEL.689.820.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6595                  de  23/08/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5044825-05.2018.4.04.7100,  da  6ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, do servidor UBAYAR CARBONELL CLOSS  ,  matrícula SIAPE n° 0355608, ativo no cargo de 
Técnico de Tecnologia da Informação - 701226, para o nível IV, conforme o Processo nº 23078.521540/2018-
61.
RUI VICENTE OPPERMANN
Reitor.
