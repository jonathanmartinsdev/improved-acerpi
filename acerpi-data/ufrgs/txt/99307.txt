Documento gerado sob autenticação Nº BGJ.810.317.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9175                  de  10/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ERICO ESTEVES DUARTE,  Professor do Magistério Superior,
lotado no Departamento de Economia e Relações Internacionais da Faculdade de Ciências Econômicas e com
exercício na Comissão de Graduação em Relações Internacionais, com a finalidade de participar da "ISSS-IS
Annual  Conference  2019",  em  Denver,  Estados  Unidos,  no  período  compreendido  entre  18/10/2019  e
20/10/2019, incluído trânsito, com ônus CAPES/PROAP. Solicitação nº 86898.
RUI VICENTE OPPERMANN
Reitor
