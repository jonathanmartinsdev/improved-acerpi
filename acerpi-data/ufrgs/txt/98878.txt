Documento gerado sob autenticação Nº QPV.078.017.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8904                  de  02/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  SANDRA  DE  FATIMA  BATISTA  DE  DEUS,  Professor  do
Magistério  Superior,  lotada  no  Departamento  de  Comunicação  da  Faculdade  de  Biblioteconomia  e
Comunicação e com exercício na Pró-Reitoria de Extensão, com a finalidade de participar do "IV Congresso de
Extensión Universitaria de AUGM" e da "II Reunión Anual da Comissão Permanente Extensión Universitaria",
em Santiago, Chile, no período compreendido entre 03/11/2019 e 07/11/2019, incluído trânsito, com ônus
UFRGS (Pró-Reitoria de Extensão: diárias e passagens). Solicitação nº 86587.
RUI VICENTE OPPERMANN
Reitor
