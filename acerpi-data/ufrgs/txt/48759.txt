Documento gerado sob autenticação Nº IWE.887.366.548, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11645                  de  28/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, RITA DE CÁSSIA DOS SANTOS SILVEIRA, matrícula SIAPE n° 1267026, lotada no
Departamento de Pediatria da Faculdade de Medicina,  como Coordenadora Substituta da Comissão de
Pesquisa  da  Faculdade  de  Medicina,  para  substituir  automaticamente  o  titular  desta  função  em  seus
afastamentos ou impedimentos regulamentares no período de 05/01/2018 até 04/01/2020. Processo nº
23078.523429/2017-29.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
