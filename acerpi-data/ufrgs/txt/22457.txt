Documento gerado sob autenticação Nº KCQ.518.328.GK4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3719                  de  20/05/2016
O VICE-SUPERINTENDENTE DE OBRAS DA SUINFRA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 6172, de 26 de outubro de 2012
RESOLVE
Designar os servidores,  THAÍS SCHMIDT FERNANDES,  ocupante do cargo de Engenheiro-área,
lotada na Superintendência de Infraestrutura e com exercício no Departamento de Fiscalização de Obras,
JOAO  JULIO  KLUSENER,  ocupante  do  cargo  de  Engenheiro-área,  lotado  na  Superintendência  de
Infraestrutura e com exercício na Prefeitura Campus do Vale (SP3), NADYA PESCE DA SILVEIRA, ocupante do
cargo de Professor do Magistério Superior, lotada no Departamento de Química Inorgânica do Instituto de
Química  e  com exercício  no Instituto  de  Química,  para  sob a  presidência  da  primeira,  constituirem a
Comissão de Recebimento, em atendimento à alínea "b" do inciso I do artigo 73 da Lei 8666/93, para emissão
do Termo de Recebimento Definitivo da Obra do contrato 073/2015, cujo objeto é "Reforma das fachadas
do Centro de Combustíveis, Biocombustíveis, Lubrificantes e Óleos - CECOM do Instituto de Química
no Campus do Vale da UFRGS",  com prazo até 15/03/2017 quando se encerra o prazo de vigência do
contrato. Processo nº 23078.202142/2014-15.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
SILVIO HENRIQUE BERSAGUI
Vice-Superintendente de Obras da SUINFRA
