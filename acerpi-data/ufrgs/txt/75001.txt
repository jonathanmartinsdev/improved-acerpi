Documento gerado sob autenticação Nº NCZ.852.533.C1S, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             467                  de  15/01/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016, e tendo em vista o que consta do Processo Administrativo n° 23078.200392/2015-00, da Lei 10.520/02,
do Contrato nº 093/2015 e ainda, da Lei 8.666/93,
           RESOLVE:
 
         Aplicar a sanção administrativa de ADVERTÊNCIA, prevista na alínea "a", cláusula décima primeira do
contrato  supracitado,  à  Empresa  BR  SUL  SERVIÇOS  LTDA,  CNPJ  nº  91.618.827/0001-19,  pela  não
apresentação de notificação formal quanto à mudança no quadro de prestadores, conforme atestado pela
fiscalização  (fls.  479  e  487),  bem  como  pelo  NUDECON  (fl.  489)  do  processo  administrativo
23078.200392/2015-00.
 
          Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG. 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
