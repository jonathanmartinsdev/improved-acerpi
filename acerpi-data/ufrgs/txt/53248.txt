Documento gerado sob autenticação Nº RVA.825.585.P8E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2802                  de  16/04/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias, e conforme a Solicitação de Afastamento n°45462,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997,  a ocupante do cargo de , do Quadro de Pessoal desta
Universidade, VÂNIA CRISTINA SANTOS PEREIRA (Siape: 6355656 ),  para substituir  automaticamente 
MAURÍCIO VIÉGAS DA SILVA (Siape:  0354315 ),  Pró-Reitor  de Gestão de Pessoas,  Código CD-2,  em seu
afastamento no país, no período de 17/04/2018 a 20/04/2018, com o decorrente pagamento das vantagens
por 4 dias.
RUI VICENTE OPPERMANN
Reitor 
