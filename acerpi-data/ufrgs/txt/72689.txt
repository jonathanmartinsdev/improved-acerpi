Documento gerado sob autenticação Nº ZHG.541.216.O34, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9590                  de  28/11/2018
O PRÓ-REITOR DE GRADUAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de
suas atribuições, considerando o disposto na Portaria nº 7626, de 29 de setembro de 2016
RESOLVE:
 
 
Designar, LUCIENE JULIANO SIMÕES, interlocutora do Programa de Educação Tutorial junto à Secretaria de
Educação Superior, suplente RICARDO STRACK; JESSICA CICI DE CARPES, suplente MAURA PRIOR ROLDO;
ARÃO DA SILVA MORAES, suplente BEATRIZ REGINA KLING TROTT; ANDRÉA KRUGER GONÇALVES, suplente
ÉRIKA FERNANDES COTA; BRUNO CASSEL NETO, suplente RAFAELA MEDAGLIA GUARNIER; JESSE RODRIGUEZ
CARDOSO, suplente PAULO ROBERTO GUEDES DE OLIVEIRA; CLÁUDIA PORCELLIS ARISTIMUNHA, suplente
DÉBORA  SIMÕES  DA  SILVA  RIBEIRO;  TATIANA  REIDEL,  suplente  TÂNIA  ALVES  AMADOR;  para,  sob  a
presidência  da primeira,  comporem o Comitê Local  de Acompanhamento e Avaliação do Programa de
Educação Tutorial, desta Universidade, por um ano a partir da presente data.
VLADIMIR PINHEIRO DO NASCIMENTO
Pró-Reitor de Graduação
