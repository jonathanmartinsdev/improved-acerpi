Documento gerado sob autenticação Nº IXS.914.888.FP4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9375                  de  23/11/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar a Portaria n° 9057/2016, de 10/11/2016, que concedeu autorização para afastamento do
País a AURORA CARNEIRO ZEN,  Professor do Magistério Superior,  com exercício no Departamento de
Ciências Administrativas da Escola de Administração
Onde se lê: com a finalidade de participar de congresso junto à ICN BusinessSchool, em Nancy;
reunião na Université Paris-Est Créteil,  em Paris, França e da "42nd Annual Conference of the European
International Business Academy", em Viena - Áustria, ..........., com ônus limitado,
 leia-se: com a finalidade de participar de congresso junto à ICN BusinessSchool, em Nancy; reunião
na Université Paris-Est Créteil, em Paris, França, com ônus limitado e da "42nd Annual Conference of the
European International Business Academy", em Viena - Áustria,  com ônus CAPES/PROAP, ficando ratificados
os demais termos. Solicitação nº 25133.
RUI VICENTE OPPERMANN
Reitor
