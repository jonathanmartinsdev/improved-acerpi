Documento gerado sob autenticação Nº KZF.682.895.P69, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             28                  de  02/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Remover,  a partir  de 19 de dezembro de 2019, GIANE MARTINS FERREIRA, matrícula SIAPE nº.
3012797, ocupante do cargo de Assistente em Administração, do Núcleo do Campus Centro - TUA UFRGS
para o Núcleo Acadêmico do Campus Litoral Norte, conforme Processo nº. 23078.535001/2019-91.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
