Documento gerado sob autenticação Nº UPT.536.112.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3436                  de  22/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear o ocupante do cargo de Professor do Magistério Superior, do Quadro de Pessoal desta
Universidade, JOSE CARLOS FRANTZ, matrícula SIAPE 0357030, para exercer o cargo de Assessor do Reitor,
Código SRH 1388, código CD-4, com vigência a partir da data de publicação no Diário Oficial da União.
Processo nº 23078.510181/2019-06.
RUI VICENTE OPPERMANN
Reitor.
