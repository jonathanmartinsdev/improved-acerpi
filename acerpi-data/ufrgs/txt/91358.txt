Documento gerado sob autenticação Nº AYW.155.767.0PS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4230                  de  15/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CARLOS TORRES FORMOSO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia Civil da Escola de Engenharia, com a finalidade de
realizar  visita  e  participar  do "Global  Leadership Forum for  Construction Engineering and Management
Conference" junto à University of Huddersfield, em Huddersfield, Inglaterra, no período compreendido entre
02/06/2019 e 08/06/2019, incluído trânsito, com ônus limitado. Solicitação nº 83971.
RUI VICENTE OPPERMANN
Reitor
