Documento gerado sob autenticação Nº OSQ.530.101.FPM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9483                  de  28/11/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar o afastamento no país de Leandro Cesar de Godoy, ocupante do cargo de Professor do
Magistério Superior,  lotado e em exercício no Departamento de Zootecnia da Faculdade de Agronomia,  com
a finalidade de ministrar a disciplina "AQ104 - Reprodução e Larvicultura de Organismos Aquáticos", na
Universidade Nilton Lins,  em Manaus, Brasil,  no período compreendido entre 05/12/2016 e 15/12/2016,
incluído trânsito, com ônus limitado. Solicitação n° 25058.
JANE FRAGA TUTIKIAN
Vice-Reitora
