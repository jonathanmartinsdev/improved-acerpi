Documento gerado sob autenticação Nº JSK.822.979.5SL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8435                  de  19/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a partir  da data de publicação no Diário Oficial  da União,  a ocupante do cargo de
Assistente  em  Administração  -  701200,  do  Nível  de  Classificação  DIV,  do  Quadro  de  Pessoal  desta
Universidade, FRANCIELE MARQUES ZIQUINATTI, matrícula SIAPE 2091092, da função de Chefe da Seção de
Avaliação de Compras, vinc. ao Setor de Suprimentos e Logística da SUINFRA, Código SRH 870, Código FG-4,
para a qual foi designada pela Portaria nº 1967/2016 de 14/03/2016, publicada no Diário Oficial da União de
17/03/2016. Processo nº 23078.022447/2016-15.
JANE FRAGA TUTIKIAN
Vice-Reitora
