Documento gerado sob autenticação Nº WTV.205.235.MIH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1778                  de  02/03/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  02/03/2018,   referente  ao  interstício  de
02/09/2016  a  01/03/2018,  para  o  servidor  FRANCISCO  BASTOS  MOREIRA,  ocupante  do  cargo  de
Engenheiro-área -  701031,  matrícula  SIAPE 2055673,   lotado  na   Superintendência  de Infraestrutura,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  E  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  E  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.503398/2018-71:
ILB - Modalidades, Tipos e Fases da Licitação CH: 40 (13/12/2017 a 03/01/2018)
PROREXT - Curso em Diagnóstico Hidroenergético em Sistemas de Abastecimento 2 CH: 40 (20/07/2015 a
24/07/2015)
ENAP - Sistema Eletrônico de Informações - SEI CH: 20 (24/01/2017 a 13/02/2017)
ENAP - Básico em Orçamento Público CH: 30 (08/08/2017 a 04/09/2017)
Online SP - Curso Construção Civil CH: 50 (15/01/2018 a 07/02/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
