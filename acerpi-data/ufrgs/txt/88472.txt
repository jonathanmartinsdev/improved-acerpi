Documento gerado sob autenticação Nº VCE.506.650.DP1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2453                  de  19/03/2019
Nomeação  da  Representação  Discente  do
Diretório  Acadêmico  Leopoldo  Cortez  dos
Estudantes de Agronomia da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita para compor o Diretório Acadêmico Leopoldo Cortez dos
Estudantes de Agronomia, com mandato de 01 (um) ano, a contar de 29 de janeiro de 2019, atendendo ao
disposto nos artigos 175 e 178 do Regimento Geral da Universidade e 79 do Estatuto da Universidade e
considerando o processo nº 23078.506701/2019-78, conforme segue:
 
Presidente: Alexssander Henrique Sausen
Vice-presidente: Maiz Bortolomiol Dias
Secretária geral: Natália Giehl Palamar
Primeira-vice-secretária: Gabriela Silveira da Silva
Segunda vice-secretária: Mariana de Oliveira Lima
Terceiro vice-secretário: Wagner Martins Jordão
Tesoureiro: Rafael Tubino Maciel
Vice-tesoureira: Larissa Barreto Müller
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
