Documento gerado sob autenticação Nº VWI.754.364.IC2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9529                  de  29/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Tornar insubsistente a Portaria nº 9053/2016, de 10/11/2016, publicada no Diário Oficial da União
de 14/11/2016, que concedeu autorização para afastamento do País a EUGENIO AVILA PEDROZO, ocupante
do  cargo  de  Professor  do  Magistério  Superior,  lotado  e  em  exercício  no  Departamento  de  Ciências
Administrativas da Escola de Administração. Solicitação nº 24584.
RUI VICENTE OPPERMANN
Reitor
