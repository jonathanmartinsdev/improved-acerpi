Documento gerado sob autenticação Nº HXQ.151.554.C21, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8578                  de  24/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  JOANA BOSAK DE FIGUEIREDO,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Artes Visuais do Instituto de Artes, com a finalidade de
realizar  visita   à  Università  Ca'Foscari  Venezia,   Itália,  no  período  compreendido  entre  19/11/2016  e
23/12/2016, incluído trânsito, com ônus limitado. Solicitação nº 21248.
RUI VICENTE OPPERMANN
Reitor
