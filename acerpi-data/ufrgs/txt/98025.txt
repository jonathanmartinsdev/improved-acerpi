Documento gerado sob autenticação Nº ONR.076.713.HBE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8314                  de  12/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor ROQUE JUNIOR SARTORI BELLINASO,  ocupante do cargo de Técnico em
Agropecuária - 701214, lotado na Estação Experimental Agronômica, SIAPE 1637896, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 25/06/2019, tendo em vista a conclusão do
curso de Agronomia, conforme o Processo nº 23078.516420/2019-23.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
