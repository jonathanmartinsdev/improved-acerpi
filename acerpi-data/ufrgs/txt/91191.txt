Documento gerado sob autenticação Nº JHK.623.658.TVK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4154                  de  14/05/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  MARIA  IVANICE  VENDRUSCOLO,  matrícula  SIAPE  n°  2577132,  lotada  no
Departamento de Ciências Contábeis e Atuariais da Faculdade de Ciências Econômicas, para exercer a função
de Coordenadora da COMGRAD de Ciências Atuariais, Código SRH 1181, código FUC, com vigência a partir de
19/05/2019 até 18/05/2021. Processo nº 23078.509671/2019-51.
JANE FRAGA TUTIKIAN
Vice-Reitora.
