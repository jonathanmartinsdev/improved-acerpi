Documento gerado sob autenticação Nº GEK.844.519.HDC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
DEPARTAMENTO DE DESENVOLVIMENTO E GESTÃO DE PESSOAS
ESCOLA DE DESENVOLVIMENTO DE SERVIDORES DA UFRGS
Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
(51) 3308-3015/3219/3914/4596 - edufrgs@progesp.ufrgs.br
Processo n°: 23078.004090/13-25
Servidora: SUSANA BEHENCK SEIBEL
Cargo: Técnico em Contabilidade
Lotação: Pró-Reitoria de Planejamento e Administração
Ambiente Organizacional: Administrativo
Nível de Classificação e Nível de Capacitação: D III
PARECER N° 1372/2018
Trata este expediente da retificação do Parecer n° 248/2017, de 10/03/2017, que concede horário
especial para servidor estudante a SUSANA BEHENCK SEIBEL, Técnico em Contabilidade, com exercício na
Divisão  de  Gestão  de  Contratos  da  Pró-Reitoria  de  Planejamento  e  Administração.  Processo  nº
23078.004090/13-25.
Tendo em vista a exoneração da servidora em 30/06/2017, conforme publicação no DOU à fl. 125 do
processo
 
Onde se lê:
"a 15/07/2017",
leia-se:
"a 29/06/2017".
Em Data da Redação.
BIANCA SPODE BELTRAME
Divisão de Análise e Orientação do Desenvolvimento na Carreira
De acordo. Encaminhe-se à requerente, para ciência e conclusão do processo.
Em 14/11/2018.
KAREN WERLANG LUNKES
Diretora da Divisão de Análise e Orientação do Desenvolvimento na Carreira
