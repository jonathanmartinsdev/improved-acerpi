Documento gerado sob autenticação Nº GMT.113.948.F83, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10052                  de  31/10/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de NILO SERGIO MEDEIROS CARDOZO, Professor do Magistério
Superior, lotado e em exercício no Departamento de Engenharia Química da Escola de Engenharia, com a
finalidade de participar do "I  Congreso Internacional de Biotecnología:  La Biotecnología y sus tendencias,
aportes para la humanidad", em Cúcuta, Colômbia, no período compreendido entre 19/11/2017 e 25/11/2017,
incluído trânsito, com ônus limitado. Solicitação nº 32271.
RUI VICENTE OPPERMANN
Reitor
