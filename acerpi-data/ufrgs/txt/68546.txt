Documento gerado sob autenticação Nº POW.157.144.6FE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6511                  de  21/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de FLAVIO SANSON FOGLIATTO, Professor do Magistério Superior,
lotado no Departamento de Engenharia de Produção e Transportes da Escola de Engenharia e com exercício
no Programa de Pós-Graduação em Engenharia de Produção, com a finalidade de realizar  visita à Arkansas
State  University,  em  Lafayetteville,  Estados  Unidos,  no  período  compreendido  entre  17/09/2018  e
26/09/2018, incluído trânsito, com ônus UFRGS (diárias). Solicitação nº 47261.
RUI VICENTE OPPERMANN
Reitor
