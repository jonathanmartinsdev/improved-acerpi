Documento gerado sob autenticação Nº TCK.752.683.HE1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2808                  de  30/03/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a LUIZ CARLOS ALVES EBERT,
matrícula SIAPE nº 0356519, no cargo de Mestre de Edificações e Infraestrutura, nível de classificação D, nível
de capacitação I,  padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho, com exercício no Núcleo de Infraestrutura da Gerência Administrativa do Instituto de Pesquisas
Hidráulicas, com proventos integrais. Processo 23078.200340/2017-97.
RUI VICENTE OPPERMANN
Reitor.
