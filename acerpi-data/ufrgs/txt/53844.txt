Documento gerado sob autenticação Nº PFU.544.783.IOT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3193                  de  02/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de FLAVIA PILLA DO VALLE, Professor do Magistério Superior,
lotada no Departamento de Educação Física da Escola de Educação Física,  Fisioterapia e Dança e com
exercício na Comissão de Graduação de Dança, com a finalidade de participar da "LABAN 2018 International
Conference",  com ônus UFRGS (Pró-Reitoria de Pesquisa -  diárias)  e  realizar  visita à José Limón Dance
Foundation,  Inc.,  em Nova  York,  Estados  Unidos,  com ônus  limitado,  no  período  compreendido  entre
30/05/2018 e 09/06/2018, incluído trânsito. Solicitação nº 34111.
RUI VICENTE OPPERMANN
Reitor
