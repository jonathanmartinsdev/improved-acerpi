Documento gerado sob autenticação Nº FWJ.268.745.Q3T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4842                  de  06/06/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°47858,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de , do Quadro de Pessoal desta
Universidade, ALEXANDRE BRENTANO (Siape: 1611853 ),  para substituir   SAULO PINHEIRO DE QUEIROZ
(Siape: 1729364 ), Procurador-Geral da Procuradoria-Geral, Código CD-3, em seu afastamento por motivo de
férias, no período de 10/06/2019 a 17/06/2019, com o decorrente pagamento das vantagens por 8 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
