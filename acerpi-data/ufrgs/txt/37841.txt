Documento gerado sob autenticação Nº QJV.243.394.OQ8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3933                  de  05/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de INGRID OLIVEIRA DE NUNES, Professor do Magistério Superior,
lotada e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a finalidade
de realizar visita  à Technische Universtät Dortmund, Alemanha, no período compreendido entre 31/07/2017
e 01/09/2017, incluído trânsito, com ônus CAPES/Alexander von Humboldt. Solicitação nº 27115.
RUI VICENTE OPPERMANN
Reitor
