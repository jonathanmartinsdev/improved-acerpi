Documento gerado sob autenticação Nº JNC.263.626.M6R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2841                  de  01/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de DANTE AUGUSTO COUTO BARONE, Professor do Magistério
Superior, lotado e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a
finalidade de participar do "XXVII ESANN - European Symposium on Artificial Neural Networks", em Bruges,
Bélgica, no período compreendido entre 22/04/2019 e 28/04/2019, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa: diárias). Solicitação nº 83048.
RUI VICENTE OPPERMANN
Reitor
