Documento gerado sob autenticação Nº GKU.256.815.MLE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3914                  de  28/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CHRISTIAN COSTA KIELING, Professor do Magistério Superior,
lotado e em exercício no Departamento de Psiquiatria e Medicina Legal da Faculdade de Medicina, com a
finalidade  de  participar  do  "Workshop  on  Global  Mental  Health",  em  Londres,  Inglaterra,  no  período
compreendido  entre  27/06/2018  e  30/06/2018,  incluído  trânsito,  com  ônus  CNPq  (Chamada  Universal
14/2014 - Faixa A). Solicitação nº 45728.
RUI VICENTE OPPERMANN
Reitor
