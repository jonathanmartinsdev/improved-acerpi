Documento gerado sob autenticação Nº IMK.053.168.436, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             507                  de  16/01/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, com vigência a partir de 17/01/2019, como segue:
 
Transformar: Secretário da Prefeitura do Campus do Vale da SUINFRA, código SRH 871, código FG-5,
em Chefe da Gerência Administrativa da SUINFRA - Posto Vale, código SRH 871, código FG-5.
 
 
Processo nº 23078.535188/2018-41.
 
RUI VICENTE OPPERMANN
Reitor.
