Documento gerado sob autenticação Nº ORY.691.827.8K6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4691                  de  25/05/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Retificar a Portaria n° 1573/2017, de 16/02/2017,  que concedeu progressão funcional, no Quadro
desta  Universidade,  à  Professora  SIMONE  MARTINS  DE  CASTRO,  com  exercício  no  Departamento  de
Análises da Faculdade de Farmácia, da classe D de Professor Associado, nível 01, para a classe D de Professor
Associado, nível 02, com vigência financeira a partir de 01/08/2016. Processo nº 23078.506765/2016-26.
 
 
Onde se lê:
 ...referente  ao  interstício  de  01/08/2014  a  31/07/2016,  comvigência  fianceira  a  partir  de
01/08/2016...
leia-se:
...referente  ao  interstício  de  31/07/2014  a  30/11/2016,  com  vigência  financeira  a  partir  de
01/12/2016..., ficando ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
