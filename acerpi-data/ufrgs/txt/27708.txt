Documento gerado sob autenticação Nº HFX.082.405.76I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7327                  de  15/09/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Declarar vaga, a partir  de 08/09/2016, a função de Diretora da Divisão de Emissão, Registro e
Revalidação  de  Diploma  do  DCRD  da  Graduação  da  PROGRAD,  Código  SRH  419,  Código  FG-5,  desta
Universidade, tendo em vista a Aposentadoria c/ proventos Integrais (Art. 3º Inc. I, II, III) EC 47/05 de ZORAIDE
APARECIDA FERNANDES NUNES,  matrícula SIAPE n° 0355927, conforme Portaria n° 6947/2016, de 6 de
setembro de 2016, publicada no Diário Oficial da União do dia 08/09/2016. Processo nº 23078.020554/2016-
09.
RUI VICENTE OPPERMANN
Vice-Reitor
