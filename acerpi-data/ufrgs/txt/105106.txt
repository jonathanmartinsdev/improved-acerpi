Documento gerado sob autenticação Nº ACA.317.564.OUM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1668                  de  21/02/2020
O PRÓ-REITOR DE GRADUAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de
suas atribuições, considerando o disposto na Portaria nº 7626, de 29 de setembro de 2016
RESOLVE
Tornar sem efeito a Portaria nº 11375 de 20 de dezembro de 2019, a partir desta data.
VLADIMIR PINHEIRO DO NASCIMENTO
Pró-Reitor de Graduação
