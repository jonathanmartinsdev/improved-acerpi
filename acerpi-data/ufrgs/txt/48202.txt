Documento gerado sob autenticação Nº ZEE.710.621.8SP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11238                  de  15/12/2017
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
Designar os servidores,  HENRIQUE GOMES DOS SANTOS, ocupante do cargo de Engenheiro-área,
lotado na Superintendência  de  Infraestrutura  e  com exercício  na  Oficinas  de  Produção e  Manutenção
Mecânica, ALEXANDRE DE LIMA,  ocupante do cargo de Engenheiro-área, lotado na Superintendência de
Infraestrutura e com exercício na Prefeitura Campus Saúde e Olímpico, CAMILA SIMONETTI, ocupante do
cargo de  Engenheiro-área,  lotada na  Superintendência  de  Infraestrutura  e  com exercício  na  Prefeitura
Campus Centro, SUSANA MARIA WERNER SAMUEL, ocupante do cargo de Professor do Ensino Superior,
lotada e em exercício no Departamento de Odontologia Conservadora, para sob a presidência do primeiro,
constituirem a Comissão de Recebimento, em atendimento à alínea "b" do inciso I  do artigo 73 da Lei
8666/93, para emissão do Termo de Recebimento Definitivo Parcial da Obra (Sistema de Climatização) do
contrato  118/2011,  cujo  objeto  é  "Construção  do  Hospital  de  Ensino  Odontológico",  Processo  nº
23078.203371/10-71.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
