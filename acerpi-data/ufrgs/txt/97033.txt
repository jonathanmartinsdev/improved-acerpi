Documento gerado sob autenticação Nº BHR.726.636.JGN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7689                  de  26/08/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°49910,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, LUCAS WINCK ESTEVES (Siape: 2259021 ),  para
substituir   ALINE RODRIGUES SANTOS (Siape: 1830492 ), Secretário do Hospital de Clínicas Veterinárias,
Código FG-2, em seu afastamento por motivo de férias, no período de 26/08/2019 a 06/09/2019, com o
decorrente pagamento das vantagens por 12 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
