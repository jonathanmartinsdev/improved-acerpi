Documento gerado sob autenticação Nº LEO.285.641.TGU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5677                  de  08/07/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar  na  Pró-Reitoria  de  Assuntos  Estudantis,  com  exercício  na  Coordenação  de  Compras  e
Execução Orçamentária,  Ambiente Organizacional   Administrativo,  JULIANO PAHIM COLLING,  nomeado
conforme Portaria Nº 4476/2019 de 22 de maio de 2019, publicada no Diário Oficial da União no dia 30 de
maio  de  2019,  em efetivo  exercício  desde 25  de  junho de  2019,  ocupante  do cargo de  TÉCNICO EM
CONTABILIDADE,  classe D,  nível  I,  padrão 101,  no Quadro de Pessoal  desta Universidade.  Processo n°
23078.515720/2019-95.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
