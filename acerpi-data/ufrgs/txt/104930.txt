Documento gerado sob autenticação Nº KSY.152.030.OM3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1307                  de  06/02/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, à
Professora SILVIA REGINA FERABOLLI, matrícula SIAPE 2357608, lotada no Departamento de Economia e
Relações  Internacionais  da  Faculdade  de  Ciências  Econômicas  e  com  exercício  no  Programa  de  Pós-
Graduação em Estudos Estratégicos Internacionais, da classe A  de Professor Adjunto A, nível 02, para a
classe C  de Professor Adjunto, nível 01, com vigência financeira a partir de 04/02/2020, de acordo com o que
dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554, de 20 de junho de
2013 do Ministério da Educação e a Decisão nº 331/2017. Processo nº 23078.502434/2020-01.
JANE FRAGA TUTIKIAN
Vice-Reitora.
