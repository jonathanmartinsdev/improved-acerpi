Documento gerado sob autenticação Nº DLS.311.353.C4E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5145                  de  17/06/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  CLECIO  HOMRICH  DA  SILVA,  matrícula  SIAPE  n°  3192667,  lotado  no
Departamento de Pediatria da Faculdade de Medicina, para exercer a função de Chefe do Depto de Pediatria
da Faculdade de Medicina, Código SRH 92, código FG-1, com vigência a partir de 29/06/2019 até 28/06/2021,
por ter sido reeleito, sem prejuízo e cumulativamente com a função de Coordenador Substituto do PPG em
Saúde da Criança e do Adolescente. Processo nº 23078.513984/2019-12.
JANE FRAGA TUTIKIAN
Vice-Reitora.
