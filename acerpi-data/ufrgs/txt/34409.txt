Documento gerado sob autenticação Nº KEF.941.233.EOM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1520                  de  15/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de TIAGO ROBERTO BALEN,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia Elétrica da Escola de Engenharia, com a finalidade de
participar do "18th IEEE Latin-American Test Symposium", em Bogota, Colômbia, no período compreendido
entre 12/03/2017 e 14/03/2017, incluído trânsito, com ônus CNPq (Projeto de um conversor Analógico Digital
e Sub-Circuitos analógicos tolerantes à Radiação, processo nº 56947/2014-0). Solicitação nº 26167.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
