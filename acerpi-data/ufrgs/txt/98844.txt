Documento gerado sob autenticação Nº MXS.082.481.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8908                  de  02/10/2019
  Designa Comissão para elaborar proposta
de Código de Ética e Convivência Discente
na UFRGS.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Designar
ELTON LUIS BERNARDI CAMPANARO
VÂNIA CRISTINA SANTOS PEREIRA
LEONARDO DA SILVA PETTENON
 
para,  sob  a  presidência  do  primeiro,  compor  Comissão  para  elaborar  proposta  de  Código  de  Ética  e
Convivência Discente na UFRGS, no prazo de 30 (trinta) dias, prorrogáveis.
 
RUI VICENTE OPPERMANN,
Reitor.
