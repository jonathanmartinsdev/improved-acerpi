Documento gerado sob autenticação Nº TGF.055.734.VNN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6180                  de  19/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°21690,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do Quadro de  Pessoal  desta  Universidade,  ARLEI  SANDER DAMO (Siape:  1545172 ),   para
substituir   CORNELIA ECKERT (Siape: 0356699 ), Chefe do Depto de Antropologia do IFCH, Código FG-1, em
seu afastamento  do país,  no  período de  09/08/2016 a  13/08/2016,  com o  decorrente  pagamento  das
vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
