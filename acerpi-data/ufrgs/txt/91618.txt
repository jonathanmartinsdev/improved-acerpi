Documento gerado sob autenticação Nº YWT.754.476.TM8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4365                  de  21/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de MARCIO DORN, Professor do Magistério Superior, lotado e em
exercício no Departamento de Informática Teórica do Instituto de Informática, com a finalidade de participar
da  "International  Conference  on  Computational  Science  -  ICCS  2019",  em  Faro,  Portugal,  no  período
compreendido entre 10/06/2019 e 15/06/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa:
diárias). Solicitação nº 72950.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
