Documento gerado sob autenticação Nº QOS.327.468.BPQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3475                  de  10/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ANDREA GABRIELA FERRARI, Professor do Magistério Superior,
lotada e em exercício no Departamento de Psicanálise e Psicopatologia do Instituto de Psicologia, com a
finalidade de participar da "Conference Children's Rights and Early Intervention", em Estocolmo, Suécia, no
período compreendido entre 06/06/2016 e 10/06/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa - diárias). Solicitação nº 18246.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
