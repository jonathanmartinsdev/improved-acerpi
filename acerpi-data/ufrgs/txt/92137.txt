Documento gerado sob autenticação Nº ISC.974.773.D3O, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 775/2019-PROGESP Porto Alegre, 03 de junho de 2019.
Senhor  Diretor
Encaminhamos  a  servidora  ALYNNI  LUIZA  RICCO  AVILA,  ocupante  do  cargo  Assistente  em
Administração, para exercício nessa Unidade no dia 03 de junho de 2019.
Solicitamos que nos sejam encaminhadas, no prazo máximo de 03 dias, as seguintes informações:
- atividades a serem desenvolvidas pela servidora;
- local de exercício (divisão, setor), observando a hierarquia dos órgãos registrada no SRH;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal.
Informamos que este encaminhamento se faz para reposição de vaga, em virtude de remoção do
servidor Marcelo Teodoro de Assis. 
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
Ilmo. Sr.
Professor RAFAEL ROESLER,
Pró-Reitor de Pesquisa,
N/Universidade.
