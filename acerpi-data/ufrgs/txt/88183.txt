Documento gerado sob autenticação Nº QRH.459.133.UNU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2322                  de  14/03/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARIA DO CARMO GONÇALVES CURTIS, matrícula SIAPE n° 1766644, lotada e em exercício no
Departamento de Design e Expressão Gráfica da Faculdade de Arquitetura, da classe C  de Professor Adjunto,
nível 01, para a classe C  de Professor Adjunto, nível 02, referente ao interstício de 19/02/2016 a 18/02/2018,
com vigência financeira a partir de 08/02/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro
de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.502236/2019-04.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
