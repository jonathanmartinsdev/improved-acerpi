Documento gerado sob autenticação Nº BQZ.383.929.87C, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3042                  de  25/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  ELISEO BERNI  REATEGUI,  matrícula  SIAPE  n°  1612443,  com exercício  no  Programa de  Pós-
Graduação em Informática na Educação do Centro de Estudos Interdisciplinares em Novas Tecnologias da
Educação, da classe C  de Professor Adjunto, nível 04, para a classe D  de Professor Associado, nível 01,
referente ao interstício de 26/02/2014 a 25/02/2016, com vigência financeira a partir de 22/04/2016, de
acordo com o que dispõe a Decisão nº 197/2006-CONSUN, alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.008612/2016-18.
RUI VICENTE OPPERMANN
Vice-Reitor
