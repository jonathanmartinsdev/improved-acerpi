Documento gerado sob autenticação Nº WJU.692.547.TUV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2399                  de  17/03/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de JOEL GUSMAO OUTTES WANDERLEY FILHO,  Professor do
Magistério Superior, lotado e em exercício no Departamento de Urbanismo da Faculdade de Arquitetura,
com a finalidade de participar do "Annual Meeting of the Association of American Geographers", em Boston,
Massachusets, Estados Unidos, no período compreendido entre 04/04/2017 e 10/04/2017, incluído trânsito,
com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 26307.
RUI VICENTE OPPERMANN
Reitor
