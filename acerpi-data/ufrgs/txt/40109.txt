Documento gerado sob autenticação Nº ANG.488.544.TGG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5588                  de  27/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARCIA LORENA FAGUNDES CHAVES, matrícula SIAPE n° 0357652, lotada e em exercício no
Departamento de Medicina Interna da Faculdade de Medicina, da classe D  de Professor Associado, nível 04,
para a classe E  de Professor Titular,  referente ao interstício de 30/12/2012 a 04/05/2017, com vigência
financeira a partir de 05/05/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Decisão nº 232/2014 - CONSUN. Processo nº 23078.507055/2017-02.
JANE FRAGA TUTIKIAN
Vice-Reitora.
