Documento gerado sob autenticação Nº WJW.007.817.NRS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4452                  de  20/06/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Dispensar, a partir da data de publicação no Diário Oficial da União, o(a) ocupante do cargo de
Técnico em Mecânica - 701245, do Nível de Classificação DII, do Quadro de Pessoal desta Universidade,
SERGIO LUIS RODRIGUES, CPF nº 28407547034, matrícula SIAPE 1861459 da função de Diretor da Div de
Estrutura e Logística do Núcleo de Apoio ao Ensino da Diretoria Administrativa do Campus Litoral Norte,
Código SRH 1453, Código FG-3, para a qual foi designado(a) pela Portaria nº 6164/2015 de 10/08/2015,
publicada no Diário Oficial da União de 12/08/2015, por ter sido designado para outra função gratificada.
Processo nº 23078.013011/2016-27.
RUI VICENTE OPPERMANN
Vice-Reitor
