Documento gerado sob autenticação Nº CKD.550.106.TJ3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2955                  de  04/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  28/03/2017,   referente  ao  interstício  de
14/08/2014 a 27/03/2017, para a servidora SUSANA DA ROSA TRAVI, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 0357019,  lotada  na  Faculdade de Odontologia, passando do Nível
de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.004903/2017-18:
Formação Integral de Servidores da UFRGS VI CH: 151 Carga horária utilizada: 150 hora(s) / Carga horária
excedente: 1 hora(s) (18/11/2009 a 24/03/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
