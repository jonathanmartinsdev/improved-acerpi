Documento gerado sob autenticação Nº MVZ.764.370.I76, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5098                  de  09/06/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  UNDERLÉA MIOTTO BRUSCATO,  Professor  do Magistério
Superior,  lotada e  em exercício  no Departamento de Arquitetura  da  Faculdade de Arquitetura,  com a
finalidade de realizar visita à Universidad de Zaragoza, Espanha, no período compreendido entre 10/07/2017
e 28/07/2017, incluído trânsito, com ônus limitado. Solicitação nº 27242.
RUI VICENTE OPPERMANN
Reitor
