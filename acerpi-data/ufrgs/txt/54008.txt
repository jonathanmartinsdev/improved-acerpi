Documento gerado sob autenticação Nº UNF.990.194.UN7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3305                  de  07/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ALBERTO BRACAGIOLI NETO, matrícula SIAPE n° 2266956, lotado e em exercício no Departamento
de Horticultura e Silvicultura da Faculdade de Agronomia, da classe A  de Professor Adjunto A, nível 01, para a
classe A  de Professor Adjunto A, nível 02, referente ao interstício de 01/12/2015 a 30/11/2016, com vigência
financeira a partir de 16/01/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.500025/2018-48.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
