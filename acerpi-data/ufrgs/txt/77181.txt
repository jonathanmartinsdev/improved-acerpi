Documento gerado sob autenticação Nº PNQ.911.986.8VL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1751                  de  21/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
                         Autorizar o afastamento no País de FERNANDA TARABAL LOPES, ocupante do cargo de
Professor do Magistério Superior, lotada e com exercício no Departamento de Ciências Administrativas, com
a finalidade de realizar estudos em nível de Pós-Doutorado junto à Pontifícia Universidade Católica do Rio de
Janeiro, Rio de Janeiro/RJ, no período compreendido entre 01/03/2019 e 29/02/2020, com ônus limitado.
Processo 23078.533929/2018-50.
RUI VICENTE OPPERMANN
Reitor.
