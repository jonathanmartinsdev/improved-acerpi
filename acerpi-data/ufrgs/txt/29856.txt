Documento gerado sob autenticação Nº BQB.427.386.QJH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8671                  de  26/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  29/08/2016,
correspondente ao grau Insalubridade Média, ao servidor RAFAEL GOMES DIONELLO, Identificação Única
15659755, Professor do Magistério Superior, com exercício no Departamento de Fitossanidade da Faculdade
de Agronomia, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei
8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Insalubres conforme
Laudo Pericial  constante  no Processo nº  23078.512042/2016-66,  Código  SRH n°  22927 e  Código  SIAPE
2016003903.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
