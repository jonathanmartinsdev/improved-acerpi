Documento gerado sob autenticação Nº MWM.889.193.CR0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9445                  de  17/10/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a JAIRO LASER
PROCIANOY, matrícula SIAPE nº 0356816, no cargo de Professor Titular da Carreira do Magistério Superior,
do  Quadro  desta  Universidade,  no  regime  de  vinte  horas  semanais  de  trabalho,  com  exercício  no
Departamento de Ciências Administrativas da Escola de Administração, com proventos integrais. Processo
23078.523967/2019-85.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
