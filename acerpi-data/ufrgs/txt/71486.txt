Documento gerado sob autenticação Nº RIF.741.754.LF0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8695                  de  29/10/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 26 de outubro de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
LUIZ  FERNANDO  MACHADO  SOARES,  ocupante  do  cargo  de  Assistente  em  Administração,  Ambiente
Organizacional Administrativo, Código 701200, Classe D, Nível de Capacitação IV, Padrão de Vencimento 04,
SIAPE nº. 2080641 da Escola de Administração para a lotação Faculdade de Educação, com novo exercício no
Núcleo de apoio a Projetos e Execução Financeira da Gerência Administrativa da Faculdade de Educação.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
