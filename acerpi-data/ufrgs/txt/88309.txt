Documento gerado sob autenticação Nº NIS.825.710.E3T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2422                  de  18/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  15/02/2019,   referente  ao  interstício  de
15/08/2017 a 14/02/2019, para a servidora CARMEN PATRICIA GUERREIRO ROEDES, ocupante do cargo de
Técnico em Edificações - 701228, matrícula SIAPE 2146555,  lotada  na  Superintendência de Infraestrutura,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.503290/2019-69:
Formação Integral de Servidores da UFRGS III CH: 77 (03/02/2017 a 24/07/2018)
ILB - ÉTICA E ADMINISTRAÇÃO PÚBLICA CH: 40 Carga horária utilizada: 23 hora(s) / Carga horária excedente:
17 hora(s) (07/01/2019 a 27/01/2019)
Prime Cursos - AUTOCAD 2017 Básico CH: 50 (08/12/2018 a 04/01/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
