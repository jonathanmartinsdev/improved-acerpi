Documento gerado sob autenticação Nº UVW.785.893.UPC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             6527                  de  20/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto nas Decisões nºs 319/2016 e 144/2017 do Conselho
Universitário, nas Resoluções nºs 41/2016 e 17/2017 do Conselho de Ensino, Pesquisa e Extensão e conforme
eleição realizada em 13/07/2017, 
RESOLVE:
Designar
 
MARIA LUIZA SARAIVA PEREIRA
CARLA ZANELLA SOUZA
HENRIQUE CAETANO NARDI
CHARLES FLORCZAK ALMEIDA
LOURENCO BRITO FELIN
JOÃO FRANCISCO MILANI DE SOUZA CASTRO
LUCAS PIZZOLATTO KONZEN
DENISE MARIA COMERLATO
FERNANDA NOGUEIRA
EDSON MENDES DA SILVA JUNIOR
EZEQUIEL ERIC OLEJAZ FREIRE
SAMARA AYRES MORAES
 
para, sob a presidência da primeira e vice-presidência da segunda, constituírem Comissão
Especial  Mista CONSUN/CEPE para análise da Decisão nº 268/2012, referente ao Programa de
Ações Afirmativas da UFRGS, a partir desta data.
 
Também fazem parte desta comissão, sem direito a voto, os seguintes representantes:
ARAO DA SILVA MORAES - Pró-Reitoria de Graduação - PROGRAD
Documento gerado sob autenticação Nº UVW.785.893.UPC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
EDILSON AMARAL NABARRO - Coordenadoria do Programa de Ações Afirmativas - CAF
VERA ROSANE RODRIGUES DE OLIVEIRA - Pró-Reitoria de Assuntos Estudantis - PRAE
DENISE FAGUNDES JARDIM - Comitê contra a Intolerância e a Discriminação - UFRGS
FABIANO RIOS HECK - Centro de Processamento de Dados - CPD
CLEDISSON GERALDO DOS SANTOS JUNIOR - Associação de Pós-Graduandos - APG
MARCOS VESOLOSQUZKI - Diretório Central dos Estudantes - DCE
 
JANE FRAGA TUTIKIAN,
Vice-Reitora, no exercício da Reitoria.
