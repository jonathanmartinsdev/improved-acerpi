Documento gerado sob autenticação Nº EIV.398.877.RCD, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6405                  de  17/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor RAFAEL COSTA RODRIGUES, matrícula SIAPE n° 2687227, lotado e em exercício no Departamento
de Tecnologia dos Alimentos do Instituto de Ciências e Tecnologia de Alimentos, da classe C  de Professor
Adjunto, nível 04, para a classe D  de Professor Associado, nível 01, referente ao interstício de 14/06/2016 a
13/06/2018, com vigência financeira a partir de 25/06/2018, de acordo com o que dispõe a Lei 12.772 de 28
de  dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.513975/2018-32.
JANE FRAGA TUTIKIAN
Vice-Reitora.
