Documento gerado sob autenticação Nº QOV.067.101.KE3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6166                  de  12/07/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°31669,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MAURICIO ASSUMPÇÃO MOYA (Siape: 1718759 ),
 para substituir   LUIS GUSTAVO MELLO GROHMANN (Siape: 0382789 ), Chefe do Departamento de Ciência
Política do IFCH,  Código FG-1,  em seu afastamento por motivo de férias,  no período de 17/07/2017 a
31/07/2017, com o decorrente pagamento das vantagens por 15 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
