Documento gerado sob autenticação Nº UIC.076.074.I4C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8163                  de  11/10/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°42617,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, FLAVIO JOSE LORINI (Siape: 1058309 ),  para substituir  
THAMY CRISTINA HAYASHI (Siape: 1008887 ), Coordenador da COMGRAD de Engenharia Mecânica, Código
FUC, em seu afastamento por motivo de férias, no período de 13/10/2018 a 20/10/2018, com o decorrente
pagamento das vantagens por 8 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
