Documento gerado sob autenticação Nº SVX.691.251.9CJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4219                  de  09/06/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder à ocupante do cargo de Professor do Magistério Superior, KARIN GOLDBERG, lotada e em
exercício no Departamento de Mineralogia e Petrologia do Instituto de Geociências, Licença para o Trato de
Assuntos Particulares, a partir de 01 de agosto de 2016, pelo prazo de dois anos, nos termos do artigo 91 da
Lei n° 8.112, de 11 de dezembro de 1990, alterado pela Medida Provisória n° 2.225-45, de 4 de setembro de
2001, e conforme previsto no artigo 3°, inciso II, da Portaria SEGRT/MP n° 35, de 01 de março de 2016.
Processo n° 23078.201595/2016-96.
CARLOS ALEXANDRE NETTO
Reitor
