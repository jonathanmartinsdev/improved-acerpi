Documento gerado sob autenticação Nº ZMD.219.571.OQ8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3923                  de  05/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de TARCÍSIO ABREU SAURIM, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia de Produção e Transportes da Escola de Engenharia,
com a finalidade de participar do "7th Symposium of the Resilience Engineering Association", em Liege,
Bélgica, no período compreendido entre 25/06/2017 e 30/06/2017, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa - diárias). Solicitação nº 27027.
RUI VICENTE OPPERMANN
Reitor
