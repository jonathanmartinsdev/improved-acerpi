Documento gerado sob autenticação Nº WSN.889.963.N62, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
DEPARTAMENTO DE DESENVOLVIMENTO E GESTÃO DE PESSOAS
ESCOLA DE DESENVOLVIMENTO DE SERVIDORES DA UFRGS
Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
(51) 3308-3015/3219/3914/4596 - edufrgs@progesp.ufrgs.br
Processo n°: 23078.003957/2017-66
Servidora: SABRINA DA ROSA POJO
Cargo: Administrador
Lotação: Secretaria de Desenvolvimento Tecnológico
Ambiente Organizacional: Administrativo
Nível de Classificação e Nível de Capacitação: E IV
PARECER N° 569/2018
Trata  este  expediente  da  retificação  do  Parecer  n°  345/2018,  de  22/03/2018,  que  concede
Afastamento  Parcial  à  SABRINA  DA  ROSA  POJO,  Administrador,  com  exercício  na  Secretaria  de
Desenvolvimento Tecnológico, conforme Processo nº 23078.003957/2017-66.
 
Onde se lê:
"no período de 01/07/2018 a 31/03/2019",
leia-se:
"no período de 01/07/2018 a 31/07/2018".
Em 07/05/2018.
KAREN WERLANG LUNKES
Divisão de Análise e Orientação do Desenvolvimento na Carreira
De acordo. 
Em 07/05/2018.
CRISTIANE DORNELLES REMIAO DIFINI
Diretora da Divisão de Análise e Orientação do Desenvolvimento na Carreira
