Documento gerado sob autenticação Nº HBQ.383.027.9HS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7057                  de  06/08/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a ENIO ROBERTO BORSATTO,
matrícula SIAPE nº 0352222, no cargo de Assistente em Administração, nível de classificação D, nível de
capacitação I, padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de trabalho,
com exercício no Setor de Controle e Execução Orçamentária da Escola de Engenharia,  com proventos
integrais. Processo 23078.513773/2019-71.
RUI VICENTE OPPERMANN
Reitor.
