Documento gerado sob autenticação Nº VDJ.093.729.6KF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2735                  de  29/03/2017
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7793, de 03 de outubro de 2016
RESOLVE
Designar os servidores  ALEXANDRE ALVES PELLIN, ocupante do cargo de Engenheiro-área, lotado
na Superintendência de Infraestrutura e com exercício na Prefeitura Campus Saúde e Olímpico, EDSON LUIS
NICOLAIT  FERNANDES,  ocupante  do  cargo  de  Engenheiro-área,  lotado  na  Superintendência  de
Infraestrutura e com exercício na Prefeitura Campus Saúde e Olímpico, VIRGINIA DE LIMA FERNANDES,
ocupante do cargo de Engenheiro-área, lotada na Superintendência de Infraestrutura e com exercício no
Setor de Patrimônio Histórico, para, sob a presidência do primeiro, comporem Grupo de Trabalho para, no
prazo de 60 dias, a partir dia 03 de abril de 2017, realizar a avaliação e laudo técnico das estruturas expostas
da Escola de Enfermagem (prédio nº 21.103), da marquise da Creche Francesca Zacaro Faraco (21.104), das
lajes  em  balanço  da  Faculdade  de  Farmácia  (21.106) ,  das  la jes  nas  janelas  do  prédio
RU02/Gráfica/Editora/Farmácia  Popular  (21.113),  do  telhado  verde  da  Faculdade  de  Biblioteconomia  e
Comunicação  (22.201)  e  do  terraço/circulação  do  prédio  de  Administração  e  Salas  de  Aulas  (31.102),
conforme processo UFRGS nº 23078.009934/2016-84, atentando ao regramento da Prefeitura Municipal de
Porto Alegre.
ANDREA PINTO LOGUERCIO
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
