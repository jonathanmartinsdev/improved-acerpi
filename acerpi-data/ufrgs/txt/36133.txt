Documento gerado sob autenticação Nº JCH.199.792.0QV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2700                  de  28/03/2017
A VICE-REITORA  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Tornar  insubsistente a  Portaria  nº  1794/2017,  de 23/02/2017,  que Declarou Vaga a  função de
Coordenador da Comissão Interna de Supervisão do Plano de Carreira dos Cargos Técnicos-Administrativos
em Educação ocupada por SILVIO ROBERTO RAMOS CORREA. 
JANE FRAGA TUTIKIAN
Vice-Reitora
