Documento gerado sob autenticação Nº BNX.234.885.MIU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4519                  de  22/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país  de RAFAEL PERETTI PEZZI,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Física do Instituto de Física, com a finalidade de trabalho de
campo junto ao European Laboratory for Particle Physics, em Genebra, Suíça, no período compreendido
entre 28/06/2017 e 29/07/2017, incluído trânsito, com ônus UFRGS (Instituto de Física - auxílio parcial de
diárias). Solicitação nº 28207.
RUI VICENTE OPPERMANN
Reitor
