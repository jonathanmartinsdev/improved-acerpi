Documento gerado sob autenticação Nº RFY.093.130.9JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             16                  de  02/01/2019
  Delegação de competência à Diretora do
Instituto  de  Ciências  e  Tecnologia  de
Alimentos,  para celebração de Contratos
previstos  na Portaria  nº  2679,  de 27 de
maio de 2011.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
              Art. 1º Delegar competência à Professora SIMONE HICKMANN FLORES, Diretora do Instituto de
Ciências e Tecnologia de Alimentos, para celebrar Contratos previstos no Art. 1º, inciso II, alínea f, e inciso III,
alínea g, da Portaria nº 2679, de 27 de maio de 2011.
 
   Art.  2º Revogar a Portaria nº 4572 de 22 de maio de 2017 .
RUI VICENTE OPPERMANN,
Reitor.
