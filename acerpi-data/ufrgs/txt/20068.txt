Documento gerado sob autenticação Nº UKN.030.478.EEP, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2377                  de  31/03/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 5263, de 01 de outubro de
2012, e conforme a Solicitação de Afastamento n°18342,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, AURORA CARNEIRO ZEN (Siape: 2610712 ),  para
substituir   FLAVIO RECH WAGNER (Siape: 6352401 ), Diretor do Parque Científico e Tecnológico da UFRGS,
Código CD-4,  em seu afastamento do país,  no período de 02/04/2016 a 07/04/2016, com o decorrente
pagamento das vantagens por 6 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
