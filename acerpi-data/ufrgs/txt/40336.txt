Documento gerado sob autenticação Nº LJL.938.282.FDT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5746                  de  30/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder o Adicional de Periculosidade, no percentual de 10%, a partir de 19/04/2017, ao servidor
JULIO DA SILVA MOREIRA, Identificação Única 3553477, Eletricista, com exercício no Setor de Infraestrutura
do Colégio de Aplicação, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado
com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Perigosas
conforme Laudo Pericial constante no Processo n º 23078.504461/2017-13, Código SRH n° 23078 e Código
SIAPE n° 2017001446.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
