Documento gerado sob autenticação Nº VRR.403.488.4B3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10333                  de  08/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 40, parágrafo 1º, inciso I, da Constituição Federal, c/c
art. 6-A da Emenda Constitucional nº 41, de 19 de dezembro de 2003, incluído pela Emenda Constitucional nº
70, de 29 de março de 2012, e art.  186, parágrafo 1º,  da Lei nº 8.112, de 11 de dezembro de 1990, a
DIMITRIOS SAMIOS, matrícula SIAPE nº 0357608, no cargo de Professor Titular da Carreira do Magistério
Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no Departamento
de Físico-Química do Instituto de Química, com proventos integrais e incorporando a vantagem pessoal de
que trata a Lei nº 9.624, de 2 de abril de 1998, que assegurou o disposto no artigo 3º  da Lei nº 8.911, de 11
de julho de 1994. Processo 23078.020394/2017-71.
RUI VICENTE OPPERMANN
Reitor.
