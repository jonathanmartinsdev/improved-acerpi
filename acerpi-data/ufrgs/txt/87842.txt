Documento gerado sob autenticação Nº UWO.637.054.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2203                  de  11/03/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Retificar a Portaria n° 8933/2018, de 01/11/2018, que concedeu jornada de trabalho reduzida com
remuneração proporcional à servidora VIVIANE MARIA DOS SANTOS SOARES, matrícula SIAPE n° 1860185,
ocupante do cargo de Assistente em Administração - 701200, lotada no Colégio de Aplicação e com exercício
no Setor de Finanças e Suprimentos do Colégio de Aplicação, alterando a jornada de trabalho de oito horas
diárias e quarenta horas semanais para 6 horas diárias e 30 semanais, no período de 01 de novembro de
2018 a 01 de novembro de 2019, de acordo com os artigos 5º a 7º da Medida Provisória nº 2.174-28, de 24 de
agosto de 2001, e conforme o Processo nº 23078.525851/2018-08.
 
Onde se lê:
<"[...] no período de 01 de novembro de 2018 a 01 de novembro de 2019, [...]">,
leia-se:
<"[...] no período de 01 de novembro de 2018 a 01 de abril de 2019, [...]">.
RUI VICENTE OPPERMANN
Reitor
