Documento gerado sob autenticação Nº QNZ.183.950.1SR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10840                  de  05/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°89220,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA, do Quadro de Pessoal desta Universidade, CRISTTOFER DE MOURA SANTOS (Siape: 1094640 ),  para
substituir   VINICIUS BERNARDI LEAL (Siape: 3068541 ), Secretário do Depto de Horticultura e Silvicultura da
Faculdade de Agronomia, Código FG-7, em seu afastamento no país, no período de 05/12/2019 a 06/12/2019,
com o decorrente pagamento das vantagens por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
