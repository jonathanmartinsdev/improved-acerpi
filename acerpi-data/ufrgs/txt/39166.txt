Documento gerado sob autenticação Nº GTZ.766.991.6N2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6188                  de  12/07/2017
 
           O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições,
RESOLVE
Designar PAULO DORNELLES PICON, matrícula SIAPE n° 0356802, ocupante do cargo de Professor
do  Magistério  Superior,  classe  Titular,  lotado  no  Departamento  de  Medicina  Interna  da  Faculdade  de
Medicina, do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Chefe do Depto de
Medicina Interna da Faculdade de Medicina, Código SRH 97, Código FG-1, com vigência a partir da data de
publicação no Diário Oficial da União e até 02/12/2017, a fim de completar o mandato de LUCIANO ZUBARAN
GOLDANI, conforme artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.010651/2017-66.
                                              
                                     CELSO GIANNETTI LOUREIRO CHAVES
                              Decano do Conselho Universitário, no Exercício da Reitoria
