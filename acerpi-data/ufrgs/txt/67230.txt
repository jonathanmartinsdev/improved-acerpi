Documento gerado sob autenticação Nº MVW.533.288.J7C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5697                  de  31/07/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLAUDIA PIANTA COSTA CABRAL,  Professor do Magistério
Superior, lotada no Departamento de Arquitetura da Faculdade de Arquitetura e com exercício no Programa
de Pós-Graduação em Arquitetura,  com a  finalidade de  participar  da  "15th  International  DOCOMOMO
Conference", em Ljubljana, Eslovênia, no período compreendido entre 27/08/2018 e 01/09/2018, incluído
trânsito, com ônus CNPq (Bolsa de Produtividade em Pesquisa). Solicitação nº 47907.
RUI VICENTE OPPERMANN
Reitor
