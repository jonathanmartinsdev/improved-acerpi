Documento gerado sob autenticação Nº ZIT.120.179.B02, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10534                  de  17/11/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, 
RESOLVE
Tornar insubsistente a Portaria nº 10419/2017, de 13/11/2017, que designa Chefe da Biblioteca de
Agronomia  ELISANGELA DA SILVA RODRIGUES, ocupante do cargo de Bibliotecário-documentalista, lotada na
Faculdade de Agronomia e com exercício na Biblioteca da Faculdade de Agronomia.
RUI VICENTE OPPERMANN
Reitor
