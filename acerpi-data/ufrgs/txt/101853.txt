Documento gerado sob autenticação Nº VLV.684.905.BJV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10912                  de  06/12/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  FERNANDO  SALDANHA  THOME,  matrícula  SIAPE  n°  0357440,  lotado  no
Departamento de Medicina Interna da Faculdade de Medicina, como Chefe Substituto do Depto de Medicina
Interna  da  Faculdade  de  Medicina,  para  substituir  o  titular  desta  função  em  seus  afastamentos  ou
impedimentos  regulamentares  na  vigência  do  presente  mandato,  por  ter  sido  reeleito.  Processo
nº 23078.532159/2019-17.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
