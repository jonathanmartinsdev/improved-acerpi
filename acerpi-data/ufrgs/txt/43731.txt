Documento gerado sob autenticação Nº HCY.297.800.8GP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8115                  de  29/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  29/08/2017,   referente  ao  interstício  de
29/02/2016 a 28/08/2017, para a servidora ALINE OZELAME DE LIMA, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2287680,  lotada  no  Instituto de Artes, passando do Nível de
Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.512791/2017-74:
ENAP - Atendimento ao Cidadão CH: 20 (16/08/2016 a 05/09/2016)
ENAP - Ética e Serviço Público CH: 20 (16/08/2016 a 05/09/2016)
ENAP - Controle Social CH: 20 (27/09/2016 a 17/10/2016)
ENAP - Gestão de Contratos de Tecnologia da Informação (GCTI) CH: 30 (11/04/2017 a 08/05/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
