Documento gerado sob autenticação Nº PBX.980.281.9VB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6531                  de  22/08/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar na Faculdade de Agronomia, com exercício no Programa de Pós-Graduação em Zootecnia,
ANDRISA RODRIGUES DA SILVEIRA,  nomeada conforme Portaria Nº 5022/2018 de 12 de julho de 2018,
publicada no Diário Oficial da União no dia 13 julho de 2018, em efetivo exercício desde 15 de agosto de
2018,  ocupante do cargo de ASSISTENTE EM ADMINISTRAÇÃO,  Ambiente Organizacional  Administrativo,
classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
