Documento gerado sob autenticação Nº MBU.251.484.OUN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4971                  de  11/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria n° 4695/2018, de 02/07/2018, que concede Incentivo à Qualificação a TAMINI
FARIAS NICOLETTI,  Bibliotecário-documentalista,  com exercício na Biblioteca do Instituto de Biociências,
conforme Processo nº 23078.515374/2018-64.
 
Onde se lê:
"ALTERAR o percentual de Incentivo à Qualificação",
leia-se:
"CONCEDER o percentual de Incentivo à Qualificação".
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
