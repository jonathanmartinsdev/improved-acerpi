Documento gerado sob autenticação Nº YJS.773.111.TM8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4381                  de  21/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder  promoção  funcional,  por  obtenção  do  título  de  Pós-Doutor,  no  Quadro  desta
Universidade, ao Professor IVES CAVALCANTE PASSOS, matrícula SIAPE 1839019, lotado e em exercício no
Departamento de Psiquiatria e Medicina Legal da Faculdade de Medicina, da classe A  de Professor Adjunto A,
nível 01, para a classe C  de Professor Adjunto, nível 01, com vigência financeira a partir de 21/05/2019, de
acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554,
de  20  de  junho  de  2013  do  Ministério  da  Educação  e  a  Decisão  nº  331/2017.  Processo  nº
23078.513083/2019-12.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
