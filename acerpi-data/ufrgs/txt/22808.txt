Documento gerado sob autenticação Nº ETA.659.228.PHC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3994                  de  03/06/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SONIA MARIA RANINCHESKI, Professor do Magistério Superior,
lotada e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de Ciências
Econômicas, com a finalidade de participar de reunião junto à Lomonosov Moscou State University, em
Moscou,  Rússia,  no período compreendido entre 11/06/2016 e 21/06/2016,  incluído trânsito,  com ônus
limitado. Solicitação nº 20042.
CARLOS ALEXANDRE NETTO
Reitor
