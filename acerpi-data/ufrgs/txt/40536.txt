Documento gerado sob autenticação Nº MXF.978.793.I3V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5930                  de  06/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  04/07/2017,   referente  ao  interstício  de
30/12/2015  a  03/07/2017,  para  o  servidor  AUGUSTO  HERTZOG,  ocupante  do  cargo  de  Técnico  de
Tecnologia da Informação - 701226, matrícula SIAPE 2270644,  lotado  no  Centro de Processamento de
Dados, passando do Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de
Capacitação  D  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.012807/2017-43:
PROREXT - Inglês para TI - 2016/2 CH: 30 Carga horária utilizada: 10 hora(s) / Carga horária excedente: 20
hora(s) (31/08/2016 a 14/12/2016)
RNP - Teste de Invasão de Aplicações Web CH: 40 (26/09/2016 a 30/09/2016)
Gestão de Segurança da Informação - NRB 27001 e NRB 27002 CH: 40 (13/06/2016 a 17/06/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
