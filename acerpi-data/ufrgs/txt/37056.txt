Documento gerado sob autenticação Nº VKS.908.425.9JT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3309                  de  19/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 90 (noventa) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de
11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
GRACIELLE  PESAMOSCA  DUARTE,  com  exercício  na  Seção  de  Análise  Contábil  da  Pró-Reitoria  de
Planejamento  e  Administração,  a  ser  usufruída  no  período  de  24/04/2017  a  22/07/2017,  referente  ao
quinquênio de 19/08/2008 a 18/08/2013, a fim de participar do curso "Programa Administração Pública -
Contabilidade - Completo" no Centro de Ensino Tecnológico de Brasília - FUBRAE, conforme Processo nº
23078.006455/2017-97.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
