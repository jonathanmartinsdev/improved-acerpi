Documento gerado sob autenticação Nº DMV.769.202.1S1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8044                  de  04/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor DANIEL CORREA SELAO,
ocupante do cargo de Assistente em Administração-701200, lotado na Pró-Reitoria de Assuntos Estudantis,
SIAPE 3075796, para 30% (trinta por cento), a contar de 27/02/2019, tendo em vista a conclusão do curso de
Especialização em Gestão Empresarial, conforme o Processo nº 23078.504589/2019-31.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
