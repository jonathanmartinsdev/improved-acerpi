Documento gerado sob autenticação Nº QON.494.801.F13, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7979                  de  05/10/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder a Retribuição por Titulação-RT, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro de 2012, com suas alterações, por obtenção do título de Mestrado + RSC III, no Quadro desta
Universidade, à Professora  MAYARA COSTA DA SILVA,  matrícula SIAPE 1248299, ocupante de cargo de
carreira do Magistério do Ensino Básico, Técnico e Tecnológico, regime de dedicação exclusiva, classe D, nível
01, lotada e em exercício no Departamento de Ciências Exatas e da Natureza do Colégio de Aplicação, com
vigência financeira a partir 06 de julho de 2018, conforme processo 23078.512686/2018-16.
RUI VICENTE OPPERMANN
Reitor.
