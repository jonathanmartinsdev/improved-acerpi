Documento gerado sob autenticação Nº SJJ.694.582.SA0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7284                  de  14/09/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Declarar vaga, a partir de 13/09/2016, a função de Secretária do Depto de Plantas Forrageiras e
Agrometeorologia da Faculdade de Agronomia, Código SRH 631, Código FG-7, desta Universidade, tendo em
vista a aposentadoria de MARILDA SANTOS DA ROCHA, matrícula SIAPE n° 1044125, conforme Portaria n°
7095/2016, de 9 de setembro de 2016, publicada no Diário Oficial da União do dia 13/09/2016. Processo nº
23078.020314/2016-04.
RUI VICENTE OPPERMANN
Vice-Reitor
