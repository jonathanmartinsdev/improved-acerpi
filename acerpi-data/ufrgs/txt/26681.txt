Documento gerado sob autenticação Nº LIG.096.353.4BE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6488                  de  25/08/2016
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de
2013
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  22/08/2016,   referente  ao  interstício  de
11/02/2015 a 21/08/2016, para a servidora THAIS DEL RIO DELLA GIUSTINA, ocupante do cargo de Técnico
em Assuntos Educacionais - 701079, matrícula SIAPE 2075548,  lotada  no  Instituto de Ciências Básicas da
Saúde, passando do Nível de Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de
Capacitação  E  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.018752/2016-02:
Formação Integral de Servidores da UFRGS V CH: 145 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 25 hora(s) (16/03/2015 a 26/04/2016)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
