Documento gerado sob autenticação Nº QGB.952.711.4L7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7005                  de  06/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  29/08/2016,   referente  ao  interstício  de
28/02/2015 a 28/08/2016, para a servidora DANIELA CAON GUERRA, ocupante do cargo de Analista de
Tecnologia da Informação - 701062, matrícula SIAPE 2054012,  lotada  na  Faculdade de Medicina, passando
do Nível de Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.019278/2016-28:
Formação Integral de Servidores da UFRGS III CH: 105 (04/12/2013 a 08/10/2015)
RNP - Gestão da Segurança da Informação - NBR 27001 e NBR 27002 CH: 40 (25/05/2015 a 29/05/2015)
ENAP - eMAG Conteudista CH: 20 Carga horária utilizada: 5 hora(s) / Carga horária excedente: 15 hora(s)
(20/10/2015 a 09/11/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
