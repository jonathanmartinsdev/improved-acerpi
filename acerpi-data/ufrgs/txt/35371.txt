Documento gerado sob autenticação Nº FPQ.116.354.3PV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2182                  de  10/03/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº 23078.000856/2017-33
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de Sociólogo, do Quadro de Pessoal
desta  Universidade,  KAREN  BRUCK  DE  FREITAS  (Siape:  0356964),   para  substituir  EDILSON  AMARAL
NABARRO (Siape: 0357080), Diretor do Depto dos Programas de Acesso e Permanência da CAF, Código SRH
1277, CD-4, em seu sua Licença para Capacitação, no período de 19/03/2017 a 05/04/2017, com o decorrente
pagamento das vantagens por 18 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
