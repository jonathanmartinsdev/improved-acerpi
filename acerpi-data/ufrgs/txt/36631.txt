Documento gerado sob autenticação Nº FKI.161.400.OMJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             3126                  de  10/04/2017
Nomeia  representantes  discentes
no Curso de Agronomia.
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita no Curso de Agronomia, com mandato de 01 (um) ano, a
contar  de 22 de dezembro de 2016 ,  atendendo ao disposto nos artigos 175 do Regimento Geral  da
Universidade  e  79  do  Estatuto  da  Universidade  e  considerando  o  processo  nº  23078.204261/2016-74,
conforme segue:
DEPARTAMENTO DE FITOSSANIDADE
Titular Francis Zanini
Suplente Gabriela Holanda Nichel
DEPARTAMENTO DE HORTICULTURA E SILVICULTURA
Titular Rubiane da Campo Rubbo
Suplente Lucas Medeiros Silva
DEPARTAMENTO DE PLANTAS DE LAVOURA
Titular Bruno Picetti Chiesa
Suplente Micael Glasenapp
DEPARTAMENTO DE PLANTAS FORRAGEIRAS E AGROMETEOROLOGIA
Titular Ana Carla Didoné
Suplente Leonardo Duarte Félix
DEPARTAMENTO DE SOLOS
Titular Leonardo Pereira Fortes
Suplente Dionata Filippi
DEPARTAMENTO DE ZOOTECNIA
Titular Manuela Leal Wolf
Suplente Oscar Luis Rigon
ESTAÇÃO EXPERIMENTAL AGRONÔMICA
Titular Ismael França
Suplente Henrique dos Santos Dalanhol
COMISSÃO DE GRADUAÇÃO DO CURSO DE AGRONOMIA
Titular Dionata Filippi
Documento gerado sob autenticação Nº FKI.161.400.OMJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Suplente Rubiane da Campo Rubbo
COMISSÃO DE EXTENSÃO DA FACULDADE DE AGRONOMIA
Titular Gabriela de Holanda Nichel
Suplente Dionata Filippi
CONSELHO DA UNIDADE
Titular Gabriela de Holanda Nichel
Suplente Leonardo Pereira Fortes
Titular Dionata Filippi
Suplente Rubiane da Campo Rubbo
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
