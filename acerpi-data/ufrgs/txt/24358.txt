Documento gerado sob autenticação Nº ECC.019.360.23A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5091                  de  12/07/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  DIRCE POZEBON,  matrícula  SIAPE n°  1318307,  lotada e  em exercício  no Departamento de
Química Inorgânica do Instituto de Química, da classe D  de Professor Associado, nível 04, para a classe E  de
Professor Titular,  referente ao interstício de 08/05/2014 a 27/06/2016, com vigência financeira a partir de
08/07/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012 e Decisão nº 232/2014 -
CONSUN. Processo nº 23078.201740/2016-39.
RUI VICENTE OPPERMANN
Vice-Reitor
