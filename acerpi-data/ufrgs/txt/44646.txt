Documento gerado sob autenticação Nº TYA.083.177.89V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8689                  de  15/09/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a pedido, a partir de 30/08/2017, o ocupante do cargo de Analista de Tecnologia da
Informação - 701062, do Nível de Classificação EII,  do Quadro de Pessoal desta Universidade, ACELINO
GEHLEN DA SILVA, matrícula SIAPE 2034818 da função de Diretor do Departamento de Gestão Integrada da
PROPLAN, Código SRH 1447, Código XX, para a qual foi designado pela Portaria nº 0036/15 de 06/01/2015,
publicada no Diário Oficial da União de 19/01/2015. Processo nº 23078.514503/2017-16.
JANE FRAGA TUTIKIAN
Vice-Reitora.
