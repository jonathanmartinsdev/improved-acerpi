Documento gerado sob autenticação Nº KPH.234.849.5BK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             482                  de  16/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Homologar o parecer que aprova o servidor técnico-administrativo VLADIMIR LUCIANO PINTO,
ocupante  do  cargo  de  Bibliotecário-documentalista,  no  estágio  probatório  cumprido  no  período  de
07/01/2015 até 07/01/2018, fazendo jus, a partir desta data, à estabilidade no serviço público federal.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
