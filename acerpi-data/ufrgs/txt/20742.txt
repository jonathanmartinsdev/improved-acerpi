Documento gerado sob autenticação Nº AIL.098.999.OM7, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2867                  de  18/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a IVONE JOB, matrícula SIAPE
nº 0352884, no cargo de Bibliotecário-documentalista, nível de classificação E, nível de capacitação IV, padrão
16, do Quadro desta Universidade, no regime de quarenta horas semanais de trabalho, com exercício na
Biblioteca da Escola de Educação Física da Escola de Educação Física, Fisioterapia e Dança, com proventos
integrais  e incorporando a vantagem pessoal  de que trata a Lei  nº  9.624,  de 2 de abril  de 1998,  que
assegurou o disposto no artigo 3º da Lei nº 8.911, de 11 de julho de 1994. Processo 23078.006768/2016-64.
CARLOS ALEXANDRE NETTO
Reitor
