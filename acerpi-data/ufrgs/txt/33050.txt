Documento gerado sob autenticação Nº OOC.610.012.E65, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             491                  de  17/01/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°34747,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PORTEIRO, do Quadro de
Pessoal desta Universidade, JOAO CARLOS FELIX (Siape: 0356131 ),   para substituir   PAULO DA SILVA
ECKARD (Siape: 0354751 ), Chefe do Setor de Manutenção e Segurança do Prédio do CPD, Código FG-5, em
seu afastamento por motivo de férias, no período de 17/01/2017 a 31/01/2017, com o decorrente pagamento
das vantagens por 15 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
