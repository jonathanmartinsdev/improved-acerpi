Documento gerado sob autenticação Nº HKB.248.826.VJK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3192                  de  11/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ANA ELÍSIA DA COSTA, Professor do Magistério Superior, lotada
e em exercício no Departamento de Arquitetura da Faculdade de Arquitetura, com a finalidade de realizar
estudos em nível  de Pós-Doutorado junto ao Instituto Universitário de Lisboa,  em Lisboa,  Portugal,  no
período compreendido entre 01/08/2019 e 31/07/2020, com ônus limitado. Processo nº 23078.533681/2018-
27.
RUI VICENTE OPPERMANN
Reitor.
