Documento gerado sob autenticação Nº IRZ.459.796.6EP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             798                  de  29/01/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, ao
Professor JULIO CESAR BITTENCOURT FRANCISCO,  matrícula SIAPE 1871028,  lotado e em exercício no
Departamento de Ciência da Informação da Faculdade de Biblioteconomia e Comunicação, da classe B  de
Professor Assistente, nível 02, para a classe C  de Professor Adjunto, nível 01, com vigência financeira a partir
da data de publicação da portaria, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações, Portaria nº 554, de 20 de junho de 2013 do Ministério da Educação e a Decisão nº
401/2013 - CONSUN. Processo nº 23078.501310/2018-86.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
