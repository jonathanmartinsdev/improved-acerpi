Documento gerado sob autenticação Nº TZZ.781.791.QTD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4688                  de  30/06/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar MATHEUS BITENCOURT DA COSTA,  Matrícula SIAPE 1964887,  ocupante do cargo de
Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a
função de Diretor da Divisão de Logística de Bens e Serviços do Núcleo de Infraestrutura da Diretoria
Administrativa do Campus Litoral Norte, código SRH 1453, código FG-3, com vigência a partir da data de
publicação no Diário Oficial da União. Processo nº 23078.515677/2018-87.
JANE FRAGA TUTIKIAN
Vice-Reitora.
