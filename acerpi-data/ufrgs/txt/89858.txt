Documento gerado sob autenticação Nº XBK.561.419.7SA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3306                  de  16/04/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  KARINA  SANTOS  MUNDSTOCK,  matrícula  SIAPE  n°  2496514,  lotada  e  em  exercício  no
Departamento de Cirurgia e Ortopedia da Faculdade de Odontologia, da classe C  de Professor Adjunto, nível
04, para a classe D  de Professor Associado, nível 01, referente ao interstício de 13/02/2015 a 12/02/2017,
com vigência financeira a partir de 05/02/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro
de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.501543/2019-60.
JANE FRAGA TUTIKIAN
Vice-Reitora.
