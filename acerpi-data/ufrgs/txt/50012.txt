Documento gerado sob autenticação Nº YDM.679.999.QOU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             811                  de  29/01/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/12/2017,   referente  ao  interstício  de
01/10/2012 a 25/12/2017, para a servidora VIVIANE MARIA DOS SANTOS SOARES, ocupante do cargo de
Assistente em Administração -  701200,  matrícula  SIAPE 1860185,   lotada  no  Colégio  de Aplicação,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.524546/2017-18:
Formação Integral de Servidores da UFRGS II CH: 51 (27/03/2012 a 03/04/2017)
ENAP - Regras e Fundamentos do Sistema de Concessão de Diárias e Passagens (SCPD) CH: 30 (18/04/2017 a
15/05/2017)
MICHIGAN - INGLÊS BÁSICO CH: 34 (19/08/2016 a 18/11/2016)
MICHIGAN -  INGLÊS C2 CH: 34 Carga horária utilizada:  5 hora(s)  /  Carga horária excedente:  29 hora(s)
(18/03/2017 a 15/07/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
