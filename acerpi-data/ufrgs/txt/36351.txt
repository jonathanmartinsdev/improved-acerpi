Documento gerado sob autenticação Nº PBA.535.949.F7D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2832                  de  31/03/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°26921,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do Quadro de Pessoal  desta Universidade,  INGRID ELEONORA SCHREIBER JANSCH PORTO
(Siape: 0352778 ),  para substituir   MARCELO SOARES LUBASZEWSKI (Siape: 0357201 ), Diretor do Parque
Científico e Tecnológico da UFRGS, Código CD-4, em seu afastamento no país, no dia 30/03/2017, com o
decorrente pagamento das vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
