Documento gerado sob autenticação Nº TFJ.639.138.6N6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11415                  de  22/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°47145,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PEDAGOGO-ÁREA, do Quadro de
Pessoal desta Universidade, ANA LUCIA BORGES ANDRADE (Siape: 2144533 ),  para substituir   NADIA DE
FATIMA BORBA MARTINS (Siape: 1200644 ), Coordenador do Núcleo Acadêmico do Campus Litoral Norte,
Código FG-1, em seu afastamento por motivo de Laudo Médico do titular da Função, no dia 16/11/2017 , com
o decorrente pagamento das vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
