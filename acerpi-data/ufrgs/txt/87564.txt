Documento gerado sob autenticação Nº HMV.550.511.4V8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2013                  de  28/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, REINER VINICIUS PEROZZO, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 18/2019 de 15 de Fevereiro de 2019, homologado em 18 de
fevereiro de 2019 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei
12.772,  de 28 de dezembro de 2012,  com redação dada pela Lei  12.863,  de 24 de setembro de 2013,
publicada  no  Diário  Oficial  da  União  de  25  de  setembro  de  2013,  para  o  cargo  de  PROFESSOR  DO
MAGISTÉRIO SUPERIOR do Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor
Adjunto A, Nível I,  do Quadro de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação
Exclusiva),  junto ao Departamento de Línguas Modernas do Instituto de Letras,  em vaga decorrente da
aposentadoria  da  Professora  Rosalia  Angelita  Neumann Garcia,  código  nº  719118,  ocorrida  em 27  de
Novembro de 2017, conforme Portaria nº. 10752/2017 de 24 de novembro de 2017, publicada no Diário
Oficial da União de 27 de novembro de 2017.  Processo nº. 23078.515778/2018-58.
RUI VICENTE OPPERMANN
Reitor.
