Documento gerado sob autenticação Nº EEB.671.677.7L6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6357                  de  17/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de SERGIO BAMPI, Professor do Magistério Superior, lotado e em
exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de  Informática,  com  a  finalidade  de
participar  da "Summer School  2017:  Verification Technology,  Systems & Applications",  em Saarbrucken,
Alemanha, no período compreendido entre 29/07/2017 e 04/08/2017, incluído trânsito, com ônus FINEP.
Solicitação nº 29623.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
