Documento gerado sob autenticação Nº XHS.996.840.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8327                  de  12/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARIO REIS ALVARES DA SILVA,  Professor do Magistério
Superior,  lotado no Departamento de Medicina Interna da Faculdade de Medicina e  com exercício  no
Programa de Pós-Graduação Ciências em Gastroenterologia e Hepatologia, com a finalidade de participar do
"World Congress of Gastroenterology (WCOG 2019)", em Istambul, Turquia, no período compreendido entre
23/09/2019 e 26/09/2019, incluído trânsito, com ônus limitado. Solicitação nº 83118.
RUI VICENTE OPPERMANN
Reitor
