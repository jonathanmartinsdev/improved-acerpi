Documento gerado sob autenticação Nº PRY.805.274.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9110                  de  08/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir de 10/10/2019, como segue:
 
Transformar: Assessor Administrativo da Divisão de Moradia Estudantil do DIE, Código FG-3, Código
SRH 304, em Chefe do Núcleo Financeiro da Gerência Administrativa da PRAE, Código FG-3, Código SRH 304.
 
 
Processo nº 23078.526577/2019-67.
 
RUI VICENTE OPPERMANN
Reitor.
