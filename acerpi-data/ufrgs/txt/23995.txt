Documento gerado sob autenticação Nº XTF.817.897.OO5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4788                  de  29/06/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias, de acordo com o processo nº 23078. 006872/2016-59, considerando o disposto na Lei nº 8.112,
de 11/12/1990, no Decreto Presidencial nº 1.590, de 10/08/1995, no Decreto nº 4.836, de 09/09/2003, na
Decisão  nº  432  do  Conselho  Universitário,  de  27/11/2015  e  na  Portaria  1.479  de  29/02/2016  desta
Universidade
RESOLVE
Art.1º Autorizar a jornada de trabalho flexibilizada na Biblioteca da Faculdade de Biblioteconomia e
Comunicação, em período diário de atendimento ao público das oito  às vinte e uma horas, pelo prazo de
doze  meses,  conforme  disposto  no  Art.  10,  da  Portaria  1.479  de  29/02/2016.  Os  servidores  técnico-
administrativos abaixo relacionados terão jornada de trabalho de seis horas diárias, cumprindo carga horária
de trinta horas semanais:
CRISTINA GIBROWSKI - SIAPE: 1446935
INES MARIA DE GASPERIN - SIAPE: 0356107
JOSIANE GONÇALVES DA COSTA - SIAPE: 1446919
LIGIA MARIA ROCKENBACH - SIAPE: 0358709
LUIS FERNANDO DA SILVA VELOSO - SIAPE: 0357250
ROBERTO ROSA DOS SANTOS - SIAPE: 0357891
Art. 2º Determinar a afixação, nas suas dependências, em local visível e de grande circulação de
usuários dos serviços, bem como no site da Unidade/Órgão, de quadro, permanentemente atualizado, com
escala  nominal  dos  servidores  arrolados no Art.  1º  desta  Portaria,  constando dias  e  horários  de seus
expedientes. Processo nº 23078.006872/2016-59.
CARLOS ALEXANDRE NETTO
Reitor
