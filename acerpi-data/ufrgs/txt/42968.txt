Documento gerado sob autenticação Nº VEP.093.204.B7G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7614                  de  15/08/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°37565,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, EDUARDO AUGUSTO REMOR (Siape: 2139817 ),  para
substituir   LUCIANE DE CONTI (Siape: 1552574 ), Coordenador da Comissão de Extensão do Instituto de
Psicologia, em seu afastamento por motivo de férias, no período de 08/08/2017 a 25/08/2017.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
