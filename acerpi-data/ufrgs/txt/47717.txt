Documento gerado sob autenticação Nº FZN.221.392.97N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10889                  de  01/12/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar a ocupante do cargo de Bibliotecário-documentalista, classe E, do Quadro de Pessoal desta
Universidade, VIVIANE CARRION CASTANHO, matrícula SIAPE n° 1092523, lotada na Faculdade de Medicina,
como Chefe Substituta da Biblioteca de Medicina, para substituir automaticamente o titular desta função em
seus afastamentos ou impedimentos regulamentares no período de 12/12/2017 até 11/12/2019. Processo nº
23078.521312/2017-19.
RUI VICENTE OPPERMANN
Reitor.
