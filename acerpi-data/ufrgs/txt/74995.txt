Documento gerado sob autenticação Nº KFK.765.785.7G6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             418                  de  11/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar na Faculdade de Direito, com exercício no Setor de Graduação da Gerência Administrativa da
Faculdade de Direito, Ambiente Organizacional Administrativo, LUIS PEDRO SILVÉRIO, nomeado conforme
Portaria Nº 9359/2018 de 20 de novembro de 2018, publicada no Diário Oficial  da União no dia 23 de
novembro de 2018, em efetivo exercício desde 10 de janeiro de 2019, ocupante do cargo de ASSISTENTE EM
ADMINISTRAÇÃO, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
