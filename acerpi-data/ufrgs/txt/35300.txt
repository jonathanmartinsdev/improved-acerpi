Documento gerado sob autenticação Nº RAL.986.184.12S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2108                  de  09/03/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº 23078.200296/2017-15
RESOLVE
Retificar a Portaria nº 1693, de 21/02/2017, que designou, temporariamente,  FABIANO PORTO
ROSA (Siape: 0358091) para substituir  ILGA SCHAUREN (Siape: 0352756 ),
 
onde se lê:
"... em seu afastamento por motivo de férias, no período de 06/02/2017 a 24/02/2017, com o
decorrente pagamento das vantagens por 19 dias."
 
          leia-se:
          "... em seu afastamento por motivo de férias, no dia 06/02/2017, com o decorrente pagamento
das vantagens por 01 dia." Ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
