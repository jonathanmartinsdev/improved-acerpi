Documento gerado sob autenticação Nº JCR.136.901.I2K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10197                  de  17/12/2018
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
Prorrogar por mais 30 (trinta) dias o prazo estabelecido pela Portaria nº 9349, de 19 de setembro
de 2018, a fim de adequar termo de referência para prestação de serviços de hidrojateamento de alta
pressão e sucção por alto vácuo em redes coletoras de esgoto, caixas de gordura e fossas sépticas, conforme
processo 23078.532477/2018-99.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
