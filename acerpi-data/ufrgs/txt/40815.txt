Documento gerado sob autenticação Nº ZMI.108.259.ABL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6101                  de  11/07/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  MARCO AURELIO PIRES IDIART,  Professor  do Magistério
Superior,  lotado e em exercício no Departamento de Física do Instituto de Física,  com a finalidade de
participar de reunião na  Aix-Marseille Université, em Marseille, França, no período compreendido entre
15/07/2017 e 22/07/2017, incluído trânsito, com ônus limitado. Solicitação nº 29541.
RUI VICENTE OPPERMANN
Reitor
