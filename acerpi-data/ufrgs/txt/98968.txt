Documento gerado sob autenticação Nº QEA.778.645.04T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8931                  de  03/10/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, e conforme a Solicitação de Afastamento n°87551,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, VALTER ROESLER (Siape: 1550432 ),  para substituir  
LEANDRO  KRUG  WIVES  (Siape:  1531202  ),  Diretor  do  Centro  Interdisciplinar  de  Novas  Tecnologias  da
Educação - CINTED, Código FG-1, em seu afastamento no país, no período de 04/10/2019 a 05/10/2019, com
o decorrente pagamento das vantagens por 2 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
