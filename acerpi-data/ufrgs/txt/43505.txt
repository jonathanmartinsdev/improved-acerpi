Documento gerado sob autenticação Nº NCO.319.853.6N4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7981                  de  25/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora VERONICA MACHADO
ROLIM, ocupante do cargo de Médico Veterinário-701048, lotada na Estação Experimental Agronômica, SIAPE
2326322, para 75% (setenta e cinco por cento), a contar de 08/06/2017, tendo em vista a conclusão do curso
de Doutorado em Ciências Veterinárias, conforme o Processo nº 23078.011325/2017-76.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
