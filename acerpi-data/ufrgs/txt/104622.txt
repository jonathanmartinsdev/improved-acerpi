Documento gerado sob autenticação Nº IAG.843.969.V24, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1082                  de  31/01/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°54739,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de NUTRICIONISTA-HABILITAÇÃO,
do Quadro de Pessoal desta Universidade, MARINA CARVALHO BERBIGIER (Siape: 1838681 ),  para substituir  
SABRINA  MELO  EVANGELISTA  (Siape:  2057696  ),  Coordenador  do  Núcleo  Técnico  Científico  da  GA  da
Faculdade de Medicina, Código FG-7, em seu afastamento por motivo de férias, no período de 03/02/2020 a
07/02/2020, com o decorrente pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
