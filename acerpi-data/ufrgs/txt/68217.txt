Documento gerado sob autenticação Nº BDC.582.054.F9H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6283                  de  15/08/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°41358,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS, do Quadro de Pessoal desta Universidade, FELIPE ANTÔNIO GUIDI SAUERESSIG (Siape:
1963089 ),  para substituir   JOSE EDUARDO HANDEL (Siape: 0357511 ), Secretário da COMGRAD de Nutrição,
Código FG-7, em seu afastamento por motivo de férias, no período de 17/08/2018 a 05/09/2018, com o
decorrente pagamento das vantagens por 20 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
