Documento gerado sob autenticação Nº IER.431.975.U2M, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             629                  de  22/01/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  DENISE  CARPENA COITINHO DAL MOLIN,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Engenharia Civil da Escola de Engenharia,
com a finalidade de ministrar disciplina junto à Universidad Nacional de Asunción, em Assunção, Paraguai, no
período compreendido entre 29/01/2018 e 03/02/2018, incluído trânsito, com ônus limitado. Solicitação nº
33764.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
