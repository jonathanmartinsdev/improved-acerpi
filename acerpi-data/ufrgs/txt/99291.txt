Documento gerado sob autenticação Nº UDU.804.930.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9170                  de  10/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de DAVIDE CARBONAI, Professor do Magistério Superior, lotado e
em exercício no Departamento de Ciências Administrativas da Escola de Administração, com a finalidade de
proferir  palestra  no "Seminario  Sindicalismo en perspectiva  comparada"  junto ao Consejo  Nacional  de
Investigaciones  Científicas  y  Técnicas  e  participar  de  reunião,  em Buenos  Aires,  Argentina,  no  período
compreendido entre 15/10/2019 e 18/10/2019, incluído trânsito, sendo de 15/10/2019 a 16/10/2019 com
ônus CAPES/PROAP, e de 17/10/2019 a 18/10/2019, com ônus limitado. Solicitação nº 87316.
RUI VICENTE OPPERMANN
Reitor
