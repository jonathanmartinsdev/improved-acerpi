Documento gerado sob autenticação Nº UVO.176.616.436, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             873                  de  25/01/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CLARICE SALETE TRAVERSINI, Professor do Magistério Superior,
lotada e em exercício no Departamento de Ensino e Currículo da Faculdade de Educação, com a finalidade de
realizar visita à Universidade do Minho, em Braga, Portugal,  à Universidad de Barcelona, em Barcelona,
Espanha e  à  Universidad Autónoma de Madrid,  em Madrid,  Espanha,  no período compreendido entre
25/02/2019 e 29/03/2019, incluído trânsito, com ônus UFRGS (Programa de Pós-Graduação em Educação).
Solicitação nº 61877.
RUI VICENTE OPPERMANN
Reitor
