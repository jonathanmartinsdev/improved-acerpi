Documento gerado sob autenticação Nº ABW.314.090.EBB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5407                  de  20/07/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, 
RESOLVE
Designar ALESSANDRA VIRGINIA DE OLIVEIRA, Matrícula SIAPE 1756196, ocupante do cargo de
Bibliotecário-documentalista,  Código 701010,  do Quadro de Pessoal  desta Universidade,  para exercer  a
função  de  Chefe  da  Biblioteca  de  Educação,  código  SRH  496,  código  FG-5,  com  vigência  a  partir  de
22/07/2018. Processo nº 23078.518130/2018-33.
JANE FRAGA TUTIKIAN
Vice-Reitora.
