Documento gerado sob autenticação Nº YQI.604.027.5KJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7124                  de  10/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°51310,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS, do Quadro de Pessoal desta Universidade, FERNANDA BRASIL MENDES (Siape: 1630339 ),
 para substituir   CLAUDETE LAMPERT GRUGINSKIE (Siape: 1756939 ), Coordenador do Núcleo de Regulação
da Secretaria de Avaliação Institucional, Código FG-2, em seu afastamento por motivo de Laudo Médico do
titular da Função, no período de 29/08/2018 a 12/09/2018, com o decorrente pagamento das vantagens por
15 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
