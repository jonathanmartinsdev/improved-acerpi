Documento gerado sob autenticação Nº ECC.864.450.7JI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10174                  de  11/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CARLOS TORRES FORMOSO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia Civil da Escola de Engenharia, com a finalidade de
realizar visita à Yonsei University, à University of Seoul e participar da Zero Energy Mass Custom Home 2019
International Conference, em Seul, Coréia Do Sul, no período compreendido entre 22/11/2019 e 01/12/2019,
incluído trânsito, com ônus CAPES/PRINT/UFRGS. Solicitação nº 88058.
RUI VICENTE OPPERMANN
Reitor
