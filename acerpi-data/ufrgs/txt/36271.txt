Documento gerado sob autenticação Nº JPF.016.992.P88, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2799                  de  30/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor JOSE CICERO MORAES, matrícula SIAPE n° 0357593, lotado e em exercício no Departamento de
Educação Física da Escola de Educação Física, Fisioterapia e Dança, da classe D  de Professor Associado, nível
03, para a classe D  de Professor Associado, nível 04, referente ao interstício de 31/12/2014 a 30/12/2016,
com vigência financeira a partir de 31/12/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  197/2006-CONSUN,  alterada  pela  Decisão  nº
401/2013-CONSUN. Processo nº 23078.516477/2016-80.
JANE FRAGA TUTIKIAN
Vice-Reitora.
