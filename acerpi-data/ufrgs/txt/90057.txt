Documento gerado sob autenticação Nº WXI.100.370.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3440                  de  22/04/2019
  D e l e g a ç ã o  d e  c o m p e t ê n c i a  a o
Superintendente de Infraestrutura.
              O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, tendo
em vista o disposto nos artigos 11 e 12 do Decreto-Lei n.º 200, de 25 de fevereiro de 1967, regulamentado
pelo Decreto n.º 62.460, de 25 de março de 1968,
 
RESOLVE:
         Art. 1º Delegar competência ao Engenheiro EDY ISAÍAS JÚNIOR, Superintendente de Infraestrutura,
para, a partir de 22 de abril de 2019, sem prejuízo das atribuições que, regimentalmente, competem ao
titular do mesmo cargo, praticar os atos administrativos a seguir enumerados:
 
I - ordenar despesas, exceto as de  Pessoal,   pertinentes   a   Superintendência de Infraestrutura;
II - designar comissões, da Superintendência de Infraestrutura;
III - autorizar  a   execução  de  obras  de   conservação,    reformas,   adaptações  e  ampliação da
planta física, inclusive as intervenções relacionadas aos prédios históricos e educação patrimonial;
IV  -  coordenar  e  autorizar  todas  as  ações  relacionadas  à  proteção  do  meio  ambiente  e
sustentabilidade, com exceção das atividades de educação ambiental;
V - autorizar afastamentos no País, a serviço, por período não superior  a  dez  (10) dias, no  âmbito
da Superintendência  de    Infraestrutura,   e   autorizar   a concessão de diárias.
           
              Art. 2º A autoridade a que se refere o Artigo 1º da presente Portaria, considerando a necessidade do
serviço, poderá subdelegar os poderes, que por este ato lhe são delegados.
 
              Art. 3º Revogar a Portaria  2751 de 28 de março de 2019 e demais disposições em contrário.
 
RUI VICENTE OPPERMANN,
Reitor.
