Documento gerado sob autenticação Nº ATW.484.418.I0L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7257                  de  13/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor ARTHUR CAVADA DE CAMPOS VELHO, ocupante do cargo de  Operador de
Estação de Tratamento Água-esgoto - 701449, lotado no Instituto de Pesquisas Hidráulicas, SIAPE 2412853, o
percentual de 25% (vinte e cinco por cento) de Incentivo à Qualificação, a contar de 06/08/2018, tendo em
vista a conclusão do curso de Química, conforme o Processo nº 23078.519950/2018-42.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
