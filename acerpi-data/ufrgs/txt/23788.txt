Documento gerado sob autenticação Nº DZR.663.736.TFV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4611                  de  23/06/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 5263, de 01 de outubro de
2012, e conforme a Solicitação de Afastamento n°20990,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  MANUEL MENEZES  DE  OLIVEIRA NETO (Siape:
1354733 ),  para substituir   LUIGI CARRO (Siape: 2174422 ), Coordenador do PPG em Computação, Código
FUC, em seu afastamento no país, no período de 20/06/2016 a 22/06/2016, com o decorrente pagamento das
vantagens por 3 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
