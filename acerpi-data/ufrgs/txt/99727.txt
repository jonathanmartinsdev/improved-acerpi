Documento gerado sob autenticação Nº NSU.624.747.7C3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9450                  de  17/10/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  LUIS  RENATO  FERREIRA  DA  SILVA,  matrícula  SIAPE  n°  1910373,  lotado  e  em  exercício  no
Departamento de Direito Privado e Processo Civil da Faculdade de Direito, da classe C  de Professor Adjunto,
nível 03, para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 12/01/2016 a 11/01/2018,
com vigência  financeira  a  partir  de  12/01/2018,  conforme decisão judicial  proferida no processo nº
5054491-30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de
28  de  dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.526022/2019-15.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
