Documento gerado sob autenticação Nº YUK.145.298.H1L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3981                  de  09/05/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  30/04/2019,   referente  ao  interstício  de
06/07/2017 a  29/04/2019,  para  o  servidor  DHEYFESSON DE SOUZA PINHEIRO,  ocupante  do  cargo  de
Técnico de Tecnologia da Informação -  701226,  matrícula SIAPE 1731363,   lotado  na  Faculdade de
Agronomia, passando do Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível
de  Capacitação  D  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.511215/2019-71:
ENAP - eMAG Conteudista CH: 20 (21/08/2018 a 24/10/2018)
ENAP- Elaboração de plano de dados abertos CH: 20 (29/08/2017 a 26/09/2017)
ENAP - Curso Básico em Orçamento Público CH: 30 (30/04/2019 a 30/04/2019)
ENAP - Direitos Humanos da Criança e do Adolescente CH: 20 (01/04/2019 a 29/04/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
