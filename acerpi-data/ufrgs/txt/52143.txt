Documento gerado sob autenticação Nº DXL.142.186.F07, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2147                  de  21/03/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  à  servidora  MARA  REGINA
RODRIGUES DOMINGUES, ocupante do cargo de Técnico de Laboratório Área-701244, lotada no Instituto de
Pesquisas Hidráulicas, SIAPE 0353823, para 30% (trinta por cento), a contar de 22/02/2018, tendo em vista a
conclusão  do  curso  de  Especialização  em  Gestão  e  Educação  Ambiental,  conforme  o  Processo  nº
23078.503219/2018-03.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
