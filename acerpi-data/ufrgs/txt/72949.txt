Documento gerado sob autenticação Nº MNU.143.914.4DM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9758                  de  03/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar  a  Portaria  n°  9703/2018,  de  30/11/2018,  publicada  no  Diário  Oficial  da  União  de
03/12/2018, que dispensou JOSE VICENTE TAVARES DOS SANTOS, Professor do Magistério Superior, com
exercício  no Instituto Latino-Americano de Estudos Avançados,  da função de Coordenador do PPG em
Segurança Cidadã, código SRH 1526, código FUC. Processo nº 23078.530991/2018-90.
 
Onde se lê:
"...Dispensar, a partir da data de publicação no Diário Oficial da União, o ocupante do cargo de
Professor do Magistério Superior, classe Titular, do Quadro de Pessoal desta Universidade, JOSE VICENTE
TAVARES DOS SANTOS, matrícula SIAPE n° 0353328,  da função de Coordenador do PPG em Segurança
Cidadã, Código SRH 1526, código FUC, para a qual foi designado pela Portaria 8503/2017, de 11/09/2017,
publicada no Diário Oficial da União do dia 14/09/2017...",
leia-se:
"...Declarar vaga, a partir da data de publicação no Diário Oficial da União, a função de Coordenador
do  PPG  em  Segurança  Cidadã,  Código  SRH  1526,  código  FUC,  desta  Universidade,  tendo  em  vista  a
aposentadoria de JOSE VICENTE TAVARES DOS SANTOS, matrícula SIAPE n° 0353328, conforme Portaria nº
9648, de 29/11/2018...", ficando ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
