Documento gerado sob autenticação Nº NJK.893.760.7ID, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6883                  de  03/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  Técnico  em  Assuntos
Educacionais, do Quadro de Pessoal desta Universidade, VANESSA VITCOSKI DAITX (Siape: 2015797),  para
substituir EMILSE MARIA AGOSTINI MARTINI (Siape: 0352201 ), Vice-Secretária de Relações Internacionais,
Código SRH 1251, em seu afastamento por motivo de férias, no período de 03/09/2018 a 23/09/2018.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
