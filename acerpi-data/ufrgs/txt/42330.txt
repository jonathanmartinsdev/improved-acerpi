Documento gerado sob autenticação Nº WFJ.699.351.EUJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7152                  de  04/08/2017
Constituição  de  Comissão  de  Sindicância
Investigativa da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Reinstaurar Sindicância Investigativa a ser processada pela Comissão integrada pelos servidores 
ADRIANO DE FRAGA ERREIRA, ocupante do cargo de Servente de Obras, lotado na Superintendência de
Infraestrutura e com exercício na Prefeitura Campus Litoral Norte, ARAO DA SILVA MORAES, ocupante do
cargo de Assistente em Administração, lotado no Instituto de Ciências Básicas da Saúde e com exercício na
Coordenadoria de Planejamento da Graduação da Pró-Reitoria de Graduação, para sob a Presidência da
primeiro, no prazo de trinta dias, concluir os trabalhos no processo nº 23078.016060/2016-11
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
