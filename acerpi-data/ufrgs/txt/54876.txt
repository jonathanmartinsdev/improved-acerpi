Documento gerado sob autenticação Nº YBL.919.498.42F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4008                  de  30/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  JULIANA  BALBINOT  HILGERT,  matrícula  SIAPE  n°  1764311,  lotada  e  em  exercício  no
Departamento de Odontologia Preventiva e Social, da classe C  de Professor Adjunto, nível 04, para a classe D 
de Professor Associado, nível 01, referente ao interstício de 17/02/2016 a 16/02/2018, com vigência financeira
a partir de 11/04/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.503360/2018-06.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
