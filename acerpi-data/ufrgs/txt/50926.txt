Documento gerado sob autenticação Nº OKP.045.648.G7R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1357                  de  15/02/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°32288,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, JAQUELINE TITTONI (Siape: 1151979 ),  para substituir  
NEUZA MARIA DE FÁTIMA GUARESCHI (Siape: 1768391 ), Chefe do Depto de Psicologia Social e Institucional
do Instituto de Psicologia, Código FG-1, em seu afastamento por motivo de férias, no período de 19/02/2018
a 23/02/2018, com o decorrente pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
