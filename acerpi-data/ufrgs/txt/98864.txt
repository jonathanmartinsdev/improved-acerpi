Documento gerado sob autenticação Nº CBB.621.934.RF3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8886                  de  01/10/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARIA CRISTINA DA SILVA MARTINS,  matrícula SIAPE n° 1495042, lotada e em exercício no
Departamento de Letras Clássicas e Vernáculas do Instituto de Letras, da classe D  de Professor Associado,
nível  02,  para  a  classe  D   de  Professor  Associado,  nível  03,  referente  ao  interstício  de  01/07/2017  a
30/06/2019,  com  vigência  financeira  a  partir  de  01/07/2019,  conforme  decisão  judicial  proferida  no
processo nº 5054491-30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a
Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo
nº 23078.522497/2019-32.
JANE FRAGA TUTIKIAN
Vice-Reitora.
