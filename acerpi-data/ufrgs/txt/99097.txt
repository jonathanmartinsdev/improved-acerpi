Documento gerado sob autenticação Nº PTQ.511.079.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9045                  de  07/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de FELIPE PADILHA LEITZKE,  Geólogo, lotado no Instituto de
Geociências e com exercício no Laboratório de Geologia Isotópica, com a finalidade de realizar trabalho de
campo no curso "Geologia dos Andes Centrais", em San Miguel de Tucumán, Purmamarca, Tilcara e Salta,
Argentina,  e  em  San  Pedro  de  Atacama,  Tocopilla,  Calama,  Iquique  e  Antofagasta,  Chile,  no  período
compreendido entre 12/10/2019 e 27/10/2019, incluído trânsito, com ônus limitado. Solicitação nº 86402.
RUI VICENTE OPPERMANN
Reitor
