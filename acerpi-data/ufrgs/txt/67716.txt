Documento gerado sob autenticação Nº UXH.089.013.6RD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5977                  de  08/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor JOAO CARLOS OLIVA, matrícula SIAPE n° 1081416, lotado e em exercício no Departamento de
Educação Física, Fisioterapia e Dança da Escola de Educação Física, Fisioterapia e Dança, da classe   de
Professor Adjunto,  nível  02,  para a  classe   de Professor Adjunto,  nível  03,  referente ao interstício  de
23/07/2009 a 22/07/2011, com vigência financeira a partir de 22/04/2018, de acordo com o que dispõe a Lei
12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº
23078.502567/2018-55.
JANE FRAGA TUTIKIAN
Vice-Reitora.
