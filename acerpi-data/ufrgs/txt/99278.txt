Documento gerado sob autenticação Nº DTE.798.478.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9165                  de  10/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar a Portaria n° 4337/2019, de 20/05/2019, que concedeu autorização para afastamento do
País a CRISTIANE PIZZUTTI DOS SANTOS, Professor do Magistério Superior, com exercício no Departamento
de Ciências Administrativas da Escola de Administração
Onde se lê: com a finalidade de participar do "16th International Research Symposium on Advancing
Service Research and Practice", em Karlstad, Suécia, no período compreendido entre 08 e 14/06/2019, com
ônus  UFRGS  (Pró-Reitoria  de  Pesquisa:  diárias)  e  realizar  visita  ao  Copenhagen  Business  School,  em
Copenhagen, Dinamarca, no período compreendido entre 15 e 22/06/2019, com ônus limitado,
leia-se: com a finalidade de participar do "16th International Research Symposium on Advancing
Service Research and Practice", em Karlstad, Suécia, no período compreendido entre 08 e 14/06/2019, com
ônus  UFRGS  (Pró-Reitoria  de  Pesquisa:  diárias)  e  realizar  visita  ao  Copenhagen  Business  School,  em
Copenhagen, Dinamarca, no período compreendido entre 15 e 22/06/2019, sendo de 16 a 20/06/2019 com
ônus CAPES/PROAP, e os dias 15,21 e 22/06/2019 com ônus limitado, ficando ratificados os demais termos.
Solicitação nº 87614.
RUI VICENTE OPPERMANN
Reitor.
