Documento gerado sob autenticação Nº YLQ.398.263.D12, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             896                  de  26/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  LUIZ  DARIO  TEIXEIRA  RIBEIRO,  matrícula  SIAPE  n°  0353704,  lotado  no
Departamento de História do Instituto de Filosofia e Ciências Humanas, como Coordenador Substituto da
COMGRAD de História, para substituir automaticamente o titular desta função em seus afastamentos ou
impedimentos  regulamentares  no  período  de  31/01/2017  e  até  30/01/2019.  Processo  nº
23078.200150/2017-70.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
