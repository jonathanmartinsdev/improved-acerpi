Documento gerado sob autenticação Nº WFM.660.885.L0E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4699                  de  31/05/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°84413,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  ERISSANDRA  GOMES  (Siape:  1675063  ),   para
substituir   JONAS DE ALMEIDA RODRIGUES (Siape: 1835685 ), Chefe do Depto de Cirurgia e Ortopedia da
Faculdade  de  Odontologia,  Código  FG-1,  em  seu  afastamento  no  país,  no  período  de  03/06/2019  a
05/06/2019, com o decorrente pagamento das vantagens por 3 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
