Documento gerado sob autenticação Nº UOV.884.292.A46, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3818                  de  24/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, LUIZ DARIO TEIXEIRA RIBEIRO (Siape: 0353704),  para
substituir  LUIZ DARIO TEIXEIRA RIBEIRO (Siape: 0353704 ), Coordenador da COMGRAD de História em seu
afastamento no país, no período de 23/05/2016 a 26/05/2016, com o decorrente pagamento das vantagens
por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
