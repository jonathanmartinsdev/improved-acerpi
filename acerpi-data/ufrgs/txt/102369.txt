Documento gerado sob autenticação Nº ICA.619.243.G4N, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11186                  de  17/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  08/12/2019,   referente  ao  interstício  de
18/05/2018 a 07/12/2019, para a servidora RENATA ISOÍ MORAIS DOS SANTOS,  ocupante do cargo de
Assistente em Administração - 701200,  matrícula SIAPE 2346562,  lotada  no  Instituto de Informática,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.528507/2019-43:
ILB - Excelência no Atendimento CH: 20 (08/08/2019 a 28/08/2019)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 40 (18/11/2019 a 08/12/2019)
ILB - CONHECENDO O NOVO ACORDO ORTOGRÁFICO CH: 20 (21/10/2019 a 10/11/2019)
ILB - ÉTICA E ADMINISTRAÇÃO PÚBLICA CH: 40 (19/12/2016 a 23/01/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
