Documento gerado sob autenticação Nº KKX.294.170.P57, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8901                  de  04/11/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de NEY FIALKOW, Professor do Magistério Superior, lotado e em
exercício no Departamento de Música do Instituto de Artes, com a finalidade de participar de jornadas junto
ao Auditorio Nacional del SODRE Dra. Adela Reta, em Montevidéu, Uruguai, no período compreendido entre
29/11/2016 e 01/12/2016, incluído trânsito, com ônus limitado. Solicitação nº 24399.
RUI VICENTE OPPERMANN
Reitor
