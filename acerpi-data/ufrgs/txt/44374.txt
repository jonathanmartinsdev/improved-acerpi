Documento gerado sob autenticação Nº EYD.722.686.F55, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8486                  de  11/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Remover, a partir de 31 de agosto de 2017,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
ANGELA FERNANDES DA SILVA, ocupante do cargo de Assistente em Administração, Ambiente Organizacional
Administrativo,  Código 701200,  Classe D,  Nível  de Capacitação IV,  Padrão de Vencimento 16,  SIAPE nº.
0357040 da Pró-Reitoria  de Graduação para a  lotação Faculdade de Educação,  com novo exercício  na
Gerência Administrativa da Faculdade de Educação.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
