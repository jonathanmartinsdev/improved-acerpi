Documento gerado sob autenticação Nº VKV.754.717.5IM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8588                  de  24/10/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Retificar  a  Portaria  n°  8487/2018,  de  23/10/2018,  publicada  no  Diário  Oficial  da  União  de
24/10/2018, que designou FERNANDA BRASIL MENDES, Técnico em Assuntos Educacionais, com exercício
no  Núcleo  de  Regulação  da  Secretaria  de  Avaliação  Institucional,  como  Coordenadora  do  Núcleo  de
Regulação da Secretaria de Avaliação Institucional. Processo nº 23078.528162/2018-47.
 
Onde se lê:
"...com vigência a partir da data de publicação no Diário Oficial da União...",
leia-se:
"...com vigência a partir de 01/11/2018...", ficando ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora.
