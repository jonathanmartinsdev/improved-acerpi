Documento gerado sob autenticação Nº WCY.125.462.58R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2666                  de  28/03/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº 23078.200473/2017-63
RESOLVE
Retificar  a  Portaria  nº  2171/2017,  de  10/03/2017,  que  designou,  temporariamente,  RODRIGO
STUMPF GONZALEZ (Siape: 6757031) para substituir PAULO SERGIO PERES (Siape: 1445592),
 
onde se lê:
 
"...  em seu afastamento por motivo de férias,  no período de 04/03/2017 a 05/04/2017, com o
decorrente pagamento das vantagens por 33 dias."
 
           leia-se:
 
"...  em  seu  afastamento  por  motivo  de  férias,  nos  dias  04  e  05/03/2017,  com  o  decorrente
pagamento das vantagens por 02 dias", ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
