Documento gerado sob autenticação Nº OVL.532.055.9E2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10427                  de  19/11/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
que lhe foram conferidas pela Portaria nº 7678, de 30 de setembro de 2016, do Magnífico Reitor, e conforme
a Solicitação de Afastamento n°88973,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, MARCELO SOARES MACHADO (Siape: 0353832
),  para substituir   MAURÍCIO VIÉGAS DA SILVA (Siape: 6354315 ), Pró-Reitor de Gestão de Pessoas, Código
CD-2, em seu afastamento no país, no período de 20/11/2019 a 22/11/2019, com o decorrente pagamento
das vantagens por 3 dias.
JANE FRAGA TUTIKIAN
Pró-Reitora
