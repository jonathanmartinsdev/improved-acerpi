Documento gerado sob autenticação Nº XSN.381.301.H6Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             579                  de  18/01/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar, a partir de 10/01/2018, o ocupante do cargo de Assistente em Administração - 701200,
do Nível  de Classificação DIII,  do Quadro de Pessoal  desta Universidade,  RAFAEL SCHRODER PEREIRA,
matrícula SIAPE 2055946 da função de Coordenador do Núcleo Acadêmico da Gerência Administrativa da
Faculdade  de  Odontologia,  Código  SRH  624,  Código  FG-7,  para  a  qual  foi  designado  pela  Portaria  nº
5524/2017  de  26/06/2017,  publicada  no  Diário  Oficial  da  União  de  27/06/2017.  Processo  nº
23078.500678/2018-27.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
