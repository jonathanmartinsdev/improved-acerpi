Documento gerado sob autenticação Nº GBY.541.313.EMK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             330                  de  09/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar  na  Superintendência  de  Infraestrutura,  com  exercício  no  Planejamento  e  Assessoria  da
Superintendência de Infraestrutura, Ambiente Organizacional Administrativo, MARCIO JOSE DOS SANTOS
MENEZES, nomeado conforme Portaria Nº 11287/2019 de 19 de dezembro de 2019, publicada no Diário
Oficial da União no dia 20 de dezembro de 2019, em efetivo exercício desde 08 de janeiro de 2020, ocupante
do cargo de ADMINISTRADOR, classe E,  nível  I,  padrão 101,  no Quadro de Pessoal  desta Universidade.
Processo n° 23078.500335/2020-87.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
