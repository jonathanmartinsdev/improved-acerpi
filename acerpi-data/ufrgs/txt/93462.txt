Documento gerado sob autenticação Nº SQK.503.260.IN1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5509                  de  01/07/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, MICHEL JOSE ANZANELLO, matrícula SIAPE n° 2449508, lotado no Departamento
de Engenharia de Produção e Transportes da Escola de Engenharia, como Coordenador Substituto do PPG
em Engenharia de Produção, para substituir automaticamente o titular desta função em seus afastamentos
ou impedimentos regulamentares na vigência do presente mandato. Processo nº 23078.514062/2019-14.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
