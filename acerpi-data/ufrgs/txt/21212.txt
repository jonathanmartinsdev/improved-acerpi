Documento gerado sob autenticação Nº HGL.190.327.9CC, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3106                  de  27/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MAITY SIMONE GUERREIRO SIQUEIRA, Professor do Magistério
Superior, lotada e em exercício no Departamento de Linguística, Filologia e Teoria Literária do Instituto de
Letras, com a finalidade de participar da "11th Conference of the Association for Researching and Applying
Metaphor", com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias) e da "Conference The Premeditated Path
Deliberate Metaphor in Ancient and Modern Texts", em Berlim, Alemanha, com ônus limitado, no período
compreendido entre 29/06/2016 e 09/07/2016, incluído trânsito. Solicitação nº 19056.
CARLOS ALEXANDRE NETTO
Reitor
