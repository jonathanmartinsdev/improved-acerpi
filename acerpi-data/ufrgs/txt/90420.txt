Documento gerado sob autenticação Nº FIQ.756.698.CT2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3696                  de  30/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  ROGERIO  DA  CUNHA  VOSER,  matrícula  SIAPE  n°  2322912,  lotado  no
Departamento de Educação Física, Fisioterapia e Dança da Escola de Educação Física, Fisioterapia e Dança,
para exercer a função de Diretor do Laboratório de Pesquisa do Exercício, Código SRH 232, código FG-1, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.510957/2019-80.
RUI VICENTE OPPERMANN
Reitor.
