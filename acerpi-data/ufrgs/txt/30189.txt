Documento gerado sob autenticação Nº SCF.595.278.QU7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9035                  de  09/11/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de DARCI BARNECH CAMPANI, Professor do Magistério Superior,
lotado no Departamento de Engenharia Mecânica da Escola de Engenharia e com exercício na Assessoria de
Gestão  Ambiental,  com a  finalidade  de  participar  do  "III  Encuentro  Latinoamericano  de  Universidades
Sustentables: Reforzando el compromiso de la Educación Superior en la protección de nuestro Planeta Tierra", em
Tucumán, Argentina, no período compreendido entre 22/11/2016 e 26/11/2016, incluído trânsito, com ônus
limitado. Solicitação nº 24550.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
