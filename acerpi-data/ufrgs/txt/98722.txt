Documento gerado sob autenticação Nº UWE.435.915.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8812                  de  27/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LUCIANE MARIA PEREIRA PASSAGLIA, Professor do Magistério
Superior, lotada e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de
participar da "21st International Conference on Nitrogen Fixation (ICNF 2019)", em Wuhan, China, no período
compreendido entre 06/10/2019 e 16/10/2019, incluído trânsito, com ônus limitado. Solicitação nº 87554.
RUI VICENTE OPPERMANN
Reitor
