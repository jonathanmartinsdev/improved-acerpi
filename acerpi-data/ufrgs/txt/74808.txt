Documento gerado sob autenticação Nº CCH.824.684.9JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             357                  de  10/01/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder  jornada  de  trabalho  reduzida  com  remuneração  proporcional  à  servidora  FLAVIA
BERTOLIN, matrícula SIAPE n° 1682692, ocupante do cargo de Assistente em Administração - 701200, lotada
no Instituto de Informática e com exercício no Setor de Pesquisa e Extensão da Gerência Administrativa do
Instituto de Informática, alterando a jornada de trabalho de oito horas diárias e quarenta horas semanais
para 6 horas diárias e 30 semanais, no período de 15 de janeiro de 2019 a 15 de janeiro de 2020, de acordo
com os art. 5º a 7º, da Medida Provisória nº 2.174-28, de 24 de agosto de 2001, e conforme o Processo nº
23078.519256/2017-44.
RUI VICENTE OPPERMANN
Reitor.
