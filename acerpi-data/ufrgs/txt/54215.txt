Documento gerado sob autenticação Nº IBM.754.112.RMV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3465                  de  11/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLARISSA GREGORY BRUNET,  Professor do Magistério do
Ensino Básico, Técnico e Tecnológico, lotada no Colégio de Aplicação e com exercício no Departamento de
Comunicação do Colégio de Aplicação, com a finalidade de participar da "XVIIème édition du SEDIFRALE", em
Bogotá, Colômbia, no período compreendido entre 05/06/2018 e 10/06/2018, incluído trânsito, com ônus
limitado. Solicitação nº 45924.
RUI VICENTE OPPERMANN
Reitor
