Documento gerado sob autenticação Nº YIU.934.846.POV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3440                  de  11/05/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder jornada de trabalho reduzida, com remuneração proporcional à servidora CAROLINA
BENITZ DA SILVA, matrícula SIAPE n° 2225212, ocupante do cargo de Auxiliar em Administração - 701405,
lotada no Instituto de Química e com exercício no Núcleo Técnico-Científico Central Analítica do Instituto de
Química, alterando a jornada de trabalho de oito horas diárias e quarenta horas semanais para 6 horas
diárias e 30 semanais, no período de 14 de maio de 2018 a 14 de maio de 2019, nos termos dos artigos 5º a
7º  da  Medida  Provisória  nº  2.174-28,  de  24  de  agosto  de  2001,  e  conforme  o  Processo  nº
23078.507284/2018-08.
JANE FRAGA TUTIKIAN
Vice-Reitora.
