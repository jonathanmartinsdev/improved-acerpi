Documento gerado sob autenticação Nº LTL.416.743.T9H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6985                  de  02/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, CRISTIANO KRUG (Siape: 1566597),  para substituir
NAIRA MARIA BALZARETTI (Siape: 0354772 ), Diretor do Centro de Nanociência e Nanotecnologia-CNANO,
Código SRH 1319, FG-1, em seu afastamento por motivo de férias, no período de 04/08/2019 a 11/08/2019.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
