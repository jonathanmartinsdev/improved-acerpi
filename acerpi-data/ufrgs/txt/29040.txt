Documento gerado sob autenticação Nº TNI.514.492.32Q, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8142                  de  11/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  09/09/2016,   referente  ao  interstício  de
05/02/2012 a 08/09/2016,  para a servidora JANAINA DIAS CUNHA,  ocupante do cargo de Técnico em
Assuntos Educacionais - 701079, matrícula SIAPE 1678331,  lotada  na  Pró-Reitoria de Pesquisa, passando
do Nível de Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.019994/2016-13:
Formação Integral de Servidores da UFRGS V CH: 147 Carga horária utilizada: 126 hora(s) / Carga horária
excedente: 21 hora(s) (03/11/2011 a 23/08/2016)
PROREXT - Curso de Introdução à Fotografia - fevereiro/2011 CH: 27 (21/02/2011 a 01/03/2011)
PROREXT - Curso de Fotografia Básico 2 CH: 27 (15/03/2011 a 25/03/2011)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
