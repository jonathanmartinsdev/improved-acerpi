Documento gerado sob autenticação Nº IDD.253.632.GCI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4662                  de  29/05/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  LEONARDO  GRANATO,  matrícula  SIAPE  n°  2264340,  lotado  na  Escola  de
Administração, como Coordenador Substituto do PPG em Ciência Política, para substituir automaticamente o
titular  desta  função em seus  afastamentos  ou  impedimentos  regulamentares  na  vigência  do  presente
mandato. Processo nº 23078.513845/2019-81.
RUI VICENTE OPPERMANN
Reitor.
