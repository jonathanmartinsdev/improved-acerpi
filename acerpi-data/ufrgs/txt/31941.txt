Documento gerado sob autenticação Nº YXC.965.390.QFF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10266                  de  26/12/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora FABIANA HORN, matrícula SIAPE n° 2280034, lotada e em exercício no Departamento de Biofísica
do Instituto de Biociências, da classe D  de Professor Associado, nível 04, para a classe E  de Professor Titular, 
referente ao interstício de 09/10/2014 a 08/10/2016, com vigência financeira a partir de 09/10/2016, de
acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e a Decisão nº
232/2014 - CONSUN. Processo nº 23078.516305/2016-14.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
