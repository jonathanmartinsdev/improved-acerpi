Documento gerado sob autenticação Nº QPI.887.063.Q7K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10954                  de  09/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/12/2019,   referente  ao  interstício  de
05/06/2018 a 04/12/2019, para a servidora CAROLINA BARCELOS, ocupante do cargo de Relações Públicas -
701072,  matrícula  SIAPE  2348756,   lotada   na   Escola  de  Administração,  passando  do  Nível  de
Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.531741/2019-58:
Formação Integral de Servidores da UFRGS III CH: 61 (24/05/2018 a 18/10/2019)
Fundação Bradesco - Comunicação Escrita CH: 34 (14/04/2018 a 14/04/2018)
ENAP - eMAG Conteudista CH: 20 Carga horária utilizada: 15 hora(s) / Carga horária excedente: 5 hora(s)
(13/11/2019 a 14/11/2019)
ENAP - Planejamento Estratégico para Organizações Públicas CH: 40 (23/10/2019 a 13/11/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
