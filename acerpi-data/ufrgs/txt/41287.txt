Documento gerado sob autenticação Nº GYR.842.797.PH1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6419                  de  18/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Exonerar, a pedido, a partir de 30 de junho de 2017, nos termos do artigo 34 "caput", da Lei n°
8.112, de 1990, SUSANA BEHENCK SEIBEL, ocupante do cargo de Técnico em Contabilidade, código 701224,
nível de classificação D, nível de capacitação III, padrão 05, do Quadro de Pessoal, lotada e com exercício na
Pró-Reitoria de Planejamento e Administração. Processo nº 23078.012500/2017-42.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
