Documento gerado sob autenticação Nº EAH.249.181.9CC, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3107                  de  27/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARIA JOAO VELOSO DA COSTA RAMOS PEREIRA, Professor
do Magistério Superior, lotada e em exercício no Departamento de Zoologia do Instituto de Biociências, com
a finalidade de participar da "17th International Bat Research Conference", em Durban, República da África
do Sul, no período compreendido entre 30/07/2016 e 06/08/2016, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa - diárias). Solicitação nº 19122.
CARLOS ALEXANDRE NETTO
Reitor
