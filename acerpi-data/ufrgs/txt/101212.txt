Documento gerado sob autenticação Nº RVK.124.383.V2N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10449                  de  20/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de Professor do Magistério
Superior,  do  Quadro  de  Pessoal  desta  Universidade,  MARA  CRISTINA  DE  MATOS RODRIGUES  (Siape:
1376066),   para substituir CAROLINE PACIEVITCH (Siape: 2119459),  Coordenadora do PPG em Ensino de
História,  Código  FUC,  em  seu  afastamento  no  país,  no  período  de  21/11/2019  a  22/11/2019,  com  o
decorrente pagamento das vantagens por 2 dias.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
