Documento gerado sob autenticação Nº IBE.148.276.VCT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4166                  de  08/06/2018
Nomeação  da  Representação  Discente  da
Coordenadoria das Licenciaturas da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita para compor a Coordenadoria das Licenciaturas,  com
mandato de 01 (um) ano, a contar da data de publicação desta portaria, atendendo ao disposto nos artigos
175 do Regimento Geral da Universidade e 79 do Estatuto da Universidade e considerando o processo
nº 23078.513617/2018-20, conforme segue:
 
Coordenadoria das Licenciaturas - COORLICEN
 
1º Titular: Juliane Rodrigues Gonçalves
1º Suplente: Giulia Minuzzo de Lima
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
