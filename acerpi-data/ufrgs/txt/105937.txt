Documento gerado sob autenticação Nº VLN.970.203.IRF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2034                  de  05/03/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora ANDREIA MARIS PERIUS,
ocupante do cargo de Assistente de Aluno-701403, lotada no Colégio de Aplicação, SIAPE 2259906, para 30%
(trinta por cento), a contar de 20/01/2020, tendo em vista a conclusão do curso de MBA em Gestão Pública,
conforme o Processo nº 23078.532731/2019-30.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
