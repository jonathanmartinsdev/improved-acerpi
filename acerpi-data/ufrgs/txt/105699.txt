Documento gerado sob autenticação Nº MXQ.431.665.IRF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1900                  de  05/03/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°51635,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, ALFREDO LUIZ MOSENA (Siape: 0358805 ),
 para substituir   MAGUINORES FERREIRA PEREIRA (Siape: 1723310 ), Diretor da Divisão de Conformidade
Normativa do Núcleo de Contratos e Normativas vinculado à Coordenadoria Administrativa e Financeira da
PROPLAN, Código FG-5, em seu afastamento por motivo de férias, no período de 26/02/2020 a 13/03/2020.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
