Documento gerado sob autenticação Nº AGR.128.406.OD5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7786                  de  03/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, EDSON TALAMINI, matrícula SIAPE n° 1642083, lotado no Departamento de
Economia  e  Relações  Internacionais  da  Faculdade  de  Ciências  Econômicas,  para  exercer  a  função  de
Coordenador do PPG em Agronegócios, Código SRH 1107, código FUC, com vigência a partir 07/10/2016 e até
06/10/2018. Processo nº 23078.019813/2016-41.
JANE FRAGA TUTIKIAN
Vice-Reitora
 
 
