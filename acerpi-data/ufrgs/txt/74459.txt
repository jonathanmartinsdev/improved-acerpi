Documento gerado sob autenticação Nº KDC.209.898.9JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             121                  de  04/01/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de JACOB SCHARCANSKI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a finalidade de
realizar  visita  à  Faculdade  de  Engenharia  da  Universidade  do  Porto,  em  Porto,  Portugal,  no  período
compreendido entre 15/01/2019 e 22/01/2019, incluído trânsito, com ônus limitado. Solicitação nº 61879.
RUI VICENTE OPPERMANN
Reitor
