Documento gerado sob autenticação Nº CWL.742.443.NK5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2774                  de  13/04/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  MARIA  INES  REINERT  AZAMBUJA,  matrícula  SIAPE  n°  1149748,  lotada  e  em  exercício  no
Departamento de Medicina Social da Faculdade de Medicina, da classe   de Professor Adjunto, nível 01, para a
classe   de Professor Adjunto, nível 02, referente ao interstício de 20/12/2001 a 20/12/2005, com vigência
financeira a partir de 12/04/2018, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo
nº 23078.522539/2017-73.
JANE FRAGA TUTIKIAN
Vice-Reitora.
