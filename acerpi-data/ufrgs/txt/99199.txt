Documento gerado sob autenticação Nº RCR.990.510.I34, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9131                  de  08/10/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  KELLY  LISSANDRA  BRUCH,  matrícula  SIAPE  n°  2201484,  lotada  no
Departamento de Direito Econômico e do Trabalho da Faculdade de Direito,  para exercer a função de
Coordenadora da COMGRAD de Direito, Código SRH 1205, código FUC, com vigência a partir de 16/10/2019
até 15/10/2021, sem prejuízo e cumulativamente com a função de Diretora Substituta do Centro de Estudos
Interdisciplinares em Agronegócios. Processo nº 23078.526920/2019-73.
JANE FRAGA TUTIKIAN
Vice-Reitora.
