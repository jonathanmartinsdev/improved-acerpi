Documento gerado sob autenticação Nº VZP.546.785.TDG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5727                  de  29/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  19/05/2016,
correspondente ao grau Insalubridade Média, à servidora VIVIANE RODRIGUES PEREIRA, Identificação Única
18388280, Assistente em Administração, com exercício no Núcleo Especializado da Gerencia Administrativa
da ODONTO, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei
8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Insalubres conforme
Laudo Pericial  constante  no Processo nº  23078.504543/2016-79,  Código  SRH n°  22851 e  Código  SIAPE
2016002689.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
