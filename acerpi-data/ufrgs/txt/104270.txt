Documento gerado sob autenticação Nº SGF.994.394.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             868                  de  24/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder prorrogação de jornada de trabalho reduzida, nos termos dos artigos 5º ao 7º da Medida
Provisória nº 2.174-28, de 24 de agosto de 2001 e conforme o Processo nº 23078.501241/2020-25, com
remuneração proporcional ao servidor CLAUDIO OLIVEIRA RIOS, matrícula SIAPE n° 2179531, ocupante do
cargo de Contador - 701015, lotado na Editora da UFRGS e com exercício na Seção de Distribuição da Editora
da UFRGS, alterando a jornada de trabalho de oito horas diárias e quarenta horas semanais para 6 horas
diárias e 30 semanais, no período de 05 de março de 2020 a 05 de março de 2021.
RUI VICENTE OPPERMANN
Reitor.
