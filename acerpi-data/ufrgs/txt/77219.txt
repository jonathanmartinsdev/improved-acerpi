Documento gerado sob autenticação Nº NMZ.360.841.T6L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1762                  de  22/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  4183,  de 17 de outubro de 2008,  do
Magnífico Reitor, e conforme a Solicitação de Férias n°46643,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, REGINA ANTUNES TEIXEIRA DOS SANTOS (Siape:
1960620 ),  para substituir   NEY FIALKOW (Siape: 0359328 ), Coordenador do PPG em Música, Código FUC,
em seu afastamento por motivo de férias,  no período de 22/02/2019 a 27/02/2019,  com o decorrente
pagamento das vantagens por 6 dias.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
