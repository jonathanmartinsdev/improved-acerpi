Documento gerado sob autenticação Nº WDV.670.696.TAF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             466                  de  15/01/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  09/11/2017,   referente  ao  interstício  de
01/03/2005 a 08/11/2017, para o servidor FRANCISCO DELMAR LOPES MATHEUS, ocupante do cargo de
Porteiro -  701458,  matrícula SIAPE 0358332,   lotado  na  Faculdade de Direito,  passando do Nível  de
Classificação/Nível de Capacitação C I, para o Nível de Classificação/Nível de Capacitação C II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.520812/2017-25:
Formação Integral de Servidores da UFRGS III  CH: 87 Carga horária utilizada: 60 hora(s) / Carga horária
excedente: 27 hora(s) (23/06/2008 a 24/11/2016)
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
