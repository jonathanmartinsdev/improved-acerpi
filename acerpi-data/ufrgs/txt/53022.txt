Documento gerado sob autenticação Nº YED.031.960.QP9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2596                  de  10/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder afastamento parcial, no período de 10/05/2018 a 09/05/2019, para o servidor RUBENS DA
COSTA  SILVA  FILHO,  ocupante  do  cargo  de  Bibliotecário-documentalista  -  701010,  matrícula  SIAPE
1568938,  lotado  na  Escola de Enfermagem, para cursar o Doutorado em Comunicação e Informação,
oferecido pela UFRGS, conforme o Processo nº 23078.504104/2018-28.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
