Documento gerado sob autenticação Nº ZFA.049.966.JGN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8266                  de  10/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder jornada de trabalho reduzida, nos termos dos artigos 5º ao 7º da Medida Provisória nº
2.174-28, de 24 de agosto de 2001 e conforme o Processo nº 23078.523767/2019-22, com remuneração
proporcional, ao servidor MAIQUEL ISAGO PAVELECINI, matrícula SIAPE n° 3088118, ocupante do cargo de
Programador Visual - 701066, lotado na Secretaria de Comunicação Social e com exercício na Secretaria de
Comunicação Social, alterando a jornada de trabalho de oito horas diárias e quarenta horas semanais para
seis horas diárias e trinta horas semanais, pelo período de um ano a contar da data de publicação desta
Portaria.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
