Documento gerado sob autenticação Nº UVT.909.171.BAH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8299                  de  11/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de PAULO CESAR DE FACCIO CARVALHO, Professor do Magistério
Superior, lotado e em exercício no Departamento de Plantas Forrageiras e Agrometeorologia da Faculdade
de Agronomia, com a finalidade de ministrar curso junto ao Instituto Nacional de Tecnología Agropecuaria,
em Reconquista, Argentina, no período compreendido entre 12/09/2019 e 15/09/2019, incluído trânsito, com
ônus limitado. Solicitação nº 87148.
RUI VICENTE OPPERMANN
Reitor
