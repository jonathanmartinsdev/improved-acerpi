Documento gerado sob autenticação Nº CTE.426.312.4O1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7615                  de  22/08/2019
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de SORAYA TANURE, Professor do Magistério Superior, lotada e
em exercício no Departamento Interdisciplinar do Campus Litoral Norte, com a finalidade de proferir palestra
e participar de reuniões junto ao Instituto Nacional de Investigación Agropecuaria, em Montevidéu, Uruguai,
no período compreendido entre 25/08/2019 e 30/08/2019, incluído trânsito, com ônus limitado. Solicitação nº
86324.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
