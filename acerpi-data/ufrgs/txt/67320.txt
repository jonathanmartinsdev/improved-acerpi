Documento gerado sob autenticação Nº HBL.054.817.J7C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5766                  de  01/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ADRIANA DORFMAN, Professor do Magistério Superior, lotada
no Departamento de Geografia do Instituto de Geociências e com exercício na Comissão de Graduação de
Geografia, com a finalidade de ministrar disciplina da "Cátedra IMANI: Encuentros entre la Amazonía, América
y Europa: ", na Universidad Nacional de Colombia e partiicpar de reuniões, em Leticia, Colômbia, no período
compreendido entre 22/08/2018 e 30/08/2018, incluído trânsito, com ônus limitado. Solicitação nº 46640.
RUI VICENTE OPPERMANN
Reitor
