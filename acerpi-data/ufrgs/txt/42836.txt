Documento gerado sob autenticação Nº ARN.961.724.UJM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7521                  de  14/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Nomear, em caráter efetivo, no cargo abaixo, na vaga referente ao respectivo código, em virtude de
habilitação em Concurso Público, conforme Edital Nº 86/2014 de 23 de Junho de 2014, homologado em 25 de
junho de 2014 e de acordo com o artigo 9º, item I, da Lei nº. 8.112, de 11 de dezembro de 1990, com as
alterações dadas pela Lei nº. 11.091, de 12 de janeiro de 2005, publicada no Diário Oficial da União de 13 de
janeiro de 2005, Decreto nº. 7.232, de 19 de julho de 2010, publicado no Diário Oficial da União de 20 de julho
de 2010, Lei 12.772, de 28 de dezembro de 2012, publicada no Diário Oficial da União de 31 de dezembro de
2012. Processo nº. 23078.007742/2014-71.
Cargo 701400 - ADMINISTRADOR DE EDIFÍCIOS - Classe C Padrão I
MAGNUM LEPKOSKI DA SILVA - Vaga SIAPE 273096
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
