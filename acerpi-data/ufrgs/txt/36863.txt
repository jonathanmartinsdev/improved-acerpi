Documento gerado sob autenticação Nº QME.221.986.QMU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3196                  de  13/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor CARLOS ALEXANDRE WENDEL, ocupante do cargo de  Técnico em Assuntos
Educacionais - 701079, lotado no Campus Litoral Norte, SIAPE 1139896, o percentual de 30% (trinta por
cento)  de  Incentivo  à  Qualificação,  a  contar  de  16/01/2017,  tendo  em vista  a  conclusão  do  curso  de
Especialização em Docência do Ensino Superior, conforme o Processo nº 23078.000791/2017-26.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
