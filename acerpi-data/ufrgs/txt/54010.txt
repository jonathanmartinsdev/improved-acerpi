Documento gerado sob autenticação Nº WKR.974.600.UN7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3293                  de  07/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARIA DO ROCIO FONTOURA TEIXEIRA, matrícula SIAPE n° 0359327, lotada no Departamento
de Ciência da Informação da Faculdade de Biblioteconomia e Comunicação e com exercício no Programa de
Pós-Graduação Educação em Ciências: Química da Vida e Saúde, da classe C  de Professor Adjunto, nível 03,
para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 19/12/2015 a 18/12/2017, com
vigência financeira a partir de 06/03/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de
2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.524450/2017-41.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
