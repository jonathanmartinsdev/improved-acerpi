Documento gerado sob autenticação Nº ZTE.712.411.SEQ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4429                  de  22/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a SHEILA
VILLANOVA BORBA, matrícula SIAPE nº 0357306, no cargo de Professor Adjunto, nível 1, da Carreira do
Magistério Superior, do Quadro desta Universidade, no regime de vinte horas semanais de trabalho, com
exercício  no Departamento de Sociologia  do Instituto de Filosofia  e  Ciências  Humanas,  com proventos
integrais. Processo 23078.501771/2019-30.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
