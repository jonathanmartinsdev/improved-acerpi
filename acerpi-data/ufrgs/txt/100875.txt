Documento gerado sob autenticação Nº CBD.733.261.1D2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10244                  de  13/11/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de EMERSON ALESSANDRO GIUMBELLI, Professor do Magistério
Superior,  lotado no Departamento de Antropologia do Instituto de Filosofia e Ciências Humanas e com
exercício  no  Programa  de  Pós-Graduação  em  Antropologia  Social,  com  a  finalidade  de  participar  de
conferência  junto  ao  Cientro  de  Investigaciones  y  Estudios  Superiores  en  Antropologia  Social,  em
Guadalajara, México, no período compreendido entre 27/11/2019 e 01/12/2019, incluído trânsito, com ônus
limitado. Solicitação nº 88175.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
