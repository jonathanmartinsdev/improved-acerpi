Documento gerado sob autenticação Nº DXN.302.796.7FF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9259                  de  05/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ROGERIO FAE, Professor do Magistério Superior, lotado e em
exercício no Departamento de Ciências Administrativas da Escola de Administração, com a finalidade de
participar do "XXXI Congreso de la Asociación Latinoamericana de Sociología", em Montevidéu, Uruguai, no
período compreendido entre 02/12/2017 e 09/12/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa - diárias). Solicitação nº 29585.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
