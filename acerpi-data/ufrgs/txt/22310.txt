Documento gerado sob autenticação Nº PSV.256.610.NSA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3688                  de  18/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ITAMAR CRISTIANO NAVA, matrícula SIAPE n° 1943861, lotado e em exercício no Departamento
de Plantas de Lavoura da Faculdade de Agronomia, da classe C  de Professor Adjunto, nível 02, para a classe
C   de  Professor  Adjunto,  nível  03,  referente  ao  interstício  de  14/05/2014  a  13/05/2016,  com vigência
financeira a partir de 14/05/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela
Decisão nº 401/2013-CONSUN. Processo nº 23078.201224/2016-12.
RUI VICENTE OPPERMANN
Vice-Reitor
