Documento gerado sob autenticação Nº WLC.775.380.GLG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2174                  de  11/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  19/02/2019,   referente  ao  interstício  de
19/08/2017 a 18/02/2019, para a servidora LILIAN PAMELA LIMA E SILVA, ocupante do cargo de Assistente
em Administração - 701200, matrícula SIAPE 1180189,  lotada  na  Escola de Enfermagem, passando do
Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.501125/2019-72:
Formação Integral de Servidores da UFRGS III CH: 61 (12/07/2018 a 12/12/2018)
ILB - Contratações Públicas CH: 60 Carga horária utilizada: 59 hora(s) / Carga horária excedente: 1 hora(s)
(06/08/2018 a 03/10/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
