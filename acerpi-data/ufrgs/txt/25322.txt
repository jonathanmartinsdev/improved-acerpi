Documento gerado sob autenticação Nº HQA.943.507.A4L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5760                  de  01/08/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  RONALDO DOS SANTOS DA ROCHA,  matrícula  SIAPE n°  1149941,  lotado e em exercício  no
Departamento de Geodésia do Instituto de Geociências, da classe D  de Professor Associado, nível 03, para a
classe D  de Professor Associado, nível 04, referente ao interstício de 12/04/2014 a 11/04/2016, com vigência
financeira a partir de 27/07/2016, de acordo com o que dispõe a Decisão nº 197/2006-CONSUN, alterada pela
Decisão nº 401/2013-CONSUN. Processo nº 23078.504135/2016-17.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
