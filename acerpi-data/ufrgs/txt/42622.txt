Documento gerado sob autenticação Nº SYZ.836.688.CB7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7534                  de  14/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LAURO JOSE GREGIANIN, Professor do Magistério Superior,
lotado e  em exercício  no  Departamento  de  Pediatria  da  Faculdade de  Medicina,  com a  finalidade  de
participar de encontro junto ao Grupo Latinoamericano de Oncologia Pediátrica, em Montevidéu, Uruguai, no
período compreendido entre 10/08/2017 e 12/08/2017, incluído trânsito, com ônus limitado. Solicitação nº
30213.
RUI VICENTE OPPERMANN
Reitor
