Documento gerado sob autenticação Nº JQI.887.345.NCT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4430                  de  19/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar na Pró-Reitoria de Gestão de Pessoas, a partir de 24/04/2017, ANA PAULA DAHLKE, matrícula
SIAPE  n°  3437694,  redistribuída  conforme  Portaria  nº  965  de  13/04/2017  do  Ministério  da  Educação,
publicada no Diário Oficial da União no dia 17/04/2017, ocupante do cargo de Técnico em Enfermagem,
Classe D, Nível III, Padrão de Vencimento 07, no Quadro de Pessoal desta Universidade.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
