Documento gerado sob autenticação Nº KMB.538.989.QU7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9023                  de  09/11/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 07/11/2016, a ocupante do cargo de Bibliotecário-documentalista - 701010, do
Nível de Classificação EIV, do Quadro de Pessoal desta Universidade, MARINA PLENTZ,  matrícula SIAPE
1849199 da função de Chefe da Biblioteca de Ciências Sociais e Humanidades, Código SRH 504, Código FG-5,
para a qual foi designada pela Portaria nº 4185/15 de 02/06/2015, publicada no Diário Oficial da União de
08/06/2015. Processo nº 23078.204101/2016-25.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
