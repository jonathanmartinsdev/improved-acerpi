Documento gerado sob autenticação Nº VMO.234.181.U25, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7949                  de  24/08/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar a Portaria n° 7516/2017, de 14/08/2017, que alterou o regime de trabalho de MARTA
SILVEIRA PEIXOTO, Professor do Magistério Superior, com exercício no Departamento de Arquitetura da
Faculdade de Arquitetura. Processo nº 23078.503036/2017-07.
 
 
Onde se lê:
"...alterar, a partir da data deste ato,...",
leia-se:
"...alterar, a partir de 17/08/2017,...",ficando ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
