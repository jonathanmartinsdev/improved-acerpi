Documento gerado sob autenticação Nº DDB.506.141.JFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             819                  de  29/01/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora SILVIA CHWARTZMANN
HALPERN,  ocupante do cargo de Assistente Social-701006, lotada na Pró-Reitoria de Gestão de Pessoas,
SIAPE 6420560, para 75% (setenta e cinco por cento), a contar de 22/12/2017, tendo em vista a conclusão do
curso  de  Doutorado  em  Psiquiatria  e  Ciências  do  Comportamento,  conforme  o  Processo  nº
23078.524424/2017-13.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
