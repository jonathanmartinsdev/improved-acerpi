Documento gerado sob autenticação Nº KJH.426.446.95E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8664                  de  24/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  LUCIANO  ZUBARAN  GOLDANI,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a
finalidade de participar da "IDWeek annual meeting 2019", em Washington, D.C., Estados Unidos, no período
compreendido entre 01/10/2019 e 07/10/2019, incluído trânsito, com ônus limitado. Solicitação nº 86269.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
