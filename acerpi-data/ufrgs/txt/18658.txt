Documento gerado sob autenticação Nº POQ.407.336.VIO, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1642                  de  04/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de LUCIANA PORCHER NEDEL, Professor do Magistério Superior,
lotada e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a finalidade
de participar do "VR 2016 IEEE Virtual Reality", em Greenville, Estados Unidos, no período compreendido
entre 17/03/2016 e 23/03/2016,  incluído trânsito,  com ônus UFRGS (Pró-Reitoria  de Pesquisa -  diárias).
Solicitação nº 18230.
CARLOS ALEXANDRE NETTO
Reitor
