Documento gerado sob autenticação Nº BOY.486.084.CLR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5849                  de  04/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  ao  servidor  LUIS  FERNANDO
GONCALVES  NUNES,  ocupante  do  cargo  de  Auxiliar  de  Marcenaria-701811,  lotado  no  Instituto  de
Matemática e Estatística, SIAPE 0358353, para 25% (vinte e cinco por cento), a contar de 30/06/2017, tendo
em vista a conclusão do curso de Graduação em Tecnologia em Gestão Ambiental, conforme o Processo nº
23078.201159/2017-06.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
