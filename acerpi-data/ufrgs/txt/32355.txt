Documento gerado sob autenticação Nº TID.943.170.8GM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             88                  de  04/01/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear o ocupante do cargo de Professor do Magistério Superior do Quadro de Pessoal desta
Universidade, MARCELO SOARES LUBASZEWSKI, matrícula SIAPE 0357201, para exercer o cargo de Diretor
do Parque Científico e Tecnológico da UFRGS, Código SRH 1070, código CD-4, com vigência a partir da data de
publicação no Diário Oficial da União, pelo período de 4 anos. Processo nº 23078.000148/2017-01.
RUI VICENTE OPPERMANN
Reitor
