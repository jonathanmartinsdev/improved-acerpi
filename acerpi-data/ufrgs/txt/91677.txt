Documento gerado sob autenticação Nº YDX.714.554.SEQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4431                  de  22/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Exonerar, a pedido, a partir da data de publicação no Diário Oficial da União, GUILHERME MARTINS
SIQUEIRA,  do  cargo  de  Diretor  do  Departamento  de  Projetos  e  Planejamento  vinculado  à  Vice-
Superintendência de Infraestrutura da SUINFRA, Código SRH 63, Código CD-4, da Universidade Federal do Rio
Grande do Sul, para o qual foi nomeado pela Portaria nº 1707/2018, de 28/02/2018, publicada no Diário
Oficial da União de 05/03/2018. Processo nº 23078.513093/2019-58.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
