Documento gerado sob autenticação Nº YZA.964.946.T2H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10741                  de  02/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°58663,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de VIGILANTE, do Quadro de
Pessoal desta Universidade, LUIZ VANDERLEI FURTADO JORIS (Siape: 0408453 ),  para substituir   JONACIR
FERNANDES ROLIM (Siape: 6355462 ), Chefe do Setor de Vigilância do Campus do Vale SP3 da Coordenadoria
de Segurança, Código FG-2, em seu afastamento por motivo de Laudo Médico do titular da Função, no
período de 01/11/2019 a 23/12/2019, com o decorrente pagamento das vantagens por 53 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
