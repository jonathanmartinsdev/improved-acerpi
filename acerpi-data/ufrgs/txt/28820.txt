Documento gerado sob autenticação Nº KLK.647.136.VG6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7997                  de  07/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  90 (noventa) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de
11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
BARBARA PILATTI PIFFER, com exercício na Biblioteca da Faculdade de Medicina, a ser usufruída no período
de 17/10/2016 a  17/11/2016 e  de 1º/02/2017 a  30/03/2017,  referente  ao quinquenio  de 26/09/2011 a
25/09/2016,  a  fim de participar  do curso de Mestrado em Memória  Social  e  Bens Culturais,  conforme
Processo nº 23078.020510/2016-71.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
