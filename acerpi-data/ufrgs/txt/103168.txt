Documento gerado sob autenticação Nº TMP.000.357.G88, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             222                  de  06/01/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar  a  prorrogação  de  afastamento  no  País  de  EVELIN  CUNHA  BIONDO,  Professor  do
Magistério  do  Ensino  Básico,  Técnico  e  Tecnológico,  lotada  e  com  exercício  no  Departamento  de
Humanidades do Colégio de Aplicação, com a finalidade de concluir os estudos em nível de Doutorado junto
à  Universidade  Federal  do  Rio  Grande  do  Sul,  em  Porto  Alegre/RS,  no  período  compreendido  entre
08/01/2020 e 21/09/2020, com ônus limitado. Processo nº 23078.520355/2017-79.
JANE FRAGA TUTIKIAN
Vice-Reitora.
