Documento gerado sob autenticação Nº JRL.418.020.T9H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6986                  de  02/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/07/2019,   referente  ao  interstício  de
26/01/2018  a  25/07/2019,  para  o  servidor  MARIO  INACIO  NEIS,  ocupante  do  cargo  de  Técnico  de
Tecnologia da Informação - 701226, matrícula SIAPE 2191182,  lotado  no  Setor de Informática da FABICO,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.514608/2019-37:
Fundação Bradesco - Fundamentos de Governança de TI CH: 29 (04/10/2016 a 03/11/2016)
Fundação Bradesco - Web Design CH: 35 (06/06/2019 a 06/06/2019)
BRADESCO - Introdução ao Javascript CH: 45 (14/11/2017 a 14/11/2017)
Fundação Bradesco - Introdução ao Javascript CH: 28 (14/11/2017 a 14/11/2017)
Bradesco - Linguagem de Programação Java - Básico CH: 24 Carga horária utilizada: 13 hora(s) / Carga horária
excedente: 11 hora(s) (24/07/2019 a 26/07/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
