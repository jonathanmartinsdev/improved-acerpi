Documento gerado sob autenticação Nº HBP.750.640.7HC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4290                  de  17/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/05/2017,   referente  ao  interstício  de
16/11/2015 a  15/05/2017,  para  a  servidora MARCIA HELENA CARVALHO BOM,  ocupante do cargo de
Administrador - 701001, matrícula SIAPE 1782768,  lotada  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.007021/2017-12:
Formação Integral de Servidores da UFRGS III CH: 79 (16/07/2014 a 28/05/2015)
SOF - Orçamento Público CH: 60 (14/04/2015 a 18/05/2015)
Unlock Training - UNLOCK PROGRAM - Destrave seu potencial de liderar CH: 20 Carga horária utilizada: 11
hora(s) / Carga horária excedente: 9 hora(s) (18/11/2016 a 20/11/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
