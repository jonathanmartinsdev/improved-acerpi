Documento gerado sob autenticação Nº FYS.695.224.6JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8648                  de  14/09/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANGELA FOERSTER, Professor do Magistério Superior, lotada e
em  exercício  no  Departamento  de  Física  do  Instituto  de  Física,  com  a  finalidade  de  participar  do
"International Workshop on Critical stabily of quantum few-body systems", em Dresden, Alemanha, no período
compreendido entre 14/10/2017 e 20/10/2017, incluído trânsito, com ônus limitado. Solicitação nº 30548.
RUI VICENTE OPPERMANN
Reitor
