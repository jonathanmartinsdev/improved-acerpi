Documento gerado sob autenticação Nº HRQ.065.976.KE3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6165                  de  12/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  10/07/2017,   referente  ao  interstício  de
04/09/2015 a 09/07/2017, para a servidora CLENIA ELENA GONCALVES DOS SANTOS, ocupante do cargo de
Administrador  -  701001,  matrícula  SIAPE  0356680,   lotada   na   Pró-Reitoria  de  Planejamento  e
Administração,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  E  III,  para  o  Nível  de
Classificação/Nível de Capacitação E IV, em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.013270/2017-39:
Formação Integral de Servidores da UFRGS I CH: 22 (02/06/2014 a 25/11/2015)
ESAF - Gestão de convênios: da solicitação à tomada de contas especiais, na visão do concedente federal CH:
30 (11/11/2014 a 14/11/2014)
CASP- Contabilidade Aplicada ao Setor Público CH: 24 (16/12/2014 a 18/12/2014)
ACELE - Espanhol 1 CH: 44 (10/08/2015 a 17/12/2015)
ACELE - Espanhol 2 CH: 44 (07/03/2016 a 11/07/2016)
ACELE  -  Espanhol  3  CH:  44  Carga  horária  utilizada:  16  hora(s)  /  Carga  horária  excedente:  28  hora(s)
(08/08/2016 a 12/12/2016)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
