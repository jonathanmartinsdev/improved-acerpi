Documento gerado sob autenticação Nº ITY.442.969.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8818                  de  27/09/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a PAULO RICARDO MIRANDA
MOREIRA, matrícula SIAPE nº 0357718, no cargo de Motorista, nível de classificação C, nível de capacitação IV,
padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de trabalho, com exercício
na  Divisão  de  Transporte  da  Pró-Reitoria  de  Planejamento  e  Administração,  com  proventos  integrais.
Processo 23078.521191/2019-69.
RUI VICENTE OPPERMANN
Reitor.
