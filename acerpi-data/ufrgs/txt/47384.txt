Documento gerado sob autenticação Nº ROK.153.572.JII, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10614                  de  21/11/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°35083,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ANALISTA DE TECNOLOGIA DA
INFORMAÇÃO, do Quadro de Pessoal desta Universidade, MARCIO POHLMANN (Siape: 1678100 ),  para
substituir   JERONIMO SOARES DE CASTRO MENEZES (Siape: 1734158 ), Chefe da Divisão de Engenharia de
Redes do Departamento de Infraestrutura de TI do CPD, Código FG-5, em seu afastamento por motivo de
férias, no período de 27/11/2017 a 09/12/2017, com o decorrente pagamento das vantagens por 13 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
