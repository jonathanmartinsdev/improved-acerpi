Documento gerado sob autenticação Nº IEB.718.888.QJH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8693                  de  26/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  17/10/2016,   referente  ao  interstício  de
13/04/2015 a 16/10/2016, para a servidora ALINE DE FREITAS VISENTINI, ocupante do cargo de Assistente
em Administração - 701200, matrícula SIAPE 2052377,  lotada  na  Faculdade de Direito, passando do Nível
de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.023248/2016-16:
CETEC - Curso de Atualização de Noções de Legislação, Português e Raciocínio Lógico CH: 133 Carga horária
utilizada: 120 hora(s) / Carga horária excedente: 13 hora(s) (12/05/2014 a 27/09/2014)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
