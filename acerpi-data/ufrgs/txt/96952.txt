Documento gerado sob autenticação Nº KHR.672.440.QF7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7628                  de  23/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de
2016, do Magnífico Reitor, e conforme a Solicitação de Afastamento n°86439,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, LIVIA KMETZSCH ROSA E SILVA (Siape: 2095352 ),
 para substituir   DIEGO BONATTO (Siape: 1766403 ), Chefe do Depto de Biologia Molecular e Biotecnologia
do  Instituto  de  Biociências,  Código  FG-1,  em  seu  afastamento  no  país,  no  período  de  23/08/2019  a
24/08/2019, com o decorrente pagamento das vantagens por 2 dias.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas
