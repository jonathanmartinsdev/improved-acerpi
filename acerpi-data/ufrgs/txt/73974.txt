Documento gerado sob autenticação Nº HOQ.119.591.FOP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10428                  de  26/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  ROBERTA  CRUZ  SILVEIRA  THYS,  matrícula  SIAPE  n°  2442512,  lotada  no
Departamento de Tecnologia dos Alimentos do Instituto de Ciências e Tecnologia de Alimentos, para exercer
a função de Coordenadora da COMGRAD de Engenharia de Alimentos, Código SRH 1213, código FUC, com
vigência a partir de 09/02/2019 até 08/02/2021. Processo nº 23078.535383/2018-71.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
