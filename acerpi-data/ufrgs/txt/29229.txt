Documento gerado sob autenticação Nº HIG.306.227.0B1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8271                  de  17/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor PEDRO RAFAEL BOLOGNESE FERNANDES, matrícula SIAPE n° 1769707, lotado e em exercício no
Departamento de Engenharia Química da Escola de Engenharia, da classe C  de Professor Adjunto, nível 02,
para a classe C  de Professor Adjunto, nível 03, referente ao interstício de 01/03/2012 a 31/07/2014, com
vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro
de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.504740/2016-98.
JANE FRAGA TUTIKIAN
Vice-Reitora
