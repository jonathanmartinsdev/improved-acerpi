Documento gerado sob autenticação Nº VYB.524.019.6JP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1521                  de  12/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°45951,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, EDUARDO ROLIM DE OLIVEIRA (Siape: 1227681 ),
 para substituir   SILVIO LUIS PEREIRA DIAS (Siape: 2195035 ), Diretor do Centro de Gestão e Tratamento de
Resíduos Químicos do Instituto de Química, Código FG-1, em seu afastamento por motivo de férias, no
período de 13/02/2019 a 27/02/2019, com o decorrente pagamento das vantagens por 15 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
