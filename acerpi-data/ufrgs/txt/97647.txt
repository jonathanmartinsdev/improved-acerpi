Documento gerado sob autenticação Nº IXN.177.609.2Q5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8069                  de  04/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  02/09/2019,   referente  ao  interstício  de
04/08/2010 a 01/09/2019, para o servidor RAFAEL MATOS GRIGOLO, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 1682868,  lotado  na  Procuradoria Geral, passando do Nível de
Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.523877/2019-94:
Formação Integral de Servidores da UFRGS I CH: 22 (07/04/2010 a 21/05/2019)
ESAF - Gestão de convênios: da solicitação à tomada de contas especiais, na visão do concedente federal CH:
30 (11/11/2014 a 14/11/2014)
ENAP - Gestão e Fiscalização de Contratos Administrativos CH: 40 (02/09/2019 a 02/09/2019)
ENAP - Noções Introdutórias de Licitação e Contratos Administrativos CH: 30 Carga horária utilizada: 28
hora(s) / Carga horária excedente: 2 hora(s) (30/08/2019 a 02/09/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
