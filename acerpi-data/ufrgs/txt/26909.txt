Documento gerado sob autenticação Nº DLA.895.675.OO8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6771                  de  01/09/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Designar JULIANA MACIEL DE SOUZA, Matrícula SIAPE 1756535, ocupante do cargo de Técnico em
Assuntos Educacionais, Código 701079, do Quadro de Pessoal desta Universidade, para exercer a função de
Coordenadora do Núcleo Acadêmico da Gerência Administrativa da Faculdade de Odontologia, Código SRH
624,  código FG-7,  com vigência a partir  da data de publicação no Diário Oficial  da União.  Processo nº
23078.019197/2016-28.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
