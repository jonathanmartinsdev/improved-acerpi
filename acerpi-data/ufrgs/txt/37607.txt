Documento gerado sob autenticação Nº OSQ.659.177.L64, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3768                  de  03/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  IRACI  LUCENA  DA  SILVA  TORRES,  matrícula  SIAPE  n°  1545035,  lotada  e  em  exercício  no
Departamento  de  Farmacologia  do  Instituto  de  Ciências  Básicas  da  Saúde,  da  classe  D   de  Professor
Associado, nível 01, para a classe D  de Professor Associado, nível 02, referente ao interstício de 01/08/2014 a
31/07/2016, com vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de
28 de dezembro de 2012, com suas alterações e a Decisão nº 197/2006-CONSUN, alterada pela Decisão nº
401/2013-CONSUN. Processo nº 23078.515233/2016-80.
JANE FRAGA TUTIKIAN
Vice-Reitora.
