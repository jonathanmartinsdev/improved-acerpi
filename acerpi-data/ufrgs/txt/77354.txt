Documento gerado sob autenticação Nº CGB.731.380.H96, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 311/2019-PROGESP Porto Alegre, 25 de fevereiro de 2019.
Senhor  Diretor
Encaminhamos o servidor Douglas Santos Bregolin, ocupante do cargo de Técnico em Tecnologia
da Informação/ Sistemas de Informação, para exercício nessa Unidade no dia 25 de fevereiro de 2019.
Solicitamos que nos sejam encaminhadas, no prazo máximo de 03 dias, as seguintes informações:
- atividades a serem desenvolvidas pelo servidor;
- local de exercício (divisão, setor), observando a hierarquia dos órgãos registrada no SRH;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal.
Informamos que este encaminhamento se faz para reposição de vaga, em virtude de vacância da
servidora ELIANE SANGUINE DA SILVA.
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
Ilmo. Sr.
Professor MARCELO ANTONIO CONTERATO,
Diretor do Centro Interdisciplinar em Sociedade Ambiente e Desenvolvimento - CISADE,
N/Universidade.
