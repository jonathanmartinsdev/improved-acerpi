Documento gerado sob autenticação Nº VNE.953.908.35O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9210                  de  18/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°30913,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, PAULO DORNELLES PICON (Siape: 0356802 ),  para
substituir    LUCIANO  ZUBARAN  GOLDANI  (Siape:  1217171  ),  Chefe  do  Depto  de  Medicina  Interna  da
Faculdade de Medicina, Código FG-1, em seu afastamento por motivo de férias, no período de 17/11/2016 a
23/11/2016, com o decorrente pagamento das vantagens por 7 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
