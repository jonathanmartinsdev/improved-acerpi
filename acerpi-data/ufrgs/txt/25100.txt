Documento gerado sob autenticação Nº SQR.076.401.MGB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5605                  de  25/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Declarar vago, a partir de 30 de junho de 2016, o cargo de Técnico em Artes Gráficas, Código
701217, Nível de Classificação D, Nível de Capacitação IV, Padrão 05, do Quadro de Pessoal, em decorrência
de posse em outro cargo inacumulável, de OBERTI DO AMARAL RUSCHEL, com lotação na Secretaria de
Comunicação Social. Processo nº 23078.012854/2016-14.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
