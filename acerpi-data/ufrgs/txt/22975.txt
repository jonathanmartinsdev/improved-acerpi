Documento gerado sob autenticação Nº MDD.616.276.C5U, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4105                  de  06/06/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, MARLOS GONÇALVES SOUSA, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 98/2014, de 26 de junho de 2014,  homologado em 30 de
junho de 2014, e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei
12.772,  de 28 de dezembro de 2012,  com redação dada pela Lei  12.863,  de 24 de setembro de 2013,
publicada no Diário Oficial da União de 25 de setembro de 2013, para o cargo de Professor do Plano de
Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro de Pessoal
desta Universidade, em regime de trabalho de Dedicação Exclusiva, junto ao Departamento de Medicina
Animal da Faculdade de Veterinária, em vaga decorrente da aposentadoria da professora Suzette Bassan
Schmitz, código 275104, conforme Portaria nº 3661, de 17 de maio de 2016, publicada no Diário Oficial da
União de 18 de maio de 2016. Processo nº. 23078.028879/13-53.
CARLOS ALEXANDRE NETTO
Reitor
