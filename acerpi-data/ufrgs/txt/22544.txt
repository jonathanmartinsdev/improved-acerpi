Documento gerado sob autenticação Nº XHH.886.573.UE3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3809                  de  23/05/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Remover, a partir de 19 de maio de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
DAMIAN STEPPACHER, ocupante do cargo de Administrador, Ambiente Organizacional Administrativo, Código
701001, Classe E, Nível de Capacitação IV, Padrão de Vencimento 04, SIAPE nº. 1872343 da Pró-Reitoria de
Gestão de Pessoas para a lotação Escola de Engenharia, com novo exercício na Secretaria da Escola de
Engenharia.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
