Documento gerado sob autenticação Nº ZIR.677.006.C0A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8537                  de  12/09/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  VANESSA  BIELEFELDT  LEOTTI,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Estatística do Instituto de Matemática e Estatística, com
a finalidade de participar de reunião junto à University of Groningen, Holanda, no período compreendido
entre 12/10/2017 e 22/10/2017, incluído trânsito, com ônus pelo Projeto CAPES/NUFFIC 061/15. Solicitação nº
28675.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
