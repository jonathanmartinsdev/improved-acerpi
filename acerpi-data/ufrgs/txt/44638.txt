Documento gerado sob autenticação Nº QFY.773.349.7IP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8660                  de  15/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  01/09/2017,   referente  ao  interstício  de
14/01/2016 a 31/08/2017, para a servidora FLAVIA WAGNER, ocupante do cargo de Psicólogo-área - 701060,
matrícula SIAPE 2140023,  lotada  no  Instituto de Psicologia, passando do Nível de Classificação/Nível de
Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude de ter realizado o(s)
seguinte(s) curso(s), conforme o Processo nº 23078.514727/2017-28:
Formação Integral de Servidores da UFRGS VI CH: 176 Carga horária utilizada: 150 hora(s) / Carga horária
excedente: 26 hora(s) (29/07/2015 a 31/08/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
