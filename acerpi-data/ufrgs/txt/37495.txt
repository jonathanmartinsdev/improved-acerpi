Documento gerado sob autenticação Nº XYQ.709.494.VE7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3719                  de  02/05/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora DANIELA MENDES CIDADE, matrícula SIAPE n° 3290172, lotada e em exercício no Departamento
de Arquitetura da Faculdade de Arquitetura, da classe C  de Professor Adjunto, nível 03, para a classe C  de
Professor Adjunto, nível 04, referente ao interstício de 17/12/2014 a 16/12/2016, com vigência financeira a
partir de 17/12/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações  e  a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.513798/2016-22.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
