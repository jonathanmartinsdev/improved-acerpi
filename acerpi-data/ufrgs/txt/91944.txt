Documento gerado sob autenticação Nº DTW.165.036.GCI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4617                  de  28/05/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a MIGUEL MACHADO DIAS,
matrícula  SIAPE  nº  0356294,  no  cargo  de  Técnico  em  Restauração,  nível  de  classificação  D,  nível  de
capacitação IV,  padrão 16,  do Quadro desta  Universidade,  no regime de quarenta  horas  semanais  de
trabalho,  com  exercício  na  Biblioteca  da  Escola  de  Administração,  com  proventos  integrais.  Processo
23078.502964/2019-16.
RUI VICENTE OPPERMANN
Reitor.
