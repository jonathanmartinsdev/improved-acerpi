Documento gerado sob autenticação Nº CBD.089.549.948, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1318                  de  06/02/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°48330,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, ROSEMAR GONCALVES (Siape: 0357312 ),  para
substituir    DANIELA  BORGES  PAVANI  (Siape:  1574520  ),  Diretor  do  Planetário,  Código  CD-4,  em  seu
afastamento por motivo de férias, no período de 06/02/2019 a 01/03/2019, com o decorrente pagamento das
vantagens por 24 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
