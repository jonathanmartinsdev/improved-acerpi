Documento gerado sob autenticação Nº HNT.207.597.MSD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8483                  de  11/09/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016e, tendo em vista o que consta do Processo Administrativo n° 23078.008626/2016-31, da Lei 10.520/02  ,
do Contrato nº 116/PROPLAN/NUDECON/2016 e da Lei 8.666/93,
RESOLVE:
 
Aplicar  a  sanção administrativa  de  ADVERTÊNCIA,  prevista  na  alínea  "a",  Cláusula  Décima do referido
Contrato, à Empresa KR CONFORTO AMBIENTAL LTDA, CNPJ 22.473.357/0001-20, pela Irregularidade do
cadastro do SICAF, conforme atestado pela PROREXT à fl. 194-verso, bem como pelo NUDECON fl. 209 do
processo administrativo supracitado.
 
 Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
