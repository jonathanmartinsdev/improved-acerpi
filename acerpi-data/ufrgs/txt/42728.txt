Documento gerado sob autenticação Nº TTL.358.942.UJM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7453                  de  11/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  14/07/2017,   referente  ao  interstício  de
03/11/2015 a 13/07/2017,  para a servidora VIVIANE CORDOVA JARDIM VOTTO,  ocupante do cargo de
Auxiliar de Enfermagem - 701411, matrícula SIAPE 1752020,  lotada  no  Ambulatório do Campus do Vale da
Pró-Reitoria de Gestão de Pessoas, passando do Nível de Classificação/Nível de Capacitação C III, para o Nível
de Classificação/Nível de Capacitação C IV, em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.201903/2017-64:
CONHECER - Biossegurança CH: 140 Carga horária utilizada: 120 hora(s) / Carga horária excedente: 20 hora(s)
(02/03/2017 a 04/05/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
