Documento gerado sob autenticação Nº IVH.652.483.VCT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4157                  de  08/06/2018
Nomeação  da  Representação  Discente  do
Centro  Acadêmico  Bruno  Kiefer  dos
Estudantes  do  Curso  de  Artes  da  UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear  a  Representação  Discente  eleita  para  compor  o  Centro  Acadêmico  Bruno Kiefer  dos
Estudantes do Curso de Artes, com mandato de 01 (um) ano, a contar de 05 de março de 2018, atendendo ao
disposto nos artigos 175 e 178 do Regimento Geral da Universidade e 79 do Estatuto da Universidade e
considerando o processo nº 23078.507188/2018-51, conforme segue:
 
Presidente: Madalena Rasslan Fischer
Vice-Presidente: Leonardo Gabriel Dos Santos Vitorino
2o Vice-presidente: Ana Maria Ribeiro Althoff
Secretária Geral: Giulia Mayumi Cantelli Tadei Nakata
Vice-Secretário: Maurício Cesar Gomes da Luz
Tesoureira Geral: Victoria Cristina da Silva Eduardo
Vice-tesoureiro: Ciro Aranha Gomes
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
