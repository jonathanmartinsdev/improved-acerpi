Documento gerado sob autenticação Nº TUL.382.471.LME, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10795                  de  03/12/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de NICOLAS BRUNO MAILLARD, Professor do Magistério Superior,
lotado no Departamento de Informática Aplicada do Instituto de Informática e com exercício na Secretaria de
Relações Internacionais, com a finalidade de participar de reunião junto ao Confucius Institute Headquarters,
em Beijing, China, no período compreendido entre 08/12/2019 e 14/12/2019, incluído trânsito, com ônus
UFRGS (Secretaria de Relações Internacionais: diárias). Solicitação nº 89153.
RUI VICENTE OPPERMANN
Reitor
