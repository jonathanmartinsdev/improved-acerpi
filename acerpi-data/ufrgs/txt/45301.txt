Documento gerado sob autenticação Nº WIO.632.394.SFO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9119                  de  29/09/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ALEXANDRE FAVERO BULGARELLI, Professor do Magistério
Superior,  lotado e em exercício no Departamento de Odontologia Preventiva e Social  da Faculdade de
Odontologia, com a finalidade de participar da "23rd Annual Qualitative Health Research Conference", em
Quebec City e realizar visita ao McGill Qualitative Health Research Group, em Montreal, Canadá, no período
compreendido entre 10/10/2017 e 19/10/2017, incluído trânsito, com ônus limitado. Solicitação nº 31188.
RUI VICENTE OPPERMANN
Reitor
