Documento gerado sob autenticação Nº VPK.018.699.MMB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3021                  de  05/04/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora JULIANA DE LIMA VIEIRA MACHADO, ocupante do cargo de  Técnico em
Contabilidade - 701224, lotada na Superintendência de Infraestrutura, SIAPE 2156700, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 30/03/2019, tendo em vista a conclusão do
curso de Bacharelado em Ciências Contábeis, conforme o Processo nº 23078.507855/2019-87.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
