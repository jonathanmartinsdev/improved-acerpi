Documento gerado sob autenticação Nº LBH.576.985.2IU, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4531                  de  24/05/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°83309,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, LUCIANA INES GOMES MIRON (Siape: 1523899 ),  para
substituir    CLARICE  MARASCHIN (Siape:  1812699 ),  Coordenador  do  PPG em Planejamento  Urbano e
Regional,  Código  FUC,  em  seu  afastamento  no  país,  no  período  de  26/05/2019  a  31/05/2019,  com  o
decorrente pagamento das vantagens por 6 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
