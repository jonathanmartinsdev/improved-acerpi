Documento gerado sob autenticação Nº HLC.979.879.ER2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3334                  de  17/04/2019
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 2646, de 25 de março de 2019
RESOLVE
Designar os servidores,  EDINEI RUDIMAR RODRIGUES VARGAS, ocupante do cargo de Engenheiro-
área, lotado na Superintendência de Infraestrutura e com exercício no Setor de Fiscalização e Adequação,
RAQUEL BUTTOW NUNES DIAS,  ocupante do cargo de Engenheiro-área, lotada na Superintendência de
Infraestrutura  e  com  exercício  na  Prefeitura  Campus  Centro,  CLAUDIA  MARA  ESCOVAR  ALFARO
BOETTCHER,  ocupante do cargo de Técnico em Secretariado, lotada na Pró-Reitoria de Extensão e com
exercício no Departamento de Difusão Cultural, para sob a presidência do primeiro, constituirem a Comissão
de Recebimento, em atendimento à alínea "b" do inciso I do artigo 73 da Lei 8666/93, para emissão do Termo
de Recebimento Definitivo da Obra do contrato 063/2015, cujo objeto é "Restauração do prédio do antigo
Instituto de Química Industrial-Prédio nº 12.109-Campus Centro da UFRGS",  com prazo de 30 dias.
Processo nº 23078.019328/2014-13.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
CAMILA SIMONETTI
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
