Documento gerado sob autenticação Nº LFM.676.489.KK2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5355                  de  25/06/2019
Nomeação  da  Representação  Discente  do
Câmara de Graduação da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita para compor o Câmara de Graduação, com mandato de 01
(um)  ano,  a  contar  da  data  de  publicação  desta  portaria,  atendendo ao  disposto  nos  artigos  175  do
Regimento  Geral  da  Universidade  e  79  do  Estatuto  da  Universidade  e  considerando  o  processo
nº 23078.516146/2019-92, conforme segue:
 
Câmara de Graduação - CAMGRAD
 
1º Titular: Lucas Tarragô Carvalho Neumann
1º Suplente: Samantha Paz Silveira
2º Titular: Luiza de Campos Morais Ramos
2º Suplente: Pedro Vellinho Corso Duval
3º Titular: Eliana Ribeiro de Freitas
3º Suplente: Ítalo Ariel Pereira Guerreiro
4º Titular: Alícia Froener
4º Suplente: Brunno Mattos da Silva
5º Titular: Franciele Rodrigues da Silva
5º Suplente: Stéphanie Venske Estrella
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
