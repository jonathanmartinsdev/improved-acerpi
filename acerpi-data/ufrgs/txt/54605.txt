Documento gerado sob autenticação Nº VFX.635.525.Q98, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3773                  de  23/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ITAMAR CRISTIANO NAVA, matrícula SIAPE n° 1943861, lotado e em exercício no Departamento
de Plantas de Lavoura da Faculdade de Agronomia, da classe C  de Professor Adjunto, nível 03, para a classe
C   de  Professor  Adjunto,  nível  04,  referente  ao  interstício  de  14/05/2016  a  13/05/2018,  com vigência
financeira a partir de 14/05/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.507972/2018-60.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
