Documento gerado sob autenticação Nº YTU.380.536.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2137                  de  08/03/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a CRISTINA AMELIA PEREIRA
DE CARVALHO,  matrícula  SIAPE nº  0388145,  no cargo de Professor  Associado,  nível  4,  da Carreira  do
Magistério Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no
Departamento de Ciências Administrativas da Escola de Administração, com proventos integrais. Processo
23078.533045/2018-03.
RUI VICENTE OPPERMANN
Reitor.
