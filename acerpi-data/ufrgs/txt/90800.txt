Documento gerado sob autenticação Nº MDA.879.717.J8D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3870                  de  07/05/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar na Faculdade de Biblioteconomia e Comunicação,  com exercício no Centro Integrado de
Comunicação, Ambiente Organizacional Artes, Comunicação e Difusão, CARLA COSTA BOMFIM, nomeada
conforme Portaria Nº 2687/2019 de 27 de março de 2019, publicada no Diário Oficial da União no dia 28 de
março de 2019, em efetivo exercício desde 29 de abril de 2019, ocupante do cargo de RELAÇÕES PÚBLICAS,
classe E, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
