Documento gerado sob autenticação Nº PNV.337.939.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7638                  de  24/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLEO SCHMITT SILVEIRA, Professor do Magistério Superior,
lotada e em exercício no Departamento Interdisciplinar do Campus Litoral Norte, com a finalidade de realizar
visita à Communication University of China, em Beijing, China, no período compreendido entre 10/10/2018 e
22/10/2018, incluído trânsito, com ônus limitado. Solicitação nº 58993.
RUI VICENTE OPPERMANN
Reitor
