Documento gerado sob autenticação Nº WSQ.407.033.NHE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4554                  de  24/05/2019
  Altera a Portaria nº 3587 de 11 de maio de
2015 ,  que  des ignou  o  Comitê  de
Segurança da Informação (CSI) da UFRGS.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Art. 1º Alterar a Portaria nº 3587 de 11 de maio de 2015, que designou o Comitê de Segurança da
Informação (CSI) da UFRGS, considerando os artigos 20 e 21 da Decisão nª 124, de 28 de março de 2014, do
Conselho Universitário - CONSUN, que passa a vigorar com os seguintes membros:
      ADRIANO LEONARDO ROSSI
      ALEXANDRE DA SILVA CARISSIMI
      ANDREA FACHEL LEAL
      ARTHUR BOOS JUNIOR
      DANIEL AUGUSTO PEREIRA
      LUIS ALBERTO SEGOVIA GONZÁLEZ
      LUIS FERNANDO NUNES FERNANDEZ
      MARCELO SOARES MACHADO
      MARILIA CANABARRO ZORDAN
      MEDIANEIRA APARECIDA PEREIRA GOULART
      ROBERTO NUNES UMPIERRE
      RUTE VERA MARIA FAVERO
      VALERIA RAQUEL BERTOTTI
      WEVERTON LUIS DA COSTA CORDEIRO
 
                  Art. 2º Revogar a Portaria nº 3587 de 11 de maio de 2015.
RUI VICENTE OPPERMANN,
Reitor.
