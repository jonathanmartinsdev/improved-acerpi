Documento gerado sob autenticação Nº NKK.876.097.489, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2062                  de  07/03/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°28537,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ROSANE AZEVEDO NEVES DA SILVA (Siape: 1354635 ),
 para substituir   ANALICE DE LIMA PALOMBINI (Siape: 2215636 ), Coordenador do PPG em Psicologia Social e
Institucional, Código FUC, em seu afastamento por motivo de férias, no período de 16/02/2017 a 19/02/2017,
com o decorrente pagamento das vantagens por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
