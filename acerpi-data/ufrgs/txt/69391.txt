Documento gerado sob autenticação Nº JRE.933.563.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7137                  de  10/09/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5040858-49.2018.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, da servidora ICELVA MARIANO DO PRADO, matrícula SIAPE n° 0356098, aposentada no cargo
de  Assistente em Administração - 701200, para o nível IV, conforme o Processo nº 23078.523258/2018-19.
RUI VICENTE OPPERMANN
Reitor
