Documento gerado sob autenticação Nº TAM.321.023.436, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/3
PORTARIA Nº             802                  de  23/01/2019
  Horário de funcionamento da UFRGS no
período compreendido entre 28/01/2019 e
1º/03/2019.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias, e considerando:
I - Que a UFRGS tem tido forte queda nas suas dotações orçamentárias para o custeio das atividades de
ensino, pesquisa e extensão nos últimos anos, sendo que o orçamento aprovado na Lei Orçamentária Anual
(LOA) 2019, proveniente dos Recursos do Tesouro Nacional, mantém a mesma dotação de 2018, e que vários
preços e tarifas de bens e serviços têm tido aumentos, especialmente a energia elétrica, cuja tarifa em Porto
Alegre já sofrera aumento de 30% no mês de janeiro de 2018;
 
II - Que a despesa de energia elétrica na UFRGS atingiu R$ 24 milhões em 2018, e que o aumento da tarifa,
em janeiro de 2019, cresceu 5% em relação a janeiro de 2018, tendo um impacto potencial de aumento de
despesas de custeio;
 
III - Que a Administração Central da UFRGS está desenvolvendo iniciativas para  modificar estruturalmente o
consumo de energia,  e que tais medidas têm efeito de médio a longo prazo, além de dependerem de
alocação de recursos em dotação de capital, e tendo em vista que a LOA 2019  manteve-se no patamar da
dotação de 2018;
 
IV - Que a restrição orçamentária enfrentada pela UFRGS somente pode ser atenuada com a redução de
despesas discricionárias, abrangendo despesas de custeio operacional e capital;
 
V  -  Que  há  uma  sazonalidade  importante  nas  demandas  de  serviços  internos  e  externos  na  UFRGS,
especialmente no mês de fevereiro, quando ocorre uma significativa redução das atividades acadêmicas
devido ao encerramento do período letivo;
 
VI  -  Que  é  fundamental  a  busca  de  alternativas  para  a  manutenção  da  disponibilidade  de  recursos
orçamentários  de  custeio  das  atividades  finalísticas  (manutenção  de  laboratórios  de  graduação  e  de
pesquisa, atualização do acervo bibliográfico, entre outros);
 
VII  -  Que  a  dotação  de  despesas  de  capital  está  em  nível  crítico,  não  sendo  mais  possível  fazer
remanejamentos de dotação de capital para custeio;
 
VIII - Que foram reduzidos postos de trabalhos em relação à  prestação de serviços terceirizados em diversas
áreas da Universidade, sendo a redução de 14,74% em 2017 e 12,17% em 2018;
 
IX  -  Que  a  Superintendência  de  Infraestrutura  da  UFRGS realizou  medições  e  estudos  que  indicam a
possibilidade de redução de cerca de 25% na despesa de energia elétrica;
Documento gerado sob autenticação Nº TAM.321.023.436, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/3
 
X - Que é necessário avaliar, com mais precisão, o impacto dessas medidas sobre os custos totais (incluindo
energia elétrica, telecomunicações, água, entre outros), permitindo diagnosticar melhor as relações entre a
sazonalidade das atividades, o horário de funcionamento e a economicidade na gestão da Universidade, para
embasar futuras decisões no caso de persistir a escassez drástica de recursos orçamentários;
 
XI - Que há registro de implantação de medida similar em diversas Instituições Públicas de todos os poderes
em âmbito nacional;
 
XII - Que foi editada a Instrução Normativa nº 1, de 31/08/2018, pela Secretaria de Gestão de Pessoas do
então Ministério do Planejamento, Desenvolvimento e Gestão, que trata da implementação do Programa de
Gestão que possibilita o trabalho semi-presencial; e
 
XIII - Que esta medida fundamenta-se nos princípios da economicidade e eficiência na gestão pública,
RESOLVE:
Art. 1º Estabelecer, de forma excepcional, exclusivamente no período compreendido entre 28/01/2019 e
1º/03/2019, o horário de funcionamento da Universidade Federal do Rio Grande do Sul, nos termos do inciso
II do art. 9º da Instrução Normativa nº 1, de 31/08/2018, da Secretaria de Gestão de Pessoas do então
Ministério do Planejamento, Desenvolvimento e Gestão.
 
Art. 2º Durante esse período, o horário de funcionamento da UFRGS será de segunda-feira  a  sexta-feira das
7h30m às 13h30m, ininterruptamente.
 
Art. 3º Os servidores com jornada de trabalho flexibilizada de seis horas diárias e carga horária de 30 horas
semanais terão suas jornadas suspensas durante a vigência desta Portaria.
 
Art. 4º Os servidores ocupantes de cargos com jornadas semanais de trabalho estabelecidas em legislação
específica,  bem  como  aqueles  com  jornada  de  trabalho  reduzida  deverão  observar  o  horário  de
funcionamento da UFRGS estabelecido nesta Portaria.
 
Art. 5º O disposto nesta Portaria não se aplica às atividades consideradas essenciais para o funcionamento
da Universidade, a critério das Direções das Unidades e dos Órgãos da Administração Central, sejam de
caráter permanente, temporário ou eventual, particularmente as atividades relacionadas ao processo de
matrícula dos estudantes.
 
Art. 6º As Unidades e os Órgãos da Administração Central deverão racionalizar suas atividades, de forma que
a alteração do horário do expediente não venha a causar prejuízos à comunidade.
 
Art. 7º A critério da chefia imediata ou da autoridade superior, os servidores poderão, havendo interesse
premente ou necessidade de serviço, ser convocados para comparecer ou realizar atividades em horário de
expediente normal.
 
Art.  8º As atividades/horários deverão ser compensadas, excepcionalmente, consoante a modalidade de
trabalho semi-presencial e/ou mediante reposição de atividades, observando as atribuições dos respectivos
cargos.
 
Art. 9º Os contratos de prestação de serviços continuados, com dedicação exclusiva de mão de obra, deverão
Documento gerado sob autenticação Nº TAM.321.023.436, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
3/3
ser ajustados aos termos desta portaria, especialmente para que haja previsão de compensação de horas
pelos prestadores, na hipótese de jornada superior a seis horas.
 
Art.10. O Centro de Processamento de Dados deverá parametrizar o Sistema de Ponto Eletrônico a fim de
adequá-lo à alteração de horário de funcionamento estabelecida nesta Portaria.
 
Art.11. Os prazos de compensação de horas, em curso no referido período, serão prorrogados por trinta
dias.
RUI VICENTE OPPERMANN,
Reitor.
