Documento gerado sob autenticação Nº YUF.846.441.225, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5954                  de  15/07/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Retificar a Portaria n° 2788/2016, de 14/04/2016 que concedeu progressão funcional, por avaliação
de desempenho, no Quadro desta Universidade, ao Professor WILIAM WEGNER, matrícula SIAPE n° 2507800,
com exercício no Departamento de Enfermagem Materno-Infantil da Escola de Enfermagem, da classe A de
Professor Adjunto A, nível 01, para a classe A de Professor Adjunto A, nível 02, referente ao interstício de
28/01/2014 a 27/01/2016, com vigência financeira a partir de 13/04/2016, de acordo com o que dispõe a
Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo nº 23078.003600/2016-
05.
 
Onde se lê:
...da classe A de Professor Adjunto A, nível 01, para a classe A de Professor Adjunto A, nível 02...
 
Leia-se:
...da classe C de Professor Adjunto, nível 01, para a classe C de Professor Adjunto, nível 02, ficando
ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora.
