Documento gerado sob autenticação Nº NNJ.495.026.UNI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4798                  de  30/06/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor DAGOBERTO ADRIANO RIZZOTTO JUSTO, matrícula SIAPE n° 3333692, lotado e em exercício no
Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística,  da classe   de
Professor Adjunto,  nível  02,  para a  classe   de Professor Adjunto,  nível  03,  referente ao interstício  de
31/05/2009 a 30/05/2011, com vigência financeira a partir de 27/06/2016, de acordo com o que dispõe a
Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo nº 23078.502189/2016-
48.
RUI VICENTE OPPERMANN
Vice-Reitor
