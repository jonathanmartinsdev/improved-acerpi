Documento gerado sob autenticação Nº FIX.025.246.63A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7075                  de  06/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016,
do Magnífico Reitor, e conforme Processo SEI Nº 23078.516018/2019-49
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior,  do Quadro de Pessoal  desta Universidade,  FELIPE AMORIM BERUTTI (Siape:  3028247),   para
substituir  DANIELA  DIETZ  VIANA  (Siape:  2360646  ),  Coordenador  da  COMGRAD  do  Curso  Bacharelado
Interdisciplin em Ciências e Tec do Campus Litoral Norte, Código SRH 1461, FUCC, em sua Prorrogação de
Licença Gestante,no período de 20 a 25 de janeiro de 2020, com o decorrente pagamento das vantagens por
6 dias.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
