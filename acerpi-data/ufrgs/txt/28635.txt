Documento gerado sob autenticação Nº FTX.726.807.U24, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7878                  de  04/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CAROLINE SILVEIRA BAUER, Professor do Magistério Superior,
lotada e em exercício no Departamento de História do Instituto de Filosofia e Ciências Humanas, com a
finalidade de participar do "IX Seminario Internacional Políticas de la Memoria: 40 años del golpe-cívico-militar
- reflexiones desde el presente", em Buenos Aires, Argentina, no período compreendido entre 02/11/2016 e
06/11/2016, incluído trânsito, com ônus limitado. Solicitação nº 21822.
RUI VICENTE OPPERMANN
Reitor
