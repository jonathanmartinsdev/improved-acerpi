Documento gerado sob autenticação Nº CEI.539.095.KI3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6422                  de  25/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de AFONSO REGULY, Professor do Magistério Superior, lotado no
Departamento  de  Metalurgia  da  Escola  de  Engenharia  e  com exercício  na  Comissão  de  Extensão  em
Engenharia, com a finalidade de visitas junto ao The Welding Institute, em Cambridge, Inglaterra, no período
compreendido entre 24/09/2016 e 01/10/2016, incluído trânsito, com ônus limitado. Solicitação nº 22077.
CARLOS ALEXANDRE NETTO
Reitor
