Documento gerado sob autenticação Nº PQC.388.331.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6064                  de  09/08/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de TARCÍSIO ABREU SAURIM, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia de Produção e Transportes da Escola de Engenharia,
com a finalidade de realizar  estudos em nível de Pós-Doutorado, junto à Macquarie University, em Sydney -
Austrállia,  no  período  compreendido  entre  01/10/2018  e  28/02/2019,  com  ônus  CAPES.  Processo  nº
23078.512818/2018-18.
RUI VICENTE OPPERMANN
Reitor.
