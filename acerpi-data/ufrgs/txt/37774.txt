Documento gerado sob autenticação Nº PFU.196.355.OQ8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3914                  de  05/05/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar a Portaria n° 3396/2017, de 24/04/2017, que concedeu autorização para afastamento do
País a ALESSANDRO DE OLIVEIRA RIOS, Professor do Magistério Superior, com exercício no Departamento
de Ciências dos Alimentos do Instituto de Ciências e Tecnologia de Alimentos
Onde se lê: com ônus limitado,
leia-se: com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias), ficando ratificados os demais termos.
Solicitação nº 27783.
RUI VICENTE OPPERMANN
Reitor.
