Documento gerado sob autenticação Nº FLT.762.559.UPB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             643                  de  23/01/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal  desta  Universidade,  PAULO  RICARDO  ZILIO  ABDALA,  matrícula  SIAPE  n°  2188779,  lotado  no
Departamento de Ciências Administrativas da Escola de Administração, como Chefe Substituto do Depto de
Ciências Administrativas da Escola de Administração, para substituir automaticamente o titular desta função
em seus  afastamentos  ou impedimentos  regulamentares  no período de 21/02/2018 e  até  20/02/2020.
Processo nº 23078.500888/2018-15.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
