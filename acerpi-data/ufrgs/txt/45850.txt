Documento gerado sob autenticação Nº BPD.117.918.N17, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9504                  de  11/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de MARCIA HELOISA TAVARES DE FIGUEREDO LIMA, Professor do
Magistério Superior, lotada e em exercício no Departamento de Ciência da Informação da Faculdade de
Biblioteconomia e Comunicação, com a finalidade de participar das "Jornadas Internacionales de Acceso a la
Información 2017", em Buenos Aires, Argentina, no período compreendido entre 31/10/2017 e 03/11/2017,
incluído trânsito, com ônus limitado. Solicitação nº 31219.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
