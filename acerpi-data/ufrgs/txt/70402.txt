Documento gerado sob autenticação Nº OVS.327.617.F13, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7898                  de  03/10/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  CARMEN  LUCIA  MOTTIN  DURO,  matrícula  SIAPE  n°  1299979,  lotada  e  em  exercício  no
Departamento de Assistência e Orientação Profissional da Escola de Enfermagem, da classe C  de Professor
Adjunto, nível 02, para a classe C  de Professor Adjunto, nível 03, referente ao interstício de 01/09/2016 a
31/08/2018, com vigência financeira a partir de 01/09/2018, de acordo com o que dispõe a Lei 12.772 de 28
de  dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.518181/2018-65.
RUI VICENTE OPPERMANN
Reitor.
