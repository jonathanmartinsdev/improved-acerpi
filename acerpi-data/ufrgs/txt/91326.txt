Documento gerado sob autenticação Nº PHH.719.365.SEQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4435                  de  22/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5026997-59.2019.4.04.7100,  da  4ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo II da Portaria nº 1874, de
13/07/2006, da servidora ANNA EMILIA PINTO LOPES , matrícula SIAPE n° 0351163, desligada no cargo de
Técnico em Contabilidade - 701224, do nível I para o nível II, a contar de 01/01/2006, conforme o Processo nº
23078.512292/2019-49.
 
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria
