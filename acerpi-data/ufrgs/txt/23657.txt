Documento gerado sob autenticação Nº OXE.152.687.EQE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4668                  de  24/06/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar a Portaria n° 4313/2016, de 13/06/2016, que concedeu autorização para afastamento do
País  a  CHRISTIAN BREDEMEIER,  Professor  do Magistério  Superior,  com exercício  no Departamento de
Plantas de Lavoura da Faculdade de Agronomia
Onde se lê: no período compreendido entre 22/06/2016 e 24/06/2016,
leia-se: no período compreendido entre 21/06/2016 e 24/06/2016, ficando ratificados os demais
termos. Solicitação nº 20940.
CARLOS ALEXANDRE NETTO
Reitor
