Documento gerado sob autenticação Nº MTX.726.993.M3A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3583                  de  17/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°48968,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ADMINISTRADOR, do Quadro de
Pessoal desta Universidade, EZEQUIEL LIMA DE MATOS (Siape: 2057737 ),  para substituir   ROBERTA SILVA
DE LEON (Siape: 2076828 ), Supervisor da Divisão de Estágios e Monitorias do Departamento de Cursos e
Políticas da Graduação da PROGRAD, Código FG-4, em seu afastamento por motivo de Laudo Médico do
titular da Função, no período de 11/04/2018 a 09/07/2018, com o decorrente pagamento das vantagens por
90 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
