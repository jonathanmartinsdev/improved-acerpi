Documento gerado sob autenticação Nº HGF.916.926.O2I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4965                  de  05/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°35052,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, MAURÍCIO DE ROSSI TOSO (Siape: 2259006 ),
 para  substituir    PAULO  RICARDO  DA  SILVA  BRAGA  (Siape:  0356336  ),  Coordenador  do  Núcleo  de
Infraestrutura da Gerência Administrativa da Faculdade de Veterinária, Código FG-7, em seu afastamento por
motivo de férias, no período de 01/06/2017 a 10/06/2017, com o decorrente pagamento das vantagens por
10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
