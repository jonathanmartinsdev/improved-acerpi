Documento gerado sob autenticação Nº GFN.192.687.P1Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2354                  de  28/03/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
          Autorizar o afastamento no País de MARCO AURELIO CHAVES CEPIK, ocupante do cargo de Professor
do Magistério Superior, com lotação e exercício no Departamento de Economia e Relações Internacionais da
Faculdade de Ciências Econômicas, com a finalidade de realizar estudos em nível de Pós-Doutorado, junto ao
Instituto  de  Relações  Internacionais,  Pontifícia  Universidade  Católica  do  Rio  de  Janeiro,  no  período
compreendido entre 30/03/2018 e 29/03/2019, com ônus limitado. Processo 23078.523959/2017-77.
JANE FRAGA TUTIKIAN
Vice-Reitora.
