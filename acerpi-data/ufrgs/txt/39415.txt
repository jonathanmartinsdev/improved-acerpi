Documento gerado sob autenticação Nº XCP.801.675.MO2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5107                  de  09/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  à  servidora  PAULA  CRISTINA
SIECZKOWSKI GONZALEZ, ocupante do cargo de Médico Veterinário-701048, lotada no Hospital de Clínicas
Veterinárias, SIAPE 2054731, para 75% (setenta e cinco por cento), a contar de 04/04/2017, tendo em vista a
conclusão do curso de Doutorado em Ciências Veterinárias, conforme o Processo nº 23078.200844/2017-15.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
