Documento gerado sob autenticação Nº MUA.160.796.R34, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10043                  de  12/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar, a partir de 07/01/2019, a ocupante do cargo de Bibliotecário-documentalista - 701010, do
Nível  de Classificação EII,  do Quadro de Pessoal  desta Universidade, PRISCILA FERNANDES MEDEIROS,
matrícula SIAPE 2270880, da função de Chefe da Biblioteca de Química, Código SRH 489, Código FG-5, para a
qual  foi  designada pela Portaria  nº  3788/2018 de 23/05/2018,  publicada no Diário Oficial  da União de
30/05/2018. Processo nº 23078.534117/2018-21.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
