Documento gerado sob autenticação Nº BOL.822.458.7I6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4514                  de  23/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  PATRICIA  ALEJANDRA  BEHAR,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Estudos Especializados da Faculdade de Educação, com
a finalidade de participar da "KES-SEEL-19", em St. Juilans, Malta, no período compreendido entre 15/06/2019
e 21/06/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa: diárias). Solicitação nº 84048.
RUI VICENTE OPPERMANN
Reitor
