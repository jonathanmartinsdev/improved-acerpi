Documento gerado sob autenticação Nº UJX.358.072.0LG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8077                  de  09/10/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder 30 (trinta) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de 11
de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
ANGELA FERNANDES DA SILVA, com exercício na Gerência Administrativa da Faculdade de Educação, a ser
usufruída no período de 16/10/2018 a 14/11/2018, referente ao quinquênio de 05/07/2010 a 04/07/2015, a
fim  de  escrever  a  dissertação  de  Mestrado  em  Diversidade  Cultural  e  Inclusão  Social,  oferecido  pela
Universidade FEEVALE; conforme Processo nº 23078.522981/2018-81.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
