Documento gerado sob autenticação Nº WAX.201.615.NAA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3859                  de  25/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  JOAO MANOEL GOMES DA SILVA JUNIOR,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Sistemas Elétricos de Automação e Energia
da Escola de Engenharia, com a finalidade de participar da "European Control Conference", em Limassol,
Chipre, no período compreendido entre 10/06/2018 e 18/06/2018, incluído trânsito, com ônus CNPq (Grant
de Pesquisador - passagens) e CAPES/PROEX (diárias). Solicitação nº 46586.
RUI VICENTE OPPERMANN
Reitor
