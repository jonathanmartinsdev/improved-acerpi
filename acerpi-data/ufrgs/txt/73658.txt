Documento gerado sob autenticação Nº VHT.937.481.LAT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10205                  de  17/12/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, EMERSON ALESSANDRO GIUMBELLI, matrícula SIAPE n° 1361480, lotado no
Departamento de Antropologia do Instituto de Filosofia e Ciências Humanas,  para exercer a função de
Coordenador do PPG em Antropologia  Social,  Código SRH 1137,  código FUC,  com vigência  a  partir  de
26/01/2019 até 25/01/2021. Processo nº 23078.534787/2018-48.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
