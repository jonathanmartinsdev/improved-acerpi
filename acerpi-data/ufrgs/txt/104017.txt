Documento gerado sob autenticação Nº HMD.149.644.L73, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 94/2020-PROGESP Porto Alegre, 20 de janeiro de 2020.
Senhor Pró- Reitor, 
Encaminhamos a servidora DANIELA FALIGUSKI, ocupante do cargo Administrador, para exercício
nessa Unidade no dia 20 de janeiro de 2020.
Solicitamos que nos sejam encaminhadas, no prazo máximo de 03 dias, as seguintes informações:
- atividades a serem desenvolvidas pela servidora;
- local de exercício (divisão, setor), observando a hierarquia dos órgãos registrada no SRH;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal.
Informamos que este encaminhamento se faz para reposição de vaga, em virtude de vacância da
servidora RAQUEL CAPIOTTI DA SILVA.
ZILMARA BONAI
Coordenadora da Coordenadoria de Concursos, Mobilidade e Acompanhamento, vinc. ao DDGP da PROGESP
em exercício
Ilmo. Sr.
MAURÍCIO VIÉGAS DA SILVA,
Pró-Reitor de Gestão de Pessoas,
N/Universidade.
