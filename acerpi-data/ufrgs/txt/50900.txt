Documento gerado sob autenticação Nº GXM.086.191.SLJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1331                  de  15/02/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°36731,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, NEUSA TERESINHA MASSONI (Siape: 1880639 ),  para
substituir   IVES SOLANO ARAUJO (Siape: 2505206 ), Coordenador do PPG em Ensino de Física, Código FUC,
em seu afastamento por motivo de férias,  no período de 17/02/2018 a 04/03/2018,  com o decorrente
pagamento das vantagens por 16 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
