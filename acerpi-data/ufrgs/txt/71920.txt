Documento gerado sob autenticação Nº RWO.099.254.UTV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9060                  de  08/11/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  STELA  NAZARETH  MENEGHEL,  matrícula  SIAPE  n°  1766933,  lotada  no
Departamento de Saúde Coletiva da Escola de Enfermagem, como Coordenadora Substituta do PPG em
Saúde Coletiva da Escola de Enfermagem, para substituir automaticamente o titular desta função em seus
afastamentos  ou  impedimentos  regulamentares  na  vigência  do  presente  mandato.  Processo  nº
23078.530732/2018-69.
JANE FRAGA TUTIKIAN
Vice-Reitora.
