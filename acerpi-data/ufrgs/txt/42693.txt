Documento gerado sob autenticação Nº UEI.911.542.UJM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7412                  de  11/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  28/06/2017,
correspondente ao grau Insalubridade Média, à servidora JULIANA PERUCA, Identificação Única 24061697,
Auxiliar  de  Saúde,  com  exercício  no  Núcleo  Especializado  da  Gerencia  Administrativa  da  ODONTO,
observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de
dezembro de  1991,  por  exercer  atividades  em áreas  consideradas  Insalubres  conforme Laudo Pericial
constante no Processo nº 23078.512044/2017-36, Código SRH n° 23220 e Código SIAPE 2017002974.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
