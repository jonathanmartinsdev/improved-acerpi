Documento gerado sob autenticação Nº VEF.759.064.QLR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             5754                  de  10/07/2019
O PRÓ-REITOR DE PÓS-GRADUAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso
de suas atribuições, considerando o disposto na Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Designar
 
Luciana Salete Buriol, Titular, Ciências Exatas e da Terra
Marcelo Walter, Titular, Ciências Exatas e da Terra
Angela Terezinha de Souza Wyse, Titular, Ciências Biológicas
Alexandra Antunes Mastroberti, Titular, Ciências Biológicas
Angelo Luis Stapassoli Piato, Suplente, Ciências Biológicas
Joao Manoel Gomes Da Silva Junior, Titular, Engenharias
Angela Borges Masuero, Titular, Engenharias
Tiago Roberto Balen, Suplente, Engenharias
Elsa Regina Justo Giugliani, Titular, Ciências da Saúde
Leticia Scherer Koester, Titular, Ciências da Saúde
Fabricio Mezzomo Collares, Suplente, Ciências da Saúde
Danilo Pedro Streit Jr., Titular, Ciências Agrárias
Eliseu Rodrigues, Titular, Ciências Agrárias
Cesar Henrique Espirito Candal Poli, Suplente, Ciências Agrárias
Marta Silveira Peixoto, Titular, Ciências Sociais e Aplicadas
Luisa Gertrudis Durán Rocca, Titular, Ciências Sociais e Aplicadas
Marcelo de Carvalho Griebeler, Suplente, Ciências Sociais e Aplicadas
Paulo Andre Niederle, Titular, Ciências Humanas
Eduardo Munhoz Svartman, Titular, Ciências Humanas
Emerson Alessandro Giumbelli, Suplente, Ciências Humanas
Paulo Antonio de Menezes Pereira da Silveira, Titular, Linguistica, Letras e Artes
Silvia Patricia Fagundes, Titular, Linguistica, Letras e Artes
Rita Lenira de Freitas Bittencourt, Suplente, Linguistica, Letras e Artes
Nathan Willig Lima, Titular, Multidisciplinar/Ensino de Ciências
Leonardo Albuquerque Heidemann, Titular, Mutidisciplinar/Ensino de Ciências
Documento gerado sob autenticação Nº VEF.759.064.QLR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
 
eleitos em 28 de maio de 2019 pelo Fórum de Coordenadores de Pós-Graduação, para, sob a presidência do
Pró-Reitor  de  Pós-Graduação,  compor  a  Comissão  Assessora  da  Pró-Reitoria  de  Pós-Graduação,  com
mandato de 2 anos.
 
 
 
CELSO GIANNETTI LOUREIRO CHAVES
Pró-Reitor de Pós-Graduação
