Documento gerado sob autenticação Nº WUO.299.440.8GP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8186                  de  30/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  29/12/2016,
correspondente ao grau Insalubridade Máxima, à servidora LUCIA MARIA KLIEMANN, Identificação Única
11432039, Professor do Magistério Superior,  com exercício na Faculdade de Medicina,  observando-se o
disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de
1991,  por  exercer  atividades  em áreas  consideradas  Insalubres  conforme Laudo Pericial  constante  no
Processo nº 23078.512691/2017-48, Código SRH n° 23268 e Código SIAPE 2017003248.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
