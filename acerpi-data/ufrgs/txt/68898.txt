Documento gerado sob autenticação Nº HVU.417.947.U5Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6789                  de  30/08/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.517072/2018-21  e
23078.510792/2018-65, da Lei 10.520/02, do PE SRP Nº 002/2017 ? CMR, e ainda da Lei 8.666/93,
RESOLVE:
 
Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 10.2.1 do Termo de Referência do Edital
do Pregão Eletrônico em epígrafe, à Empresa NORDESTE POTENCIAL COMERCIO E SERVICOS EIRELI, CNPJ
nº 22.280.916/0001-85, por entregar o produto com marca diferente da registrada na proposta, conforme
atestado pela BC/UFRGS (doc.  SEI  nº  1090076)  e pelo NUDECON (doc.  SEI  nº  1172488)  do processo nº
23078.517072/2018-21.
 
 Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
