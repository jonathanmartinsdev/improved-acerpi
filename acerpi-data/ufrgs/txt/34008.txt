Documento gerado sob autenticação Nº CPC.172.072.VJS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1174                  de  07/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria n° 1129/2017, de 06/02/2017, que concedeu Progressão por Mérito Profissional
aos servidores Técnico-Administrativos em Educação que completaram interstício para progredir em janeiro
de 2017.
 
Onde se lê:
JULIO CEZAR ANGELO DE SOUZA,
leia-se:
JULIO CEZER ANGELO DE SOUZA.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
