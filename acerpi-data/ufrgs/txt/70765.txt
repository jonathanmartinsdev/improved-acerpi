Documento gerado sob autenticação Nº XCC.172.886.E6V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8225                  de  11/10/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar BRUNO STEFANI RODRIGUES DA SILVA, Matrícula SIAPE 1664870, ocupante do cargo de
Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a
função de Coordenador do Núcleo de Infraestrutura e Patrimônio da Gerência Administrativa do IFCH,
Código SRH 602, código FG-7, com vigência a partir da data de publicação no Diário Oficial da União. Processo
nº 23078.527397/2018-11.
JANE FRAGA TUTIKIAN
Vice-Reitora.
