Documento gerado sob autenticação Nº EYA.923.644.J36, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2346                  de  14/03/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, MARCO ANTONIO DOS SANTOS MARTINS, matrícula SIAPE n° 2194261, lotado
no Departamento de Ciências Contábeis e Atuariais da Faculdade de Ciências Econômicas, para exercer a
função de Coordenador da COMGRAD de Ciências Contábeis, Código SRH 1202, código FUC, com vigência a
partir  da  data  de  publicação  no  Diário  Oficial  da  União,  pelo  período  de  2  anos.  Processo  nº
23078.505518/2019-55.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
