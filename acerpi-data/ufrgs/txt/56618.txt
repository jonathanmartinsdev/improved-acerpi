Documento gerado sob autenticação Nº UON.315.693.EBB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5408                  de  20/07/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias,  
RESOLVE
Designar ANDRE PADILHA DA SILVA, Matrícula SIAPE 0358346, ocupante do cargo de Servente de
Obras, Código 701824, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe do Setor de
Infraestrutura da FABICO, Código SRH 1287, código FG-7, com vigência a partir da data de publicação no
Diário Oficial da União. Processo nº 23078.516202/2018-16.
JANE FRAGA TUTIKIAN
Vice-Reitora.
