Documento gerado sob autenticação Nº NMM.413.582.87C, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3049                  de  25/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  ENRIQUE SERRA PADROS,  matrícula  SIAPE  n°  0359413,  com exercício  no  Departamento  de
História do Instituto de Filosofia e Ciências Humanas, da classe D  de Professor Associado, nível 01, para a
classe D  de Professor Associado, nível 02, referente ao interstício de 15/12/2013 a 14/12/2015, com vigência
financeira a partir de 20/04/2016, de acordo com o que dispõe a Decisão nº 197/2006-CONSUN, alterada pela
Decisão nº 401/2013-CONSUN. Processo nº 23078.201201/2016-08.
RUI VICENTE OPPERMANN
Vice-Reitor
