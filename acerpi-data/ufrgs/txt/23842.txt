Documento gerado sob autenticação Nº SSD.789.572.AOQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4683                  de  26/06/2016
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de
2013
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  21/06/2016,
correspondente ao grau Insalubridade Média, ao servidor JOSE FERNANDO DA SILVA SANTOS, Identificação
Única 3575802, Auxiliar de Veterinária e Zootecnia, com exercício no Departamento de Patologia Clínica
Veterinária da Faculdade de Veterinária, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de
1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas
Insalubres conforme Laudo Pericial constante no Processo nº 23078.023713/2015-38, Código SRH n° 22816 e
Código SIAPE 2016002205.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
