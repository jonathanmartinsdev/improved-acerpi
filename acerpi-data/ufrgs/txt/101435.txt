Documento gerado sob autenticação Nº UPQ.067.943.0J3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10623                  de  27/11/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, ao
Professor SAUL BENHUR SCHIRMER, matrícula SIAPE 1325121, lotado e em exercício no Departamento de
Ensino e Currículo da Faculdade de Educação, da classe A  de Professor Adjunto A, nível 02, para a classe C 
de Professor Adjunto, nível 01, com vigência financeira a partir de 31/01/2020, de acordo com o que dispõe a
Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554, de 20 de junho de 2013 do
Ministério da Educação e a Decisão nº 331/2017. Processo nº 23078.532125/2019-14.
JANE FRAGA TUTIKIAN
Vice-Reitora.
