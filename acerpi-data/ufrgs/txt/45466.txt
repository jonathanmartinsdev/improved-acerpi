Documento gerado sob autenticação Nº CBB.620.739.01A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9294                  de  06/10/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  à  servidora  ISADORA  DE  OLIVEIRA  RITZEL,  ocupante  do  cargo  de   Assistente  em
Administração - 701200, lotada na Faculdade de Veterinária, SIAPE 2422072, o percentual de 25% (vinte e
cinco por cento) de Incentivo à Qualificação, a contar de 27/09/2017, tendo em vista a conclusão do curso de
Graduação em Direito - Bacharelado, conforme o Processo nº 23078.516784/2017-41.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
