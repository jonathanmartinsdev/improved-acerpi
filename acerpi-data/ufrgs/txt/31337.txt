Documento gerado sob autenticação Nº XXK.288.194.T7D, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9938                  de  15/12/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal  desta  Universidade,  ALINE  MARIAN  CALLEGARO,  matrícula  SIAPE  n°  1110938,  lotada  no
Departamento Interdisciplinar do Campus Litoral Norte, como Coordenadora Substituta da COMGRAD do
Curso de Bacharelado em Engenharia de Serviços do Campus Litoral Norte, para substituir automaticamente
o titular desta função em seus afastamentos ou impedimentos regulamentares na vigência do presente
mandato. Processo nº 23078.019829/2016-53.
                                                                 JANE FRAGA TUTIKIAN
                                                   Vice-Reitora, no Exercício da Reitoria
