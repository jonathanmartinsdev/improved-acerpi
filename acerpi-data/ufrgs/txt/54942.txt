Documento gerado sob autenticação Nº IUM.758.391.640, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4064                  de  04/06/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  23/05/2018,   referente  ao  interstício  de
25/10/2016 a 22/05/2018, para o servidor THIAGO GALVÃO DA SILVA PAIM, ocupante do cargo de Técnico
de Laboratório Área - 701244, matrícula SIAPE 2341593,  lotado  no  Instituto de Biociências, passando do
Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.512570/2018-87:
Formação Integral de Servidores da UFRGS IV CH: 90 (26/04/2017 a 11/05/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
