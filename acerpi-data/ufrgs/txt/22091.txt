Documento gerado sob autenticação Nº TBM.053.305.6RP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3566                  de  12/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal desta Universidade, EDISON CAPP, CPF n° 60515589004, matrícula SIAPE n° 2232220, lotado no
Departamento de Ginecologia e Obstetrícia da Faculdade de Medicina, como Coordenador Substituto do PPG
em Ciências da Saúde: Ginecologia e Obstetrícia, para substituir automaticamente o titular desta função em
seus  afastamentos  ou  impedimentos  regulamentares  na  vigência  do  presente  mandato.  Processo  nº
23078.003603/2016-31.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
