Documento gerado sob autenticação Nº GBJ.257.406.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7103                  de  06/09/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Declarar que a aposentadoria concedida a MARIA DE LOURDES DE OLIVEIRA AMBROSIO, matrícula
SIAPE 358047, através da portaria nº 9146, de 20 de novembro de 2015, publicada no Diário Oficial da União
do dia 24 subsequente, passa a ser no cargo de Servente de Limpeza, nível de classificação A, nível de
capacitação  IV,  padrão  15,  conforme  determinação  judicial  contida  no  processo  nº  5045297-
06.2018.4.04.7100.  Processo  nº  23078.521437/2018-11.
 
RUI VICENTE OPPERMANN
Reitor.
