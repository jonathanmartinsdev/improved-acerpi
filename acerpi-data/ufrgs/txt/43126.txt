Documento gerado sob autenticação Nº QSW.316.525.VOJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7760                  de  17/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de VITOR MANFROI, Professor do Magistério Superior, lotado no
Departamento de Tecnologia  dos Alimentos do Instituto de Ciências  e  Tecnologia  de Alimentos e  com
exercício no Instituto de Ciências e Tecnologia de Alimentos, com a finalidade de participar de excursão à
vinicolas do Uruguai, em Canelones e Montevideu, Uruguai, no período compreendido entre 05/09/2017 e
09/09/2017, incluído trânsito, com ônus limitado. Solicitação nº 30282.
RUI VICENTE OPPERMANN
Reitor
