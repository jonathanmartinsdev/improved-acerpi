Documento gerado sob autenticação Nº GJS.020.888.D35, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2821                  de  15/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°18970,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, EDUARDO AUGUSTO REMOR (Siape: 2139817 ),  para
substituir   LUCIANE DE CONTI (Siape: 1552574 ), Coordenador da Comissão de Extensão do Instituto de
Psicologia, em seu afastamento no país, no período de 05/04/2016 a 09/04/2016.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
