Documento gerado sob autenticação Nº MII.034.148.UPC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1271                  de  06/02/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°89780,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CARLA SCHWENGBER TEN CATEN (Siape: 2174654 ),
 para substituir   LUIZ CARLOS PINTO DA SILVA FILHO (Siape: 1276034 ), Diretor da Escola de Engenharia,
Código CD-3,  em seu afastamento no país,  no período de 09/02/2020 a 10/02/2020, com o decorrente
pagamento das vantagens por 2 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
