Documento gerado sob autenticação Nº GFF.618.820.FLB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5096                  de  14/06/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar a prorrogação de afastamento no País de GEANA SILVA DO SANTOS, Enfermeiro-área,
lotada  na  Escola de Enfermagem e com exercício no Laboratório de Enfermagem da Escola de Enfermagem,
com a finalidade de continuar os estudos em nível de Doutorado junto à Universidade Federal do Rio Grande
do Sul, em Porto Alegre, Rio Grande do Sul, no período compreendido entre 01/07/2019 e 30/06/2020, com
ônus limitado. Processo nº 23078.511069/2017-12.
JANE FRAGA TUTIKIAN
Vice-Reitora.
