Documento gerado sob autenticação Nº AEJ.540.305.N5Q, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3113                  de  27/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  MAURICIO MOREIRA E  SILVA BERNARDES,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Design e Expressão Gráfica da Faculdade de
Arquitetura, com a finalidade de realizar visita à University of Illinois at Urbana-Champaign, em Champaign,
Estados Unidos, no período compreendido entre 26/05/2018 e 03/06/2018, incluído trânsito, com ônus CNPq
(Plataforma Carlos Chagas). Solicitação nº 34060.
RUI VICENTE OPPERMANN
Reitor
