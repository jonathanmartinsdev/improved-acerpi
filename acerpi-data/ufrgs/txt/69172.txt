Documento gerado sob autenticação Nº HIT.060.691.QC3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6922                  de  03/09/2018
Nomeação  da  Representação  Discente  na
Comissão de Pesquisa dos órgãos colegiados
da Escola de Enfermagem da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear  a  Representação  Discente  eleita  para  compor  a  Comissão  de  Pesquisa  na  Escola  de
Enfermagem, com mandato de 01 (um) ano, a contar de 17 de agosto de 2018, atendendo o disposto nos
artigos 175 do Regimento Geral  da Universidade e  79 do Estatuto da Universidade,  e  considerando o
processo nº 23078.523230/2018-81, conforme segue:
 
Comissão de Pesquisa (COMPESQ)
Carolina Giordani da Silva
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
