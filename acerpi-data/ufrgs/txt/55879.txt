Documento gerado sob autenticação Nº MRU.907.197.FOB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4783                  de  05/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar na Escola de Educação Física, Fisioterapia e Dança, a partir de 29/06/2018, MONICA TAVARES
FRANCA DE LIMA, matrícula SIAPE n° 2350338, redistribuída conforme Portaria nº 993 de 29 de maio de
2018, do Ministério da Educação, publicada no Diário Oficial da União no dia 30 de maio de 2018, ocupante
do cargo de Assistente em Administração, Classe D, Nível I, Padrão de Vencimento 01, no Quadro de Pessoal
desta Universidade.
Processo 23295.004360.2018-55
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
