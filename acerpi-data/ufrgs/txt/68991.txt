Documento gerado sob autenticação Nº HOQ.163.801.T77, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6805                  de  30/08/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o afastamento do país  de SILVIA REGINA MOREL CORREA,  Professor do Magistério
Superior,  lotada e  em exercício  no Departamento de Arquitetura  da  Faculdade de Arquitetura,  com a
finalidade de participar do "XXXVII Encuentro y XXII Congreso ARQUISUR", em Rosario, Argentina, no período
compreendido entre 25/09/2018 e 29/09/2018, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa -
diárias). Solicitação nº 58590.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
