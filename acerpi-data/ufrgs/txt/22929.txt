Documento gerado sob autenticação Nº YWO.401.814.QG0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4150                  de  07/06/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  25/05/2016,   referente  ao  interstício  de
29/11/2013 a 24/05/2016, para a servidora ANDRESSA BACALAU DIPRAT, ocupante do cargo de Técnico de
Laboratório Área - 701244,  matrícula SIAPE 1618609,  lotada  no  Instituto de Ciências e Tecnologia de
Alimentos, passando do Nível de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível
de  Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.201897/2016-64:
Formação Integral de Servidores da UFRGS VI CH: 152 Carga horária utilizada: 150 hora(s) / Carga horária
excedente: 2 hora(s) (03/04/2013 a 28/04/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
