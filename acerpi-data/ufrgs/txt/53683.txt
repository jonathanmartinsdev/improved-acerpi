Documento gerado sob autenticação Nº CZP.045.218.NGD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3173                  de  30/04/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.200761/2014-75  e
23078.203225/2017-74,  da  Concorrência  nº  007/CPL/UFRGS/2014,  do Contrato  227/2014 e  ainda da Lei
8.666/93,
          RESOLVE:
 
        Aplicar a sanção administrativa de MULTA de 2% (dois por cento) sobre o valor da etapa em atraso, no
montante de R$ 26.460,39 (vinte e seis mil quatrocentos e sessenta reais e trinta e nove centavos), conforme
demonstrativo de cálculo (fl. 36), prevista no item II, parágrafo primeiro da cláusula décima do Contrato c/c
parágrafo 5º da mesma cláusula décima, à Empresa HOME ENGENHARIA LTDA, CNPJ n.º 89.322.952/0001-
35, pelo atraso na conclusão da Etapa 19 da obra, conforme atestado pela SUINFRA nas fls. 01, 10 a 13, bem
como pelo NUDECON nas fls. 33/34 do processo 23078.203225/2017-74.
 
          Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
