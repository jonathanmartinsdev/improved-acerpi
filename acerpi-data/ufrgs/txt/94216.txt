Documento gerado sob autenticação Nº CPF.103.603.M1U, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6048                  de  16/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  10/07/2019,   referente  ao  interstício  de
08/08/2014 a 09/07/2019,  para o servidor CESAR RICARDO KUPPE,  ocupante do cargo de Auxiliar de
Enfermagem - 701411, matrícula SIAPE 1782066,  lotado  no  Colégio de Aplicação, passando do Nível de
Classificação/Nível de Capacitação C III, para o Nível de Classificação/Nível de Capacitação C IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.518001/2019-26:
Formação Integral de Servidores da UFRGS V CH: 130 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 10 hora(s) (04/09/2017 a 17/07/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
