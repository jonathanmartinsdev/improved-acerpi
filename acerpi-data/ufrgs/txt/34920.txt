Documento gerado sob autenticação Nº JMM.791.833.L8B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1871                  de  01/03/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°26095,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do  Quadro  de  Pessoal  desta  Universidade,  ANDRE  MOREIRA  DOS  SANTOS  (Siape:
1336242 ),  para substituir   ANDREA DOS SANTOS BENITES (Siape: 0351127 ), Diretor do Depto de Programas
Acadêmicos  da  PROGRAD,  Código  CD-4,  em  seu  afastamento  no  país,  no  período  de  12/02/2017  a
14/02/2017, com o decorrente pagamento das vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
