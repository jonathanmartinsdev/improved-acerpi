Documento gerado sob autenticação Nº IUL.981.812.2VA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2636                  de  10/04/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias,  considerando  o  resultado  da  eleição  realizada  em  05/04/2018  pelos  Coordenadores  das
Comissões de Pós-Graduação, em conformidade com os Artigos 16 do Estatuto e 14 do Regimento Geral da
Universidade,
RESOLVE:
Designar o Professor PAULO HENRIQUE SCHNEIDER como membro da Câmara de Pós-Graduação,
com mandato de 08 de maio de 2018 a 07 de maio de 2020.
RUI VICENTE OPPERMANN,
Reitor.
