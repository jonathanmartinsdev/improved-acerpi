Documento gerado sob autenticação Nº MEY.408.841.CUN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             811                  de  25/01/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°28792,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal  desta Universidade,  LUIZ MANOEL MACIEL GERMANO (Siape:
2286327 ),  para substituir   LUIZ ALCIDO SOUZA (Siape: 0356527 ), Chefe do Setor Acadêmico da Gerência
Administrativa do ICBS, Código FG-7, em seu afastamento por motivo de férias, no período de 18/01/2017 a
27/01/2017, com o decorrente pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
