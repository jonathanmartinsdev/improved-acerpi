Documento gerado sob autenticação Nº FFD.214.160.3QF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3671                  de  21/05/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Remover, a partir de 21 de maio de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
PERLA  LIMA  SOUSA,  ocupante  do  cargo  de  Assistente  em  Administração,  Ambiente  Organizacional
Administrativo, Código 701200, Classe D, Nível de Capacitação I, Padrão de Vencimento 01, SIAPE nº 3015340
da Faculdade de Veterinária para a lotação Pró-Reitoria de Gestão de Pessoas, com novo exercício na Divisão
de Controle de Cargos.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
