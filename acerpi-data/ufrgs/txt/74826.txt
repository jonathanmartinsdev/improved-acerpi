Documento gerado sob autenticação Nº FPL.405.461.6MP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             378                  de  11/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 02/02/2019, o ocupante do cargo de Assistente em Administração - 701200,
do Nível de Classificação DII, do Quadro de Pessoal desta Universidade, OTÁVIO LUÍS DA SILVA BARRADAS,
matrícula SIAPE 2317068, da função de Chefe da Divisão de Infraestrutura da Gerência Administrativa do
Instituto de Biociências, Código SRH 541, Código FG-7, para a qual foi designado pela Portaria nº 8613/2018
de 25/10/2018, publicada no Diário Oficial da União de 26/10/2018. Processo nº 23078.500465/2019-86.
JANE FRAGA TUTIKIAN
Vice-Reitora.
