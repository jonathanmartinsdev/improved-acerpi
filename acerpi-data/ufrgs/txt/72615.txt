Documento gerado sob autenticação Nº CMM.417.015.O71, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9528                  de  26/11/2018
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de FERNANDO GERCHMAN,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a finalidade de
participar do "Key Scientific Leader (KSL) Programme" e do "Diabetes Dialogue", em Madrid, Espanha, no
período compreendido entre 28/11/2018 e 02/12/2018, incluído trânsito, com ônus limitado. Solicitação nº
61226.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
