Documento gerado sob autenticação Nº DFM.059.309.6RP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3569                  de  12/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ROBERTO GIUGLIANI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de participar do
"6th International Postgraduate Course on Lysosomal Storage Disorders", em Berlim, Alemanha, no período
compreendido entre 26/05/2016 e 29/05/2016, incluído trânsito, com ônus limitado. Solicitação nº 19961.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
