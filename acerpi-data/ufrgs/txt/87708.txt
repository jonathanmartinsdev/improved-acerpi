Documento gerado sob autenticação Nº ITD.385.327.Q6C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2140                  de  08/03/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Tornar insubsistente a Portaria n° 1973/2019, de 27/02/2019, que designou JEAN SEGATA, matrícula
SIAPE n° 1097478, para a função de Coordenador Substituto da COMGRAD do Curso de Políticas Públicas do
IFCH, código SRH 1189. Processo nº 23078.504134/2019-15.
RUI VICENTE OPPERMANN
Reitor.
