Documento gerado sob autenticação Nº RWI.136.243.4DM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9753                  de  03/12/2018
A VICE-REITORIA, NO EXERCÍCIO DA REITORIA, DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de CLAUDIA LIMA MARQUES, Professor do Magistério Superior,
lotada no Departamento de Direito Público e Filosofia do Direito da Faculdade de Direito e com exercício no
Programa de Pós-Graduação em Direito, com a finalidade de participar da Conferência "Envisioning the Future:
Visions  of  the  Future  for  Germany  and  Europe?,  em Berlim,  Alemanha,  no  período  compreendido  entre
05/12/2018 e 10/12/2018, incluído trânsito, com ônus limitado. Solicitação nº 61200.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
