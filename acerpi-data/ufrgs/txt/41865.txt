Documento gerado sob autenticação Nº ELH.142.583.88P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6833                  de  28/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/07/2017,   referente  ao  interstício  de
23/01/2016 a 25/07/2017, para a servidora JÉSSICA DAIANE THOMÉ,  ocupante do cargo de Técnico de
Laboratório Área - 701244,  matrícula SIAPE 2145123,  lotada  no  Centro de Gestão de Tratamento de
Resíduos  Químicos,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de
Classificação/Nível de Capacitação D III,  em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.202017/2017-58:
Formação Integral de Servidores da UFRGS III CH: 115 Carga horária utilizada: 93 hora(s) / Carga horária
excedente: 22 hora(s) (09/03/2016 a 19/07/2017)
ILB - Excelência no Atendimento CH: 5 (30/09/2015 a 20/10/2015)
PROREXT - Curso de EAD de Capacitação no Combate ao Vetor Aedes Aegypti do TELESSAÚDERS/UFRGS CH:
22 (19/03/2016 a 24/03/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
