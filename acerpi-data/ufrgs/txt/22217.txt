Documento gerado sob autenticação Nº HAS.159.201.M2M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3628                  de  16/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Tornar insubsistente a Portaria nº 3310/2016, de 04/05/2016, publicada no Diário Oficial da União
de 06 de maio de 2016, que concedeu autorização para afastamento do País a CARLOS ALEXANDRE NETTO,
ocupante do cargo de Professor do Magistério Superior, lotado no Departamento de Bioquímica do Instituto
de Ciências Básicas da Saúde e com exercício no Gabinete do Reitor. Solicitação nº 19397..
RUI VICENTE OPPERMANN
Vice-Reitor
