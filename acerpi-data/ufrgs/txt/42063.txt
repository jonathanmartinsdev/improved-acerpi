Documento gerado sob autenticação Nº QLB.894.750.7VU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6974                  de  01/08/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  GEORGE  GONZALEZ  ORTEGA,  matrícula  SIAPE  n°  0357282,  lotado  no
Departamento de Produção e Controle de Medicamentos da Faculdade de Farmácia, para exercer a função
de Chefe do Depto de Produção e Controle de Medicamentos da Faculdade de Farmácia, Código SRH 93,
código FG-1, com vigência a partir da data de publicação no Diário Oficial da União, pelo período de 02 anos,
por ter sido reeleito. Processo nº 23078.012924/2017-15.
JANE FRAGA TUTIKIAN
Vice-Reitora.
