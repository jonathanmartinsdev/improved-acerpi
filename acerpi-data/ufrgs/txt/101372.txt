Documento gerado sob autenticação Nº NQV.125.912.V84, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10582                  de  25/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  JOSE  CARLOS  RODRIGUES  NUNES ,  Técnico  em
Aerofotogrametria, lotado no Instituto de Pesquisas Hidráulicas e com exercício no Centro de Estudos de
Geologia Costeira e Oceânica, com a finalidade de realizar trabalho de campo junto ao Instituto Argentino de
Oceanografia,  em  Bahía  Blanca,  Argentina,  no  período  compreendido  entre  01/12/2019  e  13/12/2019,
incluído trânsito,  com ônus para a Fundação de Apoio da Universidade Federal  do Rio Grande do Sul.
Solicitação nº 88170.
RUI VICENTE OPPERMANN
Reitor
