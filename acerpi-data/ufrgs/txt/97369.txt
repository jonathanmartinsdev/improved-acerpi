Documento gerado sob autenticação Nº VWB.232.839.CNM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7884                  de  30/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  01/08/2019,
correspondente ao grau Insalubridade Média, à servidora CRISTIANE BAUERMANN LEITAO, Identificação
Única  12931276,  Professor  do  Magistério  Superior,  com exercício  no  Programa de  Pós-Graduação  em
Ciências Médicas: Endocrinologia da Faculdade de Medicina, observando-se o disposto na Lei nº 8.112, de 11
de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em
áreas consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.515449/2019-98,
Código SRH n° 24023.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
