Documento gerado sob autenticação Nº BUI.175.858.LPI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5815                  de  04/08/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de RODRIGO SCHRAMM, Professor do Magistério Superior, lotado
e em exercício no Departamento de Música do Instituto de Artes, com a finalidade de realizar Pesquisa, junto
à Queen Mary University of London - Reino Unido, no período compreendido entre 14 de agosto de 2016 e
13 de agosto de 2017, com ônus limitado. Processo nº 23078.504303/2016-74.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
