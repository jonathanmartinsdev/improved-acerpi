Documento gerado sob autenticação Nº OOR.997.649.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7492                  de  19/08/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CLAUDIA LIMA MARQUES, Professor do Magistério Superior,
lotada e em exercício no Departamento de Direito Público e Filosofia do Direito da Faculdade de Direito, com
a finalidade de participar de reunião na Hague Conference on Private International Law, em Haia, Holanda,
no período compreendido entre 02/09/2019 e 07/09/2019, incluído trânsito, com ônus limitado. Solicitação nº
86292.
RUI VICENTE OPPERMANN
Reitor
