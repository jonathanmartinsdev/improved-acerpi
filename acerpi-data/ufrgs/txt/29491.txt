Documento gerado sob autenticação Nº PJG.817.450.H5O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8406                  de  19/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/09/2016,   referente  ao  interstício  de
12/12/2013 a 11/09/2016, para a servidora ANDRESSA PASINI, ocupante do cargo de Técnico em Radiologia
-  701257,  matrícula  SIAPE  2078565,   lotada   na   Faculdade  de  Odontologia,  passando  do  Nível  de
Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.020138/2016-01:
ENAP - Ética e Serviço Público CH: 20 (16/08/2016 a 05/09/2016)
ENAP - Atendimento ao Cidadão CH: 20 (31/05/2016 a 20/06/2016)
UFMS - UNA-SUS - Zika: abordagem clínica na atenção básica CH: 45 (28/08/2016 a 31/08/2016)
FAMESP - Curso de Extensão em Anatomia Radiológica CH: 52 Carga horária utilizada: 5 hora(s) / Carga
horária excedente: 47 hora(s) (29/09/2016 a 07/10/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
