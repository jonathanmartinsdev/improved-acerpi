Documento gerado sob autenticação Nº KKV.883.892.CB7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7578                  de  14/08/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias,
RESOLVE
Designar  ANGELA  MARIA  SEVERO  LERINA,  Matrícula  SIAPE  0358711,  ocupante  do  cargo  de
Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a
função de Diretora da Divisão de Planejamento e Controle de Aquisições do Departamento de Licitação e
Transportes da Pró-Reitoria de Planejamento e Administração (PROPLAN), Código SRH 1525, código FG-1,
com vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.510353/2017-71.
RUI VICENTE OPPERMANN
Reitor.
