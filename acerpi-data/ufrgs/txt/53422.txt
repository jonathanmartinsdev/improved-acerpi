Documento gerado sob autenticação Nº UDC.070.956.9JC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2905                  de  19/04/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar,  a  pedido,  a partir  de 06/12/2017,  o ocupante do cargo de Professor do Magistério
Superior, classe Adjunto do Quadro de Pessoal desta Universidade, CARLOS OTAVIO PETTER, matrícula SIAPE
n° 1223695, da função de Chefe do Depto de Engenharia de Minas da Escola de Engenharia, Código SRH 208,
código FG-1, para a qual foi designado pela Portaria 4726/2016, de 28/06/2016, publicada no Diário Oficial da
União do dia 29/06/2016. Processo nº 23078.502313/2018-37.
RUI VICENTE OPPERMANN
Reitor.
