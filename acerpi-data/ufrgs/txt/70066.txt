Documento gerado sob autenticação Nº TLO.325.956.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7628                  de  24/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de HELOISA HELENA KARNAS HOEFEL, Professor do Magistério
Superior,  lotada  e  em  exercício  no  Departamento  de  Enfermagem  Médico-Cirúrgica  da  Escola  de
Enfermagem, com a finalidade de proferir palestra no 2º Congresso Nacional em Esterilização, em Lisboa,
Portugal, no período compreendido entre 15/10/2018 e 21/10/2018, incluído trânsito, com ônus limitado.
Solicitação nº 58324.
RUI VICENTE OPPERMANN
Reitor
