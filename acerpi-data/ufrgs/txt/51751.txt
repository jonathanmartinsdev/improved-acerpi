Documento gerado sob autenticação Nº BNX.391.853.RQD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1945                  de  13/03/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  02/03/2018,   referente  ao  interstício  de
02/09/2016 a  01/03/2018,  para a  servidora MARIA CLAUDIA PEREIRA SANTOS,  ocupante do cargo de
Auxiliar  em  Administração  -  701405,  matrícula  SIAPE  2057141,   lotada   na   Superintendência  de
Infraestrutura,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  C  III,  para  o  Nível  de
Classificação/Nível de Capacitação C IV, em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.503987/2018-59:
ABED - Administração do Tempo II CH: 60 (17/07/2017 a 31/07/2017)
ABED - Planejamento de Recursos Humanos CH: 60 (30/11/2017 a 14/12/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
