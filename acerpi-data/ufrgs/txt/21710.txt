Documento gerado sob autenticação Nº SIY.267.493.BPQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3451                  de  10/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  TADEU  MELLO  E  SOUZA,  matrícula  SIAPE  n°  2190495,  com  exercício  no  Departamento  de
Bioquímica do Instituto de Ciências Básicas da Saúde, da classe D  de Professor Associado, nível 02, para a
classe D  de Professor Associado, nível 03, referente ao interstício de 18/03/2014 a 17/03/2016, com vigência
financeira a partir de 04/05/2016, de acordo com o que dispõe a Decisão nº 197/2006-CONSUN, alterada pela
Decisão nº 401/2013-CONSUN. Processo nº 23078.006267/2016-88.
RUI VICENTE OPPERMANN
Vice-Reitor
