Documento gerado sob autenticação Nº JVA.199.990.U2K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9037                  de  28/09/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  28/09/2017,   referente  ao  interstício  de
28/03/2016  a  27/09/2017,  para  o  servidor  MAURÍCIO  VIÉGAS  DA  SILVA,  ocupante  do  cargo  de
Administrador - 701001, matrícula SIAPE 0354315,  lotado  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.014439/2017-78:
ENAP - Gestão Estratégica com uso de BSC CH: 10 (10/11/2015 a 30/11/2015)
CENED -  Gestão Na Administração Pública CH: 180 Carga horária utilizada:  170 hora(s)  /  Carga horária
excedente: 10 hora(s) (14/06/2017 a 10/07/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
