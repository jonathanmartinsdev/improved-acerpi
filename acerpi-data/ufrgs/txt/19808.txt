Documento gerado sob autenticação Nº FZV.861.234.JAU, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2231                  de  23/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE:
Conceder pensão vitalícia, a partir de 4 de fevereiro de 2016, a NEUSA ALVES DE SOUZA,  nos
termos dos artigos 215, 217, inciso I, 218, e 222, VII, da Lei nº 8.112/1990, com redação dada pela Lei nº
13.135/2015,  combinado  com  artigo  40  da  Constituição  Federal  de  1988,  alterado  pela  Emenda
Constitucional 41/2003, regulamentado pela Lei nº 10.887/2004, artigo 2º, I, em decorrência do falecimento
de JOSE FRAGA FACHEL,  matrícula SIAPE n° 0353233, aposentado no cargo de Professor do Magistério
Superior do quadro de pessoal desta Universidade. Processo n.º 23078.002539/2016-71.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
