Documento gerado sob autenticação Nº DMQ.826.452.17E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9840                  de  24/10/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ALBERTO REINALDO REPPOLD FILHO, Professor do Magistério
Superior,  lotado  e  em  exercício  no  Departamento  de  Educação  Física  da  Escola  de  Educação  Física,
Fisioterapia e Dança, com a finalidade de participar do evento "Play the Game 2017", em Eindhoven e realizar
visita ao Sportcentrum Papendal -  Olympic Training Centre Papendal,  em Arnhem, Holanda, no período
compreendido entre 24/11/2017 e 03/12/2017, incluído trânsito, com ônus limitado. Solicitação nº 32217.
RUI VICENTE OPPERMANN
Reitor
