Documento gerado sob autenticação Nº BOV.120.404.B7G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7639                  de  15/08/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°36217,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, NORBERTO DANI (Siape: 0357083 ),  para substituir  
EDINEI KOESTER (Siape: 2174577 ), Diretor do Centro de Estudos em Petrologia e Geoquímica, Código FG-1,
em seu afastamento por motivo de férias,  no período de 07/08/2017 a 25/08/2017,  com o decorrente
pagamento das vantagens por 19 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
