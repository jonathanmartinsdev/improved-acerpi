Documento gerado sob autenticação Nº ELK.676.739.VNN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6172                  de  19/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°30519,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de NUTRICIONISTA-HABILITAÇÃO,
do Quadro de Pessoal desta Universidade, CRISTIANE SILVA DE OLIVEIRA (Siape: 1734147 ),  para substituir  
STÉFANI ALMEIDA SCHNEIDER (Siape: 2230620 ), Chefe da Seção do RU1 da Divisão de Alimentação do DIE da
PRAE, Código FG-3, em seu afastamento por motivo de férias, no período de 08/08/2016 a 15/08/2016, com o
decorrente pagamento das vantagens por 8 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
