Documento gerado sob autenticação Nº ZEK.022.804.EUJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             5271                  de  16/06/2017
Nomeação  da  Representação  Discente  dos
órgãos colegiados do Instituto de Filosofia e
Ciências Humanas da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita no Instituto de Filosofia e Ciências Humanas, com mandato
de 01 (um) ano, a contar de 02 de junho de 2017, atendendo o disposto nos artigos 175 do Regimento Geral
da Universidade e 79 do Estatuto da Universidade, e considerando o processo nº 23078.201561/2017-82,
conforme segue:
Conselho da Unidade ? CONSUNI:
Titular: Douglas Morano de Oliveira Lopes
Suplente: Juliana Renck Bimbi
 
Comissão de Graduação ? COMGRAD:
Titular: Maurício Clipes Cunha
Suplente: Alice Schimitz Toldo
 
Plenária do Departamento:
Titular: Ítalo Ariel Pereira Guerreiro
Suplente: Juliana Prado Cros
Titular: Juliana Renck Bimbi
Suplente: Vinícius Reis Furini
Titular: Bruna Moreira da Silva
Suplente: Alice Schimitz Toldo
Titular: Wellington Lareano Alves
Suplente: Douglas Morano de Oliveira Lopes
Titular: Jodéli Fabiana Dreissig
Suplente: Nathália Moraes Correa
Titular: Tiago Pacheco Almeida
Suplente: Leonel de Cerqueira Meirelles
 
Documento gerado sob autenticação Nº ZEK.022.804.EUJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Colegiado do Departamento:
Titular: Eduarda Mariano Conceição
Suplente: Tiago Pacheco Almeida
Titular: Ítalo Ariel Pereira Guerreiro
Suplente: Jodéli Fabiana Dreissig.
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
