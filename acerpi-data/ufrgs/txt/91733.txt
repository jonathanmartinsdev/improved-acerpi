Documento gerado sob autenticação Nº FZV.205.173.TCU, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4468                  de  22/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, SIMONE CRISTINA BAGGIO GNOATTO, matrícula SIAPE n° 2342894, lotada no
Departamento de Produção de Matéria Prima da Faculdade de Farmácia, para exercer a função de Chefe do
Depto de Produção de Matéria Prima da Faculdade de Farmácia, Código SRH 101, código FG-1, com vigência a
partir de 16/06/2019 até 15/06/2021. Processo nº 23078.512742/2019-01.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
