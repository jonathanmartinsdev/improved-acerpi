Documento gerado sob autenticação Nº ZBL.780.499.I19, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6700                  de  24/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ALINE RIGON ZIMMER, matrícula SIAPE n° 1966864, lotada e em exercício no Departamento de
Produção de Matéria Prima da Faculdade de Farmácia, da classe C  de Professor Adjunto, nível 02, para a
classe C  de Professor Adjunto, nível 03, referente ao interstício de 04/09/2014 a 03/09/2016, com vigência
financeira a partir de 04/09/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo
nº 23078.505730/2017-51.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
