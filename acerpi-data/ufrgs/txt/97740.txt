Documento gerado sob autenticação Nº SYT.309.612.N1P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8116                  de  06/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor LUIS ROBERTO GUIMARAES DA SILVA, ocupante do cargo de  Assistente em
Administração - 701200, lotado no Instituto de Ciências Básicas da Saúde, SIAPE 0356497, o percentual de
25% (vinte e cinco por cento) de Incentivo à Qualificação, a contar de 26/08/2019, tendo em vista a conclusão
do curso Superior de Tecnologia em Gestão da Informação, conforme o Processo nº 23078.522910/2019-69.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
