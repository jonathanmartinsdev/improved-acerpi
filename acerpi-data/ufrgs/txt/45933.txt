Documento gerado sob autenticação Nº FSR.004.061.U1N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9550                  de  13/10/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°36817,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de JORNALISTA, do Quadro de
Pessoal desta Universidade, ANDRE SOARES GRASSI (Siape: 1094163 ),  para substituir   Odilon Rolim (Siape:
0358843 ), Secretário do Centro Nacional de Supercomputação, Código FG-4, em seu afastamento por motivo
de férias, no período de 18/10/2017 a 27/10/2017, com o decorrente pagamento das vantagens por 10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
