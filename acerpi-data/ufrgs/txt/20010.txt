Documento gerado sob autenticação Nº QUO.566.184.JP1, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2551                  de  08/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora NILVA ROSANGELA DA
ROCHA, ocupante do cargo de Telefonista-701464, lotada na Faculdade de Arquitetura, SIAPE 0358324, para
25% (vinte e cinco por cento), a contar de 19/03/2016, tendo em vista a conclusão do curso de Superior de
Tecnologia em Gestão de Recursos Humanos, conforme o Processo nº 23078.031287/2015-14.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
