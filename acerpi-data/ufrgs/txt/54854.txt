Documento gerado sob autenticação Nº GMG.230.278.KVJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3950                  de  29/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 30 (trinta) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de 11
de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
GISLAINE THOMPSON DOS SANTOS, com exercício na Divisão de Promoção da Saúde da Pró-Reitoria de
Gestão de Pessoas,  a  ser  usufruída no período de 18/06/2018 a  17/07/2018,  referente  ao quinquênio
de 28/01/2009 a 27/01/2014, a fim de participar do Curso de Aperfeiçoamento sobre Saúde, Higiene e
Segurança do Trabalho, conforme Processo nº 23078.510081/2018-91.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
