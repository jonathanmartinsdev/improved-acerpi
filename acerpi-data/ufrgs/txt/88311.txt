Documento gerado sob autenticação Nº COM.826.526.E3T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2423                  de  18/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  15/03/2019,   referente  ao  interstício  de
15/09/2017 a 14/03/2019, para o servidor JOÃO SAMUEL PASSOS BARBOSA, ocupante do cargo de Auxiliar
em Administração - 701405, matrícula SIAPE 2161274,  lotado  na  Faculdade de Educação, passando do
Nível de Classificação/Nível de Capacitação C III, para o Nível de Classificação/Nível de Capacitação C IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.502482/2019-58:
Formação Integral de Servidores da UFRGS II CH: 40 (25/05/2017 a 16/03/2018)
ILB - Excelência no Atendimento CH: 20 (22/02/2018 a 21/03/2018)
ILB - Relações Internacionais: Teoria e História CH: 60 (04/06/2018 a 25/07/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
