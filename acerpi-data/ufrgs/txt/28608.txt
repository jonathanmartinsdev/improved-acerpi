Documento gerado sob autenticação Nº YQO.699.662.U24, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7865                  de  04/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MANUEL  MENEZES  DE  OLIVEIRA  NETO,  Professor  do
Magistério  Superior,  lotado  e  em  exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de
Informática, com a finalidade de participar de conferência junto ao Special Interest Group on Computer
Graphics, em Bogotá, Colômbia, no período compreendido entre 11/10/2016 e 15/10/2016, incluído trânsito,
com ônus limitado. Solicitação nº 23554.
RUI VICENTE OPPERMANN
Reitor
