Documento gerado sob autenticação Nº NNX.271.675.P57, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8893                  de  04/11/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLAUDIA LIMA MARQUES, Professor do Magistério Superior,
lotada no Departamento de Direito Público e Filosofia do Direito da Faculdade de Direito e com exercício no
Programa de Pós-Graduação em Direito, com a finalidade de participar de reunião junto à Universidade
Humboldt Berlim - Alemanha,  no período compreendido entre 11/11/2016 e 12/11/2016, incluído trânsito,
com ônus CAPES/PROAP. Solicitação nº 23813.
RUI VICENTE OPPERMANN
Reitor
