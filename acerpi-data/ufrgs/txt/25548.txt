Documento gerado sob autenticação Nº EQE.334.593.MAF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5877                  de  08/08/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Dispensar,  a  pedido,  a partir  de 01/08/2016,  o ocupante do cargo de Professor do Magistério
Superior,  classe  Adjunto  do  Quadro  de  Pessoal  desta  Universidade,  LUIS  FERNANDO  ALVES  PEREIRA,
matrícula SIAPE n° 1667664, da função de Coordenador do PPG em Engenharia Elétrica, Código SRH 1118,
código FUC, para a qual foi designado pela Portaria 4143/2016, de 07/06/2016, publicada no Diário Oficial da
União do dia 08/06/2016. Processo nº 23078.015930/2016-35.
RUI VICENTE OPPERMANN
Vice-Reitor
