Documento gerado sob autenticação Nº MUL.498.538.FNS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2407                  de  02/04/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar MARIANGELA MACHADO TOALDO, matrícula SIAPE n° 1881220, ocupante do cargo de
Professor do Magistério Superior, classe Adjunto, lotada no Departamento de Comunicação da Faculdade de
Biblioteconomia  e  Comunicação,  do  Quadro  de  Pessoal  da  Universidade,  para  exercer  a  função  de
Coordenadora Substituta da Comissão de Pesquisa da FABICO, com vigência a partir da data deste ato até
18/01/2019, a fim de completar o mandato  do Professor FLAVIO ANTONIO CAMARGO PORCELLO, conforme
artigo  92  do  Estatuto  da  mesma  Universidade,  sem  prejuízo  e  cumulativamente  com  a  função
de Coordenadora da COMGRAD em Comunicação Social  -  Publicidade e Propaganda, código SRH 1507,
código FUC, até 27/06/2018. Processo nº 23078.505674/2018-35.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
