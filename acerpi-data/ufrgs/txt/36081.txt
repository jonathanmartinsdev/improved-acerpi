Documento gerado sob autenticação Nº EFW.144.794.0QV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2678                  de  28/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARIA JOAO VELOSO DA COSTA RAMOS PEREIRA,  matrícula  SIAPE n°  2164851,  lotada no
Departamento de Zoologia  do Instituto  de Biociências  e  com exercício  na Comissão de Graduação de
Ciências Biológicas, da classe A  de Professor Adjunto A, nível 01, para a classe A  de Professor Adjunto A,
nível 02, referente ao interstício de 12/09/2014 a 11/09/2016, com vigência financeira a partir de 12/09/2016,
de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e a Resolução
nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo nº 23078.510088/2016-41.
JANE FRAGA TUTIKIAN
Vice-Reitora.
