Documento gerado sob autenticação Nº HGN.075.347.1BB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5319                  de  15/07/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso das atribuições que lhe
foram delegadas pela Portaria nº 5148, de 01 de outubro de 2012
RESOLVE
Autorizar a prorrogação de afastamento no País de GUSTAVO ANDRADA BANDEIRA, Técnico em
Assuntos Educacionais, lotado  na  Escola de Administração e com exercício no Setor Acadêmico da Gerência
Administrativa  da  Escola  de  Administração,  para  continuar  estudos  em  nível  de  Doutorado,  junto  à
Universidade Federal do Rio Grande do Sul, no período compreendido entre 01 de agosto de 2016 e 31 de
julho de 2017, com ônus limitado. Processo nº 23078.010648/2015-81.
RUI VICENTE OPPERMANN
Vice-Reitor
