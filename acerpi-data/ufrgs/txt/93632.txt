Documento gerado sob autenticação Nº MXH.990.724.AU8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5590                  de  03/07/2019
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de LISIEUX ELAINE DE BORBA TELLES, Professor do Magistério
Superior, lotada e em exercício no Departamento de Psiquiatria e Medicina Legal da Faculdade de Medicina,
com a finalidade de participar do "XXXVI International Congress on Law and Mental Health", em Roma, Itália,
no período compreendido entre 21/07/2019 e 27/07/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria
de Pesquisa: diárias). Solicitação nº 83607.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
