Documento gerado sob autenticação Nº KWJ.851.044.CB7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7544                  de  14/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de Paulo Roberto Eckert, Professor do Magistério Superior, lotado
e em exercício no Departamento de Engenharia Elétrica da Escola de Engenharia,  com a finalidade de
participar do "18th International Symposium on Electromagnetic Fields in Mechatronics", em Lodz, Polônia,
no período compreendido entre 11/09/2017 e 18/09/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria
de Pesquisa - diárias) e CAPES/PROEX (passagens). Solicitação nº 30231.
RUI VICENTE OPPERMANN
Reitor
