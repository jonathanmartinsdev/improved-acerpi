Documento gerado sob autenticação Nº SQU.330.889.IRF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1849                  de  05/03/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  ao  servidor  JOSE CARLOS RODRIGUES NUNES,  ocupante  do cargo de   Técnico  em
Aerofotogrametria - 701218, lotado no Instituto de Pesquisas Hidráulicas, SIAPE 0353206, o percentual de
25% (vinte e cinco por cento) de Incentivo à Qualificação, a contar de 23/11/2019, tendo em vista a conclusão
do curso de Superior de Tecnólogo de Gestão Ambiental, conforme o Processo nº 23078.521970/2019-64.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
