Documento gerado sob autenticação Nº AFM.383.559.VND, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9300                  de  06/10/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°31872,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ANTONIO PEDRO VIERO (Siape: 0359408 ),   para
substituir   HEINRICH THEODOR FRANK (Siape: 0357100 ), Coordenador da Comissão de Extensão do Instituto
de Geociências,  em seu afastamento no país, no dia 04/10/2017.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
