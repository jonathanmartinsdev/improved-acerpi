Documento gerado sob autenticação Nº MTD.028.977.0PF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9687                  de  30/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  24/11/2018,   referente  ao  interstício  de
11/04/2017 a  23/11/2018,  para o  servidor  JOSÉ EDUARDO THUMS,  ocupante do cargo de Técnico de
Tecnologia da Informação - 701226, matrícula SIAPE 1202366,  lotado  no  Centro de Processamento de
Dados, passando do Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.527884/2018-84:
RNP - Arquitetura e protocolos de rede TCP-IP CH: 40 (19/03/2018 a 23/03/2018)
RNP - Administração de sistemas Linux CH: 14 (04/07/2016 a 08/07/2016)
ENAP - Sistema Eletrônico de Informações - SEI CH: 20 (26/09/2017 a 16/10/2017)
ENAP - A Previdência Social dos Servidores Públicos: Regime Próprio e Regime de Previdência CH: 30 Carga
horária utilizada: 26 hora(s) / Carga horária excedente: 4 hora(s) (15/10/2018 a 24/11/2018)
ENAP - Ética e Serviço Público CH: 20 (15/03/2018 a 05/04/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
