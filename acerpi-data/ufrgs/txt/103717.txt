Documento gerado sob autenticação Nº KRL.274.412.KIT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             488                  de  13/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°50575,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CATERINA MARTA GROPOSO PAVAO (Siape: 2093195
),  para substituir   RITA DO CARMO FERREIRA LAIPELT (Siape: 2714174 ), Coordenador da COMGRAD de
Biblioteconomia,  Código  FUC,  em seu  afastamento  por  motivo  de  férias,  no  período de  14/01/2020  a
26/01/2020, com o decorrente pagamento das vantagens por 13 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
