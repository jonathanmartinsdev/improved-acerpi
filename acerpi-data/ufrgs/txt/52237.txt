Documento gerado sob autenticação Nº LDY.074.833.IGL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2224                  de  23/03/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial  da União do dia 6 subsequente, a MARCELO MARTINS
CARBONELL, matrícula SIAPE nº 0353831, no cargo de Assistente em Administração, nível de classificação D,
nível de capacitação II, padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho,  com exercício no Setor de Finanças e Infraestrutura da Gerência Administrativa da Escola de
Enfermagem, com proventos integrais. Processo 23078.000260/2018-14.
RUI VICENTE OPPERMANN
Reitor.
