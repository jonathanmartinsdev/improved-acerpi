Documento gerado sob autenticação Nº WBV.736.464.RK9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8420                  de  16/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de MARLI MARIA KNORST,  Professor do Magistério Superior,
lotada e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a finalidade de
participar do "ERS International  Congress 2019",  em Madrid,  Espanha,  no período compreendido entre
26/09/2019 e 04/10/2019, incluído trânsito, com ônus limitado. Solicitação nº 85463.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
