Documento gerado sob autenticação Nº EJR.909.637.R3E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3234                  de  12/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ANA RIEGER SCHMIDT, Professor do Magistério Superior, lotada
e em exercício no Departamento de Filosofia do Instituto de Filosofia e Ciências Humanas, com a finalidade
de participar do "54th International Congress on Medieval Studies",  em Kalamazoo, Estados Unidos,  no
período compreendido entre 08/05/2019 e 13/05/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa: diárias). Solicitação nº 83088.
RUI VICENTE OPPERMANN
Reitor
