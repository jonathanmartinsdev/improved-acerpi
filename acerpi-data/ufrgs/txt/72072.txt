Documento gerado sob autenticação Nº TMX.765.749.U47, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9148                  de  12/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LUCAS MELLO SCHNORR, Professor do Magistério Superior,
lotado e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a finalidade
de realizar visita à Université Grenoble Alpes (INRIA), em Grenoble, França, no período compreendido entre
06/12/2018 e 17/12/2018, incluído trânsito, com ônus CAPES/COFECUB. Solicitação nº 60877.
RUI VICENTE OPPERMANN
Reitor
