Documento gerado sob autenticação Nº DBF.225.561.SR7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5490                  de  01/07/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
RESOLVE
Designar os servidores  GUILHERME LEHNEMANN RAMOS, ocupante do cargo de Contador, lotado
na  Superintendência  de  Infraestrutura  e  com  exercício  no  Setor  Financeiro  da  Superintendência  de
Infraestrutura,  CELSO  ESCADA  DA  ROSA,  ocupante  do  cargo  de  Contador,  lotado  na  Pró-Reitoria  de
Planejamento e Administração e com exercício na Seção de Escrituração da Despesa, DANIEL CRISTOVAO
FRAGA DA SILVA, ocupante do cargo de Contador, lotado na Procuradoria Geral e com exercício no Núcleo
de Instrução Judicial,  MARISLANE DE FREITAS CORRÊA,  ocupante do cargo de Contador, lotada na Pró-
Reitoria de Gestão de Pessoas e com exercício na Divisão de Contratação de Serviços Terceirizados, para, sob
a presidência do primeiro, comporem Comissão, para no prazo de 60 dias, a partir desta data e com a
finalidade de verificar os cálculos contábeis, trabalhistas, com verificação dos valores apresentados pela
empresa  HOME  ENGENHARIA  LTDA,  que  compõem  custos  demissionais,  admissionais  e  tributos
trabalhistas.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
