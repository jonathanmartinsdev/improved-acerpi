Documento gerado sob autenticação Nº AJE.919.929.4L7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6992                  de  06/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Declarar vago, a partir de 16 de agosto de 2016, o cargo de Técnico em Assuntos Educacionais,
Nível  de Classificação E,  do Quadro de Pessoal,  em decorrência  do falecimento do servidor  GUSTAVO
SANTOS DOS SANTOS, com lotação e exercício na Faculdade de Odontologia, conforme item IX, do Art. 33,
da Lei n° 8.112, de 11 dezembro de 1990. Processo nº 23078018823/2016-69
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
