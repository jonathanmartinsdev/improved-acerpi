Documento gerado sob autenticação Nº BHJ.564.852.3AV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1035                  de  30/01/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 23 de janeiro de 2019,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
TAMARA MOCH, ocupante do cargo de Relações Públicas, Ambiente Organizacional Artes, Comunicação e
Difusão, Código 701072, Classe E, Nível de Capacitação I, Padrão de Vencimento 01, SIAPE nº. 1333616 da
Secretaria de Comunicação Social para a lotação Instituto de Biociências, com novo exercício na Gerência
Administrativa do Instituto de Biociências.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
