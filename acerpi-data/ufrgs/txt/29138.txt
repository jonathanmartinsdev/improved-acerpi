Documento gerado sob autenticação Nº CQD.901.893.K30, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8193                  de  13/10/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7635, de 29 de setembro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  29/08/2016,
correspondente ao grau Insalubridade Máxima, ao servidor ÉLTON PASSOS DA CONCEIÇÃO, Identificação
Única 16772016, Técnico de Laboratório Área, com exercício no Departamento de Engenharia Química da
Escola de Engenharia, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado
com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Insalubres
conforme Laudo Pericial constante no Processo nº 23078.510168/2016-04, Código SRH n° 22919 e Código
SIAPE 2016003771.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
