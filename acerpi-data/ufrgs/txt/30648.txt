Documento gerado sob autenticação Nº FJG.933.423.FP4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9382                  de  23/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas  atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta Universidade,  MARCELO NOGUEIRA CORTIMIGLIA,  matrícula  SIAPE n°  3526000,  lotado no
Departamento de Engenharia de Produção e Transportes da Escola de Engenharia, como Vice-Presidente da
Câmara de Pós-Graduação, para substituir automaticamente o titular desta função em seus afastamentos ou
impedimentos regulamentares, com vigência a partir da data deste ato e até 07/05/2018, a fim de completar
o mandato da professora Letícia Scherer Koester. Processo nº 23078.023472/2016-16.
RUI VICENTE OPPERMANN
Reitor
