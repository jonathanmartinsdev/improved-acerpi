Documento gerado sob autenticação Nº GNR.637.125.CAK, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2732                  de  13/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar MARISE DA SILVA MACEDO, CPF nº 37808958034, Matrícula SIAPE 0357587, ocupante do
cargo de Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para
exercer a função de Secretária de Pós-Graduação da Faculdade de Farmácia, Código SRH 585, código FG-7,
com vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.007632/2016-71.
RUI VICENTE OPPERMANN
Vice-Reitor
