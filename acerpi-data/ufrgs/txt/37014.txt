Documento gerado sob autenticação Nº QWR.931.450.9JT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3317                  de  19/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  13/04/2017,   referente  ao  interstício  de
13/10/2015 a 12/04/2017, para a servidora BEATRIZ ILIBIO MORO, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2259032,  lotada  na  Faculdade de Odontologia, passando do Nível
de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.006716/2017-79:
Formação Integral de Servidores da UFRGS II CH: 57 (04/11/2015 a 07/10/2016)
ABELINE -  Arquivologia CH: 50 Carga horária utilizada: 33 hora(s)  /  Carga horária excedente: 17 hora(s)
(10/02/2017 a 28/03/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
