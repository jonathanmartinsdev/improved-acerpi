Documento gerado sob autenticação Nº TDX.503.629.O5E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5259                  de  14/07/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, à
Professora CARLA SIRTORI, matrícula SIAPE 1884256, lotada e em exercício no Departamento de Química
Inorgânica do Instituto de Química, da classe A  de Professor Adjunto A, nível 02, para a classe C  de Professor
Adjunto, nível 01, com vigência financeira a partir da publicação desta Portaria, de acordo com o que dispõe a
Lei nº 12.772, de 28 de dezembro de 2012, e a Portaria nº 554, de 20 de junho de 2013 do Ministério da
Educação,  e  a  Decisão  nº  401/2013  -  CONSUN,  e  tendo  em  vista  o  que  consta  no  Processo  nº
23078.506527/2016-11.
RUI VICENTE OPPERMANN
Vice-Reitor
