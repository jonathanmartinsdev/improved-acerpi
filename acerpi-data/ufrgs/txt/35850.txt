Documento gerado sob autenticação Nº WKO.902.825.OLQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2495                  de  22/03/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder pensão vitalícia, a partir de 6 de dezembro de 2016, a IRENE SPOTTI KAISER, nos termos
dos artigos 215, 217, inciso I, 218, e 222, VII, da Lei nº 8.112/1990, com redação dada pela Lei nº 13.135/2015,
combinado com artigo 40 da Constituição Federal de 1988, alterado pela Emenda Constitucional 41/2003,
regulamentado pela Lei nº 10.887/2004, artigo 2º, I, em decorrência do falecimento de RAYMUNDO KAISER,
matrícula SIAPE n° 0358262, aposentado no cargo de Marceneiro do quadro de pessoal desta Universidade.
Processo n.º 23078.000567/2017-34.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
