Documento gerado sob autenticação Nº JSZ.249.485.A7F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3089                  de  27/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  23/04/2018,   referente  ao  interstício  de
02/09/2016 a 22/04/2018, para a servidora CAMILA VIVIANE LOPES, ocupante do cargo de Engenheiro-área
- 701031, matrícula SIAPE 2055678,  lotada  na  Superintendência de Infraestrutura, passando do Nível de
Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.509269/2018-96:
ABED - Curso de AutoCAD 2D CH: 60 Carga horária utilizada: 28 hora(s) / Carga horária excedente: 32 hora(s)
(05/03/2018 a 22/04/2018)
ENAP  -  Gestão  e  Fiscalização  de  Contratos  Administrativos  -  Nível  Intermediário  CH:  6  (12/05/2015  a
15/06/2015)
TCU  -  Obras  Públicas  de  Edificação  e  de  Saneamento  -  Módulo  Planejamento  CH:  40  (28/08/2017  a
06/10/2017)
ENAP - SEI! USAR CH: 20 (24/01/2017 a 13/02/2017)
ENAP - Básico em Orçamento Público CH: 30 (08/08/2017 a 04/09/2017)
INSTITUTO SERZEDELLO CORRÊA -  Legislação Básica em Licitações,  Pregão e Registro de Preços CH: 30
(19/12/2017 a 28/12/2017)
UNISINOS - Workshop em Segurança contra Incêndios no Desempenho das Edificações CH: 26 (27/06/2016 a
02/07/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
