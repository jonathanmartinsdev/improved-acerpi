Documento gerado sob autenticação Nº TNV.237.130.EH8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1977                  de  14/03/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de YAN LEVIN,  Professor do Magistério Superior, lotado e em
exercício  no Departamento de Física  do Instituto  de Física,  com a finalidade de participar  do "CECAM
Workshop on Electrostatics in Concentrated Electrolytes", em Laussane, Suíça, no período compreendido
entre 18/03/2018 e 25/03/2018, incluído trânsito, com ônus limitado. Solicitação nº 34615.
                                                                                      JANE FRAGA TUTIKIAN,
                                                                            Vice-Reitora, no exercício da Reitoria.
