Documento gerado sob autenticação Nº NQD.841.463.09L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9392                  de  16/10/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
I -  Conceder a prorrogação pelo período de 06 (seis)  meses,  a partir  de outubro de 2019,  da
Colaboração, por meio de projeto, junto à Universidade Federal de Brasília, da servidora DEISE MAZZARELLA
GOULART FERREIRA, ocupante do cargo de Administrador, classe E, nível IV, padrão 07, matrícula SIAPE n°
1735397, lotada na Secretaria de Educação a Distância e com exercício na Gerência Administrativa da SEAD,
nos termos do artigo 26-A da Lei n° 11.191 de 12 de dezembro de 2005, com redação dada pela Lei n°, de 22
de dezembro de 2005.
II - Fica sob responsabilidade da Universidade Federal de Brasília o envio da frequência da citada
servidora a esta Universidade até o 5° (quinto) dia útil do mês subsequente ao trabalhado.
Processo n° 23078.517536/2017-18.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
