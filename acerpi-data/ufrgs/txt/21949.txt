Documento gerado sob autenticação Nº MHF.374.258.05C, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3426                  de  10/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  01/04/2016,
correspondente ao grau Insalubridade Máxima, à servidora GRACE GOSMANN, Identificação Única 3578674,
Professor  do  Magistério  Superior,  com  exercício  no  Departamento  de  Produção  de  Matéria  Prima  da
Faculdade de Farmácia, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado
com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Insalubres
conforme Laudo Pericial constante no Processo nº 23078.501849/2016-73, Código SRH n° 22773 e Código
SIAPE 2016001278.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
