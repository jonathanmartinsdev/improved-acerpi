Documento gerado sob autenticação Nº VVL.419.033.6FV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3015                  de  25/04/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  RENATA  PEREIRA  LIMBERGER,  matrícula  SIAPE  n°  3037876,  lotada  e  em  exercício  no
Departamento de Análises da Faculdade de Farmácia, da classe D  de Professor Associado, nível 02, para a
classe D  de Professor Associado, nível 03, referente ao interstício de 27/04/2015 a 27/01/2018, com vigência
financeira a partir de 13/04/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.504674/2017-37.
PHILIPPE OLIVIER ALEXANDRE NAVAUX
Decano do Conselho Universitário, no exercício da Reitoria.
