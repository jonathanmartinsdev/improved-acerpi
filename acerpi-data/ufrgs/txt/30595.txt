Documento gerado sob autenticação Nº IQJ.189.684.M8M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9450                  de  28/11/2016
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
 
              RESOLVE:
 
            Considerando o que consta do Processo Administrativo n° 23078.018060/2015-75, da Lei 10.520/02,
do Pregão Eletrônico SRP 182/2015 e da Lei 8.666/93,
            Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 105.1 do Edital de Licitação, à
Empresa SANDRO BORGES DA ROSA - EPP, CNPJ Nº 14.040.948/0001-85, pela irregularidade constatada
junto cadastro do SICAF, conforme atestado pela SUNFRA às fls. 117, 119 e no documento SEI nº 0314938,
bem como pelo NUDECON à fl. 130 do processo administrativo supracitado.
 
             Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
