Documento gerado sob autenticação Nº YHJ.248.970.IHJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3801                  de  03/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, MARCELO DE CARVALHO GRIEBELER, matrícula SIAPE n° 2703454, lotado no
Departamento de Economia e Relações Internacionais da Faculdade de Ciências Econômicas, para exercer a
função de Coordenador do PPG em Economia, Código SRH 1156, código FUC, com vigência a partir de
23/05/2019 até 22/05/2021. Processo nº 23078.510508/2019-31.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
