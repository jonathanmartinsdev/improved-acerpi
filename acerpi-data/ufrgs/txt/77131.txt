Documento gerado sob autenticação Nº ZLL.570.060.T6L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1802                  de  22/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  21/02/2019,   referente  ao  interstício  de
01/03/2005 a 20/02/2019, para o servidor CARLOS ROBERTO OLIVEIRA, ocupante do cargo de Assistente
em Administração - 701200, matrícula SIAPE 0355991,  lotado  na  Faculdade de Odontologia, passando do
Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.501436/2019-31:
ILB - Excelência no Atendimento CH: 20 (13/03/2018 a 02/04/2018)
ENAP - Sistema Eletrônico de Informações - SEI! USAR CH: 20 (17/01/2019 a 16/02/2019)
ENAP  -  Gestão  da  Informação  e  Documentação  -  Conceitos  Básicos  em  Gestão  Documental  CH:  20
(17/01/2019 a 16/02/2019)
ENAP - Gestão Estratégica de Pessoas e Planos de Carreira CH: 20 Carga horária utilizada: 10 hora(s) / Carga
horária excedente: 10 hora(s) (21/01/2019 a 20/02/2019)
ENAP - Ética e Serviço Público CH: 20 (21/01/2019 a 20/02/2019)
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
