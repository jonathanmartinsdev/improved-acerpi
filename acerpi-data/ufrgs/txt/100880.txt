Documento gerado sob autenticação Nº WJE.708.490.9CB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10237                  de  13/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°47705,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ENGENHEIRO-ÁREA, do Quadro
de Pessoal desta Universidade, MELINI MARIA MARQUETTI (Siape: 1746826 ),  para substituir   MAGDA DA
SILVEIRA ELKFURY (Siape: 1870553 ), Diretor do Departamento de Projetos e Planejamento vinculado à Vice-
Superintendência de Infraestrutura da SUINFRA, Código CD-4, em seu afastamento por motivo de férias, no
período de 14/11/2019 a 22/11/2019, com o decorrente pagamento das vantagens por 9 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
