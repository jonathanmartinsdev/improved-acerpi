Documento gerado sob autenticação Nº SZH.165.183.B9D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9477                  de  11/10/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°36518,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MARCELO LAZZARON LAMERS (Siape: 1734140 ),  para
substituir    ILMA  SIMONI  BRUM  DA  SILVA  (Siape:  1206479  ),  Diretor  do  ICBS,  Código  CD-3,  em  seu
afastamento por motivo de férias, no período de 15/10/2017 a 25/10/2017, com o decorrente pagamento das
vantagens por 11 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
