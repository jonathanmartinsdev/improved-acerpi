Documento gerado sob autenticação Nº NJN.268.041.DTR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7917                  de  02/09/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016  tendo em vista o disposto no artigo 7° do Decreto n° 3.555, 08 de agosto de 2000, no artigo 3°, inciso IV
da  Lei n° 10.520/2002,
RESOLVE:
Retificar a Portaria nº 7903 de 30/08/2019, no artigo 4º: 
onde se lê:
"Esta Portaria entre em vigor na data de sua publicação, e tem validade até 02/09/2020".
leia-se: 
"Esta Portaria entre em vigor na data de sua publicação, e tem validade até 29/08/2020".
 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
