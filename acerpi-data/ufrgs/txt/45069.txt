Documento gerado sob autenticação Nº FYX.765.986.UH4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8945                  de  26/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar  a  Portaria  n°  8827/2017,  de  21/09/2017,  publicada  no  Diário  Oficial  da  União  de
25/09/2017, que concedeu pensão com natureza vitalicia a Helena da Silva Degrazia, nos termos dos artigos
215 e 217, inciso III,  da Lei 8.112 de 11 de dezembro de 1990, em decorrência do falecimento de JOSE
FELIPPE  DEGRAZIA,  aposentado  no  cargo  de  Professor  do  Magistério  Superior  do  quadro  desta
Universidade. Processo nº 23078.015032/2017-68.
,
 
Onde se lê:
<... a partir de 21 de julho de 2017...>,
leia-se:
< ... a partir de 01 de setembro de 2017...>; ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
