Documento gerado sob autenticação Nº YOS.148.549.F83, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10077                  de  31/10/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor SERGIO MOACIR MARQUES, matrícula SIAPE n° 0357484, lotado e em exercício no Departamento
de Arquitetura da Faculdade de Arquitetura, da classe C  de Professor Adjunto, nível 02, para a classe C  de
Professor Adjunto, nível 03, referente ao interstício de 19/11/2014 a 18/11/2016, com vigência financeira a
partir de 19/11/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações  e  a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.505942/2017-38.
RUI VICENTE OPPERMANN
Reitor.
