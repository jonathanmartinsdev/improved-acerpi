Documento gerado sob autenticação Nº ZWN.089.092.0N3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3902                  de  07/05/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  CLARISSA  MARCELI  TRENTINI,  matrícula  SIAPE  n°  2444799,  lotada  no  Departamento  de
Psicologia do Desenvolvimento e da Personalidade do Instituto de Psicologia e com exercício no Instituto de
Psicologia, da classe D  de Professor Associado, nível 03, para a classe D  de Professor Associado, nível 04,
referente ao interstício de 05/05/2017 a 04/05/2019, com vigência financeira a partir de 05/05/2019, de
acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº
331/2017 do CONSUN. Processo nº 23078.508296/2019-22.
JANE FRAGA TUTIKIAN
Vice-Reitora.
