Documento gerado sob autenticação Nº YXU.467.227.63A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7044                  de  06/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Conceder afastamento parcial, no período de 16/08/2019 a 15/08/2020, para a servidora LISIANE
BERNARDO DA SILVA, ocupante do cargo de Técnico em Assuntos Educacionais - 701079, matrícula SIAPE
1908270,  lotada  na  Escola de Educação Física, Fisioterapia e Dança, para cursar o Mestrado Profissional em
Ensino em Saúde, oferecido pela UFRGS; conforme o Processo nº 23078.516476/2019-88.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
