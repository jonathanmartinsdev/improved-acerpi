Documento gerado sob autenticação Nº AQP.722.989.NK5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1667                  de  27/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a CLARICE MARIA DALL
AGNOL, matrícula SIAPE nº 0351718, no cargo de Professor Titular da Carreira do Magistério Superior, do
Quadro  desta  Universidade,  no  regime  de  dedicação  exclusiva,  com  exercício  no  Departamento  de
Assistência  e  Orientação  Profissional  da  Escola  de  Enfermagem,  com  proventos  integrais.  Processo
23078.000628/2018-44.
RUI VICENTE OPPERMANN
Reitor.
