Documento gerado sob autenticação Nº UIK.367.690.THK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7012                  de  08/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder Progressão por Mérito Profissional, nos termos do artigo 10-A da Lei nº 11.091, de 12 de
janeiro  de  2005,  e  da  Decisão nº  939,  de  21  de  novembro de  2008,  do  Conselho Universitário  desta
Universidade, para a servidora ZENAIDE GUIOMAR DA SILVA MOREIRA, matrícula SIAPE 0357976, Nível de
Classificação A , para o padrão de vencimento 16, com vigência a partir de 07 de julho de 2016.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
