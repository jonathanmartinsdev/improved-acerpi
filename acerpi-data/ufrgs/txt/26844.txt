Documento gerado sob autenticação Nº CKK.279.757.1MT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6613                  de  29/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme o Laudo Médico n°40100,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ADMINISTRADOR, do Quadro de
Pessoal desta Universidade, ALINE DA COSTA PEREIRA (Siape: 1788209 ),  para substituir   Volnei da Rocha
Delgado (Siape: 0358778 ),  Coordenador da Pós-Graduação Lato Sensu da PROPG, Código FG-1, em seu
afastamento por motivo de Laudo Médico do titular da Função, no período de 08/08/2016 a 12/08/2016, com
o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
