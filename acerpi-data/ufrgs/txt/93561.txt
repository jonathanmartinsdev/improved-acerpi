Documento gerado sob autenticação Nº KDJ.467.162.JUK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5545                  de  02/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°50716,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TÉCNICO EM SECRETARIADO, do
Quadro de Pessoal desta Universidade, RONISE FERREIRA DOS SANTOS (Siape: 1125011 ),  para substituir  
ANTONIO CARLOS THIESEN JUNIOR (Siape: 2145627 ), Gerente Administrativo do Instituto de Artes, Código
FG-1, em seu afastamento por motivo de férias, no período de 02/07/2019 a 10/07/2019, com o decorrente
pagamento das vantagens por 9 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
