Documento gerado sob autenticação Nº SQI.987.478.E6J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1242                  de  08/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  ANGELA  BORGES  MASUERO,  matrícula  SIAPE  n°  0358604,  lotada  e  em  exercício  no
Departamento de Engenharia Civil da Escola de Engenharia, da classe D  de Professor Associado, nível 04,
para a classe E  de Professor Titular,  referente ao interstício de 29/06/2015 a 15/12/2017, com vigência
financeira a partir de 16/12/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Decisão nº 232/2014 - CONSUN. Processo nº 23078.524077/2017-29.
RUI VICENTE OPPERMANN
Reitor.
