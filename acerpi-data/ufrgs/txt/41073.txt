Documento gerado sob autenticação Nº XOY.895.072.IQL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6265                  de  14/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora VERA SUSANA VARGAS
RIBEIRO,  ocupante do cargo de Assistente em Administração-701200, lotada na Faculdade de Medicina,
SIAPE 0355727, para 52% (cinquenta e dois por cento), a contar de 06/07/2017, tendo em vista a conclusão do
curso de Mestrado em Medicina: Ciências Médicas, conforme o Processo nº 23078.013027/2017-11.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
