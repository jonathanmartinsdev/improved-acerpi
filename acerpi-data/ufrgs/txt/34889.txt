Documento gerado sob autenticação Nº OAK.529.134.KPK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1850                  de  24/02/2017
  Declara  quanto  à  periodicidade  do
afastamento  parcial,  no  âmbito  da
Universidade Federal do Rio Grande do Sul
para os servidores técnico-administrativos
matriculados em cursos de Pós-Graduação
Stricto Sensu.
              O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
tendo  em  vista  a  Portaria  nº  5968,  de  10/08/2016,  que  orienta  quanto  aos  procedimentos  de
operacionalização  do  afastamento  parcial  no  âmbito  da  Universidade  para  os  servidores  técnico-
administrativos matriculados em cursos de pós-graduação Stricto Sensu;
 
RESOLVE:
              Declarar que a autorização de afastamento parcial passa a ser concedida por até 12 (doze) meses,
prorrogável por igual período, totalizando até 24 (vinte e quatro) meses para mestrado e 48 (quarenta e oito)
meses para doutorado.
              O pedido de renovação deverá ser encaminhado à Pró-Reitoria de Gestão de Pessoas, com 30 (trinta)
dias de antecedência, através de novo requerimento, no mesmo processo administrativo de concessão de
afastamento parcial.  É  necessário juntar  a  documentação exigida no artigo 6º  da Portaria  nº  5968,  de
10/08/2016, mantendo continuidade com a data do afastamento anterior.
RUI VICENTE OPPERMANN,
Reitor.
