Documento gerado sob autenticação Nº OTT.553.316.OIP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4899                  de  07/06/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  06/06/2019,   referente  ao  interstício  de
06/12/2017 a  05/06/2019,  para  o  servidor  OTÁVIO LUÍS  DA SILVA BARRADAS,  ocupante  do cargo de
Assistente em Administração - 701200, matrícula SIAPE 2317068,  lotado  na  Pró-Reitoria de Assuntos
Estudantis, passando do Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível
de  Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.512865/2019-34:
Formação Integral de Servidores da UFRGS I CH: 28 (05/09/2016 a 13/11/2018)
Uninter - Análise de marketing e comunicação política CH: 60 (17/07/2018 a 10/01/2019)
IFsul - Direito Administrativo CH: 40 Carga horária utilizada: 32 hora(s) / Carga horária excedente: 8 hora(s)
(10/01/2019 a 14/01/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
