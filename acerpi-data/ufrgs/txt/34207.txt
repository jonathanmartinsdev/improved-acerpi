Documento gerado sob autenticação Nº FUT.523.616.8VA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1324                  de  10/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  à  servidora  CLAUDIA  SAMPAIO
CORREA DA SILVA, ocupante do cargo de Psicólogo-área-701060, lotada no Instituto de Psicologia, SIAPE
1677254, para 75% (setenta e cinco por cento), a contar de 17/06/2016, tendo em vista a conclusão do curso
de Doutorado em Psicologia, conforme o Processo nº 23078.013383/2016-53.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
