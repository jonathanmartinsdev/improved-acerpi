Documento gerado sob autenticação Nº KPU.366.050.47D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7341                  de  13/08/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora NAIRA LISBOA FRANZOI, matrícula SIAPE n° 1220224, lotada e em exercício no Departamento de
Estudos Especializados da Faculdade de Educação, da classe D  de Professor Associado, nível 04, para a classe
E  de Professor Titular,  referente ao interstício de 10/06/2017 a 25/07/2019, com vigência financeira a partir
de 26/07/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e
a Decisão nº 331/2017 do CONSUN. Processo nº 23078.515022/2019-90.
JANE FRAGA TUTIKIAN
Vice-Reitora.
