Documento gerado sob autenticação Nº ENQ.998.100.F10, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7088                  de  06/09/2018
O DIRETOR DO DEPARTAMENTO DE ADMINISTRAÇÃO DE PESSOAL DA PROGESP DA UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183,
de 17 de outubro de 2008
RESOLVE
Retificar a Portaria n° 1285/1978, de 14/12/1978, que concedeu seis (06) seis meses de Licença
Especial a JAYME WERNER DOS REIS, Professor do Magistério Superior, Processo nº 48.548/78.
 
 
Onde se lê:                                                                                                                                                               
                                                                                                                                                                                                   
                                                                                                                                                                                                   
                                                                                                                                    
<referente ao decênio compreendido entre 30 de abril de 1967 e 29 de abril de 1977>,
leia-se:
<referente ao decênio compreendido entre 14 de abril de 1958 e 13 de abril de 1968>.
MARCELO SOARES MACHADO
Diretor do Departamento de Administração de Pessoal da PROGESP
