Documento gerado sob autenticação Nº INN.009.726.TTT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7889                  de  04/10/2016
O VICE-PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL,  no uso de suas atribuições,  considerando o disposto na Portaria nº  7633,  de 29 de
setembro de 2016, tendo em vista o que consta do Processo Administrativo n° 23078.009088/2016-01, do
Contrato nº 017/PROPLAN/NUDECON/2015, da Lei 10.520/02 e da Lei 8.666/93,
 
            RESOLVE:
 
 
           Aplicar à Empresa ONDREPSB RS LIMPEZA E SERVIÇOS ESPECIAIS LTDA, CNPJ 10.859.014/0001-19,
conforme atestado pela GERTE às fls. 01-04 (OF. 989/2016 - GERTE/PROGESP) e fls. 27-30, bem como pelo
NUDECON à fl. 44 do processo nº 23078. 009088/2016-01, a seguinte sanção administrativa:
 
ADVERTÊNCIA, prevista na alínea "a" da Cláusula Décima segunda c/c o caput, parágrafo
quarto da mesma  cláusula do referido Contrato, pelo descumprimento do dever de boa-fé
e prejuízo causado à comunidade, bem como pelas faltas sem reposição de posto e por
tratar-se da primeira ocorrência.
 
              Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
LUIS ROBERTO DA SILVA MACEDO
Vice-Pró-Reitor de Planejamento e Administração
