Documento gerado sob autenticação Nº CPD.075.258.CB7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7226                  de  07/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de Leandro Cesar de Godoy, Professor do Magistério Superior,
lotado e em exercício no Departamento de Zootecnia da Faculdade de Agronomia, com a finalidade de
participar do "6th International Workshop on the Biology of Fosh Gametes", em Ceské Budejovice, República
Tcheca, no período compreendido entre 03/09/2017 e 08/09/2017, incluído trânsito, com ônus limitado.
Solicitação nº 29508.
RUI VICENTE OPPERMANN
Reitor
