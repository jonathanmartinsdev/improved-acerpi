Documento gerado sob autenticação Nº FXS.988.853.98P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3206                  de  02/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria n° 1777/2016, de 08/03/2016, que concede Incentivo à Qualificação a CELSO
ALEGRANSI,  Técnico  em  Assuntos  Educacionais,  com  exercício  no  Núcleo  Acadêmico  da  Gerência
Administrativa do Instituto de Artes. Processo nº 23078.000623/2016-50.
 
Onde se lê:
"SIAPE 2036402",
leia-se:
"SIAPE 1036402".
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
