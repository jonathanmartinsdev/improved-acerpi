Documento gerado sob autenticação Nº HHN.771.811.NHE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             891                  de  28/01/2020
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Retificar a Portaria nº 89/2020, de 04/01/2020, que designou, temporariamente, CESAR AUGUSTO
MARCHIONATTI AVANCINI (Siape: 2174282) para substituir SAIONARA ARAUJO WAGNER (Siape: 4288393 ) ,
Processo SEI Nº 23078.501537/2020-46
 
onde se lê:
 
"...  em seu afastamento por motivo de férias,  no período de 06/01/2020 a 07/02/2020, com o
decorrente pagamento das vantagens por 33 dias...";
 
          leia-se:
 
"...   em seu afastamento por motivo de férias, no período de 06/01/2020 a 19/01/2020, com o
decorrente pagamento das vantagens por 14 dias...; ficando ratificados os demais termos.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
