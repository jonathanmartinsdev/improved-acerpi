Documento gerado sob autenticação Nº DXY.891.816.AVL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2993                  de  06/04/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de DENIS BORENSTEIN, Professor do Magistério Superior, lotado e
em exercício no Departamento de Ciências Administrativas da Escola de Administração, com a finalidade de
realizar  trabalho de  campo junto  à  Universidad de  Cuenca,  Equador,  no  período compreendido entre
23/04/2017 e 13/05/2017, incluído trânsito, com ônus limitado. Solicitação nº 26759.
RUI VICENTE OPPERMANN
Reitor
