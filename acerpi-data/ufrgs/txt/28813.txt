Documento gerado sob autenticação Nº EPW.436.389.MI9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8035                  de  07/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/09/2016,   referente  ao  interstício  de
11/03/2015 a 10/09/2016, para a servidora ILSE MARIA ZEN,  ocupante do cargo de Porteiro -  701458,
matrícula  SIAPE 0358204,   lotada  no  Instituto de Física,  passando do Nível  de Classificação/Nível  de
Capacitação C III, para o Nível de Classificação/Nível de Capacitação C IV, em virtude de ter realizado o(s)
seguinte(s) curso(s), conforme o Processo nº 23078.203316/2016-29:
Formação Integral de Servidores da UFRGS V CH: 123 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 3 hora(s) (22/05/2014 a 30/06/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
