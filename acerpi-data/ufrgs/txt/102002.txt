Documento gerado sob autenticação Nº NXP.446.191.TUS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10976                  de  10/12/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
RESOLVE
Designar  os  servidores   ROMOLO  AUGUSTO  TOLDO,  ocupante  do  cargo  de  Assistente  em
Administração,  lotado na Pró-Reitoria  de Planejamento e  Administração e com exercício  na Divisão de
Tombamento e Levantamento de Bens Móveis, PABLO CHAVES ORTIZ, ocupante do cargo de Administrador,
lotado na Pró-Reitoria de Planejamento e Administração e com exercício na Divisão de Tombamento e
Levantamento  de  Bens  Móveis,  RODRIGO  ALVAREZ  ALVES,  ocupante  do  cargo  de  Assistente  em
Administração,  lotado na Pró-Reitoria  de Planejamento e  Administração e com exercício  na Divisão de
Registro de Bens Móveis e Imóveis,  GRACIELLE PESAMOSCA DUARTE,  ocupante do cargo de Contador,
lotada na Pró-Reitoria de Planejamento e Administração e com exercício no Departamento de Contabilidade
e Finanças, SUSANA ELISA BERNO, ocupante do cargo de Engenheiro-área, lotada na Superintendência de
Infraestrutura e com exercício no Setor de Atualização e Controle de Cadastros, MARIBEL DOS SANTOS
NUNES, ocupante do cargo de Assistente de Laboratório, lotada no Instituto de Geociências e com exercício
no Seção de Infraestrutura do IGEO, para, sob a presidência do primeiro, comporem a Comissão Central de
Inventário de Bens Móveis desta Universidade, referente aos exercícios de 2019 e 2020.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
