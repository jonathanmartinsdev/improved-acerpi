Documento gerado sob autenticação Nº FCM.543.557.PFE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6813                  de  30/08/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°43772,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS, do Quadro de Pessoal desta Universidade, POLLIANE TREVISAN NUNES (Siape: 1978491 ),
 para substituir   CARLA MARIA BASTOS DOS SANTOS (Siape: 1088119 ), Diretor da Divisão de Planejamento e
Assessoria Técnica do DARE da PROREXT, Código FG-4, em seu afastamento por motivo de férias, no período
de 02/09/2018 a 06/09/2018, com o decorrente pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
