Documento gerado sob autenticação Nº FIV.760.964.NI1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6494                  de  21/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar ISABEL DE ABRANTES TIMM, Matrícula SIAPE 2316483, ocupante do cargo de Assistente
em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de
Gerente Administrativo da PRAE, Código SRH 1293, código FG-1, com vigência a partir da data de publicação
no Diário Oficial da União. Processo nº 23078.521490/2018-12.
JANE FRAGA TUTIKIAN
Vice-Reitora.
