Documento gerado sob autenticação Nº ISF.745.322.9F0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1904                  de  07/03/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a HELENA
SCHMID, matrícula SIAPE nº 0352625, no cargo de Professor Titular da Carreira do Magistério Superior, do
Quadro  desta  Universidade,  no  regime  de  quarenta  horas  semanais  de  trabalho,  com  exercício  no
Departamento de Medicina Interna da Faculdade de Medicina, com proventos integrais e incorporando a
vantagem pessoal de que trata a Lei nº 9.624, de 2 de abril de 1998, que assegurou o disposto no artigo 3º da
Lei nº 8.911, de 11 de julho de 1994. Processo 23078.000950/2018-73.
RUI VICENTE OPPERMANN
Reitor.
