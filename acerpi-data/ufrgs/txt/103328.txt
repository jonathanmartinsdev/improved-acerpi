Documento gerado sob autenticação Nº GWG.606.071.1PT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             252                  de  07/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 02/01/2020,  a ocupante do cargo de Professor do Magistério
Superior, classe Adjunto, do Quadro de Pessoal desta Universidade, PATRÍCIA SOUZA MARCHAND, matrícula
SIAPE n° 2689255, da função de Chefe Substituta do Depto de Estudos Especializados da Faculdade de
Educação, para a qual foi designada pela Portaria 238/2019, de 09/01/2019. Processo nº 23078.535257/2019-
06.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
