Documento gerado sob autenticação Nº ESB.246.165.P57, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8883                  de  04/11/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de RENATA PEREIRA DA CRUZ, Professor do Magistério Superior,
lotada e em exercício no Departamento de Plantas de Lavoura da Faculdade de Agronomia, com a finalidade
de  participar  da  "XXXI  Reunión  Argentina  de  Fisiología  Vegetal",  em Corrientes,  Argentina,  no  período
compreendido entre 13/11/2016 e 17/11/2016, incluído trânsito, com ônus limitado. Solicitação nº 24468.
RUI VICENTE OPPERMANN
Reitor
