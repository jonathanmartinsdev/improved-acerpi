Documento gerado sob autenticação Nº HPZ.571.172.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7543                  de  21/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de JOSE LUIS DUARTE RIBEIRO, Professor do Magistério Superior,
lotado no Departamento de Engenharia de Produção e Transportes da Escola de Engenharia e com exercício
na Secretaria de Desenvolvimento Tecnológico, com a finalidade de participar de Reunião do Comité Técnico
da RedEmprendia,  em Guadalajara,  México,  no período compreendido entre  06/10/2018 e  11/10/2018,
incluído trânsito, com ônus UFRGS (diárias e passagens). Solicitação nº 59437.
RUI VICENTE OPPERMANN
Reitor
