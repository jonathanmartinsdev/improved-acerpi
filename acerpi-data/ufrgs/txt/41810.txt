Documento gerado sob autenticação Nº FAY.936.969.BV5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6811                  de  26/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ALTAIR SORIA PEREIRA,  Professor do Magistério Superior,
lotado no Departamento de Engenharia dos Materiais da Escola de Engenharia e com exercício na Câmara de
Graduação,  com  a  finalidade  de  participar  de  conferência  junto  a  International  Association  for  the
Advancement of High Pressure Science and Technology, em Pequim, China, no período compreendido entre
17/08/2017 e 25/08/2017, com ônus limitado e realizar visita junto à Université Montpellier II, em Montpellier,
França,  no  período  compreendido  entre  26/08/2017  e  10/09/2017,  incluído  trânsito,  com ônus  CAPES.
Solicitação nº 29557.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
