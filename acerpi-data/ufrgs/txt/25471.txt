Documento gerado sob autenticação Nº NRD.222.797.LPI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5836                  de  04/08/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de LIVIA PEDERSEN DE OLIVEIRA, Secretário Executivo, lotada na
Pró-Reitoria  de  Pós-Graduação  e  com  exercício  no  Departamento  de  Apoio  à  Pós-Graduação,  com  a
finalidade de participar do "Santander Universities - Babson Entrepreneurship & Innovation Symposium for
Redemprendia  Fellows",  em  Boston,  Estados  Unidos,  no  período  compreendido  entre  31/08/2016  e
08/09/2016,  incluído  trânsito,  com ônus  UFRGS (Pró-Reitoria  de  Pós-Graduação  -  diárias  e  passagens).
Solicitação nº 21658.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
