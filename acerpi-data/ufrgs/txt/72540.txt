Documento gerado sob autenticação Nº PDQ.382.220.96D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9490                  de  23/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do País  de  JOAO MANOEL GOMES DA SILVA JUNIOR,  Professor  do
Magistério Superior, lotado no Departamento de Sistemas Elétricos de Automação e Energia da Escola de
Engenharia e com exercício no Programa de Pós-Graduação em Engenharia Elétrica, com a finalidade de
participar  da  "57th IEEE Conference on Decision and Control",  em Miami,  Estados Unidos,  no período
compreendido entre 14/12/2018 e 21/12/2018, incluído trânsito, com ônus pelo CNPq. Solicitação nº 61284.
RUI VICENTE OPPERMANN
Reitor
