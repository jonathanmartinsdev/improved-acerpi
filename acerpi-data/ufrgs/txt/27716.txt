Documento gerado sob autenticação Nº SDY.056.208.3QC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7303                  de  15/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°22335,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO DO
ENSINO BÁSICO,  TÉCNICO E TECNOLÓGICO,  do Quadro de Pessoal  desta Universidade,  ANA CRISTINA
CYPRIANO PEREIRA (Siape: 2174464 ),   para substituir   MARIA BERENICE DA COSTA MACHADO (Siape:
2299532 ), Coordenador da COMGRAD de Comunicação Social, Código FUC, em seu afastamento no país, no
período de 06/09/2016 a 07/09/2016, com o decorrente pagamento das vantagens por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
