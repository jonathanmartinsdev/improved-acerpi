Documento gerado sob autenticação Nº CQE.537.177.H9F, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4345                  de  20/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Retificar a Portaria n° 4257/2019, de 16/05/2019, que concede alteração de percentual de incentivo
à qualificação à ANGELA FERNANDES DA SILVA, Assistente em Administração, com exercício na Comissão
Interna de Supervisão, conforme Processo nº 23078.504796/2019-95.
 
Onde se lê:
"a contar de 30/04/2019",
leia-se:
"a contar de 28/02/2019".
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
