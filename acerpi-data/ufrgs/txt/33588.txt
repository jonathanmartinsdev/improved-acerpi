Documento gerado sob autenticação Nº KIZ.199.766.18L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             904                  de  26/01/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme o Laudo Médico n°42408,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de AUXILIAR EM ADMINISTRAÇÃO,
do Quadro de Pessoal  desta Universidade,  AMADEU PIO DE ALMEIDA NETO (Siape:  2053932 ),   para
substituir   MARILIA BORGES HACKMANN (Siape: 0756975 ), Diretor do Departamento de Atenção à Saúde da
PROGESP, Código CD-4, em seu afastamento por motivo de Laudo Médico do titular da Função, no período
de 16/01/2017 a 05/02/2017, com o decorrente pagamento das vantagens por 21 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
