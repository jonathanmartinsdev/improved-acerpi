Documento gerado sob autenticação Nº PIT.812.792.SNN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3867                  de  25/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Dispensar, a partir da data de publicação no Diário Oficial da União, o(a) ocupante do cargo de
Assistente  em  Administração  -  701200,  do  Nível  de  Classificação  DIV,  do  Quadro  de  Pessoal  desta
Universidade,  MARCO ANTONIO TEIXEIRA VIANNA,  CPF nº  45298319068,  matrícula  SIAPE 0356462 da
função de Diretor da Divisão da Vida Acadêmica do DCRD da PROGRAD, Código SRH 1457, Código FG-2, para
a qual foi designado(a) pela Portaria nº 5553/14 de 07/08/2014, publicada no Diário Oficial da União de
12/08/2014 por ter sido designado para outra função gratificada. Processo nº 23078.011988/2016-18.
RUI VICENTE OPPERMANN
Vice-Reitor
