Documento gerado sob autenticação Nº JPI.292.853.QFC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2455                  de  19/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  14/03/2019,   referente  ao  interstício  de
14/09/2017 a 13/03/2019,  para o servidor XISTON RODRIGUES DE RODRIGUES,  ocupante do cargo de
Assistente em Administração - 701200, matrícula SIAPE 2299320,  lotado  na  Faculdade de Veterinária,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.504403/2019-43:
Formação Integral de Servidores da UFRGS II CH: 44 (17/08/2016 a 27/11/2018)
Foco - Auxiliar de Biblioteca CH: 110 Carga horária utilizada: 76 hora(s) / Carga horária excedente: 34 hora(s)
(05/02/2019 a 24/02/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
