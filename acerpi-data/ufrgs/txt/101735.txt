Documento gerado sob autenticação Nº GOU.636.590.AJ8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10814                  de  03/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  01/12/2019,
correspondente ao grau Insalubridade Máxima, à servidora IONARA RODRIGUES SIQUEIRA, Identificação
Única  13091247,  Professor  do  Magistério  Superior,  com exercício  no  Programa de  Pós-Graduação  em
Ciências Biológicas: Farmacologia e Terapêutica do Instituto de Ciências Básicas da Saúde, observando-se o
disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de
1991,  por  exercer  atividades  em áreas  consideradas  Insalubres  conforme Laudo Pericial  constante  no
Processo nº 23078.530395/2019-91, Código SRH n° 24127.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
