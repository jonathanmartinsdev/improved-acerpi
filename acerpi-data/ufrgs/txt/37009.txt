Documento gerado sob autenticação Nº KOR.998.614.SIQ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3285                  de  18/04/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MARILIA  FORGEARINI  NUNES,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Ensino e Currículo da Faculdade de Educação, com a
finalidade de participar da "20ª Conferencia Europea sobre Lectura y Escritura", em Madrid, Espanha, no
período compreendido entre 01/07/2017 e 06/07/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa - diárias). Solicitação nº 26593.
RUI VICENTE OPPERMANN
Reitor
