Documento gerado sob autenticação Nº SEH.499.968.KUQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10782                  de  27/11/2017
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7793, de 03 de outubro de 2016
RESOLVE
Designar os servidores  JONAS MARTINHO RODRIGUES MACHADO, ocupante do cargo de Técnico
de Tecnologia da Informação, lotado na Superintendência de Infraestrutura e com exercício no Setor de
Tecnologia  e  Informação  da  SUINFRA,  JONES  RITTA  RODRIGUES,  ocupante  do  cargo  de  Técnico  em
Edificações,  lotado  na  Superintendência  de  Infraestrutura  e  com  exercício  no  Almoxarifado  de  Obras,
LUCIANA TEIXEIRA COSTA, ocupante do cargo de Técnico em Contabilidade, lotada na Superintendência de
Infraestrutura  e  com exercício  no Setor  Financeiro  da  Superintendência  de  Infraestrutura,  para,  sob a
presidência do primeiro, comporem Grupo de Trabalho para, a partir desta data, realizar Inventário para fins
de Balanço do Exercício de 2017 dos materiais em estoque no Almoxarifado de Obras da Superintendência
de Infraestrutura.
ANDREA PINTO LOGUERCIO
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
