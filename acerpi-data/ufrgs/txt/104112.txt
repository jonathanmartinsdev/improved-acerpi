Documento gerado sob autenticação Nº YIU.949.880.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             766                  de  22/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  MARCIA ELISA SOARES ECHEVESTE,  matrícula  SIAPE n°  1061442,  lotada e  em exercício  no
Departamento de Estatística do Instituto de Matemática e Estatística, da classe D  de Professor Associado,
nível 04, para a classe E  de Professor Titular,  referente ao interstício de 01/01/2018 a 09/01/2020, com
vigência financeira a partir de 10/01/2020, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de
2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.534056/2019-83.
RUI VICENTE OPPERMANN
Reitor.
