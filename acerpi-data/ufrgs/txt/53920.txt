Documento gerado sob autenticação Nº OFF.325.914.POC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3261                  de  04/05/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  PAULO  HENRIQUE  SCHNEIDER,  matrícula  SIAPE  n°  1466377,  lotado  no
Departamento de Química Orgânica do Instituto de Química,  como Vice-Presidente da Câmara de Pós-
Graduação, para substituir automaticamente o titular desta função em seus afastamentos ou impedimentos
regulamentares no período de 08/05/2018 e até 07/05/2020. Processo nº 23078.510057/2018-51.
RUI VICENTE OPPERMANN
Reitor.
