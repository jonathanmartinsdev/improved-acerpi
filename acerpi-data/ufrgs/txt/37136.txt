Documento gerado sob autenticação Nº PEJ.966.492.BV4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3375                  de  24/04/2017
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7793, de 03 de outubro de 2016
RESOLVE
Designar os servidores,  LUCIO ALBINO AMARO DA SILVA, ocupante do cargo de Engenheiro-área,
lotado na Superintendência de Infraestrutura e com exercício no Departamento de Projetos e Planejamento,
CAMILA SIMONETTI, ocupante do cargo de Engenheiro-área, lotada na Superintendência de Infraestrutura e
com exercício na Prefeitura Campus Centro, LUIZ CARLOS PINTO DA SILVA FILHO, ocupante do cargo de
Professor do Magistério Superior, lotado no Departamento de Engenharia Civil da Escola de Engenharia e
com exercício na Escola de Engenharia, para sob a presidência do primeiro, constituirem a Comissão de
Recebimento, em atendimento à alínea "b" do inciso I do artigo 73 da Lei 8666/93, para emissão do Termo de
Recebimento Definitivo da Obra do contrato 040/2014, cujo objeto é "Contratação de empresa para o
FORNECIMENTO E INSTALAÇÃO DE 03 (TRÊS) ELEVADORES DE PASSAGEIROS DO TIPO ELÉTRICO, NA
ESCOLA DE ENGENHARIA NOVA - CAMPUS CENTRO DA UFRGS, COM A PRESTAÇÃO DE ASSISTÊNCIA
TÉCNICA GRATUITA",  com prazo até  25/01/2018 quando se  encerra  o  prazo de vigência  do contrato.
Processo nº 23078.003111/2014-83.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
ANDREA PINTO LOGUERCIO
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
