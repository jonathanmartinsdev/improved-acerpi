Documento gerado sob autenticação Nº MYU.027.925.F07, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2106                  de  20/03/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 90 (noventa) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de
11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para o servidor
EDUARDO HERNANDES FERNANDES, com exercício na Divisão de Segurança e Medicina do Trabalho da Pró-
Reitoria  de Gestão de Pessoas,  a  ser  usufruída no período de 16/04/2018 a  14/07/2018,  referente  ao
quinquênio aquisitivo de 11/04/2011 a 10/04/2016, a fim de participar do Mestrado Acadêmico em Saúde e
Desenvolvimento Humano, conforme Processo nº 23078.500271/2018-08.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
