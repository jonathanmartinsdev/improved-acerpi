Documento gerado sob autenticação Nº ORG.081.329.6RD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5975                  de  08/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 20/08/2018, a ocupante do cargo de Assistente em Administração - 701200,
do Nível  de Classificação DIV,  do Quadro de Pessoal  desta Universidade, ALYNNI LUIZA RICCO AVILA,
matrícula SIAPE 1446236 da função de Secretária do PPG em Geografia, Código SRH 658, Código FG-7, para a
qual  foi  designada pela Portaria  nº  3458/2011 de 25/07/2011,  publicada no Diário Oficial  da União de
28/07/2011. Processo nº 23078.520040/2018-11.
JANE FRAGA TUTIKIAN
Vice-Reitora.
