Documento gerado sob autenticação Nº GTS.736.798.SUL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10591                  de  28/12/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  a  ocupante  do  cargo  de  Professor  do  Magistério  Superior,  classe  Associado,  desta
Universidade,  LIANE  WERNER,  matrícula  SIAPE  n°  1058105,  lotada  no  Departamento  de  Estatística  do
Instituto de Matemática e Estatística, para exercer a função de Coordenadora da Comissão de Extensão do
Instituto de Matemática, Código SRH 698, com vigência a partir da data deste ato, pelo período 2 anos,
ficando o pagamento de gratificação condicionado à disponibilidade de uma função gratificada. Processo nº
23078.535443/2018-56.
JANE FRAGA TUTIKIAN
Vice-Reitora.
