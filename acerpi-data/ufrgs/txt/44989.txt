Documento gerado sob autenticação Nº TLZ.405.604.BKB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8866                  de  22/09/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 18/09/2017, a ocupante do cargo de Assistente em Administração - 701200,
do Nível de Classificação DIV, do Quadro de Pessoal desta Universidade, ANA MARIA PERES ROSSI, matrícula
SIAPE  0351095  da  função  de  Coordenadora  do  Núcleo  Administrativo  da  Gerência  Administrativa  da
Faculdade de Medicina, Código SRH 1467, Código FG-4, para a qual foi designada pela Portaria nº 6837/14 de
19/09/2014, publicada no Diário Oficial da União de 25/09/2014. Processo nº 23078.516049/2017-38.
JANE FRAGA TUTIKIAN
Vice-Reitora.
