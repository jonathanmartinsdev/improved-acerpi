Documento gerado sob autenticação Nº OQX.927.775.FF2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8437                  de  16/09/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/09/2019,   referente  ao  interstício  de
28/04/2017 a 15/09/2019, para a servidora SAMARA NESSY MOTA, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 1155788,  lotada  na  Escola de Educação Física, Fisioterapia e
Dança, passando do Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.525232/2019-96:
Formação Integral de Servidores da UFRGS IV CH: 90 (26/04/2017 a 12/09/2019)
ILB - Excelência no Atendimento CH: 20 Carga horária utilizada: 10 hora(s) / Carga horária excedente: 10
hora(s) (28/06/2019 a 18/07/2019)
ENAP - Ética e Serviço Público CH: 20 (26/04/2019 a 08/05/2019)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
