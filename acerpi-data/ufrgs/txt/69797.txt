Documento gerado sob autenticação Nº VYH.142.970.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7350                  de  17/09/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar a Portaria n° 7029/2018, de 05/09/2018, que concedeu autorização para afastamento do
País a MARIA IVONE DOS SANTOS, Professor do Magistério Superior, com exercício no Departamento de
Artes Visuais do Instituto de Artes
Onde se lê: com ônus limitado,
leia-se:  com ônus CAPES/PROAP,  de 23 a 26/09/2018,  sendo o restante do período com ônus
limitado, ficando ratificados os demais termos. Solicitação nº 59462.
RUI VICENTE OPPERMANN
Reitor.
