Documento gerado sob autenticação Nº SHQ.442.219.OM3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1300                  de  06/02/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
                         Autorizar o afastamento no País de WAGNER MACHADO DA SILVA, ocupante do cargo de
Produtor Cultural,  com lotação na Faculdade de Educação e exercício no Núcleo de Apoio a Eventos e
Comunicação da Gerência Administrativa da Faculdade de Educação, com a finalidade de realizar estudos em
nível de Doutorado, junto à Pontíficia  Universidade Católica do Rio Grande do Sul, em Porto Alegre, no
período  compreendido  entre  01/08/2020  a  31/07/2021,   com  ônus  CAPES/PROSUC  .  Processo
23078.534927/2019-69.
JANE FRAGA TUTIKIAN
Vice-Reitora.
