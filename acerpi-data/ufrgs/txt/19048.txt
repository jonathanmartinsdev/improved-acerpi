Documento gerado sob autenticação Nº KBI.316.905.CR6, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1810                  de  09/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°26748,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, JULIANA CHARAO MARQUES (Siape: 3356096 ),  para
substituir   SERGIO REBELLO DILLENBURG (Siape: 0357663 ), Chefe do Depto de Geologia do Instituto de
Geociências, Código FG-1, em seu afastamento por motivo de férias, no período de 23/02/2016 a 05/03/2016,
com o decorrente pagamento das vantagens por 12 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
