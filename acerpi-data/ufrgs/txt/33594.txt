Documento gerado sob autenticação Nº RFT.299.093.H1H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             909                  de  27/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  LETICIA  FLECK  FADEL  MIGUEL,  matrícula  SIAPE  n°  2505231,  lotada  e  em  exercício  no
Departamento de Engenharia Mecânica da Escola de Engenharia, da classe D  de Professor Associado, nível
01, para a classe D  de Professor Associado, nível 02, referente ao interstício de 11/01/2015 a 10/01/2017,
com vigência financeira a partir de 11/01/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  197/2006-CONSUN,  alterada  pela  Decisão  nº
401/2013-CONSUN. Processo nº 23078.515783/2016-07.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
