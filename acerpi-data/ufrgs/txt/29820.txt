Documento gerado sob autenticação Nº ARA.728.436.QJH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8696                  de  26/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 60 (sessenta) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de
11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
LISANDRA ROSA DE VARGAS, com exercício na Seção de Arquivo Central da Pró-Reitoria de Planejamento e
Administração,  a  ser  usufruída  no  período  de  03/11/2016  a  01/01/2017,  referente  ao  quinquênio  de
14/08/2008 a 13/08/2013, a fim de realizar a elaboração da dissertação do curso de Mestrado em Memória
Social e Bens Culturais, conforme Processo nº 23078.023180/2016-75.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
