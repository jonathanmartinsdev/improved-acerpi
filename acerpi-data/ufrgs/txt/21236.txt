Documento gerado sob autenticação Nº ZDO.316.962.HKL, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3234                  de  03/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, MARIA AUXILIADORA CANNAROZZO TINOCO, CPF n° 83488154068, matrícula
SIAPE  n°  2612421,  lotada  no  Departamento  de  Engenharia  de  Produção  e  Transportes  da  Escola  de
Engenharia,  como Coordenadora  Substituta  da  COMGRAD de  Engenharia  de  Produção,  para  substituir
automaticamente o titular desta função em seus afastamentos ou impedimentos regulamentares no período
de 07/05/2016 até 06/05/2018. Processo nº 23078.008030/2016-31.
RUI VICENTE OPPERMANN
Vice-Reitor
