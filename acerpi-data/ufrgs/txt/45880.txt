Documento gerado sob autenticação Nº XPW.131.803.N17, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9517                  de  11/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  MARIA  TEREZA  FLORES  PEREIRA,  matrícula  SIAPE  n°  1854763,  lotada  e  em  exercício  no
Departamento de Ciências Administrativas da Escola de Administração, da classe C  de Professor Adjunto,
nível 03, para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 12/07/2015 a 24/07/2017,
com vigência financeira a partir de 25/07/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-
CONSUN. Processo nº 23078.513951/2017-01.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
