Documento gerado sob autenticação Nº ISC.615.801.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1862                  de  25/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Prorrogar a Licença para o Trato de Assuntos Particulares concedida pela Portaria n° 3187, de 02 de
maio de 2016, a MATEUS DE OLIVEIRA NEGREIROS, lotado no Instituto de Biociências e com exercício no
DDRH III, até 02 de fevereiro de 2020, nos termos do artigo 91 da Lei n° 8.112, de 11 de dezembro de 1990,
alterado pela Medida Provisória n° 2.225-45, de 4 de setembro de 2001, e conforme previsto na Portaria
Normativa SEGEMP/MP n° 04, de 06 de julho de 2012. Processo n° 23078.502517/2019-59.
RUI VICENTE OPPERMANN
Reitor.
