Documento gerado sob autenticação Nº ZAP.781.133.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5970                  de  15/07/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de DAIANA BACK GOUVEA, Odontólogo, lotada na Faculdade de
Odontologia  e  com exercício  no  Núcleo  Especializado da  Gerencia  Administrativa  da  ODONTO,  com a
finalidade de participar da "XIV Reunión Anual de la Sociedad Uruguaya de Investigación Odontológica", em
Montevidéu, Uruguai, no período compreendido entre 15/08/2019 e 18/08/2019, incluído trânsito, com ônus
UFRGS (Pró-Reitoria de Pesquisa: diárias). Solicitação nº 85432.
RUI VICENTE OPPERMANN
Reitor
