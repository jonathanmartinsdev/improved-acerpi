Documento gerado sob autenticação Nº JDC.102.299.MIU, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4763                  de  29/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de RODRIGO ALEX ARTHUR, Professor do Magistério Superior,
lotado e em exercício no Departamento de Odontologia Preventiva e Social da Faculdade de Odontologia,
com a finalidade de participar do "64th Congress of the European Organization for Caries Research", em
Oslo, Noruega, no período compreendido entre 03/07/2017 e 10/07/2017, incluído trânsito, com ônus UFRGS
(Pró-Reitoria de Pesquisa - diárias). Solicitação nº 28468.
RUI VICENTE OPPERMANN
Reitor
