Documento gerado sob autenticação Nº ZHG.541.083.JVV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1551                  de  20/02/2018
  Designa  Comissão  Permanente  de
Verificação  de  Documentos  da  Condição
de  Pessoa  com  Deficiência  para  os
Processos Seletivos de Ingresso nos cursos
de Graduação da UFRGS.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Designar
Membros
LIVIA PEDERSEN DE OLIVEIRA - Coordenadora do DECORDI/PROGRAD
ADRIANA MARIA ARIOLI - Coordenadora do INCLUIR
CARLA SKILHAN DE ALMEIDA - Docente do Curso de Fisioterapia
DENISE BALEM YATES - Psicóloga - Centro Interdisciplinar de Pesquisa e Atenção à Saúde
FLAVIA WAGNER - Psicóloga - Centro Interdisciplinar de Pesquisa e Atenção à Saúde
LOURENCO BRITO FELIN - Assistente Social - Pró-Reitoria de Assuntos Estudantis
LUCIANO PALMEIRO RODRIGUES - Docente do Curso de Fisioterapia
 
Membros Observadores:
ADILSO LUÍS PIMENTEL CORLASSOLI - Conselho Estadual das Pessoas com Deficiência
CLÁUDIA LUCKOW MEYER - Conselho Estadual das Pessoas com Deficiência
 
para, sob a presidência da primeira, compor a Comissão Permanente de Verificação de Documentos da
Condição de Pessoa com Deficiência para os processos seletivos de ingresso nos cursos de graduação da
UFRGS.
RUI VICENTE OPPERMANN,
Reitor.
