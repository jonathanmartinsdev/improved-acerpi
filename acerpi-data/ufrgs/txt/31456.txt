Documento gerado sob autenticação Nº ASX.760.478.LAR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9908                  de  15/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder prorrogação de afastamento parcial,  no período de 01/01/2017 a 31/03/2017, para a
servidora DANIELA CAON GUERRA, ocupante do cargo de Analista de Tecnologia da Informação - 701062,
matrícula SIAPE 2054012,  lotada  na  Faculdade de Medicina, para cursar Mestrado em Educação, conforme
o Processo nº 23078.022535/2016-17.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
