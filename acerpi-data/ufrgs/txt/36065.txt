Documento gerado sob autenticação Nº MXZ.208.953.0QV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2704                  de  28/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir da data de publicação no Diário Oficial da União, o ocupante do cargo de Analista
de  Tecnologia  da  Informação  -  701062,  do  Nível  de  Classificação  EIII,  do  Quadro  de  Pessoal  desta
Universidade, MAURO ALMEIDA DIAS DE CASTRO, matrícula SIAPE 0354320, da função de Chefe da Divisão
de Implantação e Manutenção do Depto de Infraestrutura de TI vinc ao CPD, Código SRH 949, Código FG-5,
para a qual foi designado pela Portaria nº 7071/14 de 26/09/2014, publicada no Diário Oficial da União de
09/10/2014, por ter sido designado para outra função gratificada. Processo nº 23078.004213/2017-69.
JANE FRAGA TUTIKIAN
Vice-Reitora.
