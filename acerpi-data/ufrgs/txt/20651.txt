Documento gerado sob autenticação Nº IZB.986.675.UPT, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2734                  de  13/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012do Magnífico
Reitor e conforme processo nº23078.005910/2016-56
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, RAFAEL GOMES DIONELLO (Siape: 1565975),  para
substituir  automaticamente  RENATO  LEVIEN  (Siape:  0358815  ),  Diretor  da  Estação  Experimental
Agronômica,  Código:  CD-4,  em  seu  afastamento  por  motivo  de  férias,  no  período  de  16/01/2016  a
09/02/2016, com o decorrente pagamento das vantagens por 25 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
