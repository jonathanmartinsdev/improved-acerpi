Documento gerado sob autenticação Nº YPF.475.296.ORR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10921                  de  04/12/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, e tendo em vista o que consta na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
        Autorizar o afastamento no País de PATRICIA AUGUSTA POSPICHIL CHAVES LOCATELLI, ocupante do
cargo de Secretário Executivo, com lotação na Pró-Reitoria de Pesquisa e exercício na Divisão de Fomento à
Pesquisa, com a finalidade de concluir estudos em nível de Doutorado, junto à Universidade Federal do Rio
Grande do Sul,  no período compreendido entre 05/03/2018 e 24/11/2018, com ônus limitado. Processo
23078.039422/13-10.
JANE FRAGA TUTIKIAN,
Vice-Reitora.
