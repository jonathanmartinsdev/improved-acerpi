Documento gerado sob autenticação Nº YNH.078.323.605, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8723                  de  18/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor EDUARDO AUGUSTO RIFFEL PACHECO, ocupante do cargo de  Assistente em
Administração - 701200, lotado na Pró-Reitoria de Extensão, SIAPE 2419473, o percentual de 25% (vinte e
cinco por cento) de Incentivo à Qualificação, a contar de 25/08/2017, tendo em vista a conclusão do curso de
Graduação em Administração - Bacharelado, conforme o Processo nº 23078.514145/2017-41.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
