Documento gerado sob autenticação Nº SPZ.917.091.AK1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1737                  de  28/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora CÁTIA SORAIA JESUS, ocupante do cargo de  Assistente em Administração -
701200, lotada na Escola de Educação Física, Fisioterapia e Dança, SIAPE 3011568, o percentual de 30% (trinta
por cento) de Incentivo à Qualificação, a contar de 31/01/2018, tendo em vista a conclusão do curso de
Direito do Consumidor e Direitos Fundamentais, conforme o Processo nº 23078.501913/2018-88.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
