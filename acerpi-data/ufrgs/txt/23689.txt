Documento gerado sob autenticação Nº DQN.009.887.CT8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4544                  de  22/06/2016
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de
2013
RESOLVE
Conceder à servidora DULCE INES RODRIGUES OLIVEIRA,  ocupante do cargo de  Técnico em
Nutrição e Dietética - 701252, lotada no Colégio de Aplicação, SIAPE 1736353, o percentual de 25% (vinte e
cinco por cento) de Incentivo à Qualificação, a contar de 25/04/2016, tendo em vista a conclusão do curso de
Graduação em Serviço Social, conforme o Processo nº 23078.002607/2016-00.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
