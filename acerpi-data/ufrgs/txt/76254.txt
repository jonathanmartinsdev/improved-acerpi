Documento gerado sob autenticação Nº IMH.754.418.VG8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1259                  de  04/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  24/01/2019,   referente  ao  interstício  de
22/07/2017 a 23/01/2019, para o servidor ISRAEL DA SILVA AQUINO, ocupante do cargo de Técnico em
Assuntos Educacionais - 701079, matrícula SIAPE 2668783,  lotado  no  Instituto de Psicologia, passando do
Nível de Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.501796/2019-33:
Formação Integral de Servidores da UFRGS II CH: 40 (17/03/2016 a 07/11/2016)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 40 (18/08/2017 a 07/09/2017)
ILB - Deveres, proibições e responsabilidades do servidor público federal CH: 60 (29/06/2017 a 04/08/2017)
ILB - Introdução ao controle interno CH: 40 (05/04/2018 a 25/04/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
