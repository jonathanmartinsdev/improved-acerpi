Documento gerado sob autenticação Nº TEB.557.158.FCL, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2597                  de  11/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar  ROSANGELA  NUNES  RODRIGUES,  CPF  nº  47182040034,  Matrícula  SIAPE  1090163,
ocupante do cargo de Contador, Código 701015, do Quadro de Pessoal desta Universidade, para exercer a
função de Coordenadora do Núcleo de Contadoria - NC da Procuradoria Geral, Código SRH 370, Código FG-4,
com vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.018141/11-71.
RUI VICENTE OPPERMANN
Vice-Reitor
