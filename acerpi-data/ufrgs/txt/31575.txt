Documento gerado sob autenticação Nº MNK.329.307.C77, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10016                  de  19/12/2016
  Nomeação do Presidente do Hospital  de
Clínicas de Porto Alegre.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições e
tendo em vista o disposto no Artigo 25, § 1º, do Estatuto do Hospital de Clínicas de Porto Alegre, no Artigo 29
do Estatuto da UFRGS e a Decisão nº 395/2016 do Egrégio Conselho Universitário desta Universidade, de
acordo com o Processo 23078.025088/2016-40
RESOLVE:
Nomear a ocupante do cargo de Professor Titular da Carreira do Magistério Superior, do Quadro de
Pessoal desta Universidade, NADINE OLIVEIRA CLAUSELL, para exercer o cargo de Presidente da Empresa
Pública Hospital de Clínicas de Porto Alegre.
RUI VICENTE OPPERMANN,
Reitor.
