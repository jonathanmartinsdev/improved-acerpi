Documento gerado sob autenticação Nº LOW.701.897.2RB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1081                  de  05/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar  CRISTIANE  SILVA  DE  OLIVEIRA,  Matrícula  SIAPE  1734147,  ocupante  do  cargo  de
Nutricionista-habilitação, Código 701055, do Quadro de Pessoal desta Universidade, para exercer a função de
Chefe da Seção do RU1 da Divisão de Alimentação do DIE da PRAE, Código SRH 1294, código FG-3, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.501627/2018-12.
RUI VICENTE OPPERMANN
Reitor.
