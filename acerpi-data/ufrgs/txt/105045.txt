Documento gerado sob autenticação Nº AFE.802.008.2TJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1391                  de  07/02/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  partir  de  11/02/2020,  a  ocupante  do  cargo  de  Contador  -  701015,  do  Nível  de
Classificação  EIV,  do  Quadro  de  Pessoal  desta  Universidade,  ANGELA  TERESA  JUNG,  matrícula  SIAPE
1728518, da função de Chefe da Seção de Escrituração da Despesa/DAD/DCF/PROPLAN, Código SRH 449,
Código FG-5, para a qual foi designada pela Portaria nº 3041/2016, de 25/04/2016, publicada no Diário Oficial
da União de 28/04/2016. Processo nº 23078.502413/2020-88.
JANE FRAGA TUTIKIAN
Vice-Reitora.
