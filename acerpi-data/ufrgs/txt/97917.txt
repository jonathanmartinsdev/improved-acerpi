Documento gerado sob autenticação Nº EYN.376.101.9JL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8262                  de  10/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora ANA LÚCIA CELTAN, ocupante do cargo de Assistente em Administração -
701200, lotada no Instituto de Psicologia, SIAPE 2056800, o percentual de 25% (vinte e cinco por cento) de
Incentivo à Qualificação, a contar de 05/09/2019, tendo em vista a conclusão do Curso Superior em Ciências
Jurídicas e Sociais - Bacharelado, conforme o Processo nº 23078.524360/2019-12.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
