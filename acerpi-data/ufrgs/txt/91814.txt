Documento gerado sob autenticação Nº YJF.493.620.PEG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4498                  de  23/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  à  servidora  JANAINA  CARRION
WICKERT, ocupante do cargo de Técnico de Laboratório Área-701244, lotada no Centro de Estudos Costeiros,
Limnológicos e Marinhos, SIAPE 2391826, para 75% (setenta e cinco por cento), a contar de 27/06/2018,
tendo em vista a conclusão do Doutorado em Biologia Animal, conforme o Processo nº 23078.515936/2018-
70.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
