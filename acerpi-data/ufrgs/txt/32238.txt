Documento gerado sob autenticação Nº XJB.118.890.6AA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             143                  de  05/01/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7633, de 29 de
setembro de 2016
            RESOLVE:
 
           Considerando o que consta do Processo Administrativo n° 23078.002279/2016-33, da Lei 10.520/02, do
contrato nº 016/PROPLAN/NUDECON/2016 e da Lei 8.666/93,
           Aplicar a sanção administrativa de ADVERTÊNCIA, prevista na alínea "a", cláusula décima primeira do
contrato, à Empresa HORTO CENTRAL MARATAÍSES LTDA, CNPJ nº: 39.818.737/0001-51  pelo atraso na
entrega do item 03 do Termo de Referência, conforme atestado pela DAL às fls. 96-verso, 99 a 107 e 137,
bem como pelo NUDECON fl. 140 do processo administrativo supracitado.
 
             Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
LUIS ROBERTO DA SILVA MACEDO
Pró-Reitor de Planejamento e Administração em exercício
