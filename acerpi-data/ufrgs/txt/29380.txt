Documento gerado sob autenticação Nº KPK.617.101.IDN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8443                  de  19/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de Álvaro Krüger Ramos, Professor do Magistério Superior, lotado
e em exercício no Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística, com
a finalidade de  realizar visita  à Universidade de Princeton, Estados Unidos, no período compreendido entre
04/12/2016 e 21/12/2016, incluído trânsito, com ônus CAPES/PROAP. Solicitação nº 23590.
RUI VICENTE OPPERMANN
Reitor
