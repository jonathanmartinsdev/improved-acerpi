Documento gerado sob autenticação Nº NCM.561.711.462, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7714                  de  27/09/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, , no uso de suas atribuições que lhe foram conferidas pela Portaria nº8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°58579,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MONICA PIENIZ (Siape: 2771237 ),  para substituir  
MARIA BERENICE DA COSTA MACHADO (Siape: 2299532 ),  Chefe do Depto de Comunicação da FABICO,
Código FG-1,  em seu afastamento no país,  no período de 28/09/2018 a 30/09/2018,  com o decorrente
pagamento das vantagens por 3 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
