Documento gerado sob autenticação Nº ZEP.898.417.I7C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10668                  de  29/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria de Nomeação nº 5427 de 23 de Outubro de 2009, publicada em 28 de Outubro de 2009. 
 
Onde se lê:
" ... AUXILIAR DE ENFERMAGEM - CLASSE D..."
 
Leia-se
" ... AUXILIAR DE ENFERMAGEM - CLASSE C ...", ficando ratificados os demais termos. 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
