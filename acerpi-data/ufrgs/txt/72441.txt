Documento gerado sob autenticação Nº XBN.246.660.96D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9418                  de  21/11/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Declarar  que a  aposentadoria  concedida a  ISABEL MENDES PRUNES,  matrícula  SIAPE 356651,
através da portaria nº 2862, de 28 de outubro de 2004, publicada no Diário Oficial da União do dia 3 de
novembro subsequente, passa a ser no cargo de Assistente em Administração, nível de classificação D, nível
de  capacitação  II,  padrão  11,  conforme  determinação  judicial  contida  no  processo  nº  5059584-
71.2018.4.04.7100/RS. Processo nº 23078.528757/2018-01.
RUI VICENTE OPPERMANN
Reitor.
