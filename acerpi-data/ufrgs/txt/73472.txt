Documento gerado sob autenticação Nº JSM.959.074.965, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10093                  de  13/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Alterar, a partir da data deste ato, o regime de trabalho de 20 horas semanais para Dedicação
Exclusiva, conforme o disposto no artigo 15, inciso I, do Anexo ao Decreto nº 94.664, de 23 de julho de 1987,
Decreto n° 94.664/87 e Lei n° 12.772/2012 atribuído ao Professor SERGIO MOACIR MARQUES, com exercício
no(a) Departamento de Arquitetura desta Universidade. Processo n° 23078.507067/2018-18.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
