Documento gerado sob autenticação Nº DNR.344.894.OQM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5418                  de  20/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/07/2016,   referente  ao  interstício  de
01/03/2005 a 10/07/2016, para o servidor CELSO FRIDOLINO BOTH, ocupante do cargo de Contramestre-
ofício - 701423, matrícula SIAPE 0358103,  lotado  na  Superintendência de Infraestrutura, passando do Nível
de Classificação/Nível de Capacitação C I, para o Nível de Classificação/Nível de Capacitação C II, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.014700/2016-59:
Formação Integral de Servidores da UFRGS III  CH: 63 Carga horária utilizada: 60 hora(s) / Carga horária
excedente: 3 hora(s) (27/08/2008 a 28/06/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
