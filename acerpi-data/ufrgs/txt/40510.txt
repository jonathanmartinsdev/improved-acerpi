Documento gerado sob autenticação Nº XTX.030.192.LRO, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5890                  de  06/07/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar  a  Portaria  n°  5665/2017,  de  29/06/2017,  publicada  no  Diário  Oficial  da  União  de
03/07/2017, que Declarou Vaga a função de Secretária da Procuradoria-Geral, ocupada por VIVIAN ROSE
NUNES, Assistente em Administração. Processo nº 23078.012143/2017-12.
 
 
Onde se lê:
"...Declarar vaga, a partir de 03/07/2017, ..."
leia-se:
"...Declarar vaga, a partir de 04/07/2017, ...", ficando ratificados os demais termos.
RUI VICENTE OPPERMANN
Reitor.
