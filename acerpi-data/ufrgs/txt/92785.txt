Documento gerado sob autenticação Nº XMR.754.646.4BP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5575                  de  03/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor ANDRE MENDES RIBEIRO ZANELLA, ocupante do cargo de Assistente em
Administração - 701200, lotado na Pró-Reitoria de Pós-Graduação, SIAPE 3129601, o percentual de 25% (vinte
e cinco por cento) de Incentivo à Qualificação, a contar de 11/06/2019, tendo em vista a conclusão do curso
de Graduação em Administração de Empresas, conforme o Processo nº 23078.515129/2019-38.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
