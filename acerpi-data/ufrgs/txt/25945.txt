Documento gerado sob autenticação Nº DIL.691.170.C26, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6039                  de  15/08/2016
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 5227, de 01 de outubro de 2012
e, tendo em vista o que consta dos Processos Administrativos n° 23078.002408/09-75 e 23078.008732/2015-
34, do Contrato nº 181/PROPLAN/DELIT/2009, da Lei 10.520/02 e ainda da Lei 8.666/93,
 
RESOLVE:
 
 
Aplicar a sanção administrativa de MULTA no montante de R$ 16.256,45 (dezesseis mil, duzentos e cinquenta
e seis reais e quarenta e cinco centavos), conforme demonstrativo de cálculo à fl. 110, prevista na alínea 'b'
da  Cláusula  Décima-segunda  do  Contrato,  à  Empresa  MULTIÁGIL  LIMPEZA,  PORTARIA  E  SERVIÇOS
ASSOCIADOS LTDA, CNPJ n.º 03.149.832/0001-62, pelos descumprimentos contratuais: não cumprimento de
rotinas de trabalho, postos sem cobertura, falta do uso de EPI, atraso no pagamento de Vales transporte e
alimentação e não cumprimento de política ambiental; conforme atestado pela GERTE às fls. 01-11 (OF.
692/2015) e fls. 106-107, bem como pelo NUDECON à fl. 108 do processo nº 23078.008732/2015-34.
 
Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
ARIO ZIMMERMANN
Pró-Reitor de Planejamento e Administração
