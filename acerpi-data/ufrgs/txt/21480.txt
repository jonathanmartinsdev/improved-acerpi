Documento gerado sob autenticação Nº XAO.269.861.654, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3322                  de  04/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor DANIEL GUIMARÃES GERARDI, matrícula SIAPE n° 1689907, com exercício no Departamento de
Medicina Animal da Faculdade de Veterinária, da classe C  de Professor Adjunto, nível 03, para a classe C  de
Professor Adjunto, nível 04, referente ao interstício de 25/02/2014 a 24/02/2016, com vigência financeira a
partir de 29/04/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela Decisão nº
401/2013-CONSUN. Processo nº 23078.008446/2016-50.
RUI VICENTE OPPERMANN
Vice-Reitor
