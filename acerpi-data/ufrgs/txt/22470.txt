Documento gerado sob autenticação Nº GJC.055.253.9N9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3786                  de  23/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Declarar vaga, a partir de 22/03/2016, a função de Coordenador da Coordenação Acadêmica da
SEAD, Código SRH 1364, Código FG-1, desta Universidade, tendo em vista a Exoneração Cargo Efetivo a
Pedido/Art.  34,  Lei  8112/90  de  RAFAEL  PIZZOLATO,  CPF  n°  910584060,  matrícula  SIAPE  n°  2188907,
conforme Portaria n° 3410, de 10/05/2016, publicada no Diário Oficial da União do dia 11/05/2016. Processo
nº 23078.011982/2016-32.
RUI VICENTE OPPERMANN
Vice-Reitor
