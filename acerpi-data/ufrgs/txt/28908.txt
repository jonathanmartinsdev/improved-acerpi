Documento gerado sob autenticação Nº IFT.135.914.1JC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8125                  de  10/10/2016
A VICE-REITORA NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias, 
RESOLVE
Nomear a ocupante do cargo de Técnico em Assuntos Educacionais, do Quadro de Pessoal desta
Universidade,  LAURA  WUNSCH,  matrícula  SIAPE  1654277,  para  exercer  o  cargo  de  Vice-Secretária  de
Educação a Distância, Código SRH 959, código CD-4, com vigência a partir da data de publicação no Diário
Oficial da União. Processo nº 23078.022163/2016-11.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
