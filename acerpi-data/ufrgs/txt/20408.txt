Documento gerado sob autenticação Nº PJI.453.148.7D9, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2837                  de  15/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  24/03/2016,   referente  ao  interstício  de
23/09/2014  a  23/03/2016,  para  a  servidora  CATHERINE  DA  SILVA  CUNHA,  ocupante  do  cargo  de
Bibliotecário-documentalista - 701010, matrícula SIAPE 1830581,  lotada  na  Biblioteca Central, passando
do Nível de Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.006556/2016-87:
Formação Integral de Servidores da UFRGS II CH: 53 (07/05/2013 a 18/03/2016)
SENAI - Assistente de Conservação Preventiva CH: 212 Carga horária utilizada: 127 hora(s) / Carga horária
excedente: 85 hora(s) (11/02/2014 a 16/06/2014)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
