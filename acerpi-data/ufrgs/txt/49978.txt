Documento gerado sob autenticação Nº BKL.974.735.14T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             772                  de  27/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  09/08/2017,
correspondente ao grau Insalubridade Média,  ao servidor BRUNO GARCIA DA LUZ,  Identificação Única
24178497,  Auxiliar  de  Veterinária  e  Zootecnia,  com  exercício  no  Departamento  de  Patologia  Clínica
Veterinária da Faculdade de Veterinária, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de
1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas
Insalubres conforme Laudo Pericial constante no Processo nº 23078.517492/2017-26, Código SRH n° 23383 e
Código SIAPE 2018000298.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
