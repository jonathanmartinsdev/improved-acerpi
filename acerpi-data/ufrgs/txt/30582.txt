Documento gerado sob autenticação Nº XCA.397.347.PQ8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9254                  de  20/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Remover, a partir de 1 de novembro de 2016,  de acordo com o artigo 36, parágrafo único, inciso II
da Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de
1997, RENATA PEREIRA FAGUNDES CORREA, ocupante do cargo de Técnico de Laboratório Área, Ambiente
Organizacional Ciências Biológicas, Código 701244, Classe D, Nível de Capacitação III, Padrão de Vencimento
06, SIAPE nº. 1678600 do Instituto de Biociências para a lotação Faculdade de Farmácia, com novo exercício
na Central Analítica.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
