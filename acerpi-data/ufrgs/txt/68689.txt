Documento gerado sob autenticação Nº LBP.470.950.MDC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6587                  de  23/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 01/10/2018, a ocupante do cargo de Administrador - 701001, do Nível de
Classificação EI, do Quadro de Pessoal desta Universidade, BARBARA QUATRIN TIELLET DA SILVA, matrícula
SIAPE 1964370 da função de Gerente Administrativo da PROPLAN, Código SRH 1309, Código FG-1, para a qual
foi designada pela Portaria nº 2671/2017 de 28/03/2017, publicada no Diário Oficial da União de 29/03/2017.
Processo nº 23078.521768/2018-51.
JANE FRAGA TUTIKIAN
Vice-Reitora.
