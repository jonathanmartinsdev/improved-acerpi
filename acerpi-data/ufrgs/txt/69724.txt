Documento gerado sob autenticação Nº HOM.350.728.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7301                  de  14/09/2018
  Designa  Comissão  para  coordenar  a
realização  de  concursos  públicos  para
cargos  técnico-administrativos  em
educação.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Art. 1º Designar, sob a coordenação da primeira, a Comissão que coordenará os procedimentos
necessários para a realização de concursos públicos para cargos técnico-administrativos em educação da
Universidade Federal do Rio Grande do Sul:
 ELIANA VENTORINI
 CRISTIANE BASSO
 GABRIELA NUNES D'AVILA
 VINICIUS FERRO
 
Art. 2º Revogar a Portaria 9298, de 06/10/2017.
RUI VICENTE OPPERMANN,
Reitor.
