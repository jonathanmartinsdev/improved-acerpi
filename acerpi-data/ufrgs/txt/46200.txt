Documento gerado sob autenticação Nº OSQ.514.379.MLI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9753                  de  20/10/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  PAULO  ANTONIO  DE  MENEZES  PEREIRA  DA  SILVEIRA,
Professor do Magistério Superior, lotado no Departamento de Artes Visuais do Instituto de Artes e com
exercício no Programa de Pós-Graduação em Artes Visuais, com a finalidade de participar do evento "Livres
et Revues d'Artistes: une perspective brésilienne"; montagem da exposição e palestras na Université de
Rennes 2, em Rennes, França, no período compreendido entre 23/11/2017 e 03/12/2017, incluído trânsito,
com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 31507.
RUI VICENTE OPPERMANN
Reitor
