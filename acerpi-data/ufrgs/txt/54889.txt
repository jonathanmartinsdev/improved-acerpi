Documento gerado sob autenticação Nº BAP.151.148.6CN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3982                  de  30/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°36811,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, VALESCA DALL ALBA (Siape: 1457834 ),  para substituir  
MARIO REIS ALVARES DA SILVA (Siape: 1253587 ),  Coordenador do PPG: Ciências em Gastroenterologia,
Código FUC, em seu afastamento por motivo de férias, no período de 06/06/2018 a 12/06/2018, com o
decorrente pagamento das vantagens por 7 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
