Documento gerado sob autenticação Nº BDV.488.153.LDQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2611                  de  24/03/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de THAIS STEEMBURGO, Professor do Magistério Superior, lotada
e em exercício no Departamento de Nutrição da Faculdade de Medicina, com a finalidade de participar do
"21st International Congress of Nutrition: From Sciences to Nutrition Security", em Buenos Aires, Argentina,
no período compreendido entre 14/10/2017 e 21/10/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria
de Pesquisa - diárias). Solicitação nº 26664.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
