Documento gerado sob autenticação Nº TEJ.459.131.QKA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7112                  de  12/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  01/09/2016,   referente  ao  interstício  de
20/07/2010 a 31/08/2016, para a servidora DANIELLE FINAMOR REZES DE SOUZA, ocupante do cargo de
Auxiliar de Creche - 701410,  matrícula SIAPE 1092105,  lotada  na  Faculdade de Ciências Econômicas,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  C  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  C  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.019547/2016-56:
Formação Integral de Servidores da UFRGS I CH: 21 (21/07/2010 a 27/11/2015)
Portal Educação - Moodle: Conceitos e Administração CH: 80 Carga horária utilizada: 69 hora(s) / Carga
horária excedente: 11 hora(s) (06/06/2016 a 05/08/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
