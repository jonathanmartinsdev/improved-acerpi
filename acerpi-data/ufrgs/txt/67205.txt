Documento gerado sob autenticação Nº QDC.510.791.B2S, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5675                  de  30/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Homologar o parecer que aprova o servidor técnico-administrativo ANDRE LUIS LUCERO BATISTA,
ocupante do cargo de Médico Veterinário, no estágio probatório cumprido no período de 24/07/2015 até
24/07/2018, fazendo jus, a partir desta data, à estabilidade no serviço público federal.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
