Documento gerado sob autenticação Nº NVO.401.269.2QV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7124                  de  08/08/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a LENI VIEIRA DORNELLES,
matrícula SIAPE nº 1150046, no cargo de Professor Titular da Carreira do Magistério Superior, do Quadro
desta  Universidade,  no  regime  de  dedicação  exclusiva,  com  exercício  no  Departamento  de  Estudos
Especializados da Faculdade de Educação, com proventos integrais. Processo 23078.514423/2019-22.
RUI VICENTE OPPERMANN
Reitor.
