Documento gerado sob autenticação Nº YLY.136.510.96D, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10500                  de  27/12/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5062729-38.2018.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo III da Portaria n° 1874, de
13/07/2006, do servidor ANTONIO CARLOS CORREA DA SILVA, matrícula SIAPE n° 0358195, desligado no
cargo de Bombeiro Hidráulico - 701632, para o nível II, conforme o Processo nº 23078.535198/2018-87.
RUI VICENTE OPPERMANN
Reitor
