Documento gerado sob autenticação Nº MQT.494.372.RNF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4471                  de  19/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, RICARDO BAITELLI, matrícula SIAPE n° 2030536, lotado no Departamento de
Geodésia do Instituto de Geociências, como Diretor Substituto do Centro de Estudos de Geologia Costeira e
Oceânica, para substituir automaticamente o titular desta função em seus afastamentos ou impedimentos
regulamentares  no  período  de  24/06/2017  e  até  23/06/2019,  por  ter  sido  reeleito.  Processo  nº
23078.201268/2017-15.
JANE FRAGA TUTIKIAN
Vice-Reitora.
