Documento gerado sob autenticação Nº RFD.579.157.MGQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6715                  de  28/08/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  27/08/2018,   referente  ao  interstício  de
20/01/2016 a 26/08/2018, para o servidor DIEGO FERNANDES SILVA, ocupante do cargo de Assistente em
Administração -  701200,  matrícula  SIAPE 2128885,   lotado  no  Seção de Apoio Acadêmico do IGEO,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.522588/2018-97:
Formação Integral de Servidores da UFRGS I CH: 33 (08/06/2015 a 23/05/2018)
ENAP - Acesso à Informação CH: 20 (05/07/2018 a 26/07/2018)
ENAP - Gestão de riscos no setor público CH: 20 (04/08/2018 a 25/08/2018)
ENAP  -  Defesa  do  Usuário  e  Simplificação  CH:  20  Carga  horária  utilizada:  17  hora(s)  /  Carga  horária
excedente: 3 hora(s) (04/08/2018 a 25/08/2018)
ENAP - Educação em Direitos Humanos CH: 30 (05/07/2018 a 02/08/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
