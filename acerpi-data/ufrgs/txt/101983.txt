Documento gerado sob autenticação Nº IPT.445.431.LBG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10956                  de  09/12/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar na Pró-Reitoria de Gestão de Pessoas, com exercício na Divisão de Segurança e Medicina do
Trabalho, Ambiente Organizacional Administrativo, DAVI VOGEL ZIMMER, nomeado conforme Portaria Nº
9538/2019 de 21 de outubro de 2019, publicada no Diário Oficial da União no dia 22 de outubro de 2019, em
efetivo exercício desde 03 de dezembro de 2019,  ocupante do cargo de TÉCNICO EM SEGURANÇA DO
TRABALHO,  classe  D,  nível  I,  padrão  101,  no  Quadro  de  Pessoal  desta  Universidade.  Processo  n°
23078.533465/2019-62.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
