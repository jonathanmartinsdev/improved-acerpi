Documento gerado sob autenticação Nº GYR.519.213.9TA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5297                  de  19/06/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANGELO LUIZ FREDDO,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Cirurgia e  Ortopedia da Faculdade de Odontologia,  com a
finalidade  de  participar  do  "XX  Congreso  Internacional  de  la  Asociación  Latinomaricana  de  Cirugía  y
Traumatología Bucomaxilofacial", em Buenos Aires, Argentina, no período compreendido entre 05/08/2017 e
10/08/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 27869.
RUI VICENTE OPPERMANN
Reitor
