Documento gerado sob autenticação Nº YMR.403.421.I7H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             906                  de  28/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, à
Professora  CLAUDIA  CALEGARO  MARQUES,  matrícula  SIAPE  3322496,  lotada  e  em  exercício  no
Departamento de Zoologia do Instituto de Biociências, da classe A  de Professor Adjunto A, nível 01, para a
classe C  de Professor Adjunto, nível 01, com vigência financeira a partir de 23/01/2020, de acordo com o que
dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554, de 20 de junho de
2013 do Ministério da Educação e a Decisão nº 331/2017. Processo nº 23078.501614/2020-68.
RUI VICENTE OPPERMANN
Reitor.
