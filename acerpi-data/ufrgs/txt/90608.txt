Documento gerado sob autenticação Nº IBZ.337.453.44V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3770                  de  03/05/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Homologar o parecer que aprova o servidor técnico-administrativo WILLIAM DAS CHAGAS BRITTO,
ocupante do cargo de Operador de Caldeira, no estágio probatório cumprido no período de 28/04/2016 até
28/04/2019, fazendo jus, a partir desta data, à estabilidade no serviço público federal.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
