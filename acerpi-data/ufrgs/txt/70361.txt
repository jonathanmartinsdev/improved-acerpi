Documento gerado sob autenticação Nº PYQ.318.290.F13, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7876                  de  03/10/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de EDUARDO ERNESTO FILIPPI, Professor do Magistério Superior,
lotado e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de Ciências
Econômicas, com a finalidade de realizar visita ao Instituto Superior de Relações Internacionais Venâncio de
Moura, em Luanda, Angola, no período compreendido entre 06/10/2018 e 14/10/2018, incluído trânsito, com
ônus limitado. Solicitação nº 58950.
RUI VICENTE OPPERMANN
Reitor
