Documento gerado sob autenticação Nº EHD.070.013.QC5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9982                  de  10/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor PEDRO SCHESTATSKY, matrícula SIAPE n° 1310315, lotado e em exercício no Departamento de
Medicina Interna da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 02, para a classe C  de
Professor Adjunto, nível 03, referente ao interstício de 08/01/2015 a 07/01/2017, com vigência financeira a
partir de 09/08/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.520440/2018-18.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
