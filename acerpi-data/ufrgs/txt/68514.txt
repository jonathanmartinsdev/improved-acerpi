Documento gerado sob autenticação Nº JJN.093.339.Q4D, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6466                  de  21/08/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora LIDIANE ALVES MENDES, ocupante do cargo de  Assistente em Administração
- 701200,  lotada no Instituto de Biociências,  SIAPE 3063104,  o percentual  de 30% (trinta por cento) de
Incentivo à Qualificação, a contar de 06/08/2018, tendo em vista a conclusão do curso de MBA em Gestão
Empresarial, conforme o Processo nº 23078.519984/2018-37.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
