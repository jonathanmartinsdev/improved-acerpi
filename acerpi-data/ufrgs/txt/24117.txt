Documento gerado sob autenticação Nº ZXW.855.887.MAD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4915                  de  06/07/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ALAN ALVES BRITO,  matrícula SIAPE n° 2082163, lotado e em exercício no Departamento de
Astronomia do Instituto de Física, da classe A  de Professor Adjunto A, nível 01, para a classe A  de Professor
Adjunto A, nível 02, referente ao interstício de 14/01/2014 a 13/01/2016, com vigência financeira a partir de
04/07/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-
CONSUN. Processo nº 23078.505576/2016-36.
RUI VICENTE OPPERMANN
Vice-Reitor
