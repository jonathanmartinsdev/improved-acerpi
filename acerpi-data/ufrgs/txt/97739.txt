Documento gerado sob autenticação Nº QKQ.612.870.5T7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8124                  de  06/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar CLAUDIA HOFHEINZ GIACOMONI,  matrícula SIAPE n° 3319322, ocupante do cargo de
Professor  do  Magistério  Superior,  classe  Associado,  lotada  no  Departamento  de  Psicologia  do
Desenvolvimento e da Personalidade do Instituto de Psicologia, do Quadro de Pessoal da Universidade, para
exercer  a  função de  Coordenadora  Substituta  do PPG em Psicologia,  com vigência  de  30/09/2019 até
06/10/2020, a fim de completar o mandato  da Professora ADRIANA WAGNER, conforme artigo 92 do Estatuto
da mesma Universidade, sem prejuízo e cumulativamente com a função de Chefe do Departamento de
Psicologia do Desenvolvimento e da Personalidade do Instituto de Psicologia,  Código FG-1.  Processo nº
23078.523740/2019-30.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
