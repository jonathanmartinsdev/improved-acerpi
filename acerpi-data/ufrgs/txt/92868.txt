Documento gerado sob autenticação Nº NRM.601.800.S2C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5254                  de  21/06/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/02/2019,   referente  ao  interstício  de
23/05/2017 a 10/02/2019, para o servidor PEDRO DE SOUZA PALAORO, ocupante do cargo de Jornalista -
701045,  matrícula SIAPE 2397683,  lotado  na  Secretaria de Comunicação Social,  passando do Nível de
Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.503278/2019-54:
Formação Integral de Servidores da UFRGS I CH: 36 (07/07/2017 a 23/05/2018)
NELE - Espanhol 3 CH: 43 (29/04/2017 a 29/07/2017)
NELE - Espanhol 4 CH: 48 Carga horária utilizada: 41 hora(s) / Carga horária excedente: 7 hora(s) (15/09/2017
a 08/12/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
