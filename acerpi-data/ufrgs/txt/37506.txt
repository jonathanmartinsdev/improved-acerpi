Documento gerado sob autenticação Nº RTX.687.844.VE7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3710                  de  02/05/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor SERGIO SCHNEIDER,  matrícula SIAPE n° 1150009, lotado e em exercício no Departamento de
Sociologia do Instituto de Filosofia e Ciências Humanas, da classe D  de Professor Associado, nível 04, para a
classe E  de Professor Titular,  referente ao interstício de 04/11/2013 a 01/02/2017, com vigência financeira a
partir de 02/02/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 232/2014 - CONSUN. Processo nº 23078.504435/2017-87.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
