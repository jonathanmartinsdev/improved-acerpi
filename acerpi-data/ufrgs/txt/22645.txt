Documento gerado sob autenticação Nº FCZ.858.841.84H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3890                  de  27/05/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  ALEJANDRO  GERMAN  FRANK,  Professor  do  Magistério
Superior, lotado no Departamento de Engenharia de Produção e Transportes da Escola de Engenharia e com
exercício na Comissão de Graduação de Engenharia da Produção, com a finalidade de realizar visita ao
Instituto Politécnico di Milano,  em Milão, Itália; ; ao Institut National Polytechnique de Grenoble,  França e
participar da "*8th CIRP IPSS Conference Produc-Service Systems  across Life Cycle", em Bergamo, Itália;   da
"23th Innovation and Product Development Management Conference", em Glasgow, Escócia, no período
compreendido entre 10/06/2016 e 25/06/2016, incluído trânsito, com ônus FAPERGS e CNPq (Edital Universal
- processo 473659/2013-1) . Solicitação nº 20271.
CARLOS ALEXANDRE NETTO
Reitor
