Documento gerado sob autenticação Nº BNX.284.553.EMV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 1524/2019-PROGESP Porto Alegre, 01 de outubro de 2019.
Senhor Pró-Reitor,
Encaminhamos o servidor LUIS FILIPE VIEGA DA COSTA,  ocupante do cargo de Assistente em
Administração, para exercício nessa Unidade no dia 02 de outubro de 2019.
Solicitamos que nos sejam encaminhadas, no prazo máximo de 03 dias, as seguintes informações:
- atividades a serem desenvolvidas pelo servidor;
- local de exercício (divisão, setor), observando a hierarquia dos órgãos registrada no SRH;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal.
Informamos que este encaminhamento se faz para reposição de vaga, em virtude de remoção da
servidora MARILISA GARCIA MACHADO.
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
 
Ilmo. Sr.
VLADIMIR PINHEIRO DO NASCIMENTO
Pró-Reitor de Graduação
N/Universidade.
 
