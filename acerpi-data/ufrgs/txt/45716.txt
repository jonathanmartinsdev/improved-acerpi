Documento gerado sob autenticação Nº VDP.233.716.S27, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9393                  de  09/10/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar na Pró-Reitoria de Gestão de Pessoas, com exercício na Divisão de Planejamento Institucional,
ANA LUCIA DE SOUZA MIGLIORINI, nomeada conforme Portaria Nº 7788/2017 de 18 de agosto de 2017,
publicada no Diário Oficial da União no dia 21 de agosto de 2017, em efetivo exercício desde 04 de outubro
de  2017,  ocupante  do  cargo  de  ASSISTENTE  EM  ADMINISTRAÇÃO,  Ambiente  Organizacional
Administrativo,  classe  D,  nível  I,  padrão  101,  no  Quadro  de  Pessoal  desta  Universidade.  
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
