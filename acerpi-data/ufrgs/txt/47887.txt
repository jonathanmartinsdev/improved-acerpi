Documento gerado sob autenticação Nº BOF.521.062.UUN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10970                  de  05/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder pensão, a partir de 3 de outubro de 2017, a MARIA GECY RODRIGUES LIMA nos termos
dos artigos 215 e 217, inciso I, da Lei n.º 8.112, de 11 de dezembro de 1990, combinado com o artigo 40 da
Constituição Federal de 1988, alterado pela Emenda Constitucional 41 de 2003, regulamentado pela Lei nº
10.887/2004, artigo 2º, I, em decorrência do falecimento de ANTONIO CARLOS DE SOUZA LIMA, matrícula
SIAPE n° 0351202, aposentado no cargo de Professor do Magistério Superior do quadro de pessoal desta
Universidade. Processo n.º 23078.019504/2017-51.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
