Documento gerado sob autenticação Nº FNI.477.252.Q1S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8842                  de  21/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°31440,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do Quadro de  Pessoal  desta  Universidade,  VANISE NASCIMENTO PEROBELLI  (Siape:
1836975 ),  para substituir   CLAUDIOMAR OVIEDO RIBEIRO (Siape: 0356155 ), Coordenador de Pagamento,
Cadastro e Processos Judiciais do DAP da PROGESP, Código CD-4, em seu afastamento no país, no período de
25/09/2017 a 29/09/2017, com o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
