Documento gerado sob autenticação Nº LYV.989.945.S51, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3252                  de  15/04/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 19/03/2019, a ocupante do cargo de Professor do Magistério Superior, classe
Associado, do Quadro de Pessoal desta Universidade, MARINA BENTO SOARES, matrícula SIAPE n° 2244773,
da função de Coordenadora Substituta do PPG em Geociências, para a qual foi designada pela Portaria nº
10419/2018, de 26/12/2018. Processo nº 23078.509164/2019-18.
JANE FRAGA TUTIKIAN
Vice-Reitora.
