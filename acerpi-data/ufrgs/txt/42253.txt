Documento gerado sob autenticação Nº JRE.318.711.GTC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7057                  de  03/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  CAROLINE  DE  OLIVEIRA  ORTH,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Ciências Contábeis e Atuariais da Faculdade de Ciências
Econômicas, com a finalidade de participar da "International Finance Conference 2017", em Santiago, Chile,
no período compreendido entre 29/08/2017 e 03/09/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria
de Pesquisa - diárias). Solicitação nº 29582.
RUI VICENTE OPPERMANN
Reitor
