Documento gerado sob autenticação Nº UFQ.948.838.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             765                  de  22/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MAGALE ELISA BRUCKMANN, matrícula SIAPE n° 1211086, lotada no Departamento de Física do
Instituto de Física e com exercício na Comissão de Graduação das Licenciaturas em Física, da classe   de
Professor Assistente, nível 01, para a classe   de Professor Assistente, nível 02, referente ao interstício de
13/03/1999  a  19/07/2002,  com  vigência  financeira  a  partir  de  20/07/2002,  conforme  decisão  judicial
proferida no processo nº 5054491-30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o
que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do
CONSUN. Processo nº 23078.529582/2019-21.
RUI VICENTE OPPERMANN
Reitor.
