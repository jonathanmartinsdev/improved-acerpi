Documento gerado sob autenticação Nº PAW.123.485.F83, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10059                  de  31/10/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  JOANA BOSAK DE FIGUEIREDO,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Artes Visuais do Instituto de Artes, com a finalidade de
participar  do "I  Congresso Ibérico de Semiótica",  em Lisboa,  Portugal,  no período compreendido entre
17/11/2017 e 28/11/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação
nº 32355.
RUI VICENTE OPPERMANN
Reitor
