Documento gerado sob autenticação Nº GJX.722.575.M0L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6911                  de  31/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Homologar o parecer que aprova o servidor técnico-administrativo ANIBAL RICARDO GONÇALVES
ALVAREZ,  ocupante do cargo de Técnico em Assuntos Educacionais, no estágio probatório cumprido no
período de 28/07/2014 até 28/07/2017, fazendo jus, a partir desta data, à estabilidade no serviço público
federal.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
