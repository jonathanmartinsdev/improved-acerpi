Documento gerado sob autenticação Nº GIR.413.192.2IL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9265                  de  05/10/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°31816,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ANDREAS SEBASTIAN LOUREIRO MENDEZ (Siape:
1673109 ),  para substituir   MIRIAM ANDERS APEL (Siape: 2195129 ), Coordenador da Comissão de Pesquisa
da Faculdade de Farmácia, em seu afastamento no país, no período de 03/10/2017 a 06/10/2017.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
