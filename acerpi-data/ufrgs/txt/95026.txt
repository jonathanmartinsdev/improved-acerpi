Documento gerado sob autenticação Nº INA.128.744.9BE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6770                  de  26/07/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.514867/2019-68,
23078.511933/2019-48  e  23078.506933/2018-45,  do  Pregão  Eletrônico  SRP  nº  121/2018,  da  Ata  nº
257/2018, da Lei nº 10.520/02 e ainda, da Lei nº 8.666/93,
             RESOLVE:
 
              Aplicar as sanções administrativas de IMPEDIMENTO DE LICITAR E CONTRATAR COM A UNIÃO PELO
PRAZO DE 02 (dois) ANOS e MULTA DE 5% (cinco por cento) sobre o valor das parcelas que lhe deram causa,
no  montante  de  R$  792,25  (setecentos  e  noventa  e  dois  reais  e  vinte  e  cinco  centavos),  conforme o
demonstrativo de cálculo (Doc. SEI nº 1696136), previstas nos itens 10.1.2 e inciso III do item 10.1.3 do Termo
de  Referência  do  referido  Edital  de  Licitação,  à  Empresa  BARU  COMERCIO  E  DISTRIBUIÇÃO  DE
EQUIPAMENTOS  EIRELI,  CNPJ  n.º  10.881.930/0001-55,  pela  não  entrega  do  itens  relativos  ao  Pregão
Eletrônico em epígrafe, conforme atestado pela DPCA (Doc. SEI nº 1631895), bem como pelo NUDECON (Doc.
SEI nº 1695454) do processo 23078.514867/2019-68.
 
             Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
