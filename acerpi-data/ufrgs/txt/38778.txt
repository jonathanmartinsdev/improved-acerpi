Documento gerado sob autenticação Nº IQP.915.527.ALF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4761                  de  29/05/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016 e, tendo
em vista  o  que consta  dos  Processos  Administrativos  n°  23078.034226/12-13,  23078.037601/2014-83  e
23078.003942/2015-36 do Contrato nº 213/PROPLAN/DELIT/2012 e da Lei 8.666/93,
 
 
            RESOLVE:
 
            Aplicar a sanção administrativa de MULTA no montante de R$ 106,64 (cento e seis reais e sessenta e
quatro centavos), conforme demonstrativo de cálculo à fl. 745, prevista na alínea "b" da cláusula décima
segunda c/c alíneas "e", "g" e "h" parágrafo terceiro da mesma cláusula do Contrato, à Empresa MULTIÁGIL
LIMPEZA,  PORTARIA  E  SERVIÇOS  ASSOCIADOS  LTDA,  CNPJ  n.º  03.149.832/0001-62 ,  pelos
descumprimentos: Atraso no pagamento de vale alimentação, atraso no pagamento de vale transporte e
faltas sem reposição, conforme atestado pela GERTE às fls. 01/02 e 25/26 do Proc. 037601/2014-83 (OF.
2747/2014), e fl. 743 do Proc. 23078.003942/2015-36, bem como pelo NUDECON à fl. 744 do processo nº
23078. 003942/2015-36.
 
            Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
