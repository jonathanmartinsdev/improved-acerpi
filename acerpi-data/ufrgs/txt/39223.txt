Documento gerado sob autenticação Nº PMF.618.823.C0M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4991                  de  06/06/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LUIGI CARRO, Professor do Magistério Superior, lotado e em
exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de  Informática,  com  a  finalidade  de
participar de banca de doutorado na  Technical University of Delft, Holanda, no período compreendido entre
07/06/2017 e 14/06/2017, incluído trânsito, com ônus limitado. Solicitação nº 28638.
RUI VICENTE OPPERMANN
Reitor
