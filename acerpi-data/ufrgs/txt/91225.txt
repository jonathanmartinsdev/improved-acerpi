Documento gerado sob autenticação Nº TLW.175.476.0PS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4246                  de  15/05/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5016824-73.2019.4.04.7100,  da  8ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, do servidor LUIS ADOLFO FAGUNDES NASCENTE, matrícula SIAPE n° 0353639, aposentado no
cargo de Técnico de Tecnologia da Informação - 701226, do nível I para o nível II, a contar de 01/01/2006,
conforme o Processo nº 23078.511990/2019-27.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
Conceder  progressão  por  capacitação,  a  contar  de  13/11/2013,  passando  do  nível  de
Classificação/nível  de  Capacitação  D  II  para  o  nível  de  Classificação/nível  de  Capacitação  D  III.
 
 
RUI VICENTE OPPERMANN
Reitor
