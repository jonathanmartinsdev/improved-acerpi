Documento gerado sob autenticação Nº ZGY.960.353.61T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             473                  de  16/01/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°45010,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, RENE FAUSTINO GABRIEL JUNIOR (Siape: 2322787 ),
 para substituir   RITA DO CARMO FERREIRA LAIPELT (Siape: 2714174 ),  Coordenador da COMGRAD de
Biblioteconomia,  Código  FUC,  em seu  afastamento  por  motivo  de  férias,  no  período de  16/01/2019  a
25/01/2019, com o decorrente pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
