Documento gerado sob autenticação Nº TMQ.892.870.Q5E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1224                  de  08/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ALVARO VIGO, matrícula SIAPE n° 0356920, lotado e em exercício no Departamento de Estatística
do Instituto de Matemática e Estatística, da classe D  de Professor Associado, nível 02, para a classe D  de
Professor Associado, nível 03, referente ao interstício de 31/12/2014 a 30/12/2016, com vigência financeira a
partir de 31/12/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações e  a  Decisão nº  197/2006-CONSUN,  alterada pela  Decisão nº  401/2013-CONSUN. Processo nº
23078.514403/2016-17.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
