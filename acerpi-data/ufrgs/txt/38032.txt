Documento gerado sob autenticação Nº ZSI.835.729.GR4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4061                  de  09/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora NEUSA SALTIEL STOBBE, matrícula SIAPE n° 1423921, lotada e em exercício no Departamento de
Microbiologia, Imunologia e Parasitologia do Instituto de Ciências Básicas da Saúde, da classe   de Professor
Adjunto, nível 01, para a classe   de Professor Adjunto, nível 02, referente ao interstício de 21/08/2003 a
28/02/2006, com vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de
28 de dezembro de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº
401/2013-CONSUN. Processo nº 23078.503592/2017-75.
JANE FRAGA TUTIKIAN
Vice-Reitora.
