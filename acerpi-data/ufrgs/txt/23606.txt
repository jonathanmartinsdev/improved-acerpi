Documento gerado sob autenticação Nº KCB.711.329.30G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4521                  de  21/06/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, VIRGÍLIO JOSÉ STRASBURG, CPF n° 54350042091, matrícula SIAPE n° 2769952,
lotado no Departamento de Nutrição da Faculdade de Medicina, como Coordenador Substituto da COMGRAD
de Nutrição, para substituir automaticamente o titular desta função em seus afastamentos ou impedimentos
regulamentares no período de 26/06/2016 e até 25/06/2018. Processo nº 23078.013164/2016-74.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
