Documento gerado sob autenticação Nº PRQ.165.266.MHM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1854                  de  24/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/02/2017,   referente  ao  interstício  de
24/06/2014 a 15/02/2017, para a servidora LUISA WEBER MERCADO,  ocupante do cargo de Técnico de
Laboratório Área - 701244, matrícula SIAPE 1860056,  lotada  na  Faculdade de Odontologia, passando do
Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.002293/2017-18:
Formação Integral de Servidores da UFRGS I CH: 56 (30/03/2012 a 28/09/2016)
UFRGS/CNPQ/FAPERGS - I Encontro Brasileiro de Pesquisa em Cardiologia CH: 21 (06/05/2013 a 08/05/2013)
FAMES/UFRGS Imunologia Básica aplicada a Clínica 2014/2015 - A CH: 252 Carga horária utilizada: 43 hora(s) /
Carga horária excedente: 209 hora(s) (30/03/2015 a 07/06/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
