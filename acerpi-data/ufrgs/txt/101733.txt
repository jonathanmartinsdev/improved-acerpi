Documento gerado sob autenticação Nº BFQ.410.353.7JI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10833                  de  04/12/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ANA MARIA ALBANI DE CARVALHO,  matrícula SIAPE n° 1202135, lotada e em exercício no
Departamento de Artes Visuais do Instituto de Artes, da classe C  de Professor Adjunto, nível 04, para a classe
D  de Professor Associado, nível  01,  referente ao interstício de 05/11/2011 a 04/11/2013, com vigência
financeira  a  partir  de  05/11/2013,  conforme  decisão  judicial  proferida  no  processo  nº  5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.526882/2019-59.
RUI VICENTE OPPERMANN
Reitor.
