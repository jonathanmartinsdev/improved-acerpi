Documento gerado sob autenticação Nº XUT.796.726.O6K, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             285                  de  08/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de EDSON BERTOLINI, Professor do Magistério Superior, lotado e
em exercício no Departamento de Fitossanidade da Faculdade de Agronomia, com a finalidade de realizar
visita  à  Universitat  de  València,  em  Valência,  Espanha,  no  período  compreendido  entre  01/02/2020  e
28/02/2020,  incluído  trânsito,  sendo  de  01  a  20/02/2020  com  ônus  CAPES/PRINT/UFRGS  e  de  21  a
28/02/2020 com ônus limitado. Solicitação nº 89496.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
