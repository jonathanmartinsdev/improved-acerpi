Documento gerado sob autenticação Nº HMK.293.248.4LB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1455                  de  14/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Exonerar, a partir da data de publicação no Diário Oficial da União, ELGA VALERIA RODRIGUES DE
LIMA,  do cargo de Assessora do Reitor, Código SRH 1007 Código CD-4, da Universidade Federal do Rio
Grande do Sul, para o qual foi nomeado pela Portaria nº 0782/2010 de 25/02/2010 publicada no Diário Oficial
da União de 03/03/2010. Processo nº 23078.002078/2017-17.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
