Documento gerado sob autenticação Nº SDJ.027.178.8HE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4860                  de  04/07/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora DANIELA BORGES PAVANI, matrícula SIAPE n° 1574520, lotada e em exercício no Departamento
de Astronomia do Instituto de Física, da classe C  de Professor Adjunto, nível 02, para a classe C  de Professor
Adjunto, nível 03, referente ao interstício de 23/05/2013 a 22/05/2015, com vigência financeira a partir de
01/07/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-
CONSUN. Processo nº 23078.505554/2016-76.
RUI VICENTE OPPERMANN
Vice-Reitor
