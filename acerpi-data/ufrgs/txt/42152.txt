Documento gerado sob autenticação Nº QAM.296.792.GTC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7040                  de  03/08/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir da data deste ato, como segue:
Extinguir: Secretário do Depto de Patrimônio da PROPLAN, Código FG6 .
Processo nº 23078.509807/2017-61.
RUI VICENTE OPPERMANN
Reitor.
