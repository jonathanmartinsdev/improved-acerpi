Documento gerado sob autenticação Nº UZS.346.369.T4H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5563                  de  27/06/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar na Faculdade de Educação, com exercício na Biblioteca da Faculdade de Educação, MARCELO
GAMA GOULART, nomeado conforme Portaria Nº 4542/2017 de 22 de maio de 2017, publicada no Diário
Oficial da União no dia 23 de maio de 2017, em efetivo exercício desde 19 de junho de 2017, ocupante do
cargo de TÉCNICO EM ASSUNTOS EDUCACIONAIS, Ambiente Organizacional Administrativo, classe E, nível I,
padrão 101, no Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
