Documento gerado sob autenticação Nº VOH.453.209.2JQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5520                  de  22/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Declarar  vago,  a  partir  de  4  de  junho de  2016,  o  cargo  de  Técnico  em Eletrônica,  Nível  de
Classificação D, do Quadro de Pessoal, em decorrência do falecimento do servidor PAULO ROBERTO BORBA,
com lotação no Instituto de Física, conforme item IX, do Art. 33, da Lei n° 8.112, de 11 dezembro de 1990.
Processo nº 23078015505/2016-46
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
