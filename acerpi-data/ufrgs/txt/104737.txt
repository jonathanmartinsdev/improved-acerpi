Documento gerado sob autenticação Nº VZN.043.982.NA2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1536                  de  13/02/2020
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.533528/2019-81  e
23078.512862/2019-09,  da Lei  nº  10.520/02,  do Pregão Eletrônico SRP nº:  086/2019 e  ainda,  da Lei  nº
8.666/93,
            RESOLVE:
 
            Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 18.2.1 do Termo de Referência, à
Empresa POLO AR CONDICIONADO SERVIÇOS DE REFRIGERAÇÃO EIRELI EPP, CNPJ nº 06.021.988/0001-51,
pela  não  execução  do  serviço  no  prazo  estipulado  no  Termo  de  Referência,  conforme  atestado  pela
fiscalização (Doc. SEI nº 1920901 e 1991942), bem como pelo DICON/NUDECON (Doc. SEI nº 1996262) do
processo administrativo 23078.533528/2019-81.
 
             Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
