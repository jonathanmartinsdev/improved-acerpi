Documento gerado sob autenticação Nº SLS.601.287.5C8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             696                  de  24/01/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7633, de 29 de
setembro de 2016
I - Designar Comissão Permanente de Licitações, nos termos do artigo 51, caput, da Lei nº 8.666, de 21 de
junho de 1993, com a função de receber, examinar e julgar todos os documentos e procedimentos relativos
às licitações no âmbito da PROPLAN, composta dos seguintes servidores:
 
- José João Maria de Azevedo, Presidente
- Marcelo Utz Asconavieta, Membro
- Fernanda De Latorre Fortunato, Membro
- Marcos José da Silva, Membro
- Vinicíus de Oliveira Vieira, Membro
- Danielle Bernardes Farias, Suplente
- Turene de Andrade e Silva Neto, Suplente
- José Roberto Duarte Colchete, Suplente
- Mariléa Trevisan Bastos de Quadros, Suplente
 
II  -  O Presidente da Comissão, em eventuais impedimentos,  será substituído  pelos membros titulares,
observada a ordem estabelecida no item I, bem como estes, pelos Suplentes.
III - O Presidente da Comissão poderá, em casos específicos, observada a legislação pertinente, requerer
assessoramento técnico de servidores da PROPLAN, bem como de outros servidores da Universidade.
IV - Revogar a Portaria nº 608  de  20 de janeiro de 2017.
 
LUIS ROBERTO DA SILVA MACEDO
Pró-Reitor de Planejamento e Administração em exercício
