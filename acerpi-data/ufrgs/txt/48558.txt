Documento gerado sob autenticação Nº ERU.198.750.KEU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11437                  de  26/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar insubsistente a Portaria  nº  11350/2017,  de 20/12/2017,  que designou temporariamente
MONICA  PIENIZ  (Siape:  2771237),  para  substituir  MARIA  BERENICE  LOPES  (Siape:  0353931  ),  Gerente
Administrativo da FABICO, Código SRH 1076, código FG-1, no período de 26/12/2017 à 04/01/2018, gerada
conforme Solicitacao de Férias nº 37376. Processo SEI nº 23078.524990/2017-93
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
