Documento gerado sob autenticação Nº KHQ.929.138.EOC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4427                  de  16/06/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora MARISA HELENA ERIG,
ocupante do cargo de Técnico em Assuntos Educacionais-701079, lotada na Faculdade de Farmácia, SIAPE
1028737, para 52% (cinquenta e dois por cento), a contar de 13/06/2016, tendo em vista a conclusão do curso
de Mestrado em Educação, conforme o Processo nº 23078.013177/2016-43.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
