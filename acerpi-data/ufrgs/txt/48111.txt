Documento gerado sob autenticação Nº BOI.840.341.OBH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11135                  de  12/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/12/2017,   referente  ao  interstício  de
27/11/2014 a 11/12/2017, para o servidor ERNESTO SIDNEI NOGUEIRA MARTINS, ocupante do cargo de
Pintor-área - 701650, matrícula SIAPE 0355783,  lotado  na  Superintendência de Infraestrutura, passando do
Nível de Classificação/Nível de Capacitação B II, para o Nível de Classificação/Nível de Capacitação B III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.523539/2017-91:
Formação Integral de Servidores da UFRGS III CH: 60 (09/09/2016 a 29/09/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
