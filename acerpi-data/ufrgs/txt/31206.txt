Documento gerado sob autenticação Nº ZXW.666.193.T9C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9732                  de  08/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°30132,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO EM SEGURANÇA DO
TRABALHO, do Quadro de Pessoal desta Universidade, GLAITO BRUM DA CRUZ (Siape: 1093205 ),  para
substituir    VINICIUS CIULLA JUNIOR (Siape:  1185059 ),  Diretor da Divisão de Segurança e Medicina do
Trabalho do DAS da PROGESP,  Código FG-1,  em seu afastamento por motivo de férias,  no período de
15/12/2016 a 27/12/2016, com o decorrente pagamento das vantagens por 13 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
