Documento gerado sob autenticação Nº TIL.359.410.Q9P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7696                  de  26/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  17/08/2018,
correspondente  ao  grau  Insalubridade  Média,  à  servidora  LUCIANE  LACERDA  GOMES  GONÇALVES,
Identificação Única 14407728, Médico-área, com exercício na Divisão de Saúde e Junta Médica da Pró-Reitoria
de Gestão de Pessoas, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado
com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Insalubres
conforme Laudo Pericial constante no Processo nº 23078.518815/2017-07, Código SRH n° 23635.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
