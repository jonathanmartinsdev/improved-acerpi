Documento gerado sob autenticação Nº WKP.912.976.R3E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3236                  de  12/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  ANDREA  MOURA  BERNARDES,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Engenharia dos Materiais da Escola de Engenharia, com
a finalidade de participar de encontro junto à Universidad Nacional de Trujillo, em Trujillo, Peru, realizar visita
e  participar  de  reunião  junto  à  Transducto  S.A.,  em  Calama,  Chile,  no  período  compreendido  entre
10/05/2019 e 20/05/2019, incluído trânsito, com ônus limitado. Solicitação nº 83091.
RUI VICENTE OPPERMANN
Reitor
