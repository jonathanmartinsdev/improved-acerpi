Documento gerado sob autenticação Nº NBB.638.895.Q8S, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4309                  de  13/06/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Alterar o exercício, a partir de 4 de junho de 2018, de PAULO ANTONIOLLI, matrícula SIAPE nº
1088078, ocupante do cargo de Administrador, da Gerência Administrativa da Faculdade de Educação para
a Gerência Administrativa da Escola de Engenharia. Processo n° 23078.500977/2018-61.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
