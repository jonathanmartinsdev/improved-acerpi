Documento gerado sob autenticação Nº RDA.758.243.0R6, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2177                  de  22/03/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE:
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora LILIAN EGGERS, matrícula SIAPE n° 2308147, com exercício no Departamento de Botânica do
Instituto de Biociências,  da classe D  de Professor  Associado,  nível  01,  para a  classe D  de Professor
Associado, nível 02, referente ao interstício de 31/12/2013 a 30/12/2015, com vigência financeira a partir de
14/03/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-
CONSUN. Processo nº 23078.005301/2016-05.
RUI VICENTE OPPERMANN
Vice-Reitor
