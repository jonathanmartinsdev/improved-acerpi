Documento gerado sob autenticação Nº ZXL.936.584.9JM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5401                  de  21/06/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  LUCIANA  GRUPPELLI  LOPONTE,  matrícula  SIAPE  n°  1228692,  lotada  e  em  exercício  no
Departamento de Ensino e Currículo da Faculdade de Educação, da classe D  de Professor Associado, nível
01, para a classe D  de Professor Associado, nível 02, referente ao interstício de 12/03/2015 a 11/03/2017,
com vigência financeira a partir de 12/03/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  197/2006-CONSUN,  alterada  pela  Decisão  nº
401/2013-CONSUN. Processo nº 23078.502679/2017-25.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
