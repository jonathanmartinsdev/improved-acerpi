Documento gerado sob autenticação Nº DLD.062.903.PKV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6317                  de  15/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ROGERIO LUIS MALTEZ, matrícula SIAPE n° 2299297, lotado e em exercício no Departamento de
Física do Instituto de Física, da classe D  de Professor Associado, nível 02, para a classe D  de Professor
Associado, nível 03, referente ao interstício de 15/09/2014 a 11/09/2017, com vigência financeira a partir de
12/06/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a
Decisão nº 331/2017 do CONSUN. Processo nº 23078.512876/2018-33.
JANE FRAGA TUTIKIAN
Vice-Reitora.
