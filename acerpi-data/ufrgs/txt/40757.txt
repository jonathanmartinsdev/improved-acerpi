Documento gerado sob autenticação Nº QDP.797.503.H5O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6775                  de  26/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.  7684, de 03 de outubro de 2016, e
conforme a Solicitação de Férias n°31623,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, GABRIELA PEREIRA LOPES (Siape: 1069605 ),
 para substituir   DEBORA TUSI GONÇALVES (Siape: 2146194 ), Assessora do Vice-Reitor, Código CD-4, em seu
afastamento por motivo de férias, no período de 03/07/2017 a 18/07/2017, com o decorrente pagamento das
vantagens por 16 dias.
MAURÍCIO VIEGAS DA SILVA
Pró-Reitor
