Documento gerado sob autenticação Nº UXW.284.238.6I4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3408                  de  25/04/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar  no  Instituto  de  Biociências,  com  exercício  na  Gerência  Administrativa  do  Instituto  de
Biociências,  FERNANDA STENERT,  nomeada conforme Portaria Nº 2441/2017 de 20 de março de 2017,
publicada no Diário Oficial da União no dia 21 de março de 2017, em efetivo exercício desde 18 de abril de
2017,  ocupante do cargo de ASSISTENTE EM ADMINISTRAÇÃO,  Ambiente Organizacional  Administrativo,
classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
