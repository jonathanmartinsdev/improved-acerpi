Documento gerado sob autenticação Nº PHL.446.621.TTM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             567                  de  15/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear o ocupante do cargo de Professor do Magistério Superior do Quadro de Pessoal desta
Universidade,  JOSE  ANGELO  SILVEIRA  ZUANAZZI,  matrícula  SIAPE  1225771,  para  exercer  o  cargo
de Superintendente de Infraestrutura, código 835, código CD-3, com vigência a partir da data de publicação
no Diário Oficial da União. Processo nº 23078.500881/2020-18.
RUI VICENTE OPPERMANN
Reitor.
