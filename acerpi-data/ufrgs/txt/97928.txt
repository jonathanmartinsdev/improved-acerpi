Documento gerado sob autenticação Nº RPF.048.667.JGN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8265                  de  10/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5043070-09.2019.4.04.7100,  da  8ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo II da Portaria nº 1874, de
13/07/2006, do servidor DARCI DA SILVEIRA, matrícula SIAPE n° 0357257, aposentado no cargo de Auxiliar
de  Cozinha  -  701614,  do  nível  II  para  o  nível  III,  a  contar  de  01/01/2006,  conforme  o  Processo  nº
23078.523764/2019-99.
 
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria, no exercício da Reitoria.
