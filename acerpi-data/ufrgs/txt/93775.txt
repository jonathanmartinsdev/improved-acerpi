Documento gerado sob autenticação Nº ECV.840.410.225, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5948                  de  15/07/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, à
Professora ROZANE RODRIGUES REBECHI, matrícula SIAPE 2322060, lotada e em exercício no Departamento
de Línguas Modernas do Instituto de Letras, da classe A  de Professor Adjunto A, nível 02, para a classe C  de
Professor Adjunto, nível 01, com vigência financeira a partir de 04/07/2019, de acordo com o que dispõe a Lei
nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554, de 20 de junho de 2013 do
Ministério da Educação e a Decisão nº 331/2017. Processo nº 23078.517129/2019-72.
JANE FRAGA TUTIKIAN
Vice-Reitora.
