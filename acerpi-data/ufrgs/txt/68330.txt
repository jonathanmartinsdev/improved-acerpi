Documento gerado sob autenticação Nº ZGY.156.954.86P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6349                  de  16/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor HELIO CUSTODIO FERVENZA, matrícula SIAPE n° 1079013, lotado e em exercício no Departamento
de Artes Visuais do Instituto de Artes, da classe   de Professor Associado, nível 01, para a classe   de Professor
Associado, nível 02, referente ao interstício de 16/01/2003 a 15/01/2005, com vigência financeira a partir de
31/07/2018, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e
a  Decisão  nº  197/2006-CONSUN,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.524714/2017-67.
JANE FRAGA TUTIKIAN
Vice-Reitora.
