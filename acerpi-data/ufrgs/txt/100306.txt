Documento gerado sob autenticação Nº EBH.870.159.C19, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9880                  de  01/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de VILMAR TREVISAN, Professor do Magistério Superior, lotado e
em exercício no Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística, com a
finalidade  de  realizar  visita  à  Universidad  Nacional  de  San  Luis,  em  San  Luis,  Argentina,  no  período
compreendido entre 10/11/2019 e 18/11/2019, incluído trânsito, com ônus CAPES/MATH-AmSud. Solicitação
nº 87620.
RUI VICENTE OPPERMANN
Reitor
