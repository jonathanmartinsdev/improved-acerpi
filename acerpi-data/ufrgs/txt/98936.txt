Documento gerado sob autenticação Nº HPU.839.729.I26, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8913                  de  02/10/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  01/10/2019,   referente  ao  interstício  de
31/03/2018 a 30/09/2019, para o servidor LUCIO XAVIER RIBEIRO, ocupante do cargo de Pedreiro - 701649,
matrícula  SIAPE  0356511,   lotado   na   Superintendência  de  Infraestrutura,  passando  do  Nível  de
Classificação/Nível de Capacitação B II, para o Nível de Classificação/Nível de Capacitação B III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.526677/2019-93:
ENAP - Introdução à Libras CH: 60 (17/04/2018 a 12/08/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
