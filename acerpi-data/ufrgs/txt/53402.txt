Documento gerado sob autenticação Nº JBW.662.624.9RJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2881                  de  19/04/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  19/04/2018,   referente  ao  interstício  de
19/10/2016  a  18/04/2018,  para  o  servidor  VINICIUS  FERRO,  ocupante  do  cargo  de  Assistente  em
Administração - 701200, matrícula SIAPE 1157295,  lotado  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.507726/2018-16:
Formação Integral de Servidores da UFRGS III CH: 70 (25/11/2016 a 31/10/2017)
ENAP - Gestão Estratégica de Pessoas e Planos de Carreira CH: 20 (05/09/2017 a 25/09/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
