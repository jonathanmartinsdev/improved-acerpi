Documento gerado sob autenticação Nº HCI.546.981.Q93, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6220                  de  13/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  PATRICIA  MIRANDA  DO  LAGO,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Pediatria da Faculdade de Medicina, com a finalidade de
participar  do  "22nd  International  Congress  on  Palliative  Care",  em  Montreal,  Canadá,  no  período
compreendido entre 01/10/2018 e 06/10/2018, incluído trânsito, com ônus UFRGS (Pró-reitoria de Pesquisa -
diárias). Solicitação nº 58106.
RUI VICENTE OPPERMANN
Reitor
