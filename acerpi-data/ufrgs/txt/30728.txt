Documento gerado sob autenticação Nº IPD.936.002.FP4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9383                  de  23/11/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de NELSON JURANDI ROSA FAGUNDES, Professor do Magistério
Superior, lotado e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de
participar do "IV Congreso Uruguayo de Zoología", em Maldonado, Uruguai, no período compreendido entre
03/12/2016 e 10/12/2016, incluído trânsito, com ônus limitado. Solicitação nº 25091.
RUI VICENTE OPPERMANN
Reitor
