Documento gerado sob autenticação Nº PTO.523.349.742, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8819                  de  31/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor PAULO HENRIQUE DOS SANTOS PACHECO, ocupante do cargo de  Técnico
em Edificações - 701228, lotado na Superintendência de Infraestrutura, SIAPE 1872628, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 08/08/2016, tendo em vista a conclusão do
Curso de Graduação em Engenharia Civil, conforme o Processo nº 23078.202854/16-34.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
