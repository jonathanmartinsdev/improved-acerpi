Documento gerado sob autenticação Nº OKN.554.525.Q7S, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2072                  de  08/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar SHIRLEI GALARCA SALORT TEIXEIRA, matrícula SIAPE n° 1678230, ocupante do cargo de
Bibliotecário-documentalista,  classe  E,  lotada  na  Faculdade  de  Medicina,  do  Quadro  de  Pessoal  da
Universidade, para exercer a função de Chefe Substituta da Biblioteca de Medicina, com vigência a partir da
data deste ato e até 11/12/2017, a fim de completar o mandato de RAQUEL DA ROCHA SCHIMITT DOMINGOS,
conforme artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.001873/2017-98.
JANE FRAGA TUTIKIAN
Vice-Reitora.
