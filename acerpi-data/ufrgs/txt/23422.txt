Documento gerado sob autenticação Nº KSW.243.058.FUR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4406                  de  16/06/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a MARA RUBIA DOS SANTOS
BIZARRO, matrícula SIAPE nº 0356216, no cargo de Porteiro, nível de classificação C, nível de capacitação I,
padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de trabalho, com exercício
no Setor de Portaria da Faculdade de Direito, com proventos integrais. Processo 23078.012431/2016-96.
CARLOS ALEXANDRE NETTO
Reitor
