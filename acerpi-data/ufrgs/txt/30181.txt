Documento gerado sob autenticação Nº XCP.602.205.QU7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9038                  de  09/11/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  RAFAEL  MANICA,  matrícula  SIAPE  n°  2611170,  lotado  e  em  exercício  no  Departamento  de
Hidromecânica e Hidrologia do Instituto de Pesquisas Hidráulicas, da classe C  de Professor Adjunto, nível 03,
para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 04/08/2014 a 03/08/2016, com
vigência financeira a partir de 04/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro
de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.508552/2016-39.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
