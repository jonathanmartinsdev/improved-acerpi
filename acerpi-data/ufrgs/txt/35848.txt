Documento gerado sob autenticação Nº KYU.530.682.MP0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2503                  de  22/03/2017
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, ao
Professor  GUILHERME  BALDO,  matrícula  SIAPE  2965450,  lotado  e  em  exercício  no  Departamento  de
Fisiologia do Instituto de Ciências Básicas da Saúde, da classe A  de Professor Adjunto A, nível 02, para a
classe C  de Professor Adjunto, nível 01, com vigência financeira a partir da data de publicação da portaria, de
acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554,
de 20 de junho de 2013 do Ministério  da Educação e a  Decisão nº  401/2013 -  CONSUN. Processo nº
23078.515463/2016-49.
PHILIPPE OLIVIER ALEXANDRE NAVAUX
Decano do Conselho Universitário, no exercício da Reitoria.
