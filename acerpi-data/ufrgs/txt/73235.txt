Documento gerado sob autenticação Nº BCP.102.865.851, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9926                  de  10/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°52845,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO EM MANUTENCAO DE
ÁUDIO/VÍDEO, do Quadro de Pessoal desta Universidade, EDGAR WOLFRAM HELDWEIN (Siape: 1736774 ),
 para substituir   CARLA BERENICE ESCOVAR BELLO (Siape: 0355984 ), Diretor da Divisão Administrativa do
Departamento de Difusão Cultural da PROREXT, Código FG-4, em seu afastamento por motivo de Laudo
Médico do titular da Função, no período de 11/12/2018 a 21/12/2018, com o decorrente pagamento das
vantagens por 11 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
