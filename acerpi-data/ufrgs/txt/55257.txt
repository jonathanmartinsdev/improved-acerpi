Documento gerado sob autenticação Nº YQY.500.875.J5N, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4301                  de  13/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias, 
RESOLVE
Declarar vaga, a partir de 30/05/2018, a função de Chefe da Seção de Bolsas de Extensão da Divisão
de Exec Financ do DARE da PROREXT, Código SRH 424, Código FG-5, desta Universidade, tendo em vista a
aposentadoria  de CARLOS AUGUSTO BENTO DA SILVA, matrícula SIAPE n° 0357955, conforme Portaria n°
3902/2018, de 28 de maio de 2018, publicada no Diário Oficial da União do dia 30 de maio de 2018. Processo
nº 23078.514139/2018-75.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
