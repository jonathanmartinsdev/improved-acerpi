Documento gerado sob autenticação Nº PEZ.030.572.PSJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5493                  de  21/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder à servidora DENISE SCHROEDER, ocupante do cargo de  Assistente em Administração -
701200, lotada na Faculdade de Agronomia, SIAPE 0358763, o percentual de 25% (vinte e cinco por cento) de
Incentivo à Qualificação, a contar de 23/03/2016, tendo em vista a conclusão do curso Superior de Tecnologia
em Recursos Humanos,conforme o Processo nº 23078.006469/2016-20.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
