Documento gerado sob autenticação Nº MAH.016.070.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7946                  de  02/09/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar a Portaria n° 7234/2019, de 09/08/2019, publicada no Diário Oficial da União - DOU em 12
de agosto de 2019, Seção 2, Página 39, que concedeu autorização para afastamento do País a HERALDO LUIS
DIAS  DA  SILVEIRA,  Professor  do  Magistério  Superior,  com  exercício  no  Departamento  de  Cirurgia  e
Ortopedia da Faculdade de Odontologia
Onde se lê: no período compreendido entre 30/08/2019 e 18/09/2019,
leia-se: no período compreendido entre 21/10/2019 e 09/11/2019, ficando ratificados os demais
termos. Solicitação nº 86656.
RUI VICENTE OPPERMANN
Reitor.
