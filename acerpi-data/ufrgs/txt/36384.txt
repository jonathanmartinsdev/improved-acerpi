Documento gerado sob autenticação Nº ZIT.938.794.A16, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2848                  de  03/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Declarar vago, a partir de 20 de março de 2017, o Cargo de Assistente em Administração, Código
701200, Nível de Classificação D, Nível de Capacitação III, Padrão 03, do Quadro de Pessoal, em decorrência
de posse em outro cargo inacumulável,  de MARIA CAROLINA SANTOS CARDOZO  com lotação na Pró-
Reitoria de Planejamento e Administração. Processo nº 23078.003667/2017-12.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
