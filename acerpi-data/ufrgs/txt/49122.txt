Documento gerado sob autenticação Nº KFK.721.718.0CI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             224                  de  05/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, DENISE FAGUNDES JARDIM (Siape: 0359323),  para
substituir EDILSON AMARAL NABARRO (Siape: 0357080 ), Diretora do Depto dos Programas de Acesso e
Permanência da CAF, Código SRH 1277, CD-4, em seu afastamento por motivo de férias, no período de
10/01/2018 a 17/01/2018, com o decorrente pagamento das vantagens por 8 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
