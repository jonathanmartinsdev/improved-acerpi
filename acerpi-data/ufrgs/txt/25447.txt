Documento gerado sob autenticação Nº MBM.773.041.841, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5806                  de  03/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°21844,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TÉCNICO EM CONTABILIDADE,
do Quadro de Pessoal  desta Universidade,  LETICIA SIGNOR (Siape:  1870960 ),   para substituir    JOSE
VANDERLEI  FERREIRA  (Siape:  0353326  ),  Diretor  do  Departamento  de  Programação  Orçamentária  da
PROPLAN,  Código CD-4,  em seu afastamento no país,  no período de 28/07/2016 a 29/07/2016,  com o
decorrente pagamento das vantagens por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
