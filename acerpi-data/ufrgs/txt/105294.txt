Documento gerado sob autenticação Nº OWL.198.359.952, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1592                  de  14/02/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ROBERTA DA SILVA BUSSAMARA RODRIGUES, Professor do
Magistério Superior, lotada e em exercício no Departamento de Química Inorgânica do Instituto de Química,
com a finalidade de desenvolver estudos em nível de Pesquisadora Visitante junto à  University of Illinois at
Chicago, em Chicago, nos Estados Unidos, no período compreendido entre 01/03/2020 a  31/07/2020, com
ônus Capes-Print. Processo nº 23078.533622/2019-30.
RUI VICENTE OPPERMANN
Reitor.
