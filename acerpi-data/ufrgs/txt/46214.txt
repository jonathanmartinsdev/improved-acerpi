Documento gerado sob autenticação Nº LMN.533.325.MLI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9765                  de  20/10/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  SÉRGIO  LUÍS  WETZEL  DE  MATTOS,  matrícula  SIAPE  n°  1282344,  lotado  e  em  exercício  no
Departamento de Direito Privado e Processo Civil da Faculdade de Direito, da classe C  de Professor Adjunto,
nível 03, para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 08/07/2015 a 07/07/2017,
com vigência financeira a partir de 08/07/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-
CONSUN. Processo nº 23078.510285/2017-41.
RUI VICENTE OPPERMANN
Reitor.
