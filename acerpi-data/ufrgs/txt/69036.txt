Documento gerado sob autenticação Nº CWR.863.127.UBK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6845                  de  31/08/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  27/08/2018,   referente  ao  interstício  de
19/04/2016 a 26/08/2018, para o servidor JONAS BARROS TEIXEIRA, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 0044870,  lotado  no  Centro de Estudos Costeiros, Limnológicos e
Marinhos, passando do Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.522665/2018-17:
Formação Integral de Servidores da UFRGS III CH: 80 (10/12/2007 a 20/04/2018)
ENAP - SEI! USAR CH: 20 (04/10/2016 a 24/10/2016)
ENDC - Educação Financeira para Consumidores CH: 20 (20/06/2017 a 10/07/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
