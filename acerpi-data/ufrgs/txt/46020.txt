Documento gerado sob autenticação Nº ESD.522.507.PQB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9603                  de  17/10/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Homologar  o  parecer  que  aprova  a  servidora  técnico-administrativa  FERNANDA DE LATORRE
FORTUNATO,  ocupante  do cargo de Assistente  em Administração,  no estágio  probatório  cumprido no
período de 15/10/2014 até 15/10/2017, fazendo jus, a partir desta data, à estabilidade no serviço público
federal.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
