Documento gerado sob autenticação Nº JLW.954.529.A42, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7226                  de  09/08/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora JULIANA RIGON PEDRINI, matrícula SIAPE n° 2551658, lotada no Colégio de Aplicação e com
exercício no Departamento de Expressão e Movimento, da classe de Professor D III, nível 02, para a classe de
Professor D III, nível 03, referente ao interstício de 29/08/2015 a 28/08/2017, com vigência financeira a partir
de 29/08/2017, conforme decisão judicial proferida no processo nº 5054491-30.2018.4.04.7100, da 1ª Vara
Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.521070/2018-36.
JANE FRAGA TUTIKIAN
Vice-Reitora.
