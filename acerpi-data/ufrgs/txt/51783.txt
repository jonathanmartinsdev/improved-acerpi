Documento gerado sob autenticação Nº JSM.779.302.B3E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             1960                  de  14/03/2018
Nomeação  da  Representação  Discente  do
Programa  de  Pós-graduação  da  Escola  de
Administração da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear  a  Representação  Discente  eleita  para  compor  o  Programa  de  Pós-graduação  de
Administração na Escola de Administração, com mandato de 01 (um) ano, a contar de 1º de novembro de
2017, atendendo o disposto nos artigos 175 do Regimento Geral  da Universidade e 79 do Estatuto da
Universidade, e considerando o processo nº 23078.504202/2018-65, conforme segue:
Representante Titular no Conselho do PPGA: Jaqueline Guimarães Santos
Representante Titular no Conselho do PPGA: Jéssica Pereira de Mello
Representante Titular no Conselho do PPGA: Magdalena Cortese Coelho
Representante Titular no Conselho do PPGA: Rodrigo Prado da Costa
Representante Titular no Conselho do PPGA: Camila Furlan da Costa
Representante Titular no Conselho do PPGA: Vanessa Marques Daniel
Representante Titular no Conselho do PPGA: Felipe Amaral Borges
Representante Titular no Conselho do PPGA: Matheus Oliveira Machado
Representante Titular no Conselho do PPGA: Lucas Casagrande
Representante Titular no Conselho do PPGA: Guillermo Fernando Hovermann da Cruz
Representante Titular no Conselho do PPGA: Renato Koch Colomby
Representante Titular no Conselho do PPGA: Fernando Nichterwitz Scherer
Representante Suplente no Conselho do PPGA: Bruno de Souza Lessa
Representante Suplente no Conselho do PPGA: Roberto Cunha Ferreira
Representante Suplente no Conselho do PPGA: Fernanda de Almeida Pinto
Representante Suplente no Conselho do PPGA: Carla Freitas Silveira Netto
Representante Suplente no Conselho do PPGA: Jaqueline Silinske
Representante Suplente no Conselho do PPGA: Ana Clara Aparecida Alves de Souza
Representante Suplente no Conselho do PPGA: Aniel Alberto Altamirano Ogarrio
 
SUZI ALVES CAMEY
Documento gerado sob autenticação Nº JSM.779.302.B3E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Pró-Reitora de Assuntos Estudantis
