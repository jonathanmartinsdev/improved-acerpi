Documento gerado sob autenticação Nº CVG.711.642.KFR, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2590                  de  11/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Tornar sem efeito a PUBLICAÇÃO das Portarias números 2398 e 2399, de 01/04/2016, publicadas no
Diário Oficial da União do dia 05/04/2016, seção 2, página 30. Processo nº 23078.006738/2016-58.
RUI VICENTE OPPERMANN
Vice-Reitor
