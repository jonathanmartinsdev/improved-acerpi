Documento gerado sob autenticação Nº EFO.017.079.443, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3842                  de  04/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  02/05/2017,   referente  ao  interstício  de
21/07/2014 a 01/05/2017, para o servidor GERSON ANDRADE DA SILVA, ocupante do cargo de Técnico em
Contabilidade - 701224, matrícula SIAPE 2143904,  lotado  na  Pró-Reitoria de Extensão, passando do Nível
de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.007566/2017-11:
ILB - Ética e Administração Pública CH: 40 (09/03/2017 a 29/03/2017)
ILB - Deveres, proibições e responsabilidades do servidor público federal CH: 60 Carga horária utilizada: 50
hora(s) / Carga horária excedente: 10 hora(s) (30/03/2017 a 27/04/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
