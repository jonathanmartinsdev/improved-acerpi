Documento gerado sob autenticação Nº CLG.773.172.URJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8790                  de  27/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°57224,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, MARCIA MARIA MATTEI LANZIOTTI (Siape:
2210912 ),  para substituir   MATHEUS BITENCOURT DA COSTA (Siape: 1964887 ), Coordenador do Núcleo de
Infraestrutura da Diretoria Administrativa do Campus Litoral Norte, Código FG-1, em seu afastamento por
motivo  de  Laudo Médico  do  titular  da  Função,  no  dia  28/08/2019,  com o  decorrente  pagamento  das
vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
