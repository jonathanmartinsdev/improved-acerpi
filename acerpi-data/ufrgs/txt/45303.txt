Documento gerado sob autenticação Nº YKG.708.954.8B0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9132                  de  29/09/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder afastamento parcial, no período de 16/10/2017 a 09/12/2017, para a servidora GENI DOS
SANTOS MARIA, ocupante do cargo de Técnico de Laboratório Área - 701244, matrícula SIAPE 1683544,
 lotada  na  Faculdade de Farmácia, para cursar o Mestrado Profissional em Química em Rede Nacional,
oferecido pela UFRGS; conforme o Processo nº 23078.513334/2017-05.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
