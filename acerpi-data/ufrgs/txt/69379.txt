Documento gerado sob autenticação Nº FOF.063.864.1ET, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7091                  de  06/09/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar  na  Superintendência  de  Infraestrutura,  com  exercício  no  Planejamento  e  Assessoria  da
Superintendência de Infraestrutura, ELISA CERIOTTI TRINDADE, nomeada conforme Portaria Nº 6313/2018
de 15 de agosto de 2018, publicada no Diário Oficial da União no dia 16 de agosto de 2018, em efetivo
exercício desde 04 de setembro de 2018, ocupante do cargo de ADMINISTRADOR, Ambiente Organizacional
Administrativo, classe E, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
