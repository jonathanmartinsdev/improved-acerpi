Documento gerado sob autenticação Nº LAJ.889.706.AER, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6150                  de  13/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora do Magistério  do Ensino Básico,  Técnico e  Tecnológico NARA BEATRIZ KRELING DA ROSA,
matrícula SIAPE n° 2211794, com lotação no Colégio de Aplicação, da classe D IV, nível 03, para a classe D IV,
nível 04, referente ao interstício de 24/05/2016 a 23/05/2018, com vigência financeira a partir de 14/06/2018,
de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e a Decisão nº
328/2015 - CONSUN. Processo nº 23078.514392/2018-29.
JANE FRAGA TUTIKIAN
Vice-Reitora.
