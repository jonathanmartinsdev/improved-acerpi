Documento gerado sob autenticação Nº ZXI.895.350.FP4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9367                  de  23/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais
estatutárias
RESOLVE
Retificar a Portaria n° 6103/2016, de 17/08/2016, que concedeu autorização para afastamento do
País  a  ANA  LEONOR  CHIES  SANTIAGO  SANTOS,  Professor  do  Magistério  Superior,  com  exercício  no
Departamento de Astronomia do Instituto de Física
Onde se lê: com ônus,
leia-se: com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias), ficando ratificados os demais termos.
Solicitação nº 21904.
RUI VICENTE OPPERMANN
Reitor
