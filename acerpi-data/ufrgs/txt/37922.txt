Documento gerado sob autenticação Nº BCX.459.645.D3D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4088                  de  10/05/2017
O  COORDENADOR  DA  COORDENADORIA  ADMINISTRATIVA  E  FINANCEIRA  DA  PROPLAN  DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7633, de 29 de setembro de 2016 , e tendo em vista o que consta do Processo Administrativo n°
23078.510597/2016-73,  do  Contrato  nº  005/PROPLAN/NUDECON/2017,  da  Lei  10.520/02 e  ainda da  Lei
8.666/93,
              RESOLVE:
 
            Aplicar as sanções administrativas de IMPEDIMENTO DE LICITAR E CONTRATAR COM A UNIÃO PELO
PRAZO DE 06 (SEIS) MESES e MULTA DE 5% (cinco por cento) sobre o valor das parcelas que lhe deram causa,
no montante de R$ 3.385,79 (três mil, trezentos e oitenta e cinco reais e setenta e nove centavos) conforme o
demonstrativo de cálculo documento nº 0523125, previstas no item 02 e inciso III, item 03 da cláusula décima
primeira do referido Contrato, à Empresa MARIA ALBERTINA AVILA SOARES, CNPJ n.º 05.786.780/0001-60,
pelas entregas em desacordo com o contrato, conforme atestado pela DAL documento SEI nº 0481041,
0499472,  0512162  e  0522409,  bem  como  pelo  NUDECON  documento  SEI  nº  0512829  do  processo
supracitado.
 
 
              Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
 
LUIS ROBERTO DA SILVA MACEDO
Coordenador da Coordenadoria Administrativa e Financeira da PROPLAN
