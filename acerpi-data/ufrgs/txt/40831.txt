Documento gerado sob autenticação Nº OZC.297.838.T9B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6065                  de  11/07/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°37372,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ADMINISTRADOR, do Quadro de
Pessoal desta Universidade, RAQUEL CAPIOTTI DA SILVA (Siape: 2060916 ),  para substituir   NINA CERVO
PAGNON (Siape: 2140040 ), Coordenador do Núcleo de Planejamento e Gestão, vinculado à DIMA do DDGP
DA  PROGESP,  Código  FG-2,  em  seu  afastamento  por  motivo  de  férias,  no  período  de  10/07/2017  a
31/07/2017, com o decorrente pagamento das vantagens por 22 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
