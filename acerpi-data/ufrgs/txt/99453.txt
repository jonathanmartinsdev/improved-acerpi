Documento gerado sob autenticação Nº TFE.770.847.VT3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9276                  de  14/10/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar  na Faculdade de Medicina,  com exercício  no Programa de Pós-Graduação em Saúde da
Criança  e  do  Adolescente,  Ambiente  Organizacional  Administrativo,  NICOLE  DE  CARVALHO  BARROS,
nomeada conforme Portaria Nº 8142/2019, de 06 de setembro de 2019, publicada no Diário Oficial da União
no dia 09 de setembro de 2019, em efetivo exercício desde 08 de outubro de 2019, ocupante do cargo de
Assistente em Administração,  classe D,  nível  I,  padrão 101,  no Quadro de Pessoal  desta Universidade.
Processo n° 23078.527369/2019-85.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
