Documento gerado sob autenticação Nº OAN.979.877.I3V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5931                  de  06/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/07/2017,   referente  ao  interstício  de
05/01/2016 a 04/07/2017, para a servidora JULIANA GRACIELI FONTANA, ocupante do cargo de Assistente
em Administração - 701200, matrícula SIAPE 2271224,  lotada  na  Pró-Reitoria de Graduação, passando do
Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.012813/2017-09:
Formação Integral de Servidores da UFRGS III CH: 79 (10/02/2016 a 26/05/2017)
Fundação  Bradesco  -  Comunicação  Escrita  CH:  91  Carga  horária  utilizada:  11  hora(s)  /  Carga  horária
excedente: 80 hora(s) (04/05/2016 a 01/06/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
