Documento gerado sob autenticação Nº YQB.067.175.RF5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7642                  de  25/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°59490,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CARLOS AFONSO DE CASTRO BECK (Siape: 2028820 ),
 para substituir   RAFAEL DA ROSA ULGUIM (Siape: 2357560 ), Coordenador da Comissão de Pesquisa da
Faculdade de Veterinária, Código XX, em seu afastamento no país, no período de 25/09/2018 a 27/09/2018.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
