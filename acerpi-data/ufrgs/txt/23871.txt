Documento gerado sob autenticação Nº NGE.181.513.VED, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4732                  de  28/06/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LAVINIA SCHULER FACCINI, Professor do Magistério Superior,
lotada no Departamento de Genética e com exercício na Vice-Direção do Instituto de Biociências, com a
finalidade de participar do "56th Annual Meeting of the Teratology Society", em San Antonio , Texas, Estados
Unidos, no período compreendido entre 25/06/2016 e 30/06/2016, incluído trânsito, com ônus limitado.
Solicitação nº 21066.
CARLOS ALEXANDRE NETTO
Reitor
