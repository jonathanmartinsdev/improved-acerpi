Documento gerado sob autenticação Nº FLM.510.907.USB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10262                  de  13/11/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  RUTH  FRANCINI  RAMOS  SABAT,  matrícula  SIAPE  n°  1528067,  lotada  e  em  exercício  no
Departamento de Estudos Básicos da Faculdade de Educação, da classe C  de Professor Adjunto, nível 04,
para a classe D  de Professor Associado, nível 01, referente ao interstício de 27/03/2012 a 26/03/2014, com
vigência financeira a partir de 27/03/2014, conforme decisão judicial proferida no processo nº 5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.528725/2019-88.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
