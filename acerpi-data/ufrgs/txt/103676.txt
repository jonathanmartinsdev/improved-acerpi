Documento gerado sob autenticação Nº ZVS.688.731.LKT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             468                  de  12/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°53422,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  HELOISA  JUNQUEIRA  (Siape:  0357468  ),   para
substituir    LUCIANO BEDIN DA COSTA (Siape:  2835328  ),  Coordenador  da  Comissão  de  Extensão  da
Faculdade de Educação, em seu afastamento por motivo de férias, no período de 13/01/2020 a 19/01/2020.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
