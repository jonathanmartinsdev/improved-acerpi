Documento gerado sob autenticação Nº KTU.089.854.89V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8694                  de  15/09/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar a partir  da data de publicação no Diário Oficial  da União,  a  ocupante do cargo de
Estatístico -  701033,  do Nível  de Classificação EIII,  do Quadro de Pessoal  desta Universidade,  MARILIA
CANABARRO ZORDAN, matrícula SIAPE 2571348, da função de Diretora da Divisão de Gestão de Informação
e Conhecimento do DIPI da PROPLAN, Código SRH 1389, Código FG-1, para a qual foi designada pela Portaria
nº  8011/13  de  09/12/2013,  publicada  no  Diário  Oficial  da  União  de  10/12/2013.  Processo  nº
23078.514503/2017-16.
JANE FRAGA TUTIKIAN
Vice-Reitora.
