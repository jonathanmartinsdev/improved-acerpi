Documento gerado sob autenticação Nº SMO.333.108.23C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6262                  de  14/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora JULIANA PETRI TAVARES, matrícula SIAPE n° 1724047, lotada e em exercício no Departamento de
Enfermagem Médico-Cirúrgica da Escola de Enfermagem, da classe A  de Professor Adjunto A, nível 01, para a
classe A  de Professor Adjunto A, nível 02, referente ao interstício de 18/07/2016 a 17/07/2018, com vigência
financeira a partir de 18/07/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.514689/2018-94.
JANE FRAGA TUTIKIAN
Vice-Reitora.
