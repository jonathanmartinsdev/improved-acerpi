Documento gerado sob autenticação Nº ZAC.522.965.CET, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3142                  de  11/04/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
e, tendo em vista o que consta do Processo Administrativo n° 23078.507591/2016-19, da Lei 10.520/02, do
Contrato nº 007/PROPLAN/NUDECON/2017 e da Lei 8.666/93,
            RESOLVE:
 
          Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item I, da Cláusula Décima do referido
Contrato, à Empresa ENGEVISA SERVIÇOS DE ENGENHARIA LTDA, CNPJ nº 92.969.856/0001-98 pelo atraso
na prestação do serviço, conforme atestado pela SUPLOG/SUINFRA doc. SEI nº 0474209 e nº 0492793 bem
como pelo NUDECON doc. SEI nº 0496240 do processo administrativo supracitado.
 
           Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
