Documento gerado sob autenticação Nº CQV.052.702.HKU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9158                  de  02/10/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°34701,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do  Quadro  de  Pessoal  desta  Universidade,  ALANA  DE  OLIVEIRA  MEIRELLES  (Siape:
2054539 ),  para substituir   LUIZ ANTONIO MOREIRA GONCALVES (Siape: 0356203 ), Secretário do Conselho
de Ensino, Pesquisa e Extensão (CEPE), Código CD-4, em seu afastamento por motivo de férias, no período de
11/10/2017 a 17/10/2017, com o decorrente pagamento das vantagens por 7 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
