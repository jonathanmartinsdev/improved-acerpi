Documento gerado sob autenticação Nº HGS.341.343.606, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             617                  de  18/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  CAROLINA  FAUTH  VASSAO,  Matrícula  SIAPE  1676008,  ocupante  do  cargo  de
Bibliotecário-documentalista,  Código 701010,  do Quadro de Pessoal  desta Universidade,  para exercer  a
função de Chefe da Biblioteca de Administração, código SRH 470, código FG-5, com vigência a partir da data
de publicação no Diário Oficial da União. Processo nº 23078.501309/2019-32.
JANE FRAGA TUTIKIAN
Vice-Reitora.
