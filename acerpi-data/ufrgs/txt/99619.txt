Documento gerado sob autenticação Nº YCE.331.832.4EH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9406                  de  16/10/2019
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
              RESOLVE
 
              A partir do cálculo dos custos relativos a manutenção, servidores terceirizados, água e luz, fixar os
novos valores para a hospedagem na Colônia de Férias de Tramandaí, como segue:
 
             Valores aplicados a partir de 09/01/2020:
 
Alojamento - Ala Coletiva Diária = R$ 13,00  
Apartamento Pequeno Diária = R$ 60,00 06 dias = R$ 360,00
Apartamento Grande Diária = R$ 105,00 06 dias = R$ 630,00
 
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
