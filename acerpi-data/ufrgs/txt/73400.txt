Documento gerado sob autenticação Nº GZA.532.206.8MC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10049                  de  12/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/12/2018,   referente  ao  interstício  de
12/06/2017 a 11/12/2018, para o servidor MARCELO KOCHENBORGER SCARPARO, ocupante do cargo de
Técnico em Assuntos Educacionais - 701079, matrícula SIAPE 2405912,  lotado  no  Instituto de Filosofia e
Ciências  Humanas,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  E  I,  para  o  Nível  de
Classificação/Nível de Capacitação E II,  em virtude de ter realizado o(s) seguinte(s) curso(s),  conforme o
Processo nº 23078.534210/2018-36:
ILB - Ética e Administração Pública CH: 40 (18/09/2017 a 08/10/2017)
ILB - Conhecendo o novo acordo ortográfico CH: 20 (06/07/2017 a 26/07/2017)
ENAP - Introdução à Libras CH: 60 Carga horária utilizada: 20 hora(s) / Carga horária excedente: 40 hora(s)
(31/10/2017 a 11/12/2017)
ILB - Doutrina Política: Socialismo CH: 20 (04/07/2017 a 24/07/2017)
ENAP - SEI! USAR CH: 20 (31/10/2017 a 20/11/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
