Documento gerado sob autenticação Nº BNA.126.407.BBF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2828                  de  17/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  VALNER JOAO BRUSAMARELLO,  Professor  do Magistério
Superior, lotado no Departamento de Sistemas Elétricos de Automação e Energia da Escola de Engenharia e
com exercício no Programa de Pós-Graduação em Engenharia Elétrica, com a finalidade de participar da
"2018 IEEE International Instrumentation & Measurement Technology Conference", em Houston, Estados
Unidos, no período compreendido entre 13/05/2018 e 18/05/2018, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa - diárias). Solicitação nº 34126.
RUI VICENTE OPPERMANN
Reitor
