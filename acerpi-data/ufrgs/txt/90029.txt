Documento gerado sob autenticação Nº SQA.994.586.NAM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3413                  de  22/04/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°83255,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, SIMONE ZANON MOSCHEN (Siape: 2094154 ),  para
substituir   ROSELENE RICACHENEVSKY GURSKI (Siape: 1769502 ), Coordenador do PPG em Psicanálise-Clínica
e  Cultura,  Código FUC,  em seu afastamento no país,  no  período de 24/04/2019 a  26/04/2019,  com o
decorrente pagamento das vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
