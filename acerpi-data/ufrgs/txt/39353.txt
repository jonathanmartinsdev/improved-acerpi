Documento gerado sob autenticação Nº QFL.354.227.C2V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5075                  de  08/06/2017
  Designa  Comissão  Especial  para  propor
políticas, ações e valores relativos a bolsas
estudantis.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Designar
HELIO HENKIN - PROPLAN
VLADIMIR PINHEIRO DO NASCIMENTO - PROGRAD
SUZI ALVES CAMEY - PRAE
LUIS DA CUNHA LAMB - PROPESQ
SANDRA DE FATIMA BATISTA DE DEUS - PROREXT
DENISE FAGUNDES JARDIM - CAF
     
para, sob a presidência do primeiro, compor Comissão Especial objetivando propor políticas, ações e valores
relativos a bolsas estudantis; estabelecer sistemática para seu gerenciamento, bem como acompanhar os
programas das mesmas.
 
              Revogar a Portaria nº 2463, de 30 de abril de 2013, a partir desta data.
RUI VICENTE OPPERMANN,
Reitor.
