Documento gerado sob autenticação Nº GIM.969.017.TVN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8514                  de  12/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°31467,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  BIBLIOTECÁRIO-
DOCUMENTALISTA, do Quadro de Pessoal desta Universidade, ANA LUCIA DE MACEDO RUDIGER (Siape:
0357554 ),  para substituir   LETICIA STREHL (Siape: 2280930 ), Diretor da Biblioteca Central, Código CD-4, em
seu afastamento por motivo de férias, no período de 18/09/2017 a 27/09/2017, com o decorrente pagamento
das vantagens por 10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
