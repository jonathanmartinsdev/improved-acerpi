Documento gerado sob autenticação Nº MFW.174.624.NOA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4259                  de  11/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  HELENA  ARAUJO  RODRIGUES  KANAAN,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Artes Visuais do Instituto de Artes, com a
finalidade de participar do "Summer Workshop on Aluminum Plate Lithography", em Albuquerque, Estados
Unidos, no período compreendido entre 07/07/2018 e 07/08/2018, incluído trânsito, com ônus limitado.
Solicitação nº 45171.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
