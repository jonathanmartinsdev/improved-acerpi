Documento gerado sob autenticação Nº NYN.047.153.9JC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2890                  de  19/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  IONARA RODRIGUES  SIQUEIRA,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Farmacologia do Instituto de Ciências Básicas da Saúde,
com a finalidade de participar do "Annual Meeting of the International Society for Extracellular Vesicles", em
Barcelona, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias) e realizar visita à  University of Valencia,
Espanha, com ônus limitado, no período compreendido entre 30/04/2018 e 09/05/2018, incluído trânsito.
Solicitação nº 34646.
RUI VICENTE OPPERMANN
Reitor
