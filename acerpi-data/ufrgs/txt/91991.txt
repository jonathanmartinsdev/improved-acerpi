Documento gerado sob autenticação Nº RQI.291.671.7PD, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
DEPARTAMENTO DE DESENVOLVIMENTO E GESTÃO DE PESSOAS
ESCOLA DE DESENVOLVIMENTO DE SERVIDORES DA UFRGS
Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
(51) 3308-3015/3219/3914/4596 - edufrgs@progesp.ufrgs.br
Processo n°: 23078.513998/2017-66
Servidor: TALES RENAN MIRANDA DO PRADO
Cargo: Técnico de Tecnologia da Informação
Lotação: Centro de Processamento de Dados
Ambiente Organizacional: Informação
Nível de Classificação e Nível de Capacitação: D II
PARECER N° 747/2019
Trata este expediente da retificação do Parecer n° 515/2019, de 09/04/2019, que concede horário
especial  para  servidor  estudante  a  TALES  RENAN  MIRANDA  DO  PRADO,  Técnico  de  Tecnologia  da
Informação, com exercício na Divisão de Desenvolvimento de Software do Centro de Processamento de
Dados. Processo nº 23078.513998/2017-66.
Tendo em vista a exoneração do Servidor, conforme Despacho SEI 1607281,
 
Onde se lê:
"somos pelo deferimento da solicitação para o período de 03/04/2019 a 26/07/2019",
leia-se:
"somos pelo deferimento da solicitação para o período de 03/04/2019 a 30/05/2019".
Em 29/05/2019.
BIANCA SPODE BELTRAME
Divisão de Análise e Orientação do Desenvolvimento na Carreira
De acordo. Ao requerente, para ciência e conclusão do processo.
Em 29/05/2019.
KAREN WERLANG LUNKES
Diretora da Divisão de Análise e Orientação do Desenvolvimento na Carreira
