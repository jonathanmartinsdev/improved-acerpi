Documento gerado sob autenticação Nº FGY.615.507.FLB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5093                  de  14/06/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, VALTER ROESLER, matrícula SIAPE n° 1550432, lotado no Departamento de
Informática Aplicada, como Diretor Substituto do Centro Interdisciplinar de Novas Tecnologias da Educação -
CINTED, com vigência a partir da data deste ato até 06/01/2021, a fim de completar o mandato do professor
JOSE VALDENI DE LIMA. Processo nº 23078.512324/2019-14.
JANE FRAGA TUTIKIAN
Vice-Reitora.
