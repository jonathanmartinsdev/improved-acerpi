Documento gerado sob autenticação Nº RDZ.069.854.QN4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7900                  de  30/08/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.512313/2019-26  e
23078.003088/2015-16, do Contrato nº 101/2015, da Lei nº 10.520/02 e ainda, da Lei nº 8.666/93,
            RESOLVE:
 
            Aplicar à Empresa LIDERANÇA LIMPEZA E CONSERVAÇÃO LTDA, CNPJ n.º 00.482.840/0001-38,
conforme atestado pela DEPGERTE no Ofício nº 659/2019 (Doc. SEI nº 1580154 e Doc. SEI nº 1635478), bem
como  pelo  NUDECON  (Doc.  SEI  nº  1736698)  do  Processo  nº  23078.512313/2019-26,  a(s)  seguinte(s)
sanção(ões) administrativa(s):
 
            ADVERTÊNCIA ,  prevista  na  alínea  "a"  da  Cláusula  Décima-segunda  do  Contrato,  pelos
descumprimentos:  intempestividade no pagamento de  horas  extras  e  não entrega  de  documentos  (1ª
ocorrência);
 
            Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
