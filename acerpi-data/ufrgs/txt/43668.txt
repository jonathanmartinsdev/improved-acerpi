Documento gerado sob autenticação Nº DNG.478.255.M77, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8073                  de  28/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CHARLES JOSE BONATO,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Astronomia do Instituto de Física, com a finalidade de participar
do "1st J-PLUS Workshop and 15th J-PAS Meeting", com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias) e
conferências no  Centro de Estudios de Física del Cosmos de Aragón, em Teruel, Espanha, com ônus limitado,
no período compreendido entre 27/09/2017 e 16/10/2017, incluído trânsito. Solicitação nº 30301.
RUI VICENTE OPPERMANN
Reitor
