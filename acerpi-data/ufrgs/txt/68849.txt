Documento gerado sob autenticação Nº MFJ.839.903.H6F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6752                  de  30/08/2018
A VICE-REITORA  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Retificar  a Portaria n° 6582/2018,  de 23/08/2018,  que designa  RAFAEL STIELER,  Professor do
Magistério Superior, com exercício no Departamento de Química Inorgânica do Instituto de Química, como
Coordenador Substituto da Central Analítica do Instituto de Química. Processo nº 23078.522186/2018-92.
 
Onde se lê:
"... no período de 26/08/2018 até 25/08/2020...",
leia-se:
"... no período de 28/08/2018 até 27/08/2020.", ficando ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora
