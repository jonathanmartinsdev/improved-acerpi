Documento gerado sob autenticação Nº ZLN.673.538.9J0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10770                  de  03/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°89101,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PRODUTOR CULTURAL, do
Quadro de Pessoal desta Universidade, JULIANA GONÇALVES MOTA (Siape: 1651810 ),  para substituir  
EDUARDO  CARDOSO  (Siape:  2620161  ),  Coordenador  da  Ponto  UFRGS  vinculado  ao  Departamento
Administrativo e de Registro da Extensão (DARE) da Pró-Reitoria de Extensão (PROREXT), Código FG-1, em seu
afastamento no país, no período de 03/12/2019 a 05/12/2019, com o decorrente pagamento das vantagens
por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
