Documento gerado sob autenticação Nº GZS.601.586.M15, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6917                  de  03/09/2018
A  VICE-REITORA,  NO  EXERCÍCIO  DA  REITORIA,  NO  EXERCÍCIO  DA  REITORIA  DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto
na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a partir  da data de publicação no Diário Oficial  da União,  o ocupante do cargo de
Assistente  em  Administração  -  701200,  do  Nível  de  Classificação  DIV,  do  Quadro  de  Pessoal  desta
Universidade, TURENE ANDRADE E SILVA NETO, matrícula SIAPE 0356721 da função de Diretor da Divisão
de Registro de Preços do DELIT da PROPLAN, Código SRH 393, Código FG-4, para a qual foi designado pela
Portaria nº 3770/2011, de 11/08/2011, publicada no Diário Oficial  da União de 12/08/2011, por ter sido
designado para outra função gratificada. Processo nº 23078. 517785/2018-94. 
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
