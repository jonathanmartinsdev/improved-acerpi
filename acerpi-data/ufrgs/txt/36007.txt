Documento gerado sob autenticação Nº ETY.485.646.LDQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2610                  de  24/03/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de LUCIA ROTTAVA, Professor do Magistério Superior, lotada e em
exercício no Departamento de Letras Clássicas e Vernáculas do Instituto de Letras, com a finalidade de
participar  da  "27th  European Systemic  Functional  Linguistics  Conference",  em Salamanca,  Espanha,  no
período compreendido entre 27/06/2017 e 03/07/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa - diárias). Solicitação nº 26368.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
