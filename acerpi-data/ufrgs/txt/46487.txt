Documento gerado sob autenticação Nº PNA.111.498.F83, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10050                  de  31/10/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  EDUARDO  PANDOLFI  PASSOS,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Ginecologia e Obstetrícia da Faculdade de Medicina,
com  a  finalidade  de  participar  da  "2017  Translatioal  Reproductive  Biology  and  Clinical  Reproductive
Endocrinology Conference", em Nova Iorque, Estados Unidos, no período compreendido entre 15/11/2017 e
20/11/2017, incluído trânsito, com ônus limitado. Solicitação nº 32615.
RUI VICENTE OPPERMANN
Reitor
