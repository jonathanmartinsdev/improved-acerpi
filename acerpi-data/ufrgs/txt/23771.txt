Documento gerado sob autenticação Nº BMM.839.479.F1A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4657                  de  24/06/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, ANGELO LUIZ FREDDO, CPF n° 96747870034, matrícula SIAPE n° 1494665, lotado
no Departamento de Cirurgia e Ortopedia da Faculdade de Odontologia, como Chefe Substituto do Depto de
Cirurgia e Ortopedia da Faculdade de Odontologia, para substituir automaticamente o titular desta função
em seus afastamentos ou impedimentos regulamentares, com vigência a partir da data deste ato, pelo
período de 02 anos. Processo nº 23078.013082/2016-20.
RUI VICENTE OPPERMANN
Vice-Reitor
