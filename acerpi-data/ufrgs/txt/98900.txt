Documento gerado sob autenticação Nº NNB.083.755.MUQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8923                  de  03/10/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  29/09/2019,   referente  ao  interstício  de
22/01/2018 a 28/09/2019, para o servidor VLADIMIR LUCIANO PINTO, ocupante do cargo de Bibliotecário-
documentalista - 701010, matrícula SIAPE 1646749,  lotado  no  Instituto de Letras, passando do Nível de
Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.526571/2019-90:
Formação Integral de Servidores da UFRGS V CH: 147 (07/04/2017 a 05/09/2019)
MAST - IV Curso de Formação de Gestão de Coleções Especiais CH: 35 Carga horária utilizada: 33 hora(s) /
Carga horária excedente: 2 hora(s) (10/09/2018 a 14/09/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
