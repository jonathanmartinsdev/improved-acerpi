Documento gerado sob autenticação Nº MVZ.337.661.8B5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             173                  de  07/01/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°44555,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MARILISA BIALVO HOFFMANN (Siape: 1065884 ),
 para substituir   VALERIA DA CRUZ VIANA LABREA (Siape: 1789524 ), Coordenador da COMGRAD do Curso de
Licenciatura em Educação do Campo Ciências da Natureza, Código FUC, em seu afastamento por motivo de
férias, no período de 07/01/2019 a 08/02/2019, com o decorrente pagamento das vantagens por 33 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
