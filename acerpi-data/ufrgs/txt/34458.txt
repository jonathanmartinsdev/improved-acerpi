Documento gerado sob autenticação Nº DLC.448.031.4MR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1571                  de  16/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora VALENTINA COUTINHO BALDOTO GAVA CHAKR,  matrícula SIAPE n° 1440834,  lotada e em
exercício no Departamento de Pediatria da Faculdade de Medicina, da classe A  de Professor Adjunto A, nível
01, para a classe A  de Professor Adjunto A, nível 02, referente ao interstício de 19/08/2014 a 18/08/2016, com
vigência financeira a partir de 19/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro
de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.510356/2016-24.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
