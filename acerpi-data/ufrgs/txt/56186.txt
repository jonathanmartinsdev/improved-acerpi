Documento gerado sob autenticação Nº GGG.909.796.VDP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4954                  de  10/07/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ROBERTO GIUGLIANI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de participar do
"15th International Symposium on MPS and Related Diseases", em San Diego, Estados Unidos, no período
compreendido entre 01/08/2018 e 05/08/2018, incluído trânsito, com ônus limitado. Solicitação nº 47585.
RUI VICENTE OPPERMANN
Reitor
