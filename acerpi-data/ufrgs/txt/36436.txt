Documento gerado sob autenticação Nº LHQ.751.072.290, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2909                  de  03/04/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 21/12/2016,  a ocupante do cargo de Professor do Magistério
Superior, classe Adjunto, do Quadro de Pessoal desta Universidade, LUCIANA LAUREANO PAIVA, matrícula
SIAPE n° 1844915, da função de Coordenadora da COMGRAD do Curso de Bacharelado em Fisioterapia da
ESEF, Código SRH 1237, código FUC, para a qual foi designada pela Portaria 6419/2015, de 20/08/2015,
publicada no Diário Oficial da União do dia 24/08/2015. Processo nº 23078.026335/2016-25.
JANE FRAGA TUTIKIAN
Vice-Reitora.
