Documento gerado sob autenticação Nº IQX.147.291.UKB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2468                  de  21/03/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, à
Professora  LIVIA  KMETZSCH  ROSA  E  SILVA,  matrícula  SIAPE  2095352,  lotada  e  em  exercício  no
Departamento de Biologia Molecular e Biotecnologia do Instituto de Biociências, da classe A  de Professor
Adjunto A, nível 02, para a classe C  de Professor Adjunto, nível 01, com vigência financeira a partir da data de
publicação da portaria, de acordo com o que dispõe os incisos de I a IV e o parágrafo único do artigo 10 da
Portaria nº 554 do Ministério da Educação, de 20 de agosto de 2013. Processo nº 23078.501228/2017-71.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
