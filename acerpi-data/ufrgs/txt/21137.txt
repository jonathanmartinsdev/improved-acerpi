Documento gerado sob autenticação Nº AZO.984.834.MH3, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3060                  de  26/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  08/04/2016,   referente  ao  interstício  de
01/07/2014 a 07/04/2016, para a servidora MARIANA CASULA GOMES BRANDAO, ocupante do cargo de
Assistente em Administração -  701200,  matrícula SIAPE 2137771,   lotada  na  Escola de Engenharia,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  I,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.007563/2016-04:
Formação Integral de Servidores da UFRGS IV CH: 126 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 36 hora(s) (12/08/2014 a 27/07/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
