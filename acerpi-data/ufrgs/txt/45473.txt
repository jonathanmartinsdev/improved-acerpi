Documento gerado sob autenticação Nº ANI.402.202.VND, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9316                  de  06/10/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  03/10/2017,   referente  ao  interstício  de
29/03/2011 a 02/10/2017, para o servidor ANTONIO DA MAIA VIEGAS, ocupante do cargo de Servente de
Obras - 701824,  matrícula SIAPE 0358297,  lotado  na  Faculdade de Veterinária, passando do Nível de
Classificação/Nível de Capacitação A III, para o Nível de Classificação/Nível de Capacitação A IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.517331/2017-32:
Formação Integral de Servidores da UFRGS III CH: 60 (26/08/2013 a 27/06/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
