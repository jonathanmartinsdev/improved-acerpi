Documento gerado sob autenticação Nº TZO.457.616.DIE, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2425                  de  04/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE:
Conceder 90 (noventa) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de
11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
LUCILIA MARIA SILVEIRA BERNARDINO DA SILVA, com exercício no Laboratório Núcleo Orientado para a
Inovação da Edificação da Escola de Engenharia, a ser usufruída no período de 11/04/2016 a 09/07/2016,
referente ao quinquênio de 29/09/2008 a 28/09/2013, a fim de concluir o curso de Doutorado em Engenharia
Civil, conforme Processo nº 23078.005853/2016-13.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
