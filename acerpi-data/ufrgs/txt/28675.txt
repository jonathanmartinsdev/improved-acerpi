Documento gerado sob autenticação Nº PQP.478.868.AE2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7901                  de  05/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora PATRICIA OLIVEIRA BRITO,
ocupante  do  cargo  de  Assistente  Social-701006,  lotada  na  Pró-Reitoria  de  Assuntos  Estudantis,  SIAPE
1678956, para 52% (cinquenta e dois por cento), a contar de 07/06/2016, tendo em vista a conclusão do curso
de Mestrado em Educação, conforme o Processo nº 23078.012896/2016-47.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
