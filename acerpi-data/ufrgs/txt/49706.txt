Documento gerado sob autenticação Nº UDS.620.080.2O0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             621                  de  22/01/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°39002,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ECONOMISTA, do Quadro de
Pessoal desta Universidade, THAIS SANTANA DA ROSA (Siape: 1310561 ),  para substituir   EVERSON VIEIRA
DOS SANTOS (Siape: 0352320 ), Secretário do Centro de Estudos e Pesquisas Econômicas (IEPE), Código FG-7,
em seu afastamento por motivo de férias,  no período de 25/01/2018 a 29/01/2018,  com o decorrente
pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
