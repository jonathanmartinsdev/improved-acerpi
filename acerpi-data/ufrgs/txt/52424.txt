Documento gerado sob autenticação Nº WWK.678.201.ED4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2330                  de  27/03/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de FERNANDO NEVES HUGO, Professor do Magistério Superior,
lotado no Departamento de Odontologia Preventiva e Social da Faculdade de Odontologia e com exercício no
Centro de Pesquisa em Odontologia Social, com a finalidade de ministrar curso e realizar visita  à University
of Florida, em Gainesville, Estados Unidos, no período compreendido entre 04/04/2018 e 07/04/2018, incluído
trânsito, com ônus limitado. Solicitação nº 34648.
RUI VICENTE OPPERMANN
Reitor
