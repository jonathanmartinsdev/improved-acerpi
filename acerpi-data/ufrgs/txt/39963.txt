Documento gerado sob autenticação Nº SOE.566.127.G9E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5524                  de  26/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar RAFAEL SCHRODER PEREIRA, Matrícula SIAPE 2055946, ocupante do cargo de Assistente
em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de
Coordenador do Núcleo Acadêmico da Gerência Administrativa da Faculdade de Odontologia, Código SRH
624,  código FG-7,  com vigência a partir  da data de publicação no Diário Oficial  da União.  Processo nº
23078.011587/2017-31.
JANE FRAGA TUTIKIAN
Vice-Reitora.
