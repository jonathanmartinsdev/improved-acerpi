Documento gerado sob autenticação Nº GNM.165.642.RNR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7067                  de  09/09/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Designar  ANA  MARIA  ROSADO  BUCHAIN,  Matrícula  SIAPE  0351094,  ocupante  do  cargo  de
Secretário Executivo, Código 701076, do Quadro de Pessoal desta Universidade, para exercer a função de
Coordenadora de Gabinete da Escola de Engenharia, Código SRH 366, Código FG-4, com vigência a partir da
data de publicação no Diário Oficial da União. Processo nº 23078.040027/2014-41.
CARLOS ALEXANDRE NETTO
Reitor
