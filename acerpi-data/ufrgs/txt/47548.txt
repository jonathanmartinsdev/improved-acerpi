Documento gerado sob autenticação Nº ZBC.252.035.80P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10830                  de  28/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, KARINE STORCK, em virtude de habilitação em Concurso Público de
Provas e Títulos, conforme Edital Nº 54/2017 de 1 de Novembro de 2017, homologado em 03 de novembro
de 2017 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de
28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO DO ENSINO
BÁSICO, TÉCNICO E TECNOLÓGICO do Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de
Professor D, Nível I, do Quadro de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação
Exclusiva), junto ao Departamento de Expressão e Movimento do Colégio de Aplicação, em vaga decorrente
da aposentadoria do Professor Geraldo Ribas Machado, código nº 275604, ocorrida em 16 de Outubro de
2015, conforme Portaria nº. 7.888, de 15 de outubro de 2015, publicada no Diário Oficial da União de 16 de
outubro de 2015. Processo nº. 23078.509134/2016-69.
RUI VICENTE OPPERMANN
Reitor.
