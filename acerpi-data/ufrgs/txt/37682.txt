Documento gerado sob autenticação Nº AGF.113.681.VFQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3794                  de  03/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  PATRICIA  ALEJANDRA  BEHAR,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Estudos Especializados da Faculdade de Educação, com
a finalidade de participar de reunião na Columbia University e da "The International  Conference  on E-
Learning in the Workplace", em Nova Iorque, Estados Unidos, no período compreendido entre 09/06/2017 e
17/06/2017, incluído trânsito, com ônus UFRGS (Faculdade de Educação). Solicitação nº 26618.
RUI VICENTE OPPERMANN
Reitor
