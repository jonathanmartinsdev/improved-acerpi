Documento gerado sob autenticação Nº JVG.833.584.C0L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10511                  de  17/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor LUIZ RICARDO RODRIGUES DE ANDRADE, ocupante do cargo de  Técnico em
Cinematografia - 701223, lotado no Gabinete do Reitor, SIAPE 0353755, o percentual de 25% (vinte e cinco
por cento) de Incentivo à Qualificação, a contar de 11/11/2017, tendo em vista a conclusão do curso Superior
de Tecnologia em Gestão Pública, conforme o Processo nº 23078.014446/2017-70.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
