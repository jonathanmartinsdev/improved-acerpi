Documento gerado sob autenticação Nº IYQ.808.544.QUB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6689                  de  30/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do país  de SIMONE VALDETE DOS SANTOS,  Professor do Magistério
Superior, lotada no Departamento de Estudos Básicos  e com exercício na Faculdade de Educação, com a
finalidade de proferir palestra no "Congresso Internacional Transformações e (In)Consistências das Dinâmicas
Educativas: Mudanças na Educação e Lei de Bases", em Coimbra, Portugal, no período compreendido entre
12/10/2016 e 17/10/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação
nº 21023.
CARLOS ALEXANDRE NETTO
Reitor
