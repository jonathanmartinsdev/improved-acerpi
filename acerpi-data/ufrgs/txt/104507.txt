Documento gerado sob autenticação Nº YFX.398.938.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1055                  de  30/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, de acordo com o artigo 3º da
Emenda Constitucional nº 103, de 12 de novembro de 2019, publicada no Diário Oficial da União do dia 13
subsequente, a ADRIANA SALDANHA FERRARI, matrícula SIAPE nº 0757041, no cargo de Assistente Social,
nível de classificação E, nível de capacitação IV, padrão 16, do Quadro desta Universidade, no regime de
quarenta horas semanais de trabalho, com exercício na Comissão de Graduação de Medicina da Faculdade
de Medicina, com proventos integrais e incorporando a vantagem pessoal de que trata a Lei nº 9.624, de 2 de
abril de 1998, que assegurou o disposto no artigo 3º da Lei nº 8.911, de 11 de julho de 1994. Processo
23078.533458/2019-61.
RUI VICENTE OPPERMANN
Reitor.
