Documento gerado sob autenticação Nº RMY.191.037.D1B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3427                  de  22/04/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta Universidade,  MARCELO GOTZ,  matrícula SIAPE n° 2616590,  lotado no Departamento de
Sistemas Elétricos de Automação e Energia da Escola de Engenharia, para exercer a função de Coordenador
da COMGRAD de Engenharia de Controle e Automação, Código SRH 1239, código FUC, com vigência a partir
de 28/05/2019 até 27/05/2021. Processo nº 23078.509302/2019-69.
JANE FRAGA TUTIKIAN
Vice-Reitora.
