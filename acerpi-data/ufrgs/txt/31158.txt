Documento gerado sob autenticação Nº QCK.177.169.PK4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9757                  de  08/12/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLARISSA GREGORY BRUNET,  Professor do Magistério do
Ensino Básico, Técnico e Tecnológico, lotada no Colégio de Aplicação e com exercício no Departamento de
Comunicação do Colégio de Aplicação, com a finalidade de participar de cursos junto ao Centre International
d'Études  Pédagogiques,  Université  de  Nantes,  em  Sèvres,  França,  no  período  compreendido  entre
09/12/2016 e 19/12/2016, incluído trânsito, com ônus limitado. Solicitação nº 25288.
RUI VICENTE OPPERMANN
Reitor
