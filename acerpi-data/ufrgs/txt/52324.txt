Documento gerado sob autenticação Nº FDP.406.835.KOO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2239                  de  26/03/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARCUS VINICIUS REIS SO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Odontologia Conservadora da Faculdade de Odontologia, com a
finalidade de participar de congressos junto à American Association of Endodontists, em Denver, Estados
Unidos, no período compreendido entre 24/04/2018 e 29/04/2018, incluído trânsito, com ônus limitado.
Solicitação nº 34183.
RUI VICENTE OPPERMANN
Reitor
