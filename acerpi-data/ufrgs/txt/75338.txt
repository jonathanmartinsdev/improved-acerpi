Documento gerado sob autenticação Nº LMQ.329.558.606, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             610                  de  18/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, VALESCA DALL ALBA, matrícula SIAPE n° 1457834, lotada no Departamento de
Nutrição da Faculdade de Medicina, como Chefe Substituta do Depto de Nutrição da Faculdade de Medicina,
para  substituir  automaticamente  o  titular  desta  função  em  seus  afastamentos  ou  impedimentos
regulamentares  na  vigência  do  presente  mandato,  sem  prejuízo  e  cumulativamente  com  a  função
de Coordenador Substituto do PPG: Ciências em Gastroenterologia. Processo nº 23078.501083/2019-70.
JANE FRAGA TUTIKIAN
Vice-Reitora.
