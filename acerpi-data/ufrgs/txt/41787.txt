Documento gerado sob autenticação Nº WKC.928.517.BV5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6803                  de  26/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  ALEXANDRE  DO  CANTO  ZAGO,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a
finalidade de participar de congresso junto à Sociedade Latino Americana de Cardiologia Invasiva, em Buenos
Aires,  Argentina,  no período compreendido entre 01/08/2017 e 04/08/2017,  incluído trânsito,  com ônus
limitado. Solicitação nº 29856.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
