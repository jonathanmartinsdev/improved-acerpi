Documento gerado sob autenticação Nº HVH.292.795.UTA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3310                  de  07/05/2018
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
 
              Alterar, a partir de 02 de maio de 2018, a composição da Comissão de Recursos da Pró-reitoria de
Assuntos Estudantis, estabelecida pela portaria nº 1828 de 05 de março de 2018, que passa a vigorar com os
seguintes integrantes:
 
Maria Conceição de Matos Braga, ocupante do cargo de Assistente em Administração, com exercício na•
Divisão de Bolsas e presidente da Comissão;
Élton Luís Bernardi Campanaro, Vice pró-reitor de Assuntos Estudantis;•
Andressa Lopes Nulle, ocupante do cargo de Administradora, com exercício na Divisão de Moradia•
Estudantil.
 
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
