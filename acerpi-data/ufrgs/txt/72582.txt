Documento gerado sob autenticação Nº BJF.456.336.96D, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9522                  de  23/11/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Criar, no âmbito desta Universidade, a seguinte função comissionada de coordenação de curso,
instituída pela Lei nº 12.667, de 25 de junho de 2012, publicada no Diário Oficial da União de 26 de junho de
2012, distribuídas pela Portaria nº 1.172/MEC, de 17 de setembro de 2012, publicada no Diário Oficial da
União de 18 de setembro de 2012, a partir da data deste ato, como segue:
 
Criar:  Coordenador  do  PPG  em  Dinâmicas  Regionais  e  Desenvolvimento  do  Campus  Litoral
Norte, Código FUC .
 
 
Processo nº 23078.532170/2018-98.
 
RUI VICENTE OPPERMANN
Reitor.
