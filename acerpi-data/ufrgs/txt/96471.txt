Documento gerado sob autenticação Nº RDK.898.978.4P8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7378                  de  15/08/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Retificar  a  Portaria  nº  7287/2019,  de  12/08/2019,  que  designou,  temporariamente,  CARLA
SCHWENGBER TEN CATEN (Siape: 2174654) para substituir LUIZ CARLOS PINTO DA SILVA FILHO  (Siape:
1276034), Processo SEI Nº 23078.521645/2019-00
onde se lê:
 
"... CARLA SCHWENGBER TEN CATEN (Siape: 2174654 )...";
 
   leia-se:
 
"... NILSON ROMEU MARCILIO (Siape: 0358544 )..."; ficando ratificados os demais termos.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
