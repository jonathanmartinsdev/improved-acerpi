Documento gerado sob autenticação Nº QZQ.054.505.4BD, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1958                  de  14/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de FRANCISCO ELISEU AQUINO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Geografia do Instituto de Geociências, com a finalidade de realizar
visita ao Climate Change Institute, The University of Maine, em Orono, Me, Estados Unidos, no período
compreendido  entre  01/04/2016  e  11/04/2016,  incluído  trânsito,  com  ônus  pelo  Setor  de  Projetos  e
Convênios da FAURGS. Solicitação nº 18493.
CARLOS ALEXANDRE NETTO
Reitor
