Documento gerado sob autenticação Nº JJI.514.968.8VA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1320                  de  10/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº 23078.002404/2017-96
RESOLVE
Tornar  insubsistente  a  Portaria  nº  110/2017,  de  04/01/2017,  que  designou  temporariamente
RAQUEL  CAPIOTTI  DA  SILVA  (Siape:  2060916),  para  substituir  ELIANA  VENTORINI  (Siape:  1688979  ),
Coordenadora da Coordenadoria de Concursos, Mobilidade e Acompanhamento, vinc. ao DDGP da PROGESP,
Código SRH 954, código CD-4, no período de 02/01/2017 à 04/01/2017, gerada conforme Solicitacao de Férias
nº 31143.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
