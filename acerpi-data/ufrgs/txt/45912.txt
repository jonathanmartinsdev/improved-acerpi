Documento gerado sob autenticação Nº WGK.399.780.PQB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9586                  de  16/10/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito a partir de 11/10/2017 a Portaria n° 6585/2017, de 21/07/2017, que nomeou o
Núcleo de Gestão de Desempenho do Centro de Processamento de Dados.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
