Documento gerado sob autenticação Nº IWW.413.044.U5Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6783                  de  30/08/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
e,  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.507306/2016-60  e
23078.515149/2018-28, da Lei 10.520/02, do Pregão Eletrônico SRP nº 184/2016, e ainda, da Lei 8.666/93,
RESOLVE:
 
Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 29.1.1, do Pregão Eletrônico em epígrafe,
à Empresa J. P. CAVEDON SOARES - ME, CNPJ nº 10.925.677/0001-94,  pelo atraso na entrega do objeto,
conforme atestado pela Seção de Compra e Controle de Estoque de Materiais-SCOMPRA da UFRGS e pelo
NUDECON (doc. SEI nº 1167875).
 
 Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
