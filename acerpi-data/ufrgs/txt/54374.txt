Documento gerado sob autenticação Nº EOW.437.653.M3A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3577                  de  17/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  14/05/2018,   referente  ao  interstício  de
24/09/2016 a  13/05/2018,  para  o  servidor  ARON JONATÃ FUNKE,  ocupante  do cargo de  Técnico em
Hidrologia - 701242, matrícula SIAPE 2053958,  lotado  no  Instituto de Pesquisas Hidráulicas, passando do
Nível de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.511342/2018-90:
ANA - Lei das Águas CH: 20 (20/08/2017 a 02/09/2017)
ANA - Planejamento, Manejo e Gestão de Bacias CH: 40 (20/08/2017 a 12/09/2017)
CPRM - Análises Sedimentométricas CH: 26 (27/07/2015 a 30/07/2015)
ANA- Codificação de Bacias pelo Otto Pfafstetter CH: 20 (04/08/2017 a 04/09/2017)
ANA - Hidrologia Geral CH: 40 Carga horária utilizada: 24 hora(s)  /  Carga horária excedente: 16 hora(s)
(02/04/2018 a 14/05/2018)
ANA - Plano de Recursos Hídricos e Enquadramento dos Corpos de Água CH: 20 (20/08/2017 a 04/09/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
