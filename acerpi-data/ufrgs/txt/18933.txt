Documento gerado sob autenticação Nº GAV.076.547.RS5, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1765                  de  08/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de Outubro de 2012e conforme
solicitação de férias nº 19547
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, MARIA CELESTE OSORIO WENDER (Siape: 2215441),
 para substituir EDISON CAPP (Siape: 2232220), Coordenador do PPG em Ciências da Saúde: Ginecologia e
Obstetrícia, em seu afastamento por motivo de férias, no período de 01/02/2016 a 02/03/2016.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
