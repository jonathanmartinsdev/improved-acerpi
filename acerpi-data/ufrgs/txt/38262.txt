Documento gerado sob autenticação Nº IMK.468.462.CEV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4258                  de  15/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do país  de ANDREZA FRANCISCO MARTINS,  Professor do Magistério
Superior, lotada e em exercício no Departamento de Microbiologia, Imunologia e Parasitologia do Instituto de
Ciências Básicas da Saúde, com a finalidade de participar do "ASM Microbe 2017", em New Orleans, com
ônus  UFRGS (Pró-Reitoria  de  Pesquisa  -  diárias)  e  realizar   visita  ao  Centers  for  Disease  Control  and
Prevention, em Atlanta, Estados Unidos, com ônus limitado, no período compreendido entre 31/05/2017 e
10/06/2017, incluído trânsito, com ônus limitado. Solicitação nº 26995.
RUI VICENTE OPPERMANN
Reitor
