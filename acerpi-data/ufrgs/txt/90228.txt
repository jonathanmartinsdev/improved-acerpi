Documento gerado sob autenticação Nº GFF.569.745.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3534                  de  25/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  JONAS  ALEX  MORALES  SAUTE,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a
finalidade de participar da "71st American Academy of Neurology Annual Meeting", na Filadélfia, Estados
Unidos, no período compreendido entre 03/05/2019 e 10/05/2019, incluído trânsito, com ônus limitado.
Solicitação nº 83386.
RUI VICENTE OPPERMANN
Reitor
