Documento gerado sob autenticação Nº UPX.111.765.S01, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2978                  de  22/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°18335,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS, do Quadro de Pessoal desta Universidade, NAJARA CRUZ LOURENCO MACHADO (Siape:
0044874 ),  para substituir   ALEX NICHE TEIXEIRA (Siape: 3467254 ), Diretor da Editora, Código FG-1, em seu
afastamento do país, no período de 12/04/2016 a 17/04/2016, com o decorrente pagamento das vantagens
por 6 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
