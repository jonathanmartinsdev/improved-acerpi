Documento gerado sob autenticação Nº CTH.727.384.7VN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4804                  de  05/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar DIULIANE TRINDADE DA SILVA, Matrícula SIAPE 1736921, ocupante do cargo de Técnico
em Nutrição e Dietética, Código 701252, do Quadro de Pessoal desta Universidade, para exercer a função
de Chefe da Seção do RU3 da Divisão de Alimentação do DIE da PRAE, Código SRH 1296, Código FG-3, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.514418/2019-10.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
