Documento gerado sob autenticação Nº WFU.703.248.RPF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10238                  de  18/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  18/12/2018,   referente  ao  interstício  de
18/06/2017 a 17/12/2018, para a servidora MARA LUCIA VIVIAN DA LUZ, ocupante do cargo de Assistente
em Administração - 701200, matrícula SIAPE 2125960,  lotada  na  Escola de Engenharia, passando do Nível
de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.532925/2018-54:
Formação Integral de Servidores da UFRGS III CH: 74 (23/05/2016 a 24/07/2018)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 40 (26/10/2017 a 07/12/2017)
ILB - Cerimonial no Ambiente Legislativo CH: 40 Carga horária utilizada: 14 hora(s) / Carga horária excedente:
26 hora(s) (25/01/2018 a 09/03/2018)
TELESSAÚDERS - Atualização no combate vetorial ao Aedes aegypti CH: 22 (29/08/2016 a 29/08/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
