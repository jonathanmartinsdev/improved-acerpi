Documento gerado sob autenticação Nº YHU.049.124.IGL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2202                  de  23/03/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de VANESSA MARX, Professor do Magistério Superior, lotada e em
exercício no Departamento de Sociologia do Instituto de Filosofia e Ciências Humanas, com a finalidade de
participar  do  "Encuentro  Diálogos  Urbanos:  debates  y  reflexiones  para  construir  la  agenda  urbana",  em
Montevidéu, Uruguai, no período compreendido entre 15/04/2018 e 18/04/2018, incluído trânsito, com ônus
limitado. Solicitação nº 34774.
RUI VICENTE OPPERMANN
Reitor
