Documento gerado sob autenticação Nº EOG.911.147.C19, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9702                  de  25/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ELISEO BERNI REATEGUI, Professor do Magistério Superior,
lotado e  em exercício  no Departamento de  Estudos  Especializados  da  Faculdade de  Educação,  com a
finalidade de realizar visita à University of Toronto, em Toronto, Canadá, no período compreendido entre
03/11/2019 e 14/11/2019, incluído trânsito, com ônus CAPES/PRINT/UFRGS. Solicitação nº 87884.
RUI VICENTE OPPERMANN
Reitor
