Documento gerado sob autenticação Nº VLX.633.975.EUS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3237                  de  03/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de FERNANDO HAAS, Professor do Magistério Superior, lotado e
em exercício no Departamento de Física do Instituto de Física,  com a finalidade de participar do "19th
International  Congress  on  Plasma  Physics",  em  Vancouver,  Canadá,  no  período  compreendido  entre
02/06/2018 e 10/06/2018, incluído trânsito, com ônus limitado. Solicitação nº 45452.
RUI VICENTE OPPERMANN
Reitor
