Documento gerado sob autenticação Nº TOD.691.373.SLJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             301                  de  10/01/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°42746,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, SERGIO GONCALVES MACEDO JUNIOR (Siape:
0358726 ),  para substituir   CLAUDIOMAR OVIEDO RIBEIRO (Siape: 0356155 ), Coordenador de Pagamento,
Cadastro e Processos Judiciais do DAP da PROGESP, Código CD-4, em seu afastamento por motivo de férias,
no período de 14/01/2019 a 19/01/2019, com o decorrente pagamento das vantagens por 6 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
