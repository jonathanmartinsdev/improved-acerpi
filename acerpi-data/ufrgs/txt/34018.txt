Documento gerado sob autenticação Nº QIF.059.448.JHG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1176                  de  08/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº 23078.001766/2017-60
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, MARIA APARECIDA BERGAMASCHI (Siape: 2211551),
 para substituir GILBERTO ICLE (Siape: 2228826 ), Coordenadora do PPG em Educação, Código SRH 1157,
FUCC, em seu afastamento no país, no período de 23/01/2017 a 25/01/2017, com o decorrente pagamento
das vantagens por 03 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
