Documento gerado sob autenticação Nº ITD.996.750.8O5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6743                  de  31/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  08/08/2016,   referente  ao  interstício  de
02/09/2014 a 07/08/2016, para o servidor ADALBERTO DA COSTA OLIVEIRA, ocupante do cargo de Técnico
em  Audiovisual  -  701221,  matrícula  SIAPE  1872388,   lotado   na   Faculdade  de  Biblioteconomia  e
Comunicação,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de
Classificação/Nível de Capacitação D IV, em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.017391/2016-79:
Formação Integral de Servidores da UFRGS II CH: 50 (28/05/2013 a 31/03/2016)
UNB - Capacitação dos Voluntários da Copa do Mundo de Futebol 2014 CH: 140 Carga horária utilizada: 100
hora(s) / Carga horária excedente: 40 hora(s) (12/03/2014 a 06/08/2014)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
