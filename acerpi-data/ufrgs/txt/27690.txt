Documento gerado sob autenticação Nº MUA.551.319.1FK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7290                  de  15/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°21821,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, ANELISE LINERA LUTT (Siape: 2162180 ),  para
substituir   JOSE FABIANO GREGORY CARDOZO DE AGUIAR (Siape: 1759276 ), Coordenador de Mobilidade
Internacional com a América Latina, Código FG-1, em seu afastamento do país, no período de 01/09/2016 a
31/10/2016, com o decorrente pagamento das vantagens por 61 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
