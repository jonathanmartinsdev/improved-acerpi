Documento gerado sob autenticação Nº ERC.011.660.3BU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3888                  de  05/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta  Universidade,  JEFFERSON  CARDIA  SIMOES,  matrícula  SIAPE  n°  0358816,  lotado  no
Departamento de Geografia do Instituto de Geociências, como Chefe Substituto do Depto de Geografia do
Instituto de Geociências, para substituir automaticamente o titular desta função em seus afastamentos ou
impedimentos  regulamentares  no  período  de  21/05/2017  e  até  20/05/2019.  Processo  nº
23078.201150/2017-97.
JANE FRAGA TUTIKIAN
Vice-Reitora.
