Documento gerado sob autenticação Nº LYA.390.322.KV5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7121                  de  04/08/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor IAN ALEXANDER, matrícula SIAPE n° 2523041, lotado e em exercício no Departamento de Línguas
Modernas do Instituto de Letras, da classe C  de Professor Adjunto, nível 01, para a classe C  de Professor
Adjunto, nível 02, referente ao interstício de 27/03/2012 a 27/03/2016, com vigência financeira a partir de
01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e
a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.507881/2016-62.
JANE FRAGA TUTIKIAN
Vice-Reitora.
