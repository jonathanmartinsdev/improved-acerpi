Documento gerado sob autenticação Nº XNA.477.641.DPT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9001                  de  04/10/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 07/10/2019, a ocupante do cargo de Assistente em Administração - 701200,
do Nível de Classificação DIV, do Quadro de Pessoal desta Universidade, STEFÂNIA OLIVEIRA DA COSTA,
matrícula SIAPE 1479439, da função de Assessora da Secretaria de Relações Internacionais, Código SRH 349,
Código FG-2, para a qual foi designada pela Portaria nº 3802/2019, de 03/05/2019, publicada no Diário Oficial
da  União  de  07/05/2019,  por  ter  sido  designada  para  outra  função  gratificada.  Processo  nº
23078.526704/2019-28.
JANE FRAGA TUTIKIAN
Vice-Reitora.
