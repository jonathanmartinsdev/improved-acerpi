Documento gerado sob autenticação Nº FPY.090.553.TMI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3943                  de  08/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  03/05/2017,   referente  ao  interstício  de
17/10/2014  a  02/05/2017,  para  o  servidor  ROSINEI  DA  SILVA  CANTELI,  ocupante  do  cargo  de
Instrumentador Cirúrgico - 701207, matrícula SIAPE 1856746,  lotado  no  Hospital de Clínicas Veterinárias,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.201203/2017-70:
Formação Integral de Servidores da UFRGS I CH: 21 (28/04/2016 a 15/09/2016)
IPED - Curso de Saúde Pública no Brasil CH: 80 (11/09/2014 a 17/10/2014)
INEAD - Montagem e Manutenção de Computadores CH: 55 Carga horária utilizada: 19 hora(s) / Carga horária
excedente: 36 hora(s) (13/02/2017 a 13/03/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
