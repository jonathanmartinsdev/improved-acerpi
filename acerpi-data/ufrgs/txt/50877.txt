Documento gerado sob autenticação Nº LHV.330.533.APH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1390                  de  15/02/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de EDISON LUIZ SATURNINO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Estudos Básicos da Faculdade de Educação, com a finalidade de
participar do "XIII Congreso Iberoamericano de Historia de la Educación Latinoamericana", em Montevidéu,
Uruguai, no período compreendido entre 27/02/2018 e 03/03/2018, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa - diárias). Solicitação nº 34013.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
