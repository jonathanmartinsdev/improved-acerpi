Documento gerado sob autenticação Nº RBL.512.146.8VA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1319                  de  10/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016,do Magnífico
Reitor, e conforme processo nº 23078.002404/2017-96
RESOLVE
Retificar a Portaria nº 10116, de 21/12/2016, que designou, temporariamente, RAQUEL CAPIOTTI
DA SILVA (Siape: 2060916) para substituir NINA CERVO PAGNON (Siape: 2140040), 
 
onde se lê:
 
"... em seu afastamento por motivo de férias, no período de 26/12/2016 a 06/01/2017, com o
decorrente pagamento das vantagens por 12 dias."
 
           leia-se:
 
           "... em seu afastamento por motivo de férias, no período de 26/12/2016 a 30/12/2016, com o
decorrente pagamento das vantagens por 05 dias. Ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
