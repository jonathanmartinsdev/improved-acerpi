Documento gerado sob autenticação Nº NSZ.360.405.LME, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10776                  de  03/12/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar,  a  pedido,  a partir  de 30/11/2019,  a ocupante do cargo de Professor do Magistério
Superior,  classe  Adjunto,  do  Quadro  de  Pessoal  desta  Universidade,  DANIELA  GUZZON  SANAGIOTTO,
matrícula SIAPE n° 2449144, da função de Chefe Substituta do Depto de Hidromecânica e Hidrologia do IPH,
para a qual foi designada pela Portaria 7614/2019, de 22/08/2019. Processo nº 23078.521505/2019-23.
RUI VICENTE OPPERMANN
Reitor.
