Documento gerado sob autenticação Nº IAM.235.701.96D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9409                  de  21/11/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, LETÍCIA BECKER VIEIRA, matrícula SIAPE n° 2696404, lotada no Departamento de
Assistência  e  Orientação  Profissional  da  Escola  de  Enfermagem,  como  Chefe  Substituta  do  Depto  de
Assistência e Orientação Profissional da Escola de Enfermagem, com vigência a partir de 29/12/2018 até
28/12/2020. Processo nº 23078.531920/2018-12.
RUI VICENTE OPPERMANN
Reitor.
