Documento gerado sob autenticação Nº HXV.510.061.C5U, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4062                  de  06/06/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANDRE MOREIRA CUNHA, Professor do Magistério Superior,
lotado no Departamento de Economia e Relações Internacionais da Faculdade de Ciências Econômicas e com
exercício na Vice-Direção da Faculdade de Ciências Econômicas, com a finalidade de participar de curso na
Universidad Autónoma de Madrid,  Espanha,  no período compreendido entre 27/06/2016 e 04/07/2016,
incluído trânsito, com ônus limitado. Solicitação nº 19260.
CARLOS ALEXANDRE NETTO
Reitor
