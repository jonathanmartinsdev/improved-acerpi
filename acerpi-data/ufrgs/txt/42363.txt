Documento gerado sob autenticação Nº RIC.728.900.EUJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7153                  de  04/08/2017
Nomeação  da  Representação  Discente  do
Diretório Acadêmico dos Estudantes de Física
da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita para compor o Diretório Acadêmico dos Estudantes de
Física, com mandato de 01 (um) ano, a contar de 30 de maio de 2017, atendendo ao disposto nos artigos 175
do  Regimento  Geral  da  Universidade  e  79  do  Estatuto  da  Universidade  e  considerando  o  processo
nº 23078.202080/2017-94, conforme segue:
Presidente: Gustavo Lopes Engler
Vice-Presidente: Rafael Luiz Siqueira
Secretário Geral: Lucas Nunes Lopes
Segundo Secretário: William Dutra Stradolini
Tesoureira: Luiza de Campos Morais Ramos
Colaboradora: Ana Laura Von Ende
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
