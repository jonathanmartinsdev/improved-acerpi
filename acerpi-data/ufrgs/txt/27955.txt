Documento gerado sob autenticação Nº QJV.973.674.5IG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7481                  de  22/09/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ANA KARIN NUNES, matrícula SIAPE n° 2091823, lotada e em exercício no Departamento de
Comunicação da Faculdade de Biblioteconomia e Comunicação, da classe A  de Professor Adjunto A, nível 01,
para a classe A  de Professor Adjunto A, nível 02, referente ao interstício de 19/02/2014 a 18/02/2016, com
vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro
de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.508121/2016-72.
RUI VICENTE OPPERMANN
Vice-Reitor
