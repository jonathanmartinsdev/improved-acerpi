Documento gerado sob autenticação Nº CKY.227.665.5NP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3229                  de  13/04/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta Universidade,  FELIPE DOS SANTOS MARASCHIN,  matrícula  SIAPE n°  1852631,  lotado no
Departamento de Botânica do Instituto de Biociências, para exercer a função de Chefe do Depto de Botânica
do Instituto de Biociências, Código SRH 245, Código FG-1, com vigência a partir da data de publicação no
Diário Oficial da União, pelo período de 02 anos. Processo nº 23078.003945/2017-31.
JANE FRAGA TUTIKIAN
Vice-Reitora.
