Documento gerado sob autenticação Nº SLV.901.531.548, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11659                  de  28/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Nomear, em caráter efetivo, MARINA ANDRADE CAMARA DAYRELL, em virtude de habilitação em
Concurso Público de Provas e Títulos, conforme 93/2017 de 15 de dezembro de 2017, homologado em 18 de
dezembro de 2017, e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei
12.772,  de 28 de dezembro de 2012,  com redação dada pela Lei  12.863,  de 24 de setembro de 2013,
publicada  no  Diário  Oficial  da  União  de  25  de  setembro  de  2013,  para  o  cargo  de  PROFESSOR  DO
MAGISTÉRIO SUPERIOR do Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor
Adjunto A, Nível I,  do Quadro de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação
Exclusiva), junto ao Departamento de Artes Visuais do Instituto de Artes, em vaga decorrente da posse em
outro cargo inacumulável do Prof. Marcelo Dutra Arbo, código nº 918080, ocorrida em 07/07/2016, conforme
Portaria  nº.  6.041  de   15/08/2016,  publicada  no  Diário  Oficial  da  União  de  16/08/2016.  Processo  nº.
23078.506693/2017-06.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
