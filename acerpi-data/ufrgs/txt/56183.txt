Documento gerado sob autenticação Nº IDN.906.827.VDP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4952                  de  10/07/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CARLA SCHWENGBER TEN CATEN,  Professor do Magistério
Superior, lotada no Departamento de Engenharia de Produção e Transportes da Escola de Engenharia e com
exercício na Direção da Escola de Engenharia, com a finalidade de realizar  missão contemplando visitas a
vários  parques,  incubadores,  universidades  e  empresas  inovadoras,  em  Benjing,  China,  no  período
compreendido entre 10/08/2018 e 26/08/2018, incluído trânsito, com ônus UFRGS (diárias e passagens).
Solicitação nº 47561.
RUI VICENTE OPPERMANN
Reitor
