Documento gerado sob autenticação Nº VFY.832.151.M49, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6555                  de  26/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ALEXANDRE SILVA VIRGINIO,  Professor do Magistério do
Ensino Básico, Técnico e Tecnológico, lotado no Colégio de Aplicação e com exercício no Departamento de
Sociologia, com a finalidade de participar do "I Congreso Internacional e III Jornadas de Educación Social y 
Escuela:  Un   Análisis  de  la   Última  Década",  em  Ourense,  Espanha,  no  período  compreendido  entre
28/09/2016 e 03/10/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação
nº 21475.
CARLOS ALEXANDRE NETTO
Reitor
