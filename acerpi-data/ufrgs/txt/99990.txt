Documento gerado sob autenticação Nº ENF.371.584.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9657                  de  23/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  RENATO  VENTURA  BAYAN  HENRIQUES,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Sistemas Elétricos de Automação e Energia
da Escola de Engenharia, com a finalidade de participar do "9th International Conference on Current and
Future Trends of Information and Communication Technologies in Healthcare (ICTH 2019)", em Coimbra,
Portugal,  no  período  compreendido  entre  31/10/2019  e  10/11/2019,  incluído  trânsito,  com  ônus
CAPES/PROCAD. Solicitação nº 87397.
RUI VICENTE OPPERMANN
Reitor
