Documento gerado sob autenticação Nº NOU.101.169.9AN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1103                  de  06/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  01/01/2017,   referente  ao  interstício  de
01/07/2015 a 31/12/2016, para a servidora ANA LUCIA RICHTER DREYER, ocupante do cargo de Arquiteto e
Urbanista - 701004, matrícula SIAPE 2239392,  lotada  na  Superintendência de Infraestrutura, passando do
Nível de Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.201032/2016-06:
Formação Integral de Servidores da UFRGS II CH: 48 (30/07/2015 a 13/10/2016)
PPG Geografia/UFRGS - Processamento de Imagens e Sensoriamento Remoto em Análise Ambiental CH: 90
Carga horária utilizada: 72 hora(s) / Carga horária excedente: 18 hora(s) (03/08/2015 a 19/12/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
