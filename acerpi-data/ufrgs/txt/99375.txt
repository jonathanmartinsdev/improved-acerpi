Documento gerado sob autenticação Nº VQF.239.417.G88, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9208                  de  11/10/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°51328,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, MARCELO GIGANTE ORTIZ (Siape: 2054543 ),
 para substituir   IVONE CHRISTIMANN MEIRELES (Siape: 0352881 ), Chefe do Setor Acadêmico da Gerência
Administrativa da FCE, Código FG-4, em seu afastamento por motivo de férias, no período de 14/10/2019 a
23/10/2019, com o decorrente pagamento das vantagens por 10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
