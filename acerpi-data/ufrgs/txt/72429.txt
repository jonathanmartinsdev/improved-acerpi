Documento gerado sob autenticação Nº GCO.232.504.96D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9407                  de  21/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de THOMAS GABRIEL ROSAURO CLARKE, Professor do Magistério
Superior, lotado no Departamento de Metalurgia da Escola de Engenharia e com exercício no Laboratório de
Metalurgia  Física,  com a finalidade de realizar  visita  ao Southwest  Research Institute,  em San Antonio,
Estados Unidos, ao Halliburton Technology Center Houston, em Houston, Estados Unidos e ao Halliburton
Technology Center Carrolton, em Carrolton, Estados Unidos, no período compreendido entre 03/12/2018 e
08/12/2018, incluído trânsito, com ônus limitado. Solicitação nº 61250.
RUI VICENTE OPPERMANN
Reitor
