Documento gerado sob autenticação Nº SXG.534.033.KKO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4593                  de  23/06/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MILTON  DE  SOUZA  MENDONCA  JUNIOR,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Ecologia do Instituto de Biociências, com a
finalidade de participar da "Unifying Ecology Across Scales - Gordon Research Conference", em Biddeford,
Maine, Estados Unidos, no período compreendido entre 23/07/2016 e 30/07/2016, incluído trânsito, com
ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 20603.
CARLOS ALEXANDRE NETTO
Reitor
