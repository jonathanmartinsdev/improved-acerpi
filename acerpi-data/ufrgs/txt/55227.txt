Documento gerado sob autenticação Nº FTI.171.994.NOA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4257                  de  11/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de Aline Nunes Da Rosa, Professor do Magistério Superior, lotada
e em exercício no Departamento de Artes Visuais do Instituto de Artes, com a finalidade de realizar entrevista
e participar do "30º Encuentro de la Asociación de Profesores de Expresión y Comunicación visual y el 3º
Congreso de la Red Iberoamericana de Educación Artística", em Coimbra, Portugal, no período compreendido
entre 24/06/2018 e 03/07/2018, incluído trânsito, com ônus limitado. Solicitação nº 46273.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
