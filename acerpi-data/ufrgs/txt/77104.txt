Documento gerado sob autenticação Nº LVH.331.822.MG9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1711                  de  19/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  17/02/2019,   referente  ao  interstício  de
17/08/2017 a 16/02/2019, para a servidora ROXANA FURTADO MOREIRA, ocupante do cargo de Técnico em
Assuntos Educacionais - 701079, matrícula SIAPE 1052679,  lotada  na  Pró-Reitoria de Gestão de Pessoas,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  E  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  E  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.502042/2019-09:
Educamundo - Sociologia da Educação CH: 190 Carga horária utilizada: 180 hora(s) / Carga horária excedente:
10 hora(s) (04/04/2018 a 07/05/2018)
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
