Documento gerado sob autenticação Nº XWC.694.626.6FE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6362                  de  16/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de RICARDO DATHEIN, Professor do Magistério Superior, lotado
no Departamento de Economia e Relações Internacionais  da Faculdade de Ciências Econômicas e com
exercício no Programa de Pós-Graduação em Economia, com a finalidade de participar de seminário junto à
Universidad Autónoma de Madrid e das "XVI Jornadas de Economía Crítica 2018", em León, Espanha, no
período compreendido entre 16/09/2018 e 23/09/2018, incluído trânsito, com ônus limitado. Solicitação nº
47618.
RUI VICENTE OPPERMANN
Reitor
