Documento gerado sob autenticação Nº BAR.849.979.NQA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7745                  de  28/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria n° 7289/2018, de 14/09/2018, que concede Incentivo à Qualificação a ROGER DE
BEM JAEGER, Administrador, com exercício no Departamento de Execução de Projetos e Convênios da Pró-
Reitoria de Planejamento e Administração. Processo nº 23078.505644/2018-29.
 
                Onde se lê:
                "a contar de 30/04/2018",
 
                leia-se:
                "a contar de 08/04/2018".
 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
