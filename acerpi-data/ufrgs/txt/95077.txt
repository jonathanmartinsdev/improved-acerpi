Documento gerado sob autenticação Nº JTC.235.259.BI5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6618                  de  24/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de ANDRÉ GRAHL PEREIRA,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Informática Teórica do Instituto de Informática, com a finalidade
de participar da "28th International Joint Conference on Artificial Intelligence", em Macau, China, no período
compreendido entre 08/08/2019 e 18/08/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa:
diárias) e CNPq (Proc. nº 451702/2019-0). Solicitação nº 85686.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
