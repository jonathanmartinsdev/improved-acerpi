Documento gerado sob autenticação Nº WGA.028.968.3HI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10489                  de  27/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de Desenhista-projetista, do Quadro
de Pessoal desta Universidade, FATIMA SEQUEIRA ROMANO (Siape: 0358773),  para substituir VERA REGINA
DA CUNHA (Siape: 0355713 ), Coordenador do Núcleo de Assuntos Disciplinares da PROGESP, Código FG-1,
em seu afastamento por motivo de férias,  no período de 28/12/2018 a 31/12/2018,  com o decorrente
pagamento das vantagens por 4 dias. Processo SEI Nº 23078.535481/2018-17
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
