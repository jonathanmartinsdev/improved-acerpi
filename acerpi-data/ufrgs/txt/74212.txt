Documento gerado sob autenticação Nº ZOR.085.172.AO7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10565                  de  28/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°47983,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do Quadro de Pessoal  desta Universidade,  EDISON LUIS SILVA DOS SANTOS (Siape:
0357637 ),  para substituir   SOLANGE ROSA SOUZA GUEDES (Siape: 1028719 ), Diretor da Divisão de Lazer do
Departamento de Infraestrutura da PRAE, Código FG-4, em seu afastamento por motivo de férias, no período
de 02/01/2019 a 04/01/2019, com o decorrente pagamento das vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
