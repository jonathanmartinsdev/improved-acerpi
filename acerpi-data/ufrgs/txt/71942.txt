Documento gerado sob autenticação Nº AJW.091.146.NKE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9049                  de  08/11/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Tornar sem efeito a Portaria n° 8953/2018, de 05/11/2018 que retificou a portaria nº 10262/2016, de
26/12/2016,  que  concedeu  promoção  funcional,  por  obtenção  do  título  de  Doutor,  no  Quadro  desta
Universidade, ao Professor JOÃO RICARDO VIEIRA IGANCI, com exercício no Departamento de Botânica do
Instituto de Biociências da classe A de Professor Adjunto A, nível 02, para a classe C de Professor Adjunto,
nível 02, com vigência financeira a partir de 16/12/2013. Processo nº 23078.515721/2016-97.
JANE FRAGA TUTIKIAN
Vice-Reitora.
