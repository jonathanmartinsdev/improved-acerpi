Documento gerado sob autenticação Nº LGX.070.140.42C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7407                  de  15/08/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  09/08/2019,   referente  ao  interstício  de
01/03/2005 a 08/08/2019, para o servidor ALMIRANTE SOARES DA SILVA, ocupante do cargo de Servente de
Obras -  701824,  matrícula SIAPE 0358054,   lotado  no  Instituto de Biociências,  passando do Nível  de
Classificação/Nível de Capacitação A I, para o Nível de Classificação/Nível de Capacitação A II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.521144/2019-15:
ENAP - Sistema Eletrônico de Informações - SEI! USAR CH: 20 (09/08/2019 a 09/08/2019)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
