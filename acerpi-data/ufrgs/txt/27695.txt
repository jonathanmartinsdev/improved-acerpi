Documento gerado sob autenticação Nº LCF.069.362.76I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7322                  de  15/09/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ODACI LUIZ CORRADINI, matrícula SIAPE n° 0351686, lotado e em exercício no Departamento de
Ciência Política do Instituto de Filosofia e Ciências Humanas, da classe D  de Professor Associado, nível 04,
para a classe E  de Professor Titular,  referente ao interstício de 27/06/2013 a 26/06/2015, com vigência
financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Decisão nº 232/2014 - CONSUN. Processo nº 23078.507002/2016-01.
RUI VICENTE OPPERMANN
Vice-Reitor
