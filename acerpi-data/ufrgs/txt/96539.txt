Documento gerado sob autenticação Nº BZT.071.340.42C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7408                  de  15/08/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder ao servidor MAIKEL ROSA DE OLIVEIRA, ocupante do cargo de  Assistente de Laboratório
- 701437, lotado no Instituto de Ciências Básicas da Saúde, SIAPE 2425944, o percentual de 25% (vinte e cinco
por cento) de Incentivo à Qualificação, a contar de 12/08/2019 , tendo em vista a conclusão do curso de
Graduação em Ciências Biológicas - Bacharelado, conforme o Processo nº 23078.521261/2019-89.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
