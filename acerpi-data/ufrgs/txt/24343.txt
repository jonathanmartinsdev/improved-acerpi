Documento gerado sob autenticação Nº LNT.000.725.23A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5083                  de  12/07/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, MARA CRISTINA DE MATOS RODRIGUES, CPF n° 46508546015, matrícula SIAPE
n° 1376066, lotada no Departamento de História do Instituto de Filosofia e Ciências Humanas, para exercer a
função de Coordenadora do PPG em Ensino de História, Código SRH 1452, código FUC, com vigência a partir
de 12/08/2016 e até 11/08/2018. Processo nº 23078.202137/2016-74.
RUI VICENTE OPPERMANN
Vice-Reitor
