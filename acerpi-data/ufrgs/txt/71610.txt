Documento gerado sob autenticação Nº AXV.465.339.ETC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8804                  de  31/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora MONIQUE CABRAL HAHN,
ocupante do cargo de Técnico de Laboratório Área-701244, lotada na Faculdade de Medicina, SIAPE 1677880,
para 52% (cinquenta e dois por cento), a contar de 06/07/2018, tendo em vista a conclusão do curso de
Mestrado em Saúde da Criança e do Adolescente, conforme o Processo nº 23078.516852/2018-53.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
