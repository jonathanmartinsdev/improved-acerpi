Documento gerado sob autenticação Nº ZQT.045.297.SBO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4938                  de  10/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder prorrogação de afastamento parcial,  no período de 01/08/2018 a 03/04/2019, para a
servidora JOANA BAPTISTA ROCHA, ocupante do cargo de Técnico de Laboratório Área - 701244, matrícula
SIAPE 1925787,  lotada  no  Instituto de Biociências, para cursar o Mestrado em Botânica, oferecido pela
UFRGS; conforme o Processo nº 23078.516851/2018-17.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
