Documento gerado sob autenticação Nº ELR.967.795.OHD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6941                  de  01/08/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  ÂNGELO  LUIS  STAPASSOLI  PIATO,  matrícula  SIAPE  n°  2124864,  lotado  e  em  exercício  no
Departamento de Farmacologia do Instituto de Ciências Básicas da Saúde, da classe C  de Professor Adjunto,
nível 01, para a classe C  de Professor Adjunto, nível 02, referente ao interstício de 21/06/2017 a 20/06/2019,
com vigência financeira a partir de 21/06/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro
de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.515042/2019-61.
JANE FRAGA TUTIKIAN
Vice-Reitora.
