Documento gerado sob autenticação Nº WDL.629.878.99S, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             154                  de  04/01/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  ANA  CRISTINA  GARCIA  DIAS,  matrícula  SIAPE  n°  2533348,  lotada  e  em  exercício  no
Departamento de Psicologia do Desenvolvimento e da Personalidade do Instituto de Psicologia, da classe D 
de Professor Associado, nível 01, para a classe D  de Professor Associado, nível 02, referente ao interstício de
23/05/2014 a 03/07/2016, com vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei
nº 12.772, de 28 de dezembro de 2012, com suas alterações e a Decisão nº 197/2006-CONSUN, alterada pela
Decisão nº 401/2013-CONSUN. Processo nº 23078.510075/2017-52.
JANE FRAGA TUTIKIAN
Vice-Reitora.
