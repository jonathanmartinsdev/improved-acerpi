Documento gerado sob autenticação Nº LRN.768.006.66A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9565                  de  21/10/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.519264/2019-52  e
23078.501988/2018-69, da Lei nº 10.520/02, do Pregão Eletrônico nº 066/2018 e ainda, da Lei nº 8.666/93,
            RESOLVE:
 
            Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 21.8.1 do Edital de Licitação, à
Empresa ALEXSANDER QUINTINO RAMOS, CNPJ nº 04.637.889/0001-73, pelo não comparecimento para
realização dos serviços, conforme atestado pela fiscalização (Doc. SEI nº 1696481, 1730781 e 1746297), bem
como pelo DICON/NUDECON (Doc. SEI nº 1832775) do processo administrativo 23078.519264/2019-52.
 
             Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
