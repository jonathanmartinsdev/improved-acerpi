Documento gerado sob autenticação Nº EDD.970.898.28G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1935                  de  02/03/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a ELIS REJANE BORGES DA
ROSA, matrícula SIAPE nº 0356035, no cargo de Assistente em Administração, nível de classificação D, nível de
capacitação IV,  padrão 16,  do Quadro desta  Universidade,  no regime de quarenta  horas  semanais  de
trabalho,  com exercício  na  Divisão  da  Vida  Acadêmica  do  Departamento  de  Consultoria  em Registros
Discentes da Pró-Reitoria de Graduação, com proventos integrais. Processo 23078.025249/2016-03.
RUI VICENTE OPPERMANN
Reitor.
