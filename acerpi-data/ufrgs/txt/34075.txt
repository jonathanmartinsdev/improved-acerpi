Documento gerado sob autenticação Nº LQE.187.676.R5R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1256                  de  09/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
              Tornar sem efeito a Portaria nº 1118 de 06 de fevereiro de 2017 que delegou competência ao
Professor CARLOS HENRIQUE VASCONCELLOS HORN, Diretor da Faculdade de Ciências Econômicas para
celebrar contratos previstos no Art. 1º, incido II, alínea f, e inciso III, alínea g da Portaria nº 2679, de 27 de
maio de 2011.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
