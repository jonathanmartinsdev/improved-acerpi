Documento gerado sob autenticação Nº YVV.472.300.F13, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7964                  de  05/10/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5041425-80.2018.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, do servidor JOSE FRANCISCO MACHADO DA ROSA, matrícula SIAPE n° 0357890, ativo no cargo
de  Apontador - 701602, para o nível III, conforme o Processo nº 23078.524607/2018-10.
Tornar  sem  efeito  as  portarias  de  concessão  de  progressão  por  capacitação  geradas  após  a
implementação do PCCTAE.
Conceder  progressão  por  capacitação,  a  contar  de  27/09/2007,  passando  do  Nível  de
Classificação/Nível  de  Capacitação  B  III,  para  o  Nível  de  Classificação/Nível  de  Capacitação  B  IV.
RUI VICENTE OPPERMANN
Reitor
