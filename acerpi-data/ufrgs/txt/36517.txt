Documento gerado sob autenticação Nº TJL.323.836.BHG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3063                  de  07/04/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Alterar o exercício, a partir de 3 de abril de 2017, de ROBERTA FERNANDES FAJER, matrícula SIAPE nº
0357435, ocupante do cargo de Assistente em Administração, da Divisão de Concursos Públicos para o
Museu  da  UFRGS,  com  lotação  na  Pró-Reitoria  de  Gestão  de  Pessoas,  conforme  o  Processo  n°
23078.004827/2017-41.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
