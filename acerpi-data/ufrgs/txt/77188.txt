Documento gerado sob autenticação Nº JLT.420.644.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1777                  de  22/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Autorizar a prorrogação de afastamento no País de MARIS CAROLINE GOSMANN, Professor do
Magistério Superior, lotada e com exercício no Departamento de Ciências Contábeis e Atuariais da Faculdade
de  Ciências  Econômicas,  com  a  finalidade  de  continuar  os  estudos  em  nível  de  Doutorado  junto  à
Universidade Federal do Rio de Janeiro, Rio de Janeiro/RJ, no período compreendido entre 23/04/2019 e
22/04/2020, com ônus limitado. Processo nº 23078.523816/2017-65.
RUI VICENTE OPPERMANN
Reitor.
