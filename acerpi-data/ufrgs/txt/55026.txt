Documento gerado sob autenticação Nº HEB.458.109.Q9A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4117                  de  06/06/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, 
RESOLVE
Designar  ANA PAULA GUEDES FRAZZON,  matrícula  SIAPE n°  1510044,  ocupante do cargo de
Professor do Magistério Superior, classe Associado, lotada no Departamento de Microbiologia, Imunologia e
Parasitologia do Instituto de Ciências Básicas da Saúde, do Quadro de Pessoal da Universidade Federal do Rio
Grande do Sul, como Coordenadora do PPG em Microbiologia Agrícola e do Ambiente, Código SRH 1130,
código FUC, com vigência a partir da publicação no Diário Oficial da União até 20/05/2019, a fim de completar
o mandato da Professora MARISA DA COSTA, conforme artigo 92 do Estatuto da mesma Universidade.
Processo nº 23078.512315/2018-34.
JANE FRAGA TUTIKIAN
Vice-Reitora.
