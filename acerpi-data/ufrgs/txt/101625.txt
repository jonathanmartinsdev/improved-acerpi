Documento gerado sob autenticação Nº UMJ.963.847.T2H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10740                  de  02/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°59190,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ANALISTA DE TECNOLOGIA DA
INFORMAÇÃO, do Quadro de Pessoal desta Universidade, ALCIDES VAZ DA SILVA (Siape: 1676200 ),  para
substituir   MARCIA CARLOTTO (Siape: 0356945 ), Diretor da Central de Serviços de Tecnologia da Informação,
vinculada ao CPD, Código CD-4, em seu afastamento por motivo de Laudo Médico do titular da Função, no
período de 02/11/2019 a 16/11/2019, com o decorrente pagamento das vantagens por 15 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
