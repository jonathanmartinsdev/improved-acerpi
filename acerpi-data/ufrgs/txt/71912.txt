Documento gerado sob autenticação Nº KAN.442.842.QQO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9024                  de  07/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANELISE GRACIELE RAMBO, Professor do Magistério Superior,
lotada e  em exercício  no Departamento Interdisciplinar  do Campus Litoral  Norte,  com a finalidade de
participar do   "X Congreso de la Asociación Latinoamericana de Sociología Rural",  em Montevidéu, Uruguai,
no período compreendido entre 26/11/2018 e 01/12/2018, incluído trânsito, com ônus UFRGS (Pró-Reitoria
de Pesquisa - diárias). Solicitação nº 47523.
RUI VICENTE OPPERMANN
Reitor
