Documento gerado sob autenticação Nº ASF.312.340.97E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4001                  de  30/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de PEDRO LUIS GRANDE, Professor do Magistério Superior, lotado
e em exercício no Departamento de Física do Instituto de Física, com a finalidade de participar do "9th
International Workshop on High-Resolution Depth Profiling", em Uppsala, Suécia e do "10th International
Symposium on Swift Heavy Ions Matter & 28th International Conference on Atomic Collisions in Solids", em
Caen, França, no período compreendido entre 23/06/2018 e 07/07/2018, incluído trânsito, com ônus limitado.
Solicitação nº 46655.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
