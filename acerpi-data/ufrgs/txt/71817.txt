Documento gerado sob autenticação Nº EST.593.867.2SI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9114                  de  09/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 30 (trinta) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de 11
de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
DENISE ANSCHAU RODRIGUES MORS, com exercício na Gerência Administrativa da Superintendência de
Infraestrutura,  a  ser  usufruída  no  período  de  17/11/2018  a  16/12/2018,  referente  ao  quinquênio
de 28/06/2012 a 27/06/2017, a fim de finalizar a dissertação do Curso de Mestrado em Memória Social e Bens
Culturais, oferecido pela Universidade La Salle; conforme Processo nº 23078.523023/2017-46.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
