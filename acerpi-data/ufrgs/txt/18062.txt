Documento gerado sob autenticação Nº JEV.457.877.VQF, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1159                  de  17/02/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE:
Designar JULIANA RIBEIRO AZEVEDO, CPF nº 1018381074, Matrícula SIAPE 1600590, ocupante do
cargo de Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para
exercer a função de Coordenadora do Núcleo de Processos Judiciais da Pró-Reitoria de Graduação, Código
SRH 271, Código FG-1, com vigência a partir da data de publicação no Diário Oficial da União. Processo nº
23078.027247/12-73.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
