Documento gerado sob autenticação Nº DSW.388.445.N2J, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3219                  de  03/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  do  Magistério  do  Ensino  Básico,  Técnico  e  Tecnológico  KATIUCI  PAVEI,  matrícula  SIAPE  n°
3476030, com lotação no Colégio de Aplicação, da classe D  III, nível 03, para a classe D  III, nível 04, referente
ao interstício de 01/03/2014 a 29/02/2016, com vigência financeira a partir de 27/04/2016, de acordo com o
que dispõe a Lei nº 12.772, de 28 de dezembro de 2012 e Decisão nº 328/2015 - CONSUN. Processo nº
23078.008711/2016-08.
RUI VICENTE OPPERMANN
Vice-Reitor
