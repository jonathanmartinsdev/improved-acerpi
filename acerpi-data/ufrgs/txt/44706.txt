Documento gerado sob autenticação Nº XLM.395.133.O05, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8868                  de  22/09/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Lotar no Departamento de Odontologia Conservadora, a partir de 12/09/2017, PATRÍCIA DANIELA
MELCHIORS ANGST, matrícula SIAPE n° 1021801, ocupante do cargo de Professor do Magistério Superior,
classe Adjunto A,  redistribuída conforme Portaria  nº  1.770 de 25 de agosto de 2017,  do Ministério da
Educação, publicada no Diário Oficial da União de 28 de agosto de 2017. Processo nº 23110.004627/2017-44.
RUI VICENTE OPPERMANN
Reitor.
