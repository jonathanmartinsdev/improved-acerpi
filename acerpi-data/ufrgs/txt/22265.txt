Documento gerado sob autenticação Nº BLR.089.974.5GM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3637                  de  16/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°26940,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  BIBLIOTECÁRIO-
DOCUMENTALISTA,  do Quadro de Pessoal  desta Universidade,  LILIAN MACIEL (Siape:  1678026 ),   para
substituir   ELIANE MARIA SEVERO GONCALVES (Siape: 0356483 ), Chefe da Biblioteca de Ciências Econômicas
da FCE, Código FG-5, em seu afastamento por motivo de férias, no período de 09/05/2016 a 18/05/2016, com
o decorrente pagamento das vantagens por 10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
