Documento gerado sob autenticação Nº ACD.958.335.1GN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11424                  de  22/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Nomear, em caráter efetivo, MARCELLO PANIZ GIACOMONI, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 73/2017 de 12 de Dezembro de 2017, homologado em 14 de
dezembro de 2017 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei
12.772,  de 28 de dezembro de 2012,  com redação dada pela Lei  12.863,  de 24 de setembro de 2013,
publicada  no  Diário  Oficial  da  União  de  25  de  setembro  de  2013,  para  o  cargo  de  PROFESSOR  DO
MAGISTÉRIO DO ENSINO BÁSICO, TÉCNICO E TECNOLÓGICO do Plano de Carreiras e Cargos do Magistério
Federal, na Classe "A" de Professor D, Nível I,  do Quadro de Pessoal desta Universidade, em regime de
trabalho de DE (Dedicação Exclusiva), junto ao Departamento de Humanidades do Colégio de Aplicação, em
vaga decorrente da aposentadoria da Professora Rose Maria de Oliveira Paim, código nº 274226, ocorrida em
22 de Março de 2016, conforme Portaria nº. 2108/2016 de 18 de março de 2016, publicada no Diário Oficial
da União de 22 de março de 2016. Processo nº. 23078.509131/2016-25.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
