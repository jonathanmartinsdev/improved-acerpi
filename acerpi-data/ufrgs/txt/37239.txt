Documento gerado sob autenticação Nº EZG.162.447.ONO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3426                  de  25/04/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o afastamento do país  de MARA ABEL,  Professor do Magistério Superior,  lotada no
Departamento de Informática Aplicada do Instituto de Informática e com exercício na Comissão de Pesquisa
de  Informática,  com  a  finalidade  de  participar  da  "21st  International  Conference  on  Petroleum  Data
Integration, Information and Data Management", em Houson, Estados Unidos, no período compreendido
entre 14/05/2017 e 19/05/2017, incluído trânsito, com ônus limitado. Solicitação nº 27453.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
