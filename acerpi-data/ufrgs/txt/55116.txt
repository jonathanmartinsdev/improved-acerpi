Documento gerado sob autenticação Nº KCO.126.157.VDP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4198                  de  08/06/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  CLARISSA  CAVALCANTI  FATTURI  PAROLO,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Odontologia Preventiva e Social da Faculdade
de Odontologia, com a finalidade de participar do "65th Congress of the European Organisation for Caries
Research", em Copenhague, Dinamarca, no período compreendido entre 02/07/2018 e 07/07/2018, incluído
trânsito, com ônus limitado. Solicitação nº 45621.
RUI VICENTE OPPERMANN
Reitor
