Documento gerado sob autenticação Nº PJT.825.085.MAF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5857                  de  08/08/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal desta Universidade, AGNES OLSCHOWSKY, matrícula SIAPE n° 6356803, lotada no Departamento de
Assistência e Orientação Profissional da Escola de Enfermagem, como Coordenadora Substituta da Comissão
de Pesquisa da Escola de Enfermagem, para substituir automaticamente o titular desta função em seus
afastamentos ou impedimentos regulamentares no período de 28/08/2016 e até 27/08/2018. Processo nº
23078.014608/2016-99.
RUI VICENTE OPPERMANN
Vice-Reitor
