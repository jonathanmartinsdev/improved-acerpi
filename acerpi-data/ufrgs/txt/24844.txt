Documento gerado sob autenticação Nº EPE.107.126.UON, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5439                  de  20/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor RODRIGO SCHRAMM, matrícula SIAPE n° 1722182, lotado e em exercício no Departamento de
Música do Instituto de Artes, da classe C  de Professor Adjunto, nível 01, para a classe C  de Professor
Adjunto, nível 02, referente ao interstício de 10/08/2013 a 09/08/2015, com vigência financeira a partir de
19/07/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-
CONSUN. Processo nº 23078.501934/2016-31.
CARLOS ALEXANDRE NETTO
Reitor
