Documento gerado sob autenticação Nº UFD.302.204.5FI, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2127                  de  21/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE:
Conceder  Progressão  por  Capacitação,  a  contar  de  12/03/2016,   referente  ao  interstício  de
12/09/2014 a 11/03/2016, para o servidor FELIPE RAFAEL SECCO DA SILVA, ocupante do cargo de Técnico
de Laboratório Área - 701244, matrícula SIAPE 1860381,  lotado  no  Instituto de Pesquisas Hidráulicas,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.004881/2016-13:
ESCOLAS E FACULDADES QI - Curso de Inglês QI Fly - Nível 3 Avançado CH: 12 (08/02/2014 a 05/07/2014)
ANA - Monitoramento da Qualidade da Água de Rios e Reservatórios CH: 40 (19/01/2016 a 16/02/2016)
ANA - Água na Medida Certa CH: 20 (29/09/2015 a 11/10/2015)
ANA - Lei das Águas CH: 20 (09/11/2015 a 22/11/2015)
ANA - Planejamento, Manejo e Gestão de Bacias CH: 40 Carga horária utilizada: 18 hora(s) / Carga horária
excedente: 22 hora(s) (19/01/2016 a 16/02/2016)
ANA - Caminho das Águas CH: 40 (29/09/2015 a 25/10/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
