Documento gerado sob autenticação Nº PTN.517.998.8TM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3181                  de  11/04/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar  a Portaria n° 2667/2019,  de 26/03/2019,  que concedeu Progressão por Capacitação a
servidora NICOLE ANDRESA BERTOTTI,  Técnico em Assuntos Educacionais, com exercício no Divisão de
Atendimento ao Aluno do Campus Litoral Norte, conforme processo nº 23078.501976/2019-15.
 
Onde se lê:
a contar de 28/01/2019, referente ao interstício de 22/09/2017 a 27/01/2019,
leia-se:
a contar de 22/03/2019, referente ao interstício de 22/09/2017 a 21/03/2019.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
