Documento gerado sob autenticação Nº RBT.716.168.85H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6822                  de  29/07/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CLAUDIO RADTKE, Professor do Magistério Superior, lotado e
em exercício no Departamento de Físico-Química do Instituto de Química, com a finalidade de realizar visita à
University of Manchester, em Harwell, Inglaterra, no período compreendido entre 17/08/2019 e 24/08/2019,
incluído trânsito, com ônus CAPES/PRINT/UFRGS. Solicitação nº 85819.
RUI VICENTE OPPERMANN
Reitor
