Documento gerado sob autenticação Nº TTD.767.507.A6G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6857                  de  30/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Declarar vago, a partir de 23 de julho de 2019, o Cargo de Assistente em Administração, Código
701200, Nível de Classificação D, Nível de Capacitação III, Padrão 04, do Quadro de Pessoal, em decorrência
de posse em outro cargo inacumulável, de GABRIELA PEREIRA LOPES com lotação no Gabinete do Reitor.
Processo nº 23078.518906/2019-04.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
