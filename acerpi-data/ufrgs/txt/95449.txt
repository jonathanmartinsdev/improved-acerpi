Documento gerado sob autenticação Nº FIF.269.935.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6921                  de  31/07/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LILIAM RAMOS DA SILVA, Professor do Magistério Superior,
lotada e em exercício no Departamento de Línguas Modernas do Instituto de Letras, com a finalidade de
participar das "VI Jornadas de Estudios Afrolatinoamericanos (GEALA 2019)", em Buenos Aires, Argentina, no
período compreendido entre 01/09/2019 e 06/09/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa: diárias). Solicitação nº 85277.
RUI VICENTE OPPERMANN
Reitor
