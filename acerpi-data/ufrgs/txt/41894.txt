Documento gerado sob autenticação Nº EKR.147.396.88P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6837                  de  28/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  ao  servidor  MILTON  HUMBERTO  SCHANES  DOS  SANTOS,  ocupante  do  cargo  de  
Médico-área - 701047, lotado na Pró-Reitoria de Gestão de Pessoas, SIAPE 1580258, o percentual de 52%
(cinquenta e dois por cento) de Incentivo à Qualificação, a contar de 10/07/2017, tendo em vista a conclusão
do curso de Mestrado em Ensino na Saúde, conforme o Processo nº 23078.013253/2017-00.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
