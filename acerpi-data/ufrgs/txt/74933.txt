Documento gerado sob autenticação Nº IPO.439.839.9JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             391                  de  11/01/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, com vigência a partir de 04/02/2019, como segue:
 
Transformar: Assessor do Depto de Infraestrutura da PRAE, código SRH 966, código FG-4, em Chefe
da Seção de  Infraestrutura da Divisão de Moradia Estudantil do Depto de Infraestrutura da PRAE, código SRH
966, código FG-4.
 
 
Processo nº 23078.534865/2018-12.
 
RUI VICENTE OPPERMANN
Reitor.
