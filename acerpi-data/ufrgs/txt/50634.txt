Documento gerado sob autenticação Nº AMC.421.272.E6J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1215                  de  07/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar  a  Portaria  n°  11182/2017,  de  13/12/2017,  publicada  no  Diário  Oficial  da  União  de
15/12/2017, que designou para exercer a função de Gerente Administrativo do IFCH, Código SRH 1430,
Código FG-1, FABIANO PORTO ROSA,  Mestre de Edificações e Infraestrutura, com exercício na Gerência
Administrativa do Instituto de Filosofia e Ciências Humanas. Processo nº 23078.523465/2017-92.
 
Onde se lê:
"... a partir da data de publicação no Diário Oficial da União,..."
leia-se:
"... a partir de 01/01/2018...", ficando ratificados os demais termos.
RUI VICENTE OPPERMANN
Reitor.
