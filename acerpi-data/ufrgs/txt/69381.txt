Documento gerado sob autenticação Nº MSQ.927.489.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7132                  de  10/09/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Declarar que a aposentadoria concedida a ANA MARIA BAGINSKI PORCELLO,  matrícula SIAPE
351090, através da portaria nº 5354, de 25 de outubro de 2010, publicada no Diário Oficial da União do dia 27
subsequente,  passa  a  ser  no  cargo  de   Bibliotecário-documentalista,  nível  de  classificação  E,  nível  de
capacitação  IV,  padrão  16,  conforme  determinação  judicial  contida  no  processo  nº  5044825-
05.2018.4.04.7100.  Processo  nº  23078.521540/2018-61.
 
 
 
RUI VICENTE OPPERMANN
Reitor.
