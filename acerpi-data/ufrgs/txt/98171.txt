Documento gerado sob autenticação Nº NWY.731.851.RK9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8417                  de  16/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de JAIRTON DUPONT, Professor do Magistério Superior, lotado no
Departamento de Química Orgânica do Instituto de Química e com exercício no Instituto de Química, com a
finalidade de participar de reunião junto ao New Journal of Chemistry, em Cambridge, Inglaterra; e realizar
visita  à  Universidade  de  Murcia,  em  Murcia,  Espanha,  no  período  compreendido  entre  24/09/2019  e
29/09/2019, incluído trânsito, com ônus limitado. Solicitação nº 85681.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
