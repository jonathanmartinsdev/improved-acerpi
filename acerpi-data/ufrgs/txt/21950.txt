Documento gerado sob autenticação Nº UHE.371.141.05C, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3425                  de  10/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme o Laudo Médico n°38543,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PSICÓLOGO-ÁREA, do Quadro de
Pessoal desta Universidade, CAMILA MAGGI RECH NOGUEZ (Siape: 1917493 ),  para substituir   GISLAINE
THOMPSON DOS SANTOS (Siape: 1683368 ), Diretor da Divisão de Promoção da Saúde do Departamento de
Atenção à Saúde da PROGESP, Código FG-1, em seu afastamento por motivo de Laudo Médico do titular da
Função, no período de 14/03/2016 a 18/03/2016, com o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
