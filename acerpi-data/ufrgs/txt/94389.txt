Documento gerado sob autenticação Nº HAA.632.992.J5I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6105                  de  18/07/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016  e  tendo  em  vista  o  que  consta  do(s)  Processo(s)  Administrativo(s)  n°  23078.521882/2018-81  e
23078.504259/2017-83, da Lei nº 10.520/02, do Contrato nº 084/2017 e ainda, da Lei nº 8.666/93,
            RESOLVE:
 
            Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 01 da cláusula décima segunda do
contrato,  à  Empresa  COOK  EMPREENDIMENTOS  EM  ALIMENTAÇÃO  COLETIVA  LTDA,  CNPJ  nº
16.654.626/0001-51, pela inadequação dos processos de identificação dos produtos estocados e perecíveis,
conforme atestado pela fiscalização (Doc. SEI nº 1164308 e 1603169), bem como pelo NUDECON (Doc. SEI nº
1687407) do processo administrativo 23078.521882/2018-81.
 
             Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
