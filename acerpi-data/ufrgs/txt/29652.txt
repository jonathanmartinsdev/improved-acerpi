Documento gerado sob autenticação Nº URZ.058.269.K6H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8542                  de  24/10/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 20 de outubro de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
SILVESTRE NOVAK,  ocupante  do  cargo  de  Técnico  em Assuntos  Educacionais,  Ambiente  Organizacional
Ciências Humanas, Jurídicas e Econômicas, Código 701079, Classe E, Nível de Capacitação IV, Padrão de
Vencimento 16,  SIAPE nº.  0355360 da Secretaria de Educação a Distância para a lotação Faculdade de
Educação, com novo exercício no Núcleo de Apoio Acadêmico da Gerência Administrativa da Faculdade de
Educação.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
