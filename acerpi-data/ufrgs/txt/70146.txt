Documento gerado sob autenticação Nº ISS.316.266.CJG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7684                  de  25/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ELENICE CHEIS DOS SANTOS, Assistente Social, lotada na Pró-
Reitoria de Assuntos Estudantis e com exercício na Divisão de Seleção e Acompanhamento Pedagógico e
Social, com a finalidade de participar da "8º Conferencia Latinoamericana y Caribeña de Ciencias Sociales",
em Buenos Aires, Argentina, no período compreendido entre 19/11/2018 e 23/11/2018, incluído trânsito, com
ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 59235.
RUI VICENTE OPPERMANN
Reitor
