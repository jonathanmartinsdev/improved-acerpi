Documento gerado sob autenticação Nº EPH.073.574.IBH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3409                  de  11/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor WILMAR ENS,  ocupante do cargo de  Técnico em Agropecuária - 701214,
lotado na Estação Experimental Agronômica, SIAPE 1108731, o percentual de 25% (vinte e cinco por cento) de
Incentivo à Qualificação, a contar de 22/03/2018, tendo em vista a conclusão do curso de Graduação em
História - Licenciatura, conforme o Processo nº 23078.501914/2018-22.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
