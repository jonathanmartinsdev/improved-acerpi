Documento gerado sob autenticação Nº BNU.710.607.CV6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9814                  de  24/10/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  22/10/2017,   referente  ao  interstício  de
22/04/2016 a 21/10/2017, para o servidor LOUIDI LAUER ALBORNOZ, ocupante do cargo de Técnico de
Laboratório Área -  701244,  matrícula  SIAPE 2027001,   lotado  no  Instituto de Pesquisas  Hidráulicas,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.517739/2017-12:
Formação Integral de Servidores da UFRGS IV CH: 116 Carga horária utilizada: 98 hora(s) / Carga horária
excedente: 18 hora(s) (02/09/2014 a 20/07/2017)
NELE - Curso de Italiano 1 CH: 52 (27/08/2016 a 26/11/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
