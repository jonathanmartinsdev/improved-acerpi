Documento gerado sob autenticação Nº KLI.920.609.F6G, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8168                  de  11/10/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  HUDSON  DA  SILVA  TORRENT,  matrícula  SIAPE  n°  3544285,  lotado  e  em  exercício  no
Departamento de Estatística do Instituto de Matemática e Estatística, da classe C  de Professor Adjunto, nível
03, para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 01/07/2014 a 30/06/2016, com
vigência financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro
de 2012, com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.510152/2016-93.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
