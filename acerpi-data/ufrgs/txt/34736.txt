Documento gerado sob autenticação Nº WCA.568.408.PSM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1791                  de  23/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  06/02/2017,   referente  ao  interstício  de
28/07/2015 a 05/02/2017,  para o servidor  MAURO ALMEIDA DIAS DE CASTRO,  ocupante do cargo de
Analista  de  Tecnologia  da  Informação  -  701062,  matrícula  SIAPE  0354320,   lotado   no   Centro  de
Processamento de Dados, passando do Nível de Classificação/Nível de Capacitação E II,  para o Nível de
Classificação/Nível de Capacitação E III,  em virtude de ter realizado o(s) seguinte(s) curso(s),  conforme o
Processo nº 23078.001775/2017-51:
RNP - ADR001 Arquitetura e protocolos de rede - TCP/IP CH: 34 (14/04/2008 a 18/04/2008)
RNP - GTI-003 Gerenciamento de Serviços de TI CH: 24 (28/09/2011 a 30/09/2011)
RNP - GTI-004 Governança de TI CH: 40 (05/12/2011 a 07/12/2011)
RNP - GTI-007 ITIL - Information Technology Infrastructure Library CH: 16 Carga horária utilizada: 12 hora(s) /
Carga horária excedente: 4 hora(s) (25/06/2012 a 26/06/2012)
RNP - Gestão de Riscos de TI - NBR 27005 CH: 40 (28/05/2012 a 28/06/2012)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
