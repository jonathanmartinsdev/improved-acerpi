Documento gerado sob autenticação Nº YPL.207.779.GH1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2773                  de  29/03/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a SALETE BRAGA MEDEIROS,
matrícula  SIAPE  nº  0357054,  no  cargo  de  Nutricionista-habilitação,  nível  de  classificação  E,  nível  de
capacitação IV,  padrão 16,  do Quadro desta  Universidade,  no regime de quarenta  horas  semanais  de
trabalho, com exercício no Restaurante Universitário - 2 (Saúde) da Pró-Reitoria de Assuntos Estudantis, com
proventos integrais. Processo 23078.533646/2018-16.
JANE FRAGA TUTIKIAN
Vice-Reitora.
