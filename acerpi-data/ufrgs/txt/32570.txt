Documento gerado sob autenticação Nº RRL.639.991.UMN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             243                  de  10/01/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas  atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Designar  a  ocupante  do  cargo  de  Professor  do  Magistério  Superior,  classe  Associado,  desta
Universidade,  LUCIANA  PELLIN  MIELNICZUK,  matrícula  SIAPE  n°  1437415,  lotada  no  Departamento  de
Comunicação da Faculdade de Biblioteconomia e Comunicação, para exercer a função de Coordenadora da
Comissão de Pesquisa da FABICO, Código SRH 878, com vigência no período de 19/01/2017 a 18/01/2019,
ficando o pagamento de gratificação condicionado à disponibilidade de uma função gratificada. Processo nº
23078.026039/2016-24.
RUI VICENTE OPPERMANN
Reitor
