Documento gerado sob autenticação Nº DIQ.738.142.5SL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8413                  de  19/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 30/09/2016,  o ocupante do cargo de Professor do Magistério
Superior, classe Associado do Quadro de Pessoal desta Universidade, PASCUAL ISOLDI PINKOSKI, matrícula
SIAPE n° 0354722, da função de  Chefe do Depto de Tecnologia de Alimentos do ICTA, Código SRH 213, código
FG-1, para a qual foi designado pela Portaria 4261/2015, de 08/06/2015, publicada no Diário Oficial da União
do dia 11/06/2015. Processo nº 23078.203704/2016-18.
JANE FRAGA TUTIKIAN
Vice-Reitora
