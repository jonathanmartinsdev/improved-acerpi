Documento gerado sob autenticação Nº GGQ.809.082.AD3, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2475                  de  06/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de ADRIANA SCHMIDT DIAS, Professor do Magistério Superior,
lotada e em exercício no Departamento de História do Instituto de Filosofia e Ciências Humanas, com a
finalidade de participar da VIII Reunión de Teoría Arqueológica de América del Sur", em La Paz, Bolívia, no
período compreendido entre 22/05/2016 e 28/05/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa - diárias). Solicitação nº 18360.
CARLOS ALEXANDRE NETTO
Reitor
