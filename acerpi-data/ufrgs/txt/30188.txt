Documento gerado sob autenticação Nº OIJ.592.272.QU7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9034                  de  09/11/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de JOSE VICENTE TAVARES DOS SANTOS, Professor do Magistério
Superior, lotado no Departamento de Sociologia do Instituto de Filosofia e Ciências Humanas e com exercício
no Instituto Latino-Americano de Estudos Avançados, com a finalidade de participar de seminário junto à
Université de Montréal, Canadá, no período compreendido entre 13/11/2016 e 05/12/2016, incluído trânsito,
com ônus limitado. Solicitação nº 23301.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
