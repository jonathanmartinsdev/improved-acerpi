Documento gerado sob autenticação Nº OPS.018.815.06L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5935                  de  06/07/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar na Pró-Reitoria de Gestão de Pessoas, com exercício no Departamento de Atenção à Saúde,
SILVIA LORENZINI, nomeada conforme Portaria Nº 4550/2017 de 22 de maio de 2017, publicada no Diário
Oficial da União no dia 23 de maio de 2017, em efetivo exercício desde 28 de junho de 2017, ocupante do
cargo de AUXILIAR DE SAÚDE, Ambiente Organizacional Ciências da Saúde, classe C, nível I, padrão 101, no
Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
