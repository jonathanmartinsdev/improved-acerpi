Documento gerado sob autenticação Nº WUH.246.179.01H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7359                  de  16/09/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor PAULO ROBERTO RODRIGUES SOARES,  matrícula SIAPE n° 0409354, lotado e em exercício no
Departamento de Geografia do Instituto de Geociências, da classe D  de Professor Associado, nível 01, para a
classe D  de Professor Associado, nível 02, referente ao interstício de 23/06/2014 a 22/06/2016, com vigência
financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com  suas  alterações  e  a  Decisão  nº  197/2006-CONSUN,  alterada  pela  Decisão  nº  401/2013-CONSUN.
Processo nº 23078.507243/2016-41.
RUI VICENTE OPPERMANN
Vice-Reitor
