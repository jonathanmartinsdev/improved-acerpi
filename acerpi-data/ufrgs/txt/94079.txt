Documento gerado sob autenticação Nº RDG.279.440.9F5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5895                  de  12/07/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta  Universidade,  DANILO BLANK,  matrícula  SIAPE  n°  0351864,  lotado  no  Departamento  de
Pediatria da Faculdade de Medicina, considerando Nota SAJ nº 143/2019, como Coordenador Substituto do
PPG em Ensino  na  Saúde-Mestrado Profissional,  com vigência  a  partir  de  22/07/2019  até  21/07/2021.
Processo nº 23078.517594/2019-11.
RUI VICENTE OPPERMANN
Reitor.
