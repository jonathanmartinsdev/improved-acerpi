Documento gerado sob autenticação Nº QMF.354.499.APH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1410                  de  15/02/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder jornada de trabalho reduzida,  com remuneração proporcional  à  servidora DORIANE
KOHLER SCHEIBLER  ,  matrícula SIAPE n° 1444972, ocupante do cargo de Assistente em Administração -
701200,  lotada  na  Pró-Reitoria  de  Gestão  de  Pessoas  e  com  exercício  na  Gerência  Administrativa  da
PROGESP, alterando a jornada de trabalho de oito horas diárias e quarenta horas semanais para 6 horas
diárias e 30 semanais, no período de 15 de fevereiro de 2018 a 14 de fevereiro de 2019, nos termos dos
artigos 5º a 7º da Medida Provisória nº 2.174-28, de 24 de agosto de 2001, e conforme o Processo nº
23078.501824/2018-31.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
