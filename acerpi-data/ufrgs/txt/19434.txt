Documento gerado sob autenticação Nº USH.884.328.JJA, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2061                  de  18/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°18240,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, TATIANE DA SILVA DAL PIZZOL (Siape: 1562592 ),
 para substituir   DENISE BUENO (Siape: 1412810 ), Coordenador da COMGRAD de Farmácia, Código FUC, em
seu afastamento  no país,  no  período de  21/03/2016 a  26/03/2016,  com o  decorrente  pagamento  das
vantagens por 6 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
