Documento gerado sob autenticação Nº KOK.213.015.SCC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8503                  de  20/10/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Nomear, em caráter efetivo, GEAN PAULO MICHEL, em virtude de habilitação em Concurso Público
de Provas e Títulos, conforme Edital Nº 15/2016 de 8 de Março de 2016, homologado em 09 de março de
2016 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de 28
de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do
Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro
de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento
de  Obras  Hidráulicas  do  Instituto  de  Pesquisas  Hidráulicas,  em vaga  decorrente  da  aposentadoria  do
Professor Lawson Francisco de Souza Beltrame, código nº 273306, ocorrida em 29 de Setembro de 2016,
conforme Portaria nº. 7523 de 23 de setembro de 2016, publicada no Diário Oficial da União de 29 de
setembro de 2016. Processo nº. 23078.201436/2015-19.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
