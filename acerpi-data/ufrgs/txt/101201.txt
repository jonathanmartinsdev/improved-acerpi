Documento gerado sob autenticação Nº UXX.821.407.V2N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10441                  de  20/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  13/11/2019,   referente  ao  interstício  de
06/01/2017 a 12/11/2019,  para a servidora SANDRA JUSSARA JAMES ALMEIDA,  ocupante do cargo de
Assistente em Administração - 701200, matrícula SIAPE 0356374,  lotada  na  Pró-Reitoria de Planejamento
e  Administração,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de
Classificação/Nível de Capacitação D IV, em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.531571/2019-10:
CENED - Licitações e Contratos CH: 100 (09/08/2016 a 08/11/2016)
CENED -  Gestão Na Administração Pública  CH:  180 Carga horária  utilizada:  50  hora(s)  /  Carga horária
excedente: 130 hora(s) (09/10/2019 a 03/11/2019)
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
