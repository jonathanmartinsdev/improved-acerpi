Documento gerado sob autenticação Nº BTV.059.258.9JC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2898                  de  19/04/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar JEAN SEGATA, matrícula SIAPE n° 1097478, ocupante do cargo de Professor do Magistério
Superior, classe Adjunto A, lotado no Departamento de Antropologia do Instituto de Filosofia e Ciências
Humanas, do Quadro de Pessoal da Universidade, para exercer a função de Coordenador Substituto do PPG
em Antropologia Social,  com vigência a partir  da data deste ato até 25/01/2019, a fim de completar o
mandato  do Professor SERGIO BAPTISTA DA SILVA, conforme artigo 92 do Estatuto da mesma Universidade,
sem prejuízo e cumulativamente com a função de Coordenador da COMGRAD de Ciências Sociais, código
SRH 1180, código FUC, até 10/03/2019. Processo nº 23078.506847/2018-32.
RUI VICENTE OPPERMANN
Reitor.
