Documento gerado sob autenticação Nº FBW.680.731.0F9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8290                  de  16/10/2018
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de ANGELA DE MOURA FERREIRA DANILEVICZ,  Professor do
Magistério Superior, lotada e em exercício no Departamento de Engenharia de Produção e Transportes da
Escola de Engenharia, com a finalidade de participar de reunião junto à Universidad Nacional de Córdoba, em
Córdoba, Argentina, no período compreendido entre 21/10/2018 e 26/10/2018, incluído trânsito, com ônus
UFRGS. Solicitação nº 60252.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
