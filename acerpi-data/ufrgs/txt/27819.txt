Documento gerado sob autenticação Nº JTA.848.270.J77, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7416                  de  19/09/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SERGIO LUIS CECHIN, Professor do Magistério Superior, lotado
no Departamento de Informática Aplicada do Instituto de Informática e com exercício na Comissão de
Graduação de Ciência da Computação, com a finalidade de participar do "Latin-American Symposium on
Dependable Computing",  em Cali,  Colômbia,  no período compreendido entre 18/10/2016 e 21/10/2016,
incluído trânsito, com ônus limitado. Solicitação nº 23307.
CARLOS ALEXANDRE NETTO
Reitor
