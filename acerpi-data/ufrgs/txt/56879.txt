Documento gerado sob autenticação Nº LXY.151.894.370, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5459                  de  23/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Exonerar, a pedido, a partir de 9 de julho de 2018, nos termos do artigo 34 "caput", da Lei n° 8.112,
de  1990,  KELY ELOIZA PILON BONOTTO,  ocupante  do cargo de  Assistente  em Administração,  código
701200, nível de classificação D, nível de capacitação I, padrão 01, do Quadro de Pessoal, lotada e com
exercício na Faculdade de Arquitetura. Processo nº 23078.515930/2018-01.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
