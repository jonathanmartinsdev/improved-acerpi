Documento gerado sob autenticação Nº NDH.973.019.K9B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             232                  de  05/01/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ELENA SALVATORI, Professor do Magistério Superior, lotada e
em exercício no Departamento de Arquitetura da Faculdade de Arquitetura, com a finalidade de ministrar
cursos junto à Czech Technical University in Prague, em Praga, República Tcheca, no período compreendido
entre 05/02/2018 e 14/02/2018, incluído trânsito, com ônus limitado. Solicitação nº 33318.
RUI VICENTE OPPERMANN
Reitor
