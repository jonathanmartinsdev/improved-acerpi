Documento gerado sob autenticação Nº KRD.128.928.FVH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4219                  de  15/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor AUGUSTO HAUSCHILD PELLEGRIN, ocupante do cargo de  Assistente em
Administração - 701200, lotado na Pró-Reitoria de Assuntos Estudantis, SIAPE 2391382, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 03/05/2017, tendo em vista a conclusão do
curso de Graduação em Ciências Contábeis - Bacharelado, conforme o Processo nº 23078.007617/2017-12.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
