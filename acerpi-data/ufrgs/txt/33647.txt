Documento gerado sob autenticação Nº PPW.146.425.I62, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             920                  de  27/01/2017
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
Reinstaurar a Sindicância Investigativa a ser processada pela Comissão integrada pelas servidoras GENISA
COUTO  DA  SILVA,  Assistente  em  Administração  e  ISADORA  AZUAGA  NIETIEDT,  Assistente  em
Administração,  lotadas  na  Superintendência  de  Infraestrutura,  para  sob  a  presidência  da  primeira,
constituírem Comissão Sindicante Investigativa que, no prazo de 30 (trinta) dias, deverá apurar o pagamento
de indenização à Empresa Bass Elevadores Ltda. no valor de R$ 6.506,93 (seis mil, quinhentos e seis reais e
noventa e três centavos), relativo ao Contrato nº 137/PROPLAN/NUDECON/2013 referente ao processo SEI nº
23078.504916/2016-10.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
