Documento gerado sob autenticação Nº NMZ.201.442.KEU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11438                  de  26/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de Assistente em Administração, do
Quadro de  Pessoal  desta  Universidade,  SILVIA REGINA SANTOS DE MENEZES (Siape:  0355373),   para
substituir MARIA BERENICE LOPES (Siape: 0353931 ), Gerente Administrativo da FABICO, Código SRH 1076,
FG-1, em seu afastamento por férias, no período de 26/12/2017 a 01/01/2018, com o decorrente pagamento
das vantagens por 7 dias. Processo SEI nº 23078.524490/2017-93
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
