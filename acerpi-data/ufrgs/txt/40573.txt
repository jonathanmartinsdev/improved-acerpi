Documento gerado sob autenticação Nº NRO.042.851.LRO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5903                  de  06/07/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  JULIANA  BALBINOT  HILGERT,  Professor  do  Magistério
Superior,  lotada e em exercício no Departamento de Odontologia Preventiva e Social  da Faculdade de
Odontologia, com a finalidade de proferir palestra na  Universidad Científica del Sur, em Lima, Peru, no
período compreendido entre 06/08/2017 e 12/08/2017, incluído trânsito, com ônus limitado. Solicitação nº
28876.
RUI VICENTE OPPERMANN
Reitor
