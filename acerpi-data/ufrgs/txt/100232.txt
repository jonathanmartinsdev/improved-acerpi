Documento gerado sob autenticação Nº YVW.450.128.VDM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9812                  de  30/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARIO REIS ALVARES DA SILVA,  Professor do Magistério
Superior,  lotado no Departamento de Medicina Interna da Faculdade de Medicina e  com exercício  no
Programa de Pós-Graduação Ciências em Gastroenterologia e Hepatologia, com a finalidade de participar
da "AASLD - Liver Meeting 2019", em Boston, Estados Unidos, no período compreendido entre 07/11/2019 e
13/11/2019, incluído trânsito, com ônus limitado. Solicitação nº 87806.
RUI VICENTE OPPERMANN
Reitor
