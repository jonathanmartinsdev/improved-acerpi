Documento gerado sob autenticação Nº NFV.596.371.63A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7063                  de  06/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/08/2019,   referente  ao  interstício  de
05/02/2018 a  04/08/2019,  para  a  servidora  ADRIANA DA SILVA COSTA ALVES,  ocupante  do cargo de
Assistente em Administração - 701200, matrícula SIAPE 2329379,  lotada  na  Faculdade de Agronomia,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.518398/2019-56:
Formação Integral de Servidores da UFRGS V CH: 122 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 2 hora(s) (18/03/2017 a 06/06/2019)
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
