Documento gerado sob autenticação Nº LKT.385.272.AB0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7027                  de  05/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARGARETE SCHLATTER, Professor do Magistério Superior,
lotada e em exercício no Departamento de Línguas Modernas do Instituto de Letras, com a finalidade de
participar da "I Reunião Técnica de Certificação de Competências em Português Língua Estrangeira", em
Lisboa, Portugal,  no período compreendido entre 10/09/2018 e 12/09/2018, incluído trânsito,  com ônus
limitado. Solicitação nº 59092.
RUI VICENTE OPPERMANN
Reitor
