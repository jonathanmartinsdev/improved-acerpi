Documento gerado sob autenticação Nº CSZ.370.072.D12, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             885                  de  26/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, ADRIANA DORFMAN, matrícula SIAPE n° 2099219, lotada no Departamento de
Geografia do Instituto de Geociências, para exercer a função de Coordenadora da COMGRAD de Geografia,
Código  SRH  1225,  código  FUC,  com  vigência  a  partir  de  08/02/2017  e  até  07/02/2019.  Processo  nº
23078.200143/2017-78.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
