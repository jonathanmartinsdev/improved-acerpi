Documento gerado sob autenticação Nº JFV.493.879.UN6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1167                  de  06/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias, e conforme a Solicitação de Afastamento n°34025,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ASSISTENTE SOCIAL, do Quadro
de Pessoal desta Universidade, MARILIA BORGES HACKMANN (Siape: 0756975 ),  para substituir   MAURÍCIO
VIÉGAS DA SILVA (Siape: 0354315 ), Pró-Reitor de Gestão de Pessoas, Código CD-2, em seu afastamento no
país, no período de 07/02/2018 a 08/02/2018, com o decorrente pagamento das vantagens por 2 dias.
RUI VICENTE OPPERMANN
Reitor 
