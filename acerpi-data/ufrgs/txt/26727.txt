Documento gerado sob autenticação Nº ZWI.777.093.TDO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6514                  de  26/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Declarar vago, a partir de 12 de agosto de 2016, o cargo de Técnico em Assuntos Educacionais,
Código 701079,  Nível  de Classificação E,  Nível  de Capacitação I,  Padrão 05,  do Quadro de Pessoal,  em
decorrência de posse em outro cargo inacumulável, de EDISON LUIZ SATURNINO com lotação no Instituto
de Geociências. Processo nº 23078.017959/2016-51.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
