Documento gerado sob autenticação Nº RSJ.821.158.HQN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1769                  de  01/03/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar DIOGO JOEL DEMARCO, matrícula SIAPE n° 1578025, ocupante do cargo de Professor do
Magistério  Superior,  classe Adjunto,  lotado no Departamento de Ciências  Administrativas  da Escola  de
Administração, do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Coordenador da
COMGRAD de Administração, código SRH 1194, código FUC, com vigência a partir da publicação no Diário
Oficial  da União até 19/02/2019, a fim de completar o mandato do Professor Pedro de Almeida Costa,
conforme artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.502168/2018-94.
JANE FRAGA TUTIKIAN
Vice-Reitora.
