Documento gerado sob autenticação Nº GIE.773.491.290, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2918                  de  03/04/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Bibliotecário-documentalista, classe E, do Quadro de Pessoal desta
Universidade, CLAUDIA DA SILVA GONCALVES DE LEON, matrícula SIAPE n° 1204766, lotada na Faculdade de
Farmácia, como Chefe Substituta da Biblioteca de Farmácia, com vigência a partir da data deste ato, para
substituir automaticamente o titular desta função em seus afastamentos ou impedimentos regulamentares.
Processo nº 23078.004085/2017-53.
JANE FRAGA TUTIKIAN
Vice-Reitora.
