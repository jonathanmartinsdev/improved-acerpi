Documento gerado sob autenticação Nº XIV.081.717.OJK, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3355                  de  04/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de IGNACIO ITURRIOZ, Professor do Magistério Superior, lotado e
em exercício no Departamento de Engenharia Mecânica da Escola de Engenharia,  com a finalidade de
participar de reunião na  Universidad de la República, em Montevidéu, Uruguai, no período compreendido
entre 12/05/2016 e 16/05/2016, incluído trânsito, com ônus limitado. Solicitação nº 19675.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
