Documento gerado sob autenticação Nº TOQ.681.555.7SR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2060                  de  01/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor PAULO VINICIUS LIMA DA
SILVA,  ocupante do cargo de Operador de Máquinas Agrícolas-701452,  lotado na Estação Experimental
Agronômica, SIAPE 2203043, para 25% (vinte e cinco por cento), a contar de 22/02/2019, tendo em vista a
conclusão  do  Curso  Superior  de  Tecnologia  em  Gestão  da  Informação,  conforme  o  Processo  nº
23078.534684/2018-88.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
