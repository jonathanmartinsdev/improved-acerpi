Documento gerado sob autenticação Nº UAD.078.302.OA3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1231                  de  02/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°46283,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ANDERSON LUIS RUHOFF (Siape: 1447494 ),  para
substituir    ALFONSO RISSO (Siape: 1187631 ),  Coordenador da Comissão de Extensão do IPH, em seu
afastamento por motivo de férias, no período de 04/02/2019 a 22/02/2019.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
