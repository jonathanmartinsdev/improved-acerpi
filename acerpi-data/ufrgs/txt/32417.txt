Documento gerado sob autenticação Nº MPO.615.836.UMN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             236                  de  10/01/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Retificar  a  Portaria  n°  10339/2016,  de  28/12/2016,  publicada  no  Diário  Oficial  da  União  de
29/12/2016, que dispensou LUCIA MARIA KLIEMANN, Professora do Magistério Superior, da função de Vice-
Diretora da Faculdade de Medicina. Processo nº 23078.025395/2016-21.
 
 
Onde se lê:
- "Dispensar, a pedido, a partir de 19/12/2016, ...",
leia-se:
- "Dispensar, a pedido, a partir de 29/12/2016, ...", ficando ratificados os demais termos.
RUI VICENTE OPPERMANN
Reitor
