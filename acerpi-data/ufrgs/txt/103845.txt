Documento gerado sob autenticação Nº JWG.389.917.KF1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             575                  de  15/01/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar SERGIO ROBERTO KIELING FRANCO, matrícula SIAPE n° 0359301, ocupante do cargo de
Professor do Magistério Superior,  classe Titular,  lotado no Departamento de Estudos Especializados da
Faculdade  de  Educação,  do  Quadro  de  Pessoal  da  Universidade  Federal  do  Rio  Grande  do  Sul,
como Coordenador do PPG em Educação, código SRH 1157, código FUC, com vigência a partir de 23/01/2020
e  até  24/01/2021,  a  fim  de  completar  o  mandato  do  Professor  LUIS  HENRIQUE  SACCHI  DOS
SANTOS, conforme artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.500867/2020-14.
JANE FRAGA TUTIKIAN
Vice-Reitora.
