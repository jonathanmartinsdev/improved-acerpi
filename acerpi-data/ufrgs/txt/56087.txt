Documento gerado sob autenticação Nº XAE.571.622.22A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4856                  de  09/07/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
RESOLVE
Prorrogar por 30 dias a Portaria n° 4123/2018, de 07 de junho de 2018.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
