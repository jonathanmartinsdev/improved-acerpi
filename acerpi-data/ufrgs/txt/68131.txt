Documento gerado sob autenticação Nº NUN.338.083.6FE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6235                  de  14/08/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  PATRICIA  ALEJANDRA  BEHAR,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Estudos Especializados da Faculdade de Educação, com
a finalidade de atuar como Professor Visitante, junto ao Teachers College, Columbia University, em New York,
Estados Unidos da América, no período compreendido entre 13/08/2018 e 14/02/2019, com ônus limitado.
Processo nº 23078.507744/2018-90.
RUI VICENTE OPPERMANN
Reitor.
