Documento gerado sob autenticação Nº DBS.044.814.DG1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4230                  de  09/06/2016
O VICE-SUPERINTENDENTE DE OBRAS DA SUINFRA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 6172, de 26 de outubro de 2012
RESOLVE
Designar os servidores,  HENRIQUE GOMES DOS SANTOS, ocupante do cargo de Engenheiro-área,
lotado  na  Superintendência  de  Infraestrutura  e  com  exercício  no  Planejamento  e  Assessoria  da
Superintendência  de  Infraestrutura,  LUIZ  FABRICIO  MARTINS  DOS  SANTOS,  ocupante  do  cargo  de
Assistente em Administração, lotado na Superintendência de Infraestrutura e com exercício na Prefeitura
Campus Centro (SP1), LUCIA BECKER CARPENA, ocupante do cargo de Professor do Magistério Superior,
lotada no Departamento de Música do Instituto de Artes e com exercício no Instituto de Artes, para sob a
presidência do primeiro, constituirem a Comissão de Recebimento, em atendimento à alínea "b" do inciso I
do artigo 73 da Lei  8666/93,  para emissão do Termo de Recebimento Definitivo da Obra do contrato
173/2013, cujo objeto é "Contatação de empresa para o fornecimento e instalação de 04 elevadores de
passageiros do tipo elétrico, nos Campi Centro e Saúde da UFRGS, com a prestação de assistência
técnica gratuita", com prazo até 28/10/2016 quando se encerra o prazo de vigência do contrato. Processo
nº 23078.040763/13-38.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
SILVIO HENRIQUE BERSAGUI
Vice-Superintendente de Obras da SUINFRA
