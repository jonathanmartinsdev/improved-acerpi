Documento gerado sob autenticação Nº VWT.152.119.RJB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8772                  de  27/10/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005,  publicada no Diário Oficial  da União do dia 6 subsequente,  a  DILMA NASCENTE,
matrícula SIAPE nº  0351955,  no cargo de Bibliotecário-documentalista,  nível  de classificação E,  nível  de
capacitação IV,  padrão 16,  do Quadro desta  Universidade,  no regime de quarenta  horas  semanais  de
trabalho,  com  exercício  na  Biblioteca  do  Departamento  de  Botânica  do  Instituto  de  Biociências,  com
proventos integrais e incorporando a vantagem pessoal de que trata a Lei nº 9.624, de 2 de abril de 1998, que
assegurou o disposto no artigo 3º da Lei nº 8.911, de 11 de julho de 1994. Processo 23078.022114/2016-88.
RUI VICENTE OPPERMANN
Reitor
