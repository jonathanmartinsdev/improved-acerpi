Documento gerado sob autenticação Nº SLS.347.896.1JB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5753                  de  03/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito a Portaria nº 4542/2017, de 22/05/2017, publicada no Diário Oficial da União de
23/05/2017, que nomeou em caráter efetivo, MAGDALINI KNAK e VÂNIA REGINA AMORIM DA SILVA, para
o cargo de Técnico em Assuntos Educacionais, de acordo com o que preceitua o § 6º do artigo 13 da Lei nº.
8.112,  de  11  de  dezembro de  1990,  com a  redação dada pela  Lei  nº.  9.527,  de  10  de  dezembro de
1997. Processo nº 23078.025182/2016-07.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
