Documento gerado sob autenticação Nº SQV.795.566.GLE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5430                  de  28/06/2019
O DIRETOR DO DEPARTAMENTO DE ADMINISTRAÇÃO DE PESSOAL DA PROGESP DA UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183,
de 17 de outubro de 2008
RESOLVE
Conceder, nos termos do artigo 87, combinado com o artigo 100, redação original, da Lei nº 8.112,
de 11 de dezembro de 1990 e do Parecer nº 01/91-ADP, de 26 de abril de 1991, ao servidor HELIO HENKIN,
ocupante do cargo de Professor do Magistério Superior, do Quadro de Pessoal desta Universidade, com
exercício na Pró-Reitoria de Planejamento e Administração, 03 mes(es) de Licença-prêmio por Assiduidade,
referente ao quinquênio compreendido entre 07/01/1986 e 06/01/1991, a ser gozada de acordo com a escala
organizada em conformidade com o artigo 89 do mesmo diploma legal. Processo nº 23078.515827/2019-33.
MARCELO SOARES MACHADO
Diretor do Departamento de Administração de Pessoal da PROGESP
