Documento gerado sob autenticação Nº XUO.362.396.0M0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4291                  de  13/06/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, JULIANA ROMBALDI BERNARDI, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 38/2016 de 26 de Abril de 2016, homologado em 27 de abril
de 2016 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de
28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do
Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro
de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento
de Nutrição da Faculdade de Medicina, em vaga decorrente da aposentadoria da Professora Cileide Cunha
Moulin, código nº 274266, ocorrida em 27 de Julho de 2015, conforme Portaria nº. 5590 de 24 de julho de
2015, publicada no Diário Oficial da União de27 de julho de 2015. Processo nº. 23078.025322/2015-58.
CARLOS ALEXANDRE NETTO
Reitor
