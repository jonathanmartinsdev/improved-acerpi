Documento gerado sob autenticação Nº XGX.466.750.VHE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3970                  de  03/06/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  27/05/2016,   referente  ao  interstício  de
09/07/2014 a 26/05/2016, para o servidor DANIEL AGRA ISERHARD,  ocupante do cargo de Técnico de
Tecnologia da Informação - 701226, matrícula SIAPE 1780163,  lotado  no  Centro de Processamento de
Dados, passando do Nível de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.011296/2016-61:
Formação Integral de Servidores da UFRGS III CH: 87 (09/11/2012 a 09/10/2015)
FUNDAÇÃO BRADESCO - Segurança da Informação CH: 23 (04/05/2016 a 27/05/2016)
RNP - Planejamento e contratação de Serviços de TI CH: 40 (25/04/2016 a 29/04/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
