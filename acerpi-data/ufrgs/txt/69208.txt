Documento gerado sob autenticação Nº IGP.569.450.7ND, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6975                  de  04/09/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MARCIA  CRISTINA  BERNARDES  BARBOSA,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Física do Instituto de Física, com a finalidade
de participar do "XVII Scientific Research Meeting of Social Science Faculty/',  na cidade de  Colonia, Uruguai,
no período compreendido entre 06/09/2018 e 09/09/2018, incluído trânsito, com ônus limitado. Solicitação nº
59161.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
