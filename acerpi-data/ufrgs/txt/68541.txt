Documento gerado sob autenticação Nº VAK.153.924.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6508                  de  21/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANGELA DE MOURA FERREIRA DANILEVICZ,  Professor do
Magistério Superior, lotada e em exercício no Departamento de Engenharia de Produção e Transportes da
Escola de Engenharia, com a finalidade de participar da "XVIII Semana de la Ingenieria de la Producción
Sudamericana", em Córdoba, Argentina, no período compreendido entre 29/08/2018 e 01/09/2018, incluído
trânsito, com ônus limitado. Solicitação nº 58210.
RUI VICENTE OPPERMANN
Reitor
