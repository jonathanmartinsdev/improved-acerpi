Documento gerado sob autenticação Nº BJS.142.737.4DM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9757                  de  03/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5067072-77.2018.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006,  da servidora FATIMARLEI  LUNARDELLI,  matrícula  SIAPE n°  0356505,  aposentada no cargo
de Jornalista - 701045, para o nível IV, conforme o Processo nº 23078.532687/2018-87.
Tornar sem efeito a(s)  portaria(s)  de concessão de progressão por capacitação gerada(s)  após
a implementação do PCCTAE.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria
