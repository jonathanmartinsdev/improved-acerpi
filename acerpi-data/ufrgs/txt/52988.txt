Documento gerado sob autenticação Nº EDA.126.256.2L6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2674                  de  10/04/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Lotar  no  Departamento  Interdisciplinar  do  Campus  Litoral  Norte,  área  Geociências,  subárea:
Estratigrafia, Paleontologia e Sedimentologia, a partir de 03/04/2018, FELIPE CARON,  matrícula SIAPE n°
1852145, ocupante do cargo de Professor do Magistério Superior, classe Adjunto, redistribuído conforme
Portaria nº 361 de 12 de março de 2018, do Ministério da Educação, publicada no Diário Oficial da União de
13 de março de 2018. Processo nº 23078.018564/2017-57.
RUI VICENTE OPPERMANN
Reitor.
