Documento gerado sob autenticação Nº PFH.776.654.C5D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10394                  de  19/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de SERGIO BAMPI, Professor do Magistério Superior, lotado e em
exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de  Informática,  com  a  finalidade  de
participar da "26th IEEE ICECS International Conference on Electronics, Circuits, and Systems (ICECS 2019), em
Genova,  Itália,  no  período  compreendido  entre  26/11/2019  e  30/11/2019,  incluído  trânsito,  com  ônus
CAPES/PROAP e CAPES/PROBRAL. Solicitação nº 87916.
RUI VICENTE OPPERMANN
Reitor
