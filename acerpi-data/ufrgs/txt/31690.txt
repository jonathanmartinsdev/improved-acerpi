Documento gerado sob autenticação Nº AGA.564.854.AT5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10088                  de  21/12/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANDRE CEZAR ZINGANO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia de Minas da Escola de Engenharia, com a finalidade
de realizar  visita  à China University of Mining and Technology, em Beijing, China, no período compreendido
entre 06/01/2017 e 19/01/2017, incluído trânsito, com ônus limitado. Solicitação nº 25484.
RUI VICENTE OPPERMANN
Reitor
