Documento gerado sob autenticação Nº SLX.813.526.GLG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2195                  de  11/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar  insubsistente  a  Portaria  n°  1733/2019,  de  20/02/2019,  que  concede  progressão  por
capacitação a ISMAEL GONCALVES VIEIRA, Eletricista, com exercício na Gerência Administrativa do Instituto
de Informática, conforme Processo nº 23078.500715/2019-88.
 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
