Documento gerado sob autenticação Nº SJG.842.664.OB6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7450                  de  16/08/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°85343,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CESAR LEANDRO SCHULTZ (Siape: 0359239 ),  para
substituir   ROMMULO VIEIRA CONCEIÇÃO (Siape: 1298184 ), Coordenador do PPG em Geociências, Código
FUC, em seu afastamento do país, no período de 18/08/2019 a 24/08/2019, com o decorrente pagamento das
vantagens por 7 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
