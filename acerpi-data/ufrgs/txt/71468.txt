Documento gerado sob autenticação Nº XXN.188.039.7KD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8692                  de  26/10/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5052039-47.2018.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, do servidor ROGERIO DE LEMOS COSTA , matrícula SIAPE n° 0357307, ativo no cargo de  Auxiliar
em Administração - 701405, para o nível IV, conforme o Processo nº 23078.529350/2018-92.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
RUI VICENTE OPPERMANN
Reitor
