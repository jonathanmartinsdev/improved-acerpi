Documento gerado sob autenticação Nº UUQ.521.448.77R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2959                  de  23/04/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
ALTERAR o percentual  de  Incentivo  à  Qualificação concedido à  servidora  FRANCISCA AURINA
GONCALVES,  ocupante do cargo de Enfermeiro-área-701029, lotada na Faculdade de Odontologia, SIAPE
1160531, para 75% (setenta e cinco por cento), a contar de 08/03/2018, tendo em vista a conclusão do curso
de Doutorado em Odontologia, conforme o Processo nº 23078.504578/2018-70.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
