Documento gerado sob autenticação Nº XMH.218.694.A4R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5540                  de  25/07/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Remover, a partir de 25 de julho de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
MARCO ANTONIO VIEIRA DOS SANTOS,  ocupante  do  cargo  de  Técnico  de  Laboratório  Área,  Ambiente
Organizacional Ciências Biológicas, Código 701244, Classe D, Nível de Capacitação II, Padrão de Vencimento
16, SIAPE nº. 0356656 do Instituto de Química para a lotação Instituto de Biociências, com novo exercício no
Laboratórios Multidisciplinares do Biociências.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
