Documento gerado sob autenticação Nº AYG.991.305.8NB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3980                  de  09/05/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
RESOLVE
1 - Tornar sem efeito a Portaria nº 3507 de 27 de abril de 2017.
2 - Designar os servidores  PABLO CHAVES ORTIZ, ocupante do cargo de Administrador, lotado na
Pró-Reitoria de Planejamento e Administração e com exercício na Divisão de Tombamento e Levantamento
de Bens Móveis, JOEL MARTINS APARICIO,  ocupante do cargo de Apontador, lotado na Pró-Reitoria de
Planejamento e Administração e com exercício na Secretaria do Departamento de Patrimônio, RODRIGO
ALVAREZ ALVES, ocupante do cargo de Assistente em Administração, lotado na Pró-Reitoria de Planejamento
e Administração e com exercício na Divisão de Registro de Bens Móveis e Imóveis, CLOVIS MESQUITA DE
OLIVEIRA,  ocupante do cargo de Engenheiro-área,  lotado na Superintendência de Infraestrutura e com
exercício no Setor de Fiscalização e Adequação, MARCO ANTONIO SCHUCK, ocupante do cargo de Arquiteto
e  Urbanista,  lotado na  Superintendência  de  Infraestrutura  e  com exercício  no  Setor  de  Fiscalização  e
Adequação,  para,  sob  a  presidência  do  primeiro,  comporem  a  Comissão  Permanente  de  Alienação  e
Desfazimento de Bens Móveis, que terá como atribuição efetuar a avaliação física e financeira dos bens
móveis a serem alienados nas formas previstas em Lei.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
