Documento gerado sob autenticação Nº RRG.938.387.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5373                  de  26/06/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LUCIANE DA COSTA CUERVO, Professor do Magistério Superior,
lotada e em exercício no Departamento de Música do Instituto de Artes, com a finalidade de participar da "XII
Conferencia  Latinoamericana y  IV  Panamericana de  Educación Musical  de  ISME 2019",  em Resistencia,
Argentina, no período compreendido entre 01/07/2019 e 07/07/2019, incluído trânsito, com ônus UFRGS
(Pró-Reitoria de Pesquisa: diárias). Solicitação nº 84543.
RUI VICENTE OPPERMANN
Reitor
