Documento gerado sob autenticação Nº LDD.934.132.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6008                  de  08/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de GABRIELA ZUBARAN DE AZEVEDO PIZZATO,  Professor do
Magistério Superior, lotada e em exercício no Departamento de Design e Expressão Gráfica da Faculdade de
Arquitetura, com a finalidade de participar do "20th Congress of the International Ergonomics Association",
em Florença, Itália, no período compreendido entre 24/08/2018 e 31/08/2018, incluído trânsito, com ônus
limitado. Solicitação nº 58115.
RUI VICENTE OPPERMANN
Reitor
