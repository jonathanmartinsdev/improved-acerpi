Documento gerado sob autenticação Nº FUQ.173.265.RJB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8779                  de  27/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LUCIANE MAGALHAES CORTE REAL, Professor do Magistério
Superior, lotada e em exercício no Departamento de Estudos Básicos da Faculdade de Educação, com a
finalidade de participar do "XXI Congreso Internacional de Informática Educativa", em Santiago, Chile, no
período compreendido entre 28/11/2016 e 02/12/2016, incluído trânsito, com ônus limitado. Solicitação nº
24439.
RUI VICENTE OPPERMANN
Reitor
