Documento gerado sob autenticação Nº OWV.205.851.I7H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             957                  de  29/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar,  a partir  da data de publicação no Diário Oficial  da União,  o ocupante do cargo de
Assistente  em  Administração  -  701200,  do  Nível  de  Classificação  DII,  do  Quadro  de  Pessoal  desta
Universidade, EDSON MENDES DA SILVA JUNIOR, matrícula SIAPE 2265020, da função de Secretário de Pós-
Graduação da Faculdade de Educação, Código SRH 584, Código FG-7, para a qual foi designado pela Portaria
nº  4777/2018,  de  04/07/2018,  publicada  no  Diário  Oficial  da  União  de  06/07/2018.  Processo  nº
23078.501673/2020-36.
RUI VICENTE OPPERMANN
Reitor.
