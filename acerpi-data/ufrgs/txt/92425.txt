Documento gerado sob autenticação Nº HCV.705.687.SFN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4871                  de  07/06/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°84220,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, TANIA MARQUES STROHAECKER (Siape: 0995336 ),
 para substituir   ADRIANA DORFMAN (Siape: 2099219 ), Coordenador da COMGRAD de Geografia, Código
FUC, em seu afastamento do país, no período de 09/06/2019 a 13/06/2019, com o decorrente pagamento das
vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
