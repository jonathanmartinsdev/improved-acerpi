Documento gerado sob autenticação Nº QUQ.506.343.SGF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5812                  de  04/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  03/08/2016,   referente  ao  interstício  de
03/02/2015 a 02/08/2016, para o servidor GUSTAVO LEIVAS BARBOSA,  ocupante do cargo de Biólogo -
701011, matrícula SIAPE 2718316,  lotado  na  Superintendência de Infraestrutura, passando do Nível de
Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.012964/2016-78:
Formação Integral de Servidores da UFRGS II CH: 59 (29/07/2015 a 18/05/2016)
SBAU - 1º Fórum Internacional de Avaliação de Árvores de Risco CH: 22 Carga horária utilizada: 21 hora(s) /
Carga horária excedente: 1 hora(s) (23/03/2015 a 25/03/2015)
Prime Cursos - Curso de Paisagismo e Plantas Ornamentais CH: 40 (09/02/2015 a 19/02/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
