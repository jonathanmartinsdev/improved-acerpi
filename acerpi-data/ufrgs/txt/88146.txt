Documento gerado sob autenticação Nº NWR.264.556.8E2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2295                  de  13/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  13/03/2019,   referente  ao  interstício  de
13/09/2017 a 12/03/2019, para o servidor GUILHERME DA SILVA MOTTIN, ocupante do cargo de Assistente
em Administração - 701200, matrícula SIAPE 2425814,  lotado  no  Instituto de Química, passando do Nível
de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.505864/2019-33:
Formação Integral de Servidores da UFRGS IV CH: 92 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 2 hora(s) (29/01/2018 a 01/10/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
