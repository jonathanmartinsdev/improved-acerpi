Documento gerado sob autenticação Nº QUO.166.460.0PS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4239                  de  15/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CLARISSA SEVERINO GAMA, Professor do Magistério Superior,
lotada no Departamento de Psiquiatria e Medicina Legal da Faculdade de Medicina e com exercício no
Programa de Pós-Graduação em Psiquiatria e Ciências do Comportamento, com a finalidade de participar de
reunião  junto  ao  Lundbeck  International  Neuroscience  Foundation,  em  Pavia,  Itália,  no  período
compreendido entre 06/06/2019 e 10/06/2019, incluído trânsito, com ônus limitado. Solicitação nº 84205.
RUI VICENTE OPPERMANN
Reitor
