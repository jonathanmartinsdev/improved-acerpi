Documento gerado sob autenticação Nº NHH.826.688.J5S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5518                  de  25/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor MARCELO LIMA CELENTE,
ocupante do cargo de Engenheiro-área-701031, lotado na Faculdade de Ciências Econômicas, SIAPE 1035324,
para 52% (cinquenta e dois por cento), a contar de 02/04/2018, tendo em vista a conclusão do curso de
Mestrado  em  Engenharia  -  Área  de  Concentração:  Sistemas  de  Produção,  conforme  o  Processo  nº
23078.506950/2018-82.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
