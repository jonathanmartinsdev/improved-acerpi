Documento gerado sob autenticação Nº HHB.281.474.QMQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2925                  de  20/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 40, parágrafo 1º, inciso I, da Constituição Federal, c/c
art. 6-A da Emenda Constitucional nº 41, de 19 de dezembro de 2003, incluído pela Emenda Constitucional nº
70, de 29 de março de 2012, e art. 186, parágrafo 1º, da Lei nº 8.112, de 11 de dezembro de 1990, a GISELA
COLLISCHONN,  matrícula  SIAPE  nº  1143043,  no  cargo  de  Professor  Titular  da  Carreira  do  Magistério
Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no Departamento
de  Letras  Clássicas  e  Vernáculas  do  Instituto  de  Letras,  com  proventos  integrais.  Processo
23078.006130/2016-23.
CARLOS ALEXANDRE NETTO
Reitor
