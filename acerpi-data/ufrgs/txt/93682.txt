Documento gerado sob autenticação Nº MNC.262.545.4JE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5620                  de  04/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  28/06/2019,   referente  ao  interstício  de
06/04/2015 a 27/06/2019, para a servidora NADIA DE FATIMA BORBA MARTINS,  ocupante do cargo de
Técnico em Assuntos Educacionais - 701079, matrícula SIAPE 1200644,  lotada  no  Campus Litoral Norte,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  E  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  E  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.516895/2019-10:
Formação Integral de Servidores da UFRGS III CH: 70 (22/05/2015 a 18/06/2019)
Lumina - Moodle em Ação: Atividades e Recursos CH: 30 (22/01/2019 a 13/02/2019)
Lumina - Desconstruindo o Racismo na Prática CH: 60 (18/01/2019 a 13/02/2019)
Capes - I Seminário Online "Desafios para a Docência online" CH: 30 Carga horária utilizada: 20 hora(s) /
Carga horária excedente: 10 hora(s) (20/05/2019 a 24/05/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
