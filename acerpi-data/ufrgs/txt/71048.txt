Documento gerado sob autenticação Nº ZEK.490.344.UOV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8374                  de  18/10/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor HELDER FERREIRA TEIXEIRA, matrícula SIAPE n° 1359263, lotado no Departamento de Produção e
Controle de Medicamentos da Faculdade de Farmácia e com exercício na Vice-Direção da Faculdade de
Farmácia, da classe D  de Professor Associado, nível 04, para a classe E  de Professor Titular,  referente ao
interstício de 08/08/2016 a 02/10/2018, com vigência financeira a partir de 03/10/2018, de acordo com o que
dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN.
Processo nº 23078.527714/2018-08.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
