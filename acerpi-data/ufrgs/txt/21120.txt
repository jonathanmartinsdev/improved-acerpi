Documento gerado sob autenticação Nº DNJ.535.964.87C, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3052                  de  25/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  LEANDRO  KRUG  WIVES,  matrícula  SIAPE  n°  1531202,  com  exercício  no  Departamento  de
Informática Aplicada, da classe D  de Professor Associado, nível 01, para a classe D  de Professor Associado,
nível 02, referente ao interstício de 02/05/2014 a 01/05/2016, com vigência financeira a partir de 22/04/2016,
de acordo com o que dispõe a Decisão nº 197/2006-CONSUN, alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.501964/2016-48.
RUI VICENTE OPPERMANN
Vice-Reitor
