Documento gerado sob autenticação Nº GGJ.310.778.EBB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5405                  de  20/07/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, 
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, NORBERTO DANI,  matrícula SIAPE n° 0357083, lotado no Departamento de
Geodésia  do Instituto  de  Geociências,  como Diretor  Substituto  do Centro  de  Estudos  em Petrologia  e
Geoquímica, para substituir automaticamente o titular desta função em seus afastamentos ou impedimentos
regulamentares  no  período  de  12/08/2018  até  11/08/2020,  por  ter  sido  reeleito.  Processo  nº
23078.517823/2018-17.
JANE FRAGA TUTIKIAN
Vice-Reitora.
