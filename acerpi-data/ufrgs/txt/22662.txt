Documento gerado sob autenticação Nº LKM.872.055.84H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3896                  de  27/05/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARIA CRISTINA DIAS LAY, Professor do Magistério Superior,
lotada e em exercício no Departamento de Urbanismo da Faculdade de Arquitetura, com a finalidade de
participar  da "24th Conference of  the International  Association People-Environment  Studies",  em Lund,
Suécia,  no  período  compreendido  entre  25/06/2016  e  03/07/2016,  incluído  trânsito,  com  ônus
PROPG/FAURGS.  Solicitação  nº  19543.
CARLOS ALEXANDRE NETTO
Reitor
