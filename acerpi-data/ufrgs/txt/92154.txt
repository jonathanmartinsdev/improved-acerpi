Documento gerado sob autenticação Nº FDS.810.510.OIP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4903                  de  07/06/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora ALINE DA SILVA COSTA, ocupante do cargo de  Técnico em Contabilidade -
701224, lotada na Pró-Reitoria de Planejamento e Administração, SIAPE 3125480, o percentual de 25% (vinte
e cinco por cento) de Incentivo à Qualificação, a contar de 16/05/2019 , tendo em vista a conclusão do curso
de Bacharelado em Ciências Contábeis, conforme o Processo nº 23078.512752/2019-39.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
