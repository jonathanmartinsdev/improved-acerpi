Documento gerado sob autenticação Nº APE.195.627.VGG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4238                  de  11/06/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/04/2018,   referente  ao  interstício  de
06/03/2013 a 10/04/2018, para a servidora CLARICE LEHNEN WOLFF, ocupante do cargo de Fonoaudiólogo -
701039, matrícula SIAPE 1734916,  lotada  no  Instituto de Psicologia, passando do Nível de Classificação/Nível
de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude de ter realizado o(s)
seguinte(s) curso(s), conforme o Processo nº 23078.508167/2018-53:
Formação Integral de Servidores da UFRGS II  CH: 50 Carga horária utilizada: 47 hora(s) /  Carga horária
excedente: 3 hora(s) (21/07/2010 a 02/03/2018)
2º Jornada Internacional de Alfabetização; 4º Jornada Nacional de Alfabetização; 12ª Jornada de Alfabetização
CH: 20 (22/08/2016 a 23/08/2016)
3ª  Jornada  Internacional  de  Alfabetização/  5ª  Jornada  Nacional  de  Alfabetização/  13ª  Jornada  de
Alfabetização/ 4º Jornada de Educação, Leitura e Neurociências CH: 22 (07/08/2017 a 08/08/2017)
X Encontro Nacional de Aquisição de Linguagem e IV Encontro Internacional de Aquisição de Linguagem CH:
21 (09/11/2016 a 11/11/2016)
Curso de Inglês CH: 40 (01/08/2016 a 31/10/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
