Documento gerado sob autenticação Nº MFO.717.707.KO1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10599                  de  21/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Alterar o ambiente organizacional, a partir de 10 de dezembro de 2012, de ROSEMERI SIQUEIRA
PEDROSO, matrícula SIAPE nº 358669, ocupante do cargo de Auxiliar de Laboratório, de Administrativo para
Ciências da Saúde. Processo n° 23078.009228/2014-71.
 
 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
