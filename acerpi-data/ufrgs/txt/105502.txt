Documento gerado sob autenticação Nº VHQ.321.821.IRF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1842                  de  05/03/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°54850,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  JOAO  CARLOS  OLIVA  (Siape:  1081416  ),   para
substituir   ANGELA PEÑA GHISLENI (Siape: 2085705 ), Chefe do Depto de Educação Física, Fisioterapia e
Dança da ESEFID, Código FG-1, em seu afastamento por motivo de férias,  no período de 22/02/2020 a
28/02/2020, com o decorrente pagamento das vantagens por 7 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
