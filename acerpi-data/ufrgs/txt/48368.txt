Documento gerado sob autenticação Nº LVU.116.187.2R5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11381                  de  21/12/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, BRUNA WULFF FETTER, em virtude de habilitação em Concurso Público
de Provas e Títulos,  conforme Edital  Nº  71/2017 de 05 de dezembro de 2017,  homologado em 06 de
dezembro de 2017 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei
12.772,  de 28 de dezembro de 2012,  com redação dada pela Lei  12.863,  de 24 de setembro de 2013,
publicada  no  Diário  Oficial  da  União  de  25  de  setembro  de  2013,  para  o  cargo  de  PROFESSOR  DO
MAGISTÉRIO SUPERIOR do Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor
Adjunto A, Nível I,  do Quadro de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação
Exclusiva),  junto  ao  Departamento  de  Artes  Visuais  do  Instituto  de  Artes,  em  vaga  decorrente  da
aposentadoria da Professora Monica Zielinsky, código nº 277209, ocorrida em 4 de Maio de 2017, conforme
Portaria nº.  3730/2017 de 3 de maio de 2017,  publicada no Diário Oficial  da União de 04 de maio de
2017. Processo nº. 23078.506694/2017-42.
RUI VICENTE OPPERMANN
Reitor.
