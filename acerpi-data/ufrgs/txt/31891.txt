Documento gerado sob autenticação Nº LXA.527.914.9G3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10231                  de  23/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°29318,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MARIA DE LURDES FURNO DA SILVA (Siape: 1095481
),  para substituir   CARLOS HENRIQUE VASCONCELLOS HORN (Siape: 0351358 ), Diretor da FCE, Código CD-3,
em seu afastamento por motivo de férias,  no período de 23/12/2016 a 30/12/2016,  com o decorrente
pagamento das vantagens por 8 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
