Documento gerado sob autenticação Nº HPE.409.413.KV5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7141                  de  04/08/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ANTONIO MARCOS HELGUEIRA DE ANDRADE, matrícula SIAPE n° 2545576, lotado e em exercício
no Departamento de Física do Instituto de Física, da classe C  de Professor Adjunto, nível 04, para a classe D 
de Professor Associado, nível 01, referente ao interstício de 10/07/2015 a 09/07/2017, com vigência financeira
a partir de 10/07/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações e  a  Decisão nº  197/2006-CONSUN,  alterada pela  Decisão nº  401/2013-CONSUN. Processo nº
23078.509633/2017-37.
JANE FRAGA TUTIKIAN
Vice-Reitora.
