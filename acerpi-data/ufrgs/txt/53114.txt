Documento gerado sob autenticação Nº IPY.978.773.LJG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2699                  de  11/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLAUNARA SCHILLING MENDONCA, Professor do Magistério
Superior,  lotada e em exercício no Departamento de Medicina Social da Faculdade de Medicina, com a
finalidade de participar da "First Meeting of the International Advisory Group on Primary Health Care for
Universal Health Coverage", em Genebra, Suíça, no período compreendido entre 24/04/2018 e 29/04/2018,
incluído trânsito, com ônus limitado. Solicitação nº 45186.
RUI VICENTE OPPERMANN
Reitor
