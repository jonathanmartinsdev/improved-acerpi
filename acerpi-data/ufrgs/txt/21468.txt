Documento gerado sob autenticação Nº HDL.636.507.TUD, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3253                  de  03/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°19653,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  FERNANDA  POLETTO  (Siape:  2078033  ),   para
substituir   ALOIR ANTONIO MERLO (Siape: 1142866 ), Chefe do Depto de Química Orgânica do Instituto de
Química, Código FG-1, em seu afastamento no país, no dia  28/04/2016, com o decorrente pagamento das
vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
