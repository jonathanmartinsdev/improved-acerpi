Documento gerado sob autenticação Nº VBV.779.376.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5968                  de  15/07/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do País  de JEAN CARLO PECH DE MORAES,  Professor  do Magistério
Superior, lotado e em exercício no Departamento de Matemática Pura e Aplicada do Instituto de Matemática
e  Estatística,  com  a  finalidade  de  proferir  palestra  no  "IX  CIMAC  2019",  em  Lima,  Peru,  no  período
compreendido entre 06/08/2019 e 10/08/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa:
diárias). Solicitação nº 85262.
RUI VICENTE OPPERMANN
Reitor
