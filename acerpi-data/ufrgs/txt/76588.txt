Documento gerado sob autenticação Nº BSS.588.771.ES7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1422                  de  08/02/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°45711,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, PATRICIA HELENA XAVIER DOS SANTOS (Siape:
0359307 ),  para substituir   DAIANE DOS SANTOS MORAES (Siape: 0358241 ), Coordenador da Coordenação
de Projetos Sociais do DEDS da PROREXT, Código FG-5, em seu afastamento por motivo de férias, no período
de 11/02/2019 a 22/02/2019, com o decorrente pagamento das vantagens por 12 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
