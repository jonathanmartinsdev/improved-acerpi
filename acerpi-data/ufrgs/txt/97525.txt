Documento gerado sob autenticação Nº NDU.457.740.CCM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8016                  de  03/09/2019
A VICE-REITORA  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Retificar  a  Portaria  n°  7717/2019,  de  26/08/2019,  que  lota  no  Departamento  de  Nutrição  da
Faculdade de Medicina,  a partir  de 15/08/2019,  MARIA TERESA ANSELMO OLINTO,  matrícula SIAPE n°
1867567, ocupante do cargo de Professor do Magistério Superior, classe Adjunto, redistribuída conforme
Portaria nº 1.410, de 13 de agosto de 2019, do Ministério da Educação, publicada no Diário Oficial da União
de 15 de agosto de 2019. Processo nº 23000.020536/2019-65.
 
Onde se lê:
" ...no Departamento de Nutrição da Faculdade de Medicina,"
leia-se:
" ...no Departamento de Nutrição, área de Nutrição: Saúde Coletiva, da Faculdade de Medicina," 
JANE FRAGA TUTIKIAN
Vice-Reitora
