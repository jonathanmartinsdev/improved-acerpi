Documento gerado sob autenticação Nº KZU.476.142.AE2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7900                  de  05/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  14/09/2016,   referente  ao  interstício  de
08/08/2014 a 13/09/2016, para o servidor JOSE FRANCO OLIVEIRA,  ocupante do cargo de Operador de
Máquinas Agrícolas - 701452,  matrícula SIAPE 2034685,  lotado  na  Estação Experimental Agronômica,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  C  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  C  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.018507/2016-97:
Formação Integral de Servidores da UFRGS III CH: 66 (01/07/2013 a 14/09/2016)
PROREXT - Manejo de Sistemas Pastoris CH: 24 (24/02/2016 a 26/02/2016)
SENAR -  Treinamento  em Retroescavadeira  CH:  32  Carga  horária  utilizada:  30  hora(s)  /  Carga  horária
excedente: 2 hora(s) (12/04/2016 a 15/04/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
