Documento gerado sob autenticação Nº MIN.420.662.1S1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7992                  de  03/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°56932,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, GUILHERME THESE BATISTA (Siape: 3086968 ),
 para substituir   GISLAINE MARTINS RETAMOZO (Siape: 2146530 ), Coordenador do Núcleo de Recursos
Humanos da Gerência Administrativa da Faculdade de Medicina, Código FG-7,  em seu afastamento por
motivo  de  Laudo Médico  do  titular  da  Função,  no  dia  08/08/2019,  com o  decorrente  pagamento  das
vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
