Documento gerado sob autenticação Nº XTV.735.887.14A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5713                  de  30/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder prorrogação de afastamento parcial,  no período de 01/07/2017 a 08/09/2017, para a
servidora SAMARA LOUREIRO DE MOURA, ocupante do cargo de Técnico em Assuntos Educacionais -
701079, matrícula SIAPE 1908327,  lotada  na  Faculdade de Veterinária, para cursar o Mestrado Acadêmico
em Educação, no Centro Universitário La Salle - UNILASALLE; conforme o Processo nº 23078.200371/2017-48.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
