Documento gerado sob autenticação Nº DJP.000.370.MF7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             2711                  de  28/03/2019
Nomeação  da  Representação  Discente  dos
órgãos  coleg iados  da  Faculdade  de
Biblioteconomia  e  Comunicação  da  UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita na Faculdade de Biblioteconomia e Comunicação, com
mandato de 01 (um) ano, a contar de 03 de março de 2019, atendendo o disposto nos artigos 175 do
Regimento  Geral  da  Universidade  e  79  do  Estatuto  da  Universidade,  e  considerando  o  processo
nº 23078.501819/2019-18, conforme segue:
 
Conselho da Unidade
Titular: Amanda Dall'Agnol Barbosa
Suplente: Bárbara Rotta Dalcanale
 
Plenária do DCI
Titular: Amanda Dall'Agnol Barbosa
Titular: Gabriela Meneghel Colla Mattia
Titular: Laura Victória Silva Santos
Titular: Bárbara Rotta Dalcanale
Titular: Diogo Santos Gomes
 
Colegiado do DCI
Titular: Douglas Silveira da Rosa
 
Comissão de Graduação - Arquivologia
Titular: Mariana Severo da Silva
 
Comissão de Graduação - Biblioteconomia
Titular: Amanda Dall'Agnol Barbosa
 
Comissão de Graduação - Museologia
Documento gerado sob autenticação Nº DJP.000.370.MF7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Titular: Bárbara Rotta Dalcanale
 
Comissão de Pesquisa (COMPESQ)
Titular: Arthur Basilio Venturella Alves
 
Comissão de Extensão (COMEX)
Titular: Andrine de Vargas Martins
 
Biblioteca
Titular: Sara de Vargas Moraes
 
Núcleo de Avaliação da Unidade
Titular: Sofia Perseu
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
