Documento gerado sob autenticação Nº CGD.157.988.FL7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8354                  de  06/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar  a  Portaria  nº  7391/2017,  de  10/08/2017,  que  designou,  temporariamente,  FLAVIA
CRISTIANE FARINA (Siape: 2489920) para substituir  ANDREA RITTER JELINEK (Siape: 2174454 ),
onde se lê:
"... em seu afastamento por motivo de férias, no período de 15/08/2017 a 08/09/2017, com o
decorrente pagamento das vantagens por 25 dias...".
 
leia-se:
"... em seu afastamento por motivo de férias, no período de 15/08/2018 a 25/08/2017, com o
decorrente  pagamento  das  vantagens  por  11  dias...",  ficando ratificados  os  demais  termos.  Conforme
processo SEI nº 23078.513338/2017-85
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
