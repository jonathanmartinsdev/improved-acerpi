Documento gerado sob autenticação Nº FES.387.187.Q6C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2142                  de  08/03/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de TIAGO ROBERTO BALEN,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia Elétrica da Escola de Engenharia, com a finalidade de
participar do "20th IEEE Latin American Test Symposium", em Santiago, Chile, no período compreendido
entre  10/03/2019  e  14/03/2019,  incluído  trânsito,  com ônus  UFRGS (Pró-Reitoria  de  Pesquisa:  diárias).
Solicitação nº 62026.
RUI VICENTE OPPERMANN
Reitor
