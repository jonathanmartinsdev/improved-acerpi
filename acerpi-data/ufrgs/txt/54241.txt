Documento gerado sob autenticação Nº DSJ.932.178.9NJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3481                  de  11/05/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  MARINO  MUXFELDT  BIANCHIN,  matrícula  SIAPE  n°  1299103,  lotado  no
Departamento de Medicina Interna da Faculdade de Medicina, para exercer a função de Coordenador do
PPG em Medicina:  Ciências  Médicas,  Código SRH 1169,  código FUC,  com vigência  a  partir  da  data  de
publicação no Diário Oficial da União, pelo período de 02 anos. Processo nº 23078.508157/2018-18.
JANE FRAGA TUTIKIAN
Vice-Reitora.
