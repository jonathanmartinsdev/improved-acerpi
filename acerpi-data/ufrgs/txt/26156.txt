Documento gerado sob autenticação Nº LOJ.232.955.84S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6132                  de  18/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder à servidora CHRIS KREBS DANILEVICZ, ocupante do cargo de  Técnico de Laboratório
Área - 701244, lotada na Faculdade de Odontologia, SIAPE 1650983, o percentual de 25% (vinte e cinco por
cento)  de  Incentivo  à  Qualificação,  a  contar  de  17/08/2016,  tendo  em vista  a  conclusão  do  curso  de
Graduação em Farmácia, conforme o Processo nº 23078.018417/2016-04.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
