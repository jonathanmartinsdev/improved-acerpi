Documento gerado sob autenticação Nº LLR.254.201.70K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6205                  de  19/07/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de PAULO CESAR DE FACCIO CARVALHO, Professor do Magistério
Superior, lotado e em exercício no Departamento de Plantas Forrageiras e Agrometeorologia da Faculdade
de  Agronomia,  com  a  finalidade  de  proferir  palestra  no  "XXVII  Congreso  Aapresid"  e  realizar  visita  à
Universidad Nacional  de Rosario,  em Rosario,  Argentina,  no período compreendido entre 06/08/2019 e
10/08/2019, incluído trânsito, com ônus limitado. Solicitação nº 85537.
RUI VICENTE OPPERMANN
Reitor
