Documento gerado sob autenticação Nº KLA.030.293.ASV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1334                  de  10/02/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Retificar a Portaria n° 1260/2017, de 09/02/2017, que Designou como Coordenadora Substituta
"pró-tempore"  do  PPG  em  Ciências  Farmacêuticas  SOLANGE  CRISTINA  GARCIA.  Processo  nº
23078.000058/2017-10.
 
 
Onde se lê:
"(...), na vigência do presente mandato(...)".
 
leia-se:
"(...)  com vigência a partir  de 10/02/2017 e até 31/03/2017, (...),"  ficando ratificados os demais
termos.
JANE FRAGA TUTIKIAN
Vice-Reitora.
