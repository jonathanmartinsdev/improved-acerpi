Documento gerado sob autenticação Nº XNU.055.624.F13, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8059                  de  09/10/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ALEXANDRE FAVERO BULGARELLI, Professor do Magistério
Superior,  lotado e em exercício no Departamento de Odontologia Preventiva e Social  da Faculdade de
Odontologia, com a finalidade de participar de reunião junto à McGill University, em Montreal, Canadá, no
período compreendido entre 01/11/2018 e 03/11/2018, incluído trânsito, com ônus limitado. Solicitação nº
59469.
RUI VICENTE OPPERMANN
Reitor
