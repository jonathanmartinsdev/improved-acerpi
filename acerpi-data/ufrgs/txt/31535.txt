Documento gerado sob autenticação Nº SAF.010.894.S5L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9981                  de  16/12/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta  Universidade,  ILZA MARIA TOURINHO GIRARDI,  matrícula  SIAPE  nº  0352762,  lotada  no
Departamento de Comunicação da Faculdade de Biblioteconomia e Comunicação, para exercer a função de
Vice-Diretora da FABICO, Código SRH 178, código FG-1, com vigência a partir  de 24/12/2016 e até 23/12/2020,
conforme o disposto nos artigos 5º e 6º do Decreto nº 1.916, de 23 de maio de 1996, que regulamenta a Lei
nº 9.192, de 21 de dezembro de 1995. Processo n° 23078.022797/2016-73.
RUI VICENTE OPPERMANN
Reitor
