Documento gerado sob autenticação Nº AFK.395.358.DO4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6278                  de  21/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°56521,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do Quadro de Pessoal  desta  Universidade,  DANIEL SEGALA (Siape:  3088101 ),   para
substituir   FERNANDA BORDIGNON SOARES (Siape: 2054586 ), Chefe do Setor de Finanças e Suprimentos da
Gerência Administrativa da Escola de Administração, Código FG-5, em seu afastamento por motivo de Laudo
Médico do titular da Função, no dia 10/07/2019, com o decorrente pagamento das vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
