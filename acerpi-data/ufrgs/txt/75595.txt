Documento gerado sob autenticação Nº QZA.982.626.9IP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             824                  de  23/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor GUSTAVO PEREIRA, ocupante do cargo de  Assistente em Administração -
701200, lotado no Instituto Latino-Americano de Estudos Avançados, SIAPE 2267363, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 21/02/2019, tendo em vista a conclusão do
curso de Graduação em História - Bacharelado, conforme o Processo nº 23078.501412/2019-82.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
