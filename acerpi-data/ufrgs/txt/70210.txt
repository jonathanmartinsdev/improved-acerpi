Documento gerado sob autenticação Nº QOG.328.801.V3E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7737                  de  28/09/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Tornar insubsistente a Portaria nº 7030/2018, de 05/09/2018, publicada no Diário Oficial da União
de 10/09/2018, que concedeu autorização para afastamento do País a ROMULO PLENTZ GIRALT, ocupante
do cargo de Professor do Magistério Superior, lotado e em exercício no Departamento de Arquitetura da
Faculdade de Arquitetura. Solicitação nº 58885.
RUI VICENTE OPPERMANN
Reitor.
