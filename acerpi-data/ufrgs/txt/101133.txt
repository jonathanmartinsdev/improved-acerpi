Documento gerado sob autenticação Nº THX.944.739.0AR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10465                  de  20/11/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5076323-85.2019.4.04.7100,  da  4ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, do servidor JOAO BATISTA CADEMARTORI DE MOURA, matrícula SIAPE n° 0353993, ativo no
cargo de Técnico de Laboratório Área - 701244, do nível I para o nível IV, a contar de 01/01/2006, conforme o
Processo nº 23078.531481/2019-11.
 
RUI VICENTE OPPERMANN
Reitor
