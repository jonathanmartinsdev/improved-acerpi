Documento gerado sob autenticação Nº HUT.963.083.EUJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4619                  de  23/05/2017
Nomeia  Representação  Discente
eleita  no  Programa  de  Pós-
Graduação Educação em Ciências:
Química da Vida e Saúde
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita para compor o Programa de Pós-Graduação Educação em
Ciências: Química da Vida e Saúde, no Instituto de Ciências Básicas da Saúde, com mandato de 01 (um) ano, a
contar de 01 de maio de 2017, atendendo ao disposto nos artigos 175 do Regimento Geral da Universidade e
79 do Estatuto da Universidade e considerando o processo nº 23078.008255/2017-79, conforme segue:
 
PPG Educação em Ciências: Química da Vida e Saúde
Titular Juliana Carvalho Pereira
Suplente Joice Abramowicz
 
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
