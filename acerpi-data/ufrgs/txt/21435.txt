Documento gerado sob autenticação Nº HOR.613.800.OJK, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3318                  de  04/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5148, de 01 de outubro de 2012
RESOLVE
Autorizar o afastamento no país de LIVIA DONIDA BIASOTTO, ocupante do cargo de Administrador,
lotada na Pró-Reitoria de Extensão e com exercício no Salão de Atos e Plenarinho, com a finalidade de
desenvolver estudos em nível Mestrado, junto à Universidade Federal do Rio Grande do Sul, no período
compreendido  entre  06  de  junho  de  2016  e  31  de  maio  de  2017,  com  ônus  limitado.  Processo  n°
501909/2016-58.
RUI VICENTE OPPERMANN
Vice-Reitor
