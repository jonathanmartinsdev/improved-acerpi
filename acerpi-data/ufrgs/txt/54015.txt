Documento gerado sob autenticação Nº CES.987.591.UN7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3303                  de  07/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora RITA DO CARMO FERREIRA LAIPELT, matrícula SIAPE n° 2714174, lotada no Departamento de
Ciência da Informação da Faculdade de Biblioteconomia e Comunicação e com exercício na Comissão de
Graduação de Biblioteconomia, da classe C  de Professor Adjunto, nível 01, para a classe C  de Professor
Adjunto, nível 02, referente ao interstício de 01/07/2015 a 30/06/2017, com vigência financeira a partir de
03/05/2018, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e
a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.524601/2017-61.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
