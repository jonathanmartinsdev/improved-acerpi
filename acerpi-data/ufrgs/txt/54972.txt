Documento gerado sob autenticação Nº FDX.012.245.Q1B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4056                  de  04/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de RENATO PAKTER, Professor do Magistério Superior, lotado e em
exercício no Departamento de Física do Instituto de Física, com a finalidade de participar da "45th Conference
on Plasma Physics", em Praga, República Tcheca, no período compreendido entre 29/06/2018 e 06/07/2018,
incluído trânsito, com ônus limitado. Solicitação nº 46763.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
