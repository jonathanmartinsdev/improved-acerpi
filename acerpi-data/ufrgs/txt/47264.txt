Documento gerado sob autenticação Nº FTA.924.579.B9L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10547                  de  17/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  17/11/2017,   referente  ao  interstício  de
17/05/2016 a 16/11/2017, para a servidora ANELI TORRES VENTURINI, ocupante do cargo de Estatístico -
701033,  matrícula SIAPE 2316786,  lotada  na  Faculdade de Ciências Econômicas, passando do Nível de
Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.519215/2017-58:
Formação Integral  de Servidores da UFRGS I  CH: 31 Carga horária utilizada: 30 hora(s)  /  Carga horária
excedente: 1 hora(s) (11/10/2016 a 18/04/2017)
ENAP - Ética e Serviço Público CH: 20 (09/05/2017 a 29/05/2017)
ENAP - Sistema Eletrônico de Informações - SEI CH: 20 (08/12/2016 a 28/12/2016)
ENAP - Estatística CH: 20 (01/11/2016 a 21/11/2016)
ENAP - Básico em Orçamento Público CH: 30 (08/08/2017 a 04/09/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
