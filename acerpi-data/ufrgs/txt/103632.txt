Documento gerado sob autenticação Nº DCY.699.230.FOE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             432                  de  10/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de EDUARDO CESAR TONDO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ciências dos Alimentos do Instituto de Ciências e Tecnologia de
Alimentos, com a finalidade de participar da "International Production & Processing Expo (IPPE 2020), em
Atlanta, Estados Unidos, no período compreendido entre 26/01/2020 e 01/02/2020, incluído trânsito, com
ônus limitado. Solicitação nº 89554.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
