Documento gerado sob autenticação Nº PSS.415.700.G20, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             345                  de  10/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  17/12/2018,   referente  ao  interstício  de
06/10/2016 a 16/12/2018, para a servidora PATRICIA BARRETO DOS SANTOS LIMA, ocupante do cargo de
Jornalista - 701045, matrícula SIAPE 1859696,  lotada  na  Secretaria de Comunicação Social, passando do
Nível de Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.534911/2018-75:
Formação Integral de Servidores da UFRGS IV CH: 105 (11/10/2016 a 13/12/2018)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 5 (30/05/2016 a 05/07/2016)
ENAP  -  Introdução  à  Gestão  de  Processos  CH:  20  Carga  horária  utilizada:  16  hora(s)  /  Carga  horária
excedente: 4 hora(s) (15/08/2018 a 05/09/2018)
Fluxo - Curso Básico de Fotografia Expandida CH: 24 (10/11/2018 a 24/11/2018)
Agenda Pública - Publicação de dados em formato aberto CH: 30 (01/09/2018 a 30/10/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
