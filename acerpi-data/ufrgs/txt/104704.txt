Documento gerado sob autenticação Nº XKR.448.915.V15, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1130                  de  31/01/2020
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  CELIA  ELIZABETE  CAREGNATO,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Ensino e Currículo da Faculdade de Educação, com a
finalidade de participar de encontro junto à Universidad de Costa Rica, em San Juan, Costa Rica, no período
compreendido entre 15/02/2020 e 21/02/2020, incluído trânsito, com ônus limitado. Solicitação nº 89652.
RUI VICENTE OPPERMANN
Reitor
