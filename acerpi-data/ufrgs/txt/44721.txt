Documento gerado sob autenticação Nº AZO.217.953.605, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8720                  de  18/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  17/09/2017,   referente  ao  interstício  de
17/03/2016 a 16/09/2017,  para a servidora LIZIANE DA LUZ SEBEN SCHEFFLER,  ocupante do cargo de
Assistente em Administração - 701200, matrícula SIAPE 2887228,  lotada  no  Instituto de Artes, passando
do Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.011222/2017-14:
Formação Integral de Servidores da UFRGS IV CH: 105 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 15 hora(s) (03/05/2016 a 25/11/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
