Documento gerado sob autenticação Nº EOG.686.491.I34, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9252                  de  11/10/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor LEANDRO VALIATI,  matrícula  SIAPE n°  2024347,  lotado e em exercício no Departamento de
Economia e Relações Internacionais da Faculdade de Ciências Econômicas, da classe C  de Professor Adjunto,
nível 01, para a classe C  de Professor Adjunto, nível 02, referente ao interstício de 08/05/2016 a 07/05/2018,
com vigência  financeira  a  partir  de  08/05/2018,  conforme decisão judicial  proferida no processo nº
5054491-30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de
28  de  dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.519985/2019-62.
JANE FRAGA TUTIKIAN
Vice-Reitora.
