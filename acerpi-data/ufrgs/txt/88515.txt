Documento gerado sob autenticação Nº DNE.838.114.DB5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2500                  de  20/03/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARTINE ELISABETH KIENZLE HAGEN, matrícula SIAPE n° 1674238, lotada e em exercício no
Departamento de Nutrição da Faculdade de Medicina, da classe D  de Professor Associado, nível 01, para a
classe D  de Professor Associado, nível 02, referente ao interstício de 06/02/2017 a 05/02/2019, com vigência
financeira a partir de 06/02/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.501404/2019-36.
JANE FRAGA TUTIKIAN
Vice-Reitora.
