Documento gerado sob autenticação Nº BBE.638.674.7OF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3563                  de  16/05/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°46267,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TECNOLOGO-FORMAÇÃO, do
Quadro de Pessoal desta Universidade, ROSI MARIA DA ROSA MENDES (Siape: 0357200 ),  para substituir  
VERA REGINA DA CUNHA (Siape: 0355713 ), Coordenador do Núcleo de Assuntos Disciplinares da PROGESP,
Código FG-1,  em seu afastamento no país,  no período de 17/05/2018 a 19/05/2018,  com o decorrente
pagamento das vantagens por 3 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
