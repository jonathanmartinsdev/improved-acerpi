Documento gerado sob autenticação Nº VWB.366.394.RNQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             188                  de  04/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°52080,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, ANA LUIZA VIANNA DA SILVA (Siape: 0358717
),  para substituir   ANA GENARI COSTA (Siape: 2257223 ), Chefe do Setor de Recursos Humanos da Gerência
Administrativa da Faculdade de Direito, Código FG-7, em seu afastamento por motivo de férias, no período
de 06/01/2020 a 10/01/2020.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
