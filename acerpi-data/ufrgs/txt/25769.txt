Documento gerado sob autenticação Nº WBQ.779.567.GPJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5961                  de  10/08/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar o ocupante do cargo de Professor do Ensino Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, ANDERSON LUIS RUHOFF, matrícula SIAPE n° 1447494, lotado no Departamento
de Hidromecânica e Hidrologia do Instituto de Pesquisas Hidráulicas,  como Coordenador Substituto da
COMGRAD de  Engenharia  Ambiental,  para  substituir  automaticamente  o  titular  desta  função  em seus
afastamentos ou impedimentos regulamentares no período de 12/08/2016 e até 11/08/2018. Processo nº
23078.202001/2016-64.
RUI VICENTE OPPERMANN
Vice-Reitor
