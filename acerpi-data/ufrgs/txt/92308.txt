Documento gerado sob autenticação Nº UTA.735.222.7VN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4811                  de  05/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento do País  de LEANDRO BARBOSA DE PINHO,  Professor  do Magistério
Superior, lotado e em exercício no Departamento de Assistência e Orientação Profissional da Escola de
Enfermagem, com a finalidade de participar do "Global Conference on Nursing and Healthcare", em Roma,
Itália, no período compreendido entre 19/06/2019 e 26/06/2019, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa: diárias). Solicitação nº 83465.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
