Documento gerado sob autenticação Nº AJQ.727.554.8D6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7381                  de  18/09/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, RAFAEL ROESLER, matrícula SIAPE n° 2190466, lotado no Departamento de
Farmacologia do Instituto de Ciências Básicas da Saúde, como Vice-Diretor do Parque Científico e Tecnológico
da UFRGS, código SRH1472, para substituir automaticamente o titular desta função em seus afastamentos ou
impedimentos regulamentares no período de 09/10/2018 até 08/10/2022. Processo nº 23078.524681/2018-
36.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
