Documento gerado sob autenticação Nº NXO.448.431.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9603                  de  22/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País  de ANDRE LUIZ VALCARENGH,  Assistente em Administração,
lotado na Faculdade de Medicina e com exercício na Biblioteca da Faculdade de Medicina, com a finalidade
de  participar  do  "IV  Congreso  de  Extensión  Universitaria  AUGM",  em  Santiago,  Chile,  no  período
compreendido entre 03/11/2019 e 07/11/2019, incluído trânsito, com ônus limitado. Solicitação nº 87966.
RUI VICENTE OPPERMANN
Reitor
