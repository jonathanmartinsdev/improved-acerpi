Documento gerado sob autenticação Nº ZDE.402.732.NHE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4550                  de  24/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de AMANDA MULITERNO DOMINGUES LOURENCO DE LIMA,
Técnico  de  Laboratório  Área,  lotada  no  Instituto  de  Ciências  Básicas  da  Saúde  e  com  exercício  no
Departamento de Microbiologia, Imunologia e Parasitologia, com a finalidade de participar da "ASM Microbe
2019", em San Francisco, Estados Unidos, no período compreendido entre 19/06/2019 e 25/06/2019, incluído
trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa: diárias). Solicitação nº 72647.
RUI VICENTE OPPERMANN
Reitor
