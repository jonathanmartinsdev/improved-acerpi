Documento gerado sob autenticação Nº FRU.092.493.ONA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1731                  de  20/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  4183,  de 17 de outubro de 2008,  do
Magnífico Reitor, e conforme a Solicitação de Férias n°45953,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CRISTIANO KRUG (Siape: 1566597 ),  para substituir  
NAIRA MARIA BALZARETTI (Siape: 0354772 ), Diretor do Instituto de Física, Código CD-3, em seu afastamento
por motivo de férias, no período de 20/02/2019 a 08/03/2019, com o decorrente pagamento das vantagens
por 17 dias.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
