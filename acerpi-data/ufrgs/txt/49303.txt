Documento gerado sob autenticação Nº WZU.010.589.2RB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             360                  de  11/01/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  PHILIPPE  OLIVIER  ALEXANDRE  NAVAUX,  Professor  do
Magistério  Superior,  lotado  e  em  exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de
Informática, com a finalidade de participar de reuniões junto às Universitat Politècnica de Catalunya, em
Barcelona, Espanha, no período compreendido entre 20 e 24/01/2018 e Université Pierre et Marie Curie, em
Paris, França, no período compreendido entre 25 e 28/01/2018, incluído trânsito, com ônus CNPq. Solicitação
nº 33736.
RUI VICENTE OPPERMANN
Reitor
