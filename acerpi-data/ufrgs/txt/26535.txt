Documento gerado sob autenticação Nº YSC.178.204.PID, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6362                  de  24/08/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 5263, de 01 de outubro de
2012, e conforme a Solicitação de Férias n°27636,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de NUTRICIONISTA-HABILITAÇÃO,
do Quadro de Pessoal desta Universidade, LARA DE ARARIPE DE PAULA FONSECA (Siape: 1770019 ),  para
substituir   SUELEN RAMON DA ROSA (Siape: 1838801 ), Chefe da Seção do RU4 da Divisão de Alimentação do
DIE da PRAE, Código FG-3, em seu afastamento por motivo de férias, no período de 15/08/2016 a 24/08/2016,
com o decorrente pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
