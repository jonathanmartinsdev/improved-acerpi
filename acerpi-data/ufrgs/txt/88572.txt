Documento gerado sob autenticação Nº HNO.299.094.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2538                  de  21/03/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de VIRGINIA PRADELINA DA SILVEIRA FONSECA, Professor do
Magistério  Superior,  lotada  e  em  exercício  no  Departamento  de  Comunicação  da  Faculdade  de
Biblioteconomia  e  Comunicação,  com  a  finalidade  de  participar  do  "IV  Congreso  Internacional  de
Comunicación  y  Pensamiento",  em  Sevilha,  Espanha,  no  período  compreendido  entre  09/04/2019  e
13/04/2019, incluído trânsito, com ônus limitado. Solicitação nº 72703.
RUI VICENTE OPPERMANN
Reitor
