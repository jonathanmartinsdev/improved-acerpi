Documento gerado sob autenticação Nº BFY.614.159.T9B, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6116                  de  11/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar  na Pró-Reitoria  de Assuntos Estudantis,  com exercício  na Divisão de Moradia Estudantil,
ANDRESSA LOPES NULLE, nomeada conforme Portaria Nº 5120/2017 de 12 de junho de 2017, publicada no
Diário Oficial da União no dia 13 de junho de 2017, em efetivo exercício desde 10 de julho de 2017, ocupante
do cargo de ADMINISTRADOR, Ambiente Organizacional Administrativo, classe E, nível I,  padrão 101, no
Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
