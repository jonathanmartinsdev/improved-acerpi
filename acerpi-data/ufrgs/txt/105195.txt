Documento gerado sob autenticação Nº YXH.646.911.M8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1488                  de  12/02/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  MARCIA  GAIGER  DE  OLIVEIRA,  matrícula  SIAPE  n°  0382580,  lotada  no
Departamento de Odontologia Conservadora, para exercer a função de Chefe do Depto de Odontologia
Conservadora  da  Faculdade  de  Odontologia,  Código  SRH  150,  código  FG-1,  com  vigência  a  partir  de
17/03/2020 até 16/03/2022, por ter sido reeleita. Processo nº 23078.502108/2020-96.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
