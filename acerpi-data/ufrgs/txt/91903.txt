Documento gerado sob autenticação Nº EOY.325.326.O34, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4571                  de  27/05/2019
O PRÓ-REITOR DE GRADUAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7626, de 29 de setembro de 2016
RESOLVE
                Designar
 
Hamilton Fernando dos Santos Santana - Pró-Reitoria de Graduação;
Adriana Saldanha Ferrari - Faculdade de Medicina;
Danilo de Souza Junior - Secretaria de Educação à Distância;
Eduarda Souza Stropper - Pró-Reitoria de Assuntos Estudantis;
Elisângela Rosaria Pereira Bica - Pró-Reitoria de Gestão de Pessoas;
Liliane Della Libeira - Pró-Reitoria de Graduação;
Marcelo Teodoro de Assis - Pró-Reitoria de Graduação;
Marco Antonio Teixeira Vianna - Pró-Reitoria de Graduação;
Patrícia Costa Azevedo - Pró-Reitoria de Gestão de Pessoas;
Roberta Silva de Leon - Pró-Reitoria de Graduação;
Rodrigo Silva Bregagnol - Pró-Reitoria de Graduação;
Vanessa Gabriela Saggin - Colégio de Aplicação;
Silvio Cesar Escovar Paiva - Secretaria de Educação à Distância;
 
para,  sob  presidência  do  primeiro,  constituírem  a  Comissão  de  Recursos  da  Análise
Socioeconômica para ingresso em curso de graduação da Universidade Federal do Rio Grande do
Sul, tornando-se sem efeito a partir desta data a Portaria nº 4006 de 09/05/2019.
 
VLADIMIR PINHEIRO DO NASCIMENTO
Pró-Reitor de Graduação
