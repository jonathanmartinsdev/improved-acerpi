Documento gerado sob autenticação Nº QQW.064.266.MAG, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1604                  de  03/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°17896,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, VALESCA DALL ALBA (Siape: 1457834 ),  para substituir  
ZILDA ELISABETH DE ALBUQUERQUE SANTOS (Siape: 1985998 ), Coordenador da COMGRAD de Nutrição,
Código FUC,  em seu afastamento do país,  no período de 26/02/2016 a 28/02/2016,  com o decorrente
pagamento das vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
