Documento gerado sob autenticação Nº CTN.329.039.2NR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             681                  de  24/01/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, , e conforme a Solicitação de Férias n°33790,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PEDAGOGO-ÁREA, do Quadro de
Pessoal desta Universidade, JULIANA VEIGA DE FREITAS (Siape: 2069316 ),  para substituir   IARA BAHY DE
OLIVEIRA (Siape: 0358066 ),  Coordenador do Núcleo de Apoio Acadêmico da Gerência Administrativa da
Faculdade de Educação, Código FG-5, em seu afastamento por motivo de férias, no período de 16/01/2017 a
25/01/2017, com o decorrente pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
