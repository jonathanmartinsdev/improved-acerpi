Documento gerado sob autenticação Nº KNM.452.468.4K9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3458                  de  23/04/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de WAGNER VIANA BIELEFELDT, Professor do Magistério Superior,
lotado e  em exercício  no  Departamento  de  Metalurgia  da  Escola  de  Engenharia,  com a  finalidade de
participar do "AISTech 2019 - The Iron & Steel Technology Conference and Exposition" e realizar visita à
Carnegie Mellon University, em Pittsburg, Estados Unidos, no período compreendido entre 05/05/2019 e
10/05/2019, incluído trânsito, com ônus limitado. Solicitação nº 83584.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
