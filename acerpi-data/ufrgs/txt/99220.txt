Documento gerado sob autenticação Nº DMI.381.049.B1P, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9138                  de  08/10/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.519415/2019-72  e
23078.516307/2018-67, do Pregão nº 131/2018, Ata de registro de preços nº 280/2018, da Lei nº 10.520/02 e
ainda, da Lei nº 8.666/93,
            RESOLVE:
 
           Aplicar a sanção administrativa de MULTA de 5% (cinco por cento) sobre o valor da parcela que lhe deu
causa,  no montante de R$ 459,79 (quatrocentos e cinquenta e nove reais  e setenta e nove centavos),
conforme demonstrativo de cálculo (Doc. SEI nº  1822348), prevista no inciso II, item 16.1.3 do Termo de
Referência, à Empresa LLED SOLUÇÕES INSTALAÇÕES E REFORMAS LTDA, CNPJ n.º  11.885.366/0001-01,
pelo motivo de atraso na execução do serviço, conforme atestado pela fiscalização do contrato (Doc. SEI
nº  1698999,  1726620  e  1821442),  bem  como  pelo  NUDECON  (Doc.  SEI  nº  1820498)  do  processo
23078.519415/2019-72.
 
            Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
