Documento gerado sob autenticação Nº ZYZ.080.530.9MT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5805                  de  02/08/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir da data deste ato, como segue:
 
Transformar: Secretário da Comgrad de Medicina, código SRH 1064, código FG-7,  em Coordenador
do Núcleo Técnico Científico da GA da Faculdade de Medicina, código SRH 1064, código FG-7.
 
 
Processo nº 23078.518944/2018-78.
 
RUI VICENTE OPPERMANN
Reitor.
