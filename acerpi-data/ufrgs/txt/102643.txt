Documento gerado sob autenticação Nº XNS.204.756.47M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             11367                  de  20/12/2019
  Nomeação  dos  integrantes  do  Conselho
Diretor  do  Centro  de  Microscopia  e
Microanálise.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, de
acordo com o disposto no Art.  5º da Decisão nº 251/2015 - CONSUN, Regimento Interno do Centro de
Microscopia e Microanálise, e tendo em vista o que consta no Processo SEI nº 23078.533089/2019-15,
RESOLVE:
              Nomear os professores abaixo relacionados, para integrarem o Conselho Diretor do Centro de
Microscopia e Microanálise, com mandato de 2 (dois) anos:
 
 
Instituto de Física
Titular: Milton André Tumelero
Suplente: Jonder Morais
 
Instituto de Química
Titular: Marcos José Leites Santos
Suplente: Daniel Eduardo Weibel
 
Instituto de Biociências
Titular: Jorge Ernesto de Araujo Mariath
Suplente: Rinaldo Pires dos Santos
 
Instituto de Ciências Básicas da Saúde
Titular: Daniel Pens Gelain
Suplente: Lenir Orlandi Pereira Silva
 
Instituto de Geociências
Titular: Márcia Elisa Boscato Gomes
Suplente: Carla Cristine Porcher
Documento gerado sob autenticação Nº XNS.204.756.47M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
 
Escola de Engenharia
Titular: Paulo Fernando Papaleo Fichtner
Suplente: Annelise Kopp Alves
 
Faculdade de Agronomia
Titular: Enilson Luiz Saccol de Sá
Suplente: Renar João Bender
 
Faculdade de Veterinária
Titular: Mary Jane Tweedie de Mattos Gomes
Suplente: João Fábio Soares
 
Faculdade de Odontologia
Titular: Vicente Castelo Branco Leitune
Suplente: Fernanda Visioli
 
Faculdade de Medicina
Titular:Rafael Nazario Bringhenti
Suplente: Rubia Denise Ruppenthal
 
Faculdade de Farmácia
Titular: Letícia Scherer Koester
Suplente: Karina Paese
 
Instituto de Informática
Titular: Marcelo de Oliveira Johann
Suplente: Fernanda Gusmão de Lima Kastensmidt
RUI VICENTE OPPERMANN,
Reitor.
