Documento gerado sob autenticação Nº HSE.559.279.BTQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2478                  de  04/04/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
         Autorizar o afastamento no País de ROSELENE RICACHENEVSKY GURSKI, ocupante do cargo de
Professor do Magistério Superior,  lotada e com exercício no Departamento de Psicanálise e Psicopatologia,
com a finalidade de realizar estudos em nível de Pós-Doutorado, junto à Universidade de São Paulo, no
período compreendido entre 01/05/2018 e 12/08/2018, com ônus limitado. Processo 23078.503977/2018-13.
JANE FRAGA TUTIKIAN.
Vice-Reitora.
