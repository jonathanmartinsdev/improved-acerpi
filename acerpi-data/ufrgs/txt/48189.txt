Documento gerado sob autenticação Nº MRX.562.577.CLV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11201                  de  14/12/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°38819,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CYNTHIA FEIJO SEGATTO (Siape: 1214796 ),  para
substituir   CARLOS HOPPEN (Siape: 1769606 ), Coordenador do PPG em Matemática Aplicada, Código FUC,
em seu afastamento por motivo de férias,  no período de 22/12/2017 a 02/01/2018,  com o decorrente
pagamento das vantagens por 12 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
