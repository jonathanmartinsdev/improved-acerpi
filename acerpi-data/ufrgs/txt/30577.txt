Documento gerado sob autenticação Nº IJV.906.268.T1J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9223                  de  18/11/2016
O SUPERINTENDENTE DE INFRAESTRTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7687, de 03 de outubro de 2016
Reinstaurar a Sindicância investigativa a ser processada pela
Comissão integrada pelos servidores ADRIANO DE FRAGA ERREIRA,
Chefe do Setor de Atualização e Controle de Cadastros, EDUARDO
ROSA  DA  SILVA,  Chefe  do  Departamento  de  Adequações  de
Infraestrutura e EDUARDO DOS SANTOS WILGES, engenheiro-área,
lotados  na  Superintendência  de  Infraestrutura,  para  sob  a
presidência  do  primeiro,  apurar  no  prazo  de  30  (trinta)  dias  a
responsabilidade pelos defeitos de construção do prédio de Salas
de Aula, localizado no quarteirão 2 do Campus Centro, processo
UFRGS 23078.021248/2015-09.
EDY ISAIAS JUNIOR
Superintendente de Infraestrtura
