Documento gerado sob autenticação Nº KGL.895.311.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5910                  de  07/08/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Lotar no Departamento de Ciências Contábeis e Atuariais da Faculdade de Ciências Econômicas,
área de Ciências Contábeis, a partir de 03/07/2018, EDILSON PAULO, matrícula SIAPE n° 1286518, ocupante
do cargo de Professor do Magistério Superior, classe Associado, redistribuído conforme Portaria nº 1.111 de
05 de junho de 2018, do Ministério da Educação, publicada no Diário Oficial da União de 06 de junho de 2018.
Processo nº 23078.002240/2018-88.
RUI VICENTE OPPERMANN
Reitor.
