Documento gerado sob autenticação Nº QTV.149.690.FL7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8346                  de  06/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  15/08/2017,   referente  ao  interstício  de
11/02/2016  a  14/08/2017,  para  a  servidora  ALESSANDRA DE OLIVEIRA PETRY,  ocupante  do  cargo  de
Relações Públicas - 701072, matrícula SIAPE 2143484,  lotada  na  Assessoria de Comunicação Social da
Faculdade de Educação,  passando do Nível  de Classificação/Nível  de Capacitação E  II,  para o  Nível  de
Classificação/Nível de Capacitação E III,  em virtude de ter realizado o(s) seguinte(s) curso(s),  conforme o
Processo nº 23078.513166/2017-40:
Formação Integral de Servidores da UFRGS IV CH: 116 Carga horária utilizada: 95 hora(s) / Carga horária
excedente: 21 hora(s) (03/10/2014 a 26/08/2016)
Data Lógica - HTML, JAVASCRIPT e Lógica, DREAMWEAVER CS5, FIREWORKS CS5, FLASH Básico e Avançado CH:
55 (18/09/2014 a 29/08/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
