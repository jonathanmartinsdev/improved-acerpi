Documento gerado sob autenticação Nº KDW.286.937.4BD, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1961                  de  14/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de ROBERTO GIUGLIANI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de participar do
"13th  International  Congress  of  Human  Genetics",  em  Kyoto,  Japão,  no  período  compreendido  entre
02/04/2016 e 08/04/2016, incluído trânsito, com ônus limitado. Solicitação nº 18402.
CARLOS ALEXANDRE NETTO
Reitor
