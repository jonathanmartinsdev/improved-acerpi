Documento gerado sob autenticação Nº BIQ.913.642.NQ4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             220                  de  08/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/12/2018,   referente  ao  interstício  de
26/06/2017 a 25/12/2018, para a servidora SUZETE BRUM GUIMARAES, ocupante do cargo de Técnico em
Higiene Dental - 701241, matrícula SIAPE 2405939,  lotada  na  Faculdade de Odontologia, passando do Nível
de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.532852/2018-09:
Formação Integral de Servidores da UFRGS IV CH: 100 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 10 hora(s) (23/08/2017 a 31/08/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
