Documento gerado sob autenticação Nº PXY.583.644.CJH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3964                  de  29/05/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias,
RESOLVE
Retificar a Portaria n° 3838/2018, de 25/05/2018, que designa BASILIO ALBERTO SARTOR, Professor
do Magistério Superior, com exercício no Departamento de Comunicação da Faculdade de Biblioteconomia e
Comunicação, para exercer a função de Coordenador Substituto  da COMGRAD em Jornalismo. Processo nº
23078.510744/2018-77.
 
Onde se lê:
"...no período de 27/05/2018 até 26/05/2020."
leia-se:
"... na vigência do presente mandato.", ficando ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora.
