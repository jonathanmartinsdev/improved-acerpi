Documento gerado sob autenticação Nº GNW.626.779.B9C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2320                  de  27/03/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/03/2018,   referente  ao  interstício  de
01/03/2005 a 15/03/2018, para a servidora CIRILA DOS SANTOS FERREIRA DA CRUZ, ocupante do cargo de
Recepcionista - 701459, matrícula SIAPE 0357556,  lotada  na  Secretaria de Comunicação Social, passando
do Nível de Classificação/Nível de Capacitação C II, para o Nível de Classificação/Nível de Capacitação C III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.505406/2018-13:
ENAP - Sistema Eletrônico de Informações - SEI CH: 20 (04/10/2016 a 24/10/2016)
ABELINE - Informática Aplicada às Artes CH: 80 Carga horária utilizada: 70 hora(s) / Carga horária excedente:
10 hora(s) (10/02/2018 a 09/03/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
