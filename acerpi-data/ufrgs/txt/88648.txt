Documento gerado sob autenticação Nº CYU.055.378.89Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2570                  de  21/03/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  do(s)  Processo(s)  Administrativo(s)  n°  23078.502615/2018-13  e
23078.500690/2019-12, da Lei 10.520/02, do Pregão Eletrônico SRP Nº 040/2018 e ainda, da Lei 8.666/93,
RESOLVE:
 
Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 29.1.1 do Edital de Licitação, à Empresa
PROTEPAR - AR CONDICIONADO LTDA - ME,  CNPJ nº 08.606.524/0001-32,  pelo atraso na correção de
serviço coberto pela Garantia, conforme atestado pela fiscalização (Doc. SEI nº 1399353 e 1416892), bem
como pelo NUDECON (Doc. SEI nº 1486804) do processo administrativo 23078.500690/2019-12.
 
 Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
