Documento gerado sob autenticação Nº XDD.706.855.AHV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9940                  de  27/10/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder pensão com natureza vitalícia, a partir de 26 de setembro de 2017, a GUIOMAR REGINA
LUCAS DE OLIVEIRA, nos termos dos artigos 215 e 217, inciso III da Lei n.º 8.112, de 11 de dezembro de 1990,
combinado com o artigo 40 da Constituição Federal de 1988, alterado pela Emenda Constitucional 41 de
2003, regulamentado pela Lei nº 10.887/2004, artigo 2º, I, em decorrência do falecimento de PAULO CABRAL
CASTILHOS, matrícula SIAPE n° 0357812, aposentado no cargo de Auxiliar de Mecânica do quadro de pessoal
desta Universidade. Processo n.º 23078.018817/2017-92.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
