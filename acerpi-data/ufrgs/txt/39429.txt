Documento gerado sob autenticação Nº JYK.124.997.JT4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5123                  de  12/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  08/06/2017,   referente  ao  interstício  de
01/03/2005 a 07/06/2017, para o servidor SERGIO SILVA RODRIGUES,  ocupante do cargo de Pedreiro -
701649,  matrícula SIAPE 0355334,  lotado  no  Instituto de Pesquisas Hidráulicas, passando do Nível de
Classificação/Nível de Capacitação B I, para o Nível de Classificação/Nível de Capacitação B II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.201566/2017-13:
Formação Integral de Servidores da UFRGS II  CH: 42 Carga horária utilizada: 40 hora(s) /  Carga horária
excedente: 2 hora(s) (09/09/2016 a 26/05/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
