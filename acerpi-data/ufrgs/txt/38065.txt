Documento gerado sob autenticação Nº FQL.718.783.7NB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4096                  de  10/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora CAROLINA TAGLIANI RIBEIRO,  ocupante do cargo de  Engenheiro-área -
701031, lotada na Pró-Reitoria de Assuntos Estudantis, SIAPE 2075239, o percentual de 52% (cinquenta e dois
por cento) de Incentivo à Qualificação, a contar de 20/03/2017, tendo em vista a conclusão do curso de
Mestrado em Engenharia de Produção, conforme o Processo nº 23078.003919/2017-11.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
