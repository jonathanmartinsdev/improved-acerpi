Documento gerado sob autenticação Nº BQW.595.917.8VL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1848                  de  25/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a JOCELI
MULLER, matrícula SIAPE nº 1081443, no cargo de Assistente em Administração, nível de classificação D, nível
de capacitação IV, padrão 15, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho,  com  exercício  na  Biblioteca  da  Faculdade  de  Direito,  com  proventos  integrais.  Processo
23078.535348/2018-52.
RUI VICENTE OPPERMANN
Reitor.
