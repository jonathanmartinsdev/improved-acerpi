Documento gerado sob autenticação Nº WOW.930.698.7D9, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2847                  de  15/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder ao servidor OSVALDO MACHADO DOS SANTOS, ocupante do cargo de  Contramestre-
ofício - 701423, lotado no Instituto de Biociências, SIAPE 0358108, o percentual de 15% (quinze por cento) de
Incentivo à Qualificação, a contar de 19/01/2016, tendo em vista a conclusão do curso de Ensino Médio - EJA,
conforme o Processo nº 23078.000879/2016-67.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
