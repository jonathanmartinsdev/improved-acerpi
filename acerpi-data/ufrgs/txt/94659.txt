Documento gerado sob autenticação Nº FXZ.471.119.88P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6288                  de  22/07/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Retificar  a  Portaria  n°  5170/2019,  de  18/06/2019,  que  define  o  Ambiente  Organizacional  da
servidora SARAH GOMES SAKAMOTO, lotada no Instituto de Física, com exercício no Setor de Informática do
Instituto de Física.
 
Onde se lê:
"ocupante do cargo de TÉCNICO DE TECNOLOGIA DA INFORMAÇÃO, classe D, nível I, padrão 101,
Ambiente Organizacional Administrativo",
leia-se:
"ocupante do cargo de TÉCNICO DE TECNOLOGIA DA INFORMAÇÃO, classe D, nível I, padrão 101,
Ambiente Organizacional Ciências Exatas e da Natureza".
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
