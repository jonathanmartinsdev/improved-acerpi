Documento gerado sob autenticação Nº OHT.176.213.057, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             1316                  de  07/02/2020
Nomeação  da  Representação  Discente  dos
órgãos  coleg iados  da  Faculdade  de
Biblioteconomia  e  Comunicação  da  UFRGS
O PRÓ-REITOR DE ASSUNTOS ESTUDANTIS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL,  no uso de suas atribuições,  considerando o disposto na Portaria nº  7625,  de 29 de
setembro de 2016
RESOLVE
Nomear a Representação Discente eleita na Faculdade de Biblioteconomia e Comunicação, com
mandato de 01 (um) ano, a contar de 03 de março de 2020 a 02 de março de 2021, atendendo o disposto
nos artigos 175 do Regimento Geral da Universidade e 79 do Estatuto da Universidade, e considerando o
processo nº 23078.501956/2020-88, conforme segue:
 
Plenária do DCI
Titular: Amanda Dall?Agnol Barbosa 00290176
Titular: Gabriela Meneghel Colla Mattia 00229395
Titular: Lucas Quadros Petry 00292495
Titular: Vi Mazim Dias 00276680
Titular: Sara de Vargas Moraes 00287633
Titular: Matheus Sousa 00290735
 
Conselho de Unidade
Titular: Amanda Dall?Agnol Barbosa   00290176
Suplente: Vi Mazim Dias 00276680
 
Colegiado do DCI
Titular: Amanda Dall?Agnol Barbosa 00290176
 
Núcleo de Avaliação da Unidade
Titular: Vi Mazim Dias 00276680
 
Comissão de Pesquisa
Titular: Veridiane Gritzenco Caetano 00287620
Documento gerado sob autenticação Nº OHT.176.213.057, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
 
Comissão de Extensão
Titular: Lucas Quadros Petry 00292495
 
Comissão de Graduação Arquivologia
Titular: Lucas Quadros Petry 00292495
 
Comissão de Graduação Biblioteconomia
Titular: Amanda Dall?Agnol Barbosa 00290176
 
Comissão de Graduação Museologia
Titular: Gabriela Meneghel Colla Mattia 00229395
 
Comissão da Biblioteca
Titular: Sara de Vargas Moraes 00287633
ELTON LUIS BERNARDI CAMPANARO
Pró-Reitor de Assuntos Estudantis em exercício
