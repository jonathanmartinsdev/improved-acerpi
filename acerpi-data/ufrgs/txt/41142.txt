Documento gerado sob autenticação Nº XTN.073.835.MSH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6316                  de  17/07/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°30914,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ENGENHEIRO-ÁREA, do Quadro
de Pessoal desta Universidade, REGINALDO DOS SANTOS LOPES (Siape: 2260492 ),  para substituir   PAULO
HENRIQUE DOS SANTOS PACHECO (Siape: 1872628 ), Prefeito do Campus do Vale da Vice-Superintendêcia de
Manutenção da SUINFRA, Código CD-4, em seu afastamento por motivo de férias, no período de 17/07/2017
a 03/08/2017, com o decorrente pagamento das vantagens por 18 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
