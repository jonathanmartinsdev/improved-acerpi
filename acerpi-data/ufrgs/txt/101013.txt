Documento gerado sob autenticação Nº WCV.666.741.3N1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10341                  de  14/11/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar na Faculdade de Veterinária, com exercício no Núcleo Administrativo e de Gestão de Pessoas
da Faculdade de Veterinária, Ambiente Organizacional Administrativo, JONAS ESCOUTO DA SILVA, nomeado
conforme Portaria Nº 9424/2019 de 16 de outubro de 2019, publicada no Diário Oficial da União no dia 17 de
outubro de 2019, em efetivo exercício desde 12 de novembro de 2019, ocupante do cargo de Assitente em
Administração,  classe  D,  nível  I,  padrão  101,  no  Quadro  de  Pessoal  desta  Universidade.  Processo  n°
23078.530983/2019-24.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
