Documento gerado sob autenticação Nº YEC.525.471.HRC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4389                  de  15/06/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLAUDIA LIMA MARQUES, Professor do Magistério Superior,
lotada no Departamento de Direito Público e Filosofia do Direito da Faculdade de Direito e com exercício no
Programa de Pós-Graduação em Direito,  com a finalidade de participar de curso junto à Université du
Québec à Montréal, Canadá, no período compreendido entre 03/07/2016 e 10/07/2016, incluído trânsito, com
ônus limitado. Solicitação nº 20675.
CARLOS ALEXANDRE NETTO
Reitor
