Documento gerado sob autenticação Nº ZJF.099.157.TSE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4502                  de  23/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder prorrogação de afastamento parcial,  no período de 14/06/2019 a 30/04/2020, para a
servidora ANA LUCIA AVILA XAVIER, ocupante do cargo de Auxiliar de Laboratório - 701619, matrícula
SIAPE 1106063,  lotada  na  Faculdade de Farmácia, para cursar o Mestrado em Ciências Farmacêuticas,
oferecido pela UFRGS; conforme o Processo nº 23078.506690/2018-45.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
