Documento gerado sob autenticação Nº AJQ.388.749.U7L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10874                  de  30/11/2017
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7793, de 03 de outubro de 2016
RESOLVE
Designar os servidores  REGINALDO DOS SANTOS LOPES, ocupante do cargo de Engenheiro-área,
lotado na Superintendência de Infraestrutura e com exercício na Prefeitura Campus do Vale, para fiscalizar os
serviços dos seguintes empenhos:
2017NE808244 - Processo SEI 23078.519738/2017-02
2017NE808520 - Processo SEI 23078.519745/2017-04
2017NE808245 - Processo SEI 23078.519727/2017-14
2017NE808194 - Processo SEI 23078.519743/2017-15
2017NE808483 - Processo SEI 23078.519718/2017-23
2017NE808195 - Processo SEI 23078.519744/2017-51
2017NE808487 - Processo SEI 23078.519823/2017-62
               Os prazos de execução deverão ser atendidos de acordo com a data de cada empenho
ANDREA PINTO LOGUERCIO
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
