Documento gerado sob autenticação Nº URR.323.112.QVV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3617                  de  17/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Retificar a Portaria n° 3239/2018, de 03/05/2018, que concedeu autorização para afastamento do
País  a  ANDRE  ROLIM  BEHR,  Técnico  de  Tecnologia  da  Informação,  com  exercício  na  Divisão  de
Desenvolvimento de Software do Centro de Processamento de Dados
Onde se lê: no período compreendido entre 03/06/2018 e 08/06/2018, incluído trânsito,
 leia-se:  no  período  compreendido  entre  02/06/2018  e  09/06/2018,  incluído  trânsito,  ficando
ratificados os demais termos. Solicitação nº 46322.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
