Documento gerado sob autenticação Nº PPJ.058.427.HFM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3570                  de  26/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ANDREA LUCIA CORSO, matrícula SIAPE n° 1216744, lotada e em exercício no Departamento de
Pediatria da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 01, para a classe C  de Professor
Adjunto, nível 02, referente ao interstício de 21/03/2017 a 20/03/2019, com vigência financeira a partir de
21/03/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a
Decisão nº 331/2017 do CONSUN. Processo nº 23078.500454/2019-04.
RUI VICENTE OPPERMANN
Reitor.
