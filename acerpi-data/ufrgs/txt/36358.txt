Documento gerado sob autenticação Nº FEQ.920.851.UTV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2894                  de  03/04/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Retificar  a  Portaria  n°  3455/2016,  de  10/05/2016,  publicada  no  Diário  Oficial  da  União  de
11/05/2016, que Designou ROGER PIZZATO NUNES, Professor do Magistério Superior, como Coordenador
da COMGRAD de Engenharia Elétrica, Código FUC. Processo nº 23078.006822/2016-71.
 
 
Onde se lê: 
"(...) com vigência a partir da data de publicação no Diário Oficial da União, pelo período de 02
anos."
leia-se:
"(...)  com vigência a partir  de 11/05/2016 e até 23/04/2017, a fim de completar o mandato de
Alexandre Balbinot, conforme artigo 92 do Estatuto da mesma Universidade," ficando ratificados os demais
termos. 
JANE FRAGA TUTIKIAN
Vice-Reitora.
