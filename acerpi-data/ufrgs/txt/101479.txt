Documento gerado sob autenticação Nº UZB.252.622.K0O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10641                  de  27/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do País  de VALERIA NETO DE OLIVEIRA MONARETTO,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Linguística, Filologia e Teoria Literária do
Instituto  de  Letras,  com  a  finalidade  de  participar  do  "IV  Seminário  Internacional  História  e  Língua  -
Interfaces:  Linguagem médica  no  século  XVIII:  corpus  e  Humanidades  Digitais"  e  de  reuniões  junto  à
Universidade de Évora,  em Évora,  Portugal,  no período compreendido entre  03/12/2019 e  14/12/2019,
incluído trânsito, com ônus para a Fundação de Amparo à pesquisa do Estado do RS. Solicitação nº 88522.
RUI VICENTE OPPERMANN
Reitor
