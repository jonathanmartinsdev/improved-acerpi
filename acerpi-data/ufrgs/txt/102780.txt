Documento gerado sob autenticação Nº HFK.892.568.NQ8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
DEPARTAMENTO DE DESENVOLVIMENTO E GESTÃO DE PESSOAS
ESCOLA DE DESENVOLVIMENTO DE SERVIDORES DA UFRGS
Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
(51) 3308-3015/3219/3914/4596 - edufrgs@progesp.ufrgs.br
Processo n°: 23078.519662/2018-98
Servidora: THAIS ESTHER TEIXEIRA NUNES
Cargo: Técnico de Laboratório Área
Lotação: Instituto de Ciências Básicas da Saúde
Ambiente Organizacional: Ciências Biológicas
Nível de Classificação e Nível de Capacitação: D IV
PARECER N° 1521/2019
Trata este expediente da retificação do Parecer n° 992/2019, de 05/08/2019, que concede horário
especial  para servidor estudante à THAIS ESTHER TEIXEIRA NUNES,  Técnico de Laboratório Área,  com
exercício no Departamento de Bioquímica do Instituto de Ciências Básicas da Saúde, conforme Processo nº
23078.519662/2018-98.
 
Onde se lê:
"para o período de 12/08/2019 a 11/01/2020",
leia-se:
"para o período de 12/08/2019 a 27/12/2019".
Em 26/12/2019.
FERNANDO BORGES DA SILVA
Divisão de Análise e Orientação do Desenvolvimento na Carreira
De acordo. 
Em 26/12/2019.
KAREN WERLANG LUNKES
Diretora da Divisão de Análise e Orientação do Desenvolvimento na Carreira
