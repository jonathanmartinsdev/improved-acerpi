Documento gerado sob autenticação Nº PCA.547.915.N5Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             3119                  de  27/04/2018
               O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, tendo
em vista o resultado das eleições realizadas em 10 e 11 de abril de 2018, de acordo com o Artigo 194 do
Regimento Geral da Universidade,
RESOLVE:
declarar eleitos os abaixo relacionados como representantes dos Docentes junto ao Conselho Universitário -
CONSUN, com mandato de 2 (dois) anos, a partir de 8 de maio de 2018:
 
Titular: LUIS DA CUNHA LAMB
Suplente: CARLOS PEREZ BERGMANN
 
Titular: VLADIMIR PINHEIRO DO NASCIMENTO
Suplente: MARCELO ZUBARAN GOLDANI
 
Titular: SORAYA MARIA VARGAS CORTES
Suplente: CELSO GIANNETTI LOUREIRO CHAVES
 
Titular: MARCIA CRISTINA BERNARDES BARBOSA
Suplente: SIMONE VALDETE DOS SANTOS
 
Titular: JOSE CARLOS FRANTZ
Suplente: JAIRO ALFREDO GENZ BOLTER
 
Titular: PANTELIS VARVAKI RADOS
Suplente: SUZI ALVES CAMEY
 
Titular: JOSE VICENTE TAVARES DOS SANTOS
Suplente: RODRIGO VALIN DE OLIVEIRA
 
Titular: MARIA CECI ARAUJO MISOCZKY
Suplente: MONICA TORRES BONATTO
 
Titular: CARLOS LEONARDO BONTURIM ANTUNES
Suplente: ELISABETE ZARDO BURIGO
 
 
Documento gerado sob autenticação Nº PCA.547.915.N5Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
...cont. Portaria Docentes/CONSUN
 
Titular: FERNANDO HEPP PULGATI
Suplente: MARCIA HELOISA TAVARES DE FIGUEREDO LIMA
 
Titular: MARIA LUIZA SARAIVA PEREIRA
Suplente: CAMILA GIUGLIANI
 
Titular: LILIANE FERRARI GIORDANI
Suplente: ROSANE AZEVEDO NEVES DA SILVA
 
Titular: LILIANA MARIA PASSERINO
Suplente: RENATO VENTURA BAYAN HENRIQUES
 
Titular: DANILO BLANK
Suplente: CARLA SCHWENGBER TEN CATEN
 
Titular: JOAO HENRIQUE CORREA KANAN
Suplente: PAULO BRACK
 
Titular: PEDRO DE ALMEIDA COSTA
Suplente: I JUCA PIRAMA CAMARGO GIL
 
Titular: MARCIA KAUER SANT ANNA
Suplente: ANGELA BORGES MASUERO
 
Titular: JUSSARA MARIA ROSA MENDES
Suplente: ALEXANDRE ROCHA DA SILVA
      
RUI VICENTE OPPERMANN,
Reitor.
