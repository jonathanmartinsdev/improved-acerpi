Documento gerado sob autenticação Nº WDY.491.162.FNS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2404                  de  02/04/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Declarar vaga, a partir de 07/03/2018, a função de Coordenadora da Comissão de Pesquisa da
FABICO,  Código  SRH  878,   desta  Universidade,  tendo  em  vista  o  falecimento  de  LUCIANA  PELLIN
MIELNICZUK,  matrícula  SIAPE  n°  1437415,  conforme  Portaria  n°  2324/2018,  de  27  de  março  de
2018, publicada no Diário Oficial da União do dia 29 de março de 2018. Processo nº 23078.505674/2018-35.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
