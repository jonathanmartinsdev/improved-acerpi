Documento gerado sob autenticação Nº IAZ.057.903.QC5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9975                  de  10/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  JACKSON  DA  SILVA  MEDEIROS,  matrícula  SIAPE  n°  2891750,  lotado  e  em  exercício  no
Departamento de Ciência da Informação da Faculdade de Biblioteconomia e Comunicação, da classe C  de
Professor Adjunto, nível 01, para a classe C  de Professor Adjunto, nível 02, referente ao interstício de
12/08/2016 a 11/08/2018, com vigência financeira a partir de 14/11/2018, de acordo com o que dispõe a Lei
12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº
23078.517054/2018-49.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
