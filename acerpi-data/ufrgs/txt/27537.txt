Documento gerado sob autenticação Nº FGR.542.991.JVO, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7166                  de  12/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Homologar  o  parecer  que  aprova  a  servidora  técnico-administrativa  FERNANDA  TAMBOSI
VARELLA, ocupante do cargo de Fisioterapeuta, no estágio probatório cumprido no período de 09/09/2013
até 09/09/2016, fazendo jus, a partir desta data, à estabilidade no serviço público federal.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
