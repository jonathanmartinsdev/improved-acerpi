Documento gerado sob autenticação Nº FCX.640.505.E0Q, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº                            de  
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5253, de 01 de outubro de 2012
RESOLVE:
Retificar a Portaria n° 5619 de 8 de outubro de 2012, que aprova  no Estágio Probatório VALÉRIA
REGINA ABDALLA FARIAS,  Museólogo,  lotada na Faculdade de Biblioteconomia e Comunicação e com
exercício na Gerência Administrativa da Faculdade de Biblioteconomia e Comunicação
,
Onde se lê: 
01 de outubro de 2012,
leia-se:
05 de dezembro de 2012.
Observa-se  que  devido  à  licença(s)  e/ou  afastamento(s)  houve  prolongamento  do  período  do
Estágio Probatório em 65 dias
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
