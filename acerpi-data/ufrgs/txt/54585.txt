Documento gerado sob autenticação Nº DCV.356.521.J7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3750                  de  22/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de LUIS HENRIQUE SANTOS CANANI,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a
finalidade de participar da "78th Scientific Sessions of the  American Diabetes Association", em Orlando,
Estados Unidos,  no período compreendido entre 21/06/2018 e 26/06/2018,  incluído trânsito,  com ônus
UFRGS (pró-Reitoria de Pesquisa - diárias). Solicitação nº 33976.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
