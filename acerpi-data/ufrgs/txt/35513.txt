Documento gerado sob autenticação Nº IQZ.690.281.TUV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2320                  de  15/03/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANDREA BRACHER, Professor do Magistério Superior, lotada e
em exercício no Departamento de Comunicação da Faculdade de Biblioteconomia e Comunicação, com a
finalidade de participar do "VIII  Congresso Internacional CSO'2017 -  Criadores Sobre outras Obras",  em
Lisboa, Portugal,  no período compreendido entre 09/04/2017 e 12/04/2017, incluído trânsito,  com ônus
UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 26619.
RUI VICENTE OPPERMANN
Reitor
