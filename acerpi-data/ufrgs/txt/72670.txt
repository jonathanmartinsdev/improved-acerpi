Documento gerado sob autenticação Nº LTY.438.983.PUV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9586                  de  27/11/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  CESAR  AUGUSTO  BARCELLOS  GUAZZELLI,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de História do Instituto de Filosofia e Ciências
Humanas, com a finalidade de desenvolver estudos em nível de Pós-Doutorado junto à Universidade de
Lisboa,  em  Lisboa,  Portugal,  no  período  compreendido  entre  03/01/2019  e  02/03/2019,  com  ônus
limitado. Processo nº 23078.528237/2018-90.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
