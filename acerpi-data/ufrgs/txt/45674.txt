Documento gerado sob autenticação Nº QYI.866.083.91U, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9343                  de  09/10/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar na Pró-Reitoria de Gestão de Pessoas, com exercício na Divisão de Cadastro e Registros, LAIS
CRISTINA GROSS GERHARDT,  nomeada conforme Portaria  Nº  8654/2017 de 14 de setembro de 2017,
publicada no Diário Oficial da União no dia 18 de setembro de 2017, em efetivo exercício desde  06 de
outubro  de  2017,  ocupante  do  cargo  de  ASSISTENTE  EM  ADMINISTRAÇÃO,  Ambiente  Organizacional
Administrativo, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
