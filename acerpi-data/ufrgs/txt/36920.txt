Documento gerado sob autenticação Nº WWS.605.810.OMJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3272                  de  18/04/2017
Nomeia  os  representantes
discentes  eleitos  no  Curso  de
Nutrição.
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita no Curso de Nutrição, com mandato de 01 (um) ano, a
contar  de  01  de  fevereiro  de   2017,  atendendo ao  disposto  nos  artigos  175  do  Regimento  Geral  da
Universidade  e  79  do  Estatuto  da  Universidade  e  considerando  o  processo  nº  23078.003837/2017-69,
conforme segue:
 
COMISSÃO DE GRADUAÇÃO
Titular Dafne Pavão Schattschneider
Suplente Eduardo Natan Maraschin Klein
Titular Ariádrine Freitas de Oliveira
Suplente Francine dos Santos
Titular Betina Goettems Schneider
Suplente Bianca Fasolo Franceschetto
 
DEPARTAMENTO DE NUTRIÇÃO
Titular Claudio Magalhães Dacier Lobato
Suplente Rayssa Emily Griebeler Queirolo
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
