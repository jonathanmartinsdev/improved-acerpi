Documento gerado sob autenticação Nº TFT.048.885.436, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1130                  de  01/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5068303-42.2018.4.04.7100,  da  2ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, da servidora SILVIA MARIA SECRIERU CIULEI, matrícula SIAPE n° 0355368, ativa no cargo de 
Relações Públicas - 701072, para o nível IV, conforme o Processo nº 23078.501839/2019-81.
RUI VICENTE OPPERMANN
Reitor.
