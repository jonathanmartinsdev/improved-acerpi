Documento gerado sob autenticação Nº ODY.304.503.BPQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3464                  de  10/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de JAQUELINE MOLL, Professor do Magistério Superior, lotada e
em exercício no Departamento de Estudos Especializados da Faculdade de Educação, com a finalidade de
participar do "XIV Congreso Internacional  de Ciudades Educadoras",  em Rosário,  Argentina,  no período
compreendido entre 31/05/2016 e 03/06/2016, incluído trânsito, com ônus limitado. Solicitação nº 19861.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
