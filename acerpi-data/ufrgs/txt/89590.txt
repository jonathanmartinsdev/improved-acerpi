Documento gerado sob autenticação Nº HUH.060.928.LDQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3130                  de  09/04/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Retificar a Portaria n° 6773/2018, de 30/08/2018, que concedeu autorização para afastamento do
País a CAROLINE SILVEIRA BAUER, Professor do Magistério Superior, com exercício no Departamento de
História do Instituto de Filosofia e Ciências Humanas
Onde se lê: com ônus limitado,
leia-se: com ônus CNPq (Proc. nº 204798/2018-3), ficando ratificados os demais termos. Processo nº
23078.517674/2018-88.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
