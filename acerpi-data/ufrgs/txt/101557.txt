Documento gerado sob autenticação Nº CPV.412.863.U2P, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10702                  de  29/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CLARISSA SEVERINO GAMA, Professor do Magistério Superior,
lotada no Departamento de Psiquiatria e Medicina Legal da Faculdade de Medicina e com exercício no
Programa de Pós-Graduação em Psiquiatria e Ciências do Comportamento, com a finalidade de participar
da "58th Annual  Meeting of  the American College of  Neuropsychopharmacology",  em Orlando,  Estados
Unidos, no período compreendido entre 08/12/2019 e 11/12/2019, incluído trânsito, com ônus limitado.
Solicitação nº 88713.
RUI VICENTE OPPERMANN
Reitor
