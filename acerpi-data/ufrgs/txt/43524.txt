Documento gerado sob autenticação Nº CJK.388.854.QMI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7985                  de  25/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CRISTIANE BAUERMANN LEITAO,  Professor do Magistério
Superior,  lotada no Departamento de Medicina  Interna da Faculdade de Medicina  e  com exercício  no
Programa de Pós-Graduação em Ciências Médicas: Endocrinologia, com a finalidade de participar do "53rd
Annual Meeting of the European Association for the Study of Diabetes", em Lisboa, Portugal, no período
compreendido entre 10/09/2017 e 16/09/2017, incluído trânsito, com ônus limitado. Solicitação nº 28938.
RUI VICENTE OPPERMANN
Reitor
