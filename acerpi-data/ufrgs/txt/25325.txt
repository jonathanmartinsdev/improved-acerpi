Documento gerado sob autenticação Nº WXI.435.917.F92, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5752                  de  29/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Nomear, em caráter efetivo, no cargo abaixo, na vaga referente ao respectivo código, em virtude de
habilitação em Concurso Público, conforme Edital Nº 49/15 de 16 de setembro de 2015, homologado em 17
de setembro de 2015 e de acordo com o artigo 9º, item I, da Lei nº. 8.112, de 11 de dezembro de 1990, com
as alterações dadas pela Lei nº. 11.091, de 12 de janeiro de 2005, publicada no Diário Oficial da União de 13
de janeiro de 2005, Decreto nº. 7.232, de 19 de julho de 2010, publicado no Diário Oficial da União de 20 de
julho de 2010, Lei 12.772, de 28 de dezembro de 2012, publicada no Diário Oficial  da União de 31 de
dezembro de 2012. Processo nº 23078.014002/2015-72.
 
CARGO 03 - ASSISTENTE EM ADMINISTRAÇÃO - Classe D - Padrão 1
 
- TAMARA MOCH - Vaga SIAPE 274997
- ALICE KLOTZ - Vaga SIAPE 277429
- THAIS PICCOLI FACCO - Vaga SIAPE 276058
- ISADORA AZUAGA NIETIEDT - Vaga SIAPE 962338
- NICIANE DA ROSA - Vaga SIAPE 275953
- ISABELA DE ALMEIDA RODRIGUES - Vaga SIAPE 290259
CARGO  05  -  TECNICO  EM  TECNOLOGIA  DA  INFORMAÇÃO/ÁREA:  INFRAESTRUTURA-  Classe  D  -
Padrão 1
 
- ALEX RODRIGUES SANTANA - Vaga SIAPE 904276
- MARCELO GOMES MARTINS - Vaga SIAPE 870479
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
