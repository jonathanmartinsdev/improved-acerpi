Documento gerado sob autenticação Nº REH.477.045.9SN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5571                  de  25/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor FERNANDO GERCHMAN, matrícula SIAPE n° 1253779, lotado e em exercício no Departamento de
Medicina Interna da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 01, para a classe C  de
Professor Adjunto, nível 02, referente ao interstício de 25/02/2013 a 24/02/2015, com vigência financeira a
partir de 21/07/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela Decisão nº
401/2013-CONSUN. Processo nº 23078.010752/2015-75.
CARLOS ALEXANDRE NETTO
Reitor
