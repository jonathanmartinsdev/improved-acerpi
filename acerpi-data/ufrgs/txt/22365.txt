Documento gerado sob autenticação Nº HGD.314.472.USD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3677                  de  17/05/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Lotar no Instituto de Letras, com exercício no Núcleo Acadêmico do Instituto de Letras, Veridiano
Koeffender Moreira, nomeado conforme Portaria Nº 2561 de 11 de abril de 2016, publicada no Diário Oficial
da União no dia 13 de abril de 2016, em efetivo exercício desde 12 de maio de 2016, ocupante do cargo de
Técnico em Assuntos Educacionais, Ambiente Organizacional Administrativo, classe E, nível I, padrão I, no
Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
