Documento gerado sob autenticação Nº PPJ.828.627.J5S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5520                  de  25/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  23/07/2018,   referente  ao  interstício  de
16/01/2017 a 22/07/2018, para a servidora LUCIANA DENISE FLORES, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2081417,  lotada  na  Procuradoria Geral, passando do Nível de
Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.516820/2018-58:
Formação Integral de Servidores da UFRGS I CH: 30 (24/08/2016 a 16/11/2016)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 40 (05/01/2016 a 29/02/2016)
ILB - Deveres, proibições e responsabilidades do servidor público federal CH: 43 (04/08/2016 a 03/10/2016)
ENAP  -  Gestão  da  Informação  e  Documentação  -  Conceitos  Básicos  em  Gestão  Documental  CH:  20
(03/05/2016 a 23/05/2016)
ENAP - SEI! USAR CH: 20 Carga horária utilizada: 17 hora(s) / Carga horária excedente: 3 hora(s) (02/07/2018 a
23/07/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
