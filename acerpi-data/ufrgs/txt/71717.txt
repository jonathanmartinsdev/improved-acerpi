Documento gerado sob autenticação Nº TQM.029.182.T82, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8895                  de  01/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°44398,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS, do Quadro de Pessoal desta Universidade, DÉBORA SIMÕES DA SILVA RIBEIRO (Siape:
2174717 ),   para  substituir    RITA DE CASSIA  DOS SANTOS CAMISOLAO (Siape:  0359355 ),  Diretor  do
Departamento de Educação e Desenvolvimento Social da PROREXT, Código CD-4, em seu afastamento por
motivo de férias, no período de 05/11/2018 a 14/11/2018, com o decorrente pagamento das vantagens por
10 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
