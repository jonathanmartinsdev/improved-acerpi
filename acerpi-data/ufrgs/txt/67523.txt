Documento gerado sob autenticação Nº VCD.446.465.TNI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5897                  de  06/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  KAREN DANTUR BATISTA CHAVES,  matrícula  SIAPE  n°  0358822,  lotada  e  em exercício  no
Departamento de Odontologia Conservadora, da classe D  de Professor Associado, nível 01, para a classe D 
de Professor Associado, nível 02, referente ao interstício de 10/03/2016 a 09/06/2018, com vigência financeira
a partir de 28/06/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.514335/2018-40.
JANE FRAGA TUTIKIAN
Vice-Reitora.
