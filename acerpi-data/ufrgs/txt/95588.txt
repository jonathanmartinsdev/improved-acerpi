Documento gerado sob autenticação Nº IXF.412.900.T9H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6981                  de  02/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora LUANA SMANIOTTO, ocupante do cargo de  Assistente em Administração -
701200, lotada na Pró-Reitoria de Graduação, SIAPE 3138927, o percentual de 25% (vinte e cinco por cento)
de Incentivo à Qualificação, a contar de 25/07/2019, tendo em vista a conclusão do curso de Bacharelado em
Administração, conforme o Processo nº 23078.519537/2019-69.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
