Documento gerado sob autenticação Nº JXK.699.196.PM4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1845                  de  06/03/2018
O DECANO DO CONSELHO UNIVERSITÁRIO NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias, e conforme a Solicitação de
Afastamento n°34438,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de Assistente em Administração , do
Quadro de Pessoal desta Universidade, VÂNIA CRISTINA SANTOS PEREIRA (Siape: 6355656 ),  para substituir 
automaticamente  MAURÍCIO VIÉGAS DA SILVA (Siape: 0354315 ), Pró-Reitor de Gestão de Pessoas, Código
CD-2, em seu afastamento no país, no período de 06/03/2018 a 07/03/2018, com o decorrente pagamento
das vantagens por 2 dias.
PHILIPPE OLIVIER ALEXANDRE NAVAUX
Decano do Conselho Universitário no exercício da Reitora
