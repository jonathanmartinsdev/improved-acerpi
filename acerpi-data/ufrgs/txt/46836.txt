Documento gerado sob autenticação Nº ECA.402.011.4B3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10332                  de  08/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar a Portaria n° 8749/2017, de 19/09/2017, que concedeu autorização para afastamento do
País a CARLA CRISTINE PORCHER, Professor do Magistério Superior, com exercício no Departamento de
Geologia do Instituto de Geociências
Onde se lê: com ônus limitado,
leia-se: com ônus UFRGS (Instituto de Geociências - diárias), ficando ratificados os demais termos.
Solicitação nº 32836.
RUI VICENTE OPPERMANN,
Reitor.
