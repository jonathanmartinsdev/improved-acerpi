Documento gerado sob autenticação Nº ZHO.302.111.L2U, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8161                  de  07/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°87073,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO DO
ENSINO  BÁSICO,  TÉCNICO  E  TECNOLÓGICO,  do  Quadro  de  Pessoal  desta  Universidade,  MARLUSA
BENEDETTI DA ROSA (Siape: 2241429 ),  para substituir   FERNANDA BRITTO DA SILVA (Siape: 2728740 ),
Chefe do Depto de Ciências Exatas e da Natureza do Colégio de Aplicação, Código FG-4, em seu afastamento
no país, no período de 06/09/2019 a 08/09/2019, com o decorrente pagamento das vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
