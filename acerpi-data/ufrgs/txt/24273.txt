Documento gerado sob autenticação Nº YYX.687.962.7QR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5012                  de  07/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  04/07/2016,   referente  ao  interstício  de
24/07/2014 a 03/07/2016, para a servidora JAQUELINE OLIVEIRA SILVEIRA, ocupante do cargo de Assistente
em Administração - 701200, matrícula SIAPE 2147018,  lotada  na  Faculdade de Educação, passando do
Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.014380/2016-37:
Formação Integral de Servidores da UFRGS IV CH: 96 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 6 hora(s) (20/08/2014 a 12/11/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
