Documento gerado sob autenticação Nº DTT.331.825.O7I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2655                  de  25/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual  de Incentivo à Qualificação concedido ao servidor SERGIO LUIZ TELLES
BARTEX, ocupante do cargo de Engenheiro-área-701031, lotado na Escola de Engenharia, SIAPE 1860196,
para 75% (setenta e cinco por cento), a contar de 11/12/2018, tendo em vista a conclusão do curso de
Doutorado  em  Engenharia.  Área  Concentração:  Processos  de  Fabricação,  conforme  o  Processo  nº
23078.534363/2018-83.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
