Documento gerado sob autenticação Nº IHP.439.377.MIU, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4759                  de  29/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  HELENA  ARAUJO  RODRIGUES  KANAAN,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Artes Visuais do Instituto de Artes, com a
finalidade de participar de encontro junto à Fundación ACE para el Arte Contemporáneo, em Buenos Aires,
Argentina, no período compreendido entre 04/06/2017 e 10/06/2017, incluído trânsito, com ônus limitado.
Solicitação nº 28163.
RUI VICENTE OPPERMANN
Reitor
