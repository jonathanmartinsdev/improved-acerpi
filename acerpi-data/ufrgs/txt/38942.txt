Documento gerado sob autenticação Nº ELE.162.297.MIU, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4764                  de  29/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  LUCIANA INES  GOMES MIRON,  Professor  do  Magistério
Superior,  lotada e  em exercício  no Departamento de Arquitetura  da  Faculdade de Arquitetura,  com a
finalidade de participar da "25th Annual Conference of the International Group for Lean Construction", em
Creta, Grécia, no período compreendido entre 08/07/2017 e 15/07/2017, incluído trânsito, com ônus UFRGS
(Pró-Reitoria de Pesquisa - diárias). Solicitação nº 28265.
RUI VICENTE OPPERMANN
Reitor
