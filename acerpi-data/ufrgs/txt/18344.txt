Documento gerado sob autenticação Nº WJY.818.153.2T4, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1477                  de  29/02/2016
               O Vice-Reitor DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5148, de 1º de Outubro de 2012.
RESOLVE:
Autorizar o afastamento no país de SERGIO RICARDO DE AZEVEDO SOUZA, ocupante do cargo
de Professor do Ensino Superior,  lotado e em exercício no Departamento de Física do Instituto de Física, 
com  a  finalidade  de  visitas  junto  à  Universidade  Federal  da  Bahia,  em  Salvador,  Brasil,  no  período
compreendido entre 24/02/2016 e 24/03/2016, incluído trânsito, com ônus limitado. Solicitação n° 18114.
RUI VICENTE OPPERMANN
Vice-Reitor
