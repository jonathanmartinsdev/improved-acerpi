Documento gerado sob autenticação Nº ROC.998.849.D52, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3199                  de  12/04/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  13/04/2019,   referente  ao  interstício  de
13/10/2017 a 12/04/2019, para a servidora JOICE FLOR, ocupante do cargo de Técnico em Enfermagem -
701233, matrícula SIAPE 2171157,  lotada  no  Campus Litoral Norte, passando do Nível de Classificação/Nível
de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude de ter realizado o(s)
seguinte(s) curso(s), conforme o Processo nº 23078.508766/2019-58:
Formação Integral de Servidores da UFRGS IV CH: 115 Carga horária utilizada: 108 hora(s) / Carga horária
excedente: 7 hora(s) (27/04/2016 a 17/10/2018)
ENAP - Gestão de riscos no setor público CH: 20 (02/04/2019 a 02/05/2019)
II SECOSAT - Semana da Comissão de Saúde e Ambiente de Trabalho CH: 22 (03/07/2018 a 05/07/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
