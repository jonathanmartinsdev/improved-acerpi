Documento gerado sob autenticação Nº MHZ.811.825.5G5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8308                  de  17/10/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Retificar  a  Portaria  n°  7939/2016,  de  06/10/2016,  publicada  no  Diário  Oficial  da  União  de
14/10/2016, que exonerou CARLOS FERNANDO DA ROSA, Técnico em Edificações, do cargo de Prefeito do
Campus Litoral Norte da Vice-Superintendência de Manutenção da SUINFRA, Código SRH 1324, Código CD-
4. Processo nº 23078.022447/2016-15.
 
 
Onde se lê:
"(...), para o qual foi nomeado pela Portaria nº 1305, de 26/02/2014, publicada no Diário Oficial da
União de 27/02/2014, por ter sido nomeado para outro cargo de direção."
leia-se:
"(...), para o qual foi nomeado pela Portaria nº 3311, de 05/06/2013, publicada no Diário Oficial da
União de 07/06/2013, por ter sido nomeado para outro cargo de direção."
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
