Documento gerado sob autenticação Nº CEH.253.855.R93, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4225                  de  15/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Retificar  a Portaria n° 3017/2019,  de 05/04/2019,  que concedeu Progressão por Capacitação a
ELAINE DOS ANJOS, Assistente em Administração, com exercício na Secretaria da Câmara de Graduação do
Conselho de Ensino, Pesquisa e Extensão, conforme Processo nº 23078.503937/2019-52.
 
               Onde se lê:
"a contar de 04/04/2019, referente ao interstício de 04/09/2017 a 03/04/2019", e
ENAP - Planejamento Estratégico para Organizações Públicas (03/02/2019 a 04/04/2019);
leia-se:
"a contar de 19/02/2019, referente ao interstício de 04/09/2017 a 18/02/2019", e
ENAP - Planejamento Estratégico para Organizações Públicas (03/02/2019 a 19/02/2019).
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
