Documento gerado sob autenticação Nº RAV.505.425.1R1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5143                  de  12/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/07/2016,   referente  ao  interstício  de
12/01/2015 a  11/07/2016,  para o servidor  GUILLERMO CESAR OBANDO MELO,  ocupante do cargo de
Contador - 701015, matrícula SIAPE 2184966,  lotado  na  Superintendência de Infraestrutura, passando do
Nível de Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.012129/2016-38:
Formação Integral de Servidores da UFRGS V CH: 128 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 8 hora(s) (05/05/2015 a 09/05/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
