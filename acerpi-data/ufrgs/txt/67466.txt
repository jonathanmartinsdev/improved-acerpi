Documento gerado sob autenticação Nº ZWD.187.942.6FE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5860                  de  06/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  GRISELDA  LIGIA  BARRERA  DE  GALLAND,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Química Orgânica do Instituto de Química,
com a finalidade de realizar visita  ao Consejo Superior de Investigaciones Cientificas, em Madrid, com ônus
CNPq (Bolsa de Produtividade em Pesquisa) e do "Graphene Week 2018", em San Sebastian, Espanha,  com
ônus UFRGS (Pró-Reitoria de Pesquisa - diárias), no período compreendido entre 01/09/2018 e 16/09/2018,
incluído trânsito. Solicitação nº 47176.
RUI VICENTE OPPERMANN
Reitor
