Documento gerado sob autenticação Nº YNP.198.297.2O6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5195                  de  13/07/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de INGRID ELEONORA SCHREIBER JANSCH PORTO, Professor do
Magistério Superior, lotada no Departamento de Informática Aplicada do Instituto de Informática e com
exercício no Centro de Empreendimentos em Informática, com a finalidade de participar do "Ecuadorand
Latin American Countries Workshop on Business Incubator Planning, Construction and Administration", em
Shanghai,  China,  no período compreendido entre 12/08/2016 e 28/08/2016, incluído trânsito,  com ônus
UFRGS/FAURGS (convênio 8112-4). Solicitação nº 20494.
CARLOS ALEXANDRE NETTO
Reitor
