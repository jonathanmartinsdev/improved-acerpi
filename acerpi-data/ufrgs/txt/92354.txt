Documento gerado sob autenticação Nº ZAP.577.691.PLO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4846                  de  06/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Tornar insubsistente a Portaria nº 4233/2019, de 15/05/2019, publicada no Diário Oficial da União
de 17/05/2019, que concedeu autorização para afastamento do País a MARIA EUNICE DE SOUZA MACIEL,
ocupante  do  cargo  de  Professor  do  Magistério  Superior,  lotada  e  em  exercício  no  Departamento  de
Antropologia do Instituto de Filosofia e Ciências Humanas. Solicitação nº 83388.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
