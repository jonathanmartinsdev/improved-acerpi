Documento gerado sob autenticação Nº OXQ.968.342.61T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             478                  de  16/01/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°47178,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE TECNOLOGIA DA
INFORMAÇÃO, do Quadro de Pessoal desta Universidade, EDUARDO CORRÊA MICHELSEN (Siape: 1106912 ),
 para substituir   MISAEL BASSUALDO CABREIRA (Siape: 2157867 ),  Chefe do Setor de Infraestrutura da
Gerência Administrativa da Escola de Administração, Código FG-5, em seu afastamento por motivo de férias,
no período de 16/01/2019 a 30/01/2019, com o decorrente pagamento das vantagens por 15 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
