Documento gerado sob autenticação Nº TVX.278.558.CVH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8657                  de  26/10/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5059422-76.2018.4.04.7100,  da  6ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, da servidora WANDA AURELIO KNEBEL , matrícula SIAPE n° 0357246, aposentada no cargo de 
Assistente em Administração - 701200, para o nível IV, conforme o Processo nº 23078.528512/2018-75.
RUI VICENTE OPPERMANN
Reitor
