Documento gerado sob autenticação Nº ZZZ.545.616.KKU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1907                  de  02/03/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SANDRO CAMPOS AMICO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia  dos Materiais  da Escola de Engenharia,  com a
finalidade de participar da "NanoWorld Conference", em Boston, Estados Unidos, no período compreendido
entre 01/04/2017 e 07/04/2017, incluído trânsito, com ônus limitado. Solicitação nº 26115.
RUI VICENTE OPPERMANN
Reitor
