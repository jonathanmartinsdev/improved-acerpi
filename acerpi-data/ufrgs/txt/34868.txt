Documento gerado sob autenticação Nº FOV.988.958.C5A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1845                  de  24/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  24/02/2017,   referente  ao  interstício  de
24/08/2015  a  23/02/2017,  para  o  servidor  IVONEI  SOZIO,  ocupante  do  cargo  de  Contador  -  701015,
matrícula SIAPE 1952761,  lotado  no  Campus Litoral Norte, passando do Nível de Classificação/Nível de
Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude de ter realizado o(s)
seguinte(s) curso(s), conforme o Processo nº 23078.001692/2017-61:
Formação Integral de Servidores da UFRGS III CH: 72 (20/10/2015 a 17/11/2016)
ILB - Ética e Administração Pública CH: 40 Carga horária utilizada: 31 hora(s) / Carga horária excedente: 9
hora(s) (23/11/2015 a 17/12/2015)
ILB - Introdução ao Orçamento Público CH: 7 (29/04/2014 a 19/05/2014)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 40 (27/10/2015 a 23/11/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
