Documento gerado sob autenticação Nº SGM.127.777.C6F, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11215                  de  17/12/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5089770-43.2019.4.04.7100,  da  5ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo II da Portaria nº 1874, de
13/07/2006, da servidora IZOLINA GOMES COSTA,  matrícula SIAPE n° 0352899, aposentada no cargo de
Assistente em Administração - 701200, do nível I para o nível IV, a contar de 01/01/2006, conforme o Processo
nº 23078.534661/2019-54.
 
RUI VICENTE OPPERMANN
Reitor
