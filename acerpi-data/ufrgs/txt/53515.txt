Documento gerado sob autenticação Nº WRF.126.993.6FV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2953                  de  23/04/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  do Magistério  do Ensino Básico,  Técnico e  Tecnológico JOCELITO ZALLA,  matrícula  SIAPE n°
1852699, com lotação no Colégio de Aplicação, da classe D III, nível 04, para a classe D IV, nível 01, referente
ao interstício de 04/03/2016 a 03/03/2018, com vigência financeira a partir de 07/03/2018, de acordo com o
que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e a Decisão nº 328/2015 -
CONSUN. Processo nº 23078.524486/2017-25.
PHILIPPE OLIVIER ALEXANDRE NAVAUX
Decano do Conselho Universitário, no exercício da Reitoria.
