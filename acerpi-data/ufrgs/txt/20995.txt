Documento gerado sob autenticação Nº DUO.072.138.NSQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2959                  de  20/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°18504,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, PLINHO FRANCISCO HERTZ (Siape: 1310735 ),  para
substituir   VITOR MANFROI (Siape: 0053868 ), Diretor do ICTA, Código CD-3, em seu afastamento no país, no
dia 11/03/2016, com o decorrente pagamento das vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
