Documento gerado sob autenticação Nº TWR.140.602.ONO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3417                  de  25/04/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar ANA CRISTINA CYPRIANO PEREIRA, matrícula SIAPE n° 2174464, ocupante do cargo de
Professor do Magistério do Ensino Básico, Técnico e Tecnológico, classe D, lotada no Colégio de Aplicação, do
Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Coordenadora da COMGRAD em
Relações Públicas, Código SRH 1503, Código FUC, com vigência a partir da data de publicação no Diário Oficial
da União e até 17/01/2019, a fim de completar o mandato de Monica Pieniz, conforme artigo 92 do Estatuto
da mesma Universidade. Processo nº 23078.005603/2017-56.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
