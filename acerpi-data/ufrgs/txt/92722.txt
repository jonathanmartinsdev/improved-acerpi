Documento gerado sob autenticação Nº GLK.459.347.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5107                  de  14/06/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5029843-49.2019.4.04.7100,  da  6ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874 de
13/07/2006, da servidora LOURDES ARABIAN ZEHLAOUI, matrícula SIAPE n° 0357517, aposentada no cargo
de Bibliotecário-documentalista - 701010, do nível I para o nível IV, a contar de 01/01/2006, conforme o
Processo nº 23078.514773/2019-99.
 
RUI VICENTE OPPERMANN
Reitor
