Documento gerado sob autenticação Nº WPG.216.217.NAA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3863                  de  25/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  ALEJANDRO  GERMAN  FRANK,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Engenharia de Produção e Transportes da Escola de
Engenharia,  com a finalidade de participar da "25th Innovation and Product Development Management
Conference",  na  cidade  do  Porto,  Portugal,  no  período  compreendido  entre  09/06/2018  e  14/06/2018,
incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa -diárias). Solicitação nº 46617.
RUI VICENTE OPPERMANN
Reitor
