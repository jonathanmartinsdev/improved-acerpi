Documento gerado sob autenticação Nº DKV.069.089.K1O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9467                  de  18/10/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°87371,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MICHEL JOSE ANZANELLO (Siape: 2449508 ),  para
substituir   ALEJANDRO GERMAN FRANK (Siape: 2693757 ), Coordenador do PPG em Engenharia de Produção,
Código FUC,  em seu afastamento do país,  no período de 20/10/2019 a 27/10/2019,  com o decorrente
pagamento das vantagens por 8 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
