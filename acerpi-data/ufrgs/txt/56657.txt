Documento gerado sob autenticação Nº UOF.396.560.J7C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5336                  de  19/07/2018
Altera a composição da Comissão Especial  para
revisão  da  Decisão  n°  446/2014-CONSUN,  que
trata das Normas de Concurso Público de Provas e
Títulos  para  provimento  de  cargo  no  primeiro
nível de vencimento da Classe A na Carreira de
Magistério  Superior  na Universidade Federal  do
Rio Grande do Sul.
 
O  REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no  uso de  suas  atribuições  legais  e
estatutárias, em consonância com a Decisão nº 178/2018 do Conselho Universitário
 
RESOLVE
Alterar a composição da Comissão Especial para revisão da Decisão n° 446/2014-CONSUN, que trata das
Normas de Concurso Público de Provas e Títulos para provimento de cargo no primeiro nível de vencimento
da Classe A na Carreira de Magistério Superior na Universidade Federal do Rio Grande do Sul, estabelecida
na Portaria nº 2832, de 17 de abril de 2018, que passa a vigorar com os seguintes membros:
 
JOÃO CESAR NETTO - Presidente
CARLOS HENRIQUE VASCONCELLOS HORN
ILMA SIMONI BRUM DA SILVA
JUSSARA MARIA ROSA MENDES
MARCELO ZUBARAN GOLDANI
RAFAEL BERBIGIER DE BORTOLI e
GABRIELA SILVEIRA DA SILVA
RUI VICENTE OPPERMANN
Reitor.
