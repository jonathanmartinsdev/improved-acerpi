Documento gerado sob autenticação Nº FZS.694.641.P69, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             39                  de  02/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  EDERSON STAUDT,  matrícula  SIAPE n°  1895505,  lotado no Departamento Interdisciplinar  do
Campus Litoral Norte e com exercício no Mestrado Nacional Profissional em Ensino de Física, da classe C  de
Professor Adjunto, nível 02, para a classe C  de Professor Adjunto, nível 03, referente ao interstício de
19/10/2013  a  22/10/2015,  com  vigência  financeira  a  partir  de  23/10/2015,  conforme  decisão  judicial
proferida no processo nº 5054491-30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o
que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do
CONSUN. Processo nº 23078.525384/2019-99.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
