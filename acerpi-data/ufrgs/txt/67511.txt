Documento gerado sob autenticação Nº YNU.440.361.TNI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5893                  de  06/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  CESAR BASTOS DE MATTOS VIEIRA,  matrícula  SIAPE  n°  2212020,  lotado e  em exercício  no
Departamento de Arquitetura da Faculdade de Arquitetura, da classe C  de Professor Adjunto, nível 03, para a
classe C  de Professor Adjunto, nível 04, referente ao interstício de 06/06/2016 a 05/06/2018, com vigência
financeira a partir de 08/06/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.510669/2018-44.
JANE FRAGA TUTIKIAN
Vice-Reitora.
