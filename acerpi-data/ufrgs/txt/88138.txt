Documento gerado sob autenticação Nº JHM.740.961.855, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2288                  de  13/03/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ALVARO REISCHAK DE OLIVEIRA,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Educação Física, Fisioterapia e Dança da Escola de
Educação Física, Fisioterapia e Dança, com a finalidade de participar do "Experimental Biology 2019", em
Orlando, Estados Unidos, no período compreendido entre 04/04/2019 e 10/04/2019, incluído trânsito, com
ônus limitado. Solicitação nº 62506.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
