Documento gerado sob autenticação Nº ULL.916.362.V79, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5012                  de  12/06/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar no Instituto de Biociências, com exercício na Comissão de Pós-Graduação em Genética e
Biologia Molecular, GABRIEL DORNELES SOARES, nomeado conforme Portaria Nº 4258/2019 de 16 de maio
de 2019, publicada no Diário Oficial da União no dia 17 de maio de 2019, em efetivo exercício desde 10 de
junho de 2019,  ocupante do cargo de ASSISTENTE EM ADMINISTRAÇÃO,  classe D,  nível  I,  padrão 101,
Ambiente  Organizacional  Administrativo,  no  Quadro  de  Pessoal  desta  Universidade.  Processo  n°
23078.515036/2019-11.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
