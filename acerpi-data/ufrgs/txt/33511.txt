Documento gerado sob autenticação Nº RRG.403.451.CUN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             809                  de  25/01/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°25333,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO EM CONTABILIDADE,
do Quadro de Pessoal desta Universidade, FRANCISCO ANISIO MOREIRA SILVA (Siape: 2157552 ),  para
substituir   GERSON ANDRADE DA SILVA (Siape: 2143904 ), Diretor da Divisão de Execução Financeira do DARE
da PROREXT, Código FG-5, em seu afastamento por motivo de férias, no período de 16/01/2017 a 26/01/2017,
com o decorrente pagamento das vantagens por 11 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
