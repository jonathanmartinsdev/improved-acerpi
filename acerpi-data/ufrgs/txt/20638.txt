Documento gerado sob autenticação Nº EOY.717.864.BV6, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2899                  de  18/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  01/02/2016,
correspondente ao grau Insalubridade Máxima, ao servidor LOUIDI LAUER ALBORNOZ, Identificação Única
20270011,  Técnico  de  Laboratório  Área,  com  exercício  no  Núcleo  de  Apoio  Técnico  da  Gerência
Administrativa do Instituto de Pesquisas Hidráulicas, observando-se o disposto na Lei nº 8.112, de 11 de
dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas
consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.201145/2016-01, Código
SRH n° 22772 e Código SIAPE 2016001139.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
