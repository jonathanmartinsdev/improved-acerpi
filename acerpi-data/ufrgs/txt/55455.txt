Documento gerado sob autenticação Nº KDP.515.756.GUK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4439                  de  19/06/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor MARCELO GOMES MARTINS, ocupante do cargo de  Técnico de Tecnologia da
Informação - 701226, lotado no Campus Litoral Norte, SIAPE 2334987, o percentual de 25% (vinte e cinco por
cento)  de  Incentivo  à  Qualificação,  a  contar  de  26/02/2018,  tendo  em vista  a  conclusão  do  curso  de
Graduação  em  Tecnologia  em  Análise  e  Desenvolvimento  de  Sistemas,  conforme  o  Processo  nº
23078.503436/2018-95.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
