Documento gerado sob autenticação Nº FOX.946.012.C19, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9720                  de  25/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de EDUARDO CESAR TONDO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ciências dos Alimentos do Instituto de Ciências e Tecnologia de
Alimentos, com a finalidade de participar de reunião junto à Codex Alimentarius, em Cleveland, Estados
Unidos, no período compreendido entre 01/11/2019 e 10/11/2019, incluído trânsito, com ônus limitado.
Solicitação nº 86447.
RUI VICENTE OPPERMANN
Reitor
