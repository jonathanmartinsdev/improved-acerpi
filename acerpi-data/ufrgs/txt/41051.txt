Documento gerado sob autenticação Nº CLS.080.427.UUG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6261                  de  13/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder o Adicional de Insalubridade, no percentual de 10%, a partir de guilherme 20/05/2017,
correspondente  ao  grau  Insalubridade  Média,  ao  servidor  GUILHERME  BALDO,  Identificação  Única
19654502, Professor do Magistério Superior, com exercício no Programa de Pós-Graduação em Ciências
Biológicas: Fisiologia do Instituto de Ciências Básicas da Saúde, observando-se o disposto na Lei nº 8.112, de
11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em
áreas consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.508275/2017-45,
Código SRH n° 23178 e Código SIAPE 2017002528.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
