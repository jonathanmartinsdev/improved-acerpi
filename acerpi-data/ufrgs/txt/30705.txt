Documento gerado sob autenticação Nº HSL.799.457.7EP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9339                  de  23/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°24933,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, DIEGO BONATTO (Siape: 1766403 ),  para substituir  
HENRIQUE BUNSELMEYER FERREIRA (Siape: 1038185 ), Coordenador da COMGRAD do Curso de Biotecnologia
do  Instituto  de  Biociências,  Código  FUC,  em  seu  afastamento  no  país,  no  período  de  10/11/2016  a
11/11/2016.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
