Documento gerado sob autenticação Nº QZA.252.279.GCI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4661                  de  29/05/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, FABIANO ENGELMANN, matrícula SIAPE n° 1548305, lotado no Departamento
de Ciência Política do Instituto de Filosofia e Ciências Humanas, para exercer a função de Coordenador do
PPG em Ciência Política, Código SRH 1142, código FUC, com vigência a partir de 15/06/2019 até 14/06/2021.
Processo nº 23078.513845/2019-81.
RUI VICENTE OPPERMANN
Reitor.
