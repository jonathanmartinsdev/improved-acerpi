Documento gerado sob autenticação Nº YOF.152.481.QJH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8677                  de  26/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°30987,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, DANIELE SCHONS (Siape: 1152091 ),   para
substituir    ALINE  CRISTINA  FERREIRA  (Siape:  2259118  ),  Diretor  da  Divisão  de  Acompanhamento  de
Convênios do Departamento de Execução de Projetos e Convênios da PROPLAN, Código FG-4,  em seu
afastamento por motivo de férias, no período de 31/10/2016 a 14/11/2016, com o decorrente pagamento das
vantagens por 15 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
