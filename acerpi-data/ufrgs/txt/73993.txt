Documento gerado sob autenticação Nº XFW.458.518.GA9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10426                  de  26/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de LEANDRO FARINA, Professor do Magistério Superior, lotado e
em exercício no Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística, com a
finalidade de realizar  visita  à Western Norway University  of  Applied Sciences,  em Bergen,  Noruega,  no
período compreendido entre 23/01/2019 e 26/02/2019, incluído trânsito, com ônus CAPES (Projeto ROAD
BESM). Solicitação nº 61771.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
