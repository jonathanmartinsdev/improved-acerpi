Documento gerado sob autenticação Nº PHP.146.158.C21, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8577                  de  24/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de AUGUSTO JAEGER JUNIOR, Professor do Magistério Superior,
lotado e em exercício no Departamento de Direito Público e Filosofia do Direito da Faculdade de Direito, com
a finalidade de participar do Congresso "La mise en oeuvre et l'effectivité du droit", em Montevidéu, Uruguai,
no  período  compreendido  entre  16/11/2016  e  19/11/2016,  incluído  trânsito,  com  ônus  CAPES/PROAP.
Solicitação nº 24188.
RUI VICENTE OPPERMANN
Reitor
