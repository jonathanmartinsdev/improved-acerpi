Documento gerado sob autenticação Nº DEX.526.264.7J5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 15/2020-PROGESP Porto Alegre, 06 de janeiro de 2020.
Senhora Diretora,
Encaminhamos  o  servidor  PAULO  RICARDO  DE  PIETRO  DOS  SANTOS,  ocupante  do  cargo
Assistente em Administração, para exercício nessa Unidade no dia 06/01/2020.
Solicitamos que nos sejam encaminhadas, no prazo máximo de 03 dias, as seguintes informações:
- atividades a serem desenvolvidas pelo servidor;
- local de exercício (divisão, setor), observando a hierarquia dos órgãos registrada no SRH;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal.
Informamos que este encaminhamento se faz para reposição de vaga, em virtude de vacância do
servidor Eduardo Pereira Rodrigues.
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
Ilma. Sra.
Professora CLARICE BERNHARDT FIALHO, 
Diretora do Instituto de Biociências, 
N/Universidade.
