Documento gerado sob autenticação Nº NPI.596.667.T8G, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3846                  de  25/05/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, 
RESOLVE
Dispensar,  a  pedido,  a partir  de 07/05/2018,  a ocupante do cargo de Professor do Magistério
Superior, classe Adjunto do Quadro de Pessoal desta Universidade, SILVIA PATRICIA FAGUNDES, matrícula
SIAPE n° 3451098, da função de Coordenadora do PPG em Artes Cênicas, Código SRH 1108, código FUC, para
a qual foi designada pela Portaria 9521/2017, de 11/10/2017, publicada no Diário Oficial da União do dia
16/10/2017. Processo nº 23078.517880/2017-15.
JANE FRAGA TUTIKIAN
Vice-Reitora.
