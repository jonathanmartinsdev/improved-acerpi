Documento gerado sob autenticação Nº RVD.462.399.AE2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7896                  de  05/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016,
RESOLVE
Retificar a Portaria n° 7664/2016, de 30/09/2016, que concede Progressão por Capacitação à JORGE
FERNANDO  RAMOS  DA  ROSA,  Apontador,  com  exercício  na  Prefeitura  Campus  do  Vale  (SP3)  da
Superintendência de Infraestrutura, conforme Processo nº 23078.203344/2016-46.
 
Onde se lê:
"O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012";
 
leia-se:
"O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016".
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
