Documento gerado sob autenticação Nº QQJ.069.969.EBA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5028                  de  07/06/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de VALERIO DE PATTA PILLAR, Professor do Magistério Superior,
lotado e em exercício no Departamento de Ecologia do Instituto de Biociências, com a finalidade de participar
do "60th Annual Symposium of the International Association for Vegetation Science" e excursões pré e pós-
simpósio, em Palermo, Itália, no período compreendido entre 18/06/2017 e 01/07/2017, incluído trânsito,
com ônus limitado. Solicitação nº 28331.
RUI VICENTE OPPERMANN
Reitor
