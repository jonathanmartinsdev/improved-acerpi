Documento gerado sob autenticação Nº JJS.436.473.H96, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             100                  de  04/01/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº 23078.025904/2016-15
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de Analista de Tecnologia da
Informação,  do  Quadro  de  Pessoal  desta  Universidade,  FABIANO  RIOS  HECK  (Siape:  1677900),   para
substituir HUBERT AHLERT (Siape: 6352705 ), Diretor do Depto de Sistemas de Informação vinculado ao CPD,
Código SRH 956, CD-4, em seu afastamento por Laudo Médico no período de 19/12/2016 a 25/12/2016, com
o decorrente pagamento das vantagens por 07 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
