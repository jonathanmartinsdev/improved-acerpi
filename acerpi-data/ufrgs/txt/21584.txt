Documento gerado sob autenticação Nº EDD.986.005.L3V, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             6503                  de  03/05/2016
A 
RESOLVE
Tornar insubsistente a Portaria nº 6502/2016, de 03/05/2016, publicada no Diário Oficial da União
de  que a RICARDO VIEIRA, ocupante do cargo de Analista de Tecnologia da Informação, lotado no Centro de
Processamento de Dados e com exercício na Divisão de Sistemas de Gestão Institucional.
MARIANA KEGLER LORENTZ
