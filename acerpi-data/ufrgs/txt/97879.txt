Documento gerado sob autenticação Nº ZWG.263.001.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8234                  de  09/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LUCIANA MARTA DEL BEN, Professor do Magistério Superior,
lotada e em exercício no Departamento de Música do Instituto de Artes, com a finalidade de ministrar
seminário  junto  à  Universidad  de  La  República  -  Uruguay,  em  Montevideu,  Uruguai,  no  período
compreendido entre 19/09/2019 e 21/09/2019, incluído trânsito, com ônus limitado. Solicitação nº 86914.
RUI VICENTE OPPERMANN
Reitor
