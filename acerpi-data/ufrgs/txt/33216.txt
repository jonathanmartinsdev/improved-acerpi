Documento gerado sob autenticação Nº MNC.885.704.7P7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             587                  de  19/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, MARCIA ELISA SOARES ECHEVESTE,  matrícula SIAPE n° 1061442, lotada no
Departamento de Estatística do Instituto de Matemática e Estatística, para exercer a função de Coordenadora
da COMGRAD de Estatística,  Código SRH 1222,  código FUC,  com vigência a partir  de 31/01/2017 e até
30/01/2019. Processo nº 23078.204357/2016-32.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
