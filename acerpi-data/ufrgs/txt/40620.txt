Documento gerado sob autenticação Nº MXH.239.799.KHP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5997                  de  07/07/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar ANTONIETA CARDOSO DE AZEVEDO,  Matrícula SIAPE 0357636, ocupante do cargo de
Recepcionista,  Código  701459,  do  Quadro  de  Pessoal  desta  Universidade,  para  exercer  a  função  de
Coordenadora do Núcleo de Apoio a Eventos e Comunicação da Gerência Administrativa da Faculdade de
Educação, Código SRH 817, Código FG-5, com vigência a partir da data de publicação no Diário Oficial da
União. Processo nº 23078.015476/2014-51.
RUI VICENTE OPPERMANN
Reitor.
