Documento gerado sob autenticação Nº ASV.954.511.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             770                  de  22/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder  prorrogação  de  jornada  de  trabalho  reduzida,  com  remuneração  proporcional,  nos
termos dos artigos 5º ao 7º da Medida Provisória nº 2.174-28, de 24 de agosto de 2001 e conforme o
Processo nº 23078.501824/2018-31, à servidora DORIANE KOHLER SCHEIBLER, matrícula SIAPE n° 1444972,
ocupante do cargo de Assistente em Administração - 701200, lotada na Pró-Reitoria de Gestão de Pessoas e
com exercício na Gerência Administrativa da PROGESP, alterando a jornada de trabalho de oito horas diárias
e quarenta horas semanais para 6 horas diárias e 30 semanais, no período de um ano, a partir de 16 de
fevereiro de 2020.
RUI VICENTE OPPERMANN
Reitor.
