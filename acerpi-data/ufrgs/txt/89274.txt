Documento gerado sob autenticação Nº XGW.745.502.M6R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2921                  de  03/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de PAULO RICARDO ZILIO ABDALA,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Ciências Administrativas da Escola de Administração,
com  a  finalidade  de  realizar  visita  ao  Centre  for  Humanitarian  Data,  em  Haia,  Holanda,  no  período
compreendido  entre  19  e  24/06/2019,  com  ônus  limitado  e  participar  do  "11th  International  Critical
Management  Studies  Conference",  em Milton Keynes,  Inglaterra,  no período compreendido entre  25 e
30/06/2019, com ônus UFRGS (Pró-Reitoria de Pesquisa: diárias) Solicitação nº 72853.
RUI VICENTE OPPERMANN
Reitor
