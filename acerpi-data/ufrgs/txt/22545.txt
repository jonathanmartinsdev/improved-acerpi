Documento gerado sob autenticação Nº HKC.830.179.UE3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3808                  de  23/05/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Remover, a partir de 10 de maio de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
RICARDO  STRACK,  ocupante  do  cargo  de  Técnico  em  Assuntos  Educacionais,  Ambiente  Organizacional
Ciências Humanas, Jurídicas e Econômicas, Código 701079, Classe E, Nível de Capacitação III,  Padrão de
Vencimento 05, SIAPE nº. 1677997 do Instituto de QuímicaaPró-Reitoria de Graduação para a lotação Pró-
Reitoria de Graduação, com novo exercício no Departamento de Cursos e Políticas da Graduação da Pró-
Reitoria de Graduação.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
