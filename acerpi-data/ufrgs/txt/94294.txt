Documento gerado sob autenticação Nº BVY.600.985.2QT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6031                  de  16/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARINES GARCIA,  matrícula SIAPE n° 1361909,  lotada e em exercício no Departamento de
Botânica do Instituto de Biociências, da classe D  de Professor Associado, nível 04, para a classe E  de
Professor Titular,  referente ao interstício de 12/09/2016 a 12/06/2019, com vigência financeira a partir de
13/06/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a
Decisão nº 331/2017 do CONSUN. Processo nº 23078.515896/2019-47.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
