Documento gerado sob autenticação Nº BWM.865.406.DI6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10940                  de  04/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ROBERTO GIUGLIANI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de participar do
"International MPS I Newborn Screening Advisory Board", em Lisboa, Portugal, no período compreendido
entre 07/12/2017 e 09/12/2017, incluído trânsito, com ônus limitado. Solicitação nº 33437.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
