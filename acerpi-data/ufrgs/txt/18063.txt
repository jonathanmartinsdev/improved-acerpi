Documento gerado sob autenticação Nº NAT.592.622.LE3, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1224                  de  18/02/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE:
Nomear, em caráter efetivo, GUILHERME BECKER SANDER, em virtude de habilitação em Concurso
Público de Provas e Títulos, homologado em 04 de fevereiro de 2016 e de acordo com os artigos 9º, item I e X
da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de 28 de dezembro de 2012, com redação dada
pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário Oficial da União de 25 de setembro de 2013,
para o cargo de Professor da Carreira do Magistério Superior do Plano de Carreiras e Cargos do Magistério
Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro de Pessoal desta Universidade, em regime
de trabalho de 40 (quarenta)  horas semanais,  junto ao Departamento de Farmacologia do Instituto de
Ciências Básicas da Saúde, em vaga decorrente da Aposentadoria Compulsória do Professor Jerônimo José
Zanonato, código nº 273047, ocorrida em 09 de maio de 2014, conforme Portaria nº.  4782 de 15 de julho de
2014, publicada no Diário Oficial da União de 18 de julho de 2014. Processo nº. 23078.002820/2015-22.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
