Documento gerado sob autenticação Nº WSI.091.779.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8942                  de  03/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de EDUARDO ERNESTO FILIPPI, Professor do Magistério Superior,
lotado e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de Ciências
Econômicas,  com  a  finalidade  de  ministrar  aulas  e  conferências  junto  à  Université  Paul  Valéry,  em
Montpellier, França, no período compreendido entre 10/10/2019 e 25/10/2019, incluído trânsito, com ônus
CAPES/PRINT/UFRGS. Solicitação nº 86354.
RUI VICENTE OPPERMANN
Reitor
