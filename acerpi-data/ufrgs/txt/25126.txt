Documento gerado sob autenticação Nº PQM.389.062.JQ4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5637                  de  26/07/2016
A PRÓ-REITORA DE GRADUAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 5224, de 01 de outubro de 2012
RESOLVE
Designar os servidores
RICARDO STRACK - Pró-Reitoria de Graduação;
DAVID SENGIK RIBEIRO - Pró-Reitoria de Graduação;
IRMA ANTONIETA GRAMKOV BUENO - Pró-Reitoria de Graduação;
MARLIS  MOROSINI  POLIDORI  -  Coordenadoria  de  Acompanhamento  do  Programa  de  Ações
Afirmativas;
VANESSA GABRIELA SAGGIN - Colégio de Aplicação;
para, sob a presidência do primeiro, constituírem a Comissão de Análise dos Recursos de Renda -
Análise Socioeconômica dos concursos Vestibular e SISU, a partir desta data, revogando as portarias 006 de
11 de março de 2016, 008 de 14 de março de 2016 e 5537 de 25 de julho de 2016.
ANDREA DOS SANTOS BENITES
Pró-Reitora de Graduação em exercício
