Documento gerado sob autenticação Nº VGQ.147.256.6FE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6502                  de  21/08/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  JORGE  ALBERTO  QUILLFELDT,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Biofísica do Instituto de Biociências, com a finalidade de
realizar  estudos em nível de Pós-Doutorado, junto à McGill University, em Montreal, Canadá, no período
compreendido entre 10/09/2018 e 09/09/2019, com ônus CAPES. Processo nº 23078.516929/2018-95.
RUI VICENTE OPPERMANN
Reitor.
