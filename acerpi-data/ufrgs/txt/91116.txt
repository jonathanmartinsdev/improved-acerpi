Documento gerado sob autenticação Nº SEG.055.338.RJM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4121                  de  14/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de EDER DANIEL TEIXEIRA,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Obras Hidráulicas do Instituto de Pesquisas Hidráulicas, com a
finalidade de participar do "XVI Seminário Ibero-americano sobre Sistemas de Abastecimento e Drenagem",
em Lisboa, Portugal, no período compreendido entre 13/07/2019 e 20/07/2019, incluído trânsito, com ônus
UFRGS (Pró-Reitoria de Pesquisa: diárias). Solicitação nº 83788.
RUI VICENTE OPPERMANN
Reitor
