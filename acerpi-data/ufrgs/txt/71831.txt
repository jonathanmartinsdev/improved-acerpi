Documento gerado sob autenticação Nº VQS.037.320.QQO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8979                  de  06/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do País  de MARIA DO CARMO RANGEL SANTOS VARELA,  Professor
Titular-livre  do Magistério  Superior,  lotada e  em exercício  no Departamento de Química Inorgânica do
Instituto de Química, com a finalidade de realizar visita à Universidad de Alicante, em Alicante, Espanha e à
Universidad  de  Barcelona,  em  Barcelona,  Espanha,  no  período  compreendido  entre  17/11/2018  e
30/11/2018, incluído trânsito, com ônus limitado. Solicitação nº 60880.
RUI VICENTE OPPERMANN
Reitor
