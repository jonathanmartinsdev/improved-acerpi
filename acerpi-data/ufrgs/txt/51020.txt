Documento gerado sob autenticação Nº FYN.854.109.3M3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1446                  de  16/02/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subseqüente, combinado com
o parágrafo 5º do artigo 40 da Constituição Federal, a JUÇARA GONÇALVES FREITAS, matrícula SIAPE nº
2227331, no cargo de Professor da Carreira do Magistério do Ensino Básico, Técnico e Tecnológico, classe D,
nível 2, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no Departamento de
Comunicação do Colégio de Aplicação, com proventos integrais. Processo 23078.022199/2017-85.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
