Documento gerado sob autenticação Nº OLF.415.375.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3320                  de  16/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de EDUARDO GUIMARAES BARBOZA,  Professor do Magistério
Superior,  lotado  no  Departamento  de  Paleontologia  e  Estratigrafia  do  Instituto  de  Geociências  e  com
exercício no Centro de Estudos Costeiros, Limnológicos e Marinhos, com a finalidade de realizar trabalho de
campo  junto  à  Universidad  de  la  Costa,  em  Barranquilla,  Colômbia,  no  período  compreendido  entre
07/05/2019 e 16/05/2019, incluído trânsito, com ônus limitado. Solicitação nº 83318.
RUI VICENTE OPPERMANN
Reitor
