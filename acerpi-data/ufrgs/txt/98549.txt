Documento gerado sob autenticação Nº SLA.191.106.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8698                  de  25/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CASSIANO KUCHENBECKER ROSING, Professor do Magistério
Superior,  lotado  e  em  exercício  no  Departamento  de  Odontologia  Conservadora  da  Faculdade  de
Odontologia, com a finalidade de participar do "1st IADR Caribbean Section Annual Meeting", em Santo
Domingo, República Dominicana, no período compreendido entre 02/10/2019 e 05/10/2019, incluído trânsito,
com ônus limitado. Solicitação nº 86968.
RUI VICENTE OPPERMANN
Reitor
