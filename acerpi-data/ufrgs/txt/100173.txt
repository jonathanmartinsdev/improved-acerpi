Documento gerado sob autenticação Nº TNQ.738.145.V77, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9780                  de  29/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de GIOVANA DOMENEGHINI MERCALI, Professor do Magistério
Superior,  lotada e em exercício no Departamento de Ciências dos Alimentos do Instituto de Ciências e
Tecnologia de Alimentos, com a finalidade de realizar visita à Rutgers University e à The Ohio State University,
em New Brunswick e Columbus, Estados Unidos, no período compreendido entre 04/11/2019 e 16/11/2019,
incluído trânsito, com ônus CAPES/PRINT/UFRGS. Solicitação nº 87691.
RUI VICENTE OPPERMANN
Reitor
