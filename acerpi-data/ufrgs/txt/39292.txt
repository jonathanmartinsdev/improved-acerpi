Documento gerado sob autenticação Nº GBJ.666.370.NG8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5013                  de  07/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  31/05/2017,   referente  ao  interstício  de
03/10/2012 a 30/05/2017, para a servidora LIANE DENISE THIER RUSCHEL, ocupante do cargo de Assistente
em Administração - 701200, matrícula SIAPE 0358693,  lotada  no  Instituto de Física, passando do Nível de
Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.201492/2017-15:
Formação Integral de Servidores da UFRGS V CH: 126 (01/10/2007 a 26/05/2017)
UFPR - 8º Congresso de Secretários das Universidades Brasileiras CH: 24 (29/09/2014 a 02/10/2014)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
