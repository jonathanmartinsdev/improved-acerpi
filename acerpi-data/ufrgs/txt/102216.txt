Documento gerado sob autenticação Nº ADJ.384.359.HRU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11369                  de  20/12/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.515741/2018-20  e
23078.526860/2019-99, do Contrato nº 071/2018, da Lei nº 10.520/02 e ainda, da Lei nº 8.666/93,
            RESOLVE:
 
            Apl icar  à  Empresa  SULCLEAN  SERVIÇOS  LTDA,  CNPJ  n.º  06.205.427/0001-02  as
sanções administrativas de:
 ADVERTÊNCIA prevista no inciso I, item 16.1 do Termo de Referência do Contrato, pelos motivos de faltas•
sem reposição de postos, descontos indevidos nos salários e incosistências acerda da carga horária (1ª
ocorrência), conforme atestado pela fiscalização do contrato (Doc. SEI nº 1823818 e 1918669), bem como
pelo DICON/NUDECON (Doc. SEI nº 1938038) do processo 23078.526860/2019-99.
MULTA de 5% (cinco por cento) e 8% (oito por cento) sobre os valores das parcelas que lhe deram causa, no•
montante de R$ 2.059,32 (dois mil e cinquenta e nove reais e trinta e dois centavos), conforme
demonstrativo de cálculo (Doc. SEI nº 1938752), prevista no inciso II, inciso 16.1 do Termo de Referência do
Contrato, pelos motivos de faltas sem reposição de postos, descontos indevidos nos salários, por não
tratar-se de primeira ocorrência, conforme atestado pela fiscalização do contrato (Doc. SEI
nº 1823818 e 1918669), bem como pelo DICON/NUDECON (Doc. SEI nº 1938038) do processo
23078.526860/2019-99.
 
            Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
