Documento gerado sob autenticação Nº QIX.878.093.CEO, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7458                  de  11/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/08/2017,   referente  ao  interstício  de
11/02/2016 a 10/08/2017, para a servidora JULIANA DE LIMA VIEIRA MACHADO,  ocupante do cargo de
Técnico  em  Contabilidade  -  701224,  matrícula  SIAPE  2156700,   lotada   na   Superintendência  de
Infraestrutura,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de
Classificação/Nível de Capacitação D III,  em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.012988/2017-16:
Formação Integral de Servidores da UFRGS II CH: 40 (02/05/2016 a 03/04/2017)
ILB - Deveres, proibições e responsabilidades do servidor público federal CH: 60 Carga horária utilizada: 48
hora(s) / Carga horária excedente: 12 hora(s) (06/07/2016 a 22/08/2016)
ESAF - XII Semana de Administração Orçamentária, Financeira e de Contratações Públicas CH: 32 (16/11/2015
a 20/11/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
