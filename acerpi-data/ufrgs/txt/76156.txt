Documento gerado sob autenticação Nº SHN.116.772.PBU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1242                  de  04/02/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar na Escola de Engenharia,  com exercício no Laboratório de Inovação e Fabricação Digital,
Ambiente  Organizacional  Ciências  Exatas  e  da  Natureza,  CARLOS  HENRIQUE  LAUERMANN,  nomeado
conforme Portaria Nº 10231/2018 de 18 de dezembro de 2018, publicada no Diário Oficial da União no dia 19
de dezembro de 2018, em efetivo exercício desde 25 de janeiro de 2019, ocupante do cargo de TÉCNICO EM
MECÂNICA, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
