Documento gerado sob autenticação Nº GRI.423.859.ENF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10688                  de  23/11/2017
Nomeação  da  Representação  Discente  do
Programa de Pós-Graduação em Engenharia
Elétrica da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente do Programa de Pós-Graduação em Engenharia Elétrica, para
composição na Comissão de Pós-Graduação e Conselho de Pós-Graduação, com mandato de 01 (um) ano, a
contar de 08 de agosto de 2017, atendendo o disposto nos artigos 175 do Regimento Geral da Universidade e
79 do Estatuto da Universidade, e considerando o processo nº 23078.020624/2017-00, conforme segue:
 
Titular: Vinicius Horn Cene
Titular: Marcos Antônio Jeremias Coelho
Suplente: Roger Alves de Oliveira
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
