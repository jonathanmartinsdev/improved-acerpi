Documento gerado sob autenticação Nº EKE.589.349.J72, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2096                  de  20/03/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar ANTONIO MARCOS VIEIRA SANSEVERINO,  matrícula SIAPE n° 1507012,  ocupante do
cargo de Professor do Magistério Superior, classe Associado, lotado no Departamento de Letras Clássicas e
Vernáculas  do  Instituto  de  Letras,  do  Quadro  de  Pessoal  da  Universidade,  para  exercer  a  função
de Coordenador Substituto da COMGRAD de Letras, com vigência a partir de 15/04/2018 e até 14/05/2019,  a
fim de completar o mandato da Professora Karina de Castilhos Lucena, conforme artigo 92 do Estatuto da
mesma Universidade. Processo nº 23078.504844/2018-64.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
