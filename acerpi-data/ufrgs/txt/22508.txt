Documento gerado sob autenticação Nº UBL.285.932.E3V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3775                  de  20/05/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LEANDRO KRUG WIVES,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a finalidade
de participar de banca de doutorado junto à Universidad del Cauca, em Popayan, Colômbia, no período
compreendido entre 07/06/2016 e 11/06/2016, incluído trânsito, com ônus limitado. Solicitação nº 20247.
CARLOS ALEXANDRE NETTO
Reitor
