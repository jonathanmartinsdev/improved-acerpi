Documento gerado sob autenticação Nº ZCM.411.504.CCG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5368                  de  18/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar a Portaria n° 4168/2016, de 08/06/2016, que concedeu autorização para afastamento do
País a NADYA PESCE DA SILVEIRA, Professor do Magistério Superior, com exercício no Instituto de Química
Onde se lê: no período compreendido entre 08/07/2016 e 15/07/2016,
leia-se: no período compreendido entre 09/07/2016 e 15/07/2016, ficando ratificados os demais
termos. Solicitação nº 21334.
CARLOS ALEXANDRE NETTO
Reito
