Documento gerado sob autenticação Nº EPO.763.240.8VG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9544                  de  21/10/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar  na  Faculdade  de  Arquitetura,  com  exercício  no  Núcleo  Administrativo  da  Faculdade  de
Arquitetura, Ambiente Organizacional Administrativo, ELISABETE DE VARGAS BORGES, nomeada conforme
Portaria Nº 7885/2019 de 30 de agosto de 2019, publicada no Diário Oficial da União no dia 02 de setembro
de  2019,  em  efetivo  exercício  desde  16  de  outubro  de  2019,  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, classe D,  nível  I,  padrão 101,  no Quadro de Pessoal  desta Universidade.  Processo n°
23078.528279/2019-10.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
