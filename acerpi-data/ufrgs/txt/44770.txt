Documento gerado sob autenticação Nº FBJ.046.910.PV8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             8909                  de  25/09/2017
Nomeação  da  Representação  Discente  dos
órgãos colegiados do Instituto de Informática
da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita no Instituto de Informática, com mandato de 01 (um) ano,
a  contar  de  1º  de  setembro de  2017,  atendendo o  disposto  nos  artigos  175  do  Regimento  Geral  da
Universidade  e  79  do  Estatuto  da  Universidade,  e  considerando o  processo  nº  23078.202670/2017-17,
conforme segue:
Colegiado do Departamento de Informática Aplicada
1º Titular: Lais Lemos Karsburg
1º Suplente: Germano Tietbohl Martinelli
2º Titular: Henrique Ecker Pchara
2º Suplente: Andressa Ruy
 
Plenário do Departamento de Informática Aplicada
1º Titular: Germano Tietbohl Martinelli
2º Titular: Lais Lemos Karsburg
3º Titular: Felipe Rambo Thozeski
4º Suplente: Andressa Ruy
5º Suplente: Rodrigo Daltóe Madruga
6º Suplente: Henrique Ecker Pchara
7º Suplente: Luciano Zancan
8º Suplente: Lucas da Silva Flores
9º Suplente: César Augusto de Souza Pastorini
 
Plenário do Departamento de Informática Teórica
1º Titular: Lais Lemos Karsburg
1º Suplente: Germano Tietbohl Martinelli
2º Titular: Andressa Ruy
2º Suplente: Felipe Rambo Thozeski
Documento gerado sob autenticação Nº FBJ.046.910.PV8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
 
Comissão de Pesquisa
1º Titular: Lais Lemos Karsburg
1º Suplente: Andressa Ruy
 
Comissão de Extensão
1º Titular: Germano Tietbohl Martinelli
1º Suplente: Andressa Ruy
 
Conselho do Centro de Empreendimentos em Informática
1º Titular: Felipe Rambo Thozeski
1º Suplente: Germano Tietbohl Martinelli
 
Conselho do Instituto de Informática (Graduação)
1º Titular: Felipe Rambo Thozeski
1º Suplente: Germano Tietbohl Martinelli
 
Conselho do Instituto de Informática (Pós-Graduação)
1º Titular: Pedro Henrique da Costa Avelar
 
Comissão de Graduação de Ciências da Computação
1º Titular: Andressa Ruy
1º Suplente: Lucas da Silva Flores
 
Comissão de Graduação de Engenharia de Computação
1º Titular: Henrique Ecker Pchara
1º Suplente: Germano Tietbohl Martinelli
 
Comissão do Programa de Pós-Graduação (PPGC)
1º Titular: Pedro Henrique da Costa Avelar
 
Conselho do Programa de Pós-Graduação (PPGC)
1º Titular: Pedro Henrique da Costa Avelar
 
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
