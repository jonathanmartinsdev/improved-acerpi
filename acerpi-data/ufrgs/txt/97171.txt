Documento gerado sob autenticação Nº JGR.158.835.PKK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7787                  de  28/08/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder 60 (sessenta) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de
11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a servidora
JULIANA  BEATRIS  MOURA  DO  NASCIMENTO,  com  exercício  no  Núcleo  Especializado  da  Gerencia
Administrativa da ODONTO, a ser usufruída no período de 16/09/2019 a 14/11/2019, referente ao quinquênio
de 10/08/2012 a 09/08/2017, a fim de participar do curso "Biossegurança Hospitalar", oferecido pelo Centro
de Educação Profissional - CENED, e do curso "Primeiros Socorros", oferecido pelo Instituto Nacional de
Ensino a Distância - GINEAD; conforme Processo nº 23078.521628/2019-64.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
