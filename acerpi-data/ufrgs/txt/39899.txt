Documento gerado sob autenticação Nº LRX.653.849.CUF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5449                  de  22/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor FELIX VALENTIN BUGUENO MIRANDA,  matrícula SIAPE n° 1210035, lotado e em exercício no
Departamento de Línguas Modernas do Instituto de Letras, da classe   de Professor Associado, nível 01, para
a classe   de Professor Associado, nível 02, referente ao interstício de 13/08/2010 a 12/08/2012, com vigência
financeira a partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com suas alterações e a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo
nº 23078.506183/2017-21.
JANE FRAGA TUTIKIAN
Vice-Reitora.
