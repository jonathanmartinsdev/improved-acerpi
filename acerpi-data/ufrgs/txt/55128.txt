Documento gerado sob autenticação Nº QPD.639.670.5I1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             4588                  de  26/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais  e  estatutárias,  de acordo com o processo nº  23078.005569/2016-39,
considerando o disposto na Lei nº 8.112, de 11/12/1990, no Decreto Presidencial nº 1.590, de 10/08/1995, no
Decreto nº 4.836, de 09/09/2003, na Decisão nº 432 do Conselho Universitário, de 27/11/2015 e na Portaria
9.911 de 26/10/2017 desta Universidade
RESOLVE
 
Retificar a Portaria n° 8737, de 19/09/2017:
 
Onde se lê:
 
Art.1º  Autorizar  a  jornada  de  trabalho  flexibilizada  na  Clínica  de  Atendimento  Psicológico  do
Instituto de Psicologia no período diário de atendimento ao público das 8h às 21h, pelo prazo de doze meses,
conforme disposto no Art. 11, da Portaria 3.183 de 12/04/2017. Os servidores técnico-administrativos abaixo
relacionados terão jornada de trabalho de seis  horas diárias,  cumprindo carga horária de trinta horas
semanais:
 
CAMILA MAGGI RECH NOGUEZ - SIAPE: 1917493
CAROLINA BARTH DOS SANTOS - SIAPE: 2057105
DEBORAH DA SILVA MINUZ - SIAPE: 1034535
JOSE LUIS LONGO - SIAPE: 2158492
LUIZ OCTAVIO MARTINS STAUDT - SIAPE: 1758043
MANOELA HOROWITZ PETERSEN - SIAPE: 2055426
MARCIA GIOVANA PEDRUZZI REIS - SIAPE: 1860041
MARI ELIANE PEREIRA - SIAPE: 0357028
MARIANA KRAEMER BETTS - SIAPE: 1678488
MARILIA SPINELLI JACOBY CUNDA - SIAPE: 1683108
SANDRA LAURA FRISCHENBRUDER SULZBACH - SIAPE: 0356877
 
Leia-se:
 
Art.1º  Autorizar  a  jornada  de  trabalho  flexibilizada  na  Clínica  de  Atendimento  Psicológico,  do
Instituto de Psicologia no período diário de atendimento ao público das 8h às 21h30min, pelo prazo de doze
meses a contar de 19/09/2017, conforme disposto no Art. 11 da Portaria 9911 de 26/10/2017. Os servidores
técnico-administrativos abaixo relacionados terão jornada de seis horas diárias,  cumprindo carga horária de
trinta horas semanais. 
 
 
CAMILA MAGGI RECH NOGUEZ - SIAPE: 1917493
CAROLINA BARTH DOS SANTOS - SIAPE: 2057105
DEBORAH DA SILVA MINUZ - SIAPE: 1034535
JADER REMIÃO CARRASCO - SIAPE: 1782226
JOSE LUIS LONGO - SIAPE: 2158492
LUIZ OCTAVIO MARTINS STAUDT - SIAPE: 1758043
MANOELA HOROWITZ PETERSEN - SIAPE: 2055426
Documento gerado sob autenticação Nº QPD.639.670.5I1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
MARCIA GIOVANA PEDRUZZI REIS - SIAPE: 1860041
MARI ELIANE PEREIRA - SIAPE: 0357028
MARIANA KRAEMER BETTS - SIAPE: 1678488
MARILIA SPINELLI JACOBY CUNDA - SIAPE: 1683108
SANDRA LAURA FRISCHENBRUDER SULZBACH - SIAPE: 0356877
 
Art. 2º Ficam ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
