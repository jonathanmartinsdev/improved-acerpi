Documento gerado sob autenticação Nº RBY.698.228.9EE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9565                  de  01/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Remover, a partir de 18 de outubro de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
CARLOS ERNESTO RECH, ocupante do cargo de Técnico em Assuntos Educacionais, Ambiente Organizacional
Informação, Código 701079, Classe E, Nível de Capacitação III, Padrão de Vencimento 16, SIAPE nº. 0351526
da Faculdade de Medicina para a lotação Faculdade de Medicina, com novo exercício no Núcleo Técnico-
Cientifíco da Gerência Administrativa da Faculdade de Medicina.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
