Documento gerado sob autenticação Nº PZY.477.900.I0C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4336                  de  17/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal desta Universidade, JOSE CLAUDIO FONSECA MOREIRA,  matrícula SIAPE n° 0357674, lotado no
Departamento de Bioquímica do Instituto de Ciências Básicas da Saúde, para exercer a função de Chefe do
Depto de Bioquímica do ICBS, Código SRH 138, Código FG-1, com vigência a partir da data de publicação no
Diário Oficial da União, pelo período de 02 anos. Processo nº 23078.008077/2017-86.
JANE FRAGA TUTIKIAN
Vice-Reitora.
