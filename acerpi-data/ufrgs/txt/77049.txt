Documento gerado sob autenticação Nº TOO.737.688.1PA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1691                  de  18/02/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LUIS ALBERTO DAVILA FERNANDES, Professor do Magistério
Superior, lotado e em exercício no Departamento de Geologia do Instituto de Geociências, com a finalidade
de realizar trabalho de campo, em Uyuni, Bolívia, no período compreendido entre 24/02/2019 e 12/03/2019,
incluído trânsito, com ônus limitado. Solicitação nº 62217.
RUI VICENTE OPPERMANN
Reitor
