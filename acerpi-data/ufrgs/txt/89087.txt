Documento gerado sob autenticação Nº MEW.463.335.P7M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2816                  de  01/04/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016 e conforme a Solicitação de Férias n°46696,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, EDUARDO AUGUSTO RIFFEL PACHECO (Siape:
2419473 ),  para substituir   EDUARDO CARDOSO (Siape: 2620161 ), Coordenador da Ponto UFRGS vinculado
ao Departamento Administrativo e de Registro da Extensão (DARE) da Pró-Reitoria de Extensão (PROREXT),
Código FG-1, em seu afastamento por motivo de férias, no período de 01/04/2019 a 05/04/2019, com o
decorrente pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
