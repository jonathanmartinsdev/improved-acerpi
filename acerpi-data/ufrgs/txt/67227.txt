Documento gerado sob autenticação Nº PYR.197.084.D9I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5724                  de  01/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  a  ocupante  do  cargo  de  Professor  do  Magistério  Superior,  classe  Adjunto,  desta
Universidade,  PATRÍCIA DA SILVA MALHEIROS, matrícula SIAPE n° 2098980,  lotada no Departamento de
Ciências  dos  Alimentos  do  Instituto  de  Ciências  e  Tecnologia  de  Alimentos,  para  exercer  a  função
de Coordenadora da Comissão de Extensão do ICTA, código SRH 781, com vigência a partir de 17/08/2018 até
16/08/2020, ficando o pagamento de gratificação condicionado à disponibilidade de uma função gratificada.
Processo nº 23078.518203/2018-97.
JANE FRAGA TUTIKIAN
Vice-Reitora.
