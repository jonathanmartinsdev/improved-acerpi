Documento gerado sob autenticação Nº DFP.835.892.7JI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10279                  de  14/11/2019
  Designa Grupo de Trabalho para analisar
nova  minuta  do  anteprojeto  de  Lei  do
Future-se e propor documento orientador
para Comissão Especial do CONSUN.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias, nos termos da Decisão nº 286/2019 do CONSUN,
RESOLVE
Designar
 
MARIA BEATRIZ MOREIRA LUCE
PEDRO CEZAR DUTRA FONSECA
JOSE LUIS DUARTE RIBEIRO
TIAGO CARRARD CENTURIAO
JAIRO ALFREDO GENZ BOLTER
SONIA MARA MOREIRA OGIBA
GERUSA SHAIANA DOMINGUES PENA
RAFAEL DA CÁS MAFFINI
ELISIANE DA SILVA SZUBERT
FLAVIA WAGNER
MATEUS DALMORO
LUIZA DE CAMPOS MORAIS RAMOS
 
para compor Grupo de Trabalho para analisar nova minuta do anteprojeto de Lei do Future-se e propor
documento orientador para Comissão Especial do CONSUN.
RUI VICENTE OPPERMANN,
Reitor.
