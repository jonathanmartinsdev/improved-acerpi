Documento gerado sob autenticação Nº UTK.343.729.5BK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             502                  de  16/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°41208,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, LIDIANE DA SILVA SITTONI (Siape: 2167588 ),
 para substituir   ALESSANDRA DE ALMEIDA ABREU DAGOSTINI (Siape: 1867329 ), Secretário do Centro de
Teledifusão Educativa, Código FG-5, em seu afastamento por motivo de férias, no período de 22/01/2018 a
01/02/2018, com o decorrente pagamento das vantagens por 11 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
