Documento gerado sob autenticação Nº XXS.049.066.BPS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             215                  de  05/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  20/12/2017,   referente  ao  interstício  de
20/06/2016 a 19/12/2017, para o servidor ROBERTO SEVERO MULLER, ocupante do cargo de Vigilante -
701269, matrícula SIAPE 0358181,  lotado  na  Coordenadoria de Segurança da UFRGS, passando do Nível de
Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.524218/2017-11:
ENAP - Orçamento Público: conceitos básicos CH: 30 (02/08/2016 a 29/08/2016)
ENAP - A Previdência Social dos Servidores Públicos CH: 30 (02/08/2016 a 29/08/2016)
ENAP  -  Gestão  da  Informação  e  Documentação  -  Conceitos  básicos  em  Gestão  Documental  CH:  20
(07/06/2016 a 04/07/2016)
ENAP - Gestão e Fiscalização de Contratos Administrativos CH: 40 (02/08/2016 a 05/09/2016)
ENAP - Ciclo de Gestão do Investimento Público CH: 20 (09/08/2016 a 29/08/2016)
ENAP - Regulamentação da Lei de Acesso à Infromação nos Municípios CH: 20 Carga horária utilizada: 10
hora(s) / Carga horária excedente: 10 hora(s) (16/08/2016 a 05/09/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
