Documento gerado sob autenticação Nº NZY.493.574.MPB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3817                  de  04/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MARCELLE REESINK CERSKI, matrícula SIAPE n° 0357074, lotada e em exercício no Departamento
de Patologia da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 03, para a classe C  de
Professor Adjunto, nível 04, referente ao interstício de 12/04/2014 a 31/07/2016, com vigência financeira a
partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações  e  a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.502122/2017-94.
JANE FRAGA TUTIKIAN
Vice-Reitora.
