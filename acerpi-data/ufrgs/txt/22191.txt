Documento gerado sob autenticação Nº IYI.897.139.TOV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3609                  de  13/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012,
RESOLVE
Retificar  a  Portaria  n°  3410/2016,  de  10/05/2016,  publicada  no  Diário  Oficial  da  União  de
11/05/2016, que declarou vago o cargo de RAFAEL PIZZOLATO, Assistente em Administração, com exercício
na Secretaria de Educação a Distância. Processo nº 23078.006270/2016-00.
Onde se lê: "em decorrência de exoneração de cargo efetivo a pedido prevista no Art. 34º da Lei
8.112, de 11 de dezembro de 1990",
leia-se: "em decorrência de posse em outro cargo público inacumulável".
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
