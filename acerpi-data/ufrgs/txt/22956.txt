Documento gerado sob autenticação Nº GBW.614.055.C5U, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4104                  de  06/06/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a MARIA LUIZA BONORINO
MACHADO, matrícula SIAPE nº 1031669, no cargo de Assistente em Administração, nível de classificação D,
nível de capacitação IV, padrão 13, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho, com exercício no Núcleo Acadêmico do Instituto de Letras,  com proventos integrais.  Processo
23078.201177/2016-07.
CARLOS ALEXANDRE NETTO
Reitor
