Documento gerado sob autenticação Nº OWQ.386.356.6FE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6063                  de  09/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  SANDRA  DE  FATIMA  BATISTA  DE  DEUS,  Professor  do
Magistério  Superior,  lotada  no  Departamento  de  Comunicação  da  Faculdade  de  Biblioteconomia  e
Comunicação e com exercício na Pró-Reitoria de Extensão, com a finalidade de, como  representante da 
UFRGS, participar da "II Reunião da Comissão Permanente de Extensão de AUGM" e das "Jornadas Nacionales
de Extensión Universitaria: A 100 años de Córdoba: Reflexión y Proyección de la Extensión Universitaria", em
Montevidéu, Uruguai, no período compreendido entre 12/11/2018 e 17/11/2018, incluído trânsito, com ônus
UFRGS (diárias). Solicitação nº 47757.
RUI VICENTE OPPERMANN
Reitor
