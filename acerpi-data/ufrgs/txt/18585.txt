Documento gerado sob autenticação Nº YNP.047.998.SV3, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1549                  de  02/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar  o  afastamento  do  país  de  IDA  VANESSA  DOEDERLEIN  SCHWARTZ,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Genética do Instituto de Biociências, com a
finalidade de participar do "13th International Congress of Human Genetics Please", em Kyoto, Japão, no
período compreendido entre 31/03/2016 e 08/04/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa - diárias). Solicitação nº 17965.
CARLOS ALEXANDRE NETTO
Reitor
