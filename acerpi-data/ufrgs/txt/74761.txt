Documento gerado sob autenticação Nº MWH.868.038.7I0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             266                  de  09/01/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°41505,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  DESENHISTA  TÉCNICO
ESPECIALIZADO, do Quadro de Pessoal desta Universidade, PAULO BALDO (Siape: 1008756 ),  para substituir  
VICENTE  FERNANDES  DUTRA  FONSECA  (Siape:  1271529  ),  Coordenador  do  Núcleo  de  Divulgação  do
Departamento Administrativo e de Registro da Extensão da PROREXT, Código FG-4, em seu afastamento por
motivo de férias, no período de 11/01/2019 a 04/02/2019, com o decorrente pagamento das vantagens por
25 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
