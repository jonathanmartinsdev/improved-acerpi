Documento gerado sob autenticação Nº EHH.246.958.R93, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4219                  de  15/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  16/04/2019,
correspondente ao grau Insalubridade Máxima, ao servidor GERALDO PEREIRA JOTZ,  Identificação Única
13008811, Professor do Magistério Superior, com exercício no Departamento de Ciências Morfológicas do
Instituto de Ciências Básicas da Saúde, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de
1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas
Insalubres conforme Laudo Pericial constante no Processo nº 23078.510336/2019-04, Código SRH n° 23881.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
