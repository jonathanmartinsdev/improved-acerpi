Documento gerado sob autenticação Nº RIB.188.902.82M, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2193                  de  10/03/2017
A VICE-SUPERINTENDENTE DE INFRAESTRUTURA DA SUPERINTENDÊNCIA DE INFRAESTRUTURA DA
UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na
Portaria nº 7793, de 03 de outubro de 2016
RESOLVE
Prorrogar por mais 30 (trinta) dias o prazo estabelecido pela Portaria nº 1165 de 07 de fevereiro de
2017, a fim de que a Comissão dê continuidade à elaboração de Dar suporte técnico para a implementação e
acompanhamento do Mural Digital SECOM, conforme consta no processo SEI UFRGS nº 23078.503075/2017-
04.
ANDREA PINTO LOGUERCIO
Vice-Superintendente de Infraestrutura da Superintendência de Infraestrutura
