Documento gerado sob autenticação Nº KHL.902.896.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2593                  de  22/03/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARCIO DORN, Professor do Magistério Superior, lotado e em
exercício no Departamento de Informática Teórica do Instituto de Informática, com a finalidade de participar
do "EvoStar 2019" e realizar visita ao Karlsruhe Institute of Technology, em Karlsruhe, Alemanha, no período
compreendido entre 16/04/2019 e 28/04/2019, incluído trânsito, com ônus limitado. Solicitação nº 62421.
RUI VICENTE OPPERMANN
Reitor
