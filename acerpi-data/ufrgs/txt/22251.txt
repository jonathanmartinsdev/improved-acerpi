Documento gerado sob autenticação Nº IEY.595.641.Q4L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3618                  de  16/05/2016
Nomeação da Representação Discente do  da
UFRGS
O  CHEFE  DA  DIVISÃO  DE  SISTEMAS  DE  GESTÃO  INSTITUCIONAL  DO  DEPTO  DE  SISTEMAS  DE
INFORMAÇÃO  DA  UNIVERSIDADE  FEDERAL  DO  RIO  GRANDE  DO  SUL,  no  uso  de  suas  atribuições,
considerando o disposto na Portaria nº 6502, de 03 de maio de 2016
RESOLVE
Nomear a Representação Discente eleita para compor o , com mandato de 01 (um) ano, a contar
de atendendo ao disposto  nos  artigos  175 do Regimento Geral  da  Universidade e  79  do Estatuto  da
Universidade e considerando o processo nº 23078.030051/13-19, conforme segue:
RICARDO VIEIRA
Chefe da Divisão de Sistemas de Gestão Institucional do Depto de Sistemas de Informação
