Documento gerado sob autenticação Nº ULB.946.377.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2650                  de  25/03/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Exonerar, a partir da data de publicação no Diário Oficial da União, JOSE CARLOS FRANTZ,  da
função Assessor do Reitor, Código SRH 1388, Código CD-4, da Universidade Federal do Rio Grande do Sul,
para o qual foi designado pela Portaria nº 8690/2018, de 26/10/2018, publicada no Diário Oficial da União de
29/10/2018, por ter sido nomeado para outro cargo de direção. Processo nº 23078.507511/2019-78.
RUI VICENTE OPPERMANN
Reitor.
