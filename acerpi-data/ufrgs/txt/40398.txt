Documento gerado sob autenticação Nº UHF.227.626.CLR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5799                  de  04/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  29/06/2017,   referente  ao  interstício  de
28/12/2015 a  28/06/2017,  para  o  servidor  LUIS RICARDO DA SILVA ANTUNES,  ocupante do cargo de
Recepcionista - 701459, matrícula SIAPE 0356215,  lotado  na  Pró-Reitoria de Planejamento e Administração,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  C  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  C  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.012604/2017-57:
ABED - Gestão de Recursos Humanos CH: 30 (18/11/2015 a 18/12/2015)
CAPACITAR - Informática na Educação CH: 120 Carga horária utilizada: 90 hora(s) / Carga horária excedente:
30 hora(s) (14/04/2017 a 24/06/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
