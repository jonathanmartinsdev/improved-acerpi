Documento gerado sob autenticação Nº DYW.394.631.J4F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4849                  de  01/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR  o  percentual  de  Incentivo  à  Qualificação  concedido  ao  servidor  SILVIO  HENRIQUE
BERSAGUI, ocupante do cargo de Mestre de Edificações e Infraestrutura-701208, lotado na Superintendência
de Infraestrutura, SIAPE 0358447, para 52% (cinquenta e dois por cento), a contar de 16/12/2016, tendo em
vista a conclusão do curso de Mestrado em Engenharia, conforme o Processo nº 23078.204263/2016-63.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
