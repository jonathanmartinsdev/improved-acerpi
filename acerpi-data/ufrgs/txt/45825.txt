Documento gerado sob autenticação Nº BIC.123.474.N17, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9509                  de  11/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 40, parágrafo 1º, inciso I, da Constituição Federal, c/c
art. 6-A da Emenda Constitucional nº 41, de 19 de dezembro de 2003, incluído pela Emenda Constitucional nº
70, de 29 de março de 2012, e art. 186, parágrafo 1º, da Lei nº 8.112, de 11 de dezembro de 1990, a MARCOS
JOSE PEREIRA GOMES, matrícula SIAPE nº 1115830, no cargo de Professor Associado, nível 2, da Carreira do
Magistério Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no
Departamento  de  Patologia  Clínica  Veterinária  da  Faculdade  de  Veterinária,  com  proventos  integrais.
Processo 23078.012441/2017-11.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
