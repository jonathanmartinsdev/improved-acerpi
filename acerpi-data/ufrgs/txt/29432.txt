Documento gerado sob autenticação Nº MSX.363.836.6O0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8383                  de  19/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  25/09/2016,   referente  ao  interstício  de
25/03/2015 a 24/09/2016, para a servidora SABRINA DIEHL MENEZES, ocupante do cargo de Bibliotecário-
documentalista - 701010, matrícula SIAPE 1638780,  lotada  na  Escola de Engenharia, passando do Nível de
Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.017724/2016-60:
Formação Integral de Servidores da UFRGS III CH: 66 (23/01/2014 a 25/05/2016)
ILB - Ética e Administração Pública CH: 40 Carga horária utilizada: 22 hora(s) / Carga horária excedente: 18
hora(s) (11/07/2016 a 31/07/2016)
IV  Conferência  Internacional  Biredial  -  ISTEC  -  Acesso  Aberto,  Preservação  Digital,  Interoperabilidade,
Visibilidade e Dados Científicos CH: 2 (15/10/2014 a 17/10/2014)
UNIASSELVI  -  Contribuição  Indígena  e  Africana  na  Formação  do  Povo  Brasileiro  CH:  20  (20/05/2015  a
31/05/2015)
Prime Cursos - Curso de Gestão de Conflitos CH: 40 (22/06/2016 a 30/06/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
