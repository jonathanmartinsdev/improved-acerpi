Documento gerado sob autenticação Nº GIG.383.415.NM8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2305                  de  14/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/03/2019,   referente  ao  interstício  de
05/10/2016 a 11/03/2019,  para o servidor SERGIO MOACIR DE OLIVEIRA CRUZ,  ocupante do cargo de
Assistente em Administração - 701200,  matrícula SIAPE 6358739,  lotado  na  Escola de Enfermagem,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.505757/2019-13:
ILB - Excelência no Atendimento CH: 20 (11/02/2019 a 08/03/2019)
ILB - Deveres, proibições e responsabilidades do servidor público federal CH: 60 (08/10/2018 a 03/12/2018)
ILB - ÉTICA E ADMINISTRAÇÃO PÚBLICA CH: 40 (20/12/2018 a 11/02/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
