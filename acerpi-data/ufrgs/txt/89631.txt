Documento gerado sob autenticação Nº JJX.356.638.EK3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3158                  de  10/04/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5012185-12.2019.4.04.7100,  da  6ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, da servidora MAGDA CAMARGO MAZZILLI, matrícula SIAPE n° 0353788, aposentada no cargo
de Assistente em Administração - 701200, do Nível II para o Nível IV, a contar de 01/01/2006, conforme o
Processo nº 23078.508962/2019-22.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
