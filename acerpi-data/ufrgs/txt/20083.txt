Documento gerado sob autenticação Nº TFW.178.892.QEE, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2433                  de  04/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE:
Conceder  Progressão  por  Capacitação,  a  contar  de  18/12/2015,   referente  ao  interstício  de
03/05/2013 a 17/12/2015, para o servidor TIAGO MAGALHÃES RIBEIRO, ocupante do cargo de Técnico em
Contabilidade - 701224, matrícula SIAPE 1728498,  lotado  na  Faculdade de Ciências Econômicas, passando
do Nível de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV,
em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.030777/2015-95:
Formação Integral de Servidores da UFRGS III CH: 84 (05/09/2011 a 23/11/2015)
ESAF - SIAFI - Módulo Operacional CH: 24 (09/09/2013 a 11/09/2013)
NELE -  English for  academic purposes:  reading and writing -  básico,  nível  A2 -  Programa Idiomas sem
Fronteiras/Inglês CH: 48 Carga horária utilizada: 42 hora(s) / Carga horária excedente: 6 hora(s) (24/06/2015 a
24/09/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
