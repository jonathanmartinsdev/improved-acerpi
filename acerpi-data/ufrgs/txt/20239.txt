Documento gerado sob autenticação Nº GRD.384.706.AD3, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2501                  de  06/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de ISABEL CRISTINA ECHER,  Professor do Magistério Superior,
lotada e em exercício no Departamento de Assistência e Orientação Profissional da Escola de Enfermagem,
com a finalidade de participar da "2016 NANDA International Conference", em Cancun, México, no período
compreendido entre 18/05/2016 e 22/05/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa -
diárias). Solicitação nº 18206.
CARLOS ALEXANDRE NETTO
Reitor
