Documento gerado sob autenticação Nº HSD.482.298.OES, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5241                  de  16/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°27932,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, ocupante do cargo de REVISOR DE TEXTOS, do Quadro
de Pessoal desta Universidade, LUCAS FERREIRA DE ANDRADE (Siape: 2057117 ),  para substituir   LUCIANE
GONCALVES  DELANI  (Siape:  0358459  ),  Chefe  da  Seção  de  Editoração  da  Editora  da  Pró-Reitoria  de
Coordenação Acadêmica, Código FG-5, em seu afastamento no país, no período de 23/05/2017 a 27/05/2017,
com o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
