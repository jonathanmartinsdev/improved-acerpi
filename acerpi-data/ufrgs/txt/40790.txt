Documento gerado sob autenticação Nº KIO.224.978.I0E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6056                  de  10/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Retificar a Portaria n° 6050/2017, de 10/07/2017,  que  lotou a servidora JOSIELI GUIDOLIN ROSSI,
na Superintendência de Infaestrutura, com exercício no Departamento de Suporte à Infraestrutura.
 
Onde se lê:
06 de junho de 2017,
leia-se:
26 de junho de 2017.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
