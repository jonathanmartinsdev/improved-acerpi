Documento gerado sob autenticação Nº ELZ.039.823.DDC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9637                  de  17/10/2017
Nomeação  da  Representação  Discente  do
Programa de Pós-graduação em Microbiologia
Agrícola  e  do  Ambiente  do  Instituto   de
Ciências Básicas da Saúde. 
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear  a  Representação  Discente  eleita  para  compor  o  Programa  de  Pós-graduação  em
Microbiologia Agrícola e do Ambiente do Instituto de Ciências Básicas da Saúde, com mandato de 01 (um)
ano, a contar de 09 de outubro de 2017, atendendo o disposto nos artigos 175 do Regimento Geral da
Universidade  e  79  do  Estatuto  da  Universidade,  e  considerando o  processo  nº  23078.019252/2017-61,
conforme segue:
COMISSÃO DE PÓS-GRADUAÇÃO
Titular: Diandra de Andrades
Suplente: Paulo Roberto Dallcortivo
 
CONSELHO DE PÓS-GRADUAÇÃO:
Titular: Marcela Proença Borba                            
Titular: Heloísa Giacomelli Ribeiro
Suplente: Sílvia Adriana Mayer Lentz
Suplente: Raíssa Nunes dos Santos
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
