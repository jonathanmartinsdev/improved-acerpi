Documento gerado sob autenticação Nº ZGC.218.656.IRF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1783                  de  05/03/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°52591,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de RELAÇÕES PÚBLICAS, do Quadro
de Pessoal desta Universidade, JEAN FELIPE ROSSATO (Siape: 2258751 ),  para substituir   SIMONE DA COSTA
SALDANHA (Siape: 6357032 ), Diretor do Depto Administrativo e de Registro da Extensão da PROREXT, Código
CD-4, em seu afastamento por motivo de férias, no período de 17/02/2020 a 20/02/2020, com o decorrente
pagamento das vantagens por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
