Documento gerado sob autenticação Nº ZMQ.993.551.BCI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2943                  de  04/04/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 28/03/2019,  a ocupante do cargo de Professor do Magistério
Superior,  classe  Associado,  do  Quadro  de  Pessoal  desta  Universidade,  MARIA  IZABEL  SARAIVA  NOLL,
matrícula SIAPE n° 0354093, da função de Vice-Diretora do IFCH, Código SRH 186, código FG-1, para a qual foi
designada pela Portaria 01264/2017, de 09/02/2017, publicada no Diário Oficial da União do dia 13/02/2017.
Processo nº 23078.507911/2019-83.
JANE FRAGA TUTIKIAN
Vice-Reitora.
