Documento gerado sob autenticação Nº WYQ.191.993.ART, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5483                  de  28/06/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar a Portaria n° 5003/2019, de 11/06/2019, que concedeu autorização para afastamento do
País a DAVIDE CARBONAI, Professor do Magistério Superior, com exercício no Departamento de Ciências
Administrativas da Escola de Administração
Onde se lê: com ônus limitado,
leia-se: com ônus CAPES/PROAP, ficando ratificados os demais termos. Solicitação nº 85095.
RUI VICENTE OPPERMANN
Reitor.
