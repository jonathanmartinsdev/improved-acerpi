Documento gerado sob autenticação Nº ZPM.184.900.0QV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2694                  de  28/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  a  ocupante  do  cargo  de  Professor  do  Magistério  Superior,  classe  Associado,  desta
Universidade, FLAVIA MARIA TEIXEIRA DOS SANTOS, matrícula SIAPE n° 1203675, lotada no Departamento de
Ensino e Currículo da Faculdade de Educação, para exercer a função de Coordenadora da Comissão de
Pesquisa da Faculdade de Educação, Código SRH 773, com vigência a partir da data deste ato, pelo período
de 02 anos, ficando o pagamento de gratificação condicionado à disponibilidade de uma função gratificada.
Processo nº 23078.004233/2017-30.
JANE FRAGA TUTIKIAN
Vice-Reitora.
