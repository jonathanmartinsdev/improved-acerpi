Documento gerado sob autenticação Nº LOR.290.978.FJE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6618                  de  21/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar, a partir de 08/06/2017, a ocupante do cargo de Assistente em Administração - 701200,
do Nível de Classificação DIV, do Quadro de Pessoal desta Universidade, SOLANGE BORCELLI DE CASTILHO,
matrícula SIAPE 0355400 da função de Coordenadora do Setor de Contabilidade da Faculdade de Agronomia,
Código SRH 634, Código FG-7, para a qual foi designada pela Portaria nº 1144/15 de 12/02/2015, publicada no
Diário Oficial da União de 20/02/2015. Processo nº 23078.013432/2017-39.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
