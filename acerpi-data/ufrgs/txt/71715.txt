Documento gerado sob autenticação Nº JPS.026.177.T82, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8893                  de  01/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°44996,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, GUSTAVO PEREIRA (Siape: 2267363 ),  para
substituir   TAMIREZ GALVAO DA SILVA PAIM (Siape: 1860717 ), Secretário Administrativo do Instituto Latino-
Americano de Estudos Avançados, Código FG-4, em seu afastamento por motivo de férias, no período de
05/11/2018 a 16/11/2018, com o decorrente pagamento das vantagens por 12 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
