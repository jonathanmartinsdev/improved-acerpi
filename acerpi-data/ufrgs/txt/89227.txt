Documento gerado sob autenticação Nº LDR.499.179.STL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2878                  de  02/04/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  18/03/2019,
correspondente  ao  grau  Insalubridade  Média,  à  servidora  GABRIELA  PEREIRA  DA  SILVA  MACIEL,
Identificação  Única  24075558,  Professor  do  Magistério  Superior,  com  exercício  no  Departamento
Interdisciplinar do Campus Litoral Norte, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de
1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas
Insalubres conforme Laudo Pericial constante no Processo nº 23078.503372/2019-11, Código SRH n° 23799.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
