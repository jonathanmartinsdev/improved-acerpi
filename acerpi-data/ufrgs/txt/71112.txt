Documento gerado sob autenticação Nº YWR.883.283.M1I, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8419                  de  19/10/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de RENATO AZEVEDO MATIAS SILVANO, Professor do Magistério
Superior, lotado e em exercício no Departamento de Ecologia do Instituto de Biociências, com a finalidade de
participar  do  "UK-Japan  Meeting  on  Emerging  Contaminants",  em  Cambridge,  Inglaterra,  no  período
compreendido entre 06/11/2018 e 10/11/2018, incluído trânsito, com ônus limitado. Solicitação nº 60470.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
