Documento gerado sob autenticação Nº SJE.848.172.Q5E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1204                  de  08/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, GIOVANA DOMENEGHINI MERCALI,  matrícula SIAPE n° 2110493, lotada no
Departamento de Ciências dos Alimentos do Instituto de Ciências e Tecnologia de Alimentos, para exercer a
função de Coordenadora da COMGRAD de Engenharia de Alimentos, Código SRH 1213, código FUC, com
vigência a partir da data de publicação no Diário Oficial da União, pelo período de 02 anos. Processo nº
23078.200100/2017-92.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
