Documento gerado sob autenticação Nº DIO.616.052.2IU, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4558                  de  24/05/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°48318,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS, do Quadro de Pessoal desta Universidade, IDA MARIA DE OLIVEIRA (Siape: 1756742 ),  para
substituir   FERNANDA BRASIL MENDES (Siape: 1630339 ), Coordenador do Núcleo de Regulação da Secretaria
de Avaliação Institucional, Código FG-2, em seu afastamento por motivo de férias, no período de 27/05/2019
a 07/06/2019, com o decorrente pagamento das vantagens por 12 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
