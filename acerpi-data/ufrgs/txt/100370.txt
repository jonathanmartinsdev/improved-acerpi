Documento gerado sob autenticação Nº IOB.927.860.P51, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9895                  de  02/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora LAURA GOMES MACHADO, ocupante do cargo de  Arquivista - 701005, lotada
na Pró-Reitoria de Planejamento e Administração, SIAPE 2055436, o percentual de 30% (trinta por cento) de
Incentivo à Qualificação, a contar de 21/10/2019 , tendo em vista a conclusão do curso de Especialização em
Gestão de Documentos e Informações, conforme o Processo nº 23078.527206/2019-01.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
