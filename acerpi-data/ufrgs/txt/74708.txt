Documento gerado sob autenticação Nº TAW.917.183.NQ4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             224                  de  08/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  22/10/2018,   referente  ao  interstício  de
04/03/2016  a  21/10/2018,  para  a  servidora  LUCI  MARI  CASTRO  LEITE  JORGE,  ocupante  do  cargo  de
Instrumentador Cirúrgico - 701207, matrícula SIAPE 1103276,  lotada  no  Hospital de Clínicas Veterinárias,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.500223/2019-92:
Formação Integral de Servidores da UFRGS IV CH: 108 (09/03/2016 a 19/09/2018)
FAVET - XIX Semana Acadêmica da Faculdade de Veterinária CH: 30 Carga horária utilizada: 22 hora(s) / Carga
horária excedente: 8 hora(s) (15/10/2018 a 19/10/2018)
FASUBRA - 1º Encontro de Mulheres da Base da FASUBRA no RS CH: 20 (31/08/2018 a 02/09/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
