Documento gerado sob autenticação Nº SPZ.399.056.0R2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6616                  de  24/08/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  30/08/2018,   referente  ao  interstício  de
19/11/2014 a 29/08/2018, para a servidora LUDMILA SILVA DE OLIVEIRA, ocupante do cargo de Assistente
em Administração  -  701200,  matrícula  SIAPE  1872470,   lotada   na   Faculdade  de  Biblioteconomia  e
Comunicação,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de
Classificação/Nível de Capacitação D IV, em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.521802/2018-98:
Formação Integral de Servidores da UFRGS I CH: 36 (18/08/2014 a 08/06/2018)
ILB - Introdução ao Direito Constitucional CH: 10 (22/08/2014 a 19/10/2014)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 40 (22/01/2018 a 21/02/2018)
ENAP - Gestão da Informação e Documentação - Conceitos Básicos em Gestão Documental CH: 20 Carga
horária utilizada: 4 hora(s) / Carga horária excedente: 16 hora(s) (09/08/2018 a 30/08/2018)
ENAP - Administração Pública CH: 60 (31/10/2017 a 11/12/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
