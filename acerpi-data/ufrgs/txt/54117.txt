Documento gerado sob autenticação Nº WCT.293.448.LVV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3389                  de  09/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de VITOR MANFROI, Professor do Magistério Superior, lotado no
Departamento de Tecnologia  dos Alimentos do Instituto de Ciências  e  Tecnologia  de Alimentos e  com
exercício no Instituto de Ciências e Tecnologia de Alimentos, participar de excursão  em várias cidades da
Califórnia,  Estados Unidos, no período compreendido entre 09/06/2018 e 23/06/2018, incluído trânsito, com
ônus limitado. Solicitação nº 45985.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
