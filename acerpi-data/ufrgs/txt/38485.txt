Documento gerado sob autenticação Nº CTC.665.628.SN1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4414                  de  18/05/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, DENISE ROSSATO SILVA, matrícula SIAPE n° 1369194, lotada no Departamento
de Medicina Interna da Faculdade de Medicina, para exercer a função de Coordenadora do PPG em Ciências
Pneumológicas,  Código SRH 1150,  Código FUC,  com vigência  a  partir  de 28/05/2017 e  até  27/05/2019.
Processo nº 23078.006984/2017-91.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
