Documento gerado sob autenticação Nº VYP.337.512.J7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3737                  de  22/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  MARCO AURELIO PIRES IDIART,  Professor  do Magistério
Superior,  lotado e em exercício no Departamento de Física do Instituto de Física,  com a finalidade de
participar do "Workshop Data Intensive Research on Languages of the Americas", na cidade do Mexico,
Estados Unidos,  no período compreendido entre 23/05/2018 e 26/05/2018,  incluído trânsito,  com ônus
limitado. Solicitação nº 46438.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
