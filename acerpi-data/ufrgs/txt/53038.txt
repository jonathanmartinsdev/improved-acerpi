Documento gerado sob autenticação Nº EMA.118.504.2L6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2670                  de  10/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARIA LUIZA ADAMS SANVITTO,  Professor do Magistério
Superior,  lotada e  em exercício  no Departamento de Arquitetura  da  Faculdade de Arquitetura,  com a
finalidade de participar do "III Congreso Internacional de Vivienda Colectiva  Sostenible", em Guadalajara,
México, no período compreendido entre 13/04/2018 e 19/04/2018, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa - diárias). Solicitação nº 34220.
RUI VICENTE OPPERMANN
Reitor
