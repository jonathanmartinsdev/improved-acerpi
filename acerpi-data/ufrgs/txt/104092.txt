Documento gerado sob autenticação Nº TJT.945.526.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             762                  de  22/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor FABIO GONCALVES TEIXEIRA, matrícula SIAPE n° 1071163, lotado e em exercício no Departamento
de Design e Expressão Gráfica da Faculdade de Arquitetura, da classe D  de Professor Associado, nível 04,
para a classe E  de Professor Titular,  referente ao interstício de 27/11/2017 a 08/01/2020, com vigência
financeira a partir de 09/01/2020, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.500944/2020-36.
RUI VICENTE OPPERMANN
Reitor.
