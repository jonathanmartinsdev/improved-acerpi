Documento gerado sob autenticação Nº KBI.950.373.JA7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             178                  de  04/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°38154,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ROSANE MICHELE DUARTE SOARES (Siape: 2578179 ),
 para substituir   GRISELDA LIGIA BARRERA DE GALLAND (Siape: 0357628 ), Chefe do Depto de Química
Orgânica do Instituto de Química, Código FG-1, em seu afastamento por motivo de férias, no período de
09/01/2018 a 14/01/2018, com o decorrente pagamento das vantagens por 6 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
