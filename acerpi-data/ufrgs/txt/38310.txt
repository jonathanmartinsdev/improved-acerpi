Documento gerado sob autenticação Nº ZKC.153.711.ER0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4277                  de  16/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARIANGELA MACHADO TOALDO, Professor do Magistério
Superior, lotada no Departamento de Comunicação da Faculdade de Biblioteconomia e Comunicação e com
exercício no Comissão de Graduação em Comunicação Social - Publicidade e Propaganda, com a finalidade
da "2ª Missão de Trabalho do Projeto: Matriz comparativa de pesquisas qualitativas com usuários de tecnologias
digitais", em Leuven, Bélgica, no período compreendido entre 16/06/2017 e 25/06/2017, incluído trânsito,
com ônus limitado. Solicitação nº 28089.
RUI VICENTE OPPERMANN
Reitor
