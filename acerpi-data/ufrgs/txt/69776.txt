Documento gerado sob autenticação Nº UPD.141.065.07U, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7332                  de  17/09/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  AIRTON  DAROLD,  Matrícula  SIAPE  1036699,  ocupante  do  cargo  de  Assistente  em
Administração,  Código  701200,  do  Quadro  de  Pessoal  desta  Universidade,  para  exercer  a  função  de
Coordenador do Núcleo Acadêmico da Gerência Administrativa da Faculdade de Arquitetura, Código SRH
644,  código FG-7,  com vigência a partir  da data de publicação no Diário Oficial  da União.  Processo nº
23078.522743/2018-75.
JANE FRAGA TUTIKIAN
Vice-Reitora.
