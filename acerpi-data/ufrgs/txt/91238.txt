Documento gerado sob autenticação Nº MFD.450.749.4TU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 670/2019-PROGESP Porto Alegre, 14 de maio de 2019.
Senhora Diretora
Encaminhamos  o  servidor  Luis  Augusto  Pereira  Duarte,  ocupante  do  cargo  de  Técnico  em
Eletrônica, para exercício nessa Unidade no dia 15 de maio de 2019.
Solicitamos que nos sejam encaminhadas, no prazo máximo de 03 dias, as seguintes informações:
- atividades a serem desenvolvidas pelo servidor;
- local de exercício (divisão, setor), observando a hierarquia dos órgãos registrada no SRH;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal.
Informamos que este encaminhamento se faz para reposição de vaga, em virtude de vacância do
servidor CLODOMIRO FRAGA CASTELLO.
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
Ilma. Sra.
Professora NAIRA MARIA BALZARETTI,
Diretora do Centro de Nanociência e Nanotecnologia,
N/Universidade.
