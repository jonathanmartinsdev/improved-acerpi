Documento gerado sob autenticação Nº FEA.786.084.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9158                  de  10/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de GILBERTO ICLE, Professor do Magistério Superior, lotado e em
exercício no Departamento de Ensino e Currículo da Faculdade de Educação, com a finalidade de realizar
estudos  em  nível  de  Pós-Doutorado  junto  à  Université  Paris  Nanterre,  em  Paris,  França,  no  período
compreendido  entre  01/12/2019  e  30/04/2020,  com  ônus  CAPES/PRINT/UFRGS.  Processo  nº
23078.522401/2019-36.
RUI VICENTE OPPERMANN
Reitor.
