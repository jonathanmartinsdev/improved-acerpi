Documento gerado sob autenticação Nº HHS.650.382.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6545                  de  22/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do país  de ALESSANDRO BERSCH OSVALDT,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Cirurgia da Faculdade de Medicina, com a finalidade de
participar  do "13th World Congress of  International  Hepato-Pancreato-Biliary  Association",  em Genebra,
Suíça,  no  período compreendido entre  04/09/2018 e  08/09/2018,  incluído trânsito,  com ônus  limitado.
Solicitação nº 58639.
RUI VICENTE OPPERMANN
Reitor
