Documento gerado sob autenticação Nº LDL.314.394.VG9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4338                  de  13/06/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor SERGIO BAPTISTA DA SILVA, matrícula SIAPE n° 1051298, lotado e em exercício no Departamento
de Antropologia do Instituto de Filosofia e Ciências Humanas, da classe D  de Professor Associado, nível 03,
para a classe D  de Professor Associado, nível 04, referente ao interstício de 30/12/2013 a 29/12/2015, com
vigência financeira a partir de 10/06/2016, de acordo com o que dispõe a Decisão nº 197/2006-CONSUN,
alterada pela Decisão nº 401/2013-CONSUN. Processo nº 23078.502208/2016-36.
RUI VICENTE OPPERMANN
Vice-Reitor
