Documento gerado sob autenticação Nº DZZ.314.803.F13, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7873                  de  03/10/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ALEXANDRE BATISTA SCHNEIDER,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Química Inorgânica do Instituto de Química, com a
finalidade de participar do "5to Congreso Uruguayo de Química Analítica",  em Montevidéu, Uruguai,  no
período compreendido entre 24/10/2018 e 27/10/2018, incluído trânsito, com ônus limitado. Solicitação nº
59283.
RUI VICENTE OPPERMANN
Reitor
