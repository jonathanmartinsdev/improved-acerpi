Documento gerado sob autenticação Nº CJA.964.241.6A6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             69                  de  03/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de Secretário Executivo, do Quadro
de Pessoal desta Universidade, MÔNICA TAGLIARI KRELING (Siape: 1640598),  para substituir DENISE CYBIS
FONTANA  (Siape:0358734),  Diretora  do  Centro  Estadual  de  Pesquisas  em  Sensoriamento  Remoto  e
Meteorologia, Código SRH 793, FG-1, em seu afastamento por férias, no período de 27/01/2020 a 31/01/2020,
com o decorrente pagamento das vantagens por 5 dias.  Processo SEI Nº 23078.500064/2020-60
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
