Documento gerado sob autenticação Nº MJG.801.272.SNN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3862                  de  25/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar  JORGE  ROBERTO  ESCOUTO  DIAS,  CPF  nº  23695471034,  Matrícula  SIAPE  1446461,
ocupante do cargo de Contador, Código 701015, do Quadro de Pessoal desta Universidade, para exercer a
função de Diretor da Divisão de Ingresso e Análise Socioeconômica do Departamento de Consultoria em
Registros Discentes da Pró-Reitoria de Graduação, Código SRH 1456, Código FG-5, com vigência a partir da
data de publicação no Diário Oficial da União. Processo nº 23078.011988/2016-18.
RUI VICENTE OPPERMANN
Vice-Reitor
