Documento gerado sob autenticação Nº SQI.686.112.QJH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8681                  de  26/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°23904,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CLAITON MARLON DOS SANTOS SCHERER (Siape:
2238460 ),  para substituir   PAULO ALVES DE SOUZA (Siape: 1363666 ), Coordenador do Laboratório de
Palinologia Marleni Marques Toigo, em seu afastamento no país, no período de 06/10/2016 a 09/10/2016.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
