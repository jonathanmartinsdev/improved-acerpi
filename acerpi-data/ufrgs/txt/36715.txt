Documento gerado sob autenticação Nº IPG.496.221.51K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3099                  de  10/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/04/2017,   referente  ao  interstício  de
20/12/2011 a 04/04/2017, para a servidora MICHELE DA SILVA NIMETH RIELLA,  ocupante do cargo de
Administrador - 701001, matrícula SIAPE 1583109,  lotada  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.005845/2017-40:
Formação Integral de Servidores da UFRGS VII CH: 181 Carga horária utilizada: 180 hora(s) / Carga horária
excedente: 1 hora(s) (08/07/2011 a 06/11/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
