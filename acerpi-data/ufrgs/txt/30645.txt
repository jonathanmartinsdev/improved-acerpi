Documento gerado sob autenticação Nº EKE.929.882.FP4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9381                  de  23/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear a ocupante do cargo de Professor do Magistério Superior do Quadro de Pessoal desta
Universidade, LETICIA SCHERER KOESTER, matrícula SIAPE 2412568, para exercer o cargo de Presidente da
Câmara de Pós-Graduação, Código SRH 55, Código CD-4, com vigência a partir da data de publicação no
Diário Oficial  da União e até 07/05/2018,  a  fim de completar o mandato do professor Celso Giannetti
Loureiro Chaves. Processo nº 23078.023472/2016-16.
RUI VICENTE OPPERMANN
Reitor
