Documento gerado sob autenticação Nº OCL.795.545.2TA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3964                  de  02/06/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder o Adicional de Periculosidade, no percentual de 10%, a partir de 09/03/2016, ao servidor
ALEXANDRE ALVES PELLIN,  Identificação Única 23015934, Engenheiro-área, com exercício na Prefeitura
Campus Saúde e Olímpico (SP2) da Superintendência de Infraestrutura, observando-se o disposto na Lei nº
8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer
atividades  em  áreas  consideradas  Perigosas  conforme  Laudo  Pericial  constante  no  Processo  n  º
23078.503166/2016-51, Código SRH n° 22795 e Código SIAPE n° 2016001761.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
