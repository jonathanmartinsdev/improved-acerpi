Documento gerado sob autenticação Nº DVU.848.489.S2I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4078                  de  05/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora SOLANGE MITTMANN, matrícula SIAPE n° 2099597, lotada e em exercício no Departamento de
Letras Clássicas e Vernáculas do Instituto de Letras, da classe D  de Professor Associado, nível 02, para a
classe D  de Professor Associado, nível 03, referente ao interstício de 18/04/2015 a 17/04/2017, com vigência
financeira a partir de 01/06/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.503813/2018-96.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
