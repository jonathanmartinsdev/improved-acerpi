Documento gerado sob autenticação Nº EHP.873.040.S2C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6560                  de  21/07/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7633, de 29 de
setembro de 2016 e, tendo em vista o que consta dos Processos Administrativos n° 23078.510719/2016-21 e
23078.507808/2017-71 da Lei 10.520/02, do Contrato nº 044/PROPLAN/NUDECON/2017 e da Lei 8.666/93,
            RESOLVE:
 
          Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item I, da Cláusula Décima-Primeira do
referido Contrato, à Empresa NEVES E ROMANOSKLI LTDA - ME, CNPJ 15.717.915/0001-90, pela falta de
itens  no  cardápio,  substituições  de  itens  sem prévio  aviso,  problemas  na  temperatura  dos  alimentos,
conforme atestado pelo CLN (documento SEI nº 0555322 e 0587569), bem como pelo NUDECON (documento
SEI nº 0590247) do processo administrativo supracitado.
 
            Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
LUIS ROBERTO DA SILVA MACEDO
Pró-Reitor de Planejamento e Administração em exercício
