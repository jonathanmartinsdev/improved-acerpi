Documento gerado sob autenticação Nº MDN.896.774.NF8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1586                  de  14/02/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Tornar insubsistente a Portaria nº 829/2020, de 23/01/2020, publicada no Diário Oficial da União de
27/01/2020, que designou ADALBERTO DA COSTA OLIVEIRA, ocupante do cargo de Técnico em Audiovisual,
lotado e em exercício na Faculdade de Biblioteconomia e Comunicação, como Gerente de Espaço Físico da
Faculdade de Biblioteconomia e Comunicação.
JANE FRAGA TUTIKIAN
Vice-Reitora.
