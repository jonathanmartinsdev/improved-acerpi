Documento gerado sob autenticação Nº KBV.279.996.BPQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3456                  de  10/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, JEFERSON VIEIRA FLORES, CPF n° 82706409053, matrícula SIAPE n° 2122219,
lotado no Departamento de Engenharia Elétrica da Escola de Engenharia, como Coordenador Substituto da
COMGRAD  de  Engenharia  Elétrica,  para  substituir  automaticamente  o  titular  desta  função  em  seus
afastamentos  ou  impedimentos  regulamentares  na  vigência  do  presente  mandato.  Processo  nº
23078.006822/2016-71.
RUI VICENTE OPPERMANN
Vice-Reitor
