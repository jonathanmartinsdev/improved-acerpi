Documento gerado sob autenticação Nº GFL.365.040.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1997                  de  28/02/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LAURA BANNACH JARDIM, Professor do Magistério Superior,
lotada e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a finalidade de
participar do "1st Panamerican Meeting of Hereditary Ataxias", em Havana, Cuba, no período compreendido
entre 21/03/2019 e 24/03/2019, incluído trânsito, com ônus limitado. Solicitação nº 62135.
RUI VICENTE OPPERMANN
Reitor
