Documento gerado sob autenticação Nº ILR.232.785.TAR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3842                  de  06/05/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  02/05/2019,   referente  ao  interstício  de
24/10/2017 a 01/05/2019, para a servidora MARIA LUCIA RICARDO SOUTO, ocupante do cargo de Técnico
em Restauração - 701260, matrícula SIAPE 2579219,  lotada  na  Biblioteca Central, passando do Nível de
Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.511237/2019-31:
Formação Integral de Servidores da UFRGS IV CH: 92 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 2 hora(s) (08/11/2017 a 13/12/2018)
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
