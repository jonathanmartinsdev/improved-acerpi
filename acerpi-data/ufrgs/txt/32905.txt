Documento gerado sob autenticação Nº GLI.650.411.R3C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             405                  de  16/01/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°34566,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  RICARDO  ANTONIO  LUCAS  CAMARGO  (Siape:
1853376 ),  para substituir   VANESSA CHIARI GONÇALVES (Siape: 1330550 ), Coordenador da Comissão de
Pesquisa da Faculdade de Direito, em seu afastamento por motivo de férias, no período de 21/01/2017 a
06/02/2017.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
