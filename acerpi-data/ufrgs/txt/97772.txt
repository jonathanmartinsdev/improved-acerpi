Documento gerado sob autenticação Nº PPZ.013.752.B97, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8152                  de  06/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Retificar a Portaria n° 8131/2019, de 06/09/2019 que concedeu promoção funcional, por obtenção
do título de Mestre, no Quadro desta Universidade, à Professora LETICIA MEDEIROS DA SILVA, matrícula
SIAPE 1759613, lotada e em exercício no Departamento de Ciências Contábeis e Atuariais da Faculdade de
Ciências Econômicas, da classe B de Professor Assistente, nível 01, para a classe C de Professor Adjunto, nível
01, com vigência financeira a partir de 04/06/2019, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro de 2012, com suas alterações, Portaria nº 554, de 20 de junho de 2013 do Ministério da Educação e
a Decisão nº 331/2017. Processo nº 23078.514525/2019-48.
 
Onde se lê:
...por obtenção do título de Mestre...
 
Leia-se:
...por obtenção do título de Doutor, ficando ratificados os demais termos.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
