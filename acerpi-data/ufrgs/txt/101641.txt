Documento gerado sob autenticação Nº IUB.629.511.L7A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10752                  de  02/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito a partir de 06/11/2019 a Portaria nº 7457/2018, de 20/09/2018, que nomeou o
Núcleo de Gestão de Desempenho do Instituto de Ciências Básicas da Saúde.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
