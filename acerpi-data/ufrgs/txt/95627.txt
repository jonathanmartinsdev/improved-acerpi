Documento gerado sob autenticação Nº ZOP.611.118.M30, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7080                  de  06/08/2019
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
Designar os servidores  BRUNO INOCÊNCIO HENRIQUE, ocupante do cargo de Engenheiro-área,
lotado na Superintendência de Infraestrutura e com exercício no Setor de Orçamentos e Prevenção contra
Incêndios,  EDSON  SANDRI  PACHECO,  ocupante  do  cargo  de  Contador,  lotado  na  Pró-Reitoria  de
Planejamento  e  Administração  e  com  exercício  na  Divisão  de  Execução  Orçamentária,  GUILHERME
LEHNEMANN RAMOS, ocupante do cargo de Contador, lotado na Superintendência de Infraestrutura e com
exercício no Setor Financeiro da Superintendência de Infraestrutura, para, sob a presidência do primeiro,
comporem Comissão para no prazo de 15 dias a partir de 12 de agosto de 2019, apontar os valores a serem
glosados  da  empresa  MEGATRON ENGENHARIA LTDA,   conforme item 27 do Parecer  Nº  453/2019/PF-
UFRGS/AGU, constante nos autos do processo 23078.524397/2017-89.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
