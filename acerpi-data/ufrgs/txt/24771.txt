Documento gerado sob autenticação Nº HOL.420.929.CCG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5372                  de  18/07/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a SONIA BEATRIZ COPPINI,
matrícula  SIAPE  nº  0355411,  no  cargo  de  Diretor  de  Artes  Cênicas,  nível  de  classificação  E,  nível  de
capacitação  II,  padrão  16,  do  Quadro  desta  Universidade,  no  regime de  quarenta  horas  semanais  de
trabalho,  com  exercício  no  Planetário  da  Pró-Reitoria  de  Extensão,  com  proventos  integrais.  Processo
23078.014159/2016-89.
CARLOS ALEXANDRE NETTO
Reitor
