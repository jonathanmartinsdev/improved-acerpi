Documento gerado sob autenticação Nº HPX.196.517.92T, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2029                  de  15/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de JACOB SCHARCANSKI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a finalidade de
visita  à Universidade de Coimbra,  Portugal, no período compreendido entre 10/04/2016 e 19/04/2016,
incluído trânsito, com ônus limitado. Solicitação nº 18467.
CARLOS ALEXANDRE NETTO
Reitor
