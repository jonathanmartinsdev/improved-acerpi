Documento gerado sob autenticação Nº HGK.050.828.CKG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             847                  de  24/01/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  SERGIO  FRANCISCO  SCHWARZ,  Professor  do  Magistério
Superior, lotado no Departamento de Horticultura e Silvicultura da Faculdade de Agronomia e com exercício
na  Comissão  de  Graduação  de  Agronomia,  com  a  finalidade  de  participar  de  reunião  do
Projeto:  "Cooperación internacional en carreras de Ingeniería Agronómica del  MERCOSUR: mejora de la
enseñanza, de los proyectos académicos y de las capacidades de las universidades" junto à Universidad
Nacional del Nordeste, em Corrientes, Argentina, no período compreendido entre 13/02/2019 e 16/02/2019,
incluído trânsito, com ônus UFRGS (Secretaria de Relações Internacionais: diárias e passagens). Solicitação nº
61519.
RUI VICENTE OPPERMANN
Reitor
