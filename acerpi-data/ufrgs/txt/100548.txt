Documento gerado sob autenticação Nº QPA.138.364.TD6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10054                  de  07/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ELISMAR DA ROSA OLIVEIRA, Professor do Magistério Superior,
lotado no Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística e com
exercício no Instituto de Matemática e Estatística, com a finalidade de realizar visita à Universidad Nacional
de San Luis, em San Luis, Argentina, no período compreendido entre 10/11/2019 e 16/11/2019, incluído
trânsito, com ônus CAPES/MATH-AmSud. Solicitação nº 87481.
RUI VICENTE OPPERMANN
Reitor
