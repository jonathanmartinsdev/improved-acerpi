Documento gerado sob autenticação Nº RWY.351.144.VH1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             8866                  de  03/11/2016
O P´RÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016 
RESOLVE
Retificar  a  Portaria  n°  7649/2016,  de 29/09/2016,  que concedeu progressão por  capacitação à
servidora CAMILA VIVIANE LOPES,  Engenheiro-área, com exercício no Departamento de Adequações de
Infraestrutura da Superintendência de Infraestrutura, conforme processo nº 23078.203334/2016-19.
 
               Onde se lê:
               "Formação Integral de Servidores da UFRGS I CH: 22 (15/03/2016 a 19/08/2016)
              ESAF - Elaboração de Planilhas de Orçamentos de Obras CH: 24 (12/05/2014 a 14/05/2014)
              ENAP - Gestão e Fiscalização de Contratos Administrativos - Turma 2/2015 CH: 40 (12/05/2015 a
15/06/2015)                                                                                    
              ENAP - Gestão e Fiscalização de Contratos Administrativos - Nível Intermediário CH: 40
              Carga horária utilizada: 34 hora(s) / Carga horária excedente: 6 hora(s) (12/05/2015 a 15/06/2015)
              TCU - Curso Estruturas de Gestão Pública CH: 30 (28/04/2014 a 26/05/2014)"
 
               Leia-se:
               "Formação Integral de Servidores da UFRGS III CH: 62 (15/03/2016 a 19/08/2016)
                ESAF - Elaboração de Planilhas de Orçamentos de Obras CH: 24 (12/05/2014 a 14/05/2014)
                ENAP - Gestão e Fiscalização de Contratos Administrativos - Nível Intermediário CH: 40 (12/05/2015
 a 15/06/2015)
               Carga horária utilizada: 34 hora(s) / Carga horária excedente: 6 hora(s)
               TCU - Curso Estruturas de Gestão Pública CH: 30 (28/04/2014 a 26/05/2014)"
 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
Documento gerado sob autenticação Nº RWY.351.144.VH1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
