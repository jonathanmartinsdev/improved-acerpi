Documento gerado sob autenticação Nº SUR.676.040.GTG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9425                  de  16/10/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual  de Incentivo à Qualificação concedido à servidora NALIN FERREIRA DA
SILVEIRA,  ocupante do cargo de Bibliotecário-documentalista-701010, lotada no Instituto de Biociências,
SIAPE 2329143, para 52% (cinquenta e dois por cento), a contar de 16/10/2019, tendo em vista a conclusão
do Mestrado Profissional em Informática na Educação, conforme o Processo nº 23078.525941/2019-71.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
