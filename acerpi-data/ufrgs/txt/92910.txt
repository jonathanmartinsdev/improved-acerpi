Documento gerado sob autenticação Nº ZPH.600.021.S2C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5252                  de  21/06/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor PAULO JOSUÉ GOULART DA SILVA,  ocupante do cargo de  Técnico em
Audiovisual - 701221, lotado na Faculdade de Educação, SIAPE 1334434, o percentual de 25% (vinte e cinco
por cento) de Incentivo à Qualificação, a contar de 28/05/2019, tendo em vista a conclusão do Curso Superior
de Tecnologia em Produção Multimídia, conforme o Processo nº 23078.513746/2019-07.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
