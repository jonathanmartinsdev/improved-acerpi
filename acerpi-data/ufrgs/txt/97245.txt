Documento gerado sob autenticação Nº YUU.202.201.PKK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7806                  de  28/08/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°85892,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  ADRIANA COELHO BORGES  KOWARICK (Siape:
3225570 ),  para substituir   ELIANE LOURDES DA SILVA MORO (Siape: 1150045 ), Coordenador da Comissão
de Extensão da FABICO, em seu afastamento no país, no período de 29/08/2019 a 31/08/2019.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
