Documento gerado sob autenticação Nº BTA.158.828.UKB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2473                  de  21/03/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, PAULO AUGUSTO NETZ, matrícula SIAPE n° 1535113, lotado no Departamento
de Físico-Química do Instituto de Química, para exercer a função de Coordenador da COMGRAD de Química,
Código  SRH  1233,  código  FUC,  com  vigência  a  partir  de  01/04/2017  e  até  31/03/2019.  Processo  nº
23078.204335/2016-72.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
