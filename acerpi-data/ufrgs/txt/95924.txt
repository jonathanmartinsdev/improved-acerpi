Documento gerado sob autenticação Nº JMM.032.702.SFJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7106                  de  07/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder Progressão por Mérito Profissional, nos termos do artigo 10-A da Lei nº 11.091, de 12 de
janeiro  de  2005,  e  da  Decisão nº  939,  de  21  de  novembro de  2008,  do  Conselho Universitário  desta
Universidade, para o servidor JADDER FREITAS SOUZA, matrícula SIAPE 1912521, Nível de Classificação C ,
para o padrão de vencimento 06, com vigência a partir de 21 de julho de 2019.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
