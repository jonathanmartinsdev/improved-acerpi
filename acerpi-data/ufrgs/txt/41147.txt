Documento gerado sob autenticação Nº POT.689.136.7L6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6371                  de  17/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora RITA DE CÁSSIA DOS SANTOS SILVEIRA, matrícula SIAPE n° 1267026, lotada e em exercício no
Departamento de Pediatria da Faculdade de Medicina, da classe D  de Professor Associado, nível 02, para a
classe D  de Professor Associado, nível 03, referente ao interstício de 04/07/2015 a 03/07/2017, com vigência
financeira a partir de 04/07/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012,
com  suas  alterações  e  a  Decisão  nº  197/2006-CONSUN,  alterada  pela  Decisão  nº  401/2013-CONSUN.
Processo nº 23078.508751/2017-28.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
