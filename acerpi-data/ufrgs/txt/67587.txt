Documento gerado sob autenticação Nº XSZ.912.325.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5923                  de  07/08/2018
  Dispõe  sobre  a  criação  de  Grupo  de
Trabalho de Saúde Mental do Estudante e
designa integrantes.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
              Art. 1º Constituir Grupo de Trabalho de Saúde Mental do Estudante, com o objetivo de discutir e
analisar as principais demandas sobre saúde mental relatadas pelas Comissões de Graduação, com vistas a
construir  um protocolo para atuação e um fluxograma para encaminhamento de casos de sofrimento
psíquico no corpo discente.
 
              Art. 2º Designar os integrantes abaixo listados para, sob coordenação da primeira, comporem o
Grupo de Trabalho:
 
Cristina Rolim Neumann
Andrea Fachel Leal
Bruna Molina Leal
Flávia Wagner
Giovana Freitas Bavaresco
Leticia Prezzi Fernandes
Lisia Von Diemen
Manoela Horowitz Petersen
Marilia Borges Hackmann
Naiade Salinos Teles
Thaís Ferrugem Sarmento
RUI VICENTE OPPERMANN,
Reitor.
