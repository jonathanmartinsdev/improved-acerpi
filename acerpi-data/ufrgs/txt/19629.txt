Documento gerado sob autenticação Nº FHG.582.809.UTK, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2207                  de  22/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de PAULO ANTONIO BARROS OLIVEIRA, Professor do Magistério
Superior,  lotado e em exercício no Departamento de Medicina Social da Faculdade de Medicina, com a
finalidade de participar, como  palestrante, do "XVIII Congreso Internacional de Ergonomía", em Tijuana,
México, no período compreendido entre 19/04/2016 e 24/04/2016, incluído trânsito, com ônus limitado.
Solicitação nº 17937.
CARLOS ALEXANDRE NETTO
Reitor
