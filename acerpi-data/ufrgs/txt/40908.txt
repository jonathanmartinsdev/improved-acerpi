Documento gerado sob autenticação Nº VCT.170.842.0I7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6149                  de  12/07/2017
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias, 
RESOLVE
Designar LUIZ FERNANDO BARBOSA AGUIRRE, Matrícula SIAPE 0356210, ocupante do cargo de
Assistente em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a
função de Coordenador do Núcleo de Infraestrutura da Gerência Administrativa da Faculdade de Arquitetura,
Código SRH 633, código FG-7, com vigência a partir da data de publicação no Diário Oficial da União. Processo
nº 23078.012718/2017-05.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
