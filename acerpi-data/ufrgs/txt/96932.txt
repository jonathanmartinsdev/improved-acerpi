Documento gerado sob autenticação Nº RNE.430.946.4O1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7620                  de  22/08/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Dispensar, a pedido, a partir de 01 de setembro de 2019,  o ocupante do cargo de Porteiro - 701458,
do Nível de Classificação CIV, do Quadro de Pessoal desta Universidade, EZEQUIEL DA ROSA MEDEIROS,
matrícula SIAPE 0358455 da função de Coordenador do Núcleo de Infraestrutura da Gerência Administrativa
da Escola de Educação Física, Fisioterapia e Dança, Código SRH 766, Código FG-7, para a qual foi designado
pela Portaria nº 2869/2017 de 03/04/2017, publicada no Diário Oficial da União de 04/04/2017. Processo nº
23078.522454/2019-57.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
