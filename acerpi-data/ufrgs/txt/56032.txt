Documento gerado sob autenticação Nº FCW.246.573.1E5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4977                  de  11/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria n° 4645/2018, de 27/06/2018, que lotou na Pró-Reitoria de Pós-Graduação o servidor
JADDER FREITAS SOUZA, Matrícula SIAPE n° 1912521, redistribuído conforme Portaria nº 900 de 21 de maio
de 2018, do Ministério da Educação, publicada no Diário Oficial  da União no dia 23 de maio de 2018,
ocupante do cargo de Auxiliar em Administração, Classe C, Nível III, Padrão de Vencimento 04, no Quadro de
Pessoal desta Universidade. Processo 23041.009650/2018-77.
 
Onde se lê:
 
"[...] Classe C, Nível III, Padrão de Vencimento 04 [...]"
 
leia-se:
"[...] Classe C, Nível IV, Padrão de Vencimento 05 [...]"
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
