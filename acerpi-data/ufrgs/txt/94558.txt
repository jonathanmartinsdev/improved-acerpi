Documento gerado sob autenticação Nº LBP.246.413.GD0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6474                  de  23/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor MILTON DE SOUZA MENDONCA JUNIOR, matrícula SIAPE n° 1517254, lotado e em exercício no
Departamento de Ecologia do Instituto de Biociências, da classe D  de Professor Associado, nível 02, para a
classe D  de Professor Associado, nível 03, referente ao interstício de 08/03/2017 a 07/03/2019, com vigência
financeira  a  partir  de  08/03/2019,  conforme  decisão  judicial  proferida  no  processo  nº  5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.505472/2019-74.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
