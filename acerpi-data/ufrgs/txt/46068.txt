Documento gerado sob autenticação Nº XFR.293.606.MSF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9642                  de  17/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de VITOR MANFROI, Professor do Magistério Superior, lotado no
Departamento de Tecnologia  dos Alimentos do Instituto de Ciências  e  Tecnologia  de Alimentos e  com
exercício no Instituto de Ciências e Tecnologia de Alimentos, com a finalidade de participar de encontro junto
ao Montpellier SupAgro - Institut National d'Études Supérieures Agronomiques de Montpellier, em Bordeaux,
Montpellier e Dijon, França, no período compreendido entre 07/11/2017 e 27/11/2017, incluído trânsito, com
ônus FINEP. Solicitação nº 32133.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
