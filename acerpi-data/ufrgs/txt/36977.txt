Documento gerado sob autenticação Nº LHN.447.577.UP3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3247                  de  17/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Exonerar, a pedido, a partir de 7 de abril de 2017, nos termos do artigo 34 "caput", da Lei n° 8.112,
de 1990, ANA RITA CALDART, ocupante do cargo de Assistente em Administração, código 701200, nível de
classificação D, nível de capacitação III, padrão 03, do Quadro de Pessoal, lotada e com exercício no Instituto
de Matemática e Estatística. Processo nº 23078.003593/2017-14.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
