Documento gerado sob autenticação Nº PHA.463.935.SNE, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1902                  de  11/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  01/08/2015,
correspondente ao grau Insalubridade Média, ao servidor LUIS HENRIQUE SANTOS CANANI, Identificação
Única  15527621,  Professor  do  Magistério  Superior,  com exercício  no  Programa de  Pós-Graduação  em
Ciências Médicas: Endocrinologia da Faculdade de Medicina, observando-se o disposto na Lei nº 8.112, de 11
de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em
áreas consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.021026/2015-88,
Código SRH n° 22699 e Código SIAPE 2016000614.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
