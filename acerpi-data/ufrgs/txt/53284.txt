Documento gerado sob autenticação Nº ZSO.117.455.BBF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2823                  de  17/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CASSIANO KUCHENBECKER ROSING, Professor do Magistério
Superior,  lotado no Departamento  de  Odontologia  Conservadora  da  Faculdade de  Odontologia  e  com
exercício no Programa de Pós-Graduação em Odontologia, com a finalidade de ministrar disciplina junto à
Universidad Científica del Sur, em Lima, Peru, no período compreendido entre 22/04/2018 e 27/04/2018,
incluído trânsito, com ônus limitado. Solicitação nº 45445.
RUI VICENTE OPPERMANN
Reitor
