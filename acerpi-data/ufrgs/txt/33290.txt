Documento gerado sob autenticação Nº EXN.185.807.5OP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             639                  de  23/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias,  tendo em vista o constante no Regimento Interno do
Conselho Universitário, a Decisão nº 001/2017-CONSUN e o resultado da eleição realizada em 20 de janeiro
de 2017,
RESOLVE:
Designar
      
      NADYA PESCE DA SILVEIRA
para a presidência da Comissão de Ensino, Pesquisa, Extensão e Recursos do Conselho Universitário.
JANE FRAGA TUTIKIAN,
VICE-REITORA, NO EXERCÍCIO DA REITORIA.
