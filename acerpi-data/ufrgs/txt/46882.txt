Documento gerado sob autenticação Nº ZLF.399.422.4B3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10330                  de  08/11/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de OSVALDO DE LAZARO CASAGRANDE JUNIOR, Professor do
Magistério Superior, lotado e em exercício no Departamento de Química Inorgânica do Instituto de Química,
com a finalidade de realizar   visita   à  University  of  Rennes I,  França,  no período compreendido entre
07/12/2017 e 18/12/2017, incluído trânsito, com ônus CAPES/COFECUB. Solicitação nº 31390.
RUI VICENTE OPPERMANN
Reitor
