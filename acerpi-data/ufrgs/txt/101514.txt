Documento gerado sob autenticação Nº OSB.792.172.CKM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10656                  de  28/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Remover, a partir de 25 de novembro de 2019,  de acordo com o artigo 36 da Lei n° 8.112, de 11
de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997, MARIA APARECIDA
VEIGA RODRIGUES,  ocupante do cargo de Nutricionista-habilitação, Ambiente Organizacional Ciências da
Saúde, Código 701055, Classe E, Nível de Capacitação IV, Padrão de Vencimento 14, SIAPE nº 1217564 da Pró-
Reitoria de Assuntos Estudantis para lotação no Colégio de Aplicação, com exercício na Cantina do Colégio de
Aplicação.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
