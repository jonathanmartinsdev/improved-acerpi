Documento gerado sob autenticação Nº QLT.863.930.DI6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10939                  de  04/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de NICOLAS BRUNO MAILLARD, Professor do Magistério Superior,
lotado no Departamento de Informática Aplicada do Instituto de Informática e com exercício na Secretaria de
Relações Internacionais, com a finalidade de participar da "12th Global Confucius Institute Conference", em
Pequim, China, no período compreendido entre 07/12/2017 e 17/12/2017, incluído trânsito, com ônus UFRGS
(meias diárias). Solicitação nº 33425.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
