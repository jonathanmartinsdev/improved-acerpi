Documento gerado sob autenticação Nº REC.235.621.5A0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5945                  de  07/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor DIMITRI DE AVILA CERVO, matrícula SIAPE n° 1358977, lotado e em exercício no Departamento de
Música do Instituto de Artes, da classe D  de Professor Associado, nível 03, para a classe D  de Professor
Associado, nível 04, referente ao interstício de 06/02/2016 a 05/02/2018, com vigência financeira a partir de
01/03/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a
Decisão nº 331/2017 do CONSUN. Processo nº 23078.500466/2018-40.
JANE FRAGA TUTIKIAN
Vice-Reitora.
