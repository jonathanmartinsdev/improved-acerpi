Documento gerado sob autenticação Nº WNJ.184.576.4RG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8746                  de  26/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar MARILAINE DE FRAGA SANT ANA, matrícula SIAPE n° 1530937, ocupante do cargo de
Professor do Magistério Superior, classe Associado, lotada no Departamento de Matemática Pura e Aplicada
do Instituto de Matemática e Estatística, do Quadro de Pessoal da Universidade, para exercer a função de
Coordenadora Substituta do PPG em Ensino de Matemática, com vigência a partir da data deste ato e até
20/08/2017, a fim de completar o mandato  da Professora ELISABETE ZARDO BURIGO, conforme artigo 92 do
Estatuto da mesma Universidade. Processo nº 23078.203554/2016-34.
JANE FRAGA TUTIKIAN
Vice-Reitora
