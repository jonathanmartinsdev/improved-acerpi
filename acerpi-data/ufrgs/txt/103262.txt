Documento gerado sob autenticação Nº ETL.341.619.RNQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             179                  de  04/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°54087,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA,  do Quadro de Pessoal  desta  Universidade,  LOUIDI  LAUER ALBORNOZ (Siape:  2027001 ),   para
substituir   JOAO JULIO KLUSENER (Siape: 1542645 ), Coordenador do Núcleo de Infraestrutura da Gerência
Administrativa do Instituto de Pesquisas Hidráulicas, Código FG-7, em seu afastamento por motivo de férias,
no período de 06/01/2020 a 17/01/2020.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
