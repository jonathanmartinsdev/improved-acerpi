Documento gerado sob autenticação Nº FDX.125.769.0QV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2670                  de  28/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 21/03/2017, a ocupante do cargo de Assistente em Administração - 701200,
do Nível de Classificação DIV, do Quadro de Pessoal desta Universidade, ANGELA MARIA SEVERO LERINA,
matrícula SIAPE 0358711, da função de Gerente Administrativo da PROPLAN, Código SRH 1309, Código FG-1,
para a qual foi designada pela Portaria nº 3626/13 de 14/06/2013, publicada no Diário Oficial da União de
18/06/2013. Processo nº 23078.004185/2017-80.
JANE FRAGA TUTIKIAN
Vice-Reitora.
