Documento gerado sob autenticação Nº YTX.892.293.FMT, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1303                  de  23/02/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar  o  afastamento  do  país  de  MARCIA  CRISTINA  BERNARDES  BARBOSA,  Professor  do
Magistério Superior, lotada no Departamento de Física do Instituto de Física e com exercício no Instituto de
Física, com a finalidade de realizar visita ao Institute of Physical Chemistry Rocasolano, em Madri, Espanha,
no período compreendido entre 12/03/2016 e 20/03/2016, incluído trânsito, com ônus limitado. Solicitação nº
18084.
CARLOS ALEXANDRE NETTO
Reitor
