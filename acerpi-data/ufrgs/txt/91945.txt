Documento gerado sob autenticação Nº XYY.165.917.GCI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4618                  de  28/05/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, CARMEN BEATRIZ BORGES FORTES,  matrícula SIAPE n° 0351593, lotada no
Departamento de Odontologia Conservadora, para exercer a função de Coordenadora da COMGRAD de
Odontologia, Código SRH 1230, código FUC, com vigência a partir de 14/06/2019 até 13/06/2021, por ter sido
reeleita. Processo nº 23078.513570/2019-85.
RUI VICENTE OPPERMANN
Reitor.
