Documento gerado sob autenticação Nº UFR.870.543.M4O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10160                  de  22/12/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Assistente em Administração, classe D, do Quadro de Pessoal
desta Universidade,  MATHEUS BITENCOURT DA COSTA,  matrícula SIAPE n° 1964887,  lotado no Campus
Litoral Norte, como Coordenador Substituto da Comissão de Extensão da Diretoria Acadêmica do Campus
Litoral  Norte,  para  substituir  automaticamente  o  titular  desta  função  em  seus  afastamentos  ou
impedimentos regulamentares na vigência do presente mandato. Processo nº 23078.024986/2016-81.
JANE FRAGA TUTIKIAN
Vice-Reitora
