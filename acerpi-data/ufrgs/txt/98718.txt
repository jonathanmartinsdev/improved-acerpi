Documento gerado sob autenticação Nº VUF.433.781.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8810                  de  27/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de PEDRO LUIS GRANDE, Professor do Magistério Superior, lotado
e  em  exercício  no  Departamento  de  Física  do  Instituto  de  Física,  com  a  finalidade  de  realizar  visita
à  Helmholtz  Zentrum  Dresden  Rossendorf,  em  Dresden,  Alemanha,  e  partipar  da  "24th  International
Conference on Ion Beam Analysis",  em Antibes,  França,  no período compreendido entre  05/10/2019 e
20/10/2019, incluído trânsito, com ônus CAPES/PRINT/UFRGS. Solicitação nº 87211.
RUI VICENTE OPPERMANN
Reitor
