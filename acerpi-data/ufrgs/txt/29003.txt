Documento gerado sob autenticação Nº ATB.067.067.32Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8114                  de  10/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº 23078.022702/2016-11
 
 
 
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior,  do Quadro de Pessoal desta Universidade, EDUARDO LUIS KONRATH (Siape: 2859290),   para
substituir  automaticamente  TEMISTOCLES  AMERICO  CORREA  CEZAR  (Siape:  0421114  ),  Presidente  da
Câmara de Pesquisa, Código SRH 56, CD-4, em seu sua Licença Gala, no período de 09/09/2016 a 16/09/2016,
com o decorrente pagamento das vantagens por 8 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
