Documento gerado sob autenticação Nº BHH.354.721.731, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9301                  de  14/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de AFONSO REGULY, Professor do Magistério Superior, lotado no
Departamento de Metalurgia da Escola de Engenharia e com exercício no Programa de Pós-Graduação em
Engenharia de Minas, Metalúrgica e de Materiais, com a finalidade de realizar visita ao Instituto Superior
Técnico de Lisboa e participar da "1st International Conference on Advanced Joining Processes (AJP 2019), em
Lisboa e Azores, Portugal, no período compreendido entre 19/10/2019 e 26/10/2019, incluído trânsito, com
ônus limitado. Solicitação nº 87593.
RUI VICENTE OPPERMANN
Reitor
