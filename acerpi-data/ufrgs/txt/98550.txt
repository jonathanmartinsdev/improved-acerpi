Documento gerado sob autenticação Nº EXS.207.700.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8712                  de  25/09/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Lotar no Departamento de Psicanálise e Psicopatologia do Instituto de Psicologia, área Psicologia,
com ênfase em Intervenção Terapêutica, a partir de 19/08/2019, CLAUDIA MARIA PERRONE, matrícula SIAPE
n° 2367562, ocupante do cargo de Professor do Magistério Superior, classe Associado, removida  por Decisão
Judicial, conforme Portaria nº  95.212, de 29 de julho de 2019, da Universidade Federal de Santa Maria, e
considerando o que consta no Processo Judicial n°  5027473-97.2019.4.04.7100, da Seção Judiciária do Rio
Grande do Sul, 3ª Vara Federal  de Porto Alegre, no Parecer de Força Executória da Procuradoria Regional
Federal da 4ª Região, n° 035/2019, de 26/07/2019, e no Processo UFSM 23081.040089/2019-16. Processo
UFRGS nº 23078.514226/2019-11.
RUI VICENTE OPPERMANN
Reitor.
