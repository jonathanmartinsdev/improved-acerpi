Documento gerado sob autenticação Nº IID.323.854.G4L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7285                  de  09/08/2017
Nomeação  da  Representação  Discente  dos
órgãos colegiados da Escola de Administração
da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita na Escola de Administração, com mandato de 01 (um) ano,
a  contar  de  01  de  janeiro  de  2017,  atendendo  o  disposto  nos  artigos  175  do  Regimento  Geral  da
Universidade  e  79  do  Estatuto  da  Universidade,  e  considerando o  processo  nº  23078.014975/2017-73,
conforme segue:
Conselho da Universidade (CONSUNI)
Titular: Cássia Corrêa de Sá Caetano
Suplente: Matheus de Casaes dos Passos
Colegiado
Titular: Bárbara de Souza Paim
Suplente: Cássia Corrêa de Sá Caetano
Comissão de Graduação (COMGRAD)
Titular: Matheus de Casaes dos Passos
Suplente: Cássia Corrêa de Sá Caetano
Comissão de Pesquisa (COMPESQ)
Titular: Vitória da Silveira dos Santos
Suplente: Cássia Corrêa de Sá Caetano
Comissão de Extensão (COMEX)
Titular: Clara Martins de Lucena Santafé Aguiar
Suplente: Matheus de Casaes dos Passos
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
