Documento gerado sob autenticação Nº LTL.814.241.20N, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5460                  de  20/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  13/07/2016,   referente  ao  interstício  de
09/12/2014 a 12/07/2016, para a servidora KARINA BRAZ MARCHETTI, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 1762080,  lotada  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.015040/2016-23:
Formação Integral de Servidores da UFRGS IV CH: 106 Carga horária utilizada: 100 hora(s) / Carga horária
excedente: 6 hora(s) (03/11/2014 a 29/06/2016)
UFRGS - XXXV Encontro Nacional de Dirigentes de Pessoal e Recursos Humanos das Instituições Federais de
Ensino - ENDP CH: 20 (01/09/2015 a 04/09/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
