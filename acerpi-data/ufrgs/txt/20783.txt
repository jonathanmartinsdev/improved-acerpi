Documento gerado sob autenticação Nº JXU.944.019.6LQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2751                  de  14/04/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de JULIO CARLOS DE SOUZA VAN DER LINDEN,  Professor do
Magistério Superior, lotado e em exercício no Departamento de Design e Expressão Gráfica da Faculdade de
Arquitetura, com a finalidade de participar do "Terceiro Congresso Internacional de Moda e Design", em
Buenos Aires, Argentina, no período compreendido entre 08/05/2016 e 13/05/2016, incluído trânsito, com
ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 18811.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
