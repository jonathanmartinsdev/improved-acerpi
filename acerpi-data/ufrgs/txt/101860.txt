Documento gerado sob autenticação Nº VPK.363.723.M4F, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10885                  de  05/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  01/10/2019,
correspondente ao grau Insalubridade Média,  à servidora ANGELA PEÑA GHISLENI,  Identificação Única
20857055, Professor do Magistério Superior, com exercício no Departamento de Educação Física, Fisioterapia
e Dança da Escola de Educação Física, Fisioterapia e Dança, observando-se o disposto na Lei nº 8.112, de 11
de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em
áreas consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.505094/2017-67,
Código SRH n° 24130.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
