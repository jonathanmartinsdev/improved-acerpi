Documento gerado sob autenticação Nº IWM.665.051.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3958                  de  09/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LIZÂNGELA GUERRA, Técnico em Assuntos Educacionais, lotada
na Secretaria de Relações Internacionais e com exercício na Coordenadoria de Projetos e Convênios, com a
finalidade de participar da "2019 NAFSA Annual Conference & Expo", em Washington , Estados Unidos, no
período compreendido entre 25/05/2019 e 01/06/2019, incluído trânsito, com ônus UFRGS (Secretaria de
Relações Internacionais: diárias e passagens). Solicitação nº 83945.
RUI VICENTE OPPERMANN
Reitor
