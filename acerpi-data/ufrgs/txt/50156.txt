Documento gerado sob autenticação Nº VMJ.261.216.KMC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             901                  de  31/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  24/01/2018,   referente  ao  interstício  de
19/07/2016 a 23/01/2018, para a servidora ANA CRISTINA BRAGA DE SOUZA BRAGA, ocupante do cargo de
Assistente em Administração - 701200, matrícula SIAPE 2186607,  lotada  na  Pró-Reitoria de Gestão de
Pessoas, passando do Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.501394/2018-58:
NELE - Inglês Geral Básico - Programa Inglês sem Fronteiras CH: 60 (18/05/2015 a 30/11/2015)
ENAP - Ciclo de Gestão do Investimento Público CH: 20 (15/08/2017 a 04/09/2017)
ENAP - Um por todos e todos por um - Pela ética e Cidadania CH: 40 (01/08/2017 a 28/08/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
