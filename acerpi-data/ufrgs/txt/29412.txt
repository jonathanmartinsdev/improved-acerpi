Documento gerado sob autenticação Nº EGZ.639.380.IDN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8452                  de  19/10/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas  atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Retificar a Portaria n° 8169/2016, de 11/10/2016,  que designou a Comissão Especial de Avaliação do
Instituto de Matemática e Estatística, que julgará o Processo Avaliativo para a Promoção à Classe E, com
denominação de Professor Titular da Carreira do Magistério Superior na Universidade Federal do Rio Grande
do Sul, do Departamento de Matemática Pura e Aplicada
e do Departamento de Estatística.
 
Onde se lê:
ALENI BISOGNIN - UNIFRA,
leia-se:
ELENI BISOGNIN - UNIFRA.
RUI VICENTE OPPERMANN
Reitor
