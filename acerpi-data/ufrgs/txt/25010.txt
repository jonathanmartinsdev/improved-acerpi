Documento gerado sob autenticação Nº IJD.456.027.2JQ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5521                  de  22/07/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora FABIOLA PEIXOTO DA
SILVA MELLO, ocupante do cargo de Médico Veterinário-701048, lotada no Hospital de Clínicas Veterinárias,
SIAPE 2052425, para 75% (setenta e cinco por cento), a contar de 11/03/2016, tendo em vista a conclusão do
curso de Doutorado em Ciências Veterinárias - Área de Concentração: Morfologia, Cirurgia e Reprodução
Animal/Farmacologia e Terapêutica Animal, conforme o Processo nº 23078.200949/2016-85.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
