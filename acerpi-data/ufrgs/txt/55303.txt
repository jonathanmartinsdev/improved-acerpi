Documento gerado sob autenticação Nº WPB.505.488.J5N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4304                  de  13/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder a Retribuição por Titulação-RT, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro de 2012, com suas alterações, por obtenção do título de Mestrado + RSC III, no Quadro desta
Universidade, ao Professor MARCELO ANTONIO DOS SANTOS, matrícula SIAPE 3010242, ocupante de cargo
de carreira do Magistério do Ensino Básico, Técnico e Tecnológico, regime de dedicação exclusiva, classe D,
nível 01, lotado e em exercício no Departamento de Ciências Exatas e da Natureza do Colégio de Aplicação,
com vigência financeira a partir da publicação da Portaria, conforme processo 23078.504362/2018-12.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
