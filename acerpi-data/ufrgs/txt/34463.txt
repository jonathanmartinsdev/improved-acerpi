Documento gerado sob autenticação Nº EFT.568.497.8E0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1549                  de  16/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°30066,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do  Quadro  de  Pessoal  desta  Universidade,  VALDIONOR DADA DOS SANTOS (Siape:
1120674 ),  para substituir   CARLOS ALEXANDRE WENDEL (Siape: 1139896 ), Diretor da Divisão Acadêmica da
Direção Acadêmica do Campus Litoral Norte, Código FG-3, em seu afastamento por motivo de férias, no
período de 06/02/2017 a 25/02/2017, com o decorrente pagamento das vantagens por 20 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
