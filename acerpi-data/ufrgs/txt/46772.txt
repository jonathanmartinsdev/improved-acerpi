Documento gerado sob autenticação Nº HJW.553.171.K71, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10165                  de  03/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Homologar o parecer que aprova a servidora técnico-administrativa EVELINE ARAUJO RODRIGUES,
ocupante do cargo de Engenheiro-área,  no estágio probatório cumprido no período de 03/11/2014 até
03/11/2017, fazendo jus, a partir desta data, à estabilidade no serviço público federal.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
