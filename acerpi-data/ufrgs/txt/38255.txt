Documento gerado sob autenticação Nº PYF.121.796.K7K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4241                  de  15/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 14/04/2017,  o ocupante do cargo de Professor do Magistério
Superior, classe Assistente A do Quadro de Pessoal desta Universidade, ERACLITO PEREIRA, matrícula SIAPE
n° 2134724, da função de Coordenador Substituto da COMGRAD em Museologia, para a qual foi designado
pela Portaria 10022/2015, de 17/12/2015. Processo nº 23078.008216/2017-71.
JANE FRAGA TUTIKIAN
Vice-Reitora.
