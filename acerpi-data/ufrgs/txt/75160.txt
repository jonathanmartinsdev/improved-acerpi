Documento gerado sob autenticação Nº LGC.586.813.EMM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             547                  de  17/01/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/01/2019,   referente  ao  interstício  de
20/06/2017 a 15/01/2019, para a servidora ANGELA FRANCISCA ALMEIDA DE OLIVEIRA, ocupante do cargo
de Assistente em Administração - 701200, matrícula SIAPE 2098038,  lotada  na  Faculdade de Educação,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.501052/2019-19:
Formação Integral de Servidores da UFRGS VI CH: 170 Carga horária utilizada: 150 hora(s) / Carga horária
excedente: 20 hora(s) (06/11/2015 a 31/10/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
