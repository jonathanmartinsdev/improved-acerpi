Documento gerado sob autenticação Nº SFM.420.953.KV5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7147                  de  04/08/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
      Autorizar o afastamento no País de LIA LEVY, ocupante do cargo de Professor do Magistério Superior,
lotada e exercício no Departamento de Filosofia, com a finalidade de realizar estudos em nível de Pós-
Doutorado, junto à Universidade Federal do Rio de Janeiro, no período compreendido entre 01/08/2017 e
31/07/2018, com ônus CNPq. Processo 23078.503380/2017-98.
JANE FRAGA TUTIKIAN
Vice-Reitora.
