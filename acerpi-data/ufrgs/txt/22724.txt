Documento gerado sob autenticação Nº HCI.125.485.5GO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3941                  de  30/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  24/05/2016,   referente  ao  interstício  de
06/10/2014 a 23/05/2016, para a servidora LIDIANE DA SILVA SITTONI, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2167588,  lotada  na  Secretaria de Comunicação Social, passando
do Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.012173/2016-48:
Formação Integral de Servidores da UFRGS V CH: 124 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 34 hora(s) (04/12/2014 a 20/05/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
