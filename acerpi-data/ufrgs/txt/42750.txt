Documento gerado sob autenticação Nº SLP.671.570.UJM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7441                  de  11/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°29859,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, SERGIO HOFMEISTER DE ALMEIDA MARTINS COSTA
(Siape: 0355308 ),   para substituir    EDISON CAPP (Siape: 2232220 ),  Chefe do Depto de Ginecologia e
Obstetrícia da Faculdade de Medicina, Código FG-1, em seu afastamento por motivo de férias, no período de
01/08/2017 a 31/08/2017, com o decorrente pagamento das vantagens por 31 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
