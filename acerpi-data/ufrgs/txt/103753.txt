Documento gerado sob autenticação Nº RCU.264.975.A9N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             514                  de  13/01/2020
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MILENE JORGE ALIVERTI, Professor do Magistério Superior,
lotada e  em exercício  no Departamento de Música do Instituto de Artes,  com a finalidade de realizar
atividade artística e ministrar curso junto à  Musikschule für den Kreis Gütersloh, em Güetersloh, Alemanha,
no período compreendido entre 15/02/2020 e 24/02/2020, incluído trânsito, com ônus limitado. Solicitação nº
89212.
RUI VICENTE OPPERMANN
Reitor
