Documento gerado sob autenticação Nº LWM.444.179.BCJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2483                  de  04/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°43059,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de SERVENTE DE LIMPEZA, do
Quadro de Pessoal desta Universidade, ESTELA MARIS PERES DE FREITAS (Siape: 0358068 ),  para substituir  
MISAEL  BASSUALDO  CABREIRA  (Siape:  2157867  ),  Chefe  do  Setor  de  Infraestrutura  da  Gerência
Administrativa da Escola de Administração,  Código FG-5,  em seu afastamento por motivo de férias,  no
período de 09/04/2018 a 13/04/2018, com o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
