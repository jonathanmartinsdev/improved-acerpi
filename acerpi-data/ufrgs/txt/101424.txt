Documento gerado sob autenticação Nº ECU.424.421.LME, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10809                  de  03/12/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Remover, a partir de 29 de novembro de 2019, JOSE GERALDO SOARES DAMICO, matrícula SIAPE
nº. 2276454, ocupante do cargo de Professor do Magistério Superior, do Departamento de Educação Física,
Fisioterapia  e  Dança  para  o  Departamento  de  Psicanálise  e  Psicopatologia  do  Instituto  de  Psicologia,
conforme Processo nº. 23078.530694/2019-25.
RUI VICENTE OPPERMANN
Reitor.
