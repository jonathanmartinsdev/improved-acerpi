Documento gerado sob autenticação Nº DID.563.561.46O, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6746                  de  29/08/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  28/08/2018,   referente  ao  interstício  de
28/02/2017 a 27/08/2018, para a servidora MIRTHA GOALCONDA VASQUES RODRIGUES, ocupante do cargo
de Assistente em Administração - 701200, matrícula SIAPE 2100407,  lotada  na  Escola de Administração,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.520583/2018-20:
Formação Integral de Servidores da UFRGS I CH: 36 (18/04/2017 a 31/07/2018)
Fundação Bradesco - Comunicação Escrita CH: 11 (08/08/2016 a 29/08/2016)
Fundação Bradesco - Introdução à Fotografia Digital CH: 27 (03/05/2017 a 03/05/2017)
Fundação Bradesco - Língua Portuguesa Sem Complicações CH: 20 Carga horária utilizada: 4 hora(s) / Carga
horária excedente: 16 hora(s) (20/11/2017 a 20/11/2017)
ANDRESAN - Português Extensivo CH: 72 (03/09/2016 a 12/11/2016)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
