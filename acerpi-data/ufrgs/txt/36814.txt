Documento gerado sob autenticação Nº IOD.191.603.2AQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3181                  de  12/04/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a CLAUDIA REGINA MANO,
matrícula SIAPE nº 0351733, no cargo de Técnico de Tecnologia da Informação, nível de classificação D, nível
de capacitação IV, padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho, com exercício na Secretaria do Gabinete do Reitor,  com proventos integrais e incorporando a
vantagem pessoal de que trata a Lei nº 9.624, de 2 de abril de 1998, que assegurou o disposto no artigo 3º da
Lei nº 8.911, de 11 de julho de 1994. Processo 23078.002108/2017-95.
RUI VICENTE OPPERMANN
Reitor.
