Documento gerado sob autenticação Nº ZIE.040.677.V5V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4102                  de  10/05/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LUIZ AUGUSTO ESTRELLA FARIA,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de
Ciências  Econômicas,  com a  finalidade  de  participar  de  curso  junto  ao  Instituto  Superior  de  Relações
Internacionais, em Maputo, Moçambique, no período compreendido entre 13/05/2017 e 25/05/2017, incluído
trânsito, com ônus limitado. Solicitação nº 26551.
RUI VICENTE OPPERMANN
Reitor
