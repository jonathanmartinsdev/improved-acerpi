Documento gerado sob autenticação Nº FUY.359.182.J7C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5477                  de  23/07/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de HENRIQUE MORRONE, Professor do Magistério Superior, lotado
e  em  exercício  no  Departamento  de  Economia  e  Relações  Internacionais  da  Faculdade  de  Ciências
Econômicas, com a finalidade de desenvolver estudos em nível de Pós-Doutorado junto a  THE NEW SCHOOL
FOR SOCIAL RESEARCH,  em Nova York,  Estados Unidos,  no período compreendido entre 15/08/2018 e
15/03/2019, com ônus limitado.Processo nº 23078.524419/2017-19.
RUI VICENTE OPPERMANN
Reitor.
