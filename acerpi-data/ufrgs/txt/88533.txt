Documento gerado sob autenticação Nº OAU.230.715.HAI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/5
PORTARIA Nº             2481                  de  19/03/2019
A PRÓ-REITORA DE EXTENSÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas
atribuições, considerando o disposto na Portaria nº 7636, de 29 de setembro de 2016
           RESOLVE
 
           Fixar as seguintes Normas de Utilização do Complexo do Salão de Atos, revogando-se as disposições
em contrário:
 
          Art. 1º. O presente conjunto de normas visa uniformizar os procedimentos relativos à concessão do
direito de uso do Complexo do Salão de Atos vinculado à Pró-Reitoria de Extensão e a ele deverão se
submeter  todas  as  pessoas  físicas  e  jurídicas  que  vierem  a  utilizar  os  espaços  mencionados  nesta
normatização.
 
 
TÍTULO I - DA ESTRUTURA E UTILIZAÇÃO
 
          Art. 2º. O Complexo do Salão de Atos compreende a seguinte estrutura:
          I - Salão de Atos, com capacidade para 1.174 pessoas (1.150 poltronas  normais/PMR;12 para obesos) e
mais 12 de acessibilidade (PCR) para cadeirante);
          II - Sala II do Salão de Atos, com capacidade para 241 pessoas (238 poltronas normais/PMR e 03 para
obesos);
          III - Plenarinho, com capacidade para 43 pessoas;
          IV - duas Salas de Apoio, com capacidade para cerca de 50 pessoas;
          V - Sala Vip;
          VI - chapelaria; e
          VII - sete camarins.
 
          Art. 3º. O complexo do Salão de Atos poderá ser utilizado para:
          I - seminários;
          II - congressos;
          III - fóruns;
          IV - colóquios;
          V - cursos e atividades afins;
          VI - espetáculos teatrais, de dança e musicais;
Documento gerado sob autenticação Nº OAU.230.715.HAI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/5
          VII - Festivais e mostras culturais.
 
          Art. 4º. A utilização do Salão de Atos obedecerá as seguintes prioridades:
          I - solenidades de formaturas da UFRGS;
          II - aulas magnas e atividades da Administração Central;
          III - atividades internas da UFRGS;
          IV - atividades externas voltadas para a área da cultura, educação e direitos humanos;
          V - espetáculos teatrais, de dança e musicais e
          VI - demais atividades externas a serem avaliadas.
 
          Art. 5º. O complexo do Salão de Atos poderá ser utilizado preferencialmente de segunda a quinta-feira,
reservando-se os finais de semana para espetáculos e formaturas.
 
          Art. 6º. A ocupação do saguão do Salão de Atos deverá ser acordada com a sua Direção e somente
poderá ser utilizada para instalação de secretarias executivas, editoras, livrarias e  de mesas para coffee
break.
        
 
TÍTULO II - DA ORNAMENTAÇÃO E SINALIZAÇÃO DE EVENTOS
 
          Art. 7º. É expressamente proibido o uso de parafusos, pregos, ganchos, grampos ou fita adesiva dupla
face para afixar itens nos mobiliários, púlpitos e paredes.
 
          Art. 8º. Flores e arbustos podem ser utilizados desde que não obstruam a passagem e a visualização
das pessoas que estão no palco.
 
          Art. 9º. Após o encerramento dos seminários, congressos, simpósios, palestras e cursos, não serão
permitidas reuniões nas dependências do Salão de Atos, salvo se previamente acertadas com a Direção.
 
          Art. 10. As cerimônias de Colação de Grau estão submetidas às normas de utilização específicas para
formaturas, as quais devem ser consultadas previamente.
 
 
TÍTULO III - DOS PROCEDIMENTOS PARA CONCESSÃO DE USO
A TÍTULO ONEROSO
 
          Art. 11. A utilização do Complexo do Salão de Atos deverá ser solicitada à Direção do mesmo, com
antecedência mínima de 30 (trinta) dias, através de agendamento, ofício e projeto resumido contendo a
natureza do evento,  público alvo e estimativa de participantes. As solicitações de uso dos espaços devem ser
encaminhadas através do sítio eletrônico (www.ufrgs.br/salaodeatos).
 
          Art. 12. Serão analisadas somente as solicitações que contenham a especificação do evento a ser
realizado.
 
          Art. 13. Solicitações encaminhadas por membros da comunidade interna devem ter a ciência da
Direção da Unidade/Órgão de origem do proponente, através de assinatura do dirigente em exercício, no
documento de solicitação.
 
          Art.  14. A natureza do evento, determinada conforme classificação do art.  4º,  não poderá ser
modificada depois de encaminhada a solicitação de uso do espaço do Salão de Atos.
Documento gerado sob autenticação Nº OAU.230.715.HAI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
3/5
 
          Art. 15. Na apreciação da reserva solicitada por entidade externa, será levada em conta, além das
prioridades listadas no art. 2º, a ordem de chegada das solicitações e a correlação dos objetivos do evento
com os princípios da Universidade.
 
          Art. 16. Os valores estipulados de utilização dos espaços serão diferenciados conforme a existência, ou
não, de vinculação do solicitante, com a UFRGS; havendo vinculação, trata-se de utilização interna; não
havendo a referida vinculação, trata-se de utilização externa.
 
          Art. 17. Na apreciação da reserva de solicitação interna, será levada em conta, além das prioridades
listadas no art. 3º, a relação expectativa de público e custo de utilização do espaço.
          Parágrafo único - Na relação expectativa de público e custo de utilização do espaço fica estipulado,
como o valor que atende a utilização ótima, o preenchimento de pelo menos 50% (cinqüenta por cento) da
capacidade do espaço solicitado, considerando-se o princípio de proporcionalidade da despesa pública.
 
          Art. 18. A Direção do Salão de Atos, num prazo de 10 (dez) dias úteis, comunicará o resultado da
apreciação, através de ofício a ser enviado para o e-mail do solicitante, emitindo Contrato ou Termo de
Compromisso, a ser assinado em duas vias, bem como a guia de recolhimento, para depósito na Conta Única
da UFRGS, do valor estipulado de utilização devido e com data de vencimento 05 dias úteis antes que
antecedem o início do evento.
          Parágrafo único - Será emitido Contrato de Cessão de Uso a título oneroso sempre que a concessão for
proposta por entidade externa à UFRGS, e, termo de compromisso sempre que referir-se a uma demanda
interna da Universidade.
 
          Art. 19. A efetivação da reserva só se dará quando da assinatura do contrato ou termo, e mediante a
comprovação do recolhimento do valor estipulado de utilização.
 
        Art.  20. O pagamento do valor estipulado de utilização deverá ser procedido mediante guia de
recolhimento à Conta Única da Universidade a ser retirada junto à Administração do Salão de Atos, ou
através de transferência orçamentária da Unidade/Órgão do proponente para a Pró Reitoria de Extensão da
Universidade.
          § 1º. O recolhimento do valor estipulado de utilização poderá ser parcelado, devendo a primeira parcela
ser recolhida no ato da assinatura do contrato ou termo, e a última até 05 (cinco) dias úteis antes da
realização do evento.
          § 2º. Os cancelamentos e desistências, em prazo inferior a 05 (cinco) dias antes da realização do vento
implicará  em multa  de  100% (cem por  cento)  do  valor  acordado,  a  ser  recolhido  na  Conta  Única  da
Universidade mediante guia de recolhimento própria.
          § 3º. É obrigatório o envio do comprovante de pagamento da Guia de Recolhimento da União ou da
Transferência Orçamentária entre Unidades no prazo de até 04 (quatro) dias  antes da data do evento.
 
         Art. 21. As solenidades de formaturas da UFRGS, bem como aulas magnas e atividades da Administração
Central da Universidade estão isentas do pagamento relativo à cessão de uso do Salão de Atos.
 
        Art. 22. Os valores estipulados de utilização constantes no Anexo I serão revisados sempre que
identificada essa necessidade na prestação de contas anual a ser submetida pela Direção do Salão de Atos à
Pró-Reitoria de Extensão.
 
          Art. 23. A comissão organizadora do evento será responsabilizada por quaisquer danos causados ao
imóvel do Complexo do Salão de Atos, inclusive seus móveis, utensílios e equipamentos, durante o período
em que estes ficarem à disposição do evento, devendo ser ressarcidas todas as despesas decorrentes de seu
eventual conserto e/ou reposição, assim que for administrativamente cientificado para tanto.
 
 
Documento gerado sob autenticação Nº OAU.230.715.HAI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
4/5
TÍTULO IV -DA OBSERVÂNCIA ÀS NORMAS DE HIGIENE E SEGURANÇA
 
          Art. 24. A realização de coffee break durante os eventos deverá ser limitada ao saguão do Salão de Atos
e previamente acordada com a Direção.
 
          Art. 25. De maneira alguma será permitido o consumo de alimentos e bebidas nos espaços internos do
Salão de Atos.
 
          Art. 26. Não será permitida a comercialização e/ou o consumo de cigarros ou assemelhados e bebidas
alcóolicas em qualquer dependência do Salão de Atos.
 
          Art. 27. É expressamente proibida a entrada de pessoas portando confetes, serpentinas, balões e
instrumentos sonoros nas dependências do Salão de Atos.
          Parágrafo Único - A Administração do Salão de Atos reserva-se o direito de solicitar a retirada das
pessoas que não atenderem o presente item.
 
          Art. 28. A utilização de elementos cênicos (fogo, água, terra, etc.) que contenham risco potencial para a
integridade física do Salão de Atos,  seus recursos humanos e equipamentos,  em espetáculos culturais,
deverá ser acordada previamente com a Direção.
 
          Art. 29. É expressamente proibido  o uso de qualquer tipo de aeronave não tripulada (aeromodelos,
drones, etc.) nos ambientes internos do Salão de Atos.
 
 
TÍTULO V - DAS AUTORIZAÇÕES, LICENÇAS E DIREITOS AUTORAIS
 
          Art. 30. O responsável pela organização do evento fica obrigado a obter as autorizações perante as
entidades arrecadadoras e fiscalizadoras dos titulares de direitos autorais acaso devidos, relativamente ao
evento e/ou espetáculo.
 
          Art. 31. O responsável pela organização do evento terá o prazo de até 2 (dois) dias úteis antes do início
da montagem para apresentar ficha técnica contendo os nomes, número da célula de identidade (RG) e
funções das pessoas ligadas ao mesmo.
          § 1º. A organização do evento deverá providenciar crachás ou identificação visual padrão para os
integrantes de sua equipe.
           §2º. Ficam expressamente proibidas a entrada e permanência nas dependências do Salão de Atos,
especialmente no palco e camarins, de pessoas cujos nomes não constem nesta ficha técnica e/ou sem
função determinada no evento.
 
 
TÍTULO VI - DAS DISPOSIÇÕES TRANSITÓRIAS E FINAIS
 
          Art. 32. Os objetos porventura perdidos no Salão de Atos serão guardados durante 30 (trinta dias) após
o que serão descartados.
         Parágrafo único. Os documentos não reclamados no prazo de 48 (quarenta e oito) horas serão
depositados em caixa de correio.
 
          Art. 33. Em eventos com cobrança de taxa de inscrição, caberá um percentual mínimo de 3% das vagas
para ser distribuído à comunidade universitária (discentes, docentes e técnico-administrativos) da UFRGS.
          Parágrafo único. No caso de espetáculos artístico-culturais, deverão ser colocados 30 (trinta) convites à
disposição da Pró-Reitoria de Extensão.
Documento gerado sob autenticação Nº OAU.230.715.HAI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
5/5
 
          Art. 34. Os casos omissos serão apreciados por comissão especificamente designada pela Direção do
Salão de Atos, composta por representação docente, técnico-administrativa e discente, cabendo recurso à
Pró-Reitoria de Extensão.
SANDRA DE FATIMA BATISTA DE DEUS
Pró-Reitora de Extensão
