Documento gerado sob autenticação Nº TBM.337.953.JG5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9287                  de  16/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LUIGI CARRO, Professor do Magistério Superior, lotado e em
exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de  Informática,  com  a  finalidade  de
participar de banca de doutorado junto à Tampere University of Technology, em Tampere, Finlândia, no
período compreendido entre 20/11/2018 e 25/11/2018, incluído trânsito, com ônus limitado. Solicitação nº
61184.
RUI VICENTE OPPERMANN
Reitor
