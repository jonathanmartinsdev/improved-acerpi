Documento gerado sob autenticação Nº LMV.676.832.CD4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8084                  de  29/08/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 21 de agosto de 2017,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
KATIUSSA NUNES BUENO,  ocupante  do cargo de Bibliotecário-documentalista,  Ambiente  Organizacional
Informação, Código 701010, Classe E, Nível de Capacitação IV, Padrão de Vencimento 06, SIAPE nº. 1683215
do Instituto de Psicologia para a lotação Escola de Administração, com novo exercício na Biblioteca da Escola
de Administração.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
