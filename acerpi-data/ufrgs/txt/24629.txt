Documento gerado sob autenticação Nº QNA.808.402.FP4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5311                  de  15/07/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LUCI MARI CASTRO LEITE JORGE, Instrumentador Cirúrgico,
lotada e com exercício no Hospital de Clínicas Veterinárias, com a finalidade de realizar estudos em nível de
Mestrado, junto à Universidad Tecnológica Nacional, em Buenos Aires - Argentina, no período compreendido
entre 25 de julho e 25 de agosto de 2016, com ônus limitado. Processo nº 23078.505525/2016-12.
CARLOS ALEXANDRE NETTO
Reitor
