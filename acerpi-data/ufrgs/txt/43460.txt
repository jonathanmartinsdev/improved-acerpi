Documento gerado sob autenticação Nº QYI.521.995.6N4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7968                  de  24/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  30/06/2017,
correspondente ao grau Insalubridade Máxima, ao servidor JOAO BATISTA SOUZA BORGES, Identificação
Única 12205321, Professor do Magistério Superior, com exercício no Departamento de Medicina Animal da
Faculdade de Veterinária, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado
com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Insalubres
conforme Laudo Pericial constante no Processo nº 23078.511051/2017-11, Código SRH n° 23257 e Código
SIAPE 2017003139.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
