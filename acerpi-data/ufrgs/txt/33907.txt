Documento gerado sob autenticação Nº XUY.789.619.4HR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1119                  de  06/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ROSA MARIA VICCARI, Professor do Magistério Superior, lotada
e em exercício no Departamento de Informática Teórica do Instituto de Informática, com a finalidade de
participar  de  reunião  junto  à  Universidad  Nacional  de  Colombia,  em  Medellín,  Colômbia,  no  período
compreendido entre 05/03/2017 e 11/03/2017, incluído trânsito, com ônus limitado. Solicitação nº 25856.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
