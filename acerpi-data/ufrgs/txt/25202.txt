Documento gerado sob autenticação Nº IKL.617.618.3I4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5657                  de  27/07/2016
O DECANO DO CONSELHO UNIVERISTÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Designar ROSANE AZEVEDO NEVES DA SILVA, matrícula SIAPE n° 1354635, ocupante do cargo de
Professor  do  Magistério  Superior,  classe  Associado,  lotada  no  Departamento  de  Psicologia  Social  e
Institucional do Instituto de Psicologia, do Quadro de Pessoal da Universidade, para exercer a função de
Coordenadora Substituta do PPG em Psicologia Social e Institucional, com vigência a partir da data deste ato
até 10 de junho de 2017, a fim de completar o mandato  da Professora CLECI MARASCHIN, conforme artigo
92 do Estatuto da mesma Universidade. Processo nº 23078.015714/2016-90.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no Exercício da Reitoria
