Documento gerado sob autenticação Nº DDG.899.129.FE1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8480                  de  18/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de JEAN SEGATA, Professor do Magistério Superior, lotado e em
exercício no Departamento de Antropologia do Instituto de Filosofia e Ciências Humanas, com a finalidade de
participar  do  "workshop  on  multidisciplinary  research  in  epidemic  preparedness  and  response",  junto
à  Academy of  Medical  Science,  em Londres,  Inglaterra,  no  período  compreendido  entre  30/09/2019  e
04/10/2019, incluído trânsito, com ônus limitado. Solicitação nº 86394.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
