Documento gerado sob autenticação Nº ZGP.920.941.7HC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4408                  de  18/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor JOSE GIOVANI DOS SANTOS OLIVEIRA, ocupante do cargo de  Técnico de
Tecnologia da Informação - 701226, lotado na Escola de Engenharia, SIAPE 1735549, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 05/04/2017, tendo em vista a conclusão do
curso de Graduação em Tecnologia em Análise e Desenvolvimento de Sistemas, conforme o Processo nº
23078.005944/2017-21.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
