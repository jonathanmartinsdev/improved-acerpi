Documento gerado sob autenticação Nº DXQ.876.765.C47, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9133                  de  17/11/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, MARCOS GOLDNADEL, matrícula SIAPE n° 2195332, lotado no Departamento de
Linguística, Filologia e Teoria Literária, para exercer a função de Chefe do Depto de Lingüística, Filologia e
Teoria Literária do Instituto de Letras, Código SRH 808, código FG-1, com vigência a partir de 21/12/2016 e até
20/12/2018, sem prejuízo e cumulativamente com a função de Coordenador da Comissão de Pesquisa do
Instituto de Letras. Processo nº 23078.204154/2016-46.
JANE FRAGA TUTIKIAN
Vice-Reitora
