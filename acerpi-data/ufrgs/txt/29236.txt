Documento gerado sob autenticação Nº COW.287.837.5EU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8234                  de  14/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do país  de ARTHUR GERMANO FETT NETO,  Professor  do Magistério
Superior, lotado no Departamento de Botânica do Instituto de Biociências e com exercício no Centro de
Biotecnologia, com a finalidade de participar de reunião junto ao Agenus Inc. Biotechnology Company, em
Lexington, Estados Unidos, no período compreendido entre 08/11/2016 e 12/11/2016, incluído trânsito, com
ônus limitado. Solicitação nº 24039.
RUI VICENTE OPPERMANN
Reitor
