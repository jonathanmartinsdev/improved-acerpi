Documento gerado sob autenticação Nº JVL.418.312.N12, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4266                  de  16/05/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Alterar o exercício, a partir de 2 de maio de 2017, de AMANDA OLIVEIRA ROCHA, matrícula SIAPE nº
2275985, ocupante do cargo de Tradutor e Intérprete de Linguagem de Sinais, do Divisão Acadêmica da
Direção Acadêmica do Campus Litoral Norte para o Núcleo de Inclusão e Acessibilidade da Pró-Reitoria de
Gestão de Pessoas. Processo n° 23078.006632/2017-35.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
