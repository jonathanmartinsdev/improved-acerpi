Documento gerado sob autenticação Nº LBU.132.721.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7664                  de  23/08/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar  TARÇO  DA  COSTA  DOS  SANTOS,  Matrícula  SIAPE  1860422,  ocupante  do  cargo  de
Contador, Código 701015, do Quadro de Pessoal desta Universidade, para exercer a função de Diretor da
Divisão  de  Gestão  de  Contratos  do  Núcleo  de  Contratos  e  Normativas  vinculada  à  Coordenadoria
Administrativa da PROPLAN, código SRH 343, FG-4, com vigência a partir da data de publicação no Diário
Oficial da União. Processo nº 23078.519931/2019-05.
RUI VICENTE OPPERMANN
Reitor.
