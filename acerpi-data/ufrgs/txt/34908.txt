Documento gerado sob autenticação Nº SWV.910.214.K59, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1866                  de  24/02/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a MARIA SONIR SERAFIM DA
SILVEIRA, matrícula SIAPE nº 0356636, no cargo de Assistente em Administração, nível de classificação D,
nível de capacitação IV, padrão 16, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho,  com  exercício  na  Biblioteca  da  Faculdade  de  Direito,  com  proventos  integrais.  Processo
23078.000954/2017-71.
RUI VICENTE OPPERMANN
Reitor.
