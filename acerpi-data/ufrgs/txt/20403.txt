Documento gerado sob autenticação Nº ZCO.812.303.FCL, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2674                  de  12/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar  o  ocupante  do  cargo  de  Professor  do  Magistério  Superior,  classe  Associado,  desta
Universidade, CALEB FARIA ALVES, CPF n° 7379313886, matrícula SIAPE n° 1549262, lotado no Departamento
de Antropologia do Instituto de Filosofia e Ciências Humanas, para exercer a função de Coordenador da
Comissão de Extensão do IFCH, Código SRH 735, com vigência a partir da data deste ato, pelo período de 02
anos,  ficando o  pagamento  de  gratificação  condicionado à  disponibilidade  de  uma função  gratificada.
Processo nº 23078.201309/2016-92.
RUI VICENTE OPPERMANN
Vice-Reitor
