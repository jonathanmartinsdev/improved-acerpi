Documento gerado sob autenticação Nº YSZ.338.999.9AN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1097                  de  06/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo nº 23078.001516/2017-20
RESOLVE
Retificar a Portaria nº 686, de 24/01/2017, que designou, temporariamente, ANA BEATRIZ MICHELS
(Siape: 1683201) para substituir  ELMO PELLIM MULLER (Siape: 1051058 ),
 
onde se lê:
 
"...em seu afastamento por motivo de férias, no período de 18/01/2017 a 27/01/2017, com o
decorrente pagamento das vantagens por 10 dias."
           
          leia-se:
 
"...  em seu afastamento por motivo de férias, no período de  18/01/2017 a 25/01/2017, com o
decorrente pagamento das vantagens por 08 dias." Ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
