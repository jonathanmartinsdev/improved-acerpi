Documento gerado sob autenticação Nº RKL.954.169.F6G, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8174                  de  11/10/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MARTA  ISAACSSON  DE  SOUZA  E  SILVA,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Arte Dramática do Instituto de Artes, com a
finalidade de participar de encontro junto à Aix-Marseille Université,  em Marseille ,  França,  no período
compreendido entre 31/10/2016 e 14/11/2016, incluído trânsito, com ônus CNPq (Bolsa de Produtividade).
Solicitação nº 23795.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
