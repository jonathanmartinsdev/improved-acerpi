Documento gerado sob autenticação Nº LDR.544.172.JB4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7073                  de  06/08/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal desta Universidade, LUIS ALBERTO LOUREIRO DOS SANTOS, matrícula SIAPE n° 1354710, lotado no
Departamento de Engenharia dos Materiais da Escola de Engenharia, como Chefe Substituto do Depto de
Engenharia dos Materiais da Escola de Engenharia, com vigência a partir de 22/08/2019 até 21/08/2021.
Processo nº 23078.518082/2019-64.
JANE FRAGA TUTIKIAN
Vice-Reitora.
