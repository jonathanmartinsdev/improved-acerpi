Documento gerado sob autenticação Nº EGK.963.436.KDF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10187                  de  11/11/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  VIVIANI  RUFFO  DE  OLIVEIRA,  matrícula  SIAPE  n°  1723686,  lotada  e  em  exercício  no
Departamento de Nutrição da Faculdade de Medicina, da classe D  de Professor Associado, nível 01, para a
classe D  de Professor Associado, nível 02, referente ao interstício de 13/08/2017 a 12/08/2019, com vigência
financeira  a  partir  de  13/08/2019,  conforme  decisão  judicial  proferida  no  processo  nº  5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.518349/2019-13.
JANE FRAGA TUTIKIAN
Vice-Reitora.
