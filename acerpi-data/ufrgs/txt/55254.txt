Documento gerado sob autenticação Nº XLG.589.852.VGG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4249                  de  11/06/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°46965,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ALBERTO EGON SCHAEFFER FILHO (Siape: 1973617 ),
 para substituir   JOAO LUIZ DIHL COMBA (Siape: 1355295 ), Coordenador do PPG em Computação, Código
FUC, em seu afastamento no país, no período de 11/06/2018 a 13/06/2018, com o decorrente pagamento das
vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
