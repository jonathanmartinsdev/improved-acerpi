Documento gerado sob autenticação Nº PMH.585.637.BIO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8846                  de  01/11/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5052623-17.2018.4.04.7100,  da  1ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006,  da  servidora  MARIA LUCIA LOSS MEDEIROS,  matrícula  SIAPE  n°  0354116,  ativa  no  cargo
de Assistente em Administração - 701200, para o nível IV, conforme o Processo nº 23078.528778/2018-18. 
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria
