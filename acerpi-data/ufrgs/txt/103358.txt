Documento gerado sob autenticação Nº EWI.618.063.1PT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             261                  de  07/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Tornar insubsistente a Portaria n° 9319/2019, de 14/10/2019 que concedeu progressão funcional,
por avaliação de desempenho, no Quadro desta Universidade, à Professora SILVANA SILVA, matrícula SIAPE
n° 2503545, lotada no Departamento de Letras Clássicas e Vernáculas do Instituto de Letras e com exercício
na Comissão de Pesquisa de Letras, da classe C de Professor Adjunto, nível 01, para a classe C de Professor
Adjunto, nível 02, referente ao interstício de 17/08/2015 a 16/08/2017, com vigência financeira a partir de
17/08/2017,  conforme decisão judicial  proferida no processo nº  5054491-30.2018.4.04.7100,  da 1ª  Vara
Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.523778/2019-11.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
