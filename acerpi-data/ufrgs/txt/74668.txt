Documento gerado sob autenticação Nº TGK.779.361.Q90, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             252                  de  09/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério do Ensino Básico, Técnico e Tecnológico,
classe D, do Quadro de Pessoal desta Universidade, DANUSA MANSUR LOPEZ, matrícula SIAPE n° 2214174,
lotada no Departamento de Humanidades do Colégio de Aplicação, como Chefe Substituta do Depto de
Humanidades do Colégio de Aplicação, para substituir automaticamente o titular desta função em seus
afastamentos  ou  impedimentos  regulamentares  na  vigência  do  presente  mandato.  Processo  nº
23078.500147/2019-15.
JANE FRAGA TUTIKIAN
Vice-Reitora.
