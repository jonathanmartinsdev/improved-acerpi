Documento gerado sob autenticação Nº UCR.580.480.S1C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4277                  de  17/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar no Centro de Processamento de Dados, com exercício no Divisão de Sistemas de Ensino,
Ambiente  Organizacional  Informação,  RODRIGO  HICKMANN  KLEIN,  nomeado  conforme  Portaria  Nº
2953/2019 de 04 de abril de 2019, publicada no Diário Oficial da União no dia 08 de abril de 2019, em efetivo
exercício desde 15 de maio de 2019, ocupante do cargo de ANALISTA DE TECNOLOGIA DA INFORMAÇÃO,
classe E, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
