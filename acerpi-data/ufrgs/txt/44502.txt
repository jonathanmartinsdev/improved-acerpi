Documento gerado sob autenticação Nº IDF.679.069.6JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8635                  de  14/09/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ESEQUIA SAUTER, Professor do Magistério Superior, lotado e
em exercício no Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística, com a
finalidade de participar da "5th Interenational  Conference on Transport Theory",  em Monterey,  Estados
Unidos, no período compreendido entre 15/10/2017 e 21/10/2017, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Pesquisa - diárias). Solicitação nº 29770.
RUI VICENTE OPPERMANN
Reitor
