Documento gerado sob autenticação Nº QBU.830.036.7JI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10276                  de  14/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de LETICIA MARIA SCHABBACH, Professor do Magistério Superior,
lotada no Departamento de Sociologia do Instituto de Filosofia e Ciências Humanas e com exercício no
Programa de Pós-Graduação em Segurança Cidadã,  com a finalidade de participar  do "XXXII  Congreso
Internacional ALAS Perú 2019: Hacia un nuevo horizonte de sentido histórico de una civilización de vida", em
Lima, Peru, no período compreendido entre 30/11/2019 e 01/12/2019, com ônus CAPES/PROAP, e entre
02/12/2019 e 07/12/2019, com ônus limitado. Solicitação nº 88624.
RUI VICENTE OPPERMANN
Reitor
