Documento gerado sob autenticação Nº BKI.123.479.1SF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3948                  de  30/05/2016
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso da competência que lhe foi delegada pela
Portaria n.º 5477 de 05 de outubro de 2012,
R E S O L V E:
 
 
Subdelegar competência ao servidor JOSÉ VANDERLEI FERREIRA, ocupante do cargo
de Técnico em Contabilidade, para, a partir de 27 de Maio de 2016, ordenar as despesas referentes
a diárias e passagens aéreas no Sistema de Concessão de Diárias e Passagens - SCDP, no âmbito
desta Universidade, por ser portador do Cartão de Pagamento do Governo Federal - Passagens
Aéreas.
ARIO ZIMMERMANN
Pró-Reitor de Planejamento e Administração
