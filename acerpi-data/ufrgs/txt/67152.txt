Documento gerado sob autenticação Nº QJQ.995.490.J7C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5654                  de  30/07/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5032358-91.2018.4.04.7100,  da  2ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, da servidora VERA MARIA DA SILVA SIQUEIRA, matrícula SIAPE n° 0356434, aposentado no
cargo de  Servente de Limpeza - 701823, para o nível III, conforme o Processo nº 23078.518547/2018-04.
RUI VICENTE OPPERMANN
Reitor.
