Documento gerado sob autenticação Nº VZI.541.153.6DC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10270                  de  19/12/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Dispensar,  a  partir  de  22/12/2018,  o  ocupante  do  cargo  de  Contador  -  701015,  do  Nível  de
Classificação EIV, do Quadro de Pessoal desta Universidade, IVONEI SOZIO, matrícula SIAPE 1952761, da
função de Coordenador do Núcleo de Infraestrutura da Diretoria Administrativa do Campus Litoral Norte,
Código  SRH 1419,  Código  FG-1,  para  a  qual  foi  designado pela  Portaria  nº  1990/2017 de  03/03/2017,
publicada no Diário Oficial da União de 06/03/2017. Processo nº 23078.534101/2018-19.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
