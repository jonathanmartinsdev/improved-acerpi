Documento gerado sob autenticação Nº NBY.230.143.L98, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3869                  de  07/05/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 29 de abril de 2019,  de acordo com o artigo 36, parágrafo único, inciso II da Lei
n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
ANDERSON MENEZES PEREIRA, ocupante do cargo de Assistente em Administração, Ambiente Organizacional
Administrativo,  Código 701200,  Classe  D,  Nível  de  Capacitação II,  Padrão de  Vencimento 07,  SIAPE nº.
1736857 da Escola de Enfermagem para a lotação Faculdade de Biblioteconomia e Comunicação, com novo
exercício no Setor Acadêmico da FABICO.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
