Documento gerado sob autenticação Nº JEQ.297.506.8GP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8184                  de  30/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  23/08/2017,   referente  ao  interstício  de
14/01/2016 a 22/08/2017, para a servidora AMANDA OLIVEIRA ROCHA, ocupante do cargo de Tradutor e
Intérprete de Linguagem de Sinais - 701266, matrícula SIAPE 2275985,  lotada  no  Campus Litoral Norte,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  I,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.513867/2017-89:
Formação Integral de Servidores da UFRGS IV CH: 104 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 14 hora(s) (01/03/2016 a 29/06/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
