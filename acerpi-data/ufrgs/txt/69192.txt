Documento gerado sob autenticação Nº ZKX.811.162.7ND, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6990                  de  04/09/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder a Retribuição por Titulação-RT, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro de 2012, com suas alterações, por obtenção do título de Mestrado + RSC III, no Quadro desta
Universidade, à Professora CAMILA GOULART PERES, matrícula SIAPE 1767113, ocupante de cargo de carreira
do Magistério do Ensino Básico, Técnico e Tecnológico, regime de dedicação exclusiva, classe D, nível 02,
lotada no Colégio de Aplicação e com exercício no Departamento de Expressão e Movimento, com vigência
financeira a partir de 07 de maio de 2018 , conforme processo 23078.507411/2018-61.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
