Documento gerado sob autenticação Nº HQH.731.574.855, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2281                  de  13/03/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento do País  de  CARLOS EDUARDO FORTIS  KWIETNIEWSKI,  Professor  do
Magistério Superior, lotado no Departamento de Metalurgia da Escola de Engenharia e com exercício no
Laboratório de Metalurgia Física, com a finalidade de participar do "CORROSION 2019", em Nashville, Estados
Unidos, no período compreendido entre 23/03/2019 e 29/03/2019, incluído trânsito, com ônus limitado.
Solicitação nº 62477.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
