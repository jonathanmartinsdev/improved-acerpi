Documento gerado sob autenticação Nº BIF.338.949.MCQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5725                  de  08/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  03/07/2019,   referente  ao  interstício  de
14/09/2017  a  02/07/2019,  para  a  servidora  ANGELICA  MENIN,  ocupante  do  cargo  de  Técnico  de
Laboratório Área - 701244, matrícula SIAPE 2422058,  lotada  na  Faculdade de Veterinária, passando do
Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.517307/2019-65:
Libras aplicada a situações acadêmicas I CH: 30 (23/01/2019 a 05/02/2019)
UNASUS - Doenças Infectocontagiosas na Atenção Básica à Saúde CH: 60 (26/06/2019 a 02/07/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
