Documento gerado sob autenticação Nº WVA.781.551.C5D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10396                  de  19/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARCUS VINICIUS REIS SO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Odontologia Conservadora da Faculdade de Odontologia, com a
finalidade de participar das "Terceras Jornadas Internacionales de endodoncia" junto à Sociedad Boliviana
Endodoncia, em Oruro, Bolívia, no período compreendido entre 27/11/2019 e 30/11/2019, incluído trânsito,
com ônus limitado. Solicitação nº 88299.
RUI VICENTE OPPERMANN
Reitor
