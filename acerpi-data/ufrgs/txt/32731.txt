Documento gerado sob autenticação Nº OGX.620.095.HGR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             606                  de  20/01/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
tendo  em  vista  o  que  consta  do  Processo  Administrativo  n°  23078.033697/13-95  do  Contrato  nº
146/PROPLAN/NUDECON/2014, da Lei 10.520/02 e ainda da Lei 8.666/93,
 
            RESOLVE:
 
 
            Aplicar a sanção administrativa de MULTA no montante de R$ 3,10 (três reais e dez centavos),
conforme demonstrativo de cálculo fls. 2252 prevista na alínea "b" da Cláusula Décima Primeira do Contrato
c/c alínea "e", Parágrafo Quarto da mesma Cláusula, à Empresa UNISERV - UNIÃO DE SERVIÇOS LTDA, CNPJ
n.º 02.294.475/0001-63, pela falta de cobertura de postos, conforme atestado pela GERTE às fls. 2234/2235,
2246 a 2248 e pelo NUDECON fl. 2251 do processo supracitado.
           
            Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
