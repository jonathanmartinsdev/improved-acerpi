Documento gerado sob autenticação Nº OFN.532.793.72D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             227                  de  08/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder ao servidor VALTER VELASQUES ALVES, ocupante do cargo de  Motorista - 701445, lotado
na  Faculdade  de  Veterinária,  SIAPE  0356779,  o  percentual  de  15%  (quinze  por  cento)  de  Incentivo  à
Qualificação, a contar de 21/12/2018, tendo em vista a conclusão do curso de Ensino Médio - EJA, conforme o
Processo nº 23078.535449/2018-23.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
