Documento gerado sob autenticação Nº LKR.850.426.FRP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8916                  de  04/11/2016
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO  DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7679, de 30 de Setembro de
2016,
 
RESOLVE:
Revogar a Portaria nº 2979 de 13 de Junho de 2011.1.
Delegar competência ao servidor JOSÉ VANDERLEI FERREIRA, Diretor do Departamento de Programação2.
Orçamentária, para, a partir de 20 de Outubro de 2016: ordenar as despesas pertinentes à Pró-Reitoria de
Planejamento e Administração. 
Hélio Henkin
       Pró-Reitor de Panejamento e Administração
