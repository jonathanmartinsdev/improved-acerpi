Documento gerado sob autenticação Nº QBM.507.983.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2439                  de  18/03/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do País  de ADRIANE HERNANDEZ,  Professor  do Magistério Superior,
lotada no Departamento de Artes Visuais do Instituto de Artes e com exercício na Comissão de Graduação de
Artes Visuais, com a finalidade de participar do "ICOCEP - International Congress on Contemporary European
Painting", em Porto, Portugal, no período compreendido entre 07/04/2019 e 12/04/2019, incluído trânsito,
com ônus UFRGS (Pró-Reitoria de Pesquisa: diárias). Solicitação nº 62408.
RUI VICENTE OPPERMANN
Reitor
