Documento gerado sob autenticação Nº ZOU.402.915.PQG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2933                  de  20/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SANDRA MARIA GONÇALVES VIEIRA, Professor do Magistério
Superior, lotada e em exercício no Departamento de Pediatria da Faculdade de Medicina, com a finalidade de
participar do "2018 Joint International Congress of ILTS, ELITA & LICAGE", em Lisboa, Portugal, no período
compreendido entre 21/05/2018 e 28/05/2018, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa -
diárias). Solicitação nº 45064.
RUI VICENTE OPPERMANN
Reitor
