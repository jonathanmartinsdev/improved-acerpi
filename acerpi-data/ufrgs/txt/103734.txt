Documento gerado sob autenticação Nº CRG.291.678.KIT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             502                  de  13/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  03/12/2020,
correspondente  ao  grau  Insalubridade  Máxima,  ao  servidor  DAVI  VOGEL ZIMMER,  Identificação  Única
31583911,  Técnico  em Segurança  do  Trabalho,  com exercício  na  Divisão  de  Segurança  e  Medicina  do
Trabalho da Pró-Reitoria de Gestão de Pessoas, observando-se o disposto na Lei nº 8.112, de 11 de dezembro
de 1990,  combinado com a  Lei  8.270,  de  17  de  dezembro de 1991,  por  exercer  atividades  em áreas
consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.500406/2020-41, Código
SRH n° 24154.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
