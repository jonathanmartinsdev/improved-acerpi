Documento gerado sob autenticação Nº WFH.799.754.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9166                  de  10/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Declarar vaga, a partir de 16/09/2019, a função de Coordenadora do PPG em Política Social e Serviço
Social,  Código SRH 1490, Código FUC, desta Universidade, tendo em vista a aposentadoria de JUSSARA
MARIA ROSA MENDES, matrícula SIAPE n° 1768485, conforme Portaria n° 8331/2019, de 12 de setembro de
2019, publicada no Diário Oficial da União do dia 16 de setembro de 2019. Processo nº 23078.527592/2019-
22.
RUI VICENTE OPPERMANN
Reitor.
