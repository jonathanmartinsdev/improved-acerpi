Documento gerado sob autenticação Nº YPA.714.921.14A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5694                  de  30/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder prorrogação de afastamento parcial,  no período de 01/08/2017 a 28/02/2018, para o
servidor VINICIUS DA ROSA DA SILVA,  ocupante do cargo de Bibliotecário-documentalista -  701010,
matrícula SIAPE 1684297,  lotado  na  Faculdade de Ciências Econômicas, para cursar o Mestrado Acadêmico
em Engenharia de Produção, na UFRGS; conforme o Processo nº 23078.023437/2016-99.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
