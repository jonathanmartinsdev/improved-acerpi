Documento gerado sob autenticação Nº CDF.731.227.1H7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6923                  de  05/09/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Lotar no Instituto de Biociências, com exercício na Divisão Administrativa da Gerência Administrativa
do Instituto do Biociências, TAMARA MOCH, nomeado conforme Portaria Nº 5752/2016 de 29 de julho de
2016, publicada no Diário Oficial da União no dia 01 de agosto de 2016, em efetivo exercício desde 30 de
agosto  de  2016,  ocupante  do  cargo  de  ASSISTENTE  EM  ADMINISTRAÇÃO,  Ambiente  Organizacional
Administrativo, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
