Documento gerado sob autenticação Nº AZW.877.361.5IJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1332                  de  07/02/2020
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar na Pró-Reitoria de Planejamento e Administração, com exercício no Departamento de Gestão
Integrada, Ambiente Organizacional Ciências Exatas e da Natureza, TAÍS OLIVEIRA DA SILVA ALFONSO,
nomeada conforme Portaria Nº 11494/2019 de 27 de dezembro de 2019, publicada no Diário Oficial da União
no dia 30 de dezembro de 2019, em efetivo exercício desde 03 de fevereiro de 2020, ocupante do cargo de
ENGENHEIRO-ÁREA, classe E, nível I, padrão 101, no Quadro de Pessoal desta Universidade. Processo n°
23078.502214/2020-70.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
