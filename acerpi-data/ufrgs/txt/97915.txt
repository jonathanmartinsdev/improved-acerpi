Documento gerado sob autenticação Nº NTH.388.385.02L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8273                  de  10/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  ao  servidor  GIULIANO  MARQUES  BONOTTO,  ocupante  do  cargo  de  Técnico  em
Eletrônica -  701830, lotado na Superintendência de Infraestrutura, SIAPE 2065154, o percentual de 15%
(quinze por cento) de Incentivo à Qualificação, a contar de 09/09/2019, tendo em vista a conclusão do Curso
Superior de Tecnologia em Sistemas para Internet, conforme o Processo nº 23078.524616/2019-91.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
