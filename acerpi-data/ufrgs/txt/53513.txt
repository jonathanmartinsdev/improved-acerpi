Documento gerado sob autenticação Nº SYG.031.143.6FV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2951                  de  23/04/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora SILVIA ALTMANN,  matrícula SIAPE n° 2244043,  lotada e em exercício no Departamento de
Filosofia do Instituto de Filosofia e Ciências Humanas, da classe D  de Professor Associado, nível 03, para a
classe D  de Professor Associado, nível 04, referente ao interstício de 28/07/2015 a 27/07/2017, com vigência
financeira a partir de 13/04/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.505377/2018-90.
PHILIPPE OLIVIER ALEXANDRE NAVAUX
Decano do Conselho Universitário, no exercício da Reitoria.
