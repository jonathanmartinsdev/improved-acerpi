Documento gerado sob autenticação Nº EBH.308.923.FVE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4056                  de  10/05/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  01/04/2019,
correspondente ao grau Insalubridade Média, ao servidor NERON ROBERTO MARTINS SILVA, Identificação
Única 11060735, Pedreiro, com exercício no Setor de Manutenção Predial do Campus da Saúde e Olímpico
(SP2) da Superintendência de Infraestrutura, observando-se o disposto na Lei nº 8.112, de 11 de dezembro
de 1990,  combinado com a  Lei  8.270,  de  17  de  dezembro de 1991,  por  exercer  atividades  em áreas
consideradas Insalubres conforme Laudo Pericial constante no Processo nº 23078.509418/2019-06, Código
SRH n° 23854.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
