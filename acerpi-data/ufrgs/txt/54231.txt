Documento gerado sob autenticação Nº AEW.763.108.RMV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3472                  de  11/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LUIS ARMANDO GANDIN, Professor do Magistério Superior,
lotado e em exercício no Departamento de Estudos Básicos da Faculdade de Educação, com a finalidade de
participar do evento "Schools of Tomorrow-Festival", em Berlin, Alemanha, no período compreendido entre
11/06/2018 e 16/06/2018, incluído trânsito, com ônus limitado. Solicitação nº 45736.
RUI VICENTE OPPERMANN
Reitor
