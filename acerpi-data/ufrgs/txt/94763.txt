Documento gerado sob autenticação Nº RCJ.455.287.Q1T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6308                  de  22/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito, a nomeação da candidata abaixo relacionada, ocorrida através da Portaria nº.
5166/2019 de 18 de junho de 2019, publicada no Diário Oficial da União de 19 de junho de 2019, de acordo
com o que preceitua o § 6º do artigo 13 da Lei nº. 8.112, de 11 de dezembro de 1990, com a redação dada
pela Lei nº. 9.527, de 10 de dezembro de 1997. Processo nº. 23078.025182/2016-07.
 
Cargo 701079 - TÉCNICO EM ASSUNTOS EDUCACIONAIS - Classe E Padrão I
 
ALICE CANAL - Vaga SIAPE 275848
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
