Documento gerado sob autenticação Nº URR.274.542.V44, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4023                  de  09/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/05/2017,   referente  ao  interstício  de
14/11/2012 a 04/05/2017, para o servidor JAQUES EMERSON SANTOS GUIEL, ocupante do cargo de Técnico
em Radiologia - 701257, matrícula SIAPE 1860715,  lotado  no  Hospital de Clínicas Veterinárias, passando do
Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.201244/2017-66:
Formação Integral de Servidores da UFRGS III  CH: 73 Carga horária utilizada: 60 hora(s) / Carga horária
excedente: 13 hora(s) (24/09/2015 a 27/04/2017)
Portal Educação/ABED - Controle de infecção em serviços de saúde CH: 60 (10/05/2012 a 08/06/2012)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
