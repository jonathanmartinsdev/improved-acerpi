Documento gerado sob autenticação Nº PIQ.632.443.K5Q, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6091                  de  16/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de HENRIQUE CARLOS DE OLIVEIRA DE CASTRO, Professor do
Magistério Superior,  lotado e em exercício no Departamento de Economia e Relações Internacionais da
Faculdade de Ciências Econômicas, com a finalidade de desenvolver pesquisa, junto à  University of Notre
Dame, Indiana - Estados Unidos da América, no período compreendido entre 23 de agosto e 31 de dezembro
de 2016, com ônus limitado. Processo nº 23078.001527/2016-29.
CARLOS ALEXANDRE NETTO
Reitor
