Documento gerado sob autenticação Nº ZKR.406.990.7ND, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6959                  de  04/09/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de GUILHERME DORNELAS CAMARA,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Ciências Administrativas da Escola de Administração,
com a finalidade de participar do "V Congreso Internacional Red de Posgrados de Investigación Latinos", em
Santiago, Chile, no período compreendido entre 25/09/2018 e 30/09/2018, incluído trânsito, com ônus UFRGS
(Pró-Reitoria de Pesquisa - diárias). Solicitação nº 45085.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
