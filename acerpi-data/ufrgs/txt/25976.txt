Documento gerado sob autenticação Nº MTL.208.979.88O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6066                  de  15/08/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  PAULO ZIELINSKY,  matrícula  SIAPE n°  6358570,  lotado e  em exercício  no Departamento de
Pediatria da Faculdade de Medicina, da classe   de Professor Adjunto, nível 03, para a classe   de Professor
Adjunto, nível 04, referente ao interstício de 31/12/2000 a 30/12/2003, com vigência financeira a partir de
08/08/2016, de acordo com o que dispõe o parágrafo 1° do artigo 11 da Portaria n° 475 do Ministério da
Educação, de 26 de agosto de 1987 e da Resolução n° 12/95-COCEP. Processo nº 23078.505590/2016-30.
CARLOS ALEXANDRE NETTO
Reitor
