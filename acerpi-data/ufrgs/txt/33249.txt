Documento gerado sob autenticação Nº QMW.454.804.M21, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             617                  de  20/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, JOAO LUIZ DIHL COMBA, matrícula SIAPE n° 1355295, lotado no Departamento
de Informática Aplicada, para exercer a função de Coordenador do PPG em Computação, Código SRH 1152,
código FUC, com vigência a partir da data de publicação no Diário Oficial da União, pelo período de 02 anos.
Processo nº 23078.204247/2016-71.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
