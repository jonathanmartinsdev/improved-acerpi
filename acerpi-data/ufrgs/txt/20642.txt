Documento gerado sob autenticação Nº ZBL.063.776.UFQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2739                  de  14/04/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  28/03/2016,   referente  ao  interstício  de
11/07/2012  a  27/03/2016,  para  o  servidor  MAURÍCIO  VIÉGAS  DA  SILVA,  ocupante  do  cargo  de
Administrador - 701001, matrícula SIAPE 0354315,  lotado  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.006800/2016-10:
Formação Integral de Servidores da UFRGS III CH: 60 (26/06/2009 a 14/12/2015)
ENAP  -  Gestão  da  Informação  e  Documentação  -  Conceitos  básicos  em  Gestão  Documental  CH:  20
(27/10/2015 a 16/11/2015)
ENAP - Gestão Estratégica de Pessoas e Planos de Carreira CH: 20 (20/10/2015 a 09/11/2015)
ENAP - Introdução à Gestão de Projetos CH: 20 (15/09/2015 a 05/10/2015)
ENAP - Desenho de cursos: introdução ao modelo ADDIE CH: 20 (22/03/2016 a 11/04/2016)
ENAP -  Gestão Estratégica com uso de BSC CH:  20 Carga horária  utilizada:  10 hora(s)  /  Carga horária
excedente: 10 hora(s) (10/11/2015 a 30/11/2015)
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
