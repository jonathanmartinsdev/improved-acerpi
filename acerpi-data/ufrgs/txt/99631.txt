Documento gerado sob autenticação Nº UAE.837.561.09L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9389                  de  16/10/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 16/10/2019,  a ocupante do cargo de Professor do Magistério
Superior,  classe Adjunto A,  do Quadro de Pessoal  desta Universidade,  CLARICE GONTARSKI  SPERANZA,
matrícula SIAPE n° 2543883, da função de Coordenadora Substituta da COMGRAD de História, para a qual foi
designada pela Portaria 10091/2018, de 13/12/2018. Processo nº 23078.527586/2019-75.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
