Documento gerado sob autenticação Nº RQY.555.391.MM1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
DEPARTAMENTO DE DESENVOLVIMENTO E GESTÃO DE PESSOAS
ESCOLA DE DESENVOLVIMENTO DE SERVIDORES DA UFRGS
Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
(51) 3308-3015/3219/3914/4596 - edufrgs@progesp.ufrgs.br
Processo n°: 23078.504317/2019-31
Servidora: MONICA DE LUCENA KUZMICK
Cargo: Assistente em Administração
Lotação: Pró-Reitoria de Graduação
Ambiente Organizacional: Administrativo
Nível de Classificação e Nível de Capacitação: D IV
PARECER N° 414/2019
Trata este expediente da retificação do Parecer n° 397/2019, de 20/03/2019, que concede Horário
Especial a MONICA DE LUCENA KUZMICK, Assistente em Administração, com exercício na Divisão da Vida
Acadêmica do Departamento de Consultoria em Registros Discentes da Pró-Reitoria de Graduação. Processo
nº 23078.504317/2019-31.
Tendo em vista a solicitação de cancelamento da concessão de horário especial  para servidor
estudante, feita pela Requerente, no despacho SEI nº 1495432,
 
Onde se lê:
"para o período de 06/03/2019 a 13/07/2019",
leia-se:
"para o período de 06/03/2019 a 20/03/2019".
Em Data da Redação.
BIANCA SPODE BELTRAME
Divisão de Análise e Orientação do Desenvolvimento na Carreira
De acordo. À requerente, para ciência e conclusão do processo.
Em 25/03/2019.
KAREN WERLANG LUNKES
Diretora da Divisão de Análise e Orientação do Desenvolvimento na Carreira
