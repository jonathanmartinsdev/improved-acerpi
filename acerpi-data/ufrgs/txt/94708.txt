Documento gerado sob autenticação Nº BBP.309.063.DO4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6252                  de  21/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°50280,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, GENISA COUTO DA SILVA (Siape: 1651853 ),
 para substituir   FABIANO FOGAÇA (Siape: 2128807 ), Secretário do Prefeito do Campus Litoral Norte da Vice-
Superintendência de Manutenção da SUINFRA, Código FG-2, em seu afastamento por motivo de férias, no
período de 22/07/2019 a 08/08/2019.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
