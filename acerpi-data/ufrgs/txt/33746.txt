Documento gerado sob autenticação Nº DMY.646.629.4PQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1085                  de  03/02/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
               Autorizar o afastamento no País de POLLIANE TREVISAN NUNES, ocupante do cargo de Técnico em
Assuntos Educacionais, com lotação na Gerência Administrativa da Pró-Reitoria de Extensão e exercício na
Divisão de Planejamento e Assessoria Técnica da PROREXT, com a finalidade de realizar estudos em nível de
Mestrado, junto à Universidade Federal do Rio Grande do Sul, no período compreendido entre 15/03/2017 e
14/03/2018, com ônus limitado. Processo nº 23078.500456/2016-23.
JANE FRAGA TUTIKIAN
Vice-Reitora
