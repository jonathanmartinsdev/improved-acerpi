Documento gerado sob autenticação Nº ESL.742.890.VDP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4887                  de  09/07/2018
  Determina  procedimentos  a  serem
executados  pelas  Fundações  de  Apoio
referente a transferência de recursos.
            O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL - UFRGS, no uso de suas atribuições, e
considerando o que consta no item 218.14 do Acórdão nº 3071/2006 - TCU - 2ª Câmara, e no item 1.4.1.12.,
do Acórdão nº 4759/2008 - TCU - 2ª Câmara,
RESOLVE:
Art. 1º Determinar que em todos os projetos executados em parceria com as Fundações de Apoio, ao final
dos mesmos, depois de liquidadas todas as despesas relativas à sua execução, se houver saldo, este deverá
ser transferido à conta única da UFRGS, através de GRU. A mencionada GRU deverá ser incluída na prestação
de contas do projeto.
Art. 2º Fica vedada a transferência de saldos remanescentes entre projetos executados com apoio das
Fundações, mesmo que de interesse das Unidades da UFRGS.
Art. 3º Quando, durante a vigência dos projetos, e justificadamente, houver a necessidade de participação de
outros setores da Universidade para a consecução do objeto, poderá haver a transferência de recursos entre
projetos.
§ 1º O valor a ser transferido, mediante recibo emitido pelo setor demandado na Fundação, corresponderá
aos custos dos serviços prestados; ou produtos fornecidos, na situação específica e excepcional da Loja
Ponto UFRGS e da Editora da Universidade.
§ 2º O recibo deverá conter o número do projeto solicitante e o número do projeto que realizou os serviços
ou forneceu os produtos.
§ 3º No extrato dos projetos envolvidos, deverá constar a identificação dos mesmos.
§ 4º No caso de o setor demandado não possuir projeto em andamento na Fundação de Apoio, o valor
correspondente aos custos dos serviços prestados ou produtos fornecidos ao projeto deverá ser depositado
na conta única da UFRGS, mediante GRU.
Art. 4º Fica revogada a Portaria 2828, de 18 de junho de 2010.
Art. 5º Esta Portaria entra em vigor na data de sua assinatura.
RUI VICENTE OPPERMANN,
Reitor.
