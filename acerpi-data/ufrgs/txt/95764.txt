Documento gerado sob autenticação Nº XBZ.030.741.SFJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7104                  de  07/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  30/07/2019,   referente  ao  interstício  de
12/12/2017 a 29/07/2019,  para a  servidora LIZIANE GONZALEZ,  ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 3002666,  lotada  na  Escola de Educação Física, Fisioterapia e
Dança, passando do Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de
Capacitação  D  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.519929/2019-28:
ILB - Excelência no Atendimento CH: 20 (15/04/2018 a 05/05/2018)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 40 Carga horária utilizada: 20 hora(s) / Carga
horária excedente: 20 hora(s) (10/08/2018 a 30/08/2018)
ENAP - Gestão Pessoal - Base da Liderança CH: 50 (26/07/2019 a 30/07/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
