Documento gerado sob autenticação Nº AIV.089.839.7D9, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2835                  de  15/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  ao  servidor  HENRIQUE  ORTIZ  GARCIA,  ocupante  do  cargo  de   Assistente  em
Administração - 701200, lotado na Escola de Enfermagem, SIAPE 2110517, o percentual de 25% (vinte e
cinco por cento) de Incentivo à Qualificação, a contar de 22/03/2016, tendo em vista a conclusão do curso de
Graduação em Letras - Bacharelado / Habilitação: Tradutor - Português e Alemão, conforme o Processo nº
23078.006339/2016-97.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
