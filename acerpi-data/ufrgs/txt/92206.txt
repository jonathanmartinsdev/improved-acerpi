Documento gerado sob autenticação Nº ILU.927.880.98G, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4757                  de  04/06/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 3 de junho de 2019,  de acordo com o artigo 36, parágrafo único, inciso II da Lei
n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
ALYNNI LUIZA RICCO AVILA, ocupante do cargo de Assistente em Administração, Ambiente Organizacional
Administrativo,  Código 701200,  Classe D,  Nível  de Capacitação IV,  Padrão de Vencimento 10,  SIAPE nº.
1446236 da Secretaria de Educação a Distância para a lotação Pró-Reitoria de Pesquisa, com novo exercício
na Secretaria da Pró-Reitoria de Pesquisa.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
