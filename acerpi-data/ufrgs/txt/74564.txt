Documento gerado sob autenticação Nº PMC.930.247.CG1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             170                  de  07/01/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°42231,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CARLA BRANDALISE (Siape: 0359418 ),  para substituir  
EDUARDO SANTOS NEUMANN (Siape: 2224398 ), Coordenador do PPG em História, Código FUC, em seu
afastamento por motivo de férias, no período de 08/01/2019 a 21/01/2019, com o decorrente pagamento das
vantagens por 14 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
