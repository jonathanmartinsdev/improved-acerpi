Documento gerado sob autenticação Nº IHK.408.566.OLJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10631                  de  21/11/2017
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar a Portaria n° 9792/2017, de 23/10/2017, que concedeu jornada de trabalho reduzida com
remuneração proporcional à servidora CINTIA BUENO MARQUES, Técnico em Assuntos Educacionais, lotada
na Escola de Educação Física, Fisioterapia e Dança, alterando a jornada de trabalho de oito horas diárias e
quarenta horas semanais para 4 horas diárias e 20 semanais, no período de 01 de novembro de 2017 a 31 de
outubro de 2018, de acordo com o disposto no Artigo 5º da Medida Provisória N. 2.174-28, de 24 de agosto
de 2001, e conforme o Processo nº 23078.014204/2017-86.
 
Onde se lê:
de acordo com o disposto no Artigo 5º da Medida Provisória N. 2.174-28, de 24 de agosto de 2001,
leia-se:
de acordo com o disposto na Medida Provisória Nº 792, de 26 de julho de 2017, e nos Artigos 17 a
24 da Portaria Nº 291, de 12 de setembro de 2017, do Ministério do Planejamento, Desenvolvimento e
Gestão,
PHILIPPE OLIVIER ALEXANDRE NAVAUX
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
