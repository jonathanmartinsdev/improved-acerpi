Documento gerado sob autenticação Nº ZQV.867.918.86C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9858                  de  25/10/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar  no  Instituto  de  Pesquisas  Hidráulicas,  com exercício  no  Núcleo  Acadêmico  da  Gerência
Administrativa do Instituto de Pesquisas Hidráulicas, TIAGO GHENO CUNHA, nomeado conforme Portaria Nº
8944/2017 de 26 de setembro de 2017, publicada no Diário Oficial da União no dia 27 de setembro de 2017,
em efetivo exercício desde 20 de outubro de 2017, ocupante do cargo de ASSISTENTE EM ADMINISTRAÇÃO,
Ambiente  Organizacional  Administrativo,  classe  D,  nível  I,  padrão  101,  no  Quadro  de  Pessoal  desta
Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
