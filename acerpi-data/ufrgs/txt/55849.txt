Documento gerado sob autenticação Nº BJI.678.129.ITA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4681                  de  29/06/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de KARLA MARIA MULLER,  Professor do Magistério Superior,
lotada no Departamento de Comunicação da Faculdade de Biblioteconomia e Comunicação e com exercício
na Faculdade de Biblioteconomia e Comunicação, com a finalidade de participar do "XIV Congreso de la
Asociación Latinoamericana de Investigadores de la Comunicación", em San Jose, Costa Rica, no período
compreendido entre 29/07/2018 e 02/08/2018, incluído trânsito, com ônus limitado. Solicitação nº 45842.
RUI VICENTE OPPERMANN
Reitor
