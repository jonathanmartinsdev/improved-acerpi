Documento gerado sob autenticação Nº QGE.411.780.OBT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             4973                  de  11/06/2019
  Altera a composição do Comitê de Ética
em Pesquisa - CEP da UFRGS.
A PRÓ-REITORA DE COORDENAÇÃO ACADÊMICA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL, no  uso  de suas  atribuições,
RESOLVE:
Art. 1º Alterar a composição do Comitê de Ética em Pesquisa da Universidade Federal do Rio Grande
do Sul, cuja finalidade é avaliar e acompanhar os projetos de pesquisa envolvendo seres humanos, em seus
aspectos éticos e metodológicos, estabelecido pela Portaria nº 3124 de 09 de abril de 2019, com a exclusão
dos Professores  Elaine Aparecida Felix,  Keila  Cristiane Deon,  Ricardo Francalacci  Savaris  e  Vera Regina
Schmitz;  com  a  inclusão  dos  Professores  Cláudia  Tarragô  Condotti  (ESEFID),  Mauro  Silveira  de  Castro
(ADUFRGS) e Laura Hastenpflug Wottrich (FABICO), que passa a vigorar com os seguintes membros:
      ADAUTO  LOCATELLI TAUFER
      ANA BEATRIZ AREAS DA LUZ FONTES
      ANDREA FACHEL LEAL
      ANGELA PEÑA GHISLENI
      CARLA ESTEFÂNIA ALBERT
      CAROLINE PIETA DIAS
      CLAUDIA TARRAGO CANDOTTI
      CONCEIÇÃO PALUDO
      CRISTIANNE MARIA FAMER ROCHA
      DIOGO ANDRE PILGER
      FRANCISCO MONTAGNER
      GUY GINCIENE
      HELIO RICARDO DO COUTO ALVES
      IDA VANESSA DOEDERLEIN SCHWARTZ
      JEFFERSON FAGUNDES LOSS
      JOCELISE JACQUES DE JACQUES
      JOSE ARTUR BOGO CHIES
Documento gerado sob autenticação Nº QGE.411.780.OBT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
      JULIANA JOBIM JARDIM
      LAURA HASTENPFLUG WOTTRICH
      LUCIANA GRUPPELLI LOPONTE
      MARA DA SILVEIRA BENFATO
      MAITY SIMONE GUERREIRO SIQUEIRA
      MARCIO PIZARRO NORONHA
      MARIA ALICE DIAS DA SILVA LIMA
      MARIA DA GRACA CORSO DA MOTTA
      MAURO SILVEIRA DE CASTRO
      PATRÍCIA DANIELA MELCHIORS ANGST
      PEDRO HENRIQUE DE ALMEIDA KONZEN
      RAQUEL CANUTO
      ROBERTO LAMB
      RODRIGO ALEX ARTHUR
      RODRIGO LAGES E SILVA
      TIAGO DEGANI VEIT
      VINICIUS COELHO CARRARD
      WANIA APARECIDA PARTATA
              
               Art. 2º O mandato do referido Comitê será de 19 de março de 2018 até 19 de março de 2021.
 
              Art. 3º Os mandatos dos Presidentes da Câmara de Pesquisa e Câmara de Pós-Graduação serão até o
término do mandato na Presidência das respectivas Câmaras.
 
              Art. 4º Revogar a Portaria nº 3124 de 09/04/2019.
JANE FRAGA TUTIKIAN,
Pró-Reitora.
