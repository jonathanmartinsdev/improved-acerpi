Documento gerado sob autenticação Nº LAK.875.339.RM1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1011                  de  29/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora CATARINA LEITE DOMENICI, matrícula SIAPE n° 0985844, lotada e em exercício no Departamento
de Música do Instituto de Artes, da classe D  de Professor Associado, nível 02, para a classe D  de Professor
Associado, nível 03, referente ao interstício de 25/08/2013 a 24/08/2015, com vigência financeira a partir de
20/12/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a
Decisão nº 331/2017 do CONSUN. Processo nº 23078.524081/2017-97.
JANE FRAGA TUTIKIAN
Vice-Reitora.
