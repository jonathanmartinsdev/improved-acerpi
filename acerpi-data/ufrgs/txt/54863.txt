Documento gerado sob autenticação Nº GPP.290.655.97E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3989                  de  30/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL,  no uso de suas atribuições legais e estatutárias
RESOLVE
Nomear a  Senhora  SIMONE DA COSTA SALDANHA para exercer o cargo de Diretora do Depto
Administrativo e de Registro da Extensão da PROREXT, Código SRH 1099, código CD-4, com vigência a partir
da data de publicação no Diário Oficial da União. Processo nº 23078.508214/2018-69.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
