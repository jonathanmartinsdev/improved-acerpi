Documento gerado sob autenticação Nº NAQ.016.080.HDR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5749                  de  01/08/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar no Instituto de Artes,  com exercício no Núcleo Financeiro da Gerência Administrativa do
Instituto de Artes, ELKYER SILVA BRISOLARA SILVEIRA, nomeado conforme Portaria Nº 4284/2018 de 12 de
junho de 2018, publicada no Diário Oficial da União no dia 13 de junho de 2018, em efetivo exercício desde
17 de julho de 2018, ocupante do cargo de ASSISTENTE EM ADMINISTRAÇÃO, Ambiente Organizacional
Administrativo, classe D, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
