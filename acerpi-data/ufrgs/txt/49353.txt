Documento gerado sob autenticação Nº KOR.020.177.2RB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             366                  de  11/01/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
            Retificar, a contar de 11/11/2016,  a Portaria n° 9063/2016, de 10/11/2016, publicada no Diário Oficial da União de 11/11/2016, que
nomeou ALINE REIS  CALVO HERNANDEZ,  Professor  do Magistério  Superior,  com exercício  no Departamento de Estudos  Básicos  da
Faculdade de Educação , revogando-se a nomeação por Decisão Judicial, passando a ser por aprovação em concurso público. Processo nº
23078.022893/2016-11.
             Onde se lê:
"...Nomear, em caráter efetivo, ALINE REIS CALVO HERNANDEZ, em virtude de habilitação em Concurso Público de Provas e Títulos, conforme
Edital Nº 47, de 09 de setembro de 2015, homologado em 10 de setembro de 2015, de acordo com os atigos 9º, item I e X da Lei nº 8.112, de
11 de dezembro de 1990 e Lei 12.772, de 28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada
no  Diário  Oficial  da  União  de  25  de  setembro  de  2013,  e  de  acordo  com  decisão  liminar  no  Procedimento  Comum  nº  5068821-
03.2016.4.04.7100/RS, da 4ª Vara Federal de Porto Alegre, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do Plano de Carreiras e
Cargos do Magistério Federal, ...", 
                leia-se:
"...Nomear, em caráter efetivo, ALINE REIS CALVO HERNANDEZ, em virtude de habilitação em Concurso Público de Provas e Títulos, conforme
Edital Nº 47, de 09 de setembro de 2015, homologado em 10 de setembro de 2015, de acordo com os atigos 9º, item I e X da Lei nº 8.112, de
11 de dezembro de 1990 e Lei 12.772, de 28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada
no Diário Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do Plano de Carreiras e Cargos
do Magistério Federal, ...", ficando ratificados os demais termos. 
RUI VICENTE OPPERMANN
Reitor.
