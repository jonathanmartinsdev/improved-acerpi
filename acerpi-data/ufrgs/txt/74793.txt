Documento gerado sob autenticação Nº GES.579.511.5G2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             282                  de  09/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/01/2019,   referente  ao  interstício  de
05/07/2017 a 04/01/2019, para o servidor MILTON HUMBERTO SCHANES DOS SANTOS, ocupante do cargo
de Médico-área - 701047, matrícula SIAPE 1580258,  lotado  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.535468/2018-50:
Formação Integral de Servidores da UFRGS V CH: 120 (30/08/2017 a 17/12/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
