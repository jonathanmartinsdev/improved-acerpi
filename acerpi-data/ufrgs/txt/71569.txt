Documento gerado sob autenticação Nº UMM.599.818.L2O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8767                  de  30/10/2018
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de SERGIO LUIZ VIEIRA, Professor do Magistério Superior, lotado e
em exercício no Departamento de Zootecnia da Faculdade de Agronomia, com a finalidade de participar de
reunião  junto  ao  Adisseo  Nutrição  Animal,  em  Commentry,  França,  no  período  compreendido  entre
10/11/2018 e 14/11/2018, incluído trânsito, com ônus limitado. Solicitação nº 60480.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
