Documento gerado sob autenticação Nº DCY.468.646.DNL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7275                  de  12/08/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  GUILHERME  DORNELAS  CAMARA,  matrícula  SIAPE  n°  3582170,  lotado  e  em  exercício  no
Departamento de Ciências Administrativas da Escola de Administração, da classe C  de Professor Adjunto,
nível 01, para a classe C  de Professor Adjunto, nível 02, referente ao interstício de 03/07/2017 a 02/07/2019,
com vigência  financeira  a  partir  de  03/07/2019,  conforme decisão judicial  proferida no processo nº
5054491-30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de
28  de  dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.514698/2019-66.
JANE FRAGA TUTIKIAN
Vice-Reitora.
