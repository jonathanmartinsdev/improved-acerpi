Documento gerado sob autenticação Nº ALS.713.268.NK5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1660                  de  27/02/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de PAOLA BASSO MENNA BARRETO GOMES ZORDAN, Professor
do Magistério Superior, lotada no Departamento de Artes Visuais do Instituto de Artes e com exercício na
Comissão de Graduação de Artes Visuais, com a finalidade de realizar  trabalho de campo  em Lisboa, Porto,
Évora, Óbidos, com ônus limitado e do "IX Congresso Internacional CSO - Criadores Sobre outras Obras", em
Lisboa,  Portugal,  com ônus UFRGS (Pró-Reitoria de Pesquisa -  diárias),  no período compreendido entre
24/03/2018 e 07/04/2018, incluído trânsito. Solicitação nº 33698.
RUI VICENTE OPPERMANN
Reitor
