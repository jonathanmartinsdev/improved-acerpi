Documento gerado sob autenticação Nº CED.495.188.IHC, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3015                  de  25/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor RODRIGO SCHRAMM, matrícula SIAPE n° 1722182, com exercício no Departamento de Música do
Instituto de Artes, da classe B  de Professor Assistente, nível 02, para a classe C  de Professor Adjunto, nível
01, referente ao interstício de 11/08/2011 a 10/08/2013, com vigência financeira a partir de 13/04/2016, de
acordo com o que dispõe a Resolução nº 12/1995-COCEP,  alterada pela Decisão nº 401/2013-CONSUN.
Processo nº 23078.000967/2016-69.
RUI VICENTE OPPERMANN
Vice-Reitor
