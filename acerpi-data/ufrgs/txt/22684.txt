Documento gerado sob autenticação Nº MXK.545.800.VBH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3903                  de  27/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  19/05/2016,   referente  ao  interstício  de
23/12/2009 a 18/05/2016, para o servidor ADELAR LOPES LUCAS, ocupante do cargo de Jardineiro - 701638,
matrícula  SIAPE  0355709,   lotado   na   Estação  Experimental  Agronômica,  passando  do  Nível  de
Classificação/Nível de Capacitação B III, para o Nível de Classificação/Nível de Capacitação B IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.011975/2016-31:
Formação Integral de Servidores da UFRGS IV CH: 119 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 29 hora(s) (05/05/2010 a 29/04/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
