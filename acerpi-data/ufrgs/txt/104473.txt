Documento gerado sob autenticação Nº AKH.132.492.7C3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1154                  de  03/02/2020
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.530419/2019-
10 e 23078.516350/2018-22, do Pregão Eletrônico SRP nº 020/2018, da Lei nº 10.520/02 e ainda, da Lei nº
8.666/93,
            RESOLVE:
 
            Aplicar a sanção administrativa de MULTA de 0,33% (zero vírgula trinta e três por cento) ao dia sobre o
valor da parcela que lhe deu causa, no montante de R$ 336,33 (trezentos e trinta e seis reais e trinta e três
centavos), conforme demonstrativo de cálculo (Doc. SEI nº 1988759), prevista no item 10.2.2 do Termo de
Referência, à Empresa BRASIDAS EIRELI - ME, CNPJ n.º 20483193/0001-96, pelo motivo de atraso na entrega
do objeto, conforme atestado pela fiscalização do contrato (Doc. SEI nº 1870825 e 1988549), bem como pelo
DICON/NUDECON (Doc. SEI nº 1988717) do processo 23078.530419/2019-10.
 
            Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
