Documento gerado sob autenticação Nº SJU.614.104.TH7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3824                  de  24/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de GABRIELA CORREA SOUZA, Professor do Magistério Superior,
lotada e em exercício no Departamento de Nutrição da Faculdade de Medicina, com a finalidade de participar
do XIX International Congress on Nutrition and Metabolism in Renal Disease", em Genova, Itália, no período
compreendido entre 24/06/2018 e 01/07/2018, incluído trânsito, com ônus limitado. Solicitação nº 46289.
RUI VICENTE OPPERMANN
Reitor
