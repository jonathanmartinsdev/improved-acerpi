Documento gerado sob autenticação Nº DIR.287.450.VR1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2634                  de  25/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  à  servidora  MARGARETE DAS NEVES ANTUNES,  ocupante  do cargo de  Técnico  em
Secretariado - 701275, lotada no Instituto de Ciências Básicas da Saúde, SIAPE 0353877, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 19/03/2019, tendo em vista a conclusão
do Curso Superior de Tecnologia em Gestão Pública, conforme o Processo nº 23078.506874/2019-96.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
