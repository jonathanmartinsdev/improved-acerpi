Documento gerado sob autenticação Nº TPB.074.317.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5631                  de  05/07/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, TALES TIECHER, matrícula SIAPE n° 2316711, lotado no Departamento de Solos
da Faculdade de Agronomia, como Coordenador Substituto do PPG em Ciência do Solo, com vigência a partir
de 09/08/2019 até 08/08/2021. Processo nº 23078.516323/2019-31.
RUI VICENTE OPPERMANN
Reitor.
