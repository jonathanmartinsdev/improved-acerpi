Documento gerado sob autenticação Nº LLK.313.425.RAQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             25                  de  03/01/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  15/12/2016,   referente  ao  interstício  de
30/03/2015 a 14/12/2016,  para a servidora LISIANE REIS VICENTE,  ocupante do cargo de Auxiliar em
Administração -  701405,  matrícula  SIAPE 1590605,   lotada  no  Instituto de Ciências  e  Tecnologia  de
Alimentos, passando do Nível de Classificação/Nível de Capacitação C I, para o Nível de Classificação/Nível de
Capacitação  C  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.204249/2016-60:
Formação Integral de Servidores da UFRGS III  CH: 72 Carga horária utilizada: 60 hora(s) / Carga horária
excedente: 12 hora(s) (08/06/2015 a 30/11/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
