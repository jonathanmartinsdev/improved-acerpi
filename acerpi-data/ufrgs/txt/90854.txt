Documento gerado sob autenticação Nº CRR.658.923.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3952                  de  09/05/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5032131-82.2010.4.04.7100,  da  2ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº1.874 de
13/07/2006, da servidora CARMEN TERESINHA FRASSA, matrícula SIAPE n° 0351591, aposentada no cargo
de Assistente em Administração - 701200, do Nível I para o Nível II, a contar de 01/01/2006, conforme o
Processo nº 23078.511643/2019-02.
 
RUI VICENTE OPPERMANN
Reitor
