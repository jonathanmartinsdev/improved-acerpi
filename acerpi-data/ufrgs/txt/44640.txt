Documento gerado sob autenticação Nº SCV.769.604.7IP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8659                  de  15/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/09/2017,   referente  ao  interstício  de
11/03/2016 a 10/09/2017, para a servidora IVONE TERESINHA ANGST, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2162148,  lotada  no  Campus Litoral Norte, passando do Nível de
Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.515286/2017-81:
Formação Integral de Servidores da UFRGS V CH: 120 (01/12/2014 a 22/11/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
