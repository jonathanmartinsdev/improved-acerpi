Documento gerado sob autenticação Nº GVG.833.622.M0L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6982                  de  02/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°35169,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MARIA JOAO VELOSO DA COSTA RAMOS PEREIRA
(Siape: 2164851 ),  para substituir   LUIZ ROBERTO MALABARBA (Siape: 0356678 ), Coordenador do PPG em
Biologia  Animal,  Código  FUC,  em seu  afastamento  por  motivo  de  férias,  no  período de  31/07/2017  a
16/08/2017.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
