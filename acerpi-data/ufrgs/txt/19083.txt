Documento gerado sob autenticação Nº DLS.601.023.SNE, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1900                  de  11/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Designar, nos termos do art. 1° da Lei n° 1.234, de 14 de novembro de 1950, combinado com o art.
1° e o parágrafo único do art. 7°, do Decreto 81.384, de 22 de fevereiro de 1978, alterado pelo Decreto
84.106, de 22 de outubro de 1979 e com os artigos 72 e 79, da Lei n° 8112, de 11 de dezembro de 1990, o Sr.
PAULO SANDLER, matriculado no SIAPE sob nº 0354832,  ocupando o cargo de Professor do Magistério
Superior, lotado e em exercício no Departamento de Cirurgia da Faculdade de Medicina, para operar direta,
obrigatória e habitualmente com Raio X ou substâncias radioativas, fazendo jus ao pagamento da respectiva
gratificação, calculado com base no percentual de sobre o vencimento do cargo efetivo, conforme art. 12, da
Lei n° 8.270, de 17 de dezembro de 1991, combinado com o art. 68, da Lei n° 8.112, de 11 dezembro de 1990,
com efeitos financeiros a partir de 10/02/2016. Processo nº  23078.020901/2014-23.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
