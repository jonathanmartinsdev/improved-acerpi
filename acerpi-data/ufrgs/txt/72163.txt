Documento gerado sob autenticação Nº HAJ.533.936.E6H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9190                  de  14/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/11/2018,   referente  ao  interstício  de
04/05/2017 a 11/11/2018, para o servidor JOAO UBIRAJARA DA ROSA MARTINS,  ocupante do cargo de
Vigilante - 701269, matrícula SIAPE 0356435,  lotado  na  Coordenadoria de Segurança da UFRGS, passando
do Nível de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV,
em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.531299/2018-89:
Formação Integral de Servidores da UFRGS V CH: 131 (06/12/2011 a 28/07/2016)
UNIASSELVI - Cultura Africana CH: 20 Carga horária utilizada: 19 hora(s) / Carga horária excedente: 1 hora(s)
(20/10/2012 a 14/04/2013)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
