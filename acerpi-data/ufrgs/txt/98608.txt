Documento gerado sob autenticação Nº FVG.258.476.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8739                  de  26/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento do País  de DANIEL UMPIERRE DE MORAES,  Professor  do Magistério
Superior,  lotado e em exercício no Departamento de Saúde Coletiva da Escola de Enfermagem, com a
finalidade  de  participar  da  "5th  International  Clinical  Trials  Methodology  Conference",  em  Brighton,
Inglaterra, no período compreendido entre 04/10/2019 e 10/10/2019, incluído trânsito, com ônus limitado.
Solicitação nº 86517.
RUI VICENTE OPPERMANN
Reitor
