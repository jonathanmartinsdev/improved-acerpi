Documento gerado sob autenticação Nº PGU.155.828.BCG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1555                  de  13/02/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 5658, de 29 de junho de 2017
RESOLVE
                         Autorizar o afastamento no País de ANA LUIZA PAGANELLI CALDAS, ocupante do cargo de
Professor do Magistério Superior, lotada e com exercício no Departamento de Estudos Especializados, com a
finalidade de realizar estudos em nível de Doutorado junto à Universidade Federal de Pelotas, em Pelotas,
Rio Grande do Sul, no período compreendido entre 28/02/2019 e 27/02/2020, com ônus limitado. Processo
23078.527890/2018-31.
DANILO BLANK
Decano do Conselho Universitário, no exercício da Reitoria.
