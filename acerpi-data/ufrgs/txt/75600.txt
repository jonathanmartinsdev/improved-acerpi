Documento gerado sob autenticação Nº JVT.164.346.TUP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             816                  de  23/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  DENISE  FAGUNDES  JARDIM,  matrícula  SIAPE  n°  0359323,  lotada  no  Departamento  de
Antropologia  do  Instituto  de  Filosofia  e  Ciências  Humanas  e  com  exercício  na  Coordenadoria  de
Acompanhamento do Programa de Ações Afirmativas, da classe D  de Professor Associado, nível 04, para a
classe E  de Professor Titular,  referente ao interstício de 16/07/2016 a 18/12/2018, com vigência financeira a
partir de 26/12/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.535524/2018-56.
JANE FRAGA TUTIKIAN
Vice-Reitora.
