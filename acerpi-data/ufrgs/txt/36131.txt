Documento gerado sob autenticação Nº JPN.530.180.MDU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2714                  de  28/03/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 40, parágrafo 1º, inciso I, da Constituição Federal, c/c
art. 6-A da Emenda Constitucional nº 41, de 19 de dezembro de 2003, incluído pela Emenda Constitucional nº
70, de 29 de março de 2012, a VILSON TADEU SILVA DOS SANTOS, matrícula SIAPE nº 0356780, no cargo de
Pedreiro, nível de classificação B, nível de capacitação I, padrão 16, do Quadro desta Universidade, no regime
de quarenta horas semanais de trabalho, com exercício na Prefeitura Campus do Vale da Superintendência
de Infraestrutura, com proventos proporcionais. Processo 23078.023745/2016-14.
RUI VICENTE OPPERMANN
Reitor.
