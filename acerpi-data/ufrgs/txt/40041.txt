Documento gerado sob autenticação Nº BRP.485.904.TGG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5586                  de  27/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e Funções
Gratificadas - FG, desta Universidade, a partir da data deste ato, como segue: 
 Transformar:  Chefe do Setor de Manutenção Mecânica,  Código FG-4,  em Chefe do Setor Tecnologia da
Informação da SUINFRA, Código FG-4;
 - Chefe do Setor de Oficinas de Produção vinculado ao Departamento de Logística e Suprimentos da SUINFRA,
Código FG-2, em Chefe do Setor de Oficinas de Produção e Manutenção Mecânica - SUINFRA, Código FG-2;
 - Chefe do Setor de Fiscalização e Adequação vinculado ao Departamento de Adequações de Infraestrutura da
SUINFRA, Código FG-5, em Coordenador de Infraestrutura de Comunicação - SUINFRA, Código FG-5;
 - Chefe do Setor de Atualização e Controle de Cadastros, vinc. à Gerência Administrativa da SUINFRA, Código
FG-3, em Chefe do Setor de Fiscalização e Adequação - SUINFRA, Código FG-3;
 - Coordenador do Núcleo de Infraestrutura da Pref do Campus Litoral Norte da Vice Sup de Manut SUINFRA,
Código FG-1, em Coordenador do Núcleo de Infraestrutura da Prefeitura do Campus Litoral Norte - SUINFRA, Código FG-
1;
 -  Chefe  do  Setor  de  Tecnologia  da  Informação  vinculado  à  Vice-Superintendência  de  Infraestrutura  da
SUINFRA, Código FG-1, em Assessor Administrativo do Departamento de Suporte à Infraestrutura - SUINFRA, Código FG-1;
 - Chefe do Setor de Acessibilidade, vinc. à Vice-Superintendência de Obras da SUINFRA, Código FG-5, em
Coordenador de Infraestrutura da Prefeitura Campus do Vale - SUINFRA, Código FG-5;
 - Diretor do Departamento de Logística e Suprimentos da SUINFRA, Código CD-4, em Diretor do Departamento
de Suporte à Infraestrutura - SUINFRA, Código CD-4.
JANE FRAGA TUTIKIAN
Vice-Reitora.
