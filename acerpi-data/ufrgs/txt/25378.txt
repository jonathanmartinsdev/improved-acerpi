Documento gerado sob autenticação Nº ZSR.745.934.84S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5797                  de  02/08/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições e
tendo em vista a Portaria nº 5148, de 01 de outubro de 2012,
RESOLVE
Homologar a prorrogação de afastamento no País de RUTE VERA MARIA FAVERO, Professor do
Magistério do Ensino Básico, Técnico e Tecnológico, lotada  no  Colégio de Aplicação e com exercício na
Coordenação Acadêmica da SEAD, com a finalidade de concluir estudos em nível de Doutorado, junto à
Universidade Federal do Rio Grande do Sul, no período compreendido entre 01 de agosto e 30 de setembro
de 2016, com ônus limitado. Processo nº 23078.010502/13-75.
RUI VICENTE OPPERMANN
Vice-Reitor
