Documento gerado sob autenticação Nº MVU.030.588.PID, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6360                  de  24/08/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 5263, de 01 de outubro de
2012, e conforme a Solicitação de Férias n°25635,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, CINARA LERRER ROSENFIELD (Siape: 1305053 ),  para
substituir   MARILIS LEMOS DE ALMEIDA (Siape: 1150003 ), Coordenador do PPG em Sociologia, Código FUC,
em seu afastamento por motivo de férias,  no período de 13/08/2016 a 22/08/2016,  com o decorrente
pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
