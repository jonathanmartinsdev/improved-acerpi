Documento gerado sob autenticação Nº BUD.417.149.731, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9330                  de  14/10/2019
  Nomeação  do  Diretor  Brasileiro  do
Instituto  Confúcio.
              O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, e
considerando a Decisão nº 154/2011 do CONSUN, 
RESOLVE:
nomear, a partir desta data, o ocupante do cargo de Professor da Carreira do Magistério Superior, do Quadro
de  Pessoal  desta  Universidade,  ANTONIO  DOMINGOS  PADULA,  como  Diretor  Brasileiro  do  Instituto
Confúcio - UFRGS, com mandato de 1 (um) ano.
RUI VICENTE OPPERMANN,
Reitor.
