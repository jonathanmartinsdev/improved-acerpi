Documento gerado sob autenticação Nº OBF.299.649.OSS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8219                  de  09/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar  a  Portaria  nº  5940,  de  15/07/2019,  que  designou,  temporariamente,  LEONARDO DO
PRADO RODRIGUES  (Siape: 2156563)  para substituir ISMAEL CABRAL (Siape: 2994373 ),  Processo SEI nº
23078.523548/2019-43
 
onde se lê:
 
"...em seu afastamento por  motivo de férias,  no período de 15/07/2019 a  30/07/2019,  com o
decorrente pagamento das vantagens por 16 dias...";
 
          leia-se:
 
"...  em seu afastamento por motivo de férias,  no período de 15/07/2019 a 01/08/2019, com o
decorrente pagamento das vantagens por 18 dias..."; ficando ratificados os demais termos. 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
