Documento gerado sob autenticação Nº FVT.838.100.TOU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2249                  de  14/03/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LILIANE BASSO BARICHELLO, Professor do Magistério Superior,
lotada e  em exercício  no Departamento de Matemática  Pura e  Aplicada do Instituto de Matemática  e
Estatística, com a finalidade de participar da "International Conference on Mathematics & Computational
Methods Applied to Nuclear Science & Engineering", em Jeju, Coreia Do Sul, no período compreendido entre
13/04/2017 e 22/04/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação
nº 26371.
RUI VICENTE OPPERMANN
Reitor
