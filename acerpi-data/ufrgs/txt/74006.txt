Documento gerado sob autenticação Nº JNJ.454.621.GA9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10423                  de  26/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  JAIR FELIPE BONATTO UMANN,  Professor  do Magistério
Superior, lotado e em exercício no Departamento de Educação Física, Fisioterapia e Dança da Escola de
Educação Física, Fisioterapia e Dança, com a finalidade de realizar visita à Universidad de Córdoba, Espanha;
à Université Paris 8 e encontro no Centre National de la Danse, em Paris, França, no período compreendido
entre 09/01/2019 e 03/02/2019, incluído trânsito, com ônus limitado. Solicitação nº 61840.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
