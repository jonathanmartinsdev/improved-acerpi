Documento gerado sob autenticação Nº VOM.907.826.OU9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2200                  de  10/03/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MANOELA DOMINGUES MARTINS,  Professor do Magistério
Superior,  lotada  e  em  exercício  no  Departamento  de  Odontologia  Conservadora  da  Faculdade  de
Odontologia, com a finalidade de realizar visita  à University of Michigan, em Ann Arbor, Estados Unidos, no
período compreendido entre 29/03/2017 e 06/04/2017, incluído trânsito, com ônus limitado. Solicitação nº
26511.
RUI VICENTE OPPERMANN
Reitor
