Documento gerado sob autenticação Nº ADL.267.798.CP4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10960                  de  05/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor FELIPE GRANDO BRANDAO, ocupante do cargo de  Administrador - 701001,
lotado na Secretaria de Desenvolvimento Tecnológico, SIAPE 2249444, o percentual de 52% (cinquenta e dois
por cento) de Incentivo à Qualificação, a contar de 02/09/2016, tendo em vista a conclusão do curso de
Mestrado em Gestão Estratégica de Organizações, conforme o Processo nº 23078.019627/2016-10.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
