Documento gerado sob autenticação Nº ITA.311.446.VEN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             855                  de  26/01/2017
A PRÓ-REITORA DE GRADUAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7628, de 29 de setembro de 2016
RESOLVE
Designar
LETICIA PREZZI FERNANDES - Pró-Reitoria de Graduação;
AKIE YOSHIOKA - Escola de Engenharia;
AMANDA DE MELLO MARTINS - Instituto de Matemática e Estatística;
para, sob a presidência da primeira, constituírem a Comissão de Recursos da não homologação da
documentação escolar, a partir desta data.
ANDREA DOS SANTOS BENITES
Pró-Reitora de Graduação em exercício
