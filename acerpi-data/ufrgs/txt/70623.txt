Documento gerado sob autenticação Nº GBQ.804.166.F13, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8026                  de  08/10/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do País  de MARCIA CANÇADO FIGUEIREDO,  Professor do Magistério
Superior, lotada e em exercício no Departamento de Cirurgia e Ortopedia da Faculdade de Odontologia, com
a finalidade de proferir palestra junto ao Centro de Estudios de Posgrado em Odontologia, em Guadalajara,
México, no período compreendido entre 28/10/2018 e 31/10/2018, incluído trânsito, com ônus limitado.
Solicitação nº 59886.
RUI VICENTE OPPERMANN
Reitor
