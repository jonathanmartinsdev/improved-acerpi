Documento gerado sob autenticação Nº PAE.002.831.E5L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4937                  de  02/06/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de DEISE PONZONI, Professor do Magistério Superior, lotada no
Departamento de Cirurgia e Ortopedia da Faculdade de Odontologia e com exercício na Vice-Direção do
Faculdade de Odontologia, com a finalidade de participar do "XX Congreso Internacional de la Asociación
Latinomaricana  de  Cirugía  y  Traumatología  Bucomaxilofacial",  em Buenos  Aires,  Argentina,  no  período
compreendido entre 06/08/2017 e 10/08/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa -
diárias). Solicitação nº 28116.
RUI VICENTE OPPERMANN
Reitor
