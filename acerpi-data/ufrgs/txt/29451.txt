Documento gerado sob autenticação Nº OHT.735.332.5SL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8412                  de  19/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar RAFAEL COSTA RODRIGUES, matrícula SIAPE n° 2687227, ocupante do cargo de Professor
do Magistério Superior, classe Adjunto, lotado no Departamento de Tecnologia dos Alimentos do Instituto de
Ciências e Tecnologia de Alimentos, do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul,
como  Chefe do Depto de Tecnologia de Alimentos do ICTA, Código SRH 213, código FG-1, com vigência a
partir da data de publicação no Diário Oficial da União e até 10/06/2017, a fim de completar o mandato de
Pascual  Isoldi  Pinkoski,  conforme  artigo  92  do  Estatuto  da  mesma  Universidade.  Processo  nº
23078.203704/2016-18.
JANE FRAGA TUTIKIAN
Vice-Reitora
