Documento gerado sob autenticação Nº IZL.174.478.OMJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             3274                  de  18/04/2017
Nomeia  representantes  discentes
no  Curso  de  Odontologia,  na
Faculdade  de  Odontologia.
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita no Curso de Odontologia, na Faculdade de Odontologia,
com mandato de 01 (um) ano, a contar de 02 de março de 2017, atendendo ao disposto nos artigos 175 do
Regimento  Geral  da  Universidade  e  79  do  Estatuto  da  Universidade  e  considerando  o  processo
nº 23078.003215/2017-31, conforme segue:
 
CONSELHO DA UNIDADE
Titular Guilherme Scopel Rodrigues
Titular Cezar Henrique Krausburg Vargas
Suplente Tiago Herpich
Suplente Vanessa Thais Cassel
COMISSÃO DE GRADUAÇÃO
Titular Guilherme Scopel Rodrigues
Titular Cezar Henrique Krausburg Vargas
Suplente Willian Konflanz
Suplente Eduardo Liberato
COMISSÃO DE EXTENSÃO
Titular Natália Dier
Suplente Willian Konflanz
COMISSÃO DE PÓS-GRADUAÇÃO
Titular Tiago Herpich
Suplente Cezar Henrique Krausburg Vargas
COMISSÃO DE PESQUISA
Titular Tiago Herpich
Suplente Vanessa Thais Cassel
COMISSÃO DE SAÚDE E AMBIENTE DO TRABALHO
Titular Juliane Gonçalves da Fonseca
Documento gerado sob autenticação Nº IZL.174.478.OMJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Suplente Juliane Gonçalves da Silva
CONSELHO DO DEPARTAMENTO DE ODONTOLOGIA CONSERVADORA
Titular Guilherme Scopel Rodrigues
Titular Natália Dier
Suplente Juliane Gonçalves da Fonseca
Suplente Gabriela Carvalho Massa
PLENÁRIA DO DEPARTAMENTO DE ODONTOLOGIA CONSERVADORA
Titular Guilherme Scopel Rodrigues
Titular Gabriela Carvalho Massa
Titular Willian Konflanz
Titular Cezar Henrique Krausburg Vargas
Titular Tiago Herpich
Titular Natália Dier
Suplente Vanessa Thais Cassel
Suplente Débora Grando
Suplente Eduardo Liberato
Suplente Juliane Gonçalves da Fonseca
Suplente Juliane Gonçalves da Silva
Suplente Giovana Martiniano Mendes
DEPARTAMENTO DE CIRURGIA E ORTOPEDIA
Titular Gabriela Carvalho Massa
Titular Willian Konflanz
Suplente Débora Grando
Suplente Natália Dier
DEPARTAMENTO DE ODONTOLOGIA PREVENTIVA E SOCIAL
Titular Vanessa Thais Cassel
Titular Juliane Gonçalves da Silva
Suplente Juliane Gonçalves da Fonseca
Suplente Giovana Martiniano Mendes
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
