Documento gerado sob autenticação Nº VOX.890.547.CR0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9446                  de  17/10/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  PATRICIA  CHITTONI  RAMOS  REUILLARD,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Línguas Modernas do Instituto de Letras,
com a finalidade de realizar estudos em nível de Pós-Doutorado junto à Université Sorbonne Nouvelle - Paris
3,  em  Paris,  França,  no  período  compreendido  entre  02/12/2019  e  31/03/2020,  com  ônus
CAPES/PRINT/UFRGS.  Processo  nº  23078.525419/2019-90.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
