Documento gerado sob autenticação Nº AKU.366.318.R1J, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1745                  de  08/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de Outubro de 2012
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de Técnico em Artes Gráficas, do
Quadro de Pessoal desta Universidade, ALBERTO JOSE MARTINS NETO (Siape: 0350970),  para substituir
DAVID SENGIK RIBEIRO (0356011), Assessor da Direção do Departamento de Atenção à Saúde/PROGESP, FG-
3, em seu afastamento por motivo de férias, no período de 10/02/2016 a 03/03/2016, com o decorrente
pagamento das vantagens por 23 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
