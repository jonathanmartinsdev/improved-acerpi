Documento gerado sob autenticação Nº OXT.129.229.RJB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8769                  de  27/10/2016
  Designa integrantes para o Comitê Gestor
Institucional  de  Formação  Inicial  e
Continuada de Profissionais do Magistério
da  Educação  Básica,  no  âmbito  desta
Universidade.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais
RESOLVE:
Art. 1º Designar
      
      JANE FRAGA TUTIKIAN
      CELSO GIANNETTI LOUREIRO CHAVES
      HELIO HENKIN
      LOVOIS DE ANDRADE MIGUEL
      MARCUS VINICIUS DE AZEVEDO BASSO
      ROSELANE ZORDAN COSTELLA
      SANDRA DE FATIMA BATISTA DE DEUS
      SIMONE VALDETE DOS SANTOS
      VLADIMIR PINHEIRO DO NASCIMENTO
 
para,  sob  a  Presidência  do  primeiro,  integrarem  o  Comitê  Gestor  Institucional  de  Formação  Inicial  e
Continuada de Profissionais do Magistério da Educação Básica no âmbito da Universidade Federal do Rio
Grande do Sul, vinculado à Pró-Reitoria de Coordenação Acadêmica, com as competências estabelecidas na
Portaria n.º 1.738 de 11 de abril de 2012 da Reitoria.
 
                 Art. 2º  Revogar a Portaria nº 5602 de 27de julho de 2015.
 
RUI VICENTE OPPERMANN,
Reitor.
