Documento gerado sob autenticação Nº GDM.604.572.96L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6711                  de  30/08/2016
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 5227, de 01 de outubro de
2012tendo em vista o disposto no artigo 7° do Decreto n° 3.555, 08 de agosto de 2000, no artigo 3°, inciso IV
da Lei n°10.520/2002,
R E S O L V E:
 
 
Art. 1° - Designar como PREGOEIROS da Universidade Federal do Rio Grande do Sul, os servidores: JOSÉ JOÃO
MARIA DE AZEVEDO, ocupante do cargo de Secretário Executivo, MARCOS JOSÉ DA SILVA , ocupante do cargo
de Administrador,  JOSÉ ROBERTO DUARTE COLCHETE,  MARCELO UTZ ASCONAVIETA,  MARILÉA TREVISAN
BASTOS DE QUADROS, TURENE ANDRADE E SILVA NETO, ocupantes do cargo de Assistente em Administração
e,  VINICIUS DE OLIVEIRA VIEIRA, Técnico em Contabilidade.
 
 Art. 2° - Designar para compor a equipe de apoio os servidores JOSÉ JOÃO MARIA DE AZEVEDO, ocupante do
cargo de Secretário Executivo, MARCOS JOSÉ DA SILVA, ocupante do cargo de Administrador, JOSÉ ROBERTO
DUARTE COLCHETE,  MARCELO UTZ ASCONAVIETA,  MARILÉA TREVISAN BASTOS DE QUADROS ,  TURENE
ANDRADE E SILVA NETO, ocupantes do cargo de Assistente em Administração e,  VINICIUS DE OLIVEIRA
VIEIRA, Técnico em Contabilidade.
 
Art. 3°- Para fins previstos na Lei n° 10.520/2002, a autoridade imediatamente superior ao pregoeiro é o Pró-
Reitor ou o Vice-Pró-Reitor de Planejamento e Administração.
 
Art. 4°- Esta Portaria entra em vigor na data de sua publicação, e tem validade até 30/08/2017.
 
 
 
 
ARIO ZIMMERMANN
Pró-Reitor de Planejamento e Administração
