Documento gerado sob autenticação Nº CCZ.281.348.5L6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9421                  de  22/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°44778,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ISTEFANI CARÍSIO DE PAULA (Siape: 1565585 ),  para
substituir   CARLA SCHWENGBER TEN CATEN (Siape: 2174654 ), Diretor da Incubadora Tecnológica Hestia, em
seu afastamento por motivo de férias, no período de 22/11/2018 a 01/12/2018.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
