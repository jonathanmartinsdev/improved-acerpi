Documento gerado sob autenticação Nº JZX.551.444.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7269                  de  12/08/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5081245-09.2018.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, da servidora SANDRA VIEIRA LARRATEA, matrícula SIAPE n° 0756505, aposentada no cargo de
Assistente Social  -  701006, do nível II  para o nível IV,  a contar de 01/01/2006, conforme o Processo nº
23078.520572/2019-21.
 
RUI VICENTE OPPERMANN
Reitor
