Documento gerado sob autenticação Nº SAH.122.535.L81, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7619                  de  29/09/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas  atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Retificar a Portaria n° 7176/2016, de 12/09/2016, que concedeu autorização para afastamento do
País a LUCIANO PASCHOAL GASPARY, Professor do Magistério Superior, com exercício no Departamento de
Informática Aplicada
Onde se lê: com ônus limitado,
leia-se: com ônus CNPq (  Chamada Universal 14/2013 -  Faixa B),  ficando ratificados os demais
termos. Solicitação nº 23494.
RUI VICENTE OPPERMANN
Reitor
