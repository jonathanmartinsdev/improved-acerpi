Documento gerado sob autenticação Nº VXJ.430.713.UH3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7903                  de  05/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Tornar insubsistente a Portaria nº 6683/2016, de 30/08/2016, publicada no Diário Oficial da União
de  01/09/2016,  que  concedeu  autorização  para  afastamento  do  País  a  VERA  MARIA  VIDAL  PERONI,
ocupante do cargo de Professor do Magistério Superior, lotada e em exercício no Departamento de Estudos
Especializados da Faculdade de Educação. Solicitação nº 21720.
RUI VICENTE OPPERMANN
Reitor
