Documento gerado sob autenticação Nº OXD.400.860.O05, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8873                  de  22/09/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARCELO PRIEBE GIL, Professor do Magistério Superior, lotado
e em exercício  no Departamento de Química Inorgânica do Instituto de Química,  com a finalidade de
participar de reunião junto à University of Rennes I,  França, no período compreendido entre 12/10/2017 e
23/10/2017, incluído trânsito, com ônus CAPES/COFECUB. Solicitação nº 31384.
RUI VICENTE OPPERMANN
Reitor
