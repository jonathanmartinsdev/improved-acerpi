Documento gerado sob autenticação Nº DEH.143.123.SE7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3367                  de  09/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°45840,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, LUCIANA BARCELLOS TEIXEIRA (Siape: 1462333 ),
 para substituir   STELA NAZARETH MENEGHEL (Siape: 1766933 ), Coordenador do PPG em Saúde Coletiva da
Escola de Enfermagem, Código FUC, em seu afastamento do país, no período de 09/05/2018 a 10/05/2018,
com o decorrente pagamento das vantagens por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
