Documento gerado sob autenticação Nº HUK.420.450.PN6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5211                  de  16/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Remover, a partir de 19 de junho de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
SIBILA  FRANCINE  TENGATEN  BINOTTO,  ocupante  do  cargo  de  Bibliotecário-documentalista,  Ambiente
Organizacional Informação, Código 701010, Classe E, Nível de Capacitação IV, Padrão de Vencimento 07,
SIAPE nº. 1685641 do Instituto de Geociências para a lotação Faculdade de Educação, com novo exercício na
Biblioteca da Faculdade de Educação.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
