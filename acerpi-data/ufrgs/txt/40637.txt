Documento gerado sob autenticação Nº YKL.243.495.KHP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6000                  de  07/07/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar ARTHUR LIMA DE AVILA, matrícula SIAPE n° 1899299, ocupante do cargo de Professor do
Magistério Superior, classe Adjunto, lotado no Departamento de História do Instituto de Filosofia e Ciências
Humanas, do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Coordenador da
Comissão de Extensão do IFCH, Código SRH 735, com vigência a partir da data deste ato e até 11/04/2018, a
fim de completar o mandato de Caleb Faria Alves, conforme artigo 92 do Estatuto da mesma Universidade.
Processo nº 23078.201584/2017-97.
RUI VICENTE OPPERMANN
Reitor.
