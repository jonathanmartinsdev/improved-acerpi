Documento gerado sob autenticação Nº KWF.212.763.GH1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2776                  de  29/03/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor HAROLD OSPINA PATINO, matrícula SIAPE n° 1222118, lotado e em exercício no Departamento de
Zootecnia da Faculdade de Agronomia, da classe D  de Professor Associado, nível 04, para a classe E  de
Professor Titular,  referente ao interstício de 01/05/2012 a 20/02/2019, com vigência financeira a partir de
21/02/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a
Decisão nº 331/2017 do CONSUN. Processo nº 23078.535565/2018-42.
JANE FRAGA TUTIKIAN
Vice-Reitora.
