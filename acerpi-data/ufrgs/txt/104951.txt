Documento gerado sob autenticação Nº ITD.766.959.U54, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1373                  de  07/02/2020
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar  o  afastamento no País  de ELISANDRO SCHULTZ WITTIZORECKI,  ocupante  do cargo
de Professor do Magistério Superior,  lotado e em exercício no Departamento de Educação Física, Fisioterapia
e Dança da Escola de Educação Física, Fisioterapia e Dança, com a finalidade de participar de banca de
concurso junto à Universidade Federal de Pelotas, em Pelotas - RS, Brasil, no período compreendido entre
03/02/2020 e 16/02/2020, incluído trânsito, com ônus UFPEL/ESEF. Solicitação n° 89754.
JANE FRAGA TUTIKIAN
Vice-Reitora.
