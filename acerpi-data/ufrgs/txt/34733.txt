Documento gerado sob autenticação Nº SPP.214.721.ACG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1761                  de  22/02/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de AFONSO REGULY, Professor do Magistério Superior, lotado no
Departamento  de  Metalurgia  da  Escola  de  Engenharia  e  com exercício  na  Comissão  de  Extensão  em
Engenharia, com a finalidade de participar da "CORROSION 2017 Conference & Expo", em New Orleans,
Estados Unidos,  no período compreendido entre 24/03/2017 e 01/04/2017,  incluído trânsito,  com ônus
limitado. Solicitação nº 26016.
RUI VICENTE OPPERMANN
Reitor
