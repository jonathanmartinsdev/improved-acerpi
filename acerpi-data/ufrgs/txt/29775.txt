Documento gerado sob autenticação Nº HAK.171.549.QJH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8657                  de  25/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/10/2016,   referente  ao  interstício  de
10/04/2015 a 10/10/2016,  para a servidora CRISTIANE BASSO,  ocupante do cargo de Administrador -
701001, matrícula SIAPE 1744267,  lotada  na  Pró-Reitoria de Gestão de Pessoas, passando do Nível de
Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.022994/2016-92:
Formação Integral de Servidores da UFRGS V CH: 133 (29/10/2013 a 25/11/2014)
UFAM - XXXIV Encontro Nacional de Dirigentes de Pessoal e Recursos Humanos das Instituições Federais de
Ensino - ENDP CH: 20 Carga horária utilizada: 9 hora(s) / Carga horária excedente: 11 hora(s) (01/09/2014 a
04/09/2014)
PM Tech - Capacitação em Gerenciamento Projetos e Preparação para a Certificação PMP e CAPM CH: 8
(06/10/2014 a 29/10/2014)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
