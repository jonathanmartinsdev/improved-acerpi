Documento gerado sob autenticação Nº CDN.639.433.R56, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10304                  de  08/11/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor FRANCISCO CARLOS BRAGANCA DE SOUZA, matrícula SIAPE n° 0352425, lotado e em exercício no
Departamento  de  Obras  Hidráulicas  do  Instituto  de  Pesquisas  Hidráulicas,  da  classe  D   de  Professor
Associado, nível 04, para a classe E  de Professor Titular,  referente ao interstício de 31/12/2014 a 27/10/2017,
com vigência financeira a partir de 28/10/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  232/2014  -  CONSUN.  Processo  nº
23078.520507/2017-33.
JANE FRAGA TUTIKIAN
Vice-Reitora.
