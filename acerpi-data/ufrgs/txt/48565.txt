Documento gerado sob autenticação Nº LCS.764.231.25J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11450                  de  26/12/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar  o  afastamento  no  país  de  ROMMULO  VIEIRA  CONCEIÇÃO,  ocupante  do  cargo
de Professor do Magistério Superior,  lotado no Departamento de Geologia do Instituto de Geociências e com
exercício  no  Programa  de  Pós-Graduação  em  Geociências,   com  a  finalidade  de  participar  do  "7the
Transmission Electron Microscopy (TEM) Summer School", em Campinas, Brasil, no período compreendido
entre 14/01/2018 e 26/01/2018, incluído trânsito, com ônus limitado. Solicitação n° 33682.
JANE FRAGA TUTIKIAN
Vice-Reitora.
