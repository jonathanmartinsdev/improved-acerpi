Documento gerado sob autenticação Nº KNE.815.048.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9179                  de  10/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5008987-64.2019.4.04.7100,  da  8ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006,  da  servidora  KAREN BRUCK DE  FREITAS,  matrícula  SIAPE  n°  0356964,  ativa  no  cargo  de
Sociólogo  -  701077,  do  nível  I  para  o  nível  IV,  a  contar  de  01/01/2006,  conforme  o  Processo  nº
23078.527246/2019-44.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
RUI VICENTE OPPERMANN
Reitor
