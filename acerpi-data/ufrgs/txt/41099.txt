Documento gerado sob autenticação Nº XPY.089.900.MSH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6328                  de  17/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7864, de 10 de outubro
de 2016,  e em cumprimento à decisão judicial  proferida no Processo nº 5034043-30.2017.404.0000,  do
Tribunal Regional Federal da 4ª Região,
RESOLVE
Conceder à ocupante do cargo de Programador Visual, CLARISSA FELKL PREVEDELLO, lotada na
Editora da UFRGS e com exercício na Seção de Editoração da Editora da UFRGS, Licença por Motivo de
Afastamento de Cônjuge, sem remuneração, nos termos do artigo 84, "caput", da Lei n° 8.112, de 11 de
dezembro de 1990, com a redação alterada pela Lei n° 9.527/97, a partir de 17 de julho de 2017 até 31 de
janeiro de 2018. Processo n° 23078.502433/2017-53.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
