Documento gerado sob autenticação Nº ZNO.942.286.KBF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9000                  de  26/09/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 40, parágrafo 1º, inciso I, da Constituição Federal, na
forma disposta no artigo 1º da Emenda Constitucional nº 41, de 19 de dezembro de 2003, a ADRIANA
BERLEZE,  matrícula SIAPE nº 1447491, no cargo de Professor Adjunto, nível 2, da Carreira do Magistério
Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no Departamento
de Educação Física da Escola de Educação Física, Fisioterapia e Dança, com proventos integrais e calculados
de acordo com o artigo 1º da Lei nº 10.887, de 18 de junho de 2004. Processo 23078.018255/2017-87.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
