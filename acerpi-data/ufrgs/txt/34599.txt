Documento gerado sob autenticação Nº CBY.132.884.P40, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1621                  de  20/02/2017
A SUPERINTENDENTE DE INFRAESTRUTURA EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7793, de 03 de outubro
de 2016
RESOLVE
Designar os servidores,  MARCELO SOBIERAYSKI MATUSIAK, ocupante do cargo de Engenheiro-
área,  lotado  na  Superintendência  de  Infraestrutura  e  com  exercício  no  Departamento  de  Projetos  e
Planejamento, ALEXANDRE DE LIMA, ocupante do cargo de Engenheiro-área, lotado na Superintendência de
Infraestrutura e com exercício na Prefeitura Campus Saúde e Olímpico, GISELA MARIA SCHEBELLA SOUTO
DE MOURA, ocupante do cargo de Professor do Magistério Superior, lotada no Departamento de Assistência
e Orientação Profissional da Escola de Enfermagem e com exercício na Escola de Enfermagem, para sob a
presidência do primeiro, constituirem a Comissão de Recebimento, em atendimento à alínea "b" do inciso I
do artigo 73 da Lei  8666/93,  para emissão do Termo de Recebimento Definitivo da Obra do contrato
108/2015,  cujo  objeto  é  "REFORMA  DAS  INSTALAÇÕES  ELÉTRICAS  DO  PRÉDIO  DA  ESCOLA  DE
ENFERMAGEM (PRÉDIO 21103) - Sistema de Proteção contra Descargas Atmosféricas, no Campus Saúde
da UFRGS, em Porto Alegre  RS, com prazo até 25/03/2017 quando se encerra o prazo de vigência do
contrato. Processo nº 23078.001946/2014-07.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
ANDREA PINTO LOGUERCIO
Superintendente de Infraestrutura em exercício
