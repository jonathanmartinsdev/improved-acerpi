Documento gerado sob autenticação Nº ELP.536.550.V9R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3704                  de  21/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias,
RESOLVE
Dispensar,  a  pedido,  a partir  de 23/04/2018,  o ocupante do cargo de Professor do Magistério
Superior, classe Titular do Quadro de Pessoal desta Universidade, ARIO ZIMMERMANN, matrícula SIAPE n°
0351295, da função de Diretor do Centro de Estudos Internacionais sobre Governo-CEGOV da UFRGS, Código
SRH 1274, código FG-1, para a qual foi designado pela Portaria 2933/2017, de 04/04/2017, publicada no
Diário Oficial da União do dia 06/04/2017. Processo nº 23078.508802/2018-01.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
