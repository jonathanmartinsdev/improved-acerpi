Documento gerado sob autenticação Nº YAO.694.290.O3T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5399                  de  26/06/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  MARILIA  FORGEARINI  NUNES,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Ensino e Currículo da Faculdade de Educação, com a
finalidade de realizar visita à Universidade da Carolina do Norte, em Chapel Hill, Estados Unidos, no período
compreendido entre 07/07/2019 e 03/08/2019, incluído trânsito, com ônus limitado. Solicitação nº 83850.
RUI VICENTE OPPERMANN
Reitor
