Documento gerado sob autenticação Nº HKX.413.843.NRS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1476                  de  19/02/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°40923,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO EM CONTABILIDADE,
do Quadro de Pessoal desta Universidade, CARLOS ROBERTO DA ROSA JUNIOR (Siape: 2120890 ),  para
substituir   RAUL FERNANDES CRISTINO (Siape: 0356476 ), Coordenador do Núcleo Financeiro da Gerência
Adm do Instituto  de Química,  Código FG-7,  em seu afastamento por  motivo de férias,  no período de
26/02/2018 a 16/03/2018, com o decorrente pagamento das vantagens por 19 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
