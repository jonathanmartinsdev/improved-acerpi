Documento gerado sob autenticação Nº EAH.350.968.DJC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9701                  de  19/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  WASHINGTON  PERES  NUNEZ,  Professor  do  Magistério
Superior,  lotado e em exercício no Departamento de Engenharia Civil  da Escola de Engenharia,  com a
finalidade de ministrar disciplina junto à Universidad San Francisco Xavier de Chuquisaca, em La Paz, Bolívia,
no período compreendido entre 05/11/2017 e 13/11/2017, incluído trânsito, com ônus limitado. Solicitação nº
31920.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
