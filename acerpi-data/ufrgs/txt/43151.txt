Documento gerado sob autenticação Nº ESQ.507.502.K5S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7792                  de  18/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/08/2017,   referente  ao  interstício  de
15/02/2016 a 15/08/2017, para a servidora PAULA PICCOLO DE LEMOS, ocupante do cargo de Engenheiro-
área - 701031, matrícula SIAPE 2156699,  lotada  na  Superintendência de Infraestrutura, passando do Nível
de Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.513234/2017-71:
Formação Integral de Servidores da UFRGS VI CH: 164 Carga horária utilizada: 150 hora(s) / Carga horária
excedente: 14 hora(s) (02/09/2015 a 20/07/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
