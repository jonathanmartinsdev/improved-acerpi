Documento gerado sob autenticação Nº EIA.265.963.QK1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1056                  de  01/02/2017
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL, no uso de suas atribuições, 
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  NISIA  MARTINS  DO  ROSARIO,  matrícula  SIAPE  n°  1853712,  lotada  no
Departamento de Comunicação da Faculdade de Biblioteconomia e  Comunicação,  como Coordenadora
Substituta do PPG em Comunicação e Informação, para substituir automaticamente o titular desta função em
seus afastamentos ou impedimentos regulamentares no período de 13/02/2017 e até 12/02/2019. Processo
nº 23078.025839/2016-28.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no Exercício da Reitoria
