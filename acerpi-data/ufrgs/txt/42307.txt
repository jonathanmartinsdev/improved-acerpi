Documento gerado sob autenticação Nº EQH.527.166.GTC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7162                  de  04/08/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a EUDIRA DA LUZ AMARAL
DA SILVA, matrícula SIAPE nº 0358641, no cargo de Assistente de Laboratório, nível de classificação C, nível
de capacitação IV, padrão 15, do Quadro desta Universidade, no regime de quarenta horas semanais de
trabalho, com exercício no Departamento de Ciências Morfológicas do Instituto de Ciências Básicas da Saúde,
com proventos integrais. Processo 23078.013067/2017-62.
RUI VICENTE OPPERMANN
Reitor.
