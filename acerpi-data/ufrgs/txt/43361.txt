Documento gerado sob autenticação Nº UUS.002.504.BQP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7911                  de  22/08/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 17 de agosto de 2017,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
LAUREN BENTES DE AZEVEDO PRATES, ocupante do cargo de Técnico em Assuntos Educacionais, Ambiente
Organizacional Ciências Humanas, Jurídicas e Econômicas, Código 701079, Classe E, Nível de Capacitação IV,
Padrão de Vencimento 06, SIAPE nº. 1759128 da Faculdade de Ciências Econômicas para a lotação Instituto
de Psicologia, com novo exercício na Comissão de Graduação de Serviço Social.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
