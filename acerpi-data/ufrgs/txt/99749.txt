Documento gerado sob autenticação Nº LLH.460.963.HVL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9456                  de  17/10/2019
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de ANA LUCIA TATSCH, Professor do Magistério Superior, lotada e
em exercício no Departamento de Economia e Relações Internacionais da Faculdade de Ciências Econômicas,
com a finalidade de participar de reunião junto à Agence universitaire de la Francophonie,  em Bogotá,
Colômbia, no período compreendido entre 22/10/2019 e 27/10/2019, incluído trânsito, com ônus limitado.
Solicitação nº 87218.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
