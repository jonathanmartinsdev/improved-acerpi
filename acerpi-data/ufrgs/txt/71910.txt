Documento gerado sob autenticação Nº CEA.440.300.QQO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9022                  de  07/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MICHELE LINDNER, Professor do Magistério Superior, lotada e
em exercício no Departamento Interdisciplinar do Campus Litoral Norte, com a finalidade de participar do "X
Congreso de  la  Asociación Latinoamericana de  Sociología  Rural",  em Montevidéu,  Uruguai,  no  período
compreendido entre 24/11/2018 e 01/12/2018, incluído trânsito, com ônus limitado. Solicitação nº 58452.
RUI VICENTE OPPERMANN
Reitor
