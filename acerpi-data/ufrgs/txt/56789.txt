Documento gerado sob autenticação Nº KXL.188.980.1RL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5425                  de  20/07/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7633, de 29 de
setembro de 2016e tendo em vista o que consta dos Processos Administrativos n° 23078.505280/2018-87 e
23078.502711/2017-72, do contrato nº 063/2017, da Lei 10.520/02 e ainda da Lei 8.666/93,
          RESOLVE:
 
       Aplicar à empresa W.S. COMERCIO DE REFRIGERAÇÃO E EQUIPAMENTOS INDUSTRIAIS LTDA-ME, CNPJ
n.º 13.624.180/0001-24, conforme atestado pela DAL nos documentos 0911843 e 1003170, bem como pelo
NUDECON no documento nº 1099823 do processo 23078. 505280/2018-87, a seguinte sanção administrativa:
 
        MULTA, de 5% (cinco por cento) sobre o valor da parcela que lhe deu causa, no montante de R$ 109,63
(cento e nove reais e sessenta e três centavos), conforme demonstrativo de cálculo documento nº 1101948,
prevista no inciso II, item 03 da cláusula décima primeira do Contrato, pelo atraso na execução do serviço -
manutenções preventivas das câmaras frias dos Restaurantes Universitários.
 
          Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
 
 
LUIS ROBERTO DA SILVA MACEDO
Pró-Reitor de Planejamento e Administração em exercício
