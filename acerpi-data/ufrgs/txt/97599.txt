Documento gerado sob autenticação Nº CUF.137.528.3HN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8009                  de  03/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder o Adicional de Periculosidade, no percentual de 10%, a partir de 01/08/2019, ao servidor
MAICON JADERSON SILVEIRA RAMOS, Identificação Única 30672007, Professor do Magistério Superior, com
exercício no Departamento de Engenharia Elétrica da Escola de Engenharia, observando-se o disposto na Lei
nº 8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer
atividades  em  áreas  consideradas  Perigosas  conforme  Laudo  Pericial  constante  no  Processo  n  º
23078.522062/2019-98, Código SRH n° 24025.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
