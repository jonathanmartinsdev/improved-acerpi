Documento gerado sob autenticação Nº WLM.038.321.3NG, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1729                  de  08/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar  o  afastamento  do  país  de  JAIME URDAPILLETA TAROUCO,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Zootecnia da Faculdade de Agronomia, com a finalidade
de proferir palestra no Bright Science Brighter Living, em Loma Plata, Paraguai, no período compreendido
entre 16/03/2016 e 18/03/2016, incluído trânsito, com ônus limitado. Solicitação nº 18073.
CARLOS ALEXANDRE NETTO
Reitor
