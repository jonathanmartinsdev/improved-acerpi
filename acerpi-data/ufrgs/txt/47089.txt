Documento gerado sob autenticação Nº UOA.950.228.R7N, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10436                  de  13/11/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de RAFAEL ROESLER, Professor do Magistério Superior, lotado no
Departamento de  Farmacologia  do Instituto  de  Ciências  Básicas  da  Saúde e  com exercício  no Parque
Científico e Tecnológico da Universidade Federal do Rio Grande do Sul, com a finalidade de participar de
encontro do Brain Tumour North West, University of Central Lancashire, em Preston, Inglaterra, no período
compreendido entre 11/12/2017 e 16/12/2017, incluído trânsito, com ônus limitado. Solicitação nº 32087.
RUI VICENTE OPPERMANN
Reitor
