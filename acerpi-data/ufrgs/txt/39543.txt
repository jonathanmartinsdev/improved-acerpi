Documento gerado sob autenticação Nº ZPU.506.135.RT7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5208                  de  14/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/06/2017,   referente  ao  interstício  de
10/12/2015 a 11/06/2017, para a servidora ANDREA HELENA RODRIGUES COLOMBO, ocupante do cargo de
Assistente em Administração - 701200,  matrícula SIAPE 2270622,  lotada  no  Instituto de Informática,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  I,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.201598/2017-19:
Formação Integral de Servidores da UFRGS III CH: 65 (24/03/2016 a 28/04/2017)
ILB - Conhecendo o novo acordo ortográfico CH: 20 (03/04/2017 a 23/04/2017)
ILB - Gestão Estratégica com Foco na Administração Pública CH: 40 Carga horária utilizada: 5 hora(s) / Carga
horária excedente: 35 hora(s) (05/04/2017 a 25/04/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
