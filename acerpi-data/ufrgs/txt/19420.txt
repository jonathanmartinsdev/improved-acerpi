Documento gerado sob autenticação Nº KQX.507.901.CV7, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2108                  de  18/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a ROSE MARIA DE OLIVEIRA
PAIM, matrícula SIAPE nº 0355153, no cargo de Professor Titular da Carreira do Magistério do Ensino Básico,
Técnico e Tecnológico, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no
Núcleo  de  Orientação  e  Psicologia  Educacional  do  Colégio  de  Aplicação,  com  proventos  integrais  e
incorporando a vantagem pessoal de que trata a Lei nº 9.624, de 2 de abril  de 1998, que assegurou o
disposto no artigo 3º da Lei nº 8.911, de 11 de julho de 1994. Processo 23078.003858/2016-01.
CARLOS ALEXANDRE NETTO
Reitor
