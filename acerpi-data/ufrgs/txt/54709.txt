Documento gerado sob autenticação Nº AYI.616.051.T8G, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3849                  de  25/05/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  DIOGO  POMPÉU  DE  MORAES,  matrícula  SIAPE  n°  1589233,  lotado  e  em  exercício  no
Departamento de Química Inorgânica do Instituto de Química, da classe C  de Professor Adjunto, nível 03,
para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 13/07/2014 a 12/07/2016, com
vigência financeira a partir de 20/04/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de
2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.507382/2018-37.
JANE FRAGA TUTIKIAN
Vice-Reitora.
