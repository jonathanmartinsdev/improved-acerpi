Documento gerado sob autenticação Nº CHZ.040.481.CKG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             841                  de  24/01/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, ALEXANDRE ALMEIDA DE MAGALHÃES, em virtude de habilitação em
Concurso Público de Provas e Títulos, conforme Edital Nº 01/2018 de 8 de Janeiro de 2018, homologado em
10 de janeiro de 2018 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e
Lei 12.772, de 28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013,
publicada  no  Diário  Oficial  da  União  de  25  de  setembro  de  2013,  para  o  cargo  de  PROFESSOR  DO
MAGISTÉRIO SUPERIOR do Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor
Adjunto A, Nível I,  do Quadro de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação
Exclusiva),  junto ao Departamento de Sociologia do Instituto de Filosofia e Ciências Humanas, em vaga
decorrente da aposentadoria do Professor Jose Vicente Tavares dos Santos, código nº 273234, ocorrida em 3
de Dezembro de 2018, conforme Portaria nº. 9648/2018 de 29 de novembro de 2018, publicada no Diário
Oficial da União de 3 de dezembro de 2018. Processo nº. 23078.507200/2017-47.
RUI VICENTE OPPERMANN
Reitor.
