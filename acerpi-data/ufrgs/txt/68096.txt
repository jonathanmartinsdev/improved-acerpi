Documento gerado sob autenticação Nº XVG.557.050.Q93, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6227                  de  13/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ALYNNI LUIZA RICCO AVILA,  Assistente em Administração,
lotada  no  Instituto  de  Geociências  e  com exercício  na  Secretaria  do  Programa de  Pós-Graduação  em
Geografia, com a finalidade de participar da "8ª Conferencia Latinoamericana y Caribeña de Ciencias Sociales:
Las Luchas por la Igualdad,  la Justicia Social  y  la Democracia en un mundo turbulento",  em Buenos Aires,
Argentina, no período compreendido entre 18/11/2018 e 24/11/2018, incluído trânsito, com ônus UFRGS
(Pró-Reitoria de Pesquisa - diárias). Solicitação nº 34176.
RUI VICENTE OPPERMANN
Reitor
