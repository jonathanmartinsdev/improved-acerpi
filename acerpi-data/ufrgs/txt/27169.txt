Documento gerado sob autenticação Nº QQY.050.462.2CV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6868                  de  05/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  29/08/2016,   referente  ao  interstício  de
18/11/2013  a  28/08/2016,  para  a  servidora  FERNANDA  MARTINS  MARQUES,  ocupante  do  cargo  de
Psicólogo-área - 701060, matrícula SIAPE 1652664,  lotada  no  Colégio de Aplicação, passando do Nível de
Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.019321/2016-55:
Formação Integral de Servidores da UFRGS IV CH: 103 (13/06/2013 a 01/07/2016)
CAPE - O Papel da Escola na Formação de Valores e Limites CH: 25 (29/09/2015 a 24/11/2015)
CAPE - Psicologia Escolar na Educação Fundamental CH: 25 (24/03/2016 a 18/05/2016)
IDC - Atualização em Língua Portuguesa CH: 52 Carga horária utilizada: 27 hora(s) / Carga horária excedente:
25 hora(s) (12/04/2016 a 19/07/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
