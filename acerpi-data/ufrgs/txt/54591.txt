Documento gerado sob autenticação Nº UOX.720.238.NPB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3731                  de  22/05/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Remover, a partir de 21 de maio de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
CLEOMAR KLAUCK, ocupante do cargo de Técnico de Tecnologia da Informação, Ambiente Organizacional
Infraestrutura, Código 701226, Classe D, Nível de Capacitação I, Padrão de Vencimento 01, SIAPE nº. 2416355
do Instituto de Biociências para a lotação Centro de Processamento de Dados, com novo exercício na Divisão
de Serviços de TI.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
