Documento gerado sob autenticação Nº GGD.813.591.POG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5239                  de  21/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal  desta  Universidade,  JULIO OTAVIO JARDIM BARCELLOS,  matrícula  SIAPE n°  1034086,  lotado no
Departamento  de  Zootecnia  da  Faculdade  de  Agronomia,  como  Coordenador  Substituto  do  PPG  em
Zootecnia, com vigência a partir de 10/08/2019 até 09/08/2021, sem prejuízo e cumulativamente com a
função de Vice-Presidente da Câmara de Pesquisa. Processo nº 23078.515982/2019-50.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
