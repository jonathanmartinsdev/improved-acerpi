Documento gerado sob autenticação Nº CXT.920.934.IQM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8045                  de  28/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  7684,  de 03 de outubro de 2016,  do
Magnífico Reitor, e conforme processo SEI nº 23078.511775/2017-64
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, JULIANE ELISA WELKE (Siape: 1790398),  para substituir
ROBERTA CRUZ SILVEIRA THYS (Siape: 2442512 ), Coordenadora da COMGRAD de Engenharia de Alimentos,
Código SRH 1213, FUCC, em sua Licença Gestante, no período de 01/02/2018 a 17/02/2018, com o decorrente
pagamento das vantagens por 17 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
