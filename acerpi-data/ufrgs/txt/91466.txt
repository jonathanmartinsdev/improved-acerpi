Documento gerado sob autenticação Nº DEG.313.584.KG6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4402                  de  21/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora MARCIA MARIA MATTOS
LANGELOH,  ocupante do cargo de Assistente em Administração-701200, lotada na Pró-Reitoria de Pós-
Graduação, SIAPE 0353839, para 52% (cinquenta e dois por cento), a contar de 26/03/2019, tendo em vista a
conclusão do curso de Mestrado em Engenharia de Produção, conforme o Processo nº 23078.507644/2019-
44.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
