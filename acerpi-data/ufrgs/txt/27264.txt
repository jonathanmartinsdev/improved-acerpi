Documento gerado sob autenticação Nº NWB.914.909.4L7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6990                  de  06/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  02/09/2016,   referente  ao  interstício  de
02/03/2015  a  01/09/2016,  para  o  servidor  FRANCISCO  BASTOS  MOREIRA,  ocupante  do  cargo  de
Engenheiro-área -  701031,  matrícula  SIAPE 2055673,   lotado  na   Superintendência  de Infraestrutura,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  E  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  E  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.203112/2016-98:
TCU - Curso Estruturas de Gestão Pública CH: 30 (28/04/2014 a 26/05/2014)
Cursos 24 horas - Curso de Word e Excel CH: 40 (22/01/2015 a 18/02/2015)
TCU  -  Obras  Públicas  de  Edificação  e  de  Saneamento  -  Módulo  Planejamento  CH:  50  (31/08/2015  a
14/10/2015)
QISAT - Curso Básico Hydros V4 "a Distância" CH: 30 (25/05/2016 a 15/06/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
