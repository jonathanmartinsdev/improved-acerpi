Documento gerado sob autenticação Nº ZRO.338.441.QFV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6352                  de  23/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor MARCELO TEODORO DE
ASSIS,  ocupante do cargo de Assistente em Administração-701200, lotado na Pró-Reitoria de Graduação,
SIAPE 2055680, para 30% (trinta por cento), a contar de 31/03/2017, tendo em vista a conclusão do curso de
Especialização em Direito do Estado, conforme o Processo nº 23078.020153/2016-41.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
