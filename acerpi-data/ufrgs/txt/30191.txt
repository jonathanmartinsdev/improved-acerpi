Documento gerado sob autenticação Nº PGU.599.890.QU7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9037                  de  09/11/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ROBERTO GIUGLIANI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de participar de
conferência junto à University College London,  Inglaterra, no período compreendido entre 29/11/2016 e
02/12/2016, incluído trânsito, com ônus limitado. Solicitação nº 24632.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
