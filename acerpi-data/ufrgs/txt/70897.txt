Documento gerado sob autenticação Nº JSC.500.604.60Q, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8238                  de  15/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  13/10/2018,   referente  ao  interstício  de
13/04/2017 a 12/10/2018, para a servidora ALINE HORN PITTIGLIANI, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2258296,  lotada  na  Faculdade de Agronomia, passando do Nível
de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.527676/2018-85:
Formação Integral de Servidores da UFRGS IV CH: 109 Carga horária utilizada: 105 hora(s) / Carga horária
excedente: 4 hora(s) (30/03/2017 a 09/04/2018)
ENAP - Sistema Eletrônico de Informações - SEI CH: 15 (24/01/2017 a 13/02/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
