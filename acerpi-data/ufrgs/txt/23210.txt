Documento gerado sob autenticação Nº AFZ.369.896.0M0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4296                  de  13/06/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso das atribuições que lhe
foram conferidas pelo Decreto nº 48.598, de 23 de julho de 1960, e tendo em vista o que consta no Processo
nº 23078.012459/2016-23,
RESOLVE
           Retificar, por decisão judicial proferida no processo nº 5027039-26.2010.404.7100, da 2ª Vara Federal
de Porto Alegre, o enquadramento no PCCTAE, homologado pela Portaria n° 1874, de 13/07/06, do servidor
ADEMAR MARTINS DE JESUS, matrícula SIAPE nº 357494, aposentado no cargo de Técnico em Eletricidade,
para o Nível de Capacitação III, com efeitos financeiros a partir de 1º de maio de 2016.
CARLOS ALEXANDRE NETTO
Reitor
