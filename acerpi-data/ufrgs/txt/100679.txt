Documento gerado sob autenticação Nº PZD.835.553.8N8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10083                  de  07/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor ALEX KIPPER, ocupante do cargo de  Engenheiro-área - 701031, lotado na
Escola de Engenharia, SIAPE 1094982, o percentual de 52% (cinquenta e dois por cento) de Incentivo à
Qualificação, a contar de 14/10/2019, tendo em vista a conclusão do curso de Mestrado em Engenharia Civil,
conforme o Processo nº 23078.528115/2019-84.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
