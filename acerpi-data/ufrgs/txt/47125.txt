Documento gerado sob autenticação Nº ZBQ.803.594.SA8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10456                  de  14/11/2017
A VICE-RETIROA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, e tendo em vista o que consta na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar o afastamento no país de SIMONE MAINIERI PAULON, ocupante do cargo de Professor
do Magistério Superior,   lotada e em exercício no Departamento de Psicologia Social  e Institucional do
Instituto de Psicologia,  com a finalidade de participar de congressos junto à Associação Nacional de Pesquisa
e Pós-Graduação em Psicologia, em Fortaleza, com ônus CAPES/PROAP; na Universidade Federal de Sergipe,
em Aracaju; na Universidade Federal de Minas Gerais, em Belo Horizonte e na Universidade Estadual de
Campinas, em Campinas, Brasil, no período compreendido entre 15/11/2017 e 29/11/2017, incluído trânsito,
com ônus limitado. Solicitação n° 32867.
JANE FRAGA TUTIKIAN,
Vice-Reitora.
