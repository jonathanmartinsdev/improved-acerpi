Documento gerado sob autenticação Nº ZMG.889.599.IK0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9619                  de  29/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  19/11/2018,   referente  ao  interstício  de
08/12/2009 a 18/11/2018, para o servidor VALTER VELASQUES ALVES, ocupante do cargo de Motorista -
701445,  matrícula  SIAPE  0356779,   lotado   na   Faculdade  de  Veterinária,  passando  do  Nível  de
Classificação/Nível de Capacitação C II, para o Nível de Classificação/Nível de Capacitação C III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.531969/2018-67:
Formação Integral de Servidores da UFRGS III CH: 60 (25/03/2010 a 07/11/2018)
FAVET - XIX Semana Acadêmica da Faculdade de Veterinária CH: 30 (15/10/2018 a 19/10/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
