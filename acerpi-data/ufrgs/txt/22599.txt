Documento gerado sob autenticação Nº XZG.028.832.TOS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3813                  de  24/05/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LAVINIA SCHULER FACCINI, Professor do Magistério Superior,
lotada no Departamento de Genética do Instituto de Biociências e com exercício na Vice-Direção do Instituto
de Biociências, com a finalidade de participar da "27th Conference of the European Network of Teratology
Information Services",  em Berlim, Alemanha, no período compreendido entre 31/05/2016 e 07/06/2016,
incluído trânsito, com ônus limitado. Solicitação nº 20234.
CARLOS ALEXANDRE NETTO
Reitor
