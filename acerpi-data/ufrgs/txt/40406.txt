Documento gerado sob autenticação Nº CKS.285.567.AP3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5827                  de  04/07/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Tornar insubsistente a Portaria nº 4901/2017, de 01/06/2017, publicada no Diário Oficial da União
de 05/06/2017, que concedeu autorização para afastamento do País a ALEXANDRE BELUCO, ocupante do
cargo de Professor do Magistério Superior, lotado e em exercício no Departamento de Hidromecânica e
Hidrologia do Instituto de Pesquisas Hidráulicas. Solicitação nº 28374.
RUI VICENTE OPPERMANN
Reitor.
