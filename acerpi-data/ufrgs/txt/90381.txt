Documento gerado sob autenticação Nº AHQ.565.512.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3649                  de  29/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de TIAGO ELIAS ROSITO, Professor do Magistério Superior, lotado
e em exercício no Departamento de Cirurgia da Faculdade de Medicina, com a finalidade de participar de
simpósio e realizar visita à The University of Chicago, em Chicago, Estados Unidos, no período compreendido
entre 29/04/2019 e 04/05/2019, incluído trânsito, com ônus limitado. Solicitação nº 83721.
RUI VICENTE OPPERMANN
Reitor
