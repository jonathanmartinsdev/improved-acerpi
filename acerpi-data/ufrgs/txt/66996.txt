Documento gerado sob autenticação Nº SAA.128.441.A3E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5549                  de  26/07/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  ANGELICA  ROSAT  CONSIGLIO,  matrícula  SIAPE  n°  1223094,  lotada  e  em  exercício  no
Departamento de Biofísica do Instituto de Biociências, da classe D  de Professor Associado, nível 04, para a
classe E  de Professor Titular,  referente ao interstício de 23/04/2015 a 21/06/2018, com vigência financeira a
partir de 22/06/2018, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 232/2014 - CONSUN. Processo nº 23078.516066/2018-56.
DANILO BLANK
Decano do Conselho Universitário, no exercício da Reitoria.
