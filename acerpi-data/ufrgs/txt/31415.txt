Documento gerado sob autenticação Nº AMS.273.670.T7D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9931                  de  15/12/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal desta Universidade, AGNES OLSCHOWSKY, matrícula SIAPE nº 6356803, lotada no Departamento de
Assistência e Orientação Profissional da Escola de Enfermagem, para exercer a função de Vice-Diretora da
Escola de Enfermagem, Código SRH 175, código FG-1, com vigência a partir de 02/01/2017 e até 01/01/2021,
conforme o disposto nos artigos 5º e 6º do Decreto nº 1.916, de 23 de maio de 1996, que regulamenta a Lei
nº 9.192, de 21 de dezembro de 1995. Processo n° 23078.024907/2016-31.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
