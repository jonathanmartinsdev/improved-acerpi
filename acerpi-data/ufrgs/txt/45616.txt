Documento gerado sob autenticação Nº CQG.949.809.9QG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9422                  de  09/10/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, FABRÍCIO FIGUEIRÓ, em virtude de habilitação em Concurso Público de
Provas e Títulos, conforme Edital Nº 16/2017 de 24 de fevereiro de 2017, homologado em 02 de março de
2017, de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de 28
de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do
Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro
de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento
de  Bioquímica  do  Instituto  de  Ciências  Básicas  da  Saúde,  em  vaga  decorrente  da  aposentadoria  da
Professora Ana Maria Oliveira Battastini, código nº 277134, ocorrida em 15 de Setembro de 2017, conforme
Portaria nº. 8641/2017 de 14 de setembro de 2017, publicada no Diário Oficial da União de 15 de setembro
de 2017. Processo nº. 23078.507289/2016-61.
RUI VICENTE OPPERMANN
Reitor.
