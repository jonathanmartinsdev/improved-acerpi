Documento gerado sob autenticação Nº UAG.244.864.EAV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1623                  de  20/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°42512,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, ANGELO RONALDO PEREIRA DA SILVA (Siape:
0351152  ),   para  substituir    ANA  LUCIA  DOS  SANTOS  BRUM  STRIEDER  (Siape:  0357249  ),  Gerente
Administrativo do Instituto de Geociências, Código FG-1, em seu afastamento por motivo de Laudo Médico
do titular da Função, no período de 16/01/2017 a 20/01/2017, com o decorrente pagamento das vantagens
por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
