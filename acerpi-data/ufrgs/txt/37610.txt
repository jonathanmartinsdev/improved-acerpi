Documento gerado sob autenticação Nº AXS.957.236.UAC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3895                  de  05/05/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7612, de 29 de setembro de 2016
RESOLVE
Tornar insubsistente a Portaria nº 3542/2017, de 27/04/2017  que nomeou os candidatos ANGELICA
MENIN para o cargo de Técnico de Laboratório/Área: Biologia e GUILHERME ROTTH ZIBETTI para o cargo de
Técnico  de  Tecnologia  da  Informação/Área:  Infraestrutura,  conforme justificativa  anexada  ao  processo.
Processo nº 23078. 014002/2015-72.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas.
