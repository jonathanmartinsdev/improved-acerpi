Documento gerado sob autenticação Nº LMN.543.705.MD1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8338                  de  18/10/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar a prorrogação de afastamento do País de SARA COPETTI KLOHN, Professor do Magistério
Superior,  lotada   e  com exercício  no Departamento de  Design e  Expressão Gráfica  da  Faculdade de
Arquitetura, com a finalidade de continuar estudos em nível  de Doutorado, junto à University of Reading -
Inglaterra,  no  período  compreendido  entre  22/11/2016  e  31/08/2017,  com  ônus  CAPES.  Processo  nº
23078.006232/2014-87.
RUI VICENTE OPPERMANN
Reitor
