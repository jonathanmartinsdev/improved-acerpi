Documento gerado sob autenticação Nº ENS.187.660.398, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1637                  de  27/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar  a  Portaria  nº  685/2018,  de  24/01/2018,  que  designou,  temporariamente,  GABRIELA
RABELLO DE LIMA (Siape: 2333458) para substituir  JESSICA FROEHLICH FLORES (Siape: 2053484 ), Processo
SEI nº 23078.503206/2018-26
onde se lê:
"... em seu afastamento por motivo de férias, no período de 29/01/2018 a 09/02/2018...";
          leia-se:
"... em seu afastamento por motivo de férias, no período de 05/01/2018 a 16/02/2018...", ficando
ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
