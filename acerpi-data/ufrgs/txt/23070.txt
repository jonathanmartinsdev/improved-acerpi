Documento gerado sob autenticação Nº LLA.241.851.T9Q, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4154                  de  08/06/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Lotar na Pró-Reitoria de Assuntos Estudantis, com exercício na Gerência Administrativa da PRAE,
Isabel de Abrantes Timm, nomeada conforme Portaria Nº 3392 de 09 de maio de 2016, publicada no Diário
Oficial da União no dia 11 de maio de 2016, em efetivo exercício desde 07 de junho de 2016, ocupante do
cargo de Assistente em Administração, Ambiente Organizacional Administrativo, classe D, nível I, padrão I, no
Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
