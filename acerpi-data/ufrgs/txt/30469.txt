Documento gerado sob autenticação Nº HMS.306.756.MP1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9150                  de  17/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder pensão com natureza vitalícia, com efeitos financeiros a partir de 01 de agosto de 2016,
a MARIA CRISTINA KREIN, nos termos dos artigos 215 e 217, inciso I, alínea 'c', da Lei nº 8.112, de 11 de
dezembro  de  1990,  combinado  com  artigo  40  da  Constituição  Federal/88,  alterada  pela  Emenda
Constitucional 41/03, regulamentada pela Lei nº 10.887/04, artigo 2º, inciso II, em decorrência do falecimento
de CESAR AUGUSTO BARROS, matrícula SIAPE n° 0358020, ativo no cargo de Auxiliar de Eletricista do quadro
de pessoal desta Universidade. Processo n.º 23078.007741/2014-27.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
