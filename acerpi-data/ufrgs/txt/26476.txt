Documento gerado sob autenticação Nº ZDB.543.448.KI3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6424                  de  25/08/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear o(a) ocupante do cargo de Professor do Magistério Superior do Quadro de Pessoal desta
Universidade, CLARICE SALETE TRAVERSINI, matrícula SIAPE 1496442, para exercer o cargo de Diretora do
Depto de Cursos e Políticas da Graduação da PROGRAD, Código SRH 918, com vigência a partir da data de
publicação no Diário Oficial da União. Processo nº 23078.018459/2016-37.
CARLOS ALEXANDRE NETTO
Reitor
