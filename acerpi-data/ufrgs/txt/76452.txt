Documento gerado sob autenticação Nº XMN.580.378.HSA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1387                  de  07/02/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 5658, de 29 de junho de 2017
RESOLVE
Designar  PABLO  GUILHERME  SILVEIRA,  Matrícula  SIAPE  2259888,  ocupante  do  cargo  de
Engenheiro-área,  Código 701031,  do Quadro de Pessoal  desta  Universidade,  para exercer  a  função de
Coordenador do Núcleo Técnico-Científico do Campus Litoral Norte, código SRH 1418, código FG-2, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.502698/2019-13.
DANILO BLANK
Decano do Conselho Universitário, no exercício da Reitoria.
