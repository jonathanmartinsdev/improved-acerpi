Documento gerado sob autenticação Nº CEV.702.334.QQO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9027                  de  07/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de RAPHAEL MARTINS BRUM, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia Elétrica da Escola de Engenharia, com a finalidade de
realizar  visita à Ecole Nationale Supérieure d'Ingénieurs de Caen, França, no período compreendido entre
30/11/2018 e 16/12/2018,  incluído trânsito,  com ônus CAPES (Edital  CAPES BRAFITEC nº  24/2016 sob o
número 220/17). Solicitação nº 60821.
RUI VICENTE OPPERMANN
Reitor
