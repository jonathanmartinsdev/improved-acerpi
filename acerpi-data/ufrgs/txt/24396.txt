Documento gerado sob autenticação Nº SSO.029.199.23A, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5095                  de  12/07/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ROCHELE DE QUADROS LOGUERCIO,  matrícula SIAPE n° 2418539, lotada e em exercício no
Departamento de Química Inorgânica do Instituto de Química, da classe D  de Professor Associado, nível 01,
para a classe D  de Professor Associado, nível 02, referente ao interstício de 05/03/2014 a 04/03/2016, com
vigência financeira a partir de 11/07/2016, de acordo com o que dispõe a Decisão nº 197/2006-CONSUN,
alterada pela Decisão nº 401/2013-CONSUN. Processo nº 23078.503724/2016-88.
RUI VICENTE OPPERMANN
Vice-Reitor
