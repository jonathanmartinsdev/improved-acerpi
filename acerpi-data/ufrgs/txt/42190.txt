Documento gerado sob autenticação Nº MEQ.458.948.M0L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7073                  de  03/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°27551,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, FLAVIA CRISTIANE FARINA (Siape: 2489920 ),  para
substituir    ANDREA  RITTER  JELINEK  (Siape:  2174454  ),  Chefe  do  Depto  de  Geodésia  do  Instituto  de
Geociências, Código FG-1, em seu afastamento do país, no período de 23/07/2017 a 29/07/2017, com o
decorrente pagamento das vantagens por 7 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
