Documento gerado sob autenticação Nº KCR.615.386.3I4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5656                  de  27/07/2016
O DECANO DO CONSELHO UNIVERISTÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Designar VALENCIA CRISTINA MEIER, Matrícula SIAPE 2324135, ocupante do cargo de Assistente
em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de
Secretária de Pós-Graduação da Faculdade de Farmácia, Código SRH 585, código FG-7, com vigência a partir
da data de publicação no Diário Oficial da União. Processo nº 23078.013151/2016-03.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no Exercício da Reitoria
