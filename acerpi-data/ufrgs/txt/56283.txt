Documento gerado sob autenticação Nº ALK.540.501.Q1E, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5156                  de  13/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  11/07/2018,   referente  ao  interstício  de
01/03/2005 a 10/07/2018, para o servidor SERGIO LUIZ FIDELIS PACHECO, ocupante do cargo de Apontador
- 701602, matrícula SIAPE 0358000,  lotado  na  Pró-Reitoria de Planejamento e Administração, passando do
Nível de Classificação/Nível de Capacitação B I, para o Nível de Classificação/Nível de Capacitação B II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.517343/2018-48:
ILB - Ética e Administração Pública CH: 40 (03/06/2018 a 10/07/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
