Documento gerado sob autenticação Nº VUF.998.717.GL6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10745                  de  24/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar, a partir da data de publicação no Diário Oficial da União, o ocupante do cargo de Técnico
em Mecânica - 701245, do Nível de Classificação DII, do Quadro de Pessoal desta Universidade, FELIPE ROSA,
matrícula SIAPE 2258891 da função de Diretor da Divisão Técnico-Científica da Diretoria  Acadêmica do
Campus Litoral Norte, Código SRH 1425, Código FG-3, para a qual foi designado pela Portaria nº 8433/2017
de 08/09/2017, publicada no Diário Oficial da União de 12/09/2017, por ter sido designado para outra função
gratificada. Processo nº 23078.520255/2017-42.
RUI VICENTE OPPERMANN
Reitor.
