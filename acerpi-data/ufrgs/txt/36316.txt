Documento gerado sob autenticação Nº TGX.708.951.EGI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/3
PORTARIA Nº             2805                  de  30/03/2017
O PRÓ-REITOR DE GRADUAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de
suas atribuições, considerando o disposto na Portaria nº 7626, de 29 de setembro de 2016
  R E S O L V E:
 
Publicar as Datas Acadêmicas para o segundo semestre de 2017.
10/04/2017 ALTERAÇÃO CURRICULAR: Data limite para recebimento, pela Câmara deGraduação, de processos advindos das COMGRADs para o Período Letivo de 2017/2.
17/04/2017 às 8h até
26/05/2017 às 23h59
DEFINIÇÃO DOS PLANOS DE ENSINO: Período para os docentes definirem as
alterações ou incluírem novos Planos de Ensino com vistas ao semestre 2017/2.
17/04/2017 às 8h até
09/06/2017 às 23h59
HOMOLOGAÇÃO DOS PLANOS DE ENSINO: Período para as COMGRADs
homologarem as alterações ou novos Planos de Ensino com vistas ao semestre
2017/2.
17/04/2017 às 8h até
19/05/2017 às 23h59
SOLICITAÇÃO DE VAGAS AOS DEPARTAMENTOS: Período para as COMGRADs
enviarem seus pedidos de vagas aos Departamentos com vistas ao semestre
2017/2.
17/04/2017 às 8h até
19/06/2017 às 23h59
OFERECIMENTO DE ATIVIDADES DE ENSINO: Período para Oferecimento de
Disciplinas/Turmas, com definição de seus espaços físicos, para as atividades do
semestre 2017/2.
02/05/2017 a
10/06/2017
RECEBIMENTO de solicitação de matrícula de mobilidade acadêmica nacional de
discentes de outras IFEs e visitantes para 2017/2
30/05/2017 às 8h a
19/06/2017 às 23h59
SOLICITAÇÃO DE VAGAS DE MONITORIA: período para as COMGRADs e
Departamentos encaminharem as suas solicitações de vaga para monitoria para o
semestre 2017/2.
Até 20/06/2017
DIVULGAÇÃO DE FAIXA HORÁRIA da Matrícula dos Calouros de 2017/2:
Divulgação no site da UFRGS, www.ufrgs.br, da faixa horária da matrícula presencial
dos calouros classificados no Concurso Vestibular e no SiSU 2017, para ingresso no
2º Período Letivo de 2017. 
21/06/2017 às 8h a
26/06/2017 às 23h59
AJUSTE DO OFERECIMENTO: Período para as correções de oferecimento que se
fizerem necessárias para o semestre 2017/2.
27/06/2017 às 8h a
30/06/2017 às 23h59
RESERVA DE VAGAS PARA CALOUROS: Período para a COMGRAD reservar, no
Portal da COMGRAD, as vagas destinadas aos calouros.
04/07/2017 a
06/07/2017
MATRÍCULA PRESENCIAL DOS CALOUROS, candidatos classificados do Concurso
Vestibular e no SiSU 2017 com ingresso no 2º Período Letivo de 2017.
Documento gerado sob autenticação Nº TGX.708.951.EGI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/3
07/07/2017às 23h59 DATA LIMITE para os gerentes de Espaço Físico atenderem ou negarem ospedidos de alocação de espaço físico.
08/07/2017 às 8h a
14/07/2017 às 18h
Período para envio da relação das turmas sem espaço físico para veteranos e
calouros à PROGRAD pelos Departamentos e COMGRADs. Estas turmas serão
realocadas em escolas externas à UFRGS.
Enviar as solicitações para: moduloturmas@prograd.ufrgs.br
10/08/2017 a partir das
4h a 14/08/2017 até as
16h
ENCOMENDA DE MATRÍCULA: Período para os alunos veteranos, aptos à matrícula
do 2º Período Letivo de 2017, solicitarem disciplinas, via Portal do Aluno.
15/08/2017 a partir das
10h a 17/08/2017 até
as 16h
AMPLIAÇÃO DE VAGAS: Período para os Departamentos ampliarem vagas,
cancelarem ou incluírem novas disciplinas/turmas para as demandas solicitadas na
Encomenda de Matrícula.
18/08/2017 a partir das
4h
DIVULGAÇÃO DO RESULTADO DA ENCOMENDA DE MATRÍCULA: Divulgação dos
resultados da matrícula pela Internet.
18/08/2017 a partir das
12h até 21/08/2017 às
16h
AJUSTE DE MATRÍCULA: Período para solicitações de inclusão de matrícula de novas
disciplinas/turmas, substituições ou, ainda, ajustes no resultado da Encomenda (1ª
fase) para o 2º Período Letivo de 2017, via Portal do Aluno.
18/08/2017 a partir das
12h até 21/08/2017 às
16h
EXCLUSÃO DE MATRÍCULA: Data limite para efetuar Exclusão de Matrícula nas
disciplinas do 2º Período Letivo de 2017 via Portal do Aluno, sem prejuízo no
Ordenamento de Matrícula.
22/08/2017 a partir das
08h até 24/08/2017 às
16h.
AMPLIAÇÃO DE VAGAS: Período para os Departamentos e COMGRADs que ofertam
disciplinas ampliarem vagas, cancelarem ou incluírem novas disciplinas/turmas para
as demandas solicitadas no Ajuste.
23/08/2017
DATA LIMITE PARA MANUTENÇÃO DAS VAGAS RESERVADAS PARA CALOUROS
não utilizadas na matrícula presencial. Data para as Comissões de Graduação
liberarem as vagas não utilizadas na matrícula presencial de calouros para
veteranos.
23/08/2017 às 12h Resultado dos pedidos de alocação das turmas de veteranos e calouros semespaço físico enviados à PROGRAD
25/08/2017 às 4h DIVULGAÇÃO DO RESULTADO FINAL DA MATRÍCULA: Divulgação dos resultadosfinais da matrícula pela Internet.
28/08/2017 INÍCIO DAS AULAS do 2º Período Letivo de 2017
28/08/2017 INÍCIO DAS ATIVIDADES DE MONITORIA em 2017/2.
28/08/2017 a
03/12/2017
SOLICITAÇÃO DE COLAÇÃO DE GRAU: período para os alunos formandos em 2017/2
solicitarem colação de grau pelo Portal do Aluno.
04 a 06/09/2017 ALUNO ESPECIAL: Período para requerer matrícula como aluno especial, com vistasao 2º Período Letivo de 2017, através do site www.prograd.ufrgs.br
10/09/2017 CANCELAMENTO DE MATRÍCULA: Data limite para efetuar Cancelamento deMatrícula das disciplinas do 2º Período Letivo de 2017.
11/09/2017 FLEXIBILIZAÇÃO de pré-requisito: prazo máximo para concessão de flexibilização depré-requisito com vistas ao semestre 2017/2
11/09/2017
ALTERAÇÃO CURRICULAR: Data limite para recebimento, pela Câmara de
Graduação, de processos advindos das Comissões de Graduação para o 1º Período
Letivo de 2018.
01/10/2017
TRANCAMENTO SEM MATRÍCULA: Data limite para efetuar o Trancamento de
Matrícula ou Renovação de Trancamento de Matrícula, no Período 2017/2, para
alunos aptos à matrícula no semestre e que não a tenham efetuado no prazo hábil.
 16 a 20/10/2017 SEMANA ACADÊMICA/SALÃO UFRGS (Portaria 195 de 06/01/2017 do CEPE).
01/11/2017 a
29/01/2018
TROCA DE CURRÍCULO ou HABILITAÇÃO e FDC: Data limite para solicitações de
Troca de Currículo ou Habilitação.
20/11/2017 a
05/03/2018
AVALIAÇÃO DOCENTE 2017/2: Período para Avaliação Docente pelos Discentes,
através do Portal do Aluno (Portaria 8394 de 29/10/2015).
Documento gerado sob autenticação Nº TGX.708.951.EGI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
3/3
03/12/2017
TRANCAMENTO DE MATRÍCULA: Data limite para efetuar Trancamento de
Matrícula, no semestre corrente, para alunos regularmente matriculados em
2017/2.
15/12/2017 a
30/01/2018 às 16h PERMANÊNCIA: período para solicitações de Permanência no Curso.
23/12/2017 a
01/01/2018 RECESSO - período não letivo.
06/01/2018 a
10/01/2018 CONCURSO VESTIBULAR - período não letivo.
27/01/2018 TÉRMINO DAS AULAS do 2º Período Letivo de 2017.
Até 29/01/2018 APROPRIAÇÃO DOS CONCEITOS: Data limite para apropriação dos conceitos pelosdocentes.
30/01/2018 Divulgação dos CONCEITOS FINAIS do segundo período letivo de 2017.
31/01/2018 ANÁLISE DO PEDIDO DE PERMANÊNCIA: Prazo limite para COMGRAD analisar ospedidos de permanência.
Até 31/01/2018
AFASTAMENTO POR DIPLOMAÇÃO: Período aconselhável para as COMGRADs
efetuarem o afastamento por diplomação dos alunos aptos à colação de grau em
2017/2.
05/02/2018 a
15/04/2018 Período destinado às COLAÇÕES DE GRAU do 2º Período Letivo de 2017.
 
OBSERVAÇÕES:
 
1) A reunião de planejamento do calendário de matrículas de 2018 será realizada em agosto de 2017.
2) As datas específicas referentes às monitorias acadêmicas serão divulgadas em calendário próprio.
 
Porto Alegre, 29 de março de 2017.
VLADIMIR PINHEIRO DO NASCIMENTO
Pró-Reitor de Graduação
