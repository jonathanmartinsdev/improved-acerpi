Documento gerado sob autenticação Nº DHY.596.085.K69, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9891                  de  01/11/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  24/10/2019,   referente  ao  interstício  de
27/04/2018 a 23/10/2019, para o servidor CARLOS ALEXANDRE WENDEL, ocupante do cargo de Técnico em
Assuntos Educacionais - 701079, matrícula SIAPE 1139896,  lotado  no  Campus Litoral Norte, passando do
Nível de Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.529416/2019-25:
Formação Integral de Servidores da UFRGS I CH: 20 (29/05/2017 a 24/09/2019)
ILB - Política Contemporânea CH: 60 Carga horária utilizada: 50 hora(s) / Carga horária excedente: 10 hora(s)
(01/07/2019 a 19/08/2019)
ENAP - Gestão Pessoal - Base da Liderança CH: 50 (01/07/2019 a 21/08/2019)
UERGS - Educação Musical I CH: 30 (08/03/2018 a 12/07/2018)
UERGS - Educação Musical II CH: 30 (16/08/2018 a 13/12/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
