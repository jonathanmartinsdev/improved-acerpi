Documento gerado sob autenticação Nº QSX.274.731.ACK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7234                  de  13/09/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de FERNANDO NEVES HUGO, Professor do Magistério Superior,
lotado no Departamento de Odontologia Preventiva e Social da Faculdade de Odontologia e com exercício no
Centro de Pesquisa em Odontologia Social, com a finalidade de proferir palestra na Asociación Peruana de
Odontologia Preventiva y Social, em Lima, Peru, no período compreendido entre 13/10/2016 e 24/10/2016,
incluído trânsito, com ônus limitado. Solicitação nº 22557.
CARLOS ALEXANDRE NETTO
Reitor
