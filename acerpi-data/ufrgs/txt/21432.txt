Documento gerado sob autenticação Nº NTC.967.105.N68, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3264                  de  03/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  20/04/2016,   referente  ao  interstício  de
20/10/2014 a 19/04/2016, para a servidora SIMONE NODARI GIANOTTI, ocupante do cargo de Assistente
em Administração - 701200,  matrícula SIAPE 1895483,  lotada  na  Pró-Reitoria de Gestão de Pessoas,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.007136/2016-18:
SOF - Orçamento Público CH: 70 (14/04/2015 a 18/05/2015)
ESAF - Cidadania Fiscal CH: 20 (02/02/2015 a 01/03/2015)
ESAF - Escola de Administração Fazendária -  Educação Financeira - Gestão de Finanças Pessoais CH: 20
(23/09/2014 a 23/10/2014)
UFRGS - XXXV Encontro Nacional de Dirigentes de Pessoal e Recursos Humanos das Instituições Federais de
Ensino - ENDP CH: 20 (01/09/2015 a 04/09/2015)
SOF - ENAP - Ética e serviço Público CH: 20 (03/02/2015 a 01/03/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
