Documento gerado sob autenticação Nº JZV.104.001.B02, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10524                  de  17/11/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  DENISE  CARPENA COITINHO DAL MOLIN,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Engenharia Civil da Escola de Engenharia,
com a finalidade de ministrar a disciplina "Topicos Avanzados de Materiales",  na Universidad Nacional de
Asunción, Paraguai, no período compreendido entre 26/11/2017 e 02/12/2017, incluído trânsito, com ônus
limitado. Solicitação nº 32927.
RUI VICENTE OPPERMANN
Reitor
