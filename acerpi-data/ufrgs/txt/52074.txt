Documento gerado sob autenticação Nº VBY.594.847.J72, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2098                  de  20/03/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de ROBERTO IANNUZZI, Professor do Magistério Superior, lotado e
em exercício no Departamento de Paleontologia e Estratigrafia do Instituto de Geociências, com a finalidade
de  participar  do  "VII  Simposio  Argentino  del  Paleozoico  Superior",  em  Esquel,  Argentina,  no  período
compreendido entre 24/03/2018 e 30/03/2018, incluído trânsito, com ônus CNPq (bolsa de pesquisador).
Solicitação nº 34248.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
