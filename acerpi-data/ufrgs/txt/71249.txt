Documento gerado sob autenticação Nº HMF.312.634.302, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8507                  de  23/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, ALBERTO MARINHO RIBAS SEMELER (Siape: 2193716),
 para substituir PAULO ANTONIO DE MENEZES PEREIRA DA SILVEIRA (Siape: 6354734 ),  Coordenador da
Comissão de Extensão do Instituto de Artes, Código SRH 719, em seu afastamento do país, no período de 23
a 28/10/2018.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
