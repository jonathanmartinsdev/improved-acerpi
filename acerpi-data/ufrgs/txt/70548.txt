Documento gerado sob autenticação Nº MBR.501.321.F13, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7985                  de  05/10/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  ROSANE GOMEZ,  matrícula  SIAPE  n°  1228937,  lotada  e  em exercício  no  Departamento  de
Farmacologia do Instituto de Ciências Básicas da Saúde, da classe C  de Professor Adjunto, nível 04, para a
classe D  de Professor Associado, nível 01, referente ao interstício de 05/08/2016 a 04/08/2018, com vigência
financeira a partir de 15/08/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.519386/2018-68.
RUI VICENTE OPPERMANN
Reitor.
