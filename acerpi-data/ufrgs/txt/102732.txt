Documento gerado sob autenticação Nº ETI.769.223.LA0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11422                  de  23/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora JAQUELINE TROMBIN,
ocupante do cargo de Bibliotecário-documentalista-701010, lotada na Editora da UFRGS, SIAPE 0358612, para
52% (cinquenta e dois por cento), a contar de 18/12/2019, tendo em vista a conclusão do curso de Mestrado
em Memória Social e Bens Culturais, conforme o Processo nº 23078.535023/2019-51.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
