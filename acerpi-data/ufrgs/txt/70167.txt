Documento gerado sob autenticação Nº JTK.360.965.Q9P, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7697                  de  26/09/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder o Adicional de Periculosidade, no percentual de 10%, a partir de 05/02/2018, ao servidor
DIORGE ALEX BÁO ZAMBRA, Identificação Única 30111668, Professor do Magistério Superior, com exercício
no Departamento Interdisciplinar do Campus Litoral Norte, observando-se o disposto na Lei nº 8.112, de 11
de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em
áreas consideradas Perigosas conforme Laudo Pericial constante no Processo n º 23078.521231/2018-91,
Código SRH n° 23632.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
