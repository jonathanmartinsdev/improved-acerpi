Documento gerado sob autenticação Nº YSU.909.577.5UO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9428                  de  24/11/2016
O SUPERINTENDENTE DE INFRAESTRTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7687, de 03 de outubro de 2016
 
RECONDUZIR a Comissão designada pela Portaria nº 8526 de 21/10/2016, a fim de que no
prazo de 30 (trinta) dias sejam complementados os trabalhos de elaboração de Laudo Técnico
Circunstanciado referente à obra de Construção do Prédio de Acolhimento de Programas de Pós-
Graduação Multidisciplinares, processo UFRGS 23078.200060/10-12.
EDY ISAIAS JUNIOR
Superintendente de Infraestrtura
