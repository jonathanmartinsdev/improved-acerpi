Documento gerado sob autenticação Nº MAY.200.195.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8705                  de  25/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de JOCELITO ZALLA, Professor do Magistério do Ensino Básico,
Técnico e Tecnológico, lotado no Colégio de Aplicação e com exercício no Departamento de Humanidades,
com  a  finalidade  de  participar  das  "XVII  Jornadas  Interescuelas/Departamentos  de  Historia",  em  San
Fernando del Valle de Catamarca, Argentina,  no período compreendido entre 01/10/2019 e 06/10/2019,
incluído trânsito, com ônus limitado. Solicitação nº 87114.
RUI VICENTE OPPERMANN
Reitor
