Documento gerado sob autenticação Nº SRL.219.336.5A0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5933                  de  07/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  GUSTAVO  JAVIER  ZANI  NUNEZ,  matrícula  SIAPE  n°  2612583,  lotado  e  em  exercício  no
Departamento de Design e Expressão Gráfica da Faculdade de Arquitetura, da classe C  de Professor Adjunto,
nível 03, para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 21/12/2014 a 01/02/2018,
com vigência financeira a partir de 10/04/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro
de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.506366/2018-27.
JANE FRAGA TUTIKIAN
Vice-Reitora.
