Documento gerado sob autenticação Nº PXS.309.135.RO6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4354                  de  21/05/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°72886,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de DIAGRAMADOR, do Quadro de
Pessoal desta Universidade, EDINA MARIA DA ROCHA FERREIRA (Siape: 0352039 ),  para substituir   ANDRE
IRIBURE RODRIGUES (Siape: 3347107 ), Secretário de Comunicação Social, Código CD-3, em seu afastamento
no país, no período de 21/05/2019 a 25/05/2019, com o decorrente pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
