Documento gerado sob autenticação Nº PXY.847.156.M49, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6559                  de  26/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ALEXANDRE FAVERO BULGARELLI, Professor do Magistério
Superior,  lotado e em exercício no Departamento de Odontologia Preventiva e Social  da Faculdade de
Odontologia,  com  a  finalidade  de  participar  do  "21st   European  Association  of  Dental  Public  Health
Congress",  em  Budapest,  Hungria,  no  período  compreendido  entre  28/09/2016  e  03/10/2016,  incluído
trânsito, com ônus limitado. Solicitação nº 21106.
CARLOS ALEXANDRE NETTO
Reitor
