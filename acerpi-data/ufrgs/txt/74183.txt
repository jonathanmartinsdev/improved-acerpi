Documento gerado sob autenticação Nº WSQ.028.481.AO7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10537                  de  28/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°46527,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ENGENHEIRO-ÁREA, do Quadro
de Pessoal desta Universidade, JOÃO PEDRO MARINS TREVISO (Siape: 2257648 ),  para substituir   FLORA
GOULART (Siape: 2053467 ),  Chefe do Setor de Orçamentos e Prevenção contra Incêndios vinculado ao
Departamento de Adequações de Infraestrutura da SUINFRA, Código FG-3, em seu afastamento por motivo
de férias, no período de 02/01/2019 a 31/01/2019, com o decorrente pagamento das vantagens por 30 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
