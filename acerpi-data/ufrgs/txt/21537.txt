Documento gerado sob autenticação Nº EEM.704.452.3N4, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3305                  de  03/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°19525,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, LUCIANA NUNES KIEFER (Siape: 3656432 ),  para
substituir   LUCIANA PRASS (Siape: 2290251 ), Chefe do Depto de Música do Instituto de Artes, Código FG-1,
em seu afastamento no país, no período de 27/04/2016 a 01/05/2016, com o decorrente pagamento das
vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
