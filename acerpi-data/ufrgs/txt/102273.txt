Documento gerado sob autenticação Nº CFV.934.075.N0R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11179                  de  16/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  13/12/2019,   referente  ao  interstício  de
13/06/2018 a 12/12/2019, para o servidor HENRIQUE WILSON WERKHAUSEN FILHO, ocupante do cargo de
Técnico de Laboratório Área -  701244,  matrícula  SIAPE 2155472,   lotado  na  Escola  de Engenharia,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.530480/2019-59:
Formação Integral de Servidores da UFRGS V CH: 124 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 4 hora(s) (05/03/2018 a 08/10/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
