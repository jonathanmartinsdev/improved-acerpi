Documento gerado sob autenticação Nº SVF.805.117.SDE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8232                  de  15/10/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
e tendo em vista  o  que consta  do  Processo  Administrativo  n°  23078.011090/2014-70,  do  Contrato  de
Permissão de Uso a Título Oneroso nº 002/2018, do Decreto-Lei 9.760/49 e ainda da Lei 8.666/93,
              RESOLVE:
 
            Extinguir, por esta PORTARIA RESCISÓRIA, unilateralmente, os vinculos contratuais com o servidor
CRISTIANO DE ÁVILA DOTTO, CPF nº 937.201.730-20, firmados no Contrato de Permissão de Uso a Título
Oneroso nº  002/2018,  a  partir  de 14/06/2018,  em razão do advento de cláusula  resolutiva  expressa,
conforme previsto no parágrafo 2º da cláusula décima primeira do Contrato supracitado e autorizado pelo
artigo 54 da Lei 8.666/93 c/c artigo 474 do CCB.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
