Documento gerado sob autenticação Nº ORG.347.298.RC2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10339                  de  28/12/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 19/12/2016,  a ocupante do cargo de Professor do Magistério
Superior, classe Adjunto do Quadro de Pessoal desta Universidade, LUCIA MARIA KLIEMANN, matrícula SIAPE
n° 1143203, da função de Vice-Diretora da Faculdade de Medicina, Código SRH 238, código FG-1, para a qual
foi designada pela Portaria 3083/13, de 27/05/2013, publicada no Diário Oficial da União do dia 28/05/2013.
Processo nº 23078.025395/2016-21.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
