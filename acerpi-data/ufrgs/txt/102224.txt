Documento gerado sob autenticação Nº VEK.523.926.C0I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11116                  de  13/12/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de ELISEO BERNI REATEGUI, Professor do Magistério Superior,
lotado e  em exercício  no Departamento de  Estudos  Especializados  da  Faculdade de  Educação,  com a
finalidade de ministrar disciplina junto à Universidad de La República - Uruguay, em Montevidéu, Uruguai, no
período compreendido entre 15/12/2019 e 20/12/2019, incluído trânsito, com ônus limitado. Solicitação nº
89358.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
