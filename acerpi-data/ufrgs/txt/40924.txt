Documento gerado sob autenticação Nº LOA.056.666.KE3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6156                  de  12/07/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°31466,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  BIBLIOTECÁRIO-
DOCUMENTALISTA, do Quadro de Pessoal desta Universidade, LETÍCIA ANGHEBEN EL AMMAR CONSONI
(Siape: 1743847 ),  para substituir   JUNE MAGDA ROSA SCHARNBERG (Siape: 0351463 ), Chefe da Biblioteca
de  Engenharia,  Código  FG-5,  em  seu  afastamento  por  motivo  de  férias,  no  período  de  10/07/2017  a
20/07/2017, com o decorrente pagamento das vantagens por 11 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
