Documento gerado sob autenticação Nº GZZ.161.465.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6515                  de  21/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de Tanise Muller Ramos,  Professor do Magistério do Ensino
Básico,  Técnico e Tecnológico,  lotada e em exercício no Departamento de Humanidades do Colégio de
Aplicação,  com a finalidade do "IV  Colóquio Luso-Afro-Brasileiro  de Questões Curriculares",  em Lisboa,
Portugal, no período compreendido entre 08/09/2018 e 14/09/2018, incluído trânsito, com ônus limitado.
Solicitação nº 58317.
RUI VICENTE OPPERMANN
Reitor
