Documento gerado sob autenticação Nº YRG.134.456.5GQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11333                  de  20/12/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°36215,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO,  do Quadro de  Pessoal  desta  Universidade,  VANISE NASCIMENTO PEROBELLI  (Siape:
1836975 ),  para substituir   MARCELO SOARES MACHADO (Siape: 0353832 ), Diretor do Departamento de
Administração de Pessoal da PROGESP, Código CD-4, em seu afastamento por motivo de férias, no período
de 30/12/2017 a 05/01/2018, com o decorrente pagamento das vantagens por 7 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
