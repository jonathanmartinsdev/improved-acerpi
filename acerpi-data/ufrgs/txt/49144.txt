Documento gerado sob autenticação Nº TZW.413.616.Q42, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             249                  de  05/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°41143,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de APONTADOR, do Quadro de
Pessoal desta Universidade, LEANDRO DE FREITAS HENRIQUES (Siape: 0357981 ),  para substituir   FATIMA
EDITH  CORREA  DA  LUZ  (Siape:  0356064  ),  Secretário  Assistente  da  PROPLAN,  Código  FG-2,  em  seu
afastamento por motivo de férias, no período de 15/01/2018 a 31/01/2018, com o decorrente pagamento das
vantagens por 17 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
