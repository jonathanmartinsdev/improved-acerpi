Documento gerado sob autenticação Nº JMB.932.823.B9L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10548                  de  17/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora LISIANE DE SOUZA NUNES DE MOURA, ocupante do cargo de  Estatístico -
701033, lotada na Faculdade de Ciências Econômicas, SIAPE 1860001, o percentual de 30% (trinta por cento)
de Incentivo à Qualificação, a contar de 11/07/2017, tendo em vista a conclusão do curso de Especialização
em Gestão Pública, conforme o Processo nº 23078.013406/2017-19.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
