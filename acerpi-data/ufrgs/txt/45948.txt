Documento gerado sob autenticação Nº OYM.726.550.MTU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9560                  de  13/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de JAIRTON DUPONT, Professor do Magistério Superior, lotado e
em exercício no Departamento de Química Orgânica do Instituto de Química, com a finalidade de participar
de  encontro  junto  à  Universidade  de  Murcia,  Espanha,  no  período  compreendido  entre  13/11/2017  e
17/11/2017, incluído trânsito, com ônus limitado. Solicitação nº 31639.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
