Documento gerado sob autenticação Nº SHD.178.755.D9T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1433                  de  11/02/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de
2016, do Magnífico Reitor, e conforme a Solicitação de Férias n°50993
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, IGNACIO MARIA BENITES MORENO (Siape: 2687222),
 para substituir FELIPE CARON (Siape: 1852145), Coordenador da COMGRAD do Curso de Bacharelado em
Ciências Biológicas - ênfase em Biologia Marinha e Costeira e Gestão Ambiental, Marinha e Costeira, Código
SRH 1185, FUCC, em seu afastamento por férias, no período de 16/02/2020 a 21/02/2020, com o decorrente
pagamento das vantagens por 6 dias. Processo SEI Nº 23078.502776/2020-13
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
