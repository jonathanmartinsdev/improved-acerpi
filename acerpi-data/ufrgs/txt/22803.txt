Documento gerado sob autenticação Nº NSZ.654.581.PHC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3992                  de  03/06/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de Felipe José Comunello,  Professor do Magistério Superior,
lotado e em exercício  no Departamento Interdisciplinar  do Campus Litoral  Norte,  com a finalidade de
participar de encontro na Brown University, em Providence, Estados Unidos, no período compreendido entre
04/06/2016 e 18/06/2016, incluído trânsito, com ônus limitado. Solicitação nº 19204.
CARLOS ALEXANDRE NETTO
Reitor
