Documento gerado sob autenticação Nº DEJ.549.498.Q0T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9924                  de  26/10/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  22/10/2017,   referente  ao  interstício  de
22/04/2016 a 21/10/2017, para o servidor VALERI BRANDO, ocupante do cargo de Técnico de Laboratório
Área -  701244,  matrícula  SIAPE 0355631,   lotado  no  Instituto  de  Biociências,  passando do Nível  de
Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.518075/2017-09:
UFRGS - Aprimoramento de Técnicas de Conservação e Aprendizado Teórico e Prático Visando a Manutenção
de Cultivos de Espécies Aquáticas como Peixes e Plantas CH: 166 Carga horária utilizada: 150 hora(s) / Carga
horária excedente: 16 hora(s) (01/12/2014 a 30/04/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
