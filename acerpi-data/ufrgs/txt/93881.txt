Documento gerado sob autenticação Nº XAB.966.096.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5764                  de  10/07/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, à
Professora NEILA SELIANE PEREIRA WITT, matrícula SIAPE 2240694, lotada no Campus Litoral Norte e com
exercício no Departamento Interdisciplinar, da classe A  de Professor Adjunto A, nível 02, para a classe C  de
Professor Adjunto, nível 01, com vigência financeira a partir de 08/05/2019, de acordo com o que dispõe a Lei
nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554, de 20 de junho de 2013 do
Ministério da Educação e a Decisão nº 331/2017. Processo nº 23078.511224/2019-62.
RUI VICENTE OPPERMANN
Reitor.
