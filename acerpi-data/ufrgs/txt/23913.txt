Documento gerado sob autenticação Nº XIQ.490.954.G4B, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4716                  de  27/06/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 5263, de 01 de outubro de
2012, e conforme a Solicitação de Afastamento n°20504,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, TATIANE DA SILVA DAL PIZZOL (Siape: 1562592 ),
 para substituir   DENISE BUENO (Siape: 1412810 ), Coordenador da COMGRAD de Farmácia, Código FUC, em
seu afastamento no país, no dia 14/06/2016, com o decorrente pagamento das vantagens por 1 dia.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
