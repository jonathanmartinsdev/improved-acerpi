Documento gerado sob autenticação Nº NOC.593.143.RKU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4003                  de  09/05/2019
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 16 de maio de 2019,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
GABRIEL OLIVEIRA BATISTA, ocupante do cargo de Assistente em Administração, Ambiente Organizacional
Administrativo,  Código 701200,  Classe  D,  Nível  de  Capacitação II,  Padrão de  Vencimento 02,  SIAPE nº.
2423627 da Pró-Reitoria de Gestão de Pessoas para a lotação Escola de Administração, com novo exercício
no Setor Acadêmico da Gerência Administrativa. 
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
