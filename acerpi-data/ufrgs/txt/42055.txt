Documento gerado sob autenticação Nº QIS.949.558.M0L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6933                  de  31/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Declarar vago, a partir de 13 de junho de 2017, o Cargo de Assistente em Administração, Código
701200, Nível de Classificação D, Nível de Capacitação I, Padrão 03, do Quadro de Pessoal, em decorrência de
posse em outro cargo inacumulável, de MAYQUEL FERREIRA ELEUTHÉRIO com lotação na Faculdade de
Educação. Processo nº 23078.011167/2017-54.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
