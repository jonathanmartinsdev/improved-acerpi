Documento gerado sob autenticação Nº JGB.389.205.CL3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5456                  de  23/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  20/06/2017,   referente  ao  interstício  de
19/10/2015 a 19/06/2017, para a servidora ANGELA FRANCISCA ALMEIDA DE OLIVEIRA, ocupante do cargo
de Assistente em Administração - 701200, matrícula SIAPE 2098038,  lotada  no  Instituto de Psicologia,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.025486/2015-85:
Formação Integral de Servidores da UFRGS V CH: 121 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 1 hora(s) (04/04/2014 a 26/05/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
