Documento gerado sob autenticação Nº QHU.153.107.86P, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6346                  de  16/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  CAROLINA  HESSEL  SILVEIRA,  matrícula  SIAPE  n°  1558099,  lotada  e  em  exercício  no
Departamento de Estudos Especializados da Faculdade de Educação, da classe C  de Professor Adjunto, nível
01, para a classe C  de Professor Adjunto, nível 02, referente ao interstício de 12/08/2015 a 11/08/2017, com
vigência financeira a partir de 13/12/2017, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de
2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.522342/2017-34.
JANE FRAGA TUTIKIAN
Vice-Reitora.
