Documento gerado sob autenticação Nº MIH.151.518.FP9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9871                  de  06/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir da data deste ato, como segue:
 
Transformar:  Assessor Administrativo do Departamento de Suporte à Infraestrutura -  SUINFRA,
código SRH 995, código FG-1,  em Diretor do Departamento de Meio Ambiente e Licenciamento, código
SRH 995, código FG-1.
 
 
Processo nº 23078.533093/2018-93.
 
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
