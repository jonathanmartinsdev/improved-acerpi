Documento gerado sob autenticação Nº APM.829.590.H6D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2302                  de  14/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar  a  Portaria  n°  2035/2019,  de  01/03/2019,   que  designa,  temporariamente,   VANISE
NASCIMENTO PEROBELLI, Assistente em Administração, com exercício na Divisão de Saúde Suplementar da
Pró-Reitoria de Gestão de Pessoas, para substituir MARCELO SOARES MACHADO, Diretor do Departamento
de Administração de Pessoal da PROGESP. Processo nº 23078.505838/2019-13. 
 
Onde se lê:
"... no período de 06/03/2019 a 13/03/2019, com o decorrente pagamento das vantagens por 8
dias.".
leia-se:
"...  no período de 06/03/2019 a 12/03/2019,  com o decorrente pagamento das vantagens por
7 dias.", ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
