Documento gerado sob autenticação Nº UXZ.445.533.KMC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             928                  de  31/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  21/12/2017,   referente  ao  interstício  de
28/06/2010 a 20/12/2017, para a servidora ROSA MARIA PACHECO, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 1446874,  lotada  no  Colégio de Aplicação, passando do Nível de
Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.524331/2017-99:
Formação Integral de Servidores da UFRGS III CH: 68 (18/05/2009 a 01/12/2017)
ENAP - Regulamentação da LAI nos Municípios CH: 20 (14/11/2017 a 04/12/2017)
ENAP - Impactos da Mudança do Clima para Gestão Municipal CH: 20 (14/11/2017 a 04/12/2017)
ENAP - Controles Institucional e Social dos Gastos Públicos CH: 20 Carga horária utilizada: 17 hora(s) / Carga
horária excedente: 3 hora(s) (28/11/2017 a 18/12/2017)
ENAP - Introdução ao Federalismo e ao Federalismo Fiscal no Brasil CH: 25 (28/11/2017 a 18/12/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
