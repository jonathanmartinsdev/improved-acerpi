Documento gerado sob autenticação Nº VHE.540.647.L1O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1533                  de  20/02/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°42062,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de AUXILIAR EM ADMINISTRAÇÃO,
do Quadro de Pessoal desta Universidade, ALEXANDRA GONÇALVES MARTINS (Siape: 2059154 ),   para
substituir   VANISE NASCIMENTO PEROBELLI (Siape: 1836975 ), Diretor da Divisão de Saúde Suplementar da
CPCPJ/DAP/PROGESP, Código FG-1, em seu afastamento por motivo de férias, no período de 26/02/2018 a
02/03/2018, com o decorrente pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
