Documento gerado sob autenticação Nº HYO.138.015.FOP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10433                  de  26/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar,  a  partir  de  01/02/2019,  o  ocupante  do  cargo  de  Porteiro  -  701458,  do  Nível  de
Classificação CIV, do Quadro de Pessoal desta Universidade, EZEQUIEL DA ROSA MEDEIROS, matrícula SIAPE
0358455 da função de Coordenador do Núcleo Administrativo da Gerência Administrativa da Escola de
Educação Física, Fisioterapia e Dança, código SRH 490, código FG-5, para a qual foi designado pela Portaria nº
10399/2018  de  21/12/2018,  publicada  no  Diário  Oficial  da  União  de  26/12/2018.  Processo  nº
23078.534904/2018-73.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
