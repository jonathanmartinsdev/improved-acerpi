Documento gerado sob autenticação Nº PZB.335.245.EMK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             333                  de  09/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria nº 59/2020, de 03/01/2020, que designou, temporariamente, RAPHAEL ZILLIG
(Siape:  2611124)  para  substituir   ALFREDO  CARLOS  STORCK  (Siape:  1067820  ),  Processo  SEI  Nº
23078.500297/2020-62
 
onde se lê:
 
"...em seu afastamento por motivo de férias, no período de 03/01/2020 a 08/02/2020... ";
 
           leia-se:
 
 "... em seu afastamento por motivo de férias, no período de 03/01/2020 a 31/01/2020..."; ficando
ratificados os demais termos. 
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
