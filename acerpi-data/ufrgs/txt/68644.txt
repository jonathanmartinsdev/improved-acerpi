Documento gerado sob autenticação Nº YXJ.661.608.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6552                  de  22/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de PAOLO RECH, Professor do Magistério Superior, lotado e em
exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de  Informática,  com a  finalidade  de  
participar  de  reunião na University of Oxford, em Didcot, Inglaterra, com ônus limitado e da "Conference on
Radiation Effects on Components and Systems",  em Gotemburgo,  Suécia,  com ônus CAPES/PROAP,  no
período compreendido entre 12/09/2018 e 24/09/2018, incluído trânsito. Solicitação nº 58621.
RUI VICENTE OPPERMANN
Reitor
