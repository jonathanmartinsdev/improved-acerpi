Documento gerado sob autenticação Nº VDO.799.352.4UO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9185                  de  10/10/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor FERNANDO FONTANA DIAS, ocupante do cargo de  Administrador - 701001,
lotado na Faculdade de Ciências Econômicas, SIAPE 2406898, o percentual de 30% (trinta por cento) de
Incentivo à Qualificação, a contar de 01/08/2019, tendo em vista a conclusão do curso de Especialização em
Gestão Pública - UAB, conforme o Processo nº 23078.513816/2019-19.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
