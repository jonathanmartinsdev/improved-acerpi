Documento gerado sob autenticação Nº GXD.580.464.EMJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2552                  de  09/04/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Declarar vaga, a partir de 29/03/2018, a função de Diretora da Divisão Administrativa do DARE da
PROREXT, Código SRH 1284, Código FG-5, desta Universidade, tendo em vista a aposentadoria de NECY DE
LIMA SCHNITZER, matrícula SIAPE n° 0354438, conforme Portaria n° 2328/2018, de 27 de março de 2018,
publicada no Diário Oficial da União do dia 29 de março de 2018. Processo nº 23078.507627/2018-26.
JANE FRAGA TUTIKIAN
Vice-Reitora.
