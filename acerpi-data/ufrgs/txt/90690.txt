Documento gerado sob autenticação Nº QAL.970.631.K2M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3844                  de  07/05/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016, e conforme
Solicitação de Férias nº 50046,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior,  do Quadro de Pessoal  desta Universidade,  LUIZ ANTONIO BRESSANI (Siape:  0353663),   para
substituir  WAI YING YUK GEHLING (Siape:  0355840 ),  Chefe do Depto de Engenharia Civil  da Escola de
Engenharia, Código SRH 109, FG-1, em seu afastamento por férias, no período de 01/05/2019 a 04/05/2019,
com o decorrente pagamento das vantagens por 4 dias. Processo SEI nº 23078.511181/2019-15
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
