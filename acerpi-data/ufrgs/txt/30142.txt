Documento gerado sob autenticação Nº HMQ.288.589.OMJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9356                  de  23/11/2016
Nomeia a Representação Discente do Centro
Acadêmico  dos  Estudantes  do  Curso  de
Engenharia Física da UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita para compor o Centro Acadêmico dos Estudantes do Curso
de Engenharia Física, com mandato de 01 (um) ano, a contar de 19 de dezembro de 2015, atendendo ao
disposto  nos  artigos  175  do  Regimento  Geral  da  Universidade  e  79  do  Estatuto  da  Universidade  e
considerando o processo nº 23078.017668/2016-63, conforme segue:
 
PRESIDENTE Carlos Ballardin Beltrami
VICE-PRESIDENTE Eduardo Renani Ribeiro
TESOUREIRO Gabriel Giovanaz
VICE-TESOUREIRA Carina Sand
VICE-SECRETÁRIO Leo Filipe Lima
RELAÇÕES INSTITUCIONAIS Katiane Pereira Maciel
 
 
 
 
 
     
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
