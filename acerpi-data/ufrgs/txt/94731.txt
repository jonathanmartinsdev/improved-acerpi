Documento gerado sob autenticação Nº BRZ.391.763.DO4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6275                  de  21/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°56573,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de AUXILIAR EM ADMINISTRAÇÃO,
do Quadro de Pessoal desta Universidade, GIANE GAZZANA E SILVA RODRIGUES (Siape: 2055561 ),  para
substituir   FATIMA EDITH CORREA DA LUZ (Siape: 0356064 ), Diretor da Divisão de Gestão de Pessoas da GA
da PROPLAN, Código FG-2,  em seu afastamento por motivo de Laudo Médico do titular da Função, no
período de 11/07/2019 a 12/07/2019, com o decorrente pagamento das vantagens por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
