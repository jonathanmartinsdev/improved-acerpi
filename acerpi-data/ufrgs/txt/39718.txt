Documento gerado sob autenticação Nº RKX.471.706.0OA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5355                  de  21/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ALFREDO DE JESUS DAL MOLIN FLORES, matrícula SIAPE n° 1565112, lotado e em exercício no
Departamento de Direito Público e Filosofia do Direito da Faculdade de Direito, da classe D  de Professor
Associado, nível 01, para a classe D  de Professor Associado, nível 02, referente ao interstício de 23/02/2015 a
22/02/2017, com vigência financeira a partir de 23/02/2017, de acordo com o que dispõe a Lei nº 12.772, de
28 de dezembro de 2012, com suas alterações e a Decisão nº 197/2006-CONSUN, alterada pela Decisão nº
401/2013-CONSUN. Processo nº 23078.505281/2017-41.
JANE FRAGA TUTIKIAN
Vice-Reitora.
