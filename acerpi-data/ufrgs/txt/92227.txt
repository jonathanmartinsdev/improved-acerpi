Documento gerado sob autenticação Nº DFH.968.907.O3T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4776                  de  04/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de CLAUDIO ROSITO JUNG,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Informática Aplicada do Instituto de Informática, com a finalidade
de participar da conferência "CVPR 2019", em Los Angeles, Estados Unidos, no período compreendido entre
15/06/2019 e 22/06/2019, incluído trânsito, com ônus CNPq. Solicitação nº 84115. 
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
