Documento gerado sob autenticação Nº NOS.024.321.06L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5942                  de  06/07/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Lotar  na  Faculdade  de  Odontologia,  com  exercício  no  Núcleo  Especializado  da  Gerencia
Administrativa da ODONTO, DOUGLAS BERNARDO PAIXÃO, nomeado conforme Portaria Nº 4549/2017 de
22 de maio de 2017, publicada no Diário Oficial da União no dia 23 de maio de 2017, em efetivo exercício
desde  28  de  junho  de  2017,  ocupante  do  cargo  de  ASSISTENTE  DE  LABORATÓRIO,  Ambiente
Organizacional Ciências da Saúde, classe C, nível I, padrão 101, no Quadro de Pessoal desta Universidade. 
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
