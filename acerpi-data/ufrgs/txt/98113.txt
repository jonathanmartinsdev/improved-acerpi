Documento gerado sob autenticação Nº GNM.321.406.001, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8385                  de  13/09/2019
  Dispensa, a pedido, integrante do Comitê
Gestor  do  Projeto  Institucional  de
Internacionalização  da  Universidade
F e d e r a l  d o  R i o  G r a n d e  d o  S u l  -
P I I /PRINT/UFRGS.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Dispensar,  a  pedido,  do  Comitê  Gestor  do  Projeto  Institucional  de  Internacionalização  da
Universidade Federal do Rio Grande do Sul -  PII/PRINT/UFRGS, o Professor KEPLER DE SOUZA OLIVEIRA
FILHO, instituído pela Portaria nº 5037 de 13 de junho de 2019, a partir desta data.
RUI VICENTE OPPERMANN,
Reitor.
