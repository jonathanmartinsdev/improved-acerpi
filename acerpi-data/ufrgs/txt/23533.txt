Documento gerado sob autenticação Nº LCQ.232.036.GMC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4486                  de  20/06/2016
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 5263, de 01 de outubro de
2012, e conforme a Solicitação de Férias n°25458,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, SILVIA FERNANDA PECANHA MARTINS (Siape:
0355363 ),  para substituir   FRANCISCO CARLOS DA ROSA (Siape: 0356072 ), Chefe da Seção de Análise do
Empenho da Div de Análise da Desp /DCF/PROPLAN, Código FG-5, em seu afastamento por motivo de férias,
no período de 20/06/2016 a 29/06/2016, com o decorrente pagamento das vantagens por 10 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
