Documento gerado sob autenticação Nº PZG.313.290.HJP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10499                  de  22/11/2019
o Pró-Reitor de Gestão de Pessoas em exercício DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO
SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, LUCIANE MAGALHAES CORTE REAL (Siape: 2213295 ),
 para substituir   MARIA CLARA BUENO FISCHER (Siape: 1847122 ), Coordenador da Comissão de Pesquisa da
Faculdade de Educação, em seu afastamento do país, no período de 24/11/2019 a 29/11/2019.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
