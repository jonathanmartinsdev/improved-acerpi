Documento gerado sob autenticação Nº YZQ.106.645.HIP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6173                  de  19/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°49836,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, RAFAELA SCHILARDI SCAPINI (Siape: 3082820
),  para substituir   LOUIDI LAUER ALBORNOZ (Siape: 2027001 ), Coordenador do Núcleo de Apoio Técnico da
Gerência Administrativa do Instituto de Pesquisas Hidráulicas, Código FG-7, em seu afastamento por motivo
de férias, no período de 22/07/2019 a 27/07/2019, com o decorrente pagamento das vantagens por 6 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
