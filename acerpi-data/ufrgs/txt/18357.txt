Documento gerado sob autenticação Nº TZG.338.470.H0T, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1393                  de  25/02/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°26049,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS, do Quadro de Pessoal desta Universidade, CARLA MARIA BASTOS DOS SANTOS (Siape:
1088119 ),  para substituir   POLLIANE TREVISAN NUNES (Siape: 1978491 ), Chefe da Seção de Registro da
DPAT  do  DARE  da  PROREXT,  Código  FG-5,  em  seu  afastamento  por  motivo  de  férias,  no  período  de
11/02/2016 a 19/02/2016.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
