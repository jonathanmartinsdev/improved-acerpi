Documento gerado sob autenticação Nº GMD.165.392.0PS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4238                  de  15/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CAMILA BAUER BRONSTRUP, Professor do Magistério Superior,
lotada e em exercício  no Departamento de Arte Dramática do Instituto de Artes,  com a finalidade de
participar da "14th Prague Quadrennial of Performance Design and Space", em Praga, República Tcheca, no
período compreendido entre 05/06/2019 e 16/06/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de
Pesquisa: diárias). Solicitação nº 83514.
RUI VICENTE OPPERMANN
Reitor
