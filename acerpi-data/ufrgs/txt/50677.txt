Documento gerado sob autenticação Nº VFK.685.329.5UP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1177                  de  07/02/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, e conforme a Solicitação de Férias n°41884,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ANA SOLEDADE GRAEFF MARTINS (Siape: 2323497 ),
 para substituir   MARCELO PIO DE ALMEIDA FLECK (Siape: 1051306 ),  Chefe do Depto de Psiquiatria e
Medicina Legal da Faculdade de Medicina, Código FG-1, em seu afastamento por motivo de férias, no período
de 07/02/2018 a 13/02/2018, com o decorrente pagamento das vantagens por 7 dias.
MARILIA BORGES HACKMANN
Pró-Reitora
