Documento gerado sob autenticação Nº LFN.494.900.3QO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1989                  de  03/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 03/03/2017, o ocupante do cargo de Analista de Tecnologia da Informação -
701062, do Nível de Classificação EIV, do Quadro de Pessoal desta Universidade, CARLOS EDUARDO REIS
VALADARES, matrícula SIAPE 1729066, da função de Coordenador do Núcleo de Infraestrutura da Diretoria
Administrativa do Campus Litoral Norte, Código SRH 1419, Código FG-1, para a qual foi designado pela
Portaria nº  4450/2016 de 20/06/2016,  publicada no Diário Oficial  da União de 21/06/2016.  Processo nº
23078.002902/2017-39.
JANE FRAGA TUTIKIAN
Vice-Reitora.
