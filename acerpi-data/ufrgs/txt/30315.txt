Documento gerado sob autenticação Nº JOP.844.045.G7M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9057                  de  10/11/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de AURORA CARNEIRO ZEN, Professor do Magistério Superior,
lotada e em exercício no Departamento de Ciências Administrativas da Escola de Administração, com a
finalidade de participar de congresso junto à ICN BusinessSchool, em Nancy; reunião na Université Paris-Est
Créteil, em Paris,  França e da "42nd Annual Conference of the European International Business Academy",
em Viena, Áustria, no período compreendido entre 26/11/2016 e 09/12/2016, incluído trânsito, com ônus
limitado. Solicitação nº 24215.
RUI VICENTE OPPERMANN
Reitor
