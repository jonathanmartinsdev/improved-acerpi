Documento gerado sob autenticação Nº LQZ.069.563.0JR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             928                  de  27/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de IGNACIO ITURRIOZ, Professor do Magistério Superior, lotado e
em exercício no Departamento de Engenharia Mecânica da Escola de Engenharia,  com a finalidade de
participar  de  cursos  junto  à  Universitá  di  Parma,  Itália,  no  período compreendido entre  27/02/2017 e
02/07/2017, incluído trânsito, com ônus limitado. Solicitação nº 22070.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
