Documento gerado sob autenticação Nº YCO.592.745.DPG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9929                  de  04/11/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.526522/2019-57  e
23078.500212/2018-21, do Pregão Eletrônico SRP nº 046/2018, da Lei nº 10.520/02 e ainda, da Lei nº 8.666/93,
 
            RESOLVE:
 
            Aplicar a sanção administrativa de MULTA de 2,5% (dois vírgula cinco por cento) sobre o valor da
parcela  que  lhe  deu  causa,  no  montante  de  R$  60,10  (sessenta  reais  e  dez  centavos),  conforme
demonstrativo de cálculo (Doc. SEI nº 1852527), prevista na alínea "c" do item 29.1.3 do Edital de Licitação, à
Empresa CARAS REVESTIMENTO EIRELI, CNPJ n.º 21.089.749/0001-27, pelo motivo de entrega de material
fora do prazo, conforme atestado pela fiscalização do contrato (Doc. SEI nº 1808900 e 1850218), bem como
pelo DICON/NUDECON (Doc. SEI nº 1852473) do processo 23078.526522/2019-57.
 
            Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG. 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
