Documento gerado sob autenticação Nº XFU.417.725.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             918                  de  28/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Declarar vago, a partir de 17 de janeiro de 2020, o Cargo de Professor do Magistério Superior,
Código 705001, Classe Adjunto A, Nível 01, do Quadro de Pessoal, em decorrência de nomeação para outro
cargo  público  inacumulável,  de  KATIA  REZZADORI  com  Lotação  no  Departamento  de  Tecnologia  dos
Alimentos do Instituto de Ciências e Tecnologia de Alimentos. Processo nº 23078.500801/2020-24.
RUI VICENTE OPPERMANN
Reitor.
