Documento gerado sob autenticação Nº GVQ.976.161.QC3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6920                  de  03/09/2018
Nomeação  da  Representação  Discente  na
Comissão  de  Extensão  da  Escola  de
Enfermagem  da  UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear  a  Representação Discente  eleita  para  compor  a  Comissão de  Extensão na  Escola  de
Enfermagem, com mandato de 01 (um) ano, a contar de 14 de agosto de 2018, atendendo o disposto nos
artigos 175 do Regimento Geral  da Universidade e  79 do Estatuto da Universidade,  e  considerando o
processo nº 23078.521775/2018-53, conforme segue:
 
Comissão de Extensão
 
Titular: Christofer da Silva Cristofoli
Suplente: Jéssica Lopes Lucio
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
