Documento gerado sob autenticação Nº ZKF.542.139.EA8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4473                  de  22/05/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  25/04/2019,   referente  ao  interstício  de
01/09/2016 a 24/04/2019, para a servidora DANIELLE FINAMOR REZES DE SOUZA, ocupante do cargo de
Auxiliar de Creche - 701410,  matrícula SIAPE 1092105,  lotada  na  Faculdade de Ciências Econômicas,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  C  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  C  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.510715/2019-96:
Portal Educação - Moodle: Conceitos e Administração CH: 11 (06/06/2016 a 05/08/2016)
Portal Educação - Inglês para Viagens CH: 120 Carga horária utilizada: 109 hora(s) / Carga horária excedente:
11 hora(s) (14/10/2017 a 14/10/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
