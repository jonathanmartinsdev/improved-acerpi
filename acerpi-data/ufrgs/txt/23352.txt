Documento gerado sob autenticação Nº IYD.377.787.9I7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4559                  de  22/06/2016
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de
2013
RESOLVE
Alterar o exercício, a partir de 12 de maio de 2016, de MANOEL CARLOS CARVALHO DOS SANTOS,
matrícula SIAPE nº 0356574, ocupante do cargo de Técnico em Eletricidade, da Prefeitura Campus Centro
(SP1) para a Pró-Reitoria de Assuntos Estudantis, mantendo lotação na Superintendência de Infraestrutura.
Processo n° 23078.011731/2016-58.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
