Documento gerado sob autenticação Nº KRL.828.118.GR4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4058                  de  09/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal desta Universidade, RODRIGO LAGES E SILVA, matrícula SIAPE n° 1016335, lotado no Departamento
de Estudos Básicos da Faculdade de Educação, como Chefe Substituto do Depto de Estudos Básicos da
Faculdade de Educação, para substituir automaticamente o titular desta função em seus afastamentos ou
impedimentos  regulamentares  no  período  de  28/05/2017  e  até  27/05/2019.  Processo  nº
23078.007746/2017-01.
JANE FRAGA TUTIKIAN
Vice-Reitora.
