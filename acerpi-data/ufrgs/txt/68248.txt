Documento gerado sob autenticação Nº GNZ.064.023.F9H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6298                  de  15/08/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor CARLOS ALBERTO ALVES MEDEIROS, ocupante do cargo de  Servente de
Obras - 701824, lotado na Superintendência de Infraestrutura, SIAPE 0358144, o percentual de 10% (dez por
cento) de Incentivo à Qualificação, a contar de 03/08/2018, tendo em vista a conclusão do curso de Ensino
Fundamental, conforme o Processo nº 23078.519767/2018-47.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
