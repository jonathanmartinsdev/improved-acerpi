Documento gerado sob autenticação Nº BPL.136.612.IHC, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3377                  de  06/05/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Remover, a partir de 25 de abril de 2016,  de acordo com o artigo 36, parágrafo único, inciso II da Lei
n° 8.112, de 11 de dezembro de1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997, ALINE
LANGHAMMER CARVALHO, ocupante do cargo de Assistente em Administração, Ambiente Organizacional
Administrativo, Código 701200, Classe D, Nível de Capacitação I, Padrão de Vencimento 01, SIAPE nº. 2157196
da Secretaria de Comunicação Social para a lotação Faculdade de Direito, com novo exercício no Programa
de Pós-Graduação em Direito.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
