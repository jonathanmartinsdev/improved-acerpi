Documento gerado sob autenticação Nº HGA.219.292.605, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8721                  de  18/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor GUSTAVO LEAES GOMEZ,
ocupante do cargo de Assistente em Administração-701200, lotado na Secretaria de Relações Internacionais,
SIAPE 1838885, para 30% (trinta por cento), a contar de 16/12/2016, tendo em vista a conclusão do curso de
Especialização  em  Estratégia  e  Relações  Internacionais  Contemporâneas,  conforme  o  Processo  nº
23078.023417/2016-18.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
