Documento gerado sob autenticação Nº YOQ.765.581.0F9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8302                  de  16/10/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Designar ROBERTO PETRY HOMRICH, matrícula SIAPE n° 0357390, ocupante do cargo de Professor
do Magistério Superior,  classe Associado,  lotado no Departamento de Engenharia Elétrica da Escola de
Engenharia, do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Coordenador da
COMGRAD de Engenharia Elétrica, Código SRH 1218, código FUC, com vigência a partir de 01/11/2018 até
23/05/2019, a fim de completar o mandato do Professor Alberto Bastos do Canto Filho, conforme artigo 92
do Estatuto da mesma Universidade. Processo nº 23078.525873/2018-60.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
