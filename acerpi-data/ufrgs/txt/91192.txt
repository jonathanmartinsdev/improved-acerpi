Documento gerado sob autenticação Nº ESI.644.822.TVK, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4155                  de  14/05/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  FERNANDA  GOMES  VICTOR,  matrícula  SIAPE  n°  1621086,  lotada  no
Departamento de Ciências Contábeis e Atuariais da Faculdade de Ciências Econômicas, como Coordenadora
Substituta da COMGRAD de Ciências Atuariais, para substituir automaticamente o titular desta função em
seus  afastamentos  ou  impedimentos  regulamentares  na  vigência  do  presente  mandato.  Processo  nº
23078.509671/2019-51.
JANE FRAGA TUTIKIAN
Vice-Reitora.
