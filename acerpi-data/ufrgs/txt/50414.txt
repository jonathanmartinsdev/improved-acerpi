Documento gerado sob autenticação Nº VVI.234.709.ASV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1061                  de  05/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  ao  servidor  MARCELO  MAIA  FERREIRA,  ocupante  do  cargo  de   Assistente  em
Administração - 701200, lotado no Campus Litoral Norte, SIAPE 1872892, o percentual de 15% (quinze por
cento) de Incentivo à Qualificação, a contar de 18/01/2018, tendo em vista a conclusão do Curso Superior de
Tecnologia em Fruticultura, conforme o Processo nº 23078.500938/2018-64.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
