Documento gerado sob autenticação Nº SNO.640.739.QIC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7289                  de  13/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°86078,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, PAULO RICARDO DA ROZA TERRA (Siape:
0356544 ),  para substituir   GABRIEL DE FREITAS FOCKING (Siape: 1757966 ), Coordenador do Núcleo Financ
de Pós-Grad da Coord de Apoio Acad da Gerência Administrativa do IFCH, Código FG-7, em seu afastamento
n o  p a í s ,  n o  p e r í o d o  d e  1 1 / 0 8 / 2 0 1 9  a  1 5 / 0 8 / 2 0 1 9 .  C o n f o r m e  o  O f í c i o
nº00021/2019/CGJURADV/PRU4R/PGU/AGU  e  o  PARECER  DE  FORÇA  EXECUTÓRIA  n.  00005/2019/CGJUR-
ADV/PRU4R/PGU/AGU.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
