Documento gerado sob autenticação Nº PJW.061.842.436, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             776                  de  22/01/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de IGNACIO ITURRIOZ, Professor do Magistério Superior, lotado e
em exercício no Departamento de Engenharia Mecânica da Escola de Engenharia, com a finalidade de atuar
como Professor Visitante junto à Università degli Studi di Parma, em Parma, Itália, no período compreendido
entre 15/02/2019 e 30/06/2019, com ônus limitado. Processo nº 23078.532196/2018-36.
RUI VICENTE OPPERMANN
Reitor.
