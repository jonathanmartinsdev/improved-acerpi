Documento gerado sob autenticação Nº ZJU.471.718.H3N, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4808                  de  01/07/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Dispensar, a partir de 02/07/2016, o(a) ocupante do cargo de Auxiliar em Administração - 701405, do
Nível de Classificação CII, do Quadro de Pessoal desta Universidade, ROBINSON DUARTE DE SOUZA, CPF nº
56098588015, matrícula SIAPE 2056339 da função de Coordenador do Núcleo Administrativo/Compras da
Gerência  Administrativa  do  Instituto  de  Matemática,  Código  SRH  358,  Código  FG-4,  para  a  qual  foi
designado(a) pela Portaria nº 2196/2016 de 22/03/2016, publicada no Diário Oficial da União de 24/03/2016.
Processo nº 23078.202165/2016-91.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
