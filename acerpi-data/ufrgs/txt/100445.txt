Documento gerado sob autenticação Nº NNB.815.827.V8I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10002                  de  06/11/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias, de acordo com o processo nº 23078.008301/2019-00, considerando o disposto na Lei nº 8.112,
de 11/12/1990, no Decreto Presidencial nº 1.590, de 10/08/1995, no Decreto nº 4.836, de 09/09/2003, na
Decisão  nº  432  do  Conselho  Universitário,  de  27/11/2015  e  na  Portaria  9.911  de  26/10/2017  desta
Universidade
RESOLVE
Art.  1° ALTERAR  a Portaria n° 9314/2019, de 14/10/2019 que concedeu a jornada de trabalho
flexibilizada no Centro de Teledifusão Educativa período diário de atendimento ao público das 07h às 19h,
pelo prazo de doze meses, a contar de 14/10/2019, conforme disposto no Art. 11 da Portaria 9.911 de
26/10/2017, inclusão da servidora Silvia Maria Secrieru Ciulei - Siape n° 0355368.
Art. 2° Os servidores técnico-administrativos abaixo relacionados terão jornada de trabalho de seis
horas diárias, cumprindo carga horária de trinta horas semanais de acordo com os termos desta Portaria:
 
ALESSANDRA DE ALMEIDA ABREU DAGOSTINI - SIAPE: 1867329
CLAUDIA MARIA ROCCA RIBEIRO - SIAPE: 6358722
CLAUDIO ROBERTO DORNELLES REMIAO - SIAPE: 2343869
DEBORAH DOS SANTOS RODRIGUES - SIAPE: 1096030
FRANCISCO RAMOS MILANEZ - SIAPE: 1889901
GUSTAVO CORTE REAL CORREA - SIAPE: 2162565
JULIANO FONTANIVE DUPONT - SIAPE: 2054423
LIDIANE DA SILVA SITTONI - SIAPE: 2167588
OSVALDINHO ARBOIT - SIAPE: 0354701
SILVIA MARIA SECRIERU CIULEI - SIAPE: 0355368
Art. 3° Ficam ratificados os demais termos.
RUI VICENTE OPPERMANN
Reitor.
