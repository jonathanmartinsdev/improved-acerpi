Documento gerado sob autenticação Nº MVR.860.473.S29, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9639                  de  17/10/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Alterar o exercício, a partir de 16 de outubro de 2017, de CLAUDIO ANTONIO SEVERO GONCALVES,
matrícula SIAPE nº  0358688,  ocupante do cargo de Assistente em Administração,  do Departamento de
Adequações  de  Infraestrutura  para  o  Centro  de  Estudos  Internacionais  sobre  Governo.  Processo
n° 23078.010917/2017-71.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
