Documento gerado sob autenticação Nº DZR.199.906.D9I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5726                  de  01/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a pedido, a partir de 23/07/2018, a ocupante do cargo de Assistente em Administração -
701200, do Nível de Classificação DIII, do Quadro de Pessoal desta Universidade, SUSANA MIRANDA DA
SILVA, matrícula SIAPE 1691952 da função de Chefe do Setor Administrativo, Financeiro e de Planejamento
da GA da FCE, Código SRH 643, Código FG-7,  para a qual foi  designada pela Portaria nº 7280/2016 de
14/09/2016, publicada no Diário Oficial da União de 16/09/2016. Processo nº 23078.518379/2018-49.
JANE FRAGA TUTIKIAN
Vice-Reitora.
