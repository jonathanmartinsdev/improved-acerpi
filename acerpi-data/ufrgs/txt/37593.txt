Documento gerado sob autenticação Nº AVM.894.322.EUJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             3762                  de  03/05/2017
Nomeia  Representação  Discente
eleita  no  Curso  de  Relações
Internacionais.
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita no Curso de Relações Internacionais, com mandato de 01
(um) ano, a contar de 20 de dezembro de 2016, atendendo ao disposto nos artigos 175 do Regimento Geral
da Universidade e 79 do Estatuto da Universidade e considerando o processo nº 23078.005199/2017-11,
conforme segue:
 
COMISSÃO DE GRADUAÇÃO
Titular João Otávio Figueiredo Bueno Cadore
Suplente Natália Alves Dorneles
DERI                                                                                                  
Titular Laura Castro Gonçalves
Suplente Sofia Oliveira Perusso
CONSUNI
Titular Lorenso Andreoli da Silva
Suplente Lucca Pires Silva Lima
PLENÁRIA DO DEPARTAMENTO DE ECONOMIA
Titular Camila da Silva Souza
Suplente Sofia Oliveira Perusso
COMISSÃO DE ACOMPANHAMENTO DE DISCIPLINAS
Titular Eduardo Tomankievicz Secchi
Suplente Lucca Pires Silva Lima
NÚCLEO DE AVALIAÇÃO DA UNIDADE
Titular Lucca Pires Silva Lima
Suplente Eduardo Tomankievicz Secchi
CURSO DE IDIOMAS
Titular  Natália Alves Dorneles
Suplente Sofia Oliveira Perusso
Documento gerado sob autenticação Nº AVM.894.322.EUJ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
TRI
Titular Camilla Martins Pereira
Suplente Lucca Pires Silva Lima
 
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
