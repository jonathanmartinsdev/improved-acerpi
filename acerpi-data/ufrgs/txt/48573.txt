Documento gerado sob autenticação Nº GZS.771.415.25J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11455                  de  26/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora FERNANDA VIEIRA AMORIM DA COSTA, matrícula SIAPE n° 1903362, lotada no Departamento de
Medicina Animal da Faculdade de Veterinária e com exercício na Comissão de Graduação de Veterinária, da
classe C  de Professor Adjunto,  nível  03,  para a classe C  de Professor Adjunto,  nível  04,  referente ao
interstício de 25/11/2015 a 24/11/2017, com vigência financeira a partir de 25/11/2017, de acordo com o que
dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e a Resolução nº 12/1995-COCEP,
alterada pela Decisão nº 401/2013-CONSUN. Processo nº 23078.522608/2017-49.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
