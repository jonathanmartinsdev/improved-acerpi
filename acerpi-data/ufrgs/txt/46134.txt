Documento gerado sob autenticação Nº EHN.354.569.DJC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9704                  de  19/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento do país  de  MARCO AURELIO PIRES IDIART,  Professor  do Magistério
Superior, lotado e em exercício no Departamento de Física do Instituto de Física, com a finalidade de realizar
visita  à University of Birmingham, Inglaterra, no período compreendido entre 01/11/2017 e 29/01/2018,
incluído trânsito, com ônus limitado. Solicitação nº 32270.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
