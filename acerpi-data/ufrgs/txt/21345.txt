Documento gerado sob autenticação Nº EAP.296.403.N2J, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3217                  de  03/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor do Magistério do Ensino Básico, Técnico e Tecnológico ITALO MODESTO DUTRA, matrícula SIAPE
n° 1069415, com lotação no Colégio de Aplicação, da classe D  IV, nível 02, para a classe D  IV, nível 03,
referente ao interstício de 22/10/2008 a 21/04/2010, com vigência financeira a partir de 27/04/2016, de
acordo com o que dispõe a Lei nº 11.784, de 22 de setembro de 2008 e da Resolução nº 38/2006-CEPE.
Processo nº 23078.008715/2016-88.
RUI VICENTE OPPERMANN
Vice-Reitor
