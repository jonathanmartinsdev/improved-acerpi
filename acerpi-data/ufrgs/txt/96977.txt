Documento gerado sob autenticação Nº CBO.374.007.QF7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7632                  de  23/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de
2016, do Magnífico Reitor, e conforme a Solicitação de Férias n°47197,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, LUCIA HELENA RIBEIRO RODRIGUES (Siape: 3434821
),  para substituir   ANTONIO DOMINGUES BENETTI (Siape: 0357668 ), Coordenador do PPG em Recursos
Hídricos e Saneamento Ambiental, Código FUC, em seu afastamento por motivo de férias, no período de
25/08/2019 a 02/09/2019, com o decorrente pagamento das vantagens por 9 dias.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas
