Documento gerado sob autenticação Nº WIL.561.323.O6H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             441                  de  16/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
                         Autorizar a prorrogação do afastamento no País de ALEXANDRE RIBASSEMELER, Bibliotecário-Documentalista, lotado no Instituto de Geociências e com exercíciona  Biblioteca  do  Instituto  de  Geociências,  com a  finalidade  de  dar  continuidade  aosestudos  em nível  de  Doutorado,  junto  à  Universidade  Federal  de  Santa  Catarina,  noperíodo compreendido entre 31 de março de 2017 e 30 de março de 2018, com ônuslimitado. Processo 23078.200086/2015-65.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
