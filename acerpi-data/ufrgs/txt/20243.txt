Documento gerado sob autenticação Nº MVJ.167.422.AD3, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2498                  de  06/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar o afastamento do país de INARA ZANUZZI, Professor do Magistério Superior, lotada e em
exercício no Departamento de Filosofia do Instituto de Filosofia e Ciências Humanas, com a finalidade de
participar do "World Congress Aristotle 2400 Years", em Tessalonica, Grécia, no período compreendido entre
21/05/2016 e 30/05/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação
nº 18262.
CARLOS ALEXANDRE NETTO
Reitor
