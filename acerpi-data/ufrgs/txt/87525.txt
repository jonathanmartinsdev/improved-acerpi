Documento gerado sob autenticação Nº UZX.844.291.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1975                  de  27/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a ROZI MARA MENDES,
matrícula SIAPE nº 1038003, no cargo de Assistente em Administração, nível de classificação D, nível de
capacitação IV,  padrão 15,  do Quadro desta  Universidade,  no regime de quarenta  horas  semanais  de
trabalho,  com  exercício  no  Setor  Acadêmico  da  Faculdade  de  Biblioteconomia  e  Comunicação,  com
proventos integrais. Processo 23078.528643/2018-52.
RUI VICENTE OPPERMANN
Reitor.
