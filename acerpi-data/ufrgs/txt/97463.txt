Documento gerado sob autenticação Nº SUK.029.438.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7959                  de  02/09/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  PATRICIA  ALEJANDRA  BEHAR,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Estudos Especializados da Faculdade de Educação, com
a finalidade de participar da "11th International Conference on Knowledge Management and Information
Systems (KMIS 2019)", em Viena, Áustria e realizar visita à University Of West Bohemia, em Plzen , Repúbica
Tcheca, no período compreendido entre 15/09/2019 e 25/09/2019, incluído trânsito, com ônus limitado.
Solicitação nº 86134.
RUI VICENTE OPPERMANN
Reitor
