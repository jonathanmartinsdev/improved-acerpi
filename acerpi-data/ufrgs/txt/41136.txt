Documento gerado sob autenticação Nº LIG.676.198.7L6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6360                  de  17/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento do País  de  MENDELI  HENNING VAINSTEIN,  Professor  do Magistério
Superior, lotado e em exercício no Departamento de Física do Instituto de Física, com a finalidade de realizar
Estágio Sênior, junto ao Massachusetts Institute of Technology, em Cambridge - Estados Unidos da América,
no  período  compreendido  entre  01/10/2017  e  30/09/2018,  com  ônus  CAPES.  Processo  nº
23078.507026/2017-32.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
