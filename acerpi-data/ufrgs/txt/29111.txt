Documento gerado sob autenticação Nº PLH.911.697.F6G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8164                  de  11/10/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de HUGO FRIDOLINO MULLER NETO,  Professor do Magistério
Superior, lotado no Departamento de Ciências Administrativas da Escola de Administração e com exercício na
Escola de Administração, com a finalidade de realizar  visita  à Universidade de Lisboa, Portugal, no período
compreendido entre 13/11/2016 e 25/11/2016, incluído trânsito, com ônus UFRGS (Escola de Administração -
diárias e passagens). Solicitação nº 22408.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
