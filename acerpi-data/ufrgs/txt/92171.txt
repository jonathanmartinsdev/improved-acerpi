Documento gerado sob autenticação Nº VWT.976.446.JCF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4744                  de  03/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de MARCELO FAVARO BORGES, Professor do Magistério Superior,
lotado e  em exercício  no  Departamento  de  Metalurgia  da  Escola  de  Engenharia,  com a  finalidade de
participar da "38th International  Conference on Ocean,  Offshore and Arctic  Engineering (OMAE19)",  em
Glasgow, Escócia e participar de conferência junto à The Open University, em Milton Keynes, Inglaterra, no
período compreendido entre 08/06/2019 e 23/06/2019, incluído trânsito, com ônus limitado. Solicitação nº
84698.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
