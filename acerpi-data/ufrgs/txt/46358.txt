Documento gerado sob autenticação Nº TQR.956.330.TER, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9910                  de  26/10/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial  da União do dia 6 subsequente, a ROSINHA DA SILVA
MACHADO CARRION, matrícula SIAPE nº 0356835, no cargo de Professor Titular da Carreira do Magistério
Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no Departamento
de  Ciências  Administrativas  da  Escola  de  Administração,  com  proventos  integrais.  Processo
23078.019290/2017-13.
RUI VICENTE OPPERMANN
Reitor.
