Documento gerado sob autenticação Nº BEA.067.475.M6R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3002                  de  05/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, LUCAS CASAGRANDE, em virtude de habilitação em Concurso Público
de Provas e Títulos, conforme Edital Nº 19/2017 de 13/03/2017, homologado em 14/03/2017, e de acordo
com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de 28 de dezembro de
2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário Oficial da União de
25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do Plano de Carreiras e
Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro de Pessoal desta
Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento Interdisciplinar do
Campus Litoral Norte, em vaga decorrente de aposentadoria da professora Rosimeri de Fátima Carvalho da
Silva, código nº 273299, ocorrida em 08/03/2019, conforme Portaria nº. 2.101 de 06/03/2019, publicada no
Diário Oficial da União de 08/03/2019. Processo nº. 23078.506292/2017-48.
RUI VICENTE OPPERMANN
Reitor.
