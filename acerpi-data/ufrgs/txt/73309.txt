Documento gerado sob autenticação Nº GEX.896.304.QC5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9961                  de  10/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  CAROLINE  SOARES  DE  ABREU,  Professor  do  Magistério
Superior,  lotada  no  Departamento  de  Música  do  Instituto  de  Artes  e  com  exercício  na  Comissão  de
Graduação de Música, com a finalidade de participar do "Tercer Congreso Chileno de Estudios en Música
Popular", em Santiago, Chile, no período compreendido entre 08/01/2019 e 12/01/2019, incluído trânsito,
com ônus limitado. Solicitação nº 61118.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
