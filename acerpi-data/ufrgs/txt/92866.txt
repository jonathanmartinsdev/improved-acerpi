Documento gerado sob autenticação Nº XCP.314.508.B5O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5186                  de  18/06/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5033346-78.2019.4.04.7100,  da  1ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, do servidor EDSON ALF, matrícula SIAPE n° 0352059, aposentado no cargo de Vigilante - 701269,
do nível III para o nível IV, a contar de 01/01/2006, conforme o Processo nº 23078.515301/2019-53.
RUI VICENTE OPPERMANN
Reitor
