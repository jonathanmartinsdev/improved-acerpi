Documento gerado sob autenticação Nº EMX.577.146.MGQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6713                  de  28/08/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Retificar  a  Portaria  n°  1025/2018,  de 02/02/2018,  que concede afastamento parcial  à  JULIANI
MENEZES  DOS  REIS,  Bibliotecário-documentalista,  com  exercício  na  Biblioteca  de  Ciências  Sociais  e
Humanidades do Instituto de Filosofia e Ciências Humanas, conforme Processo nº 23078.500844/2018-95.
 
Onde se lê:
"no período de 12/03/2018 a 11/03/2019",
leia-se:
"no período de 12/03/2018 a 19/07/2018".
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
