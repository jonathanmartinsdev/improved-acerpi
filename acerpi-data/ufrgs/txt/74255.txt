Documento gerado sob autenticação Nº YUV.802.664.MHQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             106                  de  03/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  26/12/2018,   referente  ao  interstício  de
26/06/2017 a 25/12/2018, para a servidora SHEILA IRRIBAREM DE MELLO BOTT,  ocupante do cargo de
Bibliotecário-documentalista - 701010, matrícula SIAPE 1083070,  lotada  no  Instituto de Artes, passando
do Nível de Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.535546/2018-16:
Formação Integral de Servidores da UFRGS V CH: 121 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 1 hora(s) (07/08/2017 a 04/06/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
