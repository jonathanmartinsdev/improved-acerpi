Documento gerado sob autenticação Nº KPC.807.150.7F9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6944                  de  02/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°56753,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO EM EDIFICAÇÕES, do
Quadro de Pessoal desta Universidade, RODRIGO NOBRE PIRES (Siape: 1869218 ),  para substituir   PAULO
HENRIQUE DOS SANTOS PACHECO (Siape: 1872628 ), Prefeito do Campus do Vale da Vice-Superintendêcia de
Manutenção da SUINFRA, Código CD-4, em seu afastamento por motivo de Laudo Médico do titular da
Função, no período de 27/07/2019 a 24/09/2019, com o decorrente pagamento das vantagens por 60 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
