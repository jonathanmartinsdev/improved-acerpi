Documento gerado sob autenticação Nº QQB.594.692.CUI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             301                  de  12/01/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°27063,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS,  do  Quadro  de  Pessoal  desta  Universidade,  FLAVIA  RENATA  PINTO  BARBOSA  (Siape:
1756336 ),  para substituir   CLAUDETE LAMPERT GRUGINSKIE (Siape: 1756939 ), Coordenador do Núcleo de
Regulação da Secretaria de Avaliação Institucional, Código FG-2, em seu afastamento por motivo de férias, no
período de 09/01/2017 a 20/01/2017, com o decorrente pagamento das vantagens por 12 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
