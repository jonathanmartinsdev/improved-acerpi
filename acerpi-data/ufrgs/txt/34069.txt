Documento gerado sob autenticação Nº SNL.482.798.8VA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1285                  de  10/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Alterar o exercício, a partir de 7 de fevereiro de 2017, de JULIA LÂNGARO BECKER, matrícula SIAPE nº
1757206, ocupante do cargo de Psicólogo-área, da Secretaria do Programa de Pós-Graduação em Saúde
Coletiva da Escola de Enfermagem para o Centro Interdisciplinar de Pesquisa e Atenção à Saúde do Instituto
de Psicologia. Processo n° 23078.001491/2017-64.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
