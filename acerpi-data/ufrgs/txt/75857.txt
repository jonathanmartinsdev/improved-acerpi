Documento gerado sob autenticação Nº UIP.154.962.436, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             994                  de  29/01/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  SHEILA  CRISTINA  OURIQUES  MARTINS,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Medicina Interna da Faculdade de Medicina,
com a finalidade de participar da "International Stroke Conference 2019", em Honolulu, Estados Unidos, no
período compreendido entre 05/02/2019 e 08/02/2019, incluído trânsito, com ônus limitado. Solicitação nº
62047.
RUI VICENTE OPPERMANN
Reitor
