Documento gerado sob autenticação Nº SOJ.393.808.MGK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8469                  de  22/10/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Designar  o  ocupante  do  cargo  de  Professor  do  Magistério  Superior  EDUARDO  GUIMARAES
BARBOZA,  matrícula  SIAPE  2437848,  para  exercer,  em  caráter  'pró-tempore',  a  função  de  Diretor  do
CECLIMAR, Código SRH 157, Código FG-1,  com vigência a partir  da data de 01/11/2018, sem prejuízo e
cumulativamente  com a  função de Coordenador  da  COMGRAD do Curso de Bacharelado em Ciências
Biológicas - ênfase em Biologia Marinha e Costeira e Gestão Ambiental, Marinha e Costeira, código SRH 1185,
código FUC, até 20/05/2019.
Processo nº 23078.525738/2018-14.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
