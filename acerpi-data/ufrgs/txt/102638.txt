Documento gerado sob autenticação Nº CSH.200.659.47M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11363                  de  20/12/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  VLADIMIR  PINHEIRO  DO  NASCIMENTO,  Professor  do
Magistério  Superior,  lotado no Departamento  de  Medicina  Animal  da  Faculdade de  Veterinária  e  com
exercício na Pró-Reitoria de Graduação, com a finalidade de participar da "2020 International Production and
Processing  Expo  (IPPE)",  em  Atlanta,  Estados  Unidos,  no  período  compreendido  entre  25/01/2020  e
31/01/2020, incluído trânsito, com ônus limitado. Solicitação nº 89469.
RUI VICENTE OPPERMANN
Reitor
