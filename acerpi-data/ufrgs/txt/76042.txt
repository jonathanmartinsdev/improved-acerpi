Documento gerado sob autenticação Nº YKT.591.507.F0N, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1096                  de  31/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 30 (trinta) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de 11
de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para o servidor PEDRO
LUIS OLIVEIRA VAGNER, com exercício no Setor de Informática da FABICO, a ser usufruída no período de
02/03/2019 a 31/03/2019, referente ao quinquênio de 06/07/2011 a 05/07/2016, a fim de elaborar o Trabalho
de  Conclusão  do  Curso  de  Especialização  em Segurança  Cibernética,  oferecido  pela  UFRGS;  conforme
Processo nº 23078.501027/2019-35.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
