Documento gerado sob autenticação Nº HXA.845.809.OBH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11138                  de  12/12/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/12/2017,   referente  ao  interstício  de
01/03/2005 a 11/12/2017, para o servidor VALMIR OLIVEIRA DOS SANTOS, ocupante do cargo de Servente
de Obras - 701824, matrícula SIAPE 0357883,  lotado  na  Pró-Reitoria de Planejamento e Administração,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  A  I,  para  o  Nível  de  Classificação/Nível  de
Capacitação  A  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.523576/2017-07:
Formação Integral  de Servidores da UFRGS I  CH: 30 Carga horária utilizada: 20 hora(s)  /  Carga horária
excedente: 10 hora(s) (08/09/2016 a 24/11/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
