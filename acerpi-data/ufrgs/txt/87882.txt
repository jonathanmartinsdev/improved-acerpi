Documento gerado sob autenticação Nº IQU.966.420.GLG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2166                  de  08/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  06/03/2019,   referente  ao  interstício  de
01/09/2017 a 05/03/2019, para a servidora MARISTER DE OLIVEIRA CASTANHO,  ocupante do cargo de
Assistente em Administração - 701200, matrícula SIAPE 2418157,  lotada  no  Instituto de Ciências Básicas
da Saúde, passando do Nível de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de
Capacitação  D  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.504998/2019-37:
Formação Integral de Servidores da UFRGS II CH: 52 (08/11/2017 a 14/06/2018)
ILB - Conhecendo o novo acordo ortográfico CH: 20 (26/09/2017 a 16/10/2017)
ILB - Deveres, proibições e responsabilidades do servidor público federal CH: 60 Carga horária utilizada: 18
hora(s) / Carga horária excedente: 42 hora(s) (08/06/2018 a 08/08/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
