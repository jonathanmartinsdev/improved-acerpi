Documento gerado sob autenticação Nº EVW.306.500.BPQ, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3465                  de  10/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MARISA  RIBEIRO  DE  ITAPEMA  CARDOSO,  Professor  do
Magistério  Superior,  lotada  e  em  exercício  no  Departamento  de  Medicina  Veterinária  Preventiva  da
Faculdade  de  Veterinária,  com a  finalidade  de  participar  do  "International  Symposium Salmonella  and
Salmonellosis", em Saint Malo, França, no período compreendido entre 04/06/2016 e 10/06/2016, incluído
trânsito, com ônus limitado. Solicitação nº 19602.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
