Documento gerado sob autenticação Nº WPZ.367.055.GS5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2682                  de  10/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder o Adicional de Periculosidade, no percentual de 10%, a partir de 02/01/2018, ao servidor
RICARDO IANZEN GOMES, Identificação Única 22622063, Operador de Caldeira, com exercício no Centro
Olímpico, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270,
de 17 de dezembro de 1991, por exercer atividades em áreas consideradas Perigosas conforme Laudo
Pericial  constante  no  Processo  n  º  23078.503775/2018-71,  Código  SRH  n°  23475  e  Código  SIAPE  n°
2018001348.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
