Documento gerado sob autenticação Nº MUB.949.754.KDE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1912                  de  07/03/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar PAULO ANDRE NIEDERLE, matrícula SIAPE n° 1846785, ocupante do cargo de Professor
do Magistério Superior, classe Adjunto, lotado no Departamento de Sociologia do Instituto de Filosofia e
Ciências Humanas, do Quadro de Pessoal da Universidade, para exercer a função de Coordenador Substituto
do PPG em Sociologia, com vigência a partir de 04/04/2018 até 20/05/2019, a fim de completar o mandato da
Professora Lorena Cândido Fleury, conforme artigo 92 do Estatuto da mesma Universidade. Processo nº
23078.503320/2018-56.
JANE FRAGA TUTIKIAN
Vice-Reitora.
