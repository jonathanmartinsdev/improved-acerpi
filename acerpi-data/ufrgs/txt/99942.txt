Documento gerado sob autenticação Nº IMX.347.495.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9648                  de  23/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar, por decisão judicial proferida no processo nº 5075851-60.2014.4.04.7100, a Portaria n°
2517/2017, de 22/03/2017, que concedeu promoção funcional por obtenção do título de Doutor, no Quadro
desta Universidade, ao Professor RAFAEL STIELER, com exercício no Departamento de Química Inorgânica
do Instituto de Química, da classe A, de Professor Adjunto, nível 01, para a classe C, de Professor Adjunto,
nível 01, com vigência financeira a partir da data de publicação da portaria. Processo nº 23078.528753/2019-
03.
 
 
Onde se lê:
... com vigência financeira a partir da data de publicação da portaria, ...
leia-se:
... com vigência financeira a partir de 28/01/2014, ..., ficando ratificados os demais termos.
RUI VICENTE OPPERMANN
Reitor.
