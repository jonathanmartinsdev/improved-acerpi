Documento gerado sob autenticação Nº UGT.566.620.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3650                  de  29/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5019221-08.2019.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, da servidora MARLETI REGINA DA SILVA LOCK, matrícula SIAPE n° 0354277, aposentada no
cargo  de   Assistente  em Administração  -  701200,  do  nível  I  para  o  nível  IV,  conforme o  Processo  nº
23078.510185/2019-86.
Tornar sem efeito a(s) mportaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
RUI VICENTE OPPERMANN
Reitor
