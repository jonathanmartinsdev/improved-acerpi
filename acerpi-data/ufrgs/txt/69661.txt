Documento gerado sob autenticação Nº MXK.951.903.5EG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7275                  de  13/09/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 22 de agosto de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
LETÍCIA CORRÊA BITENCOURT BIANCHI, ocupante do cargo de Técnico em Assuntos Educacionais, Ambiente
Organizacional Administrativo, Código 701079, Classe E, Nível de Capacitação III, Padrão de Vencimento 03,
SIAPE nº. 2164156 da Faculdade de Arquitetura para a lotação Faculdade de Educação, com novo exercício na
Biblioteca da Faculdade de Educação.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
