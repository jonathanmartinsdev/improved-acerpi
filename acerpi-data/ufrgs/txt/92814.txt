Documento gerado sob autenticação Nº DJM.932.174.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5140                  de  17/06/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  JONAS  ALEX  MORALES  SAUTE,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a
finalidade de participar da conferência "RARD 2019", em Bogotá, Colômbia, no período compreendido entre
19/06/2019 e 21/06/2019, incluído trânsito, com ônus limitado. Solicitação nº 84922.
RUI VICENTE OPPERMANN
Reitor
