Documento gerado sob autenticação Nº PBK.385.849.04E, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3286                  de  07/05/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°45960,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, CARLOS AUGUSTO GODOI DA SILVA (Siape:
0351485 ),  para substituir   CLAUDIOMAR OVIEDO RIBEIRO (Siape: 0356155 ), Coordenador de Pagamento,
Cadastro e Processos Judiciais do DAP da PROGESP, Código CD-4, em seu afastamento no país, no período de
09/05/2018 a 11/05/2018, com o decorrente pagamento das vantagens por 3 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
