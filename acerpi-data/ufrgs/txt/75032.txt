Documento gerado sob autenticação Nº ZVQ.466.927.PCD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             461                  de  15/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, IAMARA ROSSI BULHOES, matrícula SIAPE n° 1345157, lotada no Departamento
Interdisciplinar  do  Campus  Litoral  Norte,  como  Coordenadora  Substituta  da  COMGRAD  do  Curso  de
Bacharelado em Engenharia de Serviços do Campus Litoral Norte, com vigência a partir de 15/02/2019 até
15/01/2021. Processo nº 23078.535321/2018-60.
JANE FRAGA TUTIKIAN
Vice-Reitora.
