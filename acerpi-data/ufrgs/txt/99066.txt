Documento gerado sob autenticação Nº HFU.749.381.MQI, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9029                  de  07/10/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito a Portaria nº de 8687 de 25 de setembro de 2019, publicada no Diário Oficial da
União de 26 de setembro de 2019, que nomeou, em caráter efetivo, ANA ELISA BRUM DE OLIVEIRA, para o
cargo de ASSISTENTE EM ADMINISTRAÇÃO e de acordo com o disposto no artigo 15 da Portaria nº 450, de 06
de novembro de 2002 - MPOG. Processo nº 23078.501687/2018-35.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
