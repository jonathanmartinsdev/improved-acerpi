Documento gerado sob autenticação Nº CSM.476.844.QIB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             342                  de  12/01/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  21/12/2016,   referente  ao  interstício  de
01/03/2005 a 20/12/2016, para o servidor RENATO LIMA DA SILVA,  ocupante do cargo de Auxiliar de
Eletricista - 701616, matrícula SIAPE 0358124,  lotado  na  Superintendência de Infraestrutura, passando do
Nível de Classificação/Nível de Capacitação B I, para o Nível de Classificação/Nível de Capacitação B II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.025986/2016-06:
Formação Integral de Servidores da UFRGS II  CH: 60 Carga horária utilizada: 40 hora(s) /  Carga horária
excedente: 20 hora(s) (08/09/2016 a 25/11/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
