Documento gerado sob autenticação Nº QYK.165.967.T77, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6807                  de  30/08/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de DANIEL EDUARDO WEIBEL, Professor do Magistério Superior,
lotado e em exercício no Departamento de Físico-Química do Instituto de Química, com a finalidade de
participar da "Escuela Dispositivos fotovoltaicos y opto-electrónicos. Diseño y desarrollo de tecnologías limpias y
sustentables.  Aplicación y Impacto social", em Córdoba, Argentina, no período compreendido entre 30/09/2018
e 07/10/2018, incluído trânsito, com ônus limitado. Solicitação nº 58750.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
