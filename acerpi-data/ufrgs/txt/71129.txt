Documento gerado sob autenticação Nº SLP.889.198.MGK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8452                  de  22/10/2018
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de LUIZ EMILIO ALLEM, Professor do Magistério Superior, lotado e
em exercício no Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística, com a
finalidade  de  realizar  visita  à  Universidad  Nacional  de  San  Luis,  em  San  Luis,  Argentina,  no  período
compreendido entre 13/11/2018 e 01/12/2018, incluído trânsito, com ônus limitado. Solicitação nº 59750.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
