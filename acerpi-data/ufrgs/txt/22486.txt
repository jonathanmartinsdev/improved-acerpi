Documento gerado sob autenticação Nº UDH.256.734.E3V, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3773                  de  20/05/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear,  em caráter efetivo,  RENE FAUSTINO GABRIEL JUNIOR,  em virtude de habilitação em
Concurso Público de Provas e Títulos,  conforme Edital  Nº 42/16 de 29 de abril  de 2016 ,  homologado
em 02 de maio de 2016 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990
e Lei 12.772, de 28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013,
publicada no Diário Oficial da União de 25 de setembro de 2013, para o cargo de Professor do Magistério
Superior do Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I,
do Quadro de Pessoal desta Universidade, em regime de trabalho de DE, junto ao Departamento de Ciências
da Informação da Faculdade de Biblioteconomia e Comunicação, em vaga decorrente da Aposentadoria
Compulsória da Professora Glória Isabel Sattamini Ferreira, código nº 275980, ocorrida em 01 de junho de
2015, conforme Portaria nº. 4866 de 19 de junho de 2015, publicada no Diário Oficial da União de 24 de
junho de 2015. Processo nº. 23078.023935/2015-51.
CARLOS ALEXANDRE NETTO
Reitor
