Documento gerado sob autenticação Nº RAN.177.182.E6V, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8228                  de  11/10/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora SANDRA MARIA GONÇALVES VIEIRA,  matrícula SIAPE n° 1268431, lotada e em exercício no
Departamento de Pediatria da Faculdade de Medicina, da classe   de Professor Adjunto, nível 01, para a
classe   de Professor Adjunto, nível 02, referente ao interstício de 23/08/2010 a 22/08/2012, com vigência
financeira a partir de 03/09/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.522342/2018-15.
JANE FRAGA TUTIKIAN
Vice-Reitora.
