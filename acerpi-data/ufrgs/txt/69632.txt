Documento gerado sob autenticação Nº MSN.915.486.1T5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7245                  de  12/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LÉLIO ANTÔNIO TEIXEIRA BRITO,  Professor do Magistério
Superior,  lotado e em exercício no Departamento de Engenharia Civil  da Escola de Engenharia,  com a
finalidade de participar da conferência ?Rubberized Asphalt Rubber, 2018?, em Johanesburgo, República Da
África Do Sul, no período compreendido entre 24/09/2018 e 30/09/2018, incluído trânsito, com ônus limitado.
Solicitação nº 58917.
RUI VICENTE OPPERMANN
Reitor
