Documento gerado sob autenticação Nº POL.278.776.GD0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6495                  de  23/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor DANILO KNIJNIK, matrícula SIAPE n° 2174607, lotado no Departamento de Ciências Penais da
Faculdade de Direito e com exercício na Faculdade de Direito, da classe D  de Professor Associado, nível 01,
para a classe D  de Professor Associado, nível 02, referente ao interstício de 25/02/2017 a 24/02/2019, com
vigência financeira a partir de 25/02/2019, conforme decisão judicial proferida no processo nº 5054491-
30.2018.4.04.7100, da 1ª Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de
dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.509899/2019-41.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
