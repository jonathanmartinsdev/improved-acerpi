Documento gerado sob autenticação Nº SPB.123.760.DRR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2643                  de  27/03/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito a Portaria nº 2416/2017, de 20/03/2017, publicada no Diário Oficial da União de
21/03/2017, que nomeou em caráter efetivo, LUIS CARLOS ROCHA HENNIG, para o cargo de Auxiliar em
Administração,  de  acordo  com  o  disposto  no  artigo  15  da  Portaria  nº  450,  de  06  de  novembro  de
2002. Processo nº 23078.020126/13-81.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
