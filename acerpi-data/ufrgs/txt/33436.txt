Documento gerado sob autenticação Nº OLC.143.330.RIQ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             739                  de  24/01/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, e conforme a Solicitação de Férias n°33948,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, JOSE ROBERTO DUARTE COLCHETE (Siape:
0353310 ),  para substituir   MARCELO UTZ ASCONAVIETA (Siape: 0353834 ), Diretor da Divisão de Licitação do
DELIT da PROPLAN, Código FG-4, em seu afastamento por motivo de férias, no período de 23/01/2017 a
27/01/2017, com o decorrente pagamento das vantagens por 5 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
