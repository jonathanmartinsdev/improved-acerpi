Documento gerado sob autenticação Nº LRV.312.296.5L0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8321                  de  05/09/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005, publicada no Diário Oficial da União do dia 6 subsequente, a NAIR REGINA RITTER
RIBEIRO, matrícula SIAPE nº 1000613, no cargo de Professor Associado, nível 4, da Carreira do Magistério
Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no Departamento
de  Enfermagem  Materno-Infantil  da  Escola  de  Enfermagem,  com  proventos  integrais.  Processo
23078.025527/2016-14.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
