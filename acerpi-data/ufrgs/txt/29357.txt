Documento gerado sob autenticação Nº XND.608.229.MD1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8360                  de  18/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de SERGIO LUIZ VIEIRA, Professor do Magistério Superior, lotado e
em exercício no Departamento de Zootecnia da Faculdade de Agronomia, com a finalidade de participar do
"3º International Phytase Summit", em Miami, Estados Unidos, no período compreendido entre 07/11/2016 e
10/11/2016, incluído trânsito, com ônus limitado. Solicitação nº 23734.
RUI VICENTE OPPERMANN
Reitor
