Documento gerado sob autenticação Nº VZA.089.477.61T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             518                  de  16/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/01/2019,   referente  ao  interstício  de
05/07/2017 a 04/01/2019, para a servidora DAIANA DA SILVA FERREIRA, ocupante do cargo de Técnico em
Higiene Dental - 701241, matrícula SIAPE 2407599,  lotada  na  Faculdade de Odontologia, passando do Nível
de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.535662/2018-35:
Formação Integral de Servidores da UFRGS I CH: 32 (06/11/2017 a 23/05/2018)
TelessaúdeRS - Saúde bucal para pessoas idosas CH: 50 Carga horária utilizada: 18 hora(s) / Carga horária
excedente: 32 hora(s) (23/08/2017 a 14/12/2017)
TelessaúdeRS - Odontopediatria CH: 40 (21/09/2017 a 15/11/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
