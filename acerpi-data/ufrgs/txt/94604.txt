Documento gerado sob autenticação Nº ETY.356.629.GD0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6546                  de  23/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor DENNIS RUSSOWSKY, matrícula SIAPE n° 1216910, lotado e em exercício no Departamento de
Química Orgânica do Instituto de Química, da classe   de Professor Adjunto, nível 02, para a classe   de
Professor Adjunto, nível 03, referente ao interstício de 24/12/2000 a 31/12/2003, com vigência financeira a
partir de 01/01/2004, conforme decisão judicial proferida no processo nº 5054491-30.2018.4.04.7100, da 1ª
Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.510026/2019-81.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
