Documento gerado sob autenticação Nº VJK.341.463.05R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             896                  de  28/01/2020
Nomeação  da  Representação  Discente  dos
órgãos colegiados da Faculdade de Direito da
UFRGS
O PRÓ-REITOR DE ASSUNTOS ESTUDANTIS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL,  no uso de suas atribuições,  considerando o disposto na Portaria nº  7625,  de 29 de
setembro de 2016
RESOLVE
Nomear a Representação Discente eleita na Faculdade de Direito, com mandato de 01 (um) ano, a
contar de 19 de janeiro de 2020, atendendo o disposto nos artigos 175 do Regimento Geral da Universidade
e 79 do Estatuto da Universidade, e considerando o processo nº 23078.500178/2020-18, conforme segue:
 
Conselho da Unidade - CONSUNI           
Titular 01: Kaio Lucas Costa da Silva       
Suplente 01: Juli Karin Arnold   
Titular 02: Stéphanie Venske Estrella    
Suplente 02: Ana Beatriz Rippel Pacheco             
 
Comissão de Graduação - COMGRAD   
Titular 01: João Octávio de Souza Pires 
Titular 02: Marcelo Henrique Lazzarotto              
Titular 03: Diego da Rosa Amaro             
 
DIR01   
Titular 01: Priscila Lima Batista 
Titular 02: Aurea Julia Braga Rodrigues 
Titular 03: Aline Cantos Pires     
 
DIR02   
Titular 01: Maria Alejandra Vivanco Gonçalves 
Titular 02: Fernanda Magni Berthier      
Titular 03: Marceli Tomé Martins            
Titular 04: Gerusa Shaiana Domingues Pena      
Documento gerado sob autenticação Nº VJK.341.463.05R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Titular 05: Brunno Pereira Soares Couto              
Titular 06: Agnes Juliana Steinmetz da Rosa       
Titular 07: Fernando Berwanger Barbosa            
Titular 08: Rafael Jordano Neto Felicio
 
DIR03   
Titular 01: Jade dos Santos Alves             
Titular 02: Enrico de Carpena Ferreira Correa de Barros
Titular 03: João Pedro Juk Rocha             
Titular 04: Gustavo Santos Kafruni          
Titular 05: Diego da Rosa Amaro             
Titular 06: Rafael Jordano Neto Felício  
Titular 07: Vitor Saraiva Martinez            
 
DIR04   
Titular 01: Kaio Lucas Costa da Silva
Titular 02: Julia Escher Severo
Titular 03: Agnes Juliana Jordano de Souza         
Titular 04: Matheus Costa de Souza       
 
Comissão de Pesquisa - COMPESQ        
Titular 01: João Pedro Costa Genro        
 
Comissão de Extensão - COMEX
Titular 01: Maria Alejandra Vivanco Gonçalves
 
COBIBLI
Titular 01: Frederico Paganin Gonçalves              
 
SPPP    
Titular 01: Julia Meirelles da Rosa
 
ELTON LUIS BERNARDI CAMPANARO
Pró-Reitor de Assuntos Estudantis em exercício
