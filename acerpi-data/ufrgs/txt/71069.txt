Documento gerado sob autenticação Nº UTF.803.223.VPC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8389                  de  19/10/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar, a partir de 17/10/2018, o ocupante do cargo de Técnico em Eletricidade - 701272, do
Nível de Classificação DII,  do Quadro de Pessoal desta Universidade, JULIO CESAR PIRES DE OLIVEIRA,
matrícula SIAPE 2146523 da função de Coordenador do Núcleo Técnico-Científico do Campus Litoral Norte,
Código  SRH 1418,  Código  FG-2,  para  a  qual  foi  designado pela  Portaria  nº  4210/2018 de  08/06/2018,
publicada no Diário Oficial da União de 12/06/2018. Processo nº 23078.527817/2018-60.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
