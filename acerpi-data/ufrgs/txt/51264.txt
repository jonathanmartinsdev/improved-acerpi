Documento gerado sob autenticação Nº TGM.703.565.NK5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1653                  de  27/02/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  SILVIA LOPES CARNEIRO LEAO,  Professor  do  Magistério
Superior,  lotada e  em exercício  no Departamento de Arquitetura  da  Faculdade de Arquitetura,  com a
finalidade  de  participar  do  "Encuentro  Internacional  Patrimonio  Moderno  y  Buenas  Prácticas:
Sustentabilidad,  Conservación,  Gestión y Proyecto",  em Santiago,  Chile,  no período compreendido entre
20/03/2018 e 25/03/2018, incluído trânsito, com ônus limitado. Solicitação nº 34079.
RUI VICENTE OPPERMANN
Reitor
