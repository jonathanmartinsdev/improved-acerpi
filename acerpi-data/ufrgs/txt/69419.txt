Documento gerado sob autenticação Nº DPN.942.780.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7144                  de  10/09/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor SILVIO BUCHNER, matrícula SIAPE n° 1542880, lotado e em exercício no Departamento de Física
do Instituto de Física, da classe A  de Professor Adjunto A, nível 01, para a classe A  de Professor Adjunto A,
nível 02, referente ao interstício de 30/06/2016 a 29/06/2018, com vigência financeira a partir de 29/08/2018,
de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº
331/2017 do CONSUN. Processo nº 23078.515583/2018-16.
RUI VICENTE OPPERMANN
Reitor.
