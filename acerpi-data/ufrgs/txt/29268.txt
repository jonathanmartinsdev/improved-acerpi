Documento gerado sob autenticação Nº HVK.523.362.MD1, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8331                  de  18/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de DALVA CARMEM TONATO, Professor do Magistério Superior,
lotada e em exercício no Departamento de Direito Privado e Processo Civil da Faculdade de Direito, com a
finalidade de participar do "VI Encuentro del Grupo para la Armonización del Derecho en América Latina", em
Lima, Peru, no período compreendido entre 13/11/2016 e 17/11/2016, incluído trânsito, com ônus limitado.
Solicitação nº 23172.
RUI VICENTE OPPERMANN
Reitor
