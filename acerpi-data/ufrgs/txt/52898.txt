Documento gerado sob autenticação Nº JUD.106.828.U49, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2543                  de  07/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar na Secretaria de Educação a Distância, com exercício na Coordenação Acadêmica da SEAD,
ANELISE BUENO AMBROSINI, nomeada conforme Portaria Nº 6875/2017 de 28 de julho de 2017, publicada
no Diário Oficial da União no dia 31 de julho de 2017, em efetivo exercício desde 05 de abril de 2018,
ocupante  do  cargo  de  TÉCNICO  EM  ASSUNTOS  EDUCACIONAIS,  Ambiente  Organizacional
Administrativo,  classe  E,  nível  I,  padrão  101,  no  Quadro  de  Pessoal  desta  Universidade.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
