Documento gerado sob autenticação Nº ZMF.697.391.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6364                  de  16/08/2018
       O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias,  e  tendo  em  vista  a  Decisão  CONSUN  nº  251/2018  e  o  constante  no  Processo  SEI  n.º
23078.516817/2016-72 72.
 
 
RESOLVE
Tornar insubsistente a Portaria nº 4674/2018, de 29/06/2018,  que exonerou  GUSTAVO DORNELES
FERREIRA, ocupante do cargo de Professor do Magistério Superior, lotado e em exercício no Departamento
de Engenharia Elétrica da Escola de Engenharia.
RUI VICENTE OPPERMANN,
Reitor.
