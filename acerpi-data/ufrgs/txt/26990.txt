Documento gerado sob autenticação Nº FAE.115.667.8O5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6739                  de  31/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°30763,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de JORNALISTA, do Quadro de
Pessoal desta Universidade, PATRICIA BARRETO DOS SANTOS LIMA (Siape: 1859696 ),  para substituir  
EDINA MARIA DA ROCHA FERREIRA (Siape: 0352039 ), Vice-Secretário de Comunicação Social, vinculado à
Secretaria de Comunicação Social, Código CD-4, em seu afastamento por motivo de férias, no período de
08/08/2016 a 27/08/2016, com o decorrente pagamento das vantagens por 20 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
