Documento gerado sob autenticação Nº BXC.865.348.NPK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3818                  de  03/05/2019
     O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso
da competência que lhe foi delegada pela portaria n.º 3440 de 22 de abril de 2019, do Magnífico Reitor,
RESOLVE:
Art. 1º - Tornar sem efeito Portaria nº 2972 de 04 de abril de 2019.
Art. 2º - Subdelegar competência a CAMILA SIMONETTI, Vice-Superintendente de Infraestrutura, para, a partir
de 28 de março de 2019,  sem prejuízo  e  cumulativamente com as  demais  funções  que exerce  nesta
Universidade, praticar os atos a seguir enumerados:
I- Ordenar despesas, exceto as de pessoal, pertinentes à Superintendência de Infraestrutura;
II  -  autorizar  a execução de obras de conservação,  reformas,  adaptações e ampliação da planta física;
inclusive as intervenções relacionadas aos prédios históricos e educação patrimonial;
IV - coordenar e autorizar todas as ações relacionadas à proteção do meio ambiente e sustentabilidade, com
exceção das atividades de educação ambiental;
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
