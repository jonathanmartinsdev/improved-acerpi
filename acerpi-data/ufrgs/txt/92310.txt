Documento gerado sob autenticação Nº LCY.737.255.7VN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4813                  de  05/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de ROBERTO GIUGLIANI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Genética do Instituto de Biociências,  com a finalidade de proferir
palestra na conferência "Recent advances in rare diseases - Frequently misdiagnosed hereditary disorders
(FREMIDS)",  em  Bogotá,  Colômbia,  no  período  compreendido  entre  20/06/2019  e  21/06/2019,  incluído
trânsito, com ônus limitado. Solicitação nº 84770.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
