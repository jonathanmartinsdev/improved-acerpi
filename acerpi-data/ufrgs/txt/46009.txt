Documento gerado sob autenticação Nº IKL.015.728.MM6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9820                  de  24/10/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Conceder  à  servidora  POLIANA  SANCHEZ  DE  ARAUJO,  ocupante  do  cargo  de   Bibliotecário-
documentalista - 701010, lotada no Instituto de Filosofia e Ciências Humanas, SIAPE 2425300, o percentual de
30% (trinta por cento) de Incentivo à Qualificação, a contar de 21/09/2017, tendo em vista a conclusão do
curso de Especialização em Gestão de Pessoas, conforme o Processo nº 23078.516244/2017-68.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
