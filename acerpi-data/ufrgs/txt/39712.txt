Documento gerado sob autenticação Nº CZF.469.039.0OA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5353                  de  21/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, EVANDRO MANICA, matrícula SIAPE n° 3320717, lotado no Departamento de
Matemática Pura e Aplicada do Instituto de Matemática e Estatística, como Chefe Substituto do Depto de
Matemática Pura e Aplicada do Instituto de Matemática, para substituir automaticamente o titular desta
função  em  seus  afastamentos  ou  impedimentos  regulamentares  no  período  de  30/07/2017  e  até
29/07/2019. Processo nº 23078.201622/2017-10.
JANE FRAGA TUTIKIAN
Vice-Reitora.
