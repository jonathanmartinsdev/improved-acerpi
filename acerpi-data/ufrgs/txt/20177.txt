Documento gerado sob autenticação Nº RIE.842.709.7F2, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2444                  de  04/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469 de 04 de outubro de 2012
RESOLVE:
Retificar a Portaria n° 2231, de 23 de março de 2016, publicada no Diário Oficial da União de 29 de
março de 2016, que concedeu pensão com natureza vitalícia, a partir de 04 de fevereiro de 2016, a NEUSA
ALVES DE SOUZA,
onde se lê:
nos termos dos artigos 215, 217, inciso I, 218, e 222, VII, da Lei nº 8.112/90, com redação dada pela
Lei nº 13.135/2015,
leia-se:
nos termos dos artigos 215, 217, inciso III, 218, e 222, VII, da Lei nº 8.112/90, com redação dada pela
Lei nº 13.135/2015, ficando ratificados os demais termos. Processo nº 23078.002539/2016-71.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
