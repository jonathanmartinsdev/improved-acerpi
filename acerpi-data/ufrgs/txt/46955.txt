Documento gerado sob autenticação Nº TFU.942.228.720, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10350                  de  09/11/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Declarar  vaga,  a  partir  de  06/11/2017,  a  função  de  Coordenadora  do  Núcleo  de  Gestão
Organizacional da Gerência Administrativa da Escola de Educação Física, Fisioterapia e Dança, Código SRH
440,  Código  FG-5,  desta  Universidade,  tendo em vista  a  aposentadoria  de  CARLA SANTOS FERREIRA,
matrícula SIAPE n° 0351479, conforme Portaria n° 10128/2017, de 1 de novembro de 2017, publicada no
Diário Oficial da União do dia 06 de novembro de 2017. Processo nº 23078.520467/2017-20.
JANE FRAGA TUTIKIAN
Vice-Reitora.
