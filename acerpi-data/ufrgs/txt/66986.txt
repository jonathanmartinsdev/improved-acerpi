Documento gerado sob autenticação Nº NGD.089.528.70B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5542                  de  25/07/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Tornar sem efeito a Portaria n° 5511/2018, de 24/07/2018, que concedeu promoção funcional, por
obtenção de título de Doutor, no Quadro desta Universidade, ao Professor MARCELO ABREU DA SILVA, com
exercício no Departamento de Plantas Forrageiras e Agrometeorologia da Faculdade de Agronomia, da classe
D de Professor Associado, nível 04, para a classe E de Professor Titular, com vigência financeira a partir de
27/06/2018. Processo nº 23078.510615/2018-89.
DANILO BLANK
Decano do Conselho Universitário, no exercício da Reitoria.
