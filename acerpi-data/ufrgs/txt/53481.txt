Documento gerado sob autenticação Nº OII.076.317.BBG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3040                  de  25/04/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Conceder  à  servidora  ANA  ELIZE  RIBEIRO  D'  AVILA,  ocupante  do  cargo  de   Técnico  em
Contabilidade - 701224, lotada na Auditoria Interna, SIAPE 3036062, o percentual de 20% (vinte por cento) de
Incentivo à Qualificação, a contar de 19/04/2018, tendo em vista a conclusão do curso de Especialização em
Análises Clínicas Veterinárias, conforme o Processo nº 23078.509090/2018-39.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
