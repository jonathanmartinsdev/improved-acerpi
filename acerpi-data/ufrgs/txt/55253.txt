Documento gerado sob autenticação Nº OEW.588.526.VGG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4248                  de  11/06/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  18/05/2018,
correspondente ao grau Insalubridade Média, à servidora ROBERTA ALVARENGA REIS, Identificação Única
17602807, Professor do Magistério Superior, com exercício no Departamento de Odontologia Preventiva e
Social, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de
17 de dezembro de 1991, por exercer atividades em áreas consideradas Insalubres conforme Laudo Pericial
constante no Processo nº 23078.510408/2018-24, Código SRH n° 23541 e Código SIAPE 26244-000.054/2018.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
