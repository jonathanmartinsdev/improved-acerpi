Documento gerado sob autenticação Nº DMI.643.353.Q6C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2208                  de  11/03/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5002009-52.2011.4.04.7100,  da  2ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006,  do servidor JOAO MOISES RONDAN PEREIRA,  matrícula SIAPE n° 0353054,  ativo no cargo
de Vigilante - 701269, para o nível IV, conforme o Processo nº 23078.504801/2019-60.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
RUI VICENTE OPPERMANN
Reitor
