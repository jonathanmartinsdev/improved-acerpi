Documento gerado sob autenticação Nº OFE.086.491.I39, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             434                  de  14/01/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ERICA ROSALBA MALLMANN DUARTE, Professor do Magistério
Superior,  lotada e em exercício no Departamento de Assistência e Orientação Profissional da Escola de
Enfermagem, com a finalidade de participar do "IV Curso Internacional de Pesquisa-ação Participativa em
Saúde", em Porto, Portugal, no período compreendido entre 26/01/2019 e 11/02/2019, incluído trânsito, com
ônus limitado. Solicitação nº 60994.
RUI VICENTE OPPERMANN
Reitor
