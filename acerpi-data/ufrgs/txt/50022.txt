Documento gerado sob autenticação Nº WQV.673.777.QOU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             805                  de  29/01/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Tornar sem efeito, a nomeação do candidato abaixo relacionado, ocorrida através da Portaria nº.
11.243 de 15 de dezembro de 2017, publicada no Diário Oficial da União de 19 de dezembro de 2017, de
acordo com o que preceitua o § 6º do artigo 13 da Lei nº. 8.112, de 11 de dezembro de 1990, com a redação
dada pela Lei nº. 9.527, de 10 de dezembro de 1997. Processo nº. 23078.025182/2016-07.
 
Cargo 701830 - TÉCNICO EM ELETRÔNICA - Classe D Padrão I
 
THALES EXENBERGER BECKER - Vaga SIAPE 304708
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
