Documento gerado sob autenticação Nº HVM.288.608.EJ0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PRÓ-REITORIA DE GESTÃO DE PESSOAS
Divisão de Ingresso, Mobilidade e Acompanhamento
 Avenida Paulo Gama, 110 - 4° andar - CEP 90040-060 - Porto Alegre - RS
dima@progesp.ufrgs.br - (051) 3308-3149 ou 3308-3609
Ofício nº 197/2020-PROGESP Porto Alegre, 20 de fevereiro de 2020.
Senhora Diretora,
Encaminhamos o servidor MARCELLO DA ROCHA MACARTHY,  ocupante do cargo Professor do
Magistério Superior, para exercício nessa Unidade no dia 21 de fevereiro de 2020.
Para confecção da Portaria de Lotação, solicitamos que nos sejam encaminhadas, em até 03 dias, as
seguintes informações:
- lotação e exercício (Departamento), observando a hierarquia dos órgãos registrados no SRH;
- área de atuação do servidor docente;
- nome da chefia imediata, devidamente designada e registrada no SRH, e respectivo ramal;
- data de efetiva apresentação do servidor na Unidade.
ZILMARA BONAI
Diretora da Divisão de Ingresso, Mobilidade e Acompanhamento do DDGP da PROGESP
Ilma. Sra.
Professora LUCIANA PORCHER NEDEL,
Diretora do Instituto de Informática, em exercício,
N/Universidade.
