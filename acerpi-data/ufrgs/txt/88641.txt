Documento gerado sob autenticação Nº LFX.751.940.4SI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2561                  de  21/03/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal desta Universidade, MARCIO LAZZAROTTO, matrícula SIAPE n° 1205860, lotado no Departamento de
Química Orgânica do Instituto de Química,  como Coordenador Substituto da Comissão de Extensão do
Instituto de Química,  para substituir  automaticamente o titular desta função em seus afastamentos ou
impedimentos regulamentares na vigência do presente mandato. Processo nº 23078.506354/2019-83.
JANE FRAGA TUTIKIAN
Vice-Reitora.
