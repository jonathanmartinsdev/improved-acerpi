Documento gerado sob autenticação Nº OBN.859.428.DI6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10937                  de  04/12/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ESTELA BEATRIZ BEHLING, matrícula SIAPE n° 1740475, lotada e em exercício no Departamento
de Nutrição da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 04, para a classe D  de
Professor Associado, nível 01, referente ao interstício de 18/11/2015 a 17/11/2017, com vigência financeira a
partir de 18/11/2017, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações e  a  Decisão nº  197/2006-CONSUN,  alterada pela  Decisão nº  401/2013-CONSUN. Processo nº
23078.519768/2017-19.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
