Documento gerado sob autenticação Nº EPW.617.185.C0M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4990                  de  06/06/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  THAISA  STORCHI  BERGMANN,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Astronomia do Instituto de Física, com a finalidade de
participar de conferência da Atacama Large Millimeter Array, em Antuérpia, Bélgica e da  European Week of
Astronomyand Space Science, em Praga, República Tcheca, no período compreendido entre 16/06/2017 e
29/06/2017, incluído trânsito, com ônus limitado. Solicitação nº 28598.
RUI VICENTE OPPERMANN
Reitor
