Documento gerado sob autenticação Nº IUT.136.937.K7K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4247                  de  15/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder promoção funcional, por obtenção do título de Doutor, no Quadro desta Universidade, ao
Professor JEFERSON VIEIRA FLORES, matrícula SIAPE 2122219, lotado e em exercício no Departamento de
Engenharia Elétrica da Escola de Engenharia, da classe A  de Professor Adjunto A, nível 02, para a classe C  de
Professor Adjunto, nível 01, com vigência financeira a partir da data de publicação da portaria, de acordo com
o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e a Resolução nº 12/1995-
COCEP, alterada pela Decisão nº 401/2013-CONSUN. Processo nº 23078.505642/2017-59.
JANE FRAGA TUTIKIAN
Vice-Reitora.
