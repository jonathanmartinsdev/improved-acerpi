Documento gerado sob autenticação Nº JZS.023.837.U3S, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3370                  de  20/04/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de RAFAEL ROESLER, Professor do Magistério Superior, lotado e
em exercício no Departamento de Farmacologia do Instituto de Ciências Básicas da Saúde, com a finalidade
de realizar visita  à University of Nottingham, Inglaterra,  no período compreendido entre 12/05/2017 e
20/05/2017, incluído trânsito, com ônus limitado. Solicitação nº 27370.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
