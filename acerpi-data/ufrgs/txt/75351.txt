Documento gerado sob autenticação Nº QHJ.339.909.606, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             616                  de  18/01/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir de 02/03/2019, a ocupante do cargo de Professor do Magistério Superior, classe
Associado, do Quadro de Pessoal desta Universidade, FLAVIA GOMES MARTINEZ, matrícula SIAPE n° 1723684,
da função de Diretora Substituta do Centro Olímpico, para a qual foi designada pela Portaria 2578/2017, de
24/03/2017. Processo nº 23078.501337/2019-50.
JANE FRAGA TUTIKIAN
Vice-Reitora.
