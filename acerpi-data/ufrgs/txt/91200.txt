Documento gerado sob autenticação Nº HDO.654.759.TVK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4160                  de  14/05/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  MARIA  ELISA  CALCAGNOTTO,  matrícula  SIAPE  n°  2079920,  lotada  no
Departamento  de  Bioquímica  do  Instituto  de  Ciências  Básicas  da  Saúde,  para  exercer  a  função  de
Coordenadora do PPG em Ciências Biológicas: Neurociências, Código SRH 1144, código FUC, com vigência a
partir de 13/06/2019 até 12/06/2021. Processo nº 23078.512129/2019-86.
JANE FRAGA TUTIKIAN
Vice-Reitora.
