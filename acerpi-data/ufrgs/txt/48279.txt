Documento gerado sob autenticação Nº HZM.192.637.U1M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11310                  de  20/12/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar IGNACIO ITURRIOZ,  matrícula SIAPE n° 1313146, ocupante do cargo de Professor do
Magistério  Superior,  classe  Titular,  lotado  no  Departamento  de  Engenharia  Mecânica  da  Escola  de
Engenharia, do Quadro de Pessoal da Universidade, para exercer a função de Coordenador Substituto da
COMGRAD de Engenharia Mecânica, com vigência a partir de 20 de dezembro de 2017 até 15 de maio de
2019, a fim de completar o mandato do Professor JUAN PABLO RAGGIO QUINTAS, conforme artigo 92 do
Estatuto da mesma Universidade. Processo nº 23078.523290/2017-13.
JANE FRAGA TUTIKIAN
Vice-Reitora.
