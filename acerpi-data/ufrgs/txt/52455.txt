Documento gerado sob autenticação Nº PYO.681.192.ED4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2332                  de  27/03/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARIA LUISA OLIVEIRA DA CUNHA, Professor do Magistério
Superior,  lotada  e  em  exercício  no  Departamento  de  Educação  Física  da  Escola  de  Educação  Física,
Fisioterapia e Dança, com a finalidade de participar do "IV Encuentro Internacional Artístico Humanista", em
Heredia, Costa Rica, no período compreendido entre 07/04/2018 e 15/04/2018, incluído trânsito, com ônus
UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 34497.
RUI VICENTE OPPERMANN
Reitor
