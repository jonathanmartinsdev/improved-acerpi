Documento gerado sob autenticação Nº TJV.156.993.0PS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4231                  de  15/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CORNELIA ECKERT, Professor do Magistério Superior, lotada e
em  exercício  no  Departamento  de  Antropologia  do  Instituto  de  Filosofia  e  Ciências  Humanas,  com  a
finalidade de participar do "VII Congresso da Associação Portuguesa de Antropologia", em Lisboa, Portugal,
no período compreendido entre 02/06/2019 e 09/06/2019, incluído trânsito, com ônus limitado. Solicitação nº
83677.
RUI VICENTE OPPERMANN
Reitor
