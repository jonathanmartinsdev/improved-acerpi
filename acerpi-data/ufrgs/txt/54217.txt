Documento gerado sob autenticação Nº UAN.756.353.RMV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3467                  de  11/05/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de PAULO MACHADO MORS, Professor do Magistério Superior,
lotado e em exercício no Departamento de Física do Instituto de Física, com a finalidade de participar da
"Conferência Regional de Educação Superior 2018", em Córdoba, Argentina, no período compreendido entre
09/06/2018 e 16/06/2018, incluído trânsito, com ônus limitado. Solicitação nº 33956.
RUI VICENTE OPPERMANN
Reitor
