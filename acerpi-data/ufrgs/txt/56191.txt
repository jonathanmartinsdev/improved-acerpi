Documento gerado sob autenticação Nº YIC.662.385.T2T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4926                  de  10/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°50192,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO EM MANUTENCAO DE
ÁUDIO/VÍDEO, do Quadro de Pessoal desta Universidade, ARY NIENOW (Siape: 0351353 ),  para substituir  
DANIELA BORGES PAVANI (Siape: 1574520 ), Diretor do Planetário, Código CD-4, em seu afastamento por
motivo de Laudo Médico do titular da Função, no período de 19/07/2018 a 21/07/2018, com o decorrente
pagamento das vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
