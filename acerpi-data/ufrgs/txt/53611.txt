Documento gerado sob autenticação Nº CYH.119.240.J2R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3005                  de  25/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder afastamento parcial, no período de 02/05/2018 a 01/05/2019, para a servidora ALINE
PORTELLA FERNANDES, ocupante do cargo de Museólogo - 701052, matrícula SIAPE 2143272,  lotada  no 
Centro de Estudos Costeiros, Limnológicos e Marinhos, para cursar o Mestrado Acadêmico do Programa de
Pós-graduação  em  Museologia  e  Patrimônio,  oferecido  pela  UFRGS;  conforme  o  Processo  nº
23078.505802/2018-41.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
