Documento gerado sob autenticação Nº XQB.196.562.U1M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11312                  de  20/12/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar KARINA DE CASTILHOS LUCENA,  matrícula SIAPE n° 1824920, ocupante do cargo de
Professor do Magistério Superior, classe Adjunto, lotada no Departamento de Línguas Modernas do Instituto
de Letras, do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Coordenadora da
COMGRAD de Letras, Código SRH 1227, código FUC, com vigência a partir da data de publicação no Diário
Oficial da União até 14/05/2019, a fim de completar o mandato da Professora INGRID FINGER, conforme
artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.514984/2017-60.
JANE FRAGA TUTIKIAN
Vice-Reitora.
