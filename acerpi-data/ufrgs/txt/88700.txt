Documento gerado sob autenticação Nº LFR.051.305.E7T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2800                  de  29/03/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016,  e  tendo  em  vista  o  que  consta  dos  Processos  Administrativos  n°  23078.501401/2019-01  e
23078.511667/2017-91, da Lei 10.520/02, do Contrato nº 006/2018 e ainda, da Lei 8.666/93,
          RESOLVE:
 
        Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no 01 da cláusula décima primeira do
contrato,  à  Empresa ROGER PORTAL BORGES ME,  CNPJ nº 16.491.357/0001-50,  pela irregularidade no
cadastro do SICAF, bem como erros em dados da nota fiscal conforme atestado pela fiscalização (Doc. SEI nº
1410732  e  1429113),  bem  como  pelo  NUDECON  (Doc.  SEI  nº  1490354)  do  processo  administrativo
23078.501401/2019-01
 
            Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG. 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
