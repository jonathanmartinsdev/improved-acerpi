Documento gerado sob autenticação Nº JHE.635.049.E67, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4122                  de  07/06/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar  INES  SEIDEL,  CPF  nº  29496799000,  Matrícula  SIAPE  1637850,  ocupante  do  cargo  de
Secretário Executivo, Código 701076, do Quadro de Pessoal desta Universidade, para exercer a função de
Coordenadora do Núcleo Administrativo da Gerência Adm do Instituto de Química, Código SRH 1247, código
FG-4,  com  vigência  a  partir  da  data  de  publicação  no  Diário  Oficial  da  União.  Processo  nº
23078.012328/2016-46.
RUI VICENTE OPPERMANN
Vice-Reitor
