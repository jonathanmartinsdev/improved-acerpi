Documento gerado sob autenticação Nº YHB.823.450.M49, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6551                  de  26/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de MARIA LUIZA SARAIVA PEREIRA,  Professor do Magistério
Superior, lotada e em exercício no Departamento de Bioquímica do Instituto de Ciências Básicas da Saúde,
com a finalidade de realizar  visita  à University of Groningen, Holanda, no período compreendido entre
27/09/2016 e 09/10/2016, incluído trânsito, com ônus CAPES/NUFFIC. Solicitação nº 22146.
CARLOS ALEXANDRE NETTO
Reitor
