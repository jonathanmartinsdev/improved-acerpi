Documento gerado sob autenticação Nº AVM.293.250.T7D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9940                  de  15/12/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar IARA SOUZA PEREIRA,  Matrícula SIAPE 0352730, ocupante do cargo de Assistente em
Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe da
Seção de Arquivo, vinc. ao Departamento de Contabilidade e Finanças da PROPLAN, Código SRH 1074, código
FG-5,  com  vigência  a  partir  da  data  de  publicação  no  Diário  Oficial  da  União.  Processo  nº
23078.025122/2016-86.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
