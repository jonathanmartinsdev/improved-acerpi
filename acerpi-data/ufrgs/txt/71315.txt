Documento gerado sob autenticação Nº HTT.056.726.P4H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8558                  de  24/10/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 22 de outubro de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
MARINA CARVALHO BERBIGIER, ocupante do cargo de Nutricionista-habilitação, Ambiente Organizacional
Ciências da Saúde, Código 701055, Classe E, Nível de Capacitação III, Padrão de Vencimento 03, SIAPE nº.
1838681 da Pró-Reitoria de Assuntos Estudantis para a lotação Faculdade de Medicina, com novo exercício
no Núcleo Técnico-Científico da Gerência Administrativa da Faculdade de Medicina.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
