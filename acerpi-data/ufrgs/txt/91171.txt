Documento gerado sob autenticação Nº AMZ.071.975.RJM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4133                  de  14/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARCUS ROLF PETER RITT, Professor do Magistério Superior,
lotado e em exercício no Departamento de Informática Teórica do Instituto de Informática, com a finalidade
de realizar visita à The University of Melbourne, em Melbourne, Austrália e participar da "2019 IEEE Congress
on Evolutionary Computation", em Wellington, Nova Zelândia, no período compreendido entre 31/05/2019 e
15/06/2019, incluído trânsito, com ônus limitado. Solicitação nº 83903.
RUI VICENTE OPPERMANN
Reitor
