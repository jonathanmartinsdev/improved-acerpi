Documento gerado sob autenticação Nº FVJ.307.421.T8R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6733                  de  28/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MANOEL  ROBERTO  MACIEL  TRINDADE,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Cirurgia da Faculdade de Medicina, com a
finalidade de participar do "23rd World Congress International Federation for the Surgery of Obesity and
Metabolic Disorders", em Dubai, Emirados Árabes Unidos, no período compreendido entre 25/09/2018 e
30/09/2018, incluído trânsito, com ônus limitado. Solicitação nº 58429.
RUI VICENTE OPPERMANN
Reitor
