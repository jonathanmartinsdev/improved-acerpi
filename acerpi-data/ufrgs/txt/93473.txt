Documento gerado sob autenticação Nº EBP.374.682.112, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5516                  de  01/07/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Retificar a Portaria n° 4121/2019, de 14/05/2019, que concedeu autorização para afastamento do
País a EDER DANIEL TEIXEIRA, Professor do Magistério Superior, com exercício no Departamento de Obras
Hidráulicas do Instituto de Pesquisas Hidráulicas
Onde se lê: com ônus UFRGS (Pró-Reitoria de Pesquisa: diárias),
leia-se: com ônus CAPES/PRINT/UFRGS, ficando ratificados os demais termos. Solicitação nº 85208.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
