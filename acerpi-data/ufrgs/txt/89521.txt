Documento gerado sob autenticação Nº SCQ.212.496.5RR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3077                  de  08/04/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor CARLOS VENTURA FONSECA, matrícula SIAPE n° 1020742, lotado e em exercício no Departamento
de Ensino e Currículo da Faculdade de Educação, da classe A  de Professor Adjunto A, nível 01, para a classe A 
de Professor Adjunto A, nível 02, referente ao interstício de 31/01/2017 a 30/01/2019, com vigência financeira
a partir de 31/01/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.500283/2019-13.
JANE FRAGA TUTIKIAN
Vice-Reitora.
