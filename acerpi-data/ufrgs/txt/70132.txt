Documento gerado sob autenticação Nº NCR.944.103.P05, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7661                  de  25/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de PRISCILA ZAVADIL PEREIRA, Professor do Magistério Superior,
lotada e em exercício no Departamento de Design e Expressão Gráfica da Faculdade de Arquitetura, com a
finalidade de participar da 5ª CIDAG - Conferência Internacional em Design e Artes Gráficas, em Lisboa,
Portugal, no período compreendido entre 23/10/2018 e 28/10/2018, incluído trânsito, com ônus limitado.
Solicitação nº 59173.
RUI VICENTE OPPERMANN
Reitor
