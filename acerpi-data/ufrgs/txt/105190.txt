Documento gerado sob autenticação Nº WFE.975.227.MG7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1481                  de  12/02/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Retificar a Portaria n° 7405/2019, de 15/08/2019, que concede incentivo à qualificação à CANDICE
CRESQUI, Assistente em Administração, SIAPE 3141326, com exercício na Divisão de Ingresso, Mobilidade e
Acompanhamento da Pró-Reitoria de Gestão de Pessoas, conforme Processo nº 23078.520663/2019-66.
 
Onde se lê:
"percentual de 35% (trinta e cinco por cento)",
leia-se:
"percentual de 52% (cinquenta e dois por cento)".
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
