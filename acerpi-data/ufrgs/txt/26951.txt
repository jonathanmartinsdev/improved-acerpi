Documento gerado sob autenticação Nº ALC.777.713.0MB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6759                  de  01/09/2016
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 5227, de 01 de outubro de 2012
 
              RESOLVE:
 
              Considerando o que consta do Processo Administrativo n° 23078.017319/2015-61, da Lei 10.520/02,
do Contrato nº 145/PROPLAN/NUDECON/2015 e da Lei 8.666/93,
              Aplicar a sanção administrativa de ADVERTÊNCIA, prevista na alínea "a" da cláusula décima primeira
do Contrato, à Empresa W.S. COMÉRCIO DE REFRIGERAÇÃO E EQUIPAMENTOS INDUSTRIAIS LTDA-ME,
CNPJ 13.624.180/0001-24, pelo atraso na execução do serviço, conforme atestado pela PRAE fl. 258-verso e
293, bem como pelo NUDECON fl. 298 do processo administrativo supracitado.
 
               Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
ARIO ZIMMERMANN
Pró-Reitor de Planejamento e Administração
