Documento gerado sob autenticação Nº NHE.496.585.RH4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8767                  de  21/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder o Adicional de Periculosidade, no percentual de 10%, a partir de 25/04/2017, ao servidor
ANDRE  LUIS  DE  MARISE  ROSA,  Identificação  Única  3578887,  Servente  de  Obras,  com  exercício  na
Subprefeitura do Campus Olímpico da Superintendência de Infraestrutura, observando-se o disposto na Lei
nº 8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer
atividades  em  áreas  consideradas  Perigosas  conforme  Laudo  Pericial  constante  no  Processo  n  º
23078.513957/2017-70, Código SRH n° 23284 e Código SIAPE n° 2017003578.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
