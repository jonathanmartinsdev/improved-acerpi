Documento gerado sob autenticação Nº IVZ.894.085.5I3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8765                  de  27/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  01/09/2019,
correspondente ao grau Insalubridade Máxima, ao servidor THALES RENATO OCHOTORENA DE FREITAS,
Identificação  Única  3572331,  Professor  do  Magistério  Superior,  com  exercício  no  Programa  de  Pós-
Graduação em Genética e Biologia Molecular do Instituto de Biociências, observando-se o disposto na Lei nº
8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer
atividades  em  áreas  consideradas  Insalubres  conforme  Laudo  Pericial  constante  no  Processo  nº
23078.525797/2019-73, Código SRH n° 24057.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
