Documento gerado sob autenticação Nº UGE.623.025.RF3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8887                  de  01/10/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  CAROLINE PACIEVITCH,  matrícula  SIAPE n°  2119459,  lotada no Departamento de  Ensino e
Currículo da Faculdade de Educação e com exercício no Programa de Pós-Graduação em Ensino de História,
da classe C  de Professor Adjunto, nível 01, para a classe C  de Professor Adjunto, nível 02, referente ao
interstício de 17/07/2017 a 16/07/2019, com vigência financeira a partir de 17/07/2019, de acordo com o que
dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN.
Processo nº 23078.514362/2019-01.
JANE FRAGA TUTIKIAN
Vice-Reitora.
