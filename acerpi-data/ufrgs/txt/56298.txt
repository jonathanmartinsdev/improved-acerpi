Documento gerado sob autenticação Nº MUY.135.054.I34, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5052                  de  12/07/2018
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016 e a PRÓ-REITORA DE
ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando
o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVEM
1 -  Estabelecer os preços das refeições servidas pelos Restaurantes Universitários, a serem praticados no
almoço e janta, conforme segue:
- Isentos: alunos UFRGS de Graduação integrantes do Programa de Benefícios da PRAE optantes do benefício
do restaurante universitário.
- R$ 1,30: alunos UFRGS.
- R$ 9,10: servidores UFRGS, terceirizados e demais usuários.
2 - O café da manhã será servido, na categoria isento, exclusivamente para moradores das casas de Estudante
integrantes do Programa de Benefícios da PRAE e optantes do benefício do restaurante universitário.
3 - Exceções a esses valores serão aplicadas somente com a anuência prévia da PRAE e da PROPLAN.
4 - Esta Portaria entra em vigor na data de sua publicação. Os valores serão revistos anualmente.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
