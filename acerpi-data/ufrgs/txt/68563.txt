Documento gerado sob autenticação Nº KRN.746.845.NI1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6486                  de  21/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ELZA DANIEL DE MELLO, matrícula SIAPE n° 1027369, lotada e em exercício no Departamento de
Pediatria da Faculdade de Medicina, da classe D  de Professor Associado, nível 03, para a classe D  de
Professor Associado, nível 04, referente ao interstício de 25/10/2015 a 24/10/2017, com vigência financeira a
partir de 11/07/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.517110/2018-45.
JANE FRAGA TUTIKIAN
Vice-Reitora.
