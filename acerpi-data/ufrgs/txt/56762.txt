Documento gerado sob autenticação Nº FFB.053.311.HJC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5366                  de  19/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°41167,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de TÉCNICO DE LABORATÓRIO
ÁREA,  do Quadro de Pessoal  desta Universidade,  ALEXSANDRO DALLEGRAVE (Siape:  1456353 ),   para
substituir   FRANCISCO PAULO DOS SANTOS (Siape: 2074870 ), Coordenador da Central Analítica do Instituto
de Química, Código FG-1, em seu afastamento por motivo de férias, no período de 23/07/2018 a 27/07/2018,
com o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
