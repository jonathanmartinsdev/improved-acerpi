Documento gerado sob autenticação Nº BMR.682.610.SA0, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7280                  de  14/09/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar SUSANA MIRANDA DA SILVA, Matrícula SIAPE 1691952, ocupante do cargo de Assistente
em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe
do Setor Administrativo, Financeiro e de Planejamento da GA da FCE, Código SRH 643, código FG-7, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.020184/2016-00.
RUI VICENTE OPPERMANN
Vice-Reitor
