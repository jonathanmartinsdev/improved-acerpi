Documento gerado sob autenticação Nº IHZ.157.621.0P8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9132                  de  16/11/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
   Conceder  ao  servidor  RICARDO  GOMEZ  PIRES,  ocupante  do  cargo  de   Assistente  em
Administração - 701200, lotado no Instituto de Química, SIAPE 0358698, o percentual de 25% (vinte e cinco
por cento) de Incentivo à Qualificação, a contar de 05/11/2016, tendo em vista a conclusão do curso Superior
de Tecnologia em Gestão de Recursos Humanos, conforme o Processo nº 23078.202528/2016-99.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
