Documento gerado sob autenticação Nº WBU.112.759.ABL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6106                  de  11/07/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de LUCIANA VELLINHO CORSO, Professor do Magistério Superior,
lotada  e  em exercício  no  Departamento  de  Estudos  Especializados  da  Faculdade de  Educação,  com a
finalidade  de  participar  da  "17th  Biennial  EARLI  Conference",  em  Tampere,  Finlândia,  no  período
compreendido entre 27/08/2017 e 04/09/2017, incluído trânsito, com ônus UFRGS (Faculdade de Educação -
diárias e passagens). Solicitação nº 29517.
RUI VICENTE OPPERMANN
Reitor
