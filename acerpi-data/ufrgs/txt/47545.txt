Documento gerado sob autenticação Nº VPC.743.557.87D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10693                  de  24/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar  a  Portaria  n°  3544/2017,  de  27/04/2017,  que  concede  afastamento  parcial  a
servidora GABRIELA NEGRUNI WENTZ,  Técnico de Laboratório Área,  com exercício no Núcleo Técnico-
Científico Central Analítica do Instituto de Química, conforme o Processo nº 23078.201048/2017-91.
 
Onde se lê:
"no período de 02/05/2017 a 31/03/2018",
leia-se:
"no período de 02/05/2017 a 19/11/2017".
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
