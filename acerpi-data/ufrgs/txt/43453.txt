Documento gerado sob autenticação Nº TID.518.806.6N4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7965                  de  24/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  01/07/2017,
correspondente  ao  grau  Insalubridade  Máxima,  à  servidora  FERNANDA  VIEIRA  AMORIM  DA  COSTA,
Identificação Única 19033621, Professor do Magistério Superior, com exercício na Comissão de Graduação de
Veterinária da Faculdade de Veterinária, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de
1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas
Insalubres conforme Laudo Pericial constante no Processo nº 23078.512284/2017-31, Código SRH n° 23234 e
Código SIAPE 2017003138.
Revogam-se quaisquer outros valores pagos à servidora a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
