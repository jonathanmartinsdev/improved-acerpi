Documento gerado sob autenticação Nº ATY.085.298.3R7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9935                  de  10/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Nomear, em caráter efetivo, NARA AMELIA MELO DA SILVA, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 55/2018 de 5 de Julho de 2018, homologado em 06 de julho
de 2018 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de
28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do
Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro
de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento
de Artes Visuais do Instituto de Artes, em vaga decorrente da aposentadoria do Professor Luiz Antonio
Carvalho da Rocha, código nº 277601, ocorrida em 16 de Abril de 2018, conforme Portaria nº. 2739/2018 de
13  de  abril  de  2018,  publicada  no  Diário  Oficial  da  União  de  16  de  abril  de  2018.  Processo  nº.
23078.520245/2017-15.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
