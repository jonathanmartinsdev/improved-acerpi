Documento gerado sob autenticação Nº CPN.480.369.D30, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1170                  de  07/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MARCO  VUGMAN  WAINSTEIN,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a
finalidade de participar do "Joint Interventional Meeting -  JIM 2017: in partnership with TCT",  em Bonn,
Alemanha, no período compreendido entre 07/02/2017 e 13/02/2017, incluído trânsito, com ônus limitado.
Solicitação nº 25704.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
