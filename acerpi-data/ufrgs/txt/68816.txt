Documento gerado sob autenticação Nº MAM.328.239.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6689                  de  27/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de AIRTON CATTANI, Professor do Magistério Superior, lotado e
em exercício no Departamento de Design e Expressão Gráfica da Faculdade de Arquitetura, com a finalidade
de realizar  visita  à Communication University of China, em Beijing, China, no período compreendido entre
10/10/2018 e 21/10/2018, incluído trânsito, com ônus UFRGS (passagens). Solicitação nº 58727.
RUI VICENTE OPPERMANN
Reitor
