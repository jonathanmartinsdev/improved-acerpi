Documento gerado sob autenticação Nº YRM.921.475.NHB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2125                  de  09/03/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar a Portaria n° 672/2017, de 24/01/2017, que concedeu autorização para afastamento do País
a MIRIAM THAIS GUTERRES DIAS, Professor do Magistério Superior, com exercício no Departamento de
Serviço Social do Instituto de Psicologia
Onde se lê: com ônus limitado,
leia-se: com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias), ficando ratificados os demais termos.
Solicitação nº 26178.
RUI VICENTE OPPERMANN
Reitor.
