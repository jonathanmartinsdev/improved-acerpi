Documento gerado sob autenticação Nº MUT.950.088.9F5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5385                  de  26/06/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de SEBASTIAN GONCALVES,  Professor do Magistério Superior,
lotado e em exercício no Departamento de Física do Instituto de Física,  com a finalidade de participar
da  "27th  International  Conference  on  Statistical  Physics",  em  Buenos  Aires,  Argentina,  no  período
compreendido entre 05/07/2019 e 13/07/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa:
diárias). Solicitação nº 84061.
RUI VICENTE OPPERMANN
Reitor
