Documento gerado sob autenticação Nº ASL.323.898.GTC, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7063                  de  03/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o afastamento do país  de GERHARD HANS KNORNSCHILD,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Metalurgia da Escola de Engenharia, com a finalidade de
participar do "20th International Corrosion Congress Et Process Safety Congress 2017", em Praga, República
Tcheca, no período compreendido entre 02/09/2017 e 08/09/2017, incluído trânsito, com ônus limitado.
Solicitação nº 28765.
RUI VICENTE OPPERMANN
Reitor
