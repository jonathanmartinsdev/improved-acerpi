Documento gerado sob autenticação Nº UPG.516.716.06D, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1629                  de  03/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de Outubro de 2012
RESOLVE:
Conceder  Progressão  por  Capacitação,  a  contar  de  15/02/2016,   referente  ao  interstício  de
15/08/2014  a  14/02/2016,  para  a  servidora  LUDYMILA  SCHULZ  BARROSO,  ocupante  do  cargo  de
Nutricionista-habilitação  -  701055,  matrícula  SIAPE  2157461,   lotada   na   Pró-Reitoria  de  Assuntos
Estudantis, passando do Nível de Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de
Capacitação  E  II,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.001200/2016-57:
SGS - Formação de Especialista em Segurança de Alimentos CH: 180 Carga horária utilizada: 120 hora(s) /
Carga horária excedente: 60 hora(s) (26/04/2014 a 21/02/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
