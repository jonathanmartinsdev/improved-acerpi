Documento gerado sob autenticação Nº FTK.303.530.QK0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11237                  de  18/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°48917,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de RELAÇÕES PÚBLICAS, do Quadro
de Pessoal desta Universidade, PAMELA DA SILVA POCHMANN (Siape: 3066936 ),  para substituir   MARCIA
BARCELOS (Siape:  6358808 ),  Coordenador de Cerimonial  do Gabinete do Reitor,  Código CD-4,  em seu
afastamento por motivo de férias, no período de 18/12/2019 a 20/12/2019, com o decorrente pagamento das
vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
