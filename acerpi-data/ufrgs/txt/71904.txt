Documento gerado sob autenticação Nº RQI.432.531.QQO, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9016                  de  07/11/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ELZA DANIEL DE MELLO, Professor do Magistério Superior,
lotada  e  em exercício  no  Departamento  de  Pediatria  da  Faculdade  de  Medicina,  com a  finalidade  de
participar do "28th Meeting of the European Childhood Obesity Group", na cidade do Porto, Portugal, no
período compreendido entre 14/11/2018 e 17/11/2018, incluído trânsito, com ônus limitado. Solicitação nº
60459.
RUI VICENTE OPPERMANN
Reitor
