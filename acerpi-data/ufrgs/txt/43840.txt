Documento gerado sob autenticação Nº VMJ.276.816.8GP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8165                  de  30/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°29996,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a  ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  ELIANE ANGELA VEIT  (Siape:  0352122  ),   para
substituir   IVES SOLANO ARAUJO (Siape: 2505206 ), Coordenador do PPG em Ensino de Física, Código FUC,
em seu afastamento do país, no período de 03/09/2017 a 09/09/2017, com o decorrente pagamento das
vantagens por 7 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
