Documento gerado sob autenticação Nº RKQ.561.531.FPU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10112                  de  01/11/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 30 de outubro de 2017,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
TACIA  BORGES  DE  OLIVEIRA  MILLER,  ocupante  do  cargo  de  Assistente  em  Administração,  Ambiente
Organizacional Administrativo, Código 701200, Classe D, Nível de Capacitação II, Padrão de Vencimento 02,
SIAPE nº.  2267213 do Instituto de Pesquisas Hidráulicas para a lotação Pró-Reitoria de Planejamento e
Administração, com novo exercício no Núcleo de Contratos e Normativas.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
