Documento gerado sob autenticação Nº JNR.907.639.F5N, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5196                  de  14/06/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANDRE LUIZ REIS DA SILVA, Professor do Magistério Superior,
lotado no Departamento de Economia e Relações Internacionais da Faculdade de Ciências Econômicas e com
exercício  no Programa de Pós-Graduação em Estudos  Estratégicos  Internacionais,  com a  finalidade de
ministrar curso na Beijing Foreign Studies University, em Pequim, China, no período compreendido entre
01/07/2017 e 11/07/2017, incluído trânsito, com ônus limitado. Solicitação nº 27982.
RUI VICENTE OPPERMANN
Reitor
