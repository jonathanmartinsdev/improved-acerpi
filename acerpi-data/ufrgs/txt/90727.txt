Documento gerado sob autenticação Nº LVU.836.667.KBR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3876                  de  07/05/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de KARLA SALVAGNI HEINECK, Professor do Magistério Superior,
lotada e em exercício no Departamento de Engenharia Civil da Escola de Engenharia, com a finalidade de
realizar visita à University of Miami,  em Coral  Gables,  Estados Unidos,  no período compreendido entre
24/05/2019 e 01/06/2019, incluído trânsito, com ônus limitado. Solicitação nº 83062.
RUI VICENTE OPPERMANN
Reitor
