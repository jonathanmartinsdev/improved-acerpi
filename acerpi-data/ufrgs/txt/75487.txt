Documento gerado sob autenticação Nº UVG.996.233.9IP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             828                  de  23/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora LUIZA FEDATTO VIDAL, ocupante do cargo de  Farmacêutico - 701087, lotada
na Faculdade de Farmácia, SIAPE 1006994, o percentual de 30% (trinta por cento) de Incentivo à Qualificação,
a contar de 17/12/2018, tendo em vista a conclusão do curso de Especialização em Saúde Coletiva, conforme
o Processo nº 23078.534853/2018-80.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
