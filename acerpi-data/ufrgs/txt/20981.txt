Documento gerado sob autenticação Nº KOS.190.997.LL0, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3080                  de  27/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Designar  MARIA  DELOURDES  DA  FONSECA,  CPF  nº  19927010087,  Matrícula  SIAPE  0354014,
ocupante do cargo de Secretário Executivo, Código 701076, do Quadro de Pessoal desta Universidade, para
exercer a função de Secretária de Pós-Graduação da Faculdade de Ciências Econômicas, Código SRH 593,
Código  FG-7,  com  vigência  a  partir  da  data  de  publicação  no  Diário  Oficial  da  União.  Processo  nº
23078.008293/2016-41.
RUI VICENTE OPPERMANN
Vice-Reitor
