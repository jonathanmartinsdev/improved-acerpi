Documento gerado sob autenticação Nº GRT.443.812.9OT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6703                  de  25/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Retificar a Portaria n° 9914/2017, de 26/10/2017, publicada no Diário Oficial da União - DOU em 27
de outubro de 2017, Seção 2, Página 33, que concedeu autorização para afastamento do País a VINICIUS
ANDRADE  BREI,  Professor  do  Magistério  Superior,  com  exercício  no  Departamento  de  Ciências
Administrativas da Escola de Administração
Onde se lê: no período compreendido entre 01/07/2018 e 30/06/2019,
leia-se: no período compreendido entre 01/07/2018 e 16/06/2019, ficando ratificados os demais
termos. Processo nº 23078.512851/2017-59.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
