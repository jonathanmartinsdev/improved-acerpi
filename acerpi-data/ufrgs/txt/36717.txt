Documento gerado sob autenticação Nº ORV.492.603.51K, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3098                  de  10/04/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  07/04/2017,   referente  ao  interstício  de
07/10/2015 a 06/04/2017, para a servidora ELISANGELA PIRES RAMOS DE JESUS,  ocupante do cargo de
Enfermeiro-área - 701029, matrícula SIAPE 2258857,  lotada  no  Campus Litoral Norte, passando do Nível de
Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.005754/2017-12:
Formação Integral de Servidores da UFRGS V CH: 134 Carga horária utilizada: 120 hora(s) / Carga horária
excedente: 14 hora(s) (04/11/2015 a 30/09/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
