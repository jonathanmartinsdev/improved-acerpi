Documento gerado sob autenticação Nº EKW.263.351.KC3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2537                  de  06/04/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Alterar, a partir da data deste ato, o regime de trabalho de 20 horas semanais para Dedicação
Exclusiva, conforme o disposto no artigo 15, inciso I, do Anexo ao Decreto nº 94.664, de 23 de julho de 1987,
Decreto n° 94.664/87 e Lei n° 12.772/2012 atribuído à Professora ANA FRANCISCA SCHNEIDER GRINGS, com
exercício no(a) Colégio de Aplicação desta Universidade. Processo n° 23078.508932/2016-73.
JANE FRAGA TUTIKIAN
Vice-Reitora.
