Documento gerado sob autenticação Nº EHQ.277.143.9F5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6926                  de  31/07/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de JOSÉ RODRIGO FURLANETTO DE AZAMBUJA,  Professor do
Magistério  Superior,  lotado  e  em  exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de
Informática, com a finalidade de participar do "30th European Symposium on Reliability of Electron Devices,
Failure  Physics  and  Analysis",  em  Toulouse,  França,  no  período  compreendido  entre  23/09/2019  e
27/09/2019, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa: diárias). Solicitação nº 84171.
RUI VICENTE OPPERMANN
Reitor
