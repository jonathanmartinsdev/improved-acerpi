Documento gerado sob autenticação Nº EUB.249.495.64V, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2624                  de  25/03/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Museólogo, classe E, do Quadro de Pessoal desta Universidade,
MICHELI  PEREIRA  DE  SOUZA,  matrícula  SIAPE  n°  2249499,  lotada  no  Instituto  de  Geociências,  como
Coordenadora  Substituta  da  Comissão  de  Extensão  do  Instituto  de  Geociências,  para  substituir
automaticamente  o  titular  desta  função  em  seus  afastamentos  ou  impedimentos  regulamentares  na
vigência do presente mandato. Processo nº 23078.506752/2019-08.
JANE FRAGA TUTIKIAN
Vice-Reitora.
