Documento gerado sob autenticação Nº CHG.722.814.C6L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11451                  de  26/12/2019
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 3435, de 22 de abril de 2019
RESOLVE
Designar os servidores  GISELLE REIS ANTUNES, ocupante do cargo de Engenheiro-área, lotada na
Superintendência de Infraestrutura e com exercício no Departamento de Meio Ambiente e Licenciamento,
EDSON LUIS NICOLAIT FERNANDES, ocupante do cargo de Engenheiro-área, lotado na Superintendência de
Infraestrutura e com exercício no Setor de Projetos de Edificações, MARCO ANTONIO SCHUCK, ocupante do
cargo de Arquiteto e Urbanista, lotado na Superintendência de Infraestrutura e com exercício no Setor de
Fiscalização e Adequação, para, sob a presidência da primeira, comporem Grupo de Trabalho para, no prazo
de 60 dias a partir desta data,  elaborar e apresentar laudo técnico de inspeção predial face à legislação
vigente com foco na fachada da edificação, para o prédio 21.101, localizado na Av. Protásio Alves nº 297. Tal
documento deverá  detalhar  as  condições  estruturais  da  fachada (revestimento),  uma vez  que há uma
bandeja no local, conforme Notificação nº 481947 da SMAMS/PMPA.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
