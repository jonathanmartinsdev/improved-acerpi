Documento gerado sob autenticação Nº YBG.010.498.HHP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6041                  de  09/08/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Retificar a Portaria nº 5810 de 03/08/2018  que designou, temporariamente, MARY JANE TWEEDIE
DE MATTOS GOMES  (Siape: 0354307)  para substituir  EMERSON ANTONIO CONTESINI (Siape: 1124074 ),
Processo SEI Nº 23078.520072/2018-16
onde se lê:
"...  em seu afastamento por motivo de férias,  no período de 06/08/2018 a 17/08/2018, com o
decorrente pagamento das vantagens por 12 dias..."; 
          leia-se:
"...  em seu afastamento por motivo de férias,  no período de 06/08/2018 a 07/08/2018, com o
decorrente pagamento das vantagens por 02 dias..."; ficando ratificados os demais termos.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
