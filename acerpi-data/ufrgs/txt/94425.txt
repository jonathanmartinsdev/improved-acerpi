Documento gerado sob autenticação Nº UHM.683.005.KLH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6108                  de  18/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°85683,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, Eduardo Rigon Zimmer (Siape: 3003846 ),   para
substituir   IRACI LUCENA DA SILVA TORRES (Siape: 1545035 ), Coordenador do PPG em Ciências Biológicas:
Farmacologia e Terapêutica, Código FUC, em seu afastamento no país, no dia 22/07/2019, com o decorrente
pagamento das vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
