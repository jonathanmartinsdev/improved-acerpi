Documento gerado sob autenticação Nº VEC.636.269.DV8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8100                  de  29/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANALÚCIA DANILEVICZ PEREIRA,  Professor do Magistério
Superior, lotada e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de
Ciências  Econômicas,  com a finalidade de participar  de cursos junto ao Instituto Superior  de Relações
Internacionais, em Maputo, Moçambique, no período compreendido entre 02/09/2017 e 16/09/2017, incluído
trânsito, com ônus limitado. Solicitação nº 30225.
RUI VICENTE OPPERMANN
Reitor
