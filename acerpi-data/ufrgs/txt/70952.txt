Documento gerado sob autenticação Nº HUZ.676.718.0F9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8287                  de  16/10/2018
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7620,
de 29 de setembro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, VÂNIA CRISTINA SANTOS PEREIRA (Siape: 6355656),
 para substituir automaticamente MAURÍCIO VIÉGAS DA SILVA (Siape: 6354315 ), Pró-Reitora de Gestão de
Pessoas, Código SRH 3, CD-2, em seu afastamento no país, no dia 17/10/2018, com o decorrente pagamento
das vantagens por 01 dia.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
