Documento gerado sob autenticação Nº JYF.876.823.ODA, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10193                  de  06/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder Progressão por Mérito Profissional, nos termos do artigo 10-A da Lei nº 11.091, de 12 de
janeiro  de  2005,  e  da  Decisão nº  939,  de  21  de  novembro de  2008,  do  Conselho Universitário  desta
Universidade,  para  o  servidor  GILSON  JULIAO  NUNES  LEMES,  matrícula  SIAPE  0356981,  Nível  de
Classificação B , para o padrão de vencimento 15, com vigência a partir de 07 de janeiro de 2012.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
