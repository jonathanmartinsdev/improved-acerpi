Documento gerado sob autenticação Nº GMW.096.056.O7B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4265                  de  16/05/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 13/05/2019,  o ocupante do cargo de Professor do Magistério
Superior, classe Adjunto, do Quadro de Pessoal desta Universidade, RAFAEL AISLAN AMARAL, matrícula SIAPE
n° 2258922, da função de Coordenador da COMGRAD do Curso de Engenharia de Gestão de Energia do
Campus Litoral Norte, Código SRH 1505, código FUC, para a qual foi designado pela Portaria 3550/2018, de
15/05/2018, publicada no Diário Oficial da União do dia 17/05/2018. Processo nº 23078.511973/2019-90.
JANE FRAGA TUTIKIAN
Vice-Reitora.
