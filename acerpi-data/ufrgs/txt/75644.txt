Documento gerado sob autenticação Nº BVO.045.901.CKG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             843                  de  24/01/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de FERNANDA GUSMAO DE LIMA KASTENSMIDT, Professor do
Magistério Superior, lotada no Departamento de Informática Aplicada do Instituto de Informática e com
exercício no Programa de Pós-Graduação em Microeletrônica, com a finalidade de realizar visita à Université
Montpellier  II,  em Montpellier,  França e à Université Grenoble Alpes,  em Grenoble,  França,  no período
compreendido entre 04/02/2019 e 15/03/2019, incluído trânsito, com ônus limitado. Solicitação nº 62020.
RUI VICENTE OPPERMANN
Reitor
