Documento gerado sob autenticação Nº MWK.331.479.RC2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10333                  de  28/12/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora do Magistério do Ensino Básico, Técnico e Tecnológico TAIS CRISTINE ERNST FRIZZO, matrícula
SIAPE n° 2347648, com lotação no Colégio de Aplicação, da classe D IV, nível 02, para a classe D IV, nível 03,
referente ao interstício de 01/07/2014 a 30/06/2016, com vigência financeira a partir de 01/08/2016, de
acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações e a Decisão nº
328/2015 - CONSUN. Processo nº 23078.512032/2016-21.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
