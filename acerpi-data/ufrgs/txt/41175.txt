Documento gerado sob autenticação Nº QGR.693.987.7L6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6375                  de  17/07/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora EMILENE MENDES BECKER, matrícula SIAPE n° 1555088, lotada e em exercício no Departamento
de Química Inorgânica do Instituto de Química, da classe C  de Professor Adjunto, nível 01, para a classe C  de
Professor Adjunto, nível 02, referente ao interstício de 29/05/2013 a 28/05/2015, com vigência financeira a
partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações  e  a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.505827/2017-63.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
