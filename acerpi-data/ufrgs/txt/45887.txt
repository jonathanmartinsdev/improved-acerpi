Documento gerado sob autenticação Nº KGN.127.242.N17, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9513                  de  11/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar ALINE BLAYA MARTINS DE SANTA HELENA, matrícula SIAPE n° 2687863, ocupante do
cargo  de  Professor  do  Magistério  Superior,  classe  Adjunto,  lotada  no  Departamento  de  Odontologia
Preventiva  e  Social,  do  Quadro  de  Pessoal  da  Universidade,  para  exercer  a  função  de  Coordenadora
Substituta do PPG em Saúde Coletiva da Escola de Enfermagem, com vigência a partir da data deste ato até
07/11/2018, a fim de completar o mandato do professor RICARDO BURG CECCIM, conforme artigo 92 do
Estatuto da mesma Universidade. Processo nº 23078.515942/2017-46.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
