Documento gerado sob autenticação Nº UXX.652.047.E67, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4129                  de  07/06/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Declarar vaga, a partir de 18/05/2016, a função de Chefe da Divisão de Recursos Humanos da
Gerência Administrativa do Instituto de Biociências, Código SRH 1468, Código FG-4, desta Universidade, tendo
em vista a Aposentadoria c/ proventos Integrais (Art. 3º Inc. I, II, III) EC 47/05 de TANIA MARISA DUBAL DA
SILVA, CPF n° 43955819000, matrícula SIAPE n° 1034670, conforme Portaria n° 3659/2016, de 17 de maio de
2016, publicada no Diário Oficial da União do dia 18/05/2016. Processo nº 23078.012233/2016-22.
RUI VICENTE OPPERMANN
Vice-Reitor
