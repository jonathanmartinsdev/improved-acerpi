Documento gerado sob autenticação Nº KOM.604.502.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3397                  de  18/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5061916-11.2018.4.04.7100,  da  5ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006,  da  servidora  HELENA  TEREZINHA  NOGUEIRA  CANDIDO,  matrícula  SIAPE  n°  0358497,
aposentada no cargo de Porteiro - 701458, do Nível I para o Nível III, a contar de 01/01/2006, conforme o
Processo nº 23078.508972/2019-68.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
Conceder  progressão  por  capacitação,  a  contar  de  17/03/2014,  passando  do  Nível  de
Classificação/Nível  de  Capacitação  C  III   para  o  Nível  de  Classificação/Nível  de  Capacitação  C  IV.
 
 
RUI VICENTE OPPERMANN
Reitor
