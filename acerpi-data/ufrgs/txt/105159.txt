Documento gerado sob autenticação Nº WEZ.624.241.4M9, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1464                  de  11/02/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor RENATO MARCHIORI BAKOS, matrícula SIAPE n° 1325774, lotado e em exercício no Departamento
de Medicina Interna da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 04, para a classe D  de
Professor Associado, nível 01, referente ao interstício de 03/10/2017 a 02/10/2019, com vigência financeira a
partir de 03/10/2019, conforme decisão judicial proferida no processo nº 5054491-30.2018.4.04.7100, da 1ª
Vara Federal de Porto Alegre, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com
suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.500750/2020-31.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
