Documento gerado sob autenticação Nº ZRV.397.350.HKV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9859                  de  31/10/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar BETINA BARRERAS CAETANO, Matrícula SIAPE 3149585, ocupante do cargo de Assistente
em Administração, Código 701200, do Quadro de Pessoal desta Universidade, para exercer a função de
Secretária de Pós-Graduação do Instituto de Letras, Código SRH 596, código FG-7, com vigência a partir de
01/12/2019. Processo nº 23078.529317/2019-43.
JANE FRAGA TUTIKIAN
Vice-Reitora.
