Documento gerado sob autenticação Nº GYJ.407.935.0R2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6625                  de  24/08/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
ALTERAR o percentual  de Incentivo à  Qualificação concedido à  servidora DAIENE ELISA LOSS,
ocupante do cargo de Médico Veterinário-701048, lotada no Hospital de Clínicas Veterinárias, SIAPE 1758924,
para 75% (setenta e cinco por cento), a contar de 16/04/2018, tendo em vista a conclusão do curso de
Doutorado em Medicina Animal: Equinos, conforme o Processo nº 23078.508464/2018-07.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
