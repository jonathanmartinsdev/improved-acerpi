Documento gerado sob autenticação Nº BZN.075.715.UUG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6257                  de  13/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Designar a servidora JULIANA DONADUSSI NEUHAUS LIGNATI ocupante do cargo de Médico/área,
matrícula SIAPE 1318996, lotada no Departamento de Atenção à Saúde da Pró-Reitoria de Gestão de Pessoas,
para atuar como Perito Oficial em Saúde.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
