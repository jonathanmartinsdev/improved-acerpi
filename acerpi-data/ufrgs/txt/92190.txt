Documento gerado sob autenticação Nº JBG.548.448.BQN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4759                  de  04/06/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar sem efeito, a nomeação da candidata abaixo relacionada, ocorrida através da Portaria nº.
3210/2019 de 12 de abril de 2019, publicada no Diário Oficial da União de 15 de abril de 2019, de acordo com
o que preceitua o § 6º do artigo 13 da Lei nº. 8.112, de 11 de dezembro de 1990, com a redação dada pela Lei
nº. 9.527, de 10 de dezembro de 1997. Processo nº. 23078.501687/2018-35.
 
Cargo 701200 - ASSISTENTE EM ADMINISTRAÇÃO - Classe D Padrão I
 
JOSIANE HEYDE DOS SANTOS - Vaga SIAPE 274102
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
