Documento gerado sob autenticação Nº EBK.545.198.9SG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             197                  de  06/01/2017
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
Designar os servidores  EDUARDO DOS SANTOS WILGES, ocupante do cargo de Engenheiro-área,
lotado  na  Superintendência  de  Infraestrutura  e  com  exercício  na  Prefeitura  Campus  do  Vale,  PAULA
ALEXANDRA LINN, ocupante do cargo de Administrador, lotada na Superintendência de Infraestrutura e
com  exercício  no  Planejamento  e  Assessoria  da  Superintendência  de  Infraestrutura,  ROBERTO  PAAZ,
ocupante do cargo de Engenheiro-área, lotado na Superintendência de Infraestrutura e com exercício no
Planejamento e Assessoria da Superintendência de Infraestrutura,  para,  sob a presidência do primeiro,
comporem a Comissão para, no prazo de 15 dias a contar de 09 de janeiro de 2017, elaborar Termo de
Referência para licitar a contratação de serviço de manutenção em subestações.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
