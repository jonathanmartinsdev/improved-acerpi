Documento gerado sob autenticação Nº BTD.061.723.QC5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9978                  de  10/12/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, GABRIELA BETTELLA CYBIS, matrícula SIAPE n° 2034926, lotada no Departamento
de Estatística do Instituto de Matemática e Estatística, como Coordenadora Substituta do PPG em Estatística,
para  substituir  automaticamente  o  titular  desta  função  em  seus  afastamentos  ou  impedimentos
regulamentares na vigência do presente mandato. Processo nº 23078.533229/2018-65.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
