Documento gerado sob autenticação Nº GJN.379.857.MF7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2713                  de  28/03/2019
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
              RESOLVE
 
 
             Declarar a perda de mandato, a partir de 26/03/2019, dos seguintes representantes discentes:
Haniel Monteiro Carvalho, como 4º titular e Henrique Gomes Acosta, como 4º suplente, nomeados pela
Portaria nº 4160 de 08/06/2018, junto ao Conselho Universitário, por terem faltado, sem motivo justificado, a
3 (três) reuniões consecutivas ou a 5 (cinco) intercaladas.
 
               E nomear os seguintes discentes: Giovani Culau Oliveira, como 4º titular e Jordan Wagner Correa,
como 4º suplente, para ocuparem as vagas estabelecidas acima, atendendo ao disposto nos artigos 175 do
Regimento  Geral  da  Universidade  e  79  do  Estatuto  da  Universidade  e  considerando  o  processo  nº
23078.507716/2019-53.
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
