Documento gerado sob autenticação Nº GCO.871.904.GUC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7738                  de  17/08/2017
O DIRETOR DO DEPARTAMENTO DE ADMINISTRAÇÃO DE PESSOAL DA PROGESP DA UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183,
de 17 de outubro de 2008
RESOLVE
Conceder, nos termos do artigo 87, combinado com o artigo 100, redação original, da Lei nº 8.112,
de 11 de dezembro de 1990 e do Parecer nº 01/91-ADP, de 26 de abril  de 1991, à servidora ANGELA
TEREZINHA DE SOUZA WYSE, ocupante do cargo de Professor do Magistério Superior, do Quadro de Pessoal
desta Universidade, com exercício no Departamento de Bioquímica do Instituto de Ciências Básicas da Saúde,
03 meses de Licença-prêmio por Assiduidade, referente ao quinquênio compreendido entre 01/07/1991 e
30/06/1996, a ser gozada de acordo com a escala organizada em conformidade com o artigo 89 do mesmo
diploma legal. Processo nº 23078.030068/2014-29.
MARCELO SOARES MACHADO
Diretor do Departamento de Administração de Pessoal da PROGESP
