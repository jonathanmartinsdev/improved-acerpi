Documento gerado sob autenticação Nº MYL.074.812.3N5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10635                  de  29/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°45912,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do Quadro de Pessoal  desta Universidade,  DIOGO ANDRE PILGER (Siape:  2413665 ),   para
substituir   TIANA TASCA (Siape: 1546131 ), Coordenador da COMGRAD de Farmácia, Código FUC, em seu
afastamento por motivo de férias, no período de 02/01/2019 a 04/02/2019, com o decorrente pagamento das
vantagens por 34 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
