Documento gerado sob autenticação Nº JCU.195.439.47M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11358                  de  20/12/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  EVERTON DA SILVEIRA FARIAS,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Ciências Contábeis e Atuariais da Faculdade de Ciências
Econômicas, com a finalidade de realizar visita ao Grupo INVERFIN S.A.E.C.A., em Assunção, Paraguai, no
período compreendido entre 05/01/2020 e 11/01/2020, incluído trânsito, com ônus limitado. Solicitação nº
89322.
RUI VICENTE OPPERMANN
Reitor
