Documento gerado sob autenticação Nº IYF.976.595.PSM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1792                  de  23/02/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder 89 (oitenta e nove) dias de licença para capacitação, nos termos do artigo 87 da Lei nº
8.112, de 11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para a
servidora SUSANA BEHENCK SEIBEL, com exercício na Seção de Contratos da Pró-Reitoria de Planejamento e
Administração,  a  ser  usufruída  no  período  de  27/03/2017  a  23/06/2017,  referente  ao  quinquênio  de
27/05/2011 a 27/05/2016, a fim de elaborar o trabalho de conclusão de curso de Graduação em Direito, na
Fundação Escola Superior do Ministério Público - FMP, conforme Processo nº 23078.026229/2016-41.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
