Documento gerado sob autenticação Nº MGX.908.879.8DB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8825                  de  31/10/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de PABLO TIBOR QUINTERO MANSILLA, Professor do Magistério
Superior,  lotado e  em exercício  no Departamento de Antropologia  do Instituto  de  Filosofia  e  Ciências
Humanas,  com  a  finalidade  de,  como  r  palestrante,  participar  do  "VIII  Conferencia  Latinoamericana  y
Caribeña de Ciencias Sociales:  Las luchas por la igualdad,  la justicia social  y  la democracia en un mundo
turbulento", em Buenos Aires, Argentina, no período compreendido entre 18/11/2018 e 24/11/2018, incluído
trânsito, com ônus limitado. Solicitação nº 60795.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
