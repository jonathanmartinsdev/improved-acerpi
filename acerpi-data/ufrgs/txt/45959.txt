Documento gerado sob autenticação Nº CGY.652.251.Q0T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9902                  de  26/10/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder à servidora LAIS CRISTINA GROSS GERHARDT,  ocupante do cargo de  Assistente em
Administração - 701200, lotada na Pró-Reitoria de Gestão de Pessoas, SIAPE 2425393, o percentual de 15%
(quinze por cento) de Incentivo à Qualificação, a contar de 06/10/2017, tendo em vista a conclusão do curso
de Graduação em Fisioterapia, conforme o Processo nº 23078.517718/2017-99.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
