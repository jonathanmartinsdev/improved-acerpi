Documento gerado sob autenticação Nº FZA.114.031.OU6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             10416                  de  26/12/2018
Nomeação  da  Representação  Discente  dos
órgãos colegiados da Faculdade de Medicina
da UFRGS
O PRÓ-REITOR DE ASSUNTOS ESTUDANTIS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL,  no uso de suas atribuições,  considerando o disposto na Portaria nº  7625,  de 29 de
setembro de 2016
RESOLVE
Nomear a Representação Discente eleita na Faculdade de Medicina, com mandato de 01 (um) ano, a
contar  de  18  de  dezembro  de  2018,  atendendo  o  disposto  nos  artigos  175  do  Regimento  Geral  da
Universidade  e  79  do  Estatuto  da  Universidade,  e  considerando o  processo  nº  23078.534843/2018-44,
conforme segue:
 
Comissão de Graduação
Roni Simão
Ricardo Scherer
Vitória Rech Astolfi
 
Conselho da Unidade
Roni Simão
 
Comissão de Pesquisa
Ana Maria Delgado Cunha
 
Departamento de Medicina Interna
Otávio Augusto Gonçalves Dias Cionek
Vitória Rech Astolfi
 
Departamento de Medicina Social
André Luís Marques da Silveira
 
Departamento de Psiquiatria e Medicina Legal
Júlia Stocchero Amaro
Documento gerado sob autenticação Nº FZA.114.031.OU6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
 
Departamento de Pediatria e Puericultura
André Luís Marques da Silveira
Gabriel Henrique Colpes
 
Departamento de Patologia
Ana Maria Delgado Cunha
 
 
ELTON LUIS BERNARDI CAMPANARO
Pró-Reitor de Assuntos Estudantis em exercício
