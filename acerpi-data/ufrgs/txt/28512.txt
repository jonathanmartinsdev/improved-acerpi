Documento gerado sob autenticação Nº UVT.450.993.UGI, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7720                  de  03/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  08/09/2016,   referente  ao  interstício  de
14/02/2015 a 07/09/2016, para o servidor RENAN ZIGLIOLI DE SOUSA, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 1820272,  lotado  na  Pró-Reitoria de Gestão de Pessoas, passando
do Nível de Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.018606/2016-79:
Formação Integral de Servidores da UFRGS III CH: 78 (01/09/2014 a 17/06/2016)
Fundação Bradesco - Introdução à Informática CH: 20 (08/09/2016 a 08/09/2016)
Fundação  Bradesco  -  Internet  Explorer  8.0  CH:  32  Carga  horária  utilizada:  22  hora(s)  /  Carga  horária
excedente: 10 hora(s) (08/09/2016 a 08/09/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
