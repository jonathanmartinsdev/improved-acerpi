Documento gerado sob autenticação Nº OYW.101.798.O6D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11415                  de  23/12/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Exonerar, a pedido, a partir de 16 de dezembro de 2019, nos termos do artigo 34 "caput", da Lei n°
8.112, de 1990, MORGANA CRISTINA ARNOLD, ocupante do cargo de Assistente em Administração, código
701200, nível de classificação D, nível de capacitação II, padrão 02, do Quadro de Pessoal, lotada e com
exercício na Pró-Reitoria de Gestão de Pessoas. Processo nº 23078.534393/2019-71.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
