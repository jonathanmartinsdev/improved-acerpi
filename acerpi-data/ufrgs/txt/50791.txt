Documento gerado sob autenticação Nº ZZS.004.225.E6J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1254                  de  08/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
            Autorizar o afastamento no País de LAURA NELLY MANSUR SERRES, ocupante do cargo de Professor
do  Magistério  do  Ensino  Básico,  Técnico  e  Tecnológico,  lotada  e  com  exercício  no  Departamento  de
Comunicação do Colégio de Aplicação, com a finalidade de realizar estudos em nível de Doutorado, junto à
Universidade Federal do Rio Grande do Sul, no período compreendido entre 01/03/2018 e 28/02/2019, com
ônus limitado. Processo 23078.522805/2017-68.
RUI VICENTE OPPERMANN
Reitor.
