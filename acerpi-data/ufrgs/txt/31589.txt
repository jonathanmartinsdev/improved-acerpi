Documento gerado sob autenticação Nº KML.735.053.C77, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10036                  de  19/12/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, SUELY DADALTI FRAGOSO, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 71/2015 de 4 de Dezembro de 2015 homologado em 08 de
dezembro de 2015 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei
12.772,  de 28 de dezembro de 2012,  com redação dada pela Lei  12.863,  de 24 de setembro de 2013,
publicada no Diário Oficial da União de 25 de setembro de 2013, para o cargo isolado de provimento efetivo
de PROFESSOR TITULAR-LIVRE DO MAGISTÉRIO SUPERIOR do Plano de Carreiras e Cargos do Magistério
Federal, do Quadro de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto
ao Departamento de Comunicação da Faculdade de Biblioteconomia e Comunicação, em vaga decorrente de
Criação de Cargos, código nº 992551, ocorrida em 05 de junho de 2014, conforme Portaria nº. 490/2014 de
04/06/2014, publicada no Diário Oficial da União de 05/06/2014. Processo nº. 23078.035968/2014-62.
RUI VICENTE OPPERMANN
Reitor
