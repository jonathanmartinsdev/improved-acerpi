Documento gerado sob autenticação Nº OIG.334.309.D9A, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6952                  de  01/08/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Retificar a Portaria n° 6923/2017, de 31/07/2017, que removeu a servidora NATASCHA HELENA
FRANZ HOPPEN,  Bibliotecário-documentalista  da  Faculdade de  Biblioteconomia  e  Comunicação para  a
lotação Faculdade de Biblioteconomia e Comunicação, com novo exercício na Biblioteca da Faculdade de
Biblioteconomia e Comunicação.
 
 
Onde se lê:
da Faculdade de Biblioteconomia e Comunicação para a lotação Faculdade de Biblioteconomia e
Comunicação,
leia-se:
do  Instituto  de  Ciências  Básicas  da  Saúde  para  a  lotação  Faculdade  de  Biblioteconomia  e
Comunicação.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
