Documento gerado sob autenticação Nº XIS.841.156.9JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             254                  de  09/01/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por decisão judicial  proferida no processo nº 5059205-33.2018.4.04.7100,  da 10ª Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, do servidor IVAN ROBERTO BECHER MACHADO, matrícula SIAPE n° 0381404, ativo no cargo de 
Assistente em Administração - 701200, para o nível III, conforme o Processo nº 23078.535456/2018-25.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
Conceder  progressão  por  capacitação,  a  contar  de  05/12/2016,  passando  do  Nível  de
Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de  Capacitação  D  IV.
RUI VICENTE OPPERMANN
Reitor
