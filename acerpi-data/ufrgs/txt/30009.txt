Documento gerado sob autenticação Nº KWF.067.104.QK3, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8815                  de  31/10/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLAUDIA PIANTA COSTA CABRAL,  Professor do Magistério
Superior, lotada no Departamento de Arquitetura da Faculdade de Arquitetura e com exercício no Programa
de Pós-Graduação em Arquitetura, com a finalidade de participar do "Seminario Internacional: La Arquitectura
de la gran ciudad II: Relaciones entre arquitectura y ciudad", em Santiago, Chile, no período compreendido entre
20/11/2016 e 22/11/2016, incluído trânsito, com ônus CAPES-PPCP-MERCOSUL. Solicitação nº 24501.
RUI VICENTE OPPERMANN
Reitor
