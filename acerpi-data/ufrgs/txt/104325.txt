Documento gerado sob autenticação Nº OHQ.390.000.I7H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             901                  de  28/01/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar, por decisão judicial proferida no processo nº 5024986-91.2018.4.04.7100, a Portaria n°
1486/2019, de 11/02/2019 que concedeu progressão funcional, por avaliação de desempenho, no Quadro
desta Universidade, ao Professor ROGERIO PASSOS SEVERO, matrícula SIAPE 1721916, lotado e em exercício
no Departamento de Filosofia do Instituto de Filosofia e Ciências Humanas, da classe A de Professor Adjunto
A, nível 01, para a classe A de Professor Adjunto A, nível 02, com vigência financeira a partir de 19/11/2018, de
acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas alterações, Portaria nº 554,
de 20 de junho de 2013 do Ministério da Educação e a Decisão nº 331/2017. Processo nº 23078.524427/2018-
38.
 
Onde se lê:
...da classe A de Professor Adjunto A, nível 01, para a classe A de Professor Adjunto A, nível 02, com
vigência financeira a partir de 19/11/2018...
 
Leia-se:
...da classe C de Professor Adjunto, nível 01, para a classe C de Professor Adjunto, nível 02, com
vigência financeira a partir de 18/08/2018, ficando ratificados os demais termos.
 
 
 
 
RUI VICENTE OPPERMANN
Documento gerado sob autenticação Nº OHQ.390.000.I7H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
Reitor.
