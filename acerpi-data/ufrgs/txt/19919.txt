Documento gerado sob autenticação Nº BXK.634.887.SBE, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2331                  de  29/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005,  publicada no Diário Oficial  da União do dia 6 subsequente,  a  JULIO CESAR RUIZ
CLAEYSSEN, matrícula SIAPE nº 0353368, no cargo de Professor Titular da Carreira do Magistério Superior, do
Quadro  desta  Universidade,  no  regime  de  dedicação  exclusiva,  com  exercício  no  Departamento  de
Matemática Pura e Aplicada do Instituto de Matemática e Estatística, com proventos integrais. Processo
23078.200467/2016-25.
CARLOS ALEXANDRE NETTO
Reitor
