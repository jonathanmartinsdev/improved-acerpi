Documento gerado sob autenticação Nº CTC.020.926.5BU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10469                  de  21/11/2019
Nomeação  da  Representação  Discente
do  Programa  de  Pós-Graduação  em
Computação  do  Instituto  de  Informática  da
UFRGS
O PRÓ-REITOR DE ASSUNTOS ESTUDANTIS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL,  no uso de suas atribuições,  considerando o disposto na Portaria nº  7625,  de 29 de
setembro de 2016
RESOLVE
Nomear  a  Representação  Discente  eleita  para  compor  o  Programa  de  Pós-Graduação  em
Computação no Instituto de Informática, com mandato de 01 (um) ano, a contar de 21 de outubro de 2019,
atendendo  o  disposto  nos  artigos  175  do  Regimento  Geral  da  Universidade  e  79  do  Estatuto  da
Universidade, e considerando o processo nº 23078.528835/2019-40, conforme segue:
 
Conselho do Instituto de Informática   
Titular: Cauã Roca Antunes
Suplente: Carlos Francisco Habekost dos Santos              
 
Conselho do PPGC         
Titular: Carlos Francisco Habekost dos Santos
Titular: Cauã Roca Antunes
 
Comissão do PPGC        
Titular: Carlos Francisco Habekost dos Santos
Suplente: Cauã Roca Antunes
ELTON LUIS BERNARDI CAMPANARO
Pró-Reitor de Assuntos Estudantis em exercício
