Documento gerado sob autenticação Nº JQW.935.420.FDT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5743                  de  30/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  28/06/2017,
correspondente ao grau Insalubridade Máxima, ao servidor ALEX SANDER DA ROSA ARAUJO, Identificação
Única 16955390, Professor do Magistério Superior, com exercício no Departamento de Fisiologia do Instituto
de Ciências Básicas da Saúde, observando-se o disposto na Lei nº 8.112, de 11 de dezembro de 1990,
combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas
Insalubres conforme Laudo Pericial constante no Processo nº 23078.507798/2017-74, Código SRH n° 23164 e
Código SIAPE 2017002343.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
