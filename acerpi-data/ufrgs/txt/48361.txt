Documento gerado sob autenticação Nº OLD.173.571.A5I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11388                  de  21/12/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  CLAUDIA  LUISA  ZEFERINO  PIRES,  matrícula  SIAPE  n°  1852157,  lotada  no
Departamento  de  Geografia  do  Instituto  de  Geociências,  como  Coordenadora  Substituta  do  PPG  em
Geografia, para substituir automaticamente o titular desta função em seus afastamentos ou impedimentos
regulamentares na vigência do presente mandato, por ter sido reeleita. Processo nº 23078.523985/2017-03.
JANE FRAGA TUTIKIAN
Vice-Reitora.
