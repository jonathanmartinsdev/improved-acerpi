Documento gerado sob autenticação Nº FNX.492.771.M0L, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7104                  de  03/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°29858,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, ocupante do cargo de ASSISTENTE EM ADMINISTRAÇÃO,
do Quadro de Pessoal desta Universidade, GUSTAVO PEREIRA (Siape: 2267363 ),  para substituir   JOSE
VICENTE  TAVARES  DOS  SANTOS  (Siape:  0353328  ),  Diretor  do  Instituto  Latino-Americano  de  Estudos
Avançados, Código CD-4, em seu afastamento no país,  no período de 17/07/2017 a 20/07/2017, com o
decorrente pagamento das vantagens por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
