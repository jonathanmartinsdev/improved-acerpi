Documento gerado sob autenticação Nº SQC.427.406.U2K, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9040                  de  28/09/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  30/09/2017,   referente  ao  interstício  de
30/03/2017 a 29/09/2017, para o servidor GUSTAVO AURELIO DA SILVA, ocupante do cargo de Técnico de
Laboratório Área - 701244, matrícula SIAPE 1066643,  lotado  no  Instituto de Química, passando do Nível de
Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.516108/2017-78:
Formação Integral de Servidores da UFRGS III CH: 74 (19/04/2017 a 14/08/2017)
ILB - Ética e Administração Pública CH: 40 (29/08/2017 a 18/09/2017)
ILB - Excelência no Atendimento CH: 6 (01/10/2015 a 21/10/2015)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
