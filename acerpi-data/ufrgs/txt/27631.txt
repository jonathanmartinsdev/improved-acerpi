Documento gerado sob autenticação Nº JRE.970.705.FQD, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7245                  de  14/09/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme o Laudo Médico n°40288,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  BIBLIOTECÁRIO-
DOCUMENTALISTA, do Quadro de Pessoal desta Universidade, LEONARDO FERREIRA SCAGLIONI (Siape:
1459657 ),  para substituir   MARINA PLENTZ (Siape: 1849199 ), Chefe da Biblioteca de Ciências Sociais e
Humanidades, Código FG-5, em seu afastamento por motivo de Laudo Médico do titular da Função, no dia
19/08/2016, com o decorrente pagamento das vantagens por 1 dia.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
