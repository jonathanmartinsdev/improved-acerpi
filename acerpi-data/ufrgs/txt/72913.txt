Documento gerado sob autenticação Nº DTJ.264.114.M69, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9720                  de  30/11/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor  LUIZ  CARLOS DA SILVA SCHWINDT,  matrícula  SIAPE n°  1363366,  lotado e  em exercício  no
Departamento de Linguística, Filologia e Teoria Literária, da classe D  de Professor Associado, nível 04, para a
classe E  de Professor Titular,  referente ao interstício de 07/10/2016 a 21/11/2018, com vigência financeira a
partir de 22/11/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas
alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.525986/2018-65.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
