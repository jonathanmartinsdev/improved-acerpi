Documento gerado sob autenticação Nº FAI.283.081.LVV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3383                  de  09/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  CAROLINE  SOARES  DE  ABREU,  Professor  do  Magistério
Superior,  lotada  no  Departamento  de  Música  do  Instituto  de  Artes  e  com  exercício  na  Comissão  de
Graduação de Música, com a finalidade de participar do "XIII Congreso de la rama latinoamericana de la
Asociación  Internacional  para  Estudio  de  la  Música  Popular",  em  San  Juan,  Porto  Rico,  no  período
compreendido entre 09/06/2018 e 18/06/2018, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa -
diárias, Instituto de Artes - passagens). Solicitação nº 45216.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
