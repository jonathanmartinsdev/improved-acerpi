Documento gerado sob autenticação Nº CHJ.090.260.1JB, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4880                  de  01/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Declarar vago, a partir de 17/05/2017, o cargo de Diretora do Planetário, Código SRH 982, Código
CD-4, desta Universidade, tendo em vista a aposentadoria de MARIA HELENA STEFFANI, matrícula SIAPE n°
0354071, conforme Portaria n° 4256/2017, de 15 de maio de 2017, publicada no Diário Oficial da União do dia
17/05/2017. Processo nº 23078.010300/2017-55.
JANE FRAGA TUTIKIAN
Vice-Reitora.
