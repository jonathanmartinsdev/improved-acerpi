Documento gerado sob autenticação Nº GNB.481.505.QIB, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             344                  de  12/01/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  21/12/2016,   referente  ao  interstício  de
01/04/2009 a 20/12/2016, para o servidor JOSE VILMAR ABREU CORREA, ocupante do cargo de Pedreiro -
701649,  matrícula  SIAPE  0358234,   lotado   na   Prefeitura  Campus  Centro  da  Superintendência  de
Infraestrutura,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  B  II,  para  o  Nível  de
Classificação/Nível de Capacitação B III,  em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.025988/2016-97:
Formação Integral de Servidores da UFRGS III CH: 60 (08/09/2016 a 25/11/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
