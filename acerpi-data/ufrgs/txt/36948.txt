Documento gerado sob autenticação Nº DVS.338.182.78J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3259                  de  17/04/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de GIOVANNI ABRAHÃO SALUM JUNIOR, Professor do Magistério
Superior, lotado e em exercício no Departamento de Psiquiatria e Medicina Legal da Faculdade de Medicina,
com a finalidade de participar do "72nd Annual Meeting of the Society of Biological Psychiatry", em San
Diego, Estados Unidos, no período compreendido entre 17/05/2017 e 20/05/2017, incluído trânsito, com ônus
limitado. Solicitação nº 27300.
RUI VICENTE OPPERMANN
Reitor
