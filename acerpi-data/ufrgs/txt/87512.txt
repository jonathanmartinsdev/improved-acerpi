Documento gerado sob autenticação Nº HEH.839.729.8VL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1971                  de  27/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Autorizar  a  prorrogação  de  afastamento  no  País  de  LISINEI  FATIMA  DIEGUEZ  RODRIGUES,
Professor do Magistério do Ensino Básico, Técnico e Tecnológico, lotada  no  Colégio de Aplicação e com
exercício  no  Departamento  de  Expressão  e  Movimento  do  Colégio  de  Aplicação,  com a  finalidade  de
continuar os estudos em nível de Doutorado junto à Universidade Federal do Rio Grande do Sul, em Porto
Alegre, Rio Grande do Sul, no período compreendido entre 07/03/2019 e 06/03/2020, com ônus limitado.
Processo nº 23078.500160/2017-11.
RUI VICENTE OPPERMANN
Reitor.
