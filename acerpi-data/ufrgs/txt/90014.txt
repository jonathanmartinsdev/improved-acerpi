Documento gerado sob autenticação Nº JGY.815.735.MUK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             3446                  de  23/04/2019
Nomeação  da  Representação  Discente  do
Diretório  Acadêmico  dos  Estudantes  de
Farmácia  da  UFRGS
A PRÓ-REITORA DE ASSUNTOS ESTUDANTIS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7623, de 29 de setembro de 2016
RESOLVE
Nomear a Representação Discente eleita para compor o Diretório Acadêmico dos Estudantes de
Farmácia, com mandato de 01 (um) ano, a contar de 09 de dezembro de 2018, atendendo ao disposto nos
artigos 175 e 178 do Regimento Geral da Universidade e 79 do Estatuto da Universidade e considerando o
processo nº 23078.507739/2019-68, conforme segue:
 
Coordenadores Gerais
Carolina Gessinger Bertó
Julia Werner Vieira
Natália Azeredo Paim
 
Secretária
Thassia Oliveira Lemos
 
Tesoureira
Bruna Bernar Dias
 
Coordenadora de Comunicação
Rosana Thalia Meregalli
 
Comissão de Eventos
Bianca Elingson da Silva Costa
Deise Reis Carvalho
Eliane Oliveira Salines Duarte
Natally da Silveira Barão Toson
Vitória Schmidt Caron
Documento gerado sob autenticação Nº JGY.815.735.MUK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
 
Comissão de Esportes
Cleofas Sates Manfio
Guilherme Secanho do Prado
Jéssica Brzoskowski Longaray
 
Comissão de Infraestrutura
Gabriel Chiomento da Motta
Júlia Zanotto Hoffmann
Laura Siuch Vargas
Manoela Almeida Martins Mace
Pedro Fortes Bartolomeu
Ricardo Haack Amaral Roppa
Victória Tedesco Ramos
 
Comissão de Trote
Roberta Barbizan Mascarello
Rodrigo Moraes Carlesso
SUZI ALVES CAMEY
Pró-Reitora de Assuntos Estudantis
