Documento gerado sob autenticação Nº IXK.560.020.HFM, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3644                  de  29/04/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  EDUARDO  SANTOS  NEUMANN,  Professor  do  Magistério
Superior, lotado no Departamento de História do Instituto de Filosofia e Ciências Humanas e com exercício
no Programa de Pós-Graduação em História, com a finalidade de realizar visita ao Instituto de altos Estudios
Sociales, em Buenos Aires, Argentina, no período compreendido entre 20/05/2019 e 01/06/2019, incluído
trânsito, com ônus limitado. Solicitação nº 83435.
RUI VICENTE OPPERMANN
Reitor
