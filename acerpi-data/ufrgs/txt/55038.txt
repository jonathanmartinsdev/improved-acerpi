Documento gerado sob autenticação Nº RTM.289.266.V2T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4099                  de  06/06/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder o Adicional de Periculosidade, no percentual de 10%, a partir de 27/04/2018, ao servidor
VILIBALDO SCHULER, Identificação Única 3557537, Técnico de Tecnologia da Informação, com exercício no
Núcleo de Operação e Controle do Centro de Processamento de Dados, observando-se o disposto na Lei nº
8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer
atividades  em  áreas  consideradas  Perigosas  conforme  Laudo  Pericial  constante  no  Processo  n  º
23078.509899/2018-61, Código SRH n° 23526 e Código SIAPE n° 26244-000.042/2018.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
