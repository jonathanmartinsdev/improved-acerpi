Documento gerado sob autenticação Nº OTW.442.041.8VL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1793                  de  22/02/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do País  de  LISIANE FEITEN WINGERT ODY,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Direito Privado e Processo Civil da Faculdade de Direito,
com a finalidade de participar do "Annual Oxford International Intellectual Property Law Moot Competition",
em Oxford, Inglaterra, no período compreendido entre 12/03/2019 e 18/03/2019, incluído trânsito, com ônus
limitado. Solicitação nº 62066.
RUI VICENTE OPPERMANN
Reitor
