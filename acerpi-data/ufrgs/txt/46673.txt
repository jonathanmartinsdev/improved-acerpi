Documento gerado sob autenticação Nº RFK.858.778.P3L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10116                  de  01/11/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  HELENA  ARAUJO  RODRIGUES  KANAAN,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Artes Visuais do Instituto de Artes, com a
finalidade de participar do "1º Encuentro Internacional de Litografía", em Buenos Aires, Argentina, no período
compreendido entre 15/11/2017 e 19/11/2017, incluído trânsito, com ônus limitado. Solicitação nº 31979.
RUI VICENTE OPPERMANN
Reitor
