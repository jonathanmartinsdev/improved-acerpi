Documento gerado sob autenticação Nº BRJ.386.871.SNF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1441                  de  11/02/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar SILVIO LUIS DE OLIVEIRA, Matrícula SIAPE 1031664, ocupante do cargo de Auxiliar de
Agropecuária,  Código  701611,  do  Quadro  de  Pessoal  desta  Universidade,  para  exercer  a  função  de
Coordenador do Núcleo Técnico-Científico do Campus Litoral Norte, Código FG-2, Código SRH 1418, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.501467/2020-26.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
