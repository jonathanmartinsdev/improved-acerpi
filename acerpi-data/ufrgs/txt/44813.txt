Documento gerado sob autenticação Nº TSS.882.368.15K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8733                  de  19/09/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de
2016 e  tendo em vista  o  que consta  do Processo Administrativo n°  23078.515821/2016-13,  do Pregão
Eletrônico SRP nº 047/2017, da Lei 10.520/02 e ainda da Lei 8.666/93,
          RESOLVE:
 
        Aplicar a sanção administrativa de IMPEDIMENTO DE LICITAR E CONTRATAR COM A UNIÃO PELO PRAZO
DE 24 (VINTE E QUATRO) MESES, prevista no item 29.9 do referido Edital de Licitação, à Empresa MAURO
ANADÃO TOCCHIO, CNPJ n.º 10.301.298/0001-23, pela apresentação de declaração falsa, conforme atestado
pelo DELIT no documento SEI 0536800 bem como pelo NUDECON no documento SEI 0681549 do processo
supracitado.
 
 
        Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG.
 
 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
