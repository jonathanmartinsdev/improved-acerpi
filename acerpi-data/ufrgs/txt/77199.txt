Documento gerado sob autenticação Nº TWZ.542.973.Q6H, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1757                  de  21/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições,  considerando o disposto na Portaria nº  4183,  de 17 de outubro de 2008,  do
Magnífico Reitor, e conforme a Solicitação de Férias n°48958,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do Quadro de Pessoal  desta Universidade,  JULIO BALDISSEROTTO (Siape:  0358677 ),   para
substituir   LUIZ FERNANDO WALBER (Siape: 0357871 ), Coordenador da Comissão de Extensão da Faculdade
de Odontologia, em seu afastamento por motivo de férias, no período de 21/02/2019 a 05/03/2019.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
