Documento gerado sob autenticação Nº SBA.939.065.I7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             756                  de  22/01/2020
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de SERGIO BAMPI, Professor do Magistério Superior, lotado e em
exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de  Informática,  com  a  finalidade  de
participar de reunião junto ao Institute of Electrical and Electronics Engineers, em Houston, Estados Unidos,
no período compreendido entre 05/02/2020 e 06/02/2020, incluído trânsito, com ônus limitado. Solicitação nº
89630.
RUI VICENTE OPPERMANN
Reitor
