Documento gerado sob autenticação Nº EPZ.679.008.8VL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1718                  de  19/02/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  LETICIA  MARIA  SCHABBACH,  matrícula  SIAPE  n°  1769944,  lotada  no
Departamento  de  Sociologia  do  Instituto  de  Filosofia  e  Ciências  Humanas,  para  exercer  a  função  de
Coordenadora do PPG em Segurança Cidadã,  Código SRH 1526,  código FUC,  com vigência  a  partir  de
21/02/2019 até 20/02/2021. Processo nº 23078.503885/2019-14.
RUI VICENTE OPPERMANN
Reitor.
