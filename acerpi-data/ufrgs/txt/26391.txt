Documento gerado sob autenticação Nº EOG.999.126.ICV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6298                  de  22/08/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  CESAR  AUGUSTO  ZEN  VASCONCELLOS,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Física do Instituto de Física, com a finalidade
de realizar  visita  à Pontificia Universidade Católica del Peru, em Lima, Peru, no período compreendido entre
09/09/2016 e 18/09/2016, incluído trânsito, com ônus limitado. Solicitação nº 22254.
CARLOS ALEXANDRE NETTO
Reitor
