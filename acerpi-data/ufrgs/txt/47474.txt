Documento gerado sob autenticação Nº XAL.950.297.GL6, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10712                  de  24/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar,  a  partir  da data  de publicação no Diário  Oficial  da União,a  ocupante do cargo de
Administrador - 701001, do Nível de Classificação EIV, do Quadro de Pessoal desta Universidade, ANA PAULA
MATEI,  matrícula  SIAPE  1550263  da  função  de  Assessora  de  Inserção  e  Inovação  da  Secretaria  de
Desenvolvimento Tecnológico, Código SRH 1339, Código FG-1, para a qual foi designada pela Portaria nº
8510/14 de 14/11/2014, publicada no Diário Oficial da União de 24/11/2014, por ter sido designada para
outra função gratificada. Processo nº 23078.521120/2017-02.
RUI VICENTE OPPERMANN
Reitor.
