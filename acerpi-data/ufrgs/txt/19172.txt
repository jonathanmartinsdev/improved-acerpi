Documento gerado sob autenticação Nº XPG.243.694.5FI, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2054                  de  17/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de Outubro de 2012
RESOLVE:
Conceder  Progressão  por  Capacitação,  a  contar  de  02/03/2016,   referente  ao  interstício  de
17/09/2012 a 01/03/2016, para a servidora EVELYSE RAMOS ITAQUI HERNANDEZ, ocupante do cargo de
Técnico em Assuntos Educacionais - 701079, matrícula SIAPE 1853798,  lotada  na  Secretaria de Educação a
Distância, passando do Nível de Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de
Capacitação  E  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.003996/2016-82:
Formação Integral de Servidores da UFRGS I CH: 34 (13/09/2012 a 11/05/2015)
PROREXT - Capacitação em Tutoria (UAB/UFRGS) - 2013 CH: 52 (01/10/2013 a 05/12/2013)
NELE - Inglês Geral Básico - Programa Inglês sem Fronteiras CH: 100 Carga horária utilizada: 64 hora(s) /
Carga horária excedente: 36 hora(s) (18/05/2015 a 30/11/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
