Documento gerado sob autenticação Nº QSQ.133.301.F83, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10066                  de  31/10/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  SANDRA  DE  FATIMA  BATISTA  DE  DEUS,  Professor  do
Magistério  Superior,  lotada  no  Departamento  de  Comunicação  da  Faculdade  de  Biblioteconomia  e
Comunicação e com exercício na Pró-Reitoria de Extensão, com a finalidade de participar do "11no Congreso
Internacional de Educación Superior: La universidad y la agenda 2030 para el desarrollo sostenible", em Habana,
Cuba, no período compreendido entre 11/02/2018 e 17/02/2018, incluído trânsito, com ônus UFRGS (Pró-
Reitoria de Extensão - diárias e passagens). Solicitação nº 32496.
RUI VICENTE OPPERMANN
Reitor
