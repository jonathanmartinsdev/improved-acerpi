Documento gerado sob autenticação Nº XWX.250.934.P57, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8885                  de  04/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Excluir a vantagem prevista no artigo 2º da Lei nº 8.911, de 11 de julho de 1994, dos proventos
decorrentes da aposentadoria concedida através da portaria nº 1.684, de 3 de abril de 2009, publicada no
Diário Oficial da União do dia 9 subsequente, a HÉLGIO HENRIQUE CASSES TRINDADE,  matrícula SIAPE
352632, conforme determinação judicial contida no proceso nº 5069581-20.2014.4.04.7100/RS. Processo nº
23078.035598/12-01.
 
 
 
RUI VICENTE OPPERMANN
Reitor
