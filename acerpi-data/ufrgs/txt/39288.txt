Documento gerado sob autenticação Nº IHP.358.677.C2V, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5079                  de  08/06/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, GUSTAVO GIL DA SILVEIRA, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 32/2017 de 19 de Maio de 2017, homologado em 22 de maio
de 2017 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de
28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do
Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro
de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento
de Física do Instituto de Física, em vaga decorrente da aposentadoria do Professor Flavio Horowitz, código nº
275158, ocorrida em 19 de Maio de 2017, conforme Portaria nº. 4403 de 18 de maio de 2017, publicada no
Diário Oficial da União de 19 de maio de 2017. Processo nº. 23078.507484/2016-91.
RUI VICENTE OPPERMANN
Reitor.
