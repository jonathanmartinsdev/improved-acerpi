Documento gerado sob autenticação Nº MUG.156.583.OJK, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3357                  de  04/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de LUIS ARMANDO GANDIN, Professor do Magistério Superior,
lotado e em exercício no Departamento de Estudos Básicos da Faculdade de Educação, com a finalidade de
participar de reunião na Universidade de Santiago, Chile,  no período compreendido entre 22/05/2016 e
26/05/2016, incluído trânsito, com ônus limitado. Solicitação nº 19776.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
