Documento gerado sob autenticação Nº PND.886.952.IK0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9617                  de  29/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  23/11/2018,   referente  ao  interstício  de
23/05/2017 a 22/11/2018, para a servidora LILIANE DELLA LIBERA, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 2401030,  lotada  na  Pró-Reitoria de Graduação, passando do Nível
de Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude
de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.531340/2018-17:
Formação Integral de Servidores da UFRGS III CH: 83 (07/08/2017 a 18/12/2017)
ENAP - Controles Institucional e Social dos Gastos Públicos CH: 20 Carga horária utilizada: 7 hora(s) / Carga
horária excedente: 13 hora(s) (28/11/2017 a 18/12/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
