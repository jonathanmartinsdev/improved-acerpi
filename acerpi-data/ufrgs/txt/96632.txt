Documento gerado sob autenticação Nº FPA.010.728.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7505                  de  19/08/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5043128-12.2019.4.04.7100,  da  3ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, do servidor JORGE LUIS DAS NEVES ANTUNES,  matrícula SIAPE n° 0353130, aposentado no
cargo de Técnico em Contabilidade - 701224, do nível I para o nível II, a contar de 01/01/2006, conforme o
Processo nº 23078.521471/2019-77.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
 
           Conceder  progressão  por  capacitação,  a  contar  de  22/09/2010,  passando  do  Nível  de
Classificação/Nível de Capacitação D II para o Nível de Classificação/Nível de Capacitação D III.
 
           Conceder  progressão  por  capacitação,  a  contar  de  20/12/2013 ,  passando  do  Nível  de
Classificação/Nível de Capacitação D III para o Nível de Classificação/Nível de Capacitação D IV.
 
RUI VICENTE OPPERMANN
Reitor
