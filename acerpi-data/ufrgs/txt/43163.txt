Documento gerado sob autenticação Nº MUL.703.359.K5S, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7767                  de  18/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°37910,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, RAFAEL DA ROCHA RIBEIRO (Siape: 2133718 ),  para
substituir   FRANCISCO ELISEU AQUINO (Siape: 1217090 ), Diretor do Centro Polar e Climático do Instituto de
Geociências, Código FG-1, em seu afastamento por motivo de férias, no período de 08/08/2017 a 20/08/2017,
com o decorrente pagamento das vantagens por 13 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
