Documento gerado sob autenticação Nº TGH.242.910.BHD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10167                  de  23/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor LUCIO ALBINO AMARO DA SILVA, ocupante do cargo de  Engenheiro-área -
701031, lotado na Superintendência de Infraestrutura, SIAPE 2346610, o percentual de 52% (cinquenta e dois
por cento) de Incentivo à Qualificação, a contar de 21/11/2016, tendo em vista a conclusão do Curso de
Mestrado  em  Engenharia  Mecânica  -  Área  de  Projeto  e  Fabricação,  conforme  o  Processo  nº
23078.024697/2016-81.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
