Documento gerado sob autenticação Nº PLX.388.710.4B3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10322                  de  08/11/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  MARILISE  OLIVEIRA MESQUITA,  Professor  do  Magistério
Superior,  lotada e em exercício no Departamento de Assistência e Orientação Profissional da Escola de
Enfermagem, com a finalidade do "XXXI Congreso de la  Asociación Latinoamericana de Sociología", em
Montevidéu, Uruguai, no período compreendido entre 03/12/2017 e 09/12/2017, incluído trânsito, com ônus
CAPES/PROAP. Solicitação nº 30070.
RUI VICENTE OPPERMANN
Reitor
