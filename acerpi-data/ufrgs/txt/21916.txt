Documento gerado sob autenticação Nº QGU.559.895.0LV, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3410                  de  10/05/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Declarar vago, a partir de 22 de março de 2016, o cargo de Assistente em Administração, Código
701200, Nível de Classificação D, Nível de Capacitação I, Padrão 01, do Quadro de Pessoal, em decorrência de
exoneração de cargo efetivo a pedido prevista no Art. 34º da Lei 8.112, de 11 de dezembro de 1990, de
RAFAEL PIZZOLATO com lotação na Secretaria de Educação a Distância. Processo nº 23078.006270/2016-00.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
