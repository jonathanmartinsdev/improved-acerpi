Documento gerado sob autenticação Nº TCF.868.636.L81, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7560                  de  29/09/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Autorizar  o  afastamento  do  país  de  ORLANDO  CARLOS  BELMONTE  WENDER,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Cirurgia da Faculdade de Medicina, com a
finalidade de participar do "30th Annual Meeting of the  European Association for Cardio-Thoracic Surgery",
em Barcelona, Espanha, no período compreendido entre 29/09/2016 e 07/10/2016, incluído trânsito, com
ônus limitado. Solicitação nº 23450.
RUI VICENTE OPPERMANN
Reitor
