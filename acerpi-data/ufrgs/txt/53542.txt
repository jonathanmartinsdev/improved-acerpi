Documento gerado sob autenticação Nº KBN.233.244.6FV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2977                  de  24/04/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora ENAURA HELENA BRANDAO CHAVES,  matrícula SIAPE n° 0356048, lotada e em exercício no
Departamento de Assistência e Orientação Profissional da Escola de Enfermagem, da classe C  de Professor
Adjunto, nível 04, para a classe D  de Professor Associado, nível 01, referente ao interstício de 18/12/2014 a
17/12/2016, com vigência financeira a partir de 06/04/2018, de acordo com o que dispõe a Lei 12.772 de 28
de  dezembro  de  2012,  com  suas  alterações  e  a  Decisão  nº  331/2017  do  CONSUN.  Processo  nº
23078.501672/2017-96.
PHILIPPE OLIVIER ALEXANDRE NAVAUX
Decano do Conselho Universitário, no exercício da Reitoria.
