Documento gerado sob autenticação Nº YDP.278.974.PL4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2943                  de  23/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°45238,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do Quadro de Pessoal  desta Universidade,  ALFREDO DE JESUS DAL MOLIN FLORES (Siape:
1565112 ),  para substituir   RODRIGO VALIN DE OLIVEIRA (Siape: 1998486 ), Diretor do Serviço de Pesquisa e
Preparação Profissional, Código FG-5, em seu afastamento no país, no período de 25/04/2018 a 28/04/2018,
com o decorrente pagamento das vantagens por 4 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
