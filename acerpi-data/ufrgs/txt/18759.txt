Documento gerado sob autenticação Nº YHX.653.655.MGB, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1795                  de  09/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de Outubro de 2012
RESOLVE:
Conceder  à  servidora  LUIZA  DE  MELO  MONTEIRO,  ocupante  do  cargo  de   Assistente  em
Administração - 701200, lotada na Pró-Reitoria de Gestão de Pessoas, SIAPE 2157428, o percentual de 25%
(vinte e cinco por cento) de Incentivo à Qualificação, a contar de 29/02/2016, tendo em vista a conclusão do
curso de Graduação em Letras - Licenciatura, conforme o Processo nº 23078.003351/2016-40.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
