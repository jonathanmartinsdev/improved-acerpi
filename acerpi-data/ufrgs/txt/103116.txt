Documento gerado sob autenticação Nº NNK.599.169.LFR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             75                  de  03/01/2020
A VICE REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Autorizar o afastamento no País de VITOR PINHEIRO GRUNVALD, ocupante do cargo de Professor
do Magistério Superior, lotado e em exercício no Departamento de Antropologia do Instituto de Filosofia e
Ciências Humanas,  com a finalidade de realizar visita e ministrar curso junto à Universidade Federal do Pará,
em Belém, Brasil, no período compreendido entre 02/01/2020 e 31/01/2020, incluído trânsito, com ônus
limitado. Solicitação n° 89525.
JANE FRAGA TUTIKIAN
Vice-Reitora.
