Documento gerado sob autenticação Nº GBQ.174.271.REG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1164                  de  01/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°47503,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de TÉCNICO EM ASSUNTOS
EDUCACIONAIS, do Quadro de Pessoal desta Universidade, PATRICIA SILVEIRA DA COSTA (Siape: 1681888 ),
 para substituir    VINICIUS RIBEIRO (Siape:  1446263 ),  Chefe do Setor  Acadêmico da GA da Escola  de
Enfermagem,  Código  FG-7,  em  seu  afastamento  por  motivo  de  férias,  no  período  de  04/02/2019  a
15/02/2019, com o decorrente pagamento das vantagens por 12 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
