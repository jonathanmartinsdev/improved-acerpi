Documento gerado sob autenticação Nº ZLV.073.819.SHS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1721                  de  28/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Autorizar a prorrogação de afastamento no País de CLARISSA SARTORI ZIEBELL,  Professor do
Magistério Superior, lotada  e com exercício no Departamento de Design e Expressão Gráfica da Faculdade
de Arquitetura, com a finalidade de continuar estudos em nível de Doutorado, junto à Universidade Federal
do  Rio  Grande do Sul,  no  período compreendido entre  01/03/2018 e  31/07/2018,  com ônus  limitado.
Processo nº 23078.510442/2016-37.
RUI VICENTE OPPERMANN
Reitor.
