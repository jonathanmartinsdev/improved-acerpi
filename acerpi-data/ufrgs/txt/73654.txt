Documento gerado sob autenticação Nº LKJ.269.829.O9F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10202                  de  17/12/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  12/12/2018,   referente  ao  interstício  de
02/12/2016 a 11/12/2018, para a servidora JULIANA TROLEIS, ocupante do cargo de Técnico de Laboratório
Área -  701244,  matrícula  SIAPE 1654424,   lotada  no   Instituto  de  Biociências,  passando do Nível  de
Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.533883/2018-79:
Formação Integral de Servidores da UFRGS IV CH: 98 (01/03/2010 a 23/10/2018)
UFRGS - Meios ópticos e eletrônicos no estudo da estrutura vegetal CH: 60 Carga horária utilizada: 52 hora(s)
/ Carga horária excedente: 8 hora(s) (03/12/2018 a 12/12/2018)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
