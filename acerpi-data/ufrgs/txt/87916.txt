Documento gerado sob autenticação Nº UQA.780.392.GLG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2179                  de  11/03/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°48599,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de SOCIÓLOGO, do Quadro de
Pessoal desta Universidade, LOURDES ODETE DOS SANTOS (Siape: 0353599 ),  para substituir   EMERSON
CARDOSO LAMBERTI (Siape: 1646333 ),  Secretário do Centro de Estudos e Pesquisas em Administração
(CEPA), Código FG-7, em seu afastamento por motivo de férias, no período de 11/03/2019 a 22/03/2019, com
o decorrente pagamento das vantagens por 12 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
