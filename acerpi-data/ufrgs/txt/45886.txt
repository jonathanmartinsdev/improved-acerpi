Documento gerado sob autenticação Nº ECY.128.593.N17, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9514                  de  11/10/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar a partir de 25/07/2017,o ocupante do cargo de Professor do Magistério Superior, classe
Titular do Quadro de Pessoal desta Universidade, RICARDO BURG CECCIM, matrícula SIAPE n° 1083597, da
função de Coordenador Substituto do PPG em Saúde Coletiva da Escola de Enfermagem, para a qual foi
designado pela Portaria 8939/2016, de 07/11/2016. Processo nº 23078.515942/2017-46.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
