Documento gerado sob autenticação Nº LPO.856.727.0LL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9402                  de  21/11/2018
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
Designar os servidores,  MARIA DA GRACA BRIZOLA MAYER, ocupante do cargo de Engenheiro-
área, lotada na Superintendência de Infraestrutura e com exercício no Setor de Fiscalização e Adequação,
LUIZ FABRICIO MARTINS DOS SANTOS,  ocupante do cargo de Assistente em Administração, lotado na
Superintendência  de  Infraestrutura  e  com  exercício  na  Prefeitura  Campus  Litoral  Norte,  para  sob  a
presidência da primeira, constituirem a Comissão de Recebimento, em atendimento à alínea "b" do inciso I
do artigo 73 da Lei  8666/93,  para emissão do Termo de Recebimento Definitivo da Obra do contrato
112/2017, cujo objeto é "Contratação de empresa especializada para instalação de pavilhão de concreto
pré-moldado  dimensões  15mx30mx7,0m;  com  dois  pavimentos;escada;  telhado  com  telhas
termoacústicas  instaladas;  com  todos  os  materias/equipamentos  necessários  para  a  completa
execução do serviço, no Campus Litoral Norte da UFRGS". Processo nº 23078.520100/2017-14.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
