Documento gerado sob autenticação Nº XMO.556.463.Q33, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1611                  de  18/02/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder  prorrogação  de  jornada  de  trabalho  reduzida,  com  remuneração  proporcional,  nos
termos dos artigos 5º ao 7º da Medida Provisória nº 2.174-28, de 24 de agosto de 2001 e conforme o
Processo nº 23078.505980/2018-71,  à servidora PRISCILA MOREIRA DA SILVA VINHAS, matrícula SIAPE n°
1486536,  ocupante  do  cargo  de  Assistente  em  Administração  -  701200,  lotada  na  Pró-Reitoria  de
Planejamento e Administração e com exercício na Núcleo de Contratos e Normativas da Pró-Reitoria de
Planejamento e Administração, alterando a jornada de trabalho de oito horas diárias e quarenta horas
semanais para 6 horas diárias e 30 semanais, no período de 02 de março de 2020 a 31 de dezembro de
2020.  
RUI VICENTE OPPERMANN
Reitor.
