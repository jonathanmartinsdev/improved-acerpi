Documento gerado sob autenticação Nº FDZ.459.063.MAG, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1611                  de  03/03/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme o Laudo Médico n°37638,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, LUIZ GUSTAVO SANTOS REIS DE OLIVEIRA
(Siape: 2277892 ),  para substituir   SALETE MARIA MATTJE (Siape: 1626389 ), Gerente Administrativo da
Secretaria do Gabinete do Reitor, Código FG-1, em seu afastamento por motivo de Laudo Médico do titular
da Função, no período de 29/01/2016 a 05/02/2016, com o decorrente pagamento das vantagens por 8 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
