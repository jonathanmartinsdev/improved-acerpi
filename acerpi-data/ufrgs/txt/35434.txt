Documento gerado sob autenticação Nº WRA.832.489.TOU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2247                  de  14/03/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de FERNANDO NEVES HUGO, Professor do Magistério Superior,
lotado no Departamento de Odontologia Preventiva e Social da Faculdade de Odontologia e com exercício no
Centro de Pesquisa em Odontologia Social, com a finalidade de participar do evento "Digital Research in
Dentistry";  do "School  of  Dentistry  Global  Oral  Health  Symposium" e  da Reunião Anual  da  Associação
Internacional de Pesquisa Odontológica, em San Francisco, Estados Unidos, no período compreendido entre
18/03/2017 e 26/03/2017, incluído trânsito, com ônus limitado. Solicitação nº 26327.
RUI VICENTE OPPERMANN
Reitor
