Documento gerado sob autenticação Nº NSP.696.308.67C, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8294                  de  11/09/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a partir da data desto ato, a ocupante do cargo de Psicólogo-área - 701060, do Nível de
Classificação EIV, do Quadro de Pessoal desta Universidade, TONIA CUNHA DUARTE DA SILVA, matrícula
SIAPE 0359387, da função de Coordenadora Adjunto da CIS do Plano de Carreira dos Cargos Técnicos-
Administrativos em Educação, Código SRH 866, para a qual foi designada pela Portaria nº 7338/2019, de
13/08/2019. Processo nº 23078.523128/2019-67.
JANE FRAGA TUTIKIAN
Vice-Reitora.
