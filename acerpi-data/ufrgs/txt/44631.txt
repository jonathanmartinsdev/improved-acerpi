Documento gerado sob autenticação Nº FOC.775.572.7IP, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8662                  de  15/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  05/09/2017,   referente  ao  interstício  de
07/10/2014 a 04/09/2017, para a servidora RENATA PEREIRA FAGUNDES CORREA, ocupante do cargo de
Técnico de Laboratório Área -  701244,  matrícula SIAPE 1678600,  lotada  na  Faculdade de Farmácia,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  IV,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.514968/2017-77:
Formação Integral de Servidores da UFRGS IV CH: 110 (27/04/2016 a 14/08/2017)
CLEM Idiomas - Inglês Nível Pré-Intermediário B CH: 40 (01/08/2013 a 30/12/2013)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
