Documento gerado sob autenticação Nº IJY.779.395.4IL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7828                  de  21/08/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de UBIRATÃ KICKHOFEL ALVES, Professor do Magistério Superior,
lotado e em exercício no Departamento de Línguas Modernas do Instituto de Letras, com a finalidade de
participar do "X Congreso Nacional y V Congreso Internacional de Profesores de Portugués de la República
Argentina: O Portugués na Argentina: práticas e caminhos aos 20 anos da AAPP", em Córdoba, Argentina, no
período compreendido entre 19/09/2017 e 23/09/2017, incluído trânsito, com ônus limitado. Solicitação nº
28287.
RUI VICENTE OPPERMANN
Reitor
