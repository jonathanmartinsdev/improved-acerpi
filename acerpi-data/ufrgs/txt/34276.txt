Documento gerado sob autenticação Nº PFP.582.462.8PF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1398                  de  13/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, LUCIANA LEITE LIMA, matrícula SIAPE n° 1504813, lotada no Departamento de
Sociologia do Instituto de Filosofia e Ciências Humanas, como Coordenadora Substituta da COMGRAD do
Curso  de  Políticas  Públicas  do  IFCH,  para  substituir  automaticamente  o  titular  desta  função  em  seus
afastamentos  ou  impedimentos  regulamentares  na  vigência  do  presente  mandato.  Processo  nº
23078.200200/2017-19.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
