Documento gerado sob autenticação Nº WRA.870.646.91U, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9348                  de  09/10/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Lotar no Instituto de Filosofia e Ciências Humanas, com exercício na Biblioteca de Ciências Sociais e
Humanidades, POLIANA SANCHEZ DE ARAUJO, nomeada conforme Portaria Nº 7919/2017 de 23 de agosto
de 2017, publicada no Diário Oficial da União no dia 24 de agosto de 2017, em efetivo exercício desde 21 de
setembro  de  2017,  ocupante  do  cargo  de  BIBLIOTECÁRIO-DOCUMENTALISTA,  Ambiente  Organizacional
Informação, classe E, nível I, padrão 101, no Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
