Documento gerado sob autenticação Nº EGP.845.225.2S4, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1240                  de  19/02/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE:
Autorizar  o  afastamento  do  país  de  HELENA  ARAUJO  RODRIGUES  KANAAN,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Artes Visuais do Instituto de Artes, com a
finalidade de participar do "VII  Congresso Internacional  CSO'2016 -  Criadores Sobre outras Obras",  em
Lisboa, Portugal,  no período compreendido entre 17/03/2016 e 23/03/2016, incluído trânsito,  com ônus
UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação nº 17854.
RUI VICENTE OPPERMANN
VICE-REITOR, NO EXERCÍCIO DA REITORIA
