Documento gerado sob autenticação Nº IAJ.410.416.CLR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1618                  de  15/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°45464,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, PAULA SANDRINE MACHADO (Siape: 1766518 ),  para
substituir   LUIS ARTUR COSTA (Siape: 1006657 ), Chefe do Depto de Psicologia Social e Institucional do
Instituto de Psicologia, Código FG-1, em seu afastamento por motivo de férias, no período de 18/02/2019 a
08/03/2019, com o decorrente pagamento das vantagens por 19 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
