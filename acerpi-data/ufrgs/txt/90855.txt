Documento gerado sob autenticação Nº ALX.518.081.R3F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4787                  de  04/06/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7633, de 29 de
setembro de 2016 e tendo em vista o que consta dos Processos Administrativos n° 23078.522107/2018-43 e
23078.504259/2017-83, da Lei 10.520/02, do Contrato nº 084/2017 e ainda, da Lei 8.666/93,
            RESOLVE:
 
          Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 01 da cláusula décima primeira do
contrato,  à  Empresa  COOK  EMPREENDIMENTOS  EM  ALIMENTAÇÃO  COLETIVA  LTDA,  CNPJ  nº
16.654.626/0001-51, pela falta de comprovação de treinamento atualizado, bem como irregularidades no
armazenamento, higienização e identificação dos produtos conforme atestado pela fiscalização (Doc. SEI nº
1166269  e  1212369),  bem  como  pelo  NUDECON  (Doc.  SEI  nº  1570484)  do  processo  administrativo
23078.522107/2018-43.
 
               Registre-se no SICAF, nos termos do art. 32 da IN nº 03/2018, da SEGES-MPDG. 
 
 
LUIS ROBERTO DA SILVA MACEDO
Pró-Reitor de Planejamento e Administração em exercício
