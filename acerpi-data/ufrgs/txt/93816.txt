Documento gerado sob autenticação Nº SEG.948.650.R0T, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5705                  de  08/07/2019
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do País de EDUARDO MUNHOZ SVARTMAN,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Ciência Política do Instituto de Filosofia e Ciências
Humanas, com a finalidade de participar do "XIV Congresso Nacional de Ciência Política da SAAP", em Buenos
Aires,  Argentina,  no período compreendido entre 17/07/2019 e 20/07/2019,  incluído trânsito,  com ônus
CAPES/PROAP. Solicitação nº 85287.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
