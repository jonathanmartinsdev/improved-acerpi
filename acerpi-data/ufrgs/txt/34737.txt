Documento gerado sob autenticação Nº IWX.224.213.ACG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1763                  de  22/02/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de NINA SIMONE VILAVERDE MOURA, Professor do Magistério
Superior, lotada e em exercício no Departamento de Geografia do Instituto de Geociências, com a finalidade
de participar do "Annual Meeting of the American Association of Geographers", em Boston, Estados Unidos,
no período compreendido entre 04/04/2017 e 10/04/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria
de Pesquisa - diárias) . Solicitação nº 25802.
RUI VICENTE OPPERMANN
Reitor
