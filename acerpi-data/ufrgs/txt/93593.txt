Documento gerado sob autenticação Nº SMD.921.839.AU8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5583                  de  03/07/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  ANA ELIZA PEREIRA BOCORNY,  Professor  do  Magistério
Superior,  lotada e  em exercício  no Departamento de Línguas Modernas do Instituto de Letras,  com a
finalidade de realizar estudos em nível de Pós-Doutorado junto à Northern Arizona University, em Flagstaff,
Estados Unidos, no período compreendido entre 01/08/2019 e 31/01/2020, com ônus CAPES/PRINT/UFRGS.
Processo nº 23078.511227/2019-04.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
