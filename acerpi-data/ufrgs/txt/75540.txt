Documento gerado sob autenticação Nº EJV.015.571.3LH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             755                  de  22/01/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar  insubsistente  a  Portaria  nº  691/2019,  de  20/01/2019,  que  designou  temporariamente
ALEXANDRA LORANDI MACEDO  (Siape: 2244999),  para substituir  LOVOIS DE ANDRADE MIGUEL (Siape:
1230208 ), Secretária de Educação a Distância, Código SRH 862, código CD-3, no período de 21/01/2019 à
25/01/2019, gerada conforme Solicitacao de Férias nº 45522. Processo SEI Nº 23078.500961/2019-30
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
