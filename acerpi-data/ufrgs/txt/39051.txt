Documento gerado sob autenticação Nº CDM.581.711.TKK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4835                  de  31/05/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  30/05/2017,   referente  ao  interstício  de
19/06/2009 a 29/05/2017, para a servidora BERNARDETE BARCELLOS DE SOUZA,  ocupante do cargo de
Assistente de Laboratório -  701437,  matrícula  SIAPE 0358573,   lotada  na  Faculdade de Agronomia,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  C  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  C  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.010748/2017-79:
Formação Integral de Servidores da UFRGS IV CH: 95 Carga horária utilizada: 90 hora(s) / Carga horária
excedente: 5 hora(s) (19/04/2010 a 10/05/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
