Documento gerado sob autenticação Nº KHT.668.113.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3961                  de  09/05/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Alterar a Portaria nº 5.756 de 04 de novembro de 1991, que distribuiu os Cargos de Direção - CD e
Funções Gratificadas - FG, desta Universidade, a partir de 10/05/2019, como segue:
 
Transformar: Chefe da Subprefeitura do Campus do Vale Setor 2, Código SRH 1369, Código FG-1, em
Gerente Administrativo vinculado à Superintendência de Infraestrutura, Código SRH 1369, Código FG-1.
 
 
Processo nº 23078.511380/2019-23.
 
RUI VICENTE OPPERMANN
Reitor.
