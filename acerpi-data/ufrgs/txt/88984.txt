Documento gerado sob autenticação Nº VXL.265.410.A67, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2735                  de  28/03/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, e conforme a Solicitação de Férias n°50003,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, SAMUEL ANTONIO ZIEGER MERODE (Siape:
2406429 ),   para substituir    CRISTIANE DE OLIVEIRA ENDRES (Siape:  2215420 ),  Diretor  da Divisão de
Concursos Públicos do Departamento de Desenvolvimento e Gestão de Pessoas da PROGESP, Código FG-1,
em seu afastamento por motivo de férias,  no período de 28/03/2019 a 16/04/2019,  com o decorrente
pagamento das vantagens por 20 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas, em exercício.
