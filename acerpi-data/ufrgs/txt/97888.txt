Documento gerado sob autenticação Nº PBC.712.998.NTE, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8250                  de  09/09/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  CLAUDIA  TARRAGO  CANDOTTI,  matrícula  SIAPE  n°  1717040,  lotada  no  Departamento  de
Educação Física, Fisioterapia e Dança da Escola de Educação Física, Fisioterapia e Dança e com exercício na
Comissão de Pesquisa da ESEFID, da classe D  de Professor Associado, nível 01, para a classe D  de Professor
Associado, nível 02, referente ao interstício de 31/07/2017 a 30/07/2019, com vigência financeira a partir de
31/07/2019, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de 2012, com suas alterações e a
Decisão nº 331/2017 do CONSUN. Processo nº 23078.515490/2019-64.
JANE FRAGA TUTIKIAN
Vice-Reitora.
