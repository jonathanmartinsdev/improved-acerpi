Documento gerado sob autenticação Nº UTV.241.695.DLP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10132                  de  21/12/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar CYDARA CAVEDON RIPOLL, matrícula SIAPE n° 0357177, ocupante do cargo de Professor
do Magistério Superior, classe Titular, lotada no Departamento de Matemática Pura e Aplicada do Instituto de
Matemática e Estatística, do Quadro de Pessoal da Universidade, para exercer a função de Chefe Substituta
do Depto de Matemática Pura e Aplicada do Instituto de Matemática, com vigência a partir de 21/12/2016 e
até 29/07/2017, a fim de completar o mandato de JOAO BATISTA DA PAZ CARVALHO, conforme artigo 92 do
Estatuto da mesma Universidade. Processo nº 23078.204195/2016-32.
JANE FRAGA TUTIKIAN
Vice-Reitora
