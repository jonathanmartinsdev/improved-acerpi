Documento gerado sob autenticação Nº JCM.228.226.96D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9405                  de  21/11/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Declarar  que a  aposentadoria  concedida a  SONIA REGINA FERREIRA SEFRIN,  matrícula  SIAPE
355430, através da portaria nº 1388, de 12 de junho de 1998, publicada no Diário Oficial da União do dia 22
subsequente,  passa  a  ser  no cargo de Assistente  em Administração,  nível  de  classificação D,  nível  de
capacitação  IV,  padrão  11,  conforme  determinação  judicial  contida  no  processo  nº  5048733-
70.2018.4.04.7100/RS.  Processo  nº  23078.526540/2018-58.
RUI VICENTE OPPERMANN
Reitor.
