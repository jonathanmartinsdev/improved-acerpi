Documento gerado sob autenticação Nº BGB.792.934.THG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3736                  de  02/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5014339-03.2019.4.04.7100,  da  4ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, do servidor EDUARDO MARTINS ARRACHE,  matrícula SIAPE n° 0359304, desligado no cargo
de Assistente em Administração - 701200, do Nível I para o Nível IV, a contar de 01/01/2006, conforme o
Processo nº 23078.510423/2019-53.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
