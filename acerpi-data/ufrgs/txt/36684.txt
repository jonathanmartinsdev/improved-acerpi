Documento gerado sob autenticação Nº KOM.803.993.HII, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3061                  de  07/04/2017
  Designa  Diretora  do  Centro  de  Estudos
Alemães e Europeus - CEAE.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
RESOLVE
Designar a Professora CLÁUDIA LIMA MARQUES como Diretora do Centro de Estudos Alemães e
Europeus - CEAE, a partir desta data, com mandato até 10 de abril de 2019.
RUI VICENTE OPPERMANN,
Reitor.
