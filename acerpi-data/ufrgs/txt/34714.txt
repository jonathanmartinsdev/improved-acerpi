Documento gerado sob autenticação Nº MPQ.108.358.95D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1799                  de  23/02/2017
A SUPERINTENDENTE DE INFRAESTRUTURA EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7793, de 03 de outubro
de 2016
RESOLVE
Designar os servidores  LUCIO ALBINO AMARO DA SILVA, ocupante do cargo de Engenheiro-área,
lotado na Superintendência de Infraestrutura e com exercício no Departamento de Projetos e Planejamento,
RÔMULO SEHNEM, ocupante do cargo de Engenheiro-área, lotado na Superintendência de Infraestrutura e
com exercício  na  Prefeitura  Campus do Vale,  VIRGINIA DE LIMA FERNANDES,  ocupante  do cargo de
Engenheiro-área,  lotada na Superintendência de Infraestrutura e com exercício no Setor de Patrimônio
Histórico,  GENISA  COUTO  DA  SILVA,  ocupante  do  cargo  de  Assistente  em  Administração,  lotada  na
Superintendência de Infraestrutura e com exercício na Gerência Administrativa da Superintendência de
Infraestrutura,  para,  sob a  presidência  do  primeiro,  comporem Grupo de  Trabalho,  para  no prazo  de
quarenta e cinco dias, a partir de 06/03/2017, elaborar proposta de adequação do maquinário da marcenaria
a fim de atender às exigências das normas do Ministério do Trabalho.
ANDREA PINTO LOGUERCIO
Superintendente de Infraestrutura em exercício
