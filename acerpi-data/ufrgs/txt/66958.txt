Documento gerado sob autenticação Nº KWQ.814.032.DL8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5524                  de  25/07/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  o ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, MARCELO ZUBARAN GOLDANI (Siape: 2316403),  para
substituir  CLECIO HOMRICH DA SILVA (Siape:  3192667 ),  Chefe do Depto de Pediatria da Faculdade de
Medicina, Código SRH 92, FG-1, em seu afastamento por férias no período de 25/07/2018 a 27/07/2018, com
o decorrente pagamento das vantagens por 3 dias. Processo SEI Nº 23078.518663/2018-15
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
