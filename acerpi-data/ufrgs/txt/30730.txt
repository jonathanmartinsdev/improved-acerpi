Documento gerado sob autenticação Nº BFW.938.489.FP4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9384                  de  23/11/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ROBERTO GIUGLIANI, Professor do Magistério Superior, lotado
e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de participar de
reunião  junto  à  Medical  University  of  Vienna,  Áustria,  no  período  compreendido  entre  13/12/2016  e
15/12/2016, incluído trânsito, com ônus limitado. Solicitação nº 24891.
RUI VICENTE OPPERMANN
Reitor
