Documento gerado sob autenticação Nº RXE.704.790.NQ4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             209                  de  08/01/2019
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  07/01/2019,   referente  ao  interstício  de
07/07/2017 a 06/01/2019, para a servidora EDIMARA MARTINS WILCHEN, ocupante do cargo de Assistente
em  Administração  -  701200,  matrícula  SIAPE  2137160,   lotada   na   Pró-Reitoria  de  Planejamento  e
Administração,  passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  III,  para  o  Nível  de
Classificação/Nível de Capacitação D IV, em virtude de ter realizado o(s) seguinte(s) curso(s), conforme o
Processo nº 23078.500112/2019-86:
TCU - Controles na Administração Pública CH: 30 (01/12/2018 a 31/12/2018)
ENAP - SEI! USAR CH: 20 (28/11/2017 a 18/12/2017)
ENAP - Regras e Fundamentos do SCDP CH: 21 (18/04/2017 a 15/05/2017)
ENAP - Regras e Fundamentos do Sistema de Concessão de Diárias e Passagens (SCPD) CH: 30 (18/04/2017 a
15/05/2017)
TCU - Legislação Básica em Licitações, Pregão e Registro de Preços CH: 30 (01/06/2018 a 30/06/2018)
TCU - Gestão Orçamentária e Financeira CH: 20 Carga horária utilizada: 19 hora(s) / Carga horária excedente:
1 hora(s) (01/08/2018 a 31/08/2018)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
