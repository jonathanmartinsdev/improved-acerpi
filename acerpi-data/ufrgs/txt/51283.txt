Documento gerado sob autenticação Nº HIN.668.678.2A4, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1618                  de  26/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/11/2017,   referente  ao  interstício  de
07/06/2013 a 15/11/2017, para a servidora JOZI FERNANDA RODRIGUES ESTANISLAU, ocupante do cargo de
Químico - 701068, matrícula SIAPE 1780763,  lotada  no  Centro de Biotecnologia, passando do Nível de
Classificação/Nível de Capacitação E III, para o Nível de Classificação/Nível de Capacitação E IV, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.521359/2017-74:
Formação Integral de Servidores da UFRGS VI CH: 156 (12/11/2012 a 10/11/2017)
PPGBCM - Tópicos Avançados do Metabolismo Energético CH: 30 Carga horária utilizada: 24 hora(s) / Carga
horária excedente: 6 hora(s) (15/03/2017 a 24/03/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
