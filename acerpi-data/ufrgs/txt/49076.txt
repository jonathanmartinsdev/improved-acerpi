Documento gerado sob autenticação Nº BYL.960.756.JA7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             186                  de  04/01/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°38435,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de CONTADOR, do Quadro de
Pessoal desta Universidade, IVONEI SOZIO (Siape: 1952761 ),  para substituir   PATRICIA DE OLIVEIRA LUZ
(Siape: 1757677 ), Diretor Administrativo do Campus Litoral Norte, Código CD-4, em seu afastamento por
motivo de férias, no período de 10/01/2018 a 23/01/2018, com o decorrente pagamento das vantagens por
14 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
