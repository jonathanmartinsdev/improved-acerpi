Documento gerado sob autenticação Nº NUA.723.956.I34, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9269                  de  11/10/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  ARAGON  ERICO  DASSO  JUNIOR,  matrícula  SIAPE  n°  1852380,  lotado  no
Departamento de Ciências Administrativas da Escola de Administração, como Coordenador Substituto da
COMGRAD em Administração Pública e Social, para substituir o titular desta função em seus afastamentos ou
impedimentos regulamentares na vigência do presente mandato. Processo nº 23078.527308/2019-18.
JANE FRAGA TUTIKIAN
Vice-Reitora.
