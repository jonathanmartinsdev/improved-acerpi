Documento gerado sob autenticação Nº PGH.951.505.F6G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8173                  de  11/10/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a FATIMARLEI
LUNARDELLI, matrícula SIAPE nº 0356505, no cargo de Jornalista, nível de classificação E, nível de capacitação
III, padrão 16, do Quadro desta Universidade, no regime de vinte e cinco horas semanais de trabalho, com
exercício  na  Gerência  Administrativa  da  Faculdade  de  Biblioteconomia  e  Comunicação,  com proventos
integrais  e incorporando a vantagem pessoal  de que trata a Lei  nº  9.624,  de 2 de abril  de 1998,  que
assegurou o disposto no artigo 3º da Lei nº 8.911, de 11 de julho de 1994. Processo 23078.021802/2016-21.
JANE FRAGA TUTIKIAN
Vice-Reitora, no Exercício da Reitoria
