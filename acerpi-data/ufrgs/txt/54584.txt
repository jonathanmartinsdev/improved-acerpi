Documento gerado sob autenticação Nº RXU.355.647.J7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3749                  de  22/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de LAURA BANNACH JARDIM, Professor do Magistério Superior,
lotada e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a finalidade de
participar  do  "2nd  Pan  American  Parkinson's  Disease  and  Movement  Disorders  Congress",  em Miami,
Estados Unidos,  no período compreendido entre 21/06/2018 e 25/06/2018,  incluído trânsito,  com ônus
limitado. Solicitação nº 33699.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
