Documento gerado sob autenticação Nº GWY.865.904.L81, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7559                  de  29/09/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Autorizar o afastamento do país de MARIA CELESTE OSORIO WENDER, Professor do Magistério
Superior, lotada no Departamento de Ginecologia e Obstetrícia da Faculdade de Medicina e com exercício no
Programa de Pós-Graduação em Ciências da Saúde: Ginecologia e Obstetrícia, com a finalidade de participar
do "15th World Congress on Menopause", em Praga, República Tcheca, no período compreendido entre
27/09/2016 e 30/09/2016, incluído trânsito, com ônus limitado. Solicitação nº 23510.
RUI VICENTE OPPERMANN
Reitor
