Documento gerado sob autenticação Nº KXG.742.443.IC2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9525                  de  29/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial da União do dia 31 subsequente, a ELIDA
STAROSTA TESSLER, matrícula SIAPE nº 1079010, no cargo de Professor Associado, nível 4, da Carreira do
Magistério Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no
Departamento de Artes Visuais do Instituto de Artes, com proventos integrais. Processo 23078.022741/2016-
19.
RUI VICENTE OPPERMANN
Reitor
