Documento gerado sob autenticação Nº HIJ.618.783.QJH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8673                  de  26/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder o Adicional de Periculosidade, no percentual de 10%, a partir de 01/06/2016, ao servidor
RICARDO SCHÄFFER DA ROSA,  Identificação Única 19352271, Engenheiro-área, com exercício no Divisão
Técnico-Científica da Direção Acadêmica do Campus Litoral Norte, observando-se o disposto na Lei nº 8.112,
de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades
em áreas consideradas Perigosas conforme Laudo Pericial constante no Processo n º 23078.507392/2016-19,
Código SRH n° 22926 e Código SIAPE n° 2016003898.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
