Documento gerado sob autenticação Nº OJK.638.541.E65, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             493                  de  17/01/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder ao servidor RAFAEL SILVEIRA MACHADO, ocupante do cargo de  Analista de Tecnologia
da Informação - 701062, lotado no Centro de Processamento de Dados, SIAPE 2777454, o percentual de 30%
(trinta por cento) de Incentivo à Qualificação, a contar de 21/12/2016, tendo em vista a conclusão do curso de
Especialização em Engenharia de Sistemas, conforme o Processo nº 23078.025975/2016-18.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
