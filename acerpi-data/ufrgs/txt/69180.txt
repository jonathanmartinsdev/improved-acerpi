Documento gerado sob autenticação Nº WYO.408.182.7ND, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6960                  de  04/09/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 3º, incisos I, II, III, da Emenda Constitucional nº 47, de
5 de julho de 2005,  publicada no Diário Oficial  da União do dia 6 subsequente,  a  CLAUDIO FISCHER,
matrícula SIAPE nº 0357084, no cargo de Professor Adjunto, nível 1, da Carreira do Magistério Superior, do
Quadro  desta  Universidade,  no  regime  de  quarenta  horas  semanais  de  trabalho,  com  exercício  no
Departamento de Arquitetura  da  Faculdade de Arquitetura,  com proventos  integrais  e  incorporando a
vantagem pessoal de que trata a Lei nº 9.624, de 2 de abril de 1998, que assegurou o disposto no artigo 3º da
Lei nº 8.911, de 11 de julho de 1994. Processo 23078.516595/2018-50.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
