Documento gerado sob autenticação Nº OJZ.141.454.QJH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8647                  de  25/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°23562,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, SANDRA TEREZINHA REY (Siape: 6355254 ),  para
substituir   SILVIA BALESTRERI NUNES (Siape: 1172429 ), Coordenador da Comissão de Pesquisa do Instituto
de Artes, em seu afastamento no país, no período de 27/10/2016 a 28/10/2016.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
