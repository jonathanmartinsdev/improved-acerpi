Documento gerado sob autenticação Nº CYH.603.459.A0J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             455                  de  16/01/2017
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°34191,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, CARMEN FATIMA NECKEL MACHADO (Siape:
0356730 ),  para substituir   ANA JUSSARA DUARTE DE SOUZA (Siape: 0351080 ), Gerente Administrativo da
Faculdade de Farmácia, Código FG-1, em seu afastamento por motivo de férias, no período de 02/01/2017 a
07/01/2017, com o decorrente pagamento das vantagens por 6 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
