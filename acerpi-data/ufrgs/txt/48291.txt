Documento gerado sob autenticação Nº TSA.194.843.U1M, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11311                  de  20/12/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar, a pedido, a partir de 15 de setembro de 2017, o ocupante do cargo de Professor do
Magistério  Superior,  classe  Adjunto  do  Quadro  de  Pessoal  desta  Universidade,  JUAN  PABLO  RAGGIO
QUINTAS, matrícula SIAPE n° 3280987, da função de Coordenador Substituto da COMGRAD de Engenharia
Mecânica,  para  a  qual  foi  designado  pela  Portaria  nº  4240,  de  15  de  maio  de  2017.  Processo  nº
23078.523290/2017-13.
JANE FRAGA TUTIKIAN
Vice-Reitora.
