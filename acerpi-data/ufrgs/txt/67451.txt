Documento gerado sob autenticação Nº LNT.185.569.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5858                  de  06/08/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear, em caráter efetivo, ALINE MARQUES ACOSTA, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 60/2018 de 5 de Julho de 2018, homologado em 06 de julho
de 2018 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei 12.772, de
28 de dezembro de 2012, com redação dada pela Lei 12.863, de 24 de setembro de 2013, publicada no Diário
Oficial da União de 25 de setembro de 2013, para o cargo de PROFESSOR DO MAGISTÉRIO SUPERIOR do
Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor Adjunto A, Nível I, do Quadro
de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação Exclusiva), junto ao Departamento
de Assistência e Orientação Profissional da Escola de Enfermagem, em vaga decorrente da aposentadoria da
Professora Clarice Maria Dall  Agnol,  código nº 272410, ocorrida em 28 de Fevereiro de 2018, conforme
Portaria nº. 1667/2018 de 27 de fevereiro de 2018, publicada no Diário Oficial da União de 28 de Fevereiro de
2018. Processo nº. 23078.501602/2018-19.
RUI VICENTE OPPERMANN
Reitor.
