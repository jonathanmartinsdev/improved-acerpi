Documento gerado sob autenticação Nº PBS.843.954.LCS, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6784                  de  27/07/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei  nº.9.527,  de 10 de dezembro de 1997,  a ocupante do cargo de Professor do Magistério
Superior, do Quadro de Pessoal desta Universidade, LUCIANA BARCELLOS TEIXEIRA (Siape: 1462333),  para
substituir TATIANA ENGEL GERHARDT (Siape: 1362028 ), Chefe do Departamento de Saúde Coletiva da Escola
de Enfermagem, Código SRH 1530, FG-1,  em seu afastamento por motivo de férias, no período de 24 a
26/07/2019, com o decorrente pagamento das vantagens por 3 dias. Processo SEI nº 23078.519335/2019-17
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
