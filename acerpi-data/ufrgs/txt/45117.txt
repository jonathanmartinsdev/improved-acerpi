Documento gerado sob autenticação Nº BHO.844.625.4UG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9023                  de  27/09/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  25/09/2017,   referente  ao  interstício  de
15/03/2016 a 24/09/2017, para o servidor DIRCEU ADAIR BUENO JUNIOR, ocupante do cargo de Técnico em
Mecânica  -  701245,  matrícula  SIAPE  2152580,   lotado  no   Instituto  de  Física,  passando do  Nível  de
Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.516425/2017-94:
ENAP - Introdução à Gestão de Processos CH: 20 (20/10/2015 a 09/11/2015)
ENAP - Ética e Serviço Público CH: 20 (26/05/2015 a 15/06/2015)
ENAP - Introdução à Gestão de Projetos CH: 20 (15/09/2015 a 05/10/2015)
ENAP - Legislação Aplicada à Logística de Suprimentos CH: 7 (04/08/2015 a 31/08/2015)
ENAP - Sistema Eletrônico de Informações - SEI CH: 20 Carga horária utilizada: 13 hora(s) / Carga horária
excedente: 7 hora(s) (21/03/2017 a 10/04/2017)
ENAP - Gestão e Fiscalização de Contratos Administrativos CH: 40 (02/08/2016 a 05/09/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
