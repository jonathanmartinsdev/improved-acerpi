Documento gerado sob autenticação Nº BHW.987.824.OCM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4158                  de  11/05/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  CHRISTINE  TESSELE  NODARI,  matrícula  SIAPE  n°  1534467,  lotada  no
Departamento de Engenharia de Produção e Transportes da Escola de Engenharia, como Coordenadora
Substituta do PPG em Engenharia de Produção-Mestrado Profissional, para substituir automaticamente o
titular desta função em seus afastamentos ou impedimentos regulamentares no período de 21/05/2017 e até
20/05/2019, por ter sido reeleita. Processo nº 23078.007034/2017-83.
JANE FRAGA TUTIKIAN
Vice-Reitora.
