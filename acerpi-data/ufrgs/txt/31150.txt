Documento gerado sob autenticação Nº DCT.285.955.T7D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9937                  de  15/12/2016
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar  MARICHEL  DALCIN,  Matrícula  SIAPE  1769758,  ocupante  do  cargo  de  Auxiliar  de
Enfermagem, Código 701411, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe do
Setor de Infraestrutura e Informática da Faculdade de Farmácia, Código SRH 1408, código FG-7, com vigência
a partir da data de publicação no Diário Oficial da União. Processo nº 23078.025039/2016-15.
                                                                               JANE FRAGA TUTIKIAN
                                                                   Vice-Reitora, no Exercício da Reitoria
