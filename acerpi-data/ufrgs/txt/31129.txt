Documento gerado sob autenticação Nº SNY.480.494.T9C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9676                  de  07/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar na Escola de Administração, com exercício na Escola de Administração, CAROLINA BARCELOS,
nomeada conforme Portaria Nº 8560/2016 de 24 de outubro de 2016, publicada no Diário Oficial da União no
dia 25 de outubro de 2016, em efetivo exercício desde 05 de dezembro de 2016, ocupante do cargo de
RELAÇÕES PÚBLICAS, Artes, Comunicação e Difusão, classe E, nível I, padrão 101, no Quadro de Pessoal desta
Universidade.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
