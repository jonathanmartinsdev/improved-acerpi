Documento gerado sob autenticação Nº MSP.585.942.T8G, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3839                  de  25/05/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor HUGO FRIDOLINO MULLER NETO,  matrícula SIAPE n° 2225072,  lotado no Departamento de
Ciências Administrativas da Escola de Administração e com exercício na Escola de Administração, da classe D 
de Professor Associado, nível 01, para a classe D  de Professor Associado, nível 02, referente ao interstício de
18/01/2016 a 17/01/2018, com vigência financeira a partir de 30/04/2018, de acordo com o que dispõe a Lei
12.772 de 28 de dezembro de 2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº
23078.500540/2018-28.
JANE FRAGA TUTIKIAN
Vice-Reitora.
