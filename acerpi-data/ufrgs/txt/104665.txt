Documento gerado sob autenticação Nº YVI.955.600.5B8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1124                  de  31/01/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°55161,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de TÉCNICO EM CONTABILIDADE,
do Quadro de Pessoal  desta Universidade,  LETICIA SIGNOR (Siape:  1870960 ),   para substituir    JOSE
VANDERLEI  FERREIRA  (Siape:  0353326  ),  Diretor  do  Departamento  de  Programação  Orçamentária  da
PROPLAN, Código CD-4, em seu afastamento por motivo de férias, no período de 03/02/2020 a 14/02/2020,
com o decorrente pagamento das vantagens por 12 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
