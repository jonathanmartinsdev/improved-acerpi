Documento gerado sob autenticação Nº OFK.217.039.KMC, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             993                  de  01/02/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°41485,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  ASSISTENTE  EM
ADMINISTRAÇÃO, do Quadro de Pessoal desta Universidade, ADRIANA DE CASTRO VEIGA (Siape: 0357122 ),
 para substituir   ZELIA DA SILVA ZAGHETTO (Siape: 0355900 ), Coordenador do Núcleo Administrativo da
Gerência Administrativa do Instituto de Geociências, Código FG-7, em seu afastamento por motivo de férias,
no período de 05/02/2018 a 09/02/2018.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
