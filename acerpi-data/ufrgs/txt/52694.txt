Documento gerado sob autenticação Nº KOS.489.631.D59, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2452                  de  03/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  ao  servidor  CONSTANTINOS  GUIMARAES  GIANNOULAKIS,  ocupante  do  cargo  de  
Operador de Estação de Tratamento Água-esgoto - 701449, lotado na Escola de Educação Física, Fisioterapia
e Dança, SIAPE 2409342, o percentual de 15% (quinze por cento) de Incentivo à Qualificação, a contar de
29/08/2017,  tendo  em  vista  a  conclusão  do  curso  de  Ensino  Médio,  conforme  o  Processo  nº
23078.514383/2017-57.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
