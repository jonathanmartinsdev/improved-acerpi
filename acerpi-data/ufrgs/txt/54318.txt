Documento gerado sob autenticação Nº TVB.628.829.7OF, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3557                  de  16/05/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor JOSE LUIS BRITO DA
SILVA, ocupante do cargo de Servente de Limpeza-701823, lotado no Centro de Processamento de Dados,
SIAPE 1109754, para 30% (trinta por cento), a contar de 02/04/2018, tendo em vista a conclusão do curso de
Especialização em Gestão Estratégica de Pessoas, conforme o Processo nº 23078.507017/2018-22.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
