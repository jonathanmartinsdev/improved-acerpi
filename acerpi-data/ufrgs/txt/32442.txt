Documento gerado sob autenticação Nº QQY.942.051.JCT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             154                  de  05/01/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANALÚCIA DANILEVICZ PEREIRA,  Professor do Magistério
Superior, lotada e em exercício no Departamento de Economia e Relações Internacionais da Faculdade de
Ciências Econômicas, com a finalidade de participar de congresso junto ao Jawaharlal Nehru University, em
Bangalore, Índia,  no período compreendido entre 08/01/2017 e 13/01/2017, incluído trânsito,  com ônus
limitado. Solicitação nº 25319.
RUI VICENTE OPPERMANN
Reitor
