Documento gerado sob autenticação Nº FXU.006.834.J7C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5664                  de  30/07/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  LUCIANO  ZUBARAN  GOLDANI,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a
finalidade de participar da "International Conference on Emerging Infectious Diseases", em Atlanta, Estados
Unidos, no período compreendido entre 25/08/2018 e 30/08/2018, incluído trânsito, com ônus limitado.
Solicitação nº 47596.
RUI VICENTE OPPERMANN
Reitor
