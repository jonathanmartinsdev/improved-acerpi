Documento gerado sob autenticação Nº TMK.001.784.GL6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10747                  de  24/11/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar NADIA DE FATIMA BORBA MARTINS, Matrícula SIAPE 1200644, ocupante do cargo de
Técnico em Assuntos Educacionais, Código 701079, do Quadro de Pessoal desta Universidade, para exercer a
função de Coordenadora do Núcleo Pedagógico do Campus Litoral Norte, código SRH 1411, código FG-2, com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.520255/2017-42.
RUI VICENTE OPPERMANN
Reitor.
