Documento gerado sob autenticação Nº MMU.396.083.94U, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3135                  de  27/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°25941,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do Quadro de Pessoal  desta Universidade,  LIANE ESTEVES DAUDT (Siape:  1254161 ),   para
substituir   PAULO JOSE CAUDURO MAROSTICA (Siape: 1247430 ), Coordenador do PPG em Saúde da Criança
e do Adolescente,  Código FUC, em seu afastamento por motivo de férias,  no período de 25/04/2016 a
06/05/2016, com o decorrente pagamento das vantagens por 12 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
