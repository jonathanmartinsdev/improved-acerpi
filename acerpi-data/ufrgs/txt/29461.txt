Documento gerado sob autenticação Nº NSE.812.055.H5O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8404                  de  19/10/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  01/10/2016,   referente  ao  interstício  de
31/03/2015 a 30/09/2016, para a servidora ANA PAULA ZAMBELLI,  ocupante do cargo de Auxiliar em
Administração - 701405, matrícula SIAPE 2053820,  lotada  na  Pró-Reitoria de Pós-Graduação, passando do
Nível de Classificação/Nível de Capacitação C II, para o Nível de Classificação/Nível de Capacitação C III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.019872/2016-19:
ILB - Introdução ao Orçamento Público CH: 20 (03/02/2015 a 31/03/2015)
ENAP - Introdução à Gestão de Projetos CH: 20 Carga horária utilizada: 10 hora(s) / Carga horária excedente:
10 hora(s) (20/10/2015 a 09/11/2015)
ENAP - Introdução à Gestão de Processos CH: 20 (11/08/2015 a 31/08/2015)
ENAP -  Gestão e  Fiscalização de  Contratos  Administrativos  -  Nível  Intermediário  CH:  40  (01/09/2015 a
05/10/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
