Documento gerado sob autenticação Nº DGK.798.467.8VL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2745                  de  28/03/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARIA JOAO VELOSO DA COSTA RAMOS PEREIRA, Professor
do Magistério Superior, lotada e em exercício no Departamento de Zoologia do Instituto de Biociências, com
a finalidade de ministrar curso e realizar visita à Universidade de Aveiro, em Aveiro, Portugal, no período
compreendido entre 14/04/2019 e 19/04/2019, incluído trânsito, com ônus limitado. Solicitação nº 83057.
RUI VICENTE OPPERMANN
Reitor
