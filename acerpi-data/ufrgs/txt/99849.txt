Documento gerado sob autenticação Nº JAQ.328.900.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9634                  de  23/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias, de acordo com o processo nº 23078.007313/2016-66, considerando o disposto na Lei nº 8.112,
de 11/12/1990, no Decreto Presidencial nº 1.590, de 10/08/1995, no Decreto nº 4.836, de 09/09/2003, na
Decisão  nº  432  do  Conselho  Universitário,  de  27/11/2015  e  na  Portaria  9.911  de  26/10/2017  desta
Universidade
RESOLVE
Art.1º  RENOVAR  a  jornada  de  trabalho  flexibilizada  no  Museu  da  UFRGS,  concedida  através
da Portaria nº 4194 de 15/05/2019, em período diário de atendimento ao público das 8h às 20h, pelo prazo
de doze meses a contar de 31/10/2019, conforme disposto no Art. 11, da Portaria 9.911 de 26/10/2017.
Art. 2º Os servidores técnico-administrativos abaixo relacionados terão jornada de trabalho de seis
horas diárias, cumprindo carga horária de trinta horas semanais, de acordo com os termos desta Portaria.
 
CIDARA LOGUERCIO SOUZA - SIAPE: 6359308
DIEGO SPEGGIORIN DEVINCENZI - SIAPE: 2053466
ELIANE MURATORE - SIAPE: 2247741
JOSE FRANCISCO FLORES - SIAPE: 0356846
LIGIA KETZER FAGUNDES - SIAPE: 0356178
MARCELO CAVALCANTI DA SILVEIRA - SIAPE: 0357520
MAURA BOMBARDELLI - SIAPE: 2185056
RAFAELA SILVA THOMAZ - SIAPE: 2259205
ROBERTA FERNANDES FAJER - SIAPE: 0357435
SIMONE BORSATTO - SIAPE: 1671210
Art. 3º Ficam ratificados os demais termos. 
RUI VICENTE OPPERMANN
Reitor.
