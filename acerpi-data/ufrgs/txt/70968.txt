Documento gerado sob autenticação Nº MFJ.683.433.0F9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8292                  de  16/10/2018
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento do País  de  TERESA CRISTINA TAVARES DALLA COSTA,  Professor  do
Magistério Superior, lotada e em exercício no Departamento de Produção e Controle de Medicamentos da
Faculdade  de  Farmácia,  com  a  finalidade  de  participar  do  "Iberoamerican  Pharmacometrics  Network
Congress 2018", em Guadalajara, México, no período compreendido entre 03/11/2018 e 11/11/2018, incluído
trânsito, com ônus limitado. Solicitação nº 60191.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
