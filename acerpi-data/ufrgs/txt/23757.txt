Documento gerado sob autenticação Nº TSL.518.558.KKO, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4586                  de  23/06/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de JUAN MARTÍN BRAVO, Professor do Magistério Superior, lotado
e em exercício no Departamento de Obras Hidráulicas do Instituto de Pesquisas Hidráulicas, com a finalidade
de  ministrar  curso  na  Universidad  Nacional  de  Asunción,  Paraguai,  no  período  compreendido  entre
10/07/2016 e 16/07/2016, incluído trânsito, com ônus limitado. Solicitação nº 20997.
CARLOS ALEXANDRE NETTO
Reitor
