Documento gerado sob autenticação Nº IRP.043.413.T0H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7394                  de  15/08/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar MICHELLE RAUPP SELISTER, Matrícula SIAPE 2446863, ocupante do cargo de Técnico em
Assuntos Educacionais, Código 701079, do Quadro de Pessoal desta Universidade, para exercer a função de
Coordenador do Núcleo Acadêmico Administrativo de Graduação do IFCH, código SRH 1433, FG-4,  com
vigência a partir da data de publicação no Diário Oficial da União. Processo nº 23078.521622/2019-97.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
