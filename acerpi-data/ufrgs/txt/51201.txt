Documento gerado sob autenticação Nº YKV.290.564.EQ6, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1592                  de  22/02/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias,
RESOLVE
Designar  ISMAEL  CABRAL,  Matrícula  SIAPE  2994373,  ocupante  do  cargo  de  Bibliotecário-
documentalista, Código 701010, do Quadro de Pessoal desta Universidade, para exercer a função de Chefe
da Biblioteca do CECLIMAR, Código SRH 453, código FG-5, com vigência a partir da data de publicação no
Diário Oficial da União. Processo nº 23078.522992/2017-80.
RICARDO DEMETRIO DE SOUZA PETERSEN
Decano do Conselho Universitário, no exercício da Reitoria.
