Documento gerado sob autenticação Nº HHQ.144.358.KCF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1567                  de  14/02/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder ao servidor ALEXANDRE DE LIMA,  ocupante do cargo de  Engenheiro-área - 701031,
lotado na Superintendência de Infraestrutura, SIAPE 2073508, o percentual de 30% (trinta por cento) de
Incentivo à Qualificação, a contar de 24/01/2020, tendo em vista a conclusão do curso de Especialização em
Administração Pública Comtemporânea, conforme o Processo nº 23078.501704/2020-59.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
