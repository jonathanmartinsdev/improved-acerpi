Documento gerado sob autenticação Nº GDW.057.280.0JR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             924                  de  27/01/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de EDUARDO MUNHOZ SVARTMAN,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Ciência Política do Instituto de Filosofia e Ciências
Humanas, com a finalidade de participar de cursos junto ao Instituto Superior de Relações Internacionais, em
Maputo, Moçambique, no período compreendido entre 13/02/2017 e 24/02/2017, incluído trânsito, com ônus
limitado. Solicitação nº 25854.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
