Documento gerado sob autenticação Nº CZS.504.072.IHN, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6881                  de  28/07/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme o Laudo Médico n°44854,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  a  ocupante  do  cargo  de  BIBLIOTECÁRIO-
DOCUMENTALISTA, do Quadro de Pessoal desta Universidade, ALESSANDRA VIRGINIA DE OLIVEIRA (Siape:
1756196 ),  para substituir   ANDREA REGINA SANTOS DE FREITAS (Siape: 1860063 ), Chefe da Biblioteca de
Educação, Código FG-5, em seu afastamento por motivo de Laudo Médico do titular da Função, no período de
08/06/2017 a 09/06/2017, com o decorrente pagamento das vantagens por 2 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
