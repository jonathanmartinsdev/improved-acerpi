Documento gerado sob autenticação Nº LRU.788.621.T9C, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9735                  de  08/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  25/11/2016,   referente  ao  interstício  de
08/04/2015 a 24/11/2016, para o servidor DANIEL OLIVEIRA HILARIO, ocupante do cargo de Técnico em
Eletroeletrônica  -  701232,  matrícula  SIAPE  1960254,   lotado  na   Superintendência  de  Infraestrutura,
passando  do  Nível  de  Classificação/Nível  de  Capacitação  D  II,  para  o  Nível  de  Classificação/Nível  de
Capacitação  D  III,  em  virtude  de  ter  realizado  o(s)  seguinte(s)  curso(s),  conforme  o  Processo  nº
23078.024844/2016-13:
Formação Integral de Servidores da UFRGS II CH: 48 (31/10/2013 a 13/11/2014)
SENGE-RS - Subestações 13,8 Kv CH: 20 Carga horária utilizada: 2 hora(s) / Carga horária excedente: 18
hora(s) (27/10/2014 a 31/10/2014)
SISNEMA - Formação Administração de Redes Multiplataforma CH: 70 (10/02/2014 a 24/10/2014)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
