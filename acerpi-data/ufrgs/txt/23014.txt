Documento gerado sob autenticação Nº HQC.124.734.E67, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4145                  de  07/06/2016
O VICE-REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Tornar sem efeito| a Portaria nº 4016/2016, de 03/06/2016, que concedeu progressão funcional, por
avaliação de desempenho, no Quadro desta Universidade, ao Professor RENATO SELIGMAN,  lotado e em
exercício no Departamento de Medicina Interna da Faculdade de Medicina. Processo nº 23078.503514/2016-
90.
RUI VICENTE OPPERMANN
Vice-Reitor
