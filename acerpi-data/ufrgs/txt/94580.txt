Documento gerado sob autenticação Nº PXV.246.685.70K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6197                  de  19/07/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  ANAPAULA SOMMER VINAGRE,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Fisiologia do Instituto de Ciências Básicas da Saúde, com
a finalidade de participar  do "10th International  Congress of  Comparative Physiology and Biochemistry
(ICCPB 2019)",  em Ottawa,  Canadá,  no período compreendido entre 03/08/2019 e  11/08/2019,  incluído
trânsito, com ônus CAPES/PRINT/UFRGS. Solicitação nº 85648.
RUI VICENTE OPPERMANN
Reitor
