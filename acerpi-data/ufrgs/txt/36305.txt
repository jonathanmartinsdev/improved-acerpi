Documento gerado sob autenticação Nº RTH.861.670.UTV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2869                  de  03/04/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar EZEQUIEL DA ROSA MEDEIROS, Matrícula SIAPE 0358455, ocupante do cargo de Porteiro,
Código 701458, do Quadro de Pessoal desta Universidade, para exercer a função de Coordenador do Núcleo
de Infraestrutura da Gerência Administrativa da Escola de Educação Física, Fisioterapia e Dança, Código SRH
766,  Código FG-7,  com vigência a partir  da data de publicação no Diário Oficial  da União.  Processo nº
23078.024759/2016-55.
JANE FRAGA TUTIKIAN
Vice-Reitora.
