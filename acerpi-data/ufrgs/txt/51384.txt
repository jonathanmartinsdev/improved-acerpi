Documento gerado sob autenticação Nº KBI.076.542.TV8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1707                  de  28/02/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Nomear  o  ocupante  do  cargo  de  Engenheiro-área  do  Quadro  de  Pessoal  desta  Universidade,
GUILHERME  MARTINS  SIQUEIRA,  matrícula  SIAPE  2055940,  para  exercer  o  cargo  de  Diretor  do
Departamento de Projetos e Planejamento vinculado à Vice-Superintendência de Infraestrutura da SUINFRA,
código 63, código CD-4, com vigência a partir da data de publicação no Diário Oficial da União. Processo nº
23078.502830/2018-14.
RUI VICENTE OPPERMANN
Reitor.
