Documento gerado sob autenticação Nº ZEP.598.243.2QT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6028                  de  16/07/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
                         Autorizar o afastamento no País de WILLIAM FERNANDES MOLINA, ocupante do cargo de
Professor do Magistério do Ensino Básico, Técnico e Tecnológico, lotado e com exercício no Departamento de
Expressão e Movimento do Colégio de Aplicação, com a finalidade de realizar estudos em nível de Doutorado
junto à Universidade Federal do Rio Grande do Sul, em Porto Alegre/RS, no período compreendido entre
31/08/2019 e 30/08/2020, com ônus limitado. Processo 23078.513237/2019-76.
JANE FRAGA TUTIKIAN
Vice-Reitora.
