Documento gerado sob autenticação Nº VCY.806.331.SNN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3864                  de  25/05/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor NILTON MULLET PEREIRA, matrícula SIAPE n° 1531987, lotado e em exercício no Departamento de
Ensino e Currículo da Faculdade de Educação, da classe D  de Professor Associado, nível 01, para a classe D 
de Professor Associado, nível 02, referente ao interstício de 24/05/2014 a 23/05/2016, com vigência financeira
a partir de 24/05/2016, de acordo com o que dispõe a Decisão nº 197/2006-CONSUN, alterada pela Decisão
nº 401/2013-CONSUN. Processo nº 23078.008137/2016-80.
RUI VICENTE OPPERMANN
Vice-Reitor
