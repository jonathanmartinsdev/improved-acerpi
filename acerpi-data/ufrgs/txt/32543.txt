Documento gerado sob autenticação Nº FJK.477.374.M7H, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             214                  de  09/01/2017
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 19 de dezembro de 2016,  de acordo com o artigo 36, parágrafo único, inciso II
da Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de
1997, RITA DE CASSIA SOARES DE SOUZA BUENO, ocupante do cargo de Técnico em Assuntos Educacionais,
Ambiente Organizacional  Ciências  Humanas,  Jurídicas  e  Econômicas,  Código 701079,  Classe E,  Nível  de
Capacitação IV, Padrão de Vencimento 06, SIAPE nº. 1684524 do Instituto de Filosofia e Ciências Humanas
para o Colégio de Aplicação, com exercício na Creche Francesca Zacaro Faraco.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
