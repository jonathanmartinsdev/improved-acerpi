Documento gerado sob autenticação Nº EKM.792.077.6N2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6189                  de  12/07/2017
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições, 
RESOLVE
Dispensar, a partir de 31/05/2017, o ocupante do cargo de Professor do Magistério Superior, classe
Titular, do Quadro de Pessoal desta Universidade, PAULO DORNELLES PICON, matrícula SIAPE n° 0356802, da
função de Chefe  Substituto  do Depto de Medicina Interna da Faculdade de Medicina,  para  a  qual  foi
designado pela Portaria 8738/2015, de 09/11/2015. Processo nº 23078.010651/2017-66.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
