Documento gerado sob autenticação Nº RKV.610.191.5T7, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8122                  de  06/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 30/09/2019,  a ocupante do cargo de Professor do Magistério
Superior, classe Associado do Quadro de Pessoal desta Universidade, ADRIANA WAGNER, matrícula SIAPE n°
1667658, da função de Coordenadora Substituta do PPG em Psicologia,  para a qual foi  designada pela
Portaria 7173, de 11/09/2018. Processo nº 23078.523740/2019-30.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
