Documento gerado sob autenticação Nº IXN.136.419.3HN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8008                  de  03/09/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Lotar na Faculdade de Ciências Econômicas, com exercício no Núcleo Acadêmico Administrativo do
CISADE,  Ambiente Organizacional  Informação,  ROGER DIEGO SOBIERAJSKI  ROCHA,  nomeado conforme
Portaria Nº 7178/2019 de 09 de agosto de 2019, publicada no Diário Oficial da União no dia 12 de agosto de
2019, em efetivo exercício desde 27 de agosto de 2019, ocupante do cargo de TÉCNICO DE TECNOLOGIA DA
INFORMAÇÃO,  classe  D,  nível  I,  padrão  101,  no  Quadro  de  Pessoal  desta  Universidade.  Processo  n°
23078.523012/2019-28.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
