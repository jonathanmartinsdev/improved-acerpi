Documento gerado sob autenticação Nº CIC.595.197.T8G, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3845                  de  25/05/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias, 
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal  desta  Universidade,  ANTONIO  SHIGUEAKI  TAKIMI,  matrícula  SIAPE  n°  2831648,  lotado  no
Departamento de Metalurgia da Escola de Engenharia,  como Coordenador Substituto da COMGRAD de
Engenharia Metalúrgica, para substituir automaticamente o titular desta função em seus afastamentos ou
impedimentos regulamentares na vigência do presente mandato. Processo nº 23078.512427/2018-95.
JANE FRAGA TUTIKIAN
Vice-Reitora.
