Documento gerado sob autenticação Nº USW.003.269.SIQ, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3287                  de  18/04/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  ALBERTO  VASCONCELLOS  INDA  JUNIOR,  Professor  do
Magistério Superior,  lotado no Departamento de Solos da Faculdade de Agronomia e com exercício no
Programa de Pós-Graduação em Ciência do Solo, com a finalidade de participar da "16th Internacional Clay
Conference:  Clays,  From the Oceans to Space",  em Granada,  Espanha,  no período compreendido entre
15/07/2017 e 21/07/2017, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa - diárias). Solicitação
nº 25940.
RUI VICENTE OPPERMANN
Reitor
