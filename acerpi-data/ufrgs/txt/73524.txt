Documento gerado sob autenticação Nº LTB.958.276.B2K, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10121                  de  13/12/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 14 de novembro de 2018,  de acordo com o artigo 36, parágrafo único, inciso II
da Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de
1997, JOSE ALEXANDRE DOS SANTOS GAMA, ocupante do cargo de Apontador, Ambiente Organizacional
Administrativo,  Código 701602,  Classe B,  Nível  de Capacitação III,  Padrão de Vencimento 16,  SIAPE nº.
0357956 da Pró-Reitoria de Planejamento e Administração para a lotação Superintendência de Infraestrutura,
com novo exercício na Prefeitura Campus do Vale.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
