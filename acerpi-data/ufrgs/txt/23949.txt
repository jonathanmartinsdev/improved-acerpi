Documento gerado sob autenticação Nº ZEZ.985.817.LJG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4755                  de  28/06/2016
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de
2013
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  20%,  a  partir  de  11/02/2016,
correspondente ao grau Insalubridade Máxima,  ao servidor RONEI GIUSTI OSÓRIO,  Identificação Única
22865136, Técnico de Laboratório Área, com exercício no Centro de Microscopia Eletrônica, observando-se o
disposto na Lei nº 8.112, de 11 de dezembro de 1990, combinado com a Lei 8.270, de 17 de dezembro de
1991,  por  exercer  atividades  em áreas  consideradas  Insalubres  conforme Laudo Pericial  constante  no
Processo nº 23078.200983/2016-50, Código SRH n° 22823 e Código SIAPE 2016002226.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
