Documento gerado sob autenticação Nº CUF.947.052.0AR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10467                  de  20/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  ROBERTA  CAMINEIRO  BAGGIO,  Professor  do  Magistério
Superior, lotada e em exercício no Departamento de Direito Público e Filosofia do Direito da Faculdade de
Direito, com a finalidade de participar de reunião junto à Universidad Nacional de San Martin, em Buenos
Aires,  Argentina,  no período compreendido entre 25/11/2019 e 26/11/2019,  incluído trânsito,  com ônus
limitado. Solicitação nº 88997.
RUI VICENTE OPPERMANN
Reitor
