Documento gerado sob autenticação Nº BCC.860.179.9JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10608                  de  28/12/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Retificar a Portaria n° 7427/2018, de 19/09/2018, a pedido, que concede prorrogação de jornada de
trabalho reduzida com remuneração proporcional à servidora PATRICIA CRISTINA SCHERER, Administrador,
com exercício no Núcleo de Contratos e Normativas da Pró-Reitoria de Planejamento e Administração.
Processo nº 23078.502816/2018-11.
 
Onde se lê:
"no período de 10 de outubro de 2018 a 10 de abril de 2019",
leia-se:
"no período de 10 de outubro de 2018 a 10 de janeiro de 2019".
RUI VICENTE OPPERMANN
Reitor
