Documento gerado sob autenticação Nº IJR.261.168.P57, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8889                  de  04/11/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Exonerar,  a  partir  da data de publicação no Diário  Oficial  da União,  a  ocupante do cargo de
Engenheiro-área, 274243, E, IV, do Quadro de Pessoal desta Universidade, MAGDA DA SILVEIRA ELKFURY,
matrícula SIAPE n° 1870553, do cargo de Prefeita do Campus Saúde e Olímpico da Vice-Superintendência de
Manutenção  da  SUINFRA,  Código  SRH  1014,  para  o  qual  foi  nomeada  pela  Portaria  n°  3312,  de
05/06/2013, publicada no Diário Oficial da União de 07/06/2013, por ter sido nomeada para outro cargo de
direção. Processo nº 23078.022447/2016-15.
RUI VICENTE OPPERMANN
Reitor
