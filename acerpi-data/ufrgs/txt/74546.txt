Documento gerado sob autenticação Nº IJO.842.745.9JV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             255                  de  09/01/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria especial com fundamento no artigo 57 da Lei nº 8.213, de 24 de julho de
1991, e conforme o constante na Orientação Normativa nº 16, de 23 de dezembro de 2013, a DANIELA
BENITES ROSITO, matrícula SIAPE nº 1032153, no cargo de Odontologo, nível de classificação E, nível de
capacitação IV,  padrão 15,  do Quadro desta  Universidade,  no regime de quarenta  horas  semanais  de
trabalho, com exercício no Núcleo Especializado da Gerência Administrativa da Faculdade de Odontologia,
com proventos integrais e calculados de acordo com o artigo 1º da Lei nº 10.887, de 18 de junho de 2004.
Processo 23078.532743/2018-83.
RUI VICENTE OPPERMANN
Reitor.
