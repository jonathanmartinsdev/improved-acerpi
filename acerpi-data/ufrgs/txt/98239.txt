Documento gerado sob autenticação Nº ZCT.888.422.FE1, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8471                  de  18/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5060147-65.2018.4.04.7100,  da  5ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria nº 1874, de
13/07/2006, do servidor JOSE ALEXANDRE DOS SANTOS GAMA, matrícula SIAPE n° 0357956, ativo no cargo
de Apontador  -  701602,  do  nível  II  para  o  nível  IV,  a  contar  de  01/01/2006,  conforme o  Processo  nº
23078.524946/2019-87.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
