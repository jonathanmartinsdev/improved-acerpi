Documento gerado sob autenticação Nº MDV.080.593.T8R, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7207                  de  12/09/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,  no uso de suas atribuições,
considerando o disposto na Portaria nº 0020, de 20 de setembro de 2016
RESOLVE
Tornar  sem  efeito  a  partir  da  data  deste  ato,  a  Portaria  nº  5944/2018,  de  07/08/2018,  que
designou KARINA DE OLIVEIRA AZZOLIN, ocupante do cargo de Professor do Magistério Superior, lotada no
Departamento de Enfermagem Médico-Cirúrgica da Escola de Enfermagem e com exercício na Comissão de
Pesquisa de Enfermagem, para exercer 'pró-tempore' a função de Coordenadora da Comissão de Pesquisa
da Escola de Enfermagem, Código SRH 777, com vigência a partir de 28/08/2018 até 01/10/2018. Processo nº
23078.519422/2018-93.
RUI VICENTE OPPERMANN
Reitor
