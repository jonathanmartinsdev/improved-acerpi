Documento gerado sob autenticação Nº IEO.679.739.HC9, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5611                  de  04/07/2019
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal desta Universidade, TERESA CRISTINA TAVARES DALLA COSTA, matrícula SIAPE n° 0357550, lotada no
Departamento de Produção e Controle de Medicamentos da Faculdade de Farmácia, como Coordenadora
Substituta do PPG em Ciências Biológicas: Farmacologia e Terapêutica, com vigência a partir da data deste
ato, a fim de completar o mandato da professora ROSANE GOMEZ. Processo nº 23078.517242/2019-58.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
