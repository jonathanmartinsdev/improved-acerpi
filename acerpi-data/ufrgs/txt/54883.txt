Documento gerado sob autenticação Nº YYN.160.492.6CN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3988                  de  30/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições legais  e estatutárias ,  considerando o disposto na Portaria nº  7612,  de 29 de
setembro de 2016
RESOLVE
Tornar sem efeito a nomeação do candidato abaixo relacionado, ocorrida através da Portaria nº
2721/2018, de 12/04/2018, publicada no Diário Oficial da união de 17 de julho de 2017, de acordo com o que
preceitua o § 6º do artigo 13º, da Lei nº 8.112, de 11 de dezembro de 1990, com a redação dada pela Lei nº.
9.527, de 10 de dezembro de 1997. Processo nº 23078.025182/2016-07.
 
 
TÉCNICO EM ELETRÔNICA - Classe D - Padrão 1
- GILBERTO COUTINHO DE GODOY - Vaga SIAPE 275539
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas.
