Documento gerado sob autenticação Nº ADW.217.975.436, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1278                  de  04/02/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CARLOS TORRES FORMOSO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Engenharia Civil da Escola de Engenharia, com a finalidade de
ministrar  palestra  junto  à  San  Diego  State  University,  em  San  Diego,  Estados  Unidos,  no  período
compreendido entre 03/03/2019 e 09/03/2019, incluído trânsito, com ônus limitado. Solicitação nº 61947.
RUI VICENTE OPPERMANN
Reitor
