Documento gerado sob autenticação Nº SYZ.292.934.D84, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3793                  de  23/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias,
RESOLVE
Dispensar,  a  pedido,  a partir  de 17/05/2018,  a ocupante do cargo de Professor do Magistério
Superior, classe Associado do Quadro de Pessoal desta Universidade, SUSANA CARDOSO, matrícula SIAPE n°
2176043, da função de Coordenadora Substituta da COMGRAD de Veterinária, para a qual foi designada pela
Portaria nº 5118/2017, de 12/06/2017. Processo nº 23078.511769/2018-98.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
