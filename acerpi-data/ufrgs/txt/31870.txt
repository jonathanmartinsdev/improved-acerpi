Documento gerado sob autenticação Nº ZBI.492.605.9G3, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10218                  de  23/12/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°31972,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada  pela  Lei  nº.9.527,  de  10  de  dezembro  de  1997,  o  ocupante  do  cargo  de  BIBLIOTECÁRIO-
DOCUMENTALISTA,  do  Quadro  de  Pessoal  desta  Universidade,  JOIDI  ANSELMO  DE  ANSELMO  (Siape:
1645885 ),  para substituir   ROSALIA POMAR CAMARGO (Siape: 6357127 ), Chefe da Biblioteca de Biociências,
Código FG-5, em seu afastamento por motivo de férias, no período de 26/12/2016 a 24/01/2017, com o
decorrente pagamento das vantagens por 30 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
