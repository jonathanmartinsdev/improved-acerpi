Documento gerado sob autenticação Nº AMC.855.411.5OA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2435                  de  20/03/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de FRANCISCO ELISEU AQUINO, Professor do Magistério Superior,
lotado e em exercício no Departamento de Geografia do Instituto de Geociências,  com a finalidade de
participar de reunião junto ao Climatic Change Institute, em Orono e  do "Annual Meeting of the  Association
of  American Geographers",  em Boston,  Estados Unidos,  no período compreendido entre  01/04/2017 e
11/04/2017, incluído trânsito, com ônus pelo projeto Instituto Nacional de Ciência e Tecnologia da Criosfera
(Processo CNPq 465680/2014-3). Solicitação nº 26698.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
