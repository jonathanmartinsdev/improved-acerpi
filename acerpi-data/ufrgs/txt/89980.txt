Documento gerado sob autenticação Nº NCR.106.475.HFM, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3443                  de  22/04/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5019813-52.2019.4.04.7100,  da  6ª  Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo II da Portaria nº 1874, de
13/07/2006, da servidora SANDRA MARIA RIBEIRO DA SILVA, matrícula SIAPE n° 0356934, aposentada no
cargo de Auxiliar em Administração - 701405, do Nível I para o Nível IV, a contar de 01/01/2006, conforme o
Processo nº 23078.509793/2019-48.
 
RUI VICENTE OPPERMANN
Reitor
