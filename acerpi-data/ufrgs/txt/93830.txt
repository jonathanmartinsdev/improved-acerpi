Documento gerado sob autenticação Nº MSP.983.142.9F5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5779                  de  10/07/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar, a pedido, a partir de 04/07/2019, a ocupante do cargo de Engenheiro-área - 701031, do
Nível de Classificação EIV, do Quadro de Pessoal desta Universidade, GISELLE REIS ANTUNES,  matrícula
SIAPE 2054143, da função de Chefe do Setor de Fiscalização e Adequação - SUINFRA, Código SRH 999, Código
FG-3, para a qual foi designada pela Portaria nº 2949/2019, de 04/04/2019, publicada no Diário Oficial da
União de 05/04/2019. Processo nº 23078.516239/2019-17.
RUI VICENTE OPPERMANN
Reitor.
