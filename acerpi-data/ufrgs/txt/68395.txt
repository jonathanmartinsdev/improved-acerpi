Documento gerado sob autenticação Nº AOY.939.721.BSP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6439                  de  20/08/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  06/08/2018,   referente  ao  interstício  de
30/05/2016 a 05/08/2018, para a servidora DENISE MOREIRA CANARIM, ocupante do cargo de Técnico em
Geologia - 701239, matrícula SIAPE 1356044,  lotada  no  Instituto de Geociências, passando do Nível de
Classificação/Nível de Capacitação D II, para o Nível de Classificação/Nível de Capacitação D III, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.519913/2018-34:
Formação Integral de Servidores da UFRGS IV CH: 95 (27/10/2014 a 27/06/2018)
PROREXT - Geologia dos Andes Centrais CH: 124 Carga horária utilizada: 25 hora(s) / Carga horária excedente:
99 hora(s) (14/10/2016 a 29/10/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
