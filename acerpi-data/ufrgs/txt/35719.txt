Documento gerado sob autenticação Nº APM.859.308.5OA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2436                  de  20/03/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  ALY  FERREIRA  FLORES  FILHO,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Engenharia Elétrica da Escola de Engenharia, com a
finalidade  de  participar  da  "IEEE  International  Magnetics  Conference",  em  Dublin,  Irlanda,  no  período
compreendido entre 20/04/2017 e 02/05/2017, incluído trânsito, com ônus limitado. Solicitação nº 26222.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
