Documento gerado sob autenticação Nº IZT.581.865.VM6, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1192                  de  18/02/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.5469, de 04 de outubro de 2012, do
Magnífico Reitor, e conforme a Solicitação de Férias n°28928,
RESOLVE:
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, MARCIO DORN (Siape: 1977366 ),  para substituir  
ÁLVARO  FREITAS  MOREIRA  (Siape:  1355219  ),  Chefe  do  Depto  de  Informática  Teórica  do  Instituto  de
Informática, Código FG-1, em seu afastamento por motivo de férias, no período de 04/02/2016 a 22/02/2016,
com o decorrente pagamento das vantagens por 19 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
