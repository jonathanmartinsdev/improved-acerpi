Documento gerado sob autenticação Nº DOM.562.781.F6Q, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10476                  de  21/11/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal  desta  Universidade,  LEANDRO  LANGIE  ARAUJO,  matrícula  SIAPE  n°  2474260,  lotado  no
Departamento de Física do Instituto de Física, para exercer a função de Chefe do Depto de Física do Instituto
de Física, Código SRH 235, código FG-1, com vigência a partir de 08/01/2020 até 07/01/2022, por ter sido
reeleito. Processo nº 23078.532165/2019-66.
JANE FRAGA TUTIKIAN
Vice-Reitora.
