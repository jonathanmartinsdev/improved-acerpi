Documento gerado sob autenticação Nº VXD.354.995.9FR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             672                  de  17/01/2020
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  17/01/2020,   referente  ao  interstício  de
05/07/2018 a 16/01/2020, para o servidor MATEUS BALLARDIN, ocupante do cargo de Técnico em Assuntos
Educacionais - 701079,  matrícula SIAPE 3055295,  lotado  no  Instituto de Física, passando do Nível de
Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.501173/2020-02:
Formação Integral de Servidores da UFRGS III CH: 63 (31/07/2018 a 06/12/2019)
Lumina - Desconstruindo o Racismo na Prática UNIAFRO/UFRGS CH: 60 Carga horária utilizada: 35 hora(s) /
Carga horária excedente: 25 hora(s) (16/01/2020 a 17/01/2020)
LUMINA - O Golpe de 2016 CH: 22 (18/01/2019 a 09/07/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
