Documento gerado sob autenticação Nº QRP.633.406.K5B, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1258                  de  08/02/2018
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 5298, de 22 de outubro
de 2010
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido ao servidor NEILSON RAMOS ROCHA,
ocupante do cargo de Assistente em Administração-701200, lotado na Pró-Reitoria de Gestão de Pessoas,
SIAPE 2340147, para 30% (trinta por cento), a contar de 23/01/2018, tendo em vista a conclusão do curso de
Especialização  MBA  em  Administração  Pública  e  Gerência  de  Cidades,  conforme  o  Processo  nº
23078.501221/2018-30.
MARILIA BORGES HACKMANN
Pró-Reitora de Gestão de Pessoas em exercício
