Documento gerado sob autenticação Nº XWB.735.601.TUV, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2338                  de  15/03/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 6º, incisos I, II, III e IV, da Emenda Constitucional nº
41, de 19 de dezembro de 2003, publicada no Diário Oficial  da União do dia 31 subsequente, a JULIO
ALBERTO NITZKE,  matrícula SIAPE nº 2174667, no cargo de Professor Associado, nível 3, da Carreira do
Magistério Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com exercício no
Departamento  de  Tecnologia  dos  Alimentos  do  Instituto  de  Ciências  e  Tecnologia  de  Alimentos,  com
proventos integrais. Processo 23078.204331/2016-94.
RUI VICENTE OPPERMANN
Reitor.
