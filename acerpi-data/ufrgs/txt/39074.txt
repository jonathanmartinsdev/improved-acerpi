Documento gerado sob autenticação Nº XSN.400.788.J4F, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4853                  de  01/06/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°27453,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, ANDERSON MACIEL (Siape: 1809462 ),  para substituir  
MARA ABEL (Siape: 0357591 ), Coordenador da Comissão de Pesquisa do Instituto de Informática,  em seu
afastamento do país, no dia 17/05/2017.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
