Documento gerado sob autenticação Nº ZGV.829.844.QF7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7623                  de  23/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 4183, de 17 de outubro de 2008
RESOLVE
Lotar  no  Centro  Estadual  de  Pesquisas  em Sensoriamento  Remoto  e  Meteorologia,  Ambiente
Organizacional Administrativo, a partir de 05/07/2019, ANA LUCIA DA SILVA FREITAS, matrícula SIAPE n°
1182722, redistribuída conforme Portaria nº 807 de 28 de maio de 2019 do Ministério da Educação, publicada
no Diário Oficial da União no dia 30 de maio de 2019, ocupante do cargo de Técnico em Secretariado, Classe
D,  Nível  IV,  Padrão  de  Vencimento  04,  no  Quadro  de  Pessoal  desta  Universidade.  Processo
nº  23078.501370/2019-80.
MARCELO SOARES MACHADO
Pró-Reitor de Gestão de Pessoas em exercício
