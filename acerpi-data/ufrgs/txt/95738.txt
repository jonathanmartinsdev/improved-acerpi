Documento gerado sob autenticação Nº LZE.692.743.8FD, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7002                  de  02/08/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Férias n°49342,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de ADMINISTRADOR, do Quadro de
Pessoal  desta  Universidade,  RACHEL KERBER GONÇALVES  (Siape:  1869113  ),   para  substituir    ALICE
SCHAFFER DA ROSA (Siape: 1733107 ), Diretor da Divisão de Contratação de Serviços Terceirizados, vinculada
à Ger de Serv Terc da PROGESP, Código FG-1, em seu afastamento por motivo de férias, no período de
05/08/2019 a 09/08/2019, com o decorrente pagamento das vantagens por 5 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
