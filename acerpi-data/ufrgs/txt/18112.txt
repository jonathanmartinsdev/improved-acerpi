Documento gerado sob autenticação Nº ESD.986.357.I89, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1292                  de  23/02/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de Outubro de 2012
RESOLVE:
Conceder  Progressão  por  Capacitação,  a  contar  de  21/01/2016,   referente  ao  interstício  de
20/12/2013 a 20/01/2016, para a servidora TAIS BARBOSA, ocupante do cargo de Técnico em Assuntos
Educacionais - 701079, matrícula SIAPE 2721686,  lotada  na  Secretaria de Educação a Distância, passando
do Nível de Classificação/Nível de Capacitação E II, para o Nível de Classificação/Nível de Capacitação E III, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.001175/2016-10:
Formação Integral de Servidores da UFRGS VI CH: 153 Carga horária utilizada: 150 hora(s) / Carga horária
excedente: 3 hora(s) (31/10/2013 a 18/12/2015)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
