Documento gerado sob autenticação Nº TDN.803.572.4HR, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1125                  de  06/02/2017
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar o afastamento do país de HUGO VERLI, Professor do Magistério Superior, lotado e em
exercício  no  Departamento  de  Biologia  Molecular  e  Biotecnologia  do  Instituto  de  Biociências,  com  a
finalidade  de  proferir  palestra  na  "Conference  on  Atomistic  Simulations  of  Biomolecules:  towards  a
Quantitative Understanding of Life Machinery", em Trieste, Itália, no período compreendido entre 04/03/2017
e 11/03/2017, incluído trânsito, com ônus limitado. Solicitação nº 26025.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
