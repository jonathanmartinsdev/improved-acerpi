Documento gerado sob autenticação Nº ORD.936.660.9CC, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3102                  de  27/04/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de Leandro Raizer, Professor do Magistério Superior, lotado e em
exercício no Departamento de Ensino e Currículo da Faculdade de Educação, com a finalidade de participar
do "3rd Forum of Sociology of the International Sociological Association", em Viena, Áustria, no período
compreendido entre 08/07/2016 e 16/07/2016, incluído trânsito, com ônus UFRGS (Pró-Reitoria de Pesquisa -
diárias). Solicitação nº 18531.
CARLOS ALEXANDRE NETTO
Reitor
