Documento gerado sob autenticação Nº SXQ.193.450.IBH, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3414                  de  11/05/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Exonerar, a pedido, a partir de 26 de fevereiro de 2018, nos termos do artigo 34 "caput", da Lei n°
8.112, de 1990, MELISSA DE AGUIAR PEREIRA, ocupante do cargo de Assistente em Administração, código
701200, nível de classificação D, nível de capacitação III, padrão 03, do Quadro de Pessoal, lotado e com
exercício no Campus Litoral Norte. Processo nº 23078.503392/2018-01.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
