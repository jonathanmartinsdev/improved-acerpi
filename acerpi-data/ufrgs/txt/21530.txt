Documento gerado sob autenticação Nº KFA.406.975.OJK, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3316                  de  04/05/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  JAIME URDAPILLETA TAROUCO,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento de Zootecnia da Faculdade de Agronomia, com a finalidade
de participar de jornadas da Cooperativa Colonizadora Multiactiva Fernheim Ltda, em Filadelfia, Paraguai, no
período compreendido entre 22/05/2016 e 26/05/2016, incluído trânsito, com ônus limitado. Solicitação nº
19670.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
