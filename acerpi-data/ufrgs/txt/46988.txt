Documento gerado sob autenticação Nº UDI.012.895.F76, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10388                  de  10/11/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  09/11/2017,   referente  ao  interstício  de
09/05/2016 a 08/11/2017, para a servidora THAIS SANTANA DA ROSA, ocupante do cargo de Economista -
701026,  matrícula SIAPE 1310561,  lotada  na  Faculdade de Ciências Econômicas, passando do Nível de
Classificação/Nível de Capacitação E I, para o Nível de Classificação/Nível de Capacitação E II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.518628/2017-15:
Formação Integral  de Servidores da UFRGS I  CH: 26 Carga horária utilizada: 25 hora(s)  /  Carga horária
excedente: 1 hora(s) (10/05/2016 a 09/11/2016)
ILB - Direito Administrativo para gerentes no setor público CH: 35 (14/07/2016 a 03/08/2016)
ENAP - Introdução à Gestão de Processos CH: 20 (31/05/2016 a 30/06/2016)
ENAP - Gestão Estratégica de Pessoas e Planos de Carreira CH: 20 (31/05/2016 a 30/06/2016)
ENAP - Sistema Eletrônico de Informações - SEI CH: 20 (23/08/2016 a 12/09/2016)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
