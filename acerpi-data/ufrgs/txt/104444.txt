Documento gerado sob autenticação Nº PKJ.248.290.KQ2, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             941                  de  28/01/2020
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder à servidora DAIANE BENETTI, ocupante do cargo de Técnico em Assuntos Educacionais -
701079, lotada no Instituto de Letras, SIAPE 1316247, o percentual de 30% (trinta por cento) de Incentivo à
Qualificação,  a  contar  de 26/12/2019,  tendo em vista  a  conclusão da Especialização em Administração
Pública Contemporânea, conforme o Processo nº 23078.535492/2019-70.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
