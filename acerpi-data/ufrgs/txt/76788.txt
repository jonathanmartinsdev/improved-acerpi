Documento gerado sob autenticação Nº WQX.116.419.5VS, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1546                  de  13/02/2019
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 5658,
de 29 de junho de 2017
RESOLVE
Retificar a Portaria n° 1491/2019, de 11/02/2019, que concede jornada de trabalho reduzida com
remuneração proporcional à servidora ALINE MARTINS JUSTO, matrícula SIAPE n° 3318791, ocupante do
cargo de Odontologo - 701063, lotada na Faculdade de Odontologia e com exercício no Núcleo Especializado
da Gerencia Administrativa da ODONTO, alterando a jornada de trabalho de oito horas diárias e quarenta
horas semanais para 6 horas diárias e 30 semanais, nos termos dos artigos 5º a 7º da Medida Provisória nº
2.174-28, de 24 de agosto de 2001, e conforme o Processo nº 23078.514672/2018-37.
 
Onde se lê:
<"[...] alterando a jornada de trabalho de oito horas diárias e quarenta horas semanais para 6 horas
diárias e 30 semanais [...]">,
leia-se:
<"[...] alterando a jornada de trabalho de oito horas diárias e quarenta horas semanais para 6 horas
diárias e 30 semanais, no período de 16 de fevereiro de 2019 a 16 de fevereiro de 2020, [...]">.
DANILO BLANK
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
