Documento gerado sob autenticação Nº JSB.617.567.N98, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1286                  de  19/02/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE:
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora BEATRIZ D AGORD SCHAAN, matrícula SIAPE n° 1539521, com exercício no Departamento de
Medicina Interna da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 04, para a classe D  de
Professor Associado, nível 01, referente ao interstício de 09/11/2013 a 08/11/2015, com vigência financeira a
partir de 18/02/2016, de acordo com o que dispõe a Decisão nº 197/2006-CONSUN, alterada pela Decisão nº
401/2013-CONSUN. Processo nº 23078.029075/2015-69.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
