Documento gerado sob autenticação Nº LQA.070.998.9U7, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4709                  de  27/06/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE
Conceder promoção funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora MILENE JORGE ALIVERTI, matrícula SIAPE n° 1545280, lotada e em exercício no Departamento de
Música do Instituto de Artes, da classe B  de Professor Assistente, nível 02, para a classe C  de Professor
Adjunto, nível 01, referente ao interstício de 08/08/2012 a 07/08/2014, com vigência financeira a partir de
24/06/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela Decisão nº 401/2013-
CONSUN. Processo nº 23078.007565/2016-95.
RUI VICENTE OPPERMANN
Vice-Reitor
