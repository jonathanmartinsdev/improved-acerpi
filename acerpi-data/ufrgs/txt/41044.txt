Documento gerado sob autenticação Nº MVM.171.334.DML, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6222                  de  13/07/2017
O  DECANO  DO  CONSELHO  UNIVERSITÁRIO,  NO  EXERCÍCIO  DA  REITORIA   DA  UNIVERSIDADE
FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  país  de  DILERMANDO  CATTANEO  DA  SILVEIRA,  Professor  do
Magistério Superior, lotado e em exercício no Departamento Interdisciplinar do Campus Litoral Norte, com a
finalidade de realizar trabalho de campo em Puerto Iguazú, Argentina, no período compreendido entre
14/07/2017 e 15/07/2017, incluído trânsito, com ônus UFRGS (diárias). Solicitação nº 29520.
CELSO GIANNETTI LOUREIRO CHAVES
DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA
