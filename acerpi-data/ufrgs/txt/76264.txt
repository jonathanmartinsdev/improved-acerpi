Documento gerado sob autenticação Nº URU.216.802.436, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1277                  de  04/02/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de ANTONIO PAQUES, Professor do Magistério Superior, lotado no
Departamento de Matemática Pura e Aplicada do Instituto de Matemática e Estatística e com exercício no
Programa de Pós-Graduação em Matemática Pura, com a finalidade de participar do "Colloquium of Algebras
and  Representations  -  Quantum  2019",  em  Montevidéu,  Uruguai,  no  período  compreendido  entre
03/03/2019 e 08/03/2019, incluído trânsito, com ônus limitado. Solicitação nº 61875.
RUI VICENTE OPPERMANN
Reitor
