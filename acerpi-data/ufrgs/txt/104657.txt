Documento gerado sob autenticação Nº BQU.554.402.Q33, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1609                  de  18/02/2020
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder jornada de trabalho reduzida, com remuneração proporcional, nos termos dos artigos 5º
ao  7º  da  Medida  Provisória  nº  2.174-28,  de  24  de  agosto  de  2001  e  conforme  o  Processo  nº
23078.535470/2019-18,  à  servidora LEONÉIA HOLLERWEGER,  matrícula SIAPE n° 1058263,  ocupante do
cargo de Pedagogo-área - 701058, lotada na Escola de Administração e com exercício no Setor Acadêmico da
Gerência Administrativa da Escola de Administração, alterando a jornada de trabalho de oito horas diárias e
quarenta horas semanais para seis horas diárias e trinta horas semanais, pelo período de um ano a contar
da publicação da Portaria.  
RUI VICENTE OPPERMANN
Reitor.
