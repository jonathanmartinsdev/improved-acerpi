Documento gerado sob autenticação Nº OLK.755.568.TM8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4366                  de  21/05/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  ODIRLEI  ANDRÉ  MONTICIELO,  Professor  do  Magistério
Superior,  lotado no Departamento de Medicina  Interna da Faculdade de Medicina  e  com exercício  na
Comissão de Extensão em Medicina, com a finalidade de participar do "EULAR - European Congress of
Rheumatology  2019",  em Madrid,  Espanha,  no  período  compreendido  entre  10/06/2019  e  16/06/2019,
incluído trânsito, com ônus limitado. Solicitação nº 84128.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
