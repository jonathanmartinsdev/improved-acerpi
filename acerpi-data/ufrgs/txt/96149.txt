Documento gerado sob autenticação Nº HUM.965.812.9F5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7240                  de  09/08/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 40, parágrafo 1º, inciso III, alínea B, da Constituição
Federal, com a redação dada pelo artigo 1º da Emenda Constitucional n.º 41, de 19 de dezembro de 2003, a
LUIZ CARLOS GOMES,  matrícula SIAPE nº 1705785,  no cargo de Assistente em Administração,  nível  de
classificação D, nível de capacitação IV, padrão 07, do Quadro desta Universidade, no regime de quarenta
horas semanais de trabalho, com exercício na Secretaria da Comissão Permanente de Pessoal Docente, com
proventos proporcionais e calculados de acordo com o artigo 1º da Lei nº 10.887, de 18 de junho de 2004.
Processo 23078.518605/2019-72.
RUI VICENTE OPPERMANN
Reitor.
