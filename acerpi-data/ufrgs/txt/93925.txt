Documento gerado sob autenticação Nº WVU.979.116.9F5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5775                  de  10/07/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar a Portaria n° 120/2019, de 04/01/2019, publicada no Diário Oficial da União - DOU de 07 de
janeiro de 2019,  Seção 2,  Página 31,  que concedeu autorização para afastamento do País a LEANDRO
RAIZER,  Professor  do  Magistério  Superior,  com  exercício  no  Departamento  de  Ensino  e  Currículo  da
Faculdade de Educação
Onde se lê: no período compreendido entre 10/01/2019 e 09/01/2020,
leia-se: no período compreendido entre 10/01/2019 e 04/07/2019, ficando ratificados os demais
termos. Processo nº 23078.524320/2018-90.
RUI VICENTE OPPERMANN
Reitor.
