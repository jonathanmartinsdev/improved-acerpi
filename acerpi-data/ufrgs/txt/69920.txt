Documento gerado sob autenticação Nº WID.664.429.BUH, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7421                  de  19/09/2018
A DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora DANIELA BORGES PAVANI, matrícula SIAPE n° 1574520, lotada no Departamento de Astronomia
do Instituto de Física e com exercício no Planetário da UFRGS, da classe C  de Professor Adjunto, nível 03,
para a classe C  de Professor Adjunto, nível 04, referente ao interstício de 23/05/2015 a 22/05/2017, com
vigência financeira a partir de 13/08/2018, de acordo com o que dispõe a Lei 12.772 de 28 de dezembro de
2012, com suas alterações e a Decisão nº 331/2017 do CONSUN. Processo nº 23078.520899/2018-11.
LIANE LUDWIG LODER
Decano do Conselho Universitário, no exercício da Reitoria.
