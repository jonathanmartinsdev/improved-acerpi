Documento gerado sob autenticação Nº DXL.526.171.V9R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3697                  de  21/05/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias,
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Adjunto A, do Quadro de
Pessoal  desta Universidade,  ANDRÉ GUSTAVO CABRERA DALTO,  matrícula SIAPE n° 2357538,  lotado no
Departamento  de  Medicina  Animal  da  Faculdade  de  Veterinária,  como  Chefe  Substituto  do  Depto  de
Medicina Animal da Faculdade de Veterinária, para substituir automaticamente o titular desta função em
seus  afastamentos  ou  impedimentos  regulamentares  no  período  de  13/06/2018  até  12/06/2020,  sem
prejuízo  e  cumulativamente  com  a  função  de  Coordenador  Substituto  da  Comissão  de  Extensão  da
Faculdade de Veterinária até 11/07/2019. Processo nº 23078.511158/2018-40.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
