Documento gerado sob autenticação Nº WZO.155.983.C21, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8580                  de  24/10/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de NEUSA CHAVES BATISTA, Professor do Magistério Superior,
lotada e em exercício no Departamento de Estudos Básicos da Faculdade de Educação, com a finalidade de
desenvolver estudos em nível de Pós-Doutorado, junto à Universidade de Granada - Espanha, no período
compreendido entre 01/03/2017 e 30/09/2017, com ônus limitado. Processo nº 23078.506509/2016-39.
RUI VICENTE OPPERMANN
Reitor
