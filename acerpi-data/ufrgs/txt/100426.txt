Documento gerado sob autenticação Nº EBK.242.803.C19, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9954                  de  04/11/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de MARCELLO CASACCIA BERTOLUCI,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a
finalidade  de  participar  da  "Scientific  Frontiers  in  Cardiometabolic  Disease"  e  da  "American  Heart
Association Scientific Sessions 2019", em Boston e Philadelphia, Estados Unidos, no período compreendido
entre 12/11/2019 e 20/11/2019, incluído trânsito, com ônus limitado. Solicitação nº 88445.
RUI VICENTE OPPERMANN
Reitor
