Documento gerado sob autenticação Nº WHS.176.568.NJT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9350                  de  19/11/2018
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
Designar os servidores,  THAÍS SCHMIDT FERNANDES,  ocupante do cargo de Engenheiro-área,
lotada na Superintendência de Infraestrutura e com exercício no Setor de Projetos de Edificações, EDINEI
RUDIMAR RODRIGUES VARGAS,  ocupante do cargo de Engenheiro-área, lotado na Superintendência de
Infraestrutura e com exercício no Setor de Fiscalização e Adequação, CAMILA SIMONETTI, ocupante do cargo
de Engenheiro-área, lotada na Superintendência de Infraestrutura e com exercício na Prefeitura Campus
Centro, para sob a presidência da primeira, constituírem a Comissão para Avaliação dos Inconformidades
Construtivas  e  Patologias  Apresentadas  após  a  Restauração do Prédio  do  Antigo  Instituto  de  Química
Industrial - Prédio 12.109, no Campus Centro da UFRGS em Porto Alegre - RS, com prazo de 45 dias. Processo
nº 23078.019328/2014-13.
O presidente da comissão deverá entrar em contato com os demais membros para o agendamento
da vistoria técnica de forma a atender os prazos de tramitação do processo.
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
