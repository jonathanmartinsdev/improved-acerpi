Documento gerado sob autenticação Nº OOR.349.747.RN5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8442                  de  17/09/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Conceder aposentadoria, nos termos do artigo 40, parágrafo 1º, inciso III, alínea B, da Constituição
Federal, com a redação dada pelo artigo 1º da Emenda Constitucional n.º 41, de 19 de dezembro de 2003, a
CESAR AUGUSTO TEJERA DE RE, matrícula SIAPE nº 0357304, no cargo de Professor Adjunto, nível 1, da
Carreira do Magistério Superior, do Quadro desta Universidade, no regime de dedicação exclusiva, com
exercício  no  Departamento  de  Ciências  Administrativas  da  Escola  de  Administração,  com  proventos
proporcionais e calculados de acordo com o artigo 1º da Lei nº 10.887, de 18 de junho de 2004. Processo
23078.521407/2019-96.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
