Documento gerado sob autenticação Nº GHY.744.472.2T4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2288                  de  14/03/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar RICHARD RICACHENEVSKY GURSKI, matrícula SIAPE n° 2267937, ocupante do cargo de
Professor do Magistério Superior,  classe Adjunto, lotado no Departamento de Cirurgia da Faculdade de
Medicina, do Quadro de Pessoal da Universidade Federal do Rio Grande do Sul, como Chefe do Depto de
Cirurgia da Faculdade de Medicina, Código SRH 86, código FG-1, com vigência a partir da data de publicação
deste ato no Diário Oficial da União e até 01/12/2017, a fim de completar o mandato de Brasil Silva Neto,
conforme artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.002573/2017-26.
JANE FRAGA TUTIKIAN
Vice-Reitora.
