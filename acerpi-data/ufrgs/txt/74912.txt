Documento gerado sob autenticação Nº UDP.411.402.G20, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             341                  de  10/01/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°45930,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ADMINISTRADOR, do Quadro de
Pessoal  desta  Universidade,  MARCELO DO VALE NUNES (Siape:  1734592 ),   para  substituir    CARLOS
AUGUSTO DOS SANTOS CASTILHOS (Siape: 0358176 ), Diretor da Divisão de Transportes do Departamento de
Logística e Documentos, Código FG-4, em seu afastamento por motivo de férias, no período de 14/01/2019 a
02/02/2019, com o decorrente pagamento das vantagens por 20 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
