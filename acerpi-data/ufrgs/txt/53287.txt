Documento gerado sob autenticação Nº IWU.121.213.BBF, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             2825                  de  17/04/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de CLAITON HENRIQUE DOTTO BAU,  Professor do Magistério
Superior, lotado e em exercício no Departamento de Genética do Instituto de Biociências, com a finalidade de
participar  do  "IMpACT  Meeting",  em  Örebro,  Suécia,  no  período  compreendido  entre  30/04/2018  e
05/05/2018, incluído trânsito, com ônus limitado. Solicitação nº 45467.
RUI VICENTE OPPERMANN
Reitor
