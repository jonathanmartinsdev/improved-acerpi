Documento gerado sob autenticação Nº VZX.568.978.U2J, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1693                  de  04/03/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Autorizar  o  afastamento  do  país  de  EVERTON DA SILVEIRA FARIAS,  Professor  do  Magistério
Superior, lotado e em exercício no Departamento Interdisciplinar do Campus Litoral Norte, com a finalidade
de participar do "Joint Workshop on Location and Network Design - Transportation and Logistics", em Santa
Cruz, Chile, no período compreendido entre 16/03/2016 e 18/03/2016, incluído trânsito, com ônus limitado.
Solicitação nº 17940.
CARLOS ALEXANDRE NETTO
Reitor
