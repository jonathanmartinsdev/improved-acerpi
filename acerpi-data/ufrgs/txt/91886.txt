Documento gerado sob autenticação Nº WQK.309.886.2IU, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4557                  de  24/05/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°49890,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de ANALISTA DE TECNOLOGIA DA
INFORMAÇÃO, do Quadro de Pessoal desta Universidade, JOAO LEILES TORMA KOPPS (Siape: 1732740 ),
 para substituir   GUSTAVO DA SILVA DUARTE (Siape: 1852136 ), Chefe da Divisão de Serviços de Redes do
Departamento de Infraestrutura de TI do CPD, Código FG-5, em seu afastamento por motivo de férias, no
período de 27/05/2019 a 12/06/2019, com o decorrente pagamento das vantagens por 17 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
