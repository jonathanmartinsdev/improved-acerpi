Documento gerado sob autenticação Nº CSU.591.055.2QT, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6022                  de  16/07/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  ROBERTO  PETRY  HOMRICH,  matrícula  SIAPE  n°  0357390,  lotado  no
Departamento de Engenharia Elétrica da Escola de Engenharia, para exercer a função de Chefe do Depto de
Engenharia Elétrica da Escola de Engenharia, Código SRH 246, código FG-1, com vigência a partir da data de
publicação no Diário Oficial da União, pelo período de 2 anos. Processo nº 23078.512239/2019-48.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
