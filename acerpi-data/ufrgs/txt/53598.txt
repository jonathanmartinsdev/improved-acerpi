Documento gerado sob autenticação Nº MXF.110.935.J2R, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3002                  de  25/04/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
ALTERAR o percentual de Incentivo à Qualificação concedido à servidora AMANDA LARA TAVARES,
ocupante do cargo de Auxiliar em Administração-701405, lotada no Instituto de Filosofia e Ciências Humanas,
SIAPE 2058769, para 25% (vinte e cinco por cento), a contar de 06/02/2018, tendo em vista a conclusão do
Curso  Superior  em  Tecnologia  em  Gestão  de  Recursos  Humanos,  conforme  o  Processo  nº
23078.502122/2018-75.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
