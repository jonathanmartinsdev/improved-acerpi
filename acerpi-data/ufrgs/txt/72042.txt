Documento gerado sob autenticação Nº JUN.659.593.KEL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9125                  de  12/11/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Tornar  insubsistente  a  Portaria  nº  8568/2018,  de  24/10/2018,  que  concede  progressão  por
capacitação a GABRIELA SCHUMACHER KRALIK, ocupante do cargo de Pedagogo-área, lotada no Colégio de
Aplicação  e  com  exercício  no  Núcleo  de  Orientação  e  Psicologia  Educacional.  Processo  nº
23078.524018/2017-51.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
