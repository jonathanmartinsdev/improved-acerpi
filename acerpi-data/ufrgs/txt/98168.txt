Documento gerado sob autenticação Nº RGI.648.100.JF5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8395                  de  16/09/2019
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Afastamento n°85407,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR,  do  Quadro  de  Pessoal  desta  Universidade,  VALDIR  JOSE  MORIGI  (Siape:  0337294  ),   para
substituir   JACKSON DA SILVA MEDEIROS (Siape: 2891750 ),  Coordenador da Comissão de Pesquisa da
FABICO,  em seu afastamento no país, no período de 16/09/2019 a 20/09/2019.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
