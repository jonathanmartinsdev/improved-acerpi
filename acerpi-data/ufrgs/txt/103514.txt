Documento gerado sob autenticação Nº KTP.063.907.FOE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             361                  de  10/01/2020
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Tornar sem efeito, a partir da presente data, a Portaria nº 10403/2019, de 19/11/2019, publicada no
Diário Oficial da União de 20/11/2019, que retifica o enquadramento no PCCTAE, por decisão judicial, da
servidora MARILIA TERESINHA DE OLIVEIRA BELMONTE, matrícula SIAPE n° 0356281, aposentada no cargo
de Técnico de Laboratório Área - 701244, em cumprimento ao Parecer de Força Executória nº 127/2019/PRF4,
conforme Processo SEI nº 23078.531239/2019-47.
JANE FRAGA TUTIKIAN
VICE-REITORA, NO EXERCÍCIO DA REITORIA
