Documento gerado sob autenticação Nº UFD.676.065.6FE, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6357                  de  16/08/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de FABIANO SEVERO RODEMBUSCH,  Professor do Magistério
Superior,  lotado  no  Departamento  de  Química  Orgânica  do  Instituto  de  Química  e  com  exercício  no
Programa de Pós-Graduação em Química, com a finalidade de participar da "3rd International Caparica
Conference on Chromogenic and Emissive Materials", em Lisboa, Portugal, no período compreendido entre
01/09/2018 e 08/09/2018, incluído trânsito, com ônus CNPq. Solicitação nº 58416.
RUI VICENTE OPPERMANN
Reitor
