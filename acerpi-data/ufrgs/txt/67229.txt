Documento gerado sob autenticação Nº JHS.198.446.D9I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5725                  de  01/08/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar a ocupante do cargo de Professor do Magistério Superior, classe Adjunto, do Quadro de
Pessoal desta Universidade, PAULA ROSSINI AUGUSTI, matrícula SIAPE n° 1584361, lotada no Departamento
de  Ciências  dos  Alimentos  do  Instituto  de  Ciências  e  Tecnologia  de  Alimentos,  como  Coordenadora
Substituta da Comissão de Extensão do ICTA, para substituir automaticamente o titular desta função em seus
afastamentos ou impedimentos regulamentares no período de 17/08/2018 até 16/08/2020, sem prejuízo e
cumulativamente com a função de Chefe  Substituta  do Depto de Ciências  dos  Alimentos  do ICTA até
08/01/2020. Processo nº 23078.518203/2018-97.
JANE FRAGA TUTIKIAN
Vice-Reitora.
