Documento gerado sob autenticação Nº CXL.898.647.O3T, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             1966                  de  14/03/2016
O VICE-REITOR, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE:
Dispensar, a partir da publicação no Diário Oficial da União, o(a) ocupante do cargo de Assistente
em Administração - 701200 , do Nível de Classificação DIII, do Quadro de Pessoal desta Universidade, ZULCA
MAR CORREA MANDIAN, CPF nº 23896400010, matrícula SIAPE 0355929 da função de Chefe da Seção de
Avaliação de Compras, vinc. ao Setor de Suprimentos e Logística da SUINFRA, Código SRH 870, Código FG-4,
para a qual foi designado(a) pela Portaria nº 326, de 18/01/2012, publicada no Diário Oficial da União de
23/01/2012. Processo nº 23078.004830/2016-83.
RUI VICENTE OPPERMANN
Vice-Reitor, no Exercício da Reitoria
