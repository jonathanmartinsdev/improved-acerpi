Documento gerado sob autenticação Nº XPB.268.523.TCN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4904                  de  07/06/2019
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por  decisão  judicial  proferida  no processo  nº  5052913-32.2018.4.04.7100,  da  5ª  Vara
Federal  de Porto Alegre,  o enquadramento no PCCTAE,  homologado pelo anexo I  da Portaria 1874 de
13/07/2006, do servidor MAURO ALMEIDA DIAS DE CASTRO, matrícula SIAPE n° 0354320, aposentado no
cargo de Analista de Tecnologia da Informação - 701062, do nível I para o nível IV, a contar de 01/01/2006,
conforme o Processo nº 23078.514286/2019-26.
Tornar sem efeito a(s) portaria(s) de concessão de progressão por capacitação gerada(s) após a
implementação do PCCTAE.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
