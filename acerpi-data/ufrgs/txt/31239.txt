Documento gerado sob autenticação Nº EMN.059.562.4VD, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9836                  de  13/12/2016
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país  de THEMIS ZELMANOVITZ,  Professor do Magistério Superior,
lotada e em exercício no Departamento de Medicina Interna da Faculdade de Medicina, com a finalidade de
realizar  visita  ao Jean Mayer USDA Human Nutrition Research Center on Aging, em Boston, Estados Unidos,
no período compreendido entre 02/01/2017 e 13/01/2017, incluído trânsito, com ônus limitado. Solicitação nº
25534.
RUI VICENTE OPPERMANN
Reitor
