Documento gerado sob autenticação Nº TLU.814.034.6DT, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8080                  de  29/08/2017
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  25/08/2017,   referente  ao  interstício  de
25/02/2016  a  24/08/2017,  para  a  servidora  ROCHELE  BOSCAINI  ZANDAVALLI,  ocupante  do  cargo  de
Fotografo - 701431, matrícula SIAPE 2285083,  lotada  na  Secretaria de Comunicação Social, passando do
Nível de Classificação/Nível de Capacitação C I, para o Nível de Classificação/Nível de Capacitação C II, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.012969/2017-81:
UFRJ - Segurança da Informação CH: 90 Carga horária utilizada: 60 hora(s) / Carga horária excedente: 30
hora(s) (06/03/2017 a 30/04/2017)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
