Documento gerado sob autenticação Nº GFL.182.312.NOA, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4265                  de  11/06/2018
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de
setembro de 2016
RESOLVE
Retificar a Portaria n° 4179/2018, de 08/06/2018, que concedeu Retribuição por Titulação- RT, por
obtenção do título de Doutorado, no Quadro desta Universidade, ao Professor FERNANDO DUTRA MICHEL,
com  exercício  no  Departamento  de  Engenharia  de  Produção  e  Transportes  da  Escola  de  Engenharia.
Processo nº 23078.513396/2018-90.
 
 
Onde se lê:
  ...com vigência financeira a partir de 07 de junho de 2018...,
leia-se:
 ... com vigência financeira a partir da publicação da Portaria..., ficando ratificados os demais
termos.
JANE FRAGA TUTIKIAN
Vice-Reitora, no exercício da Reitoria.
