Documento gerado sob autenticação Nº ZMQ.890.431.8DG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5855                  de  06/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder  o  Adicional  de  Insalubridade,  no  percentual  de  10%,  a  partir  de  03/03/2016,
correspondente ao grau Insalubridade Média, ao servidor ALEXANDRE SILVA DE QUEVEDO, Identificação
Única 21968322, Professor do Magistério Superior, com exercício no Departamento de Cirurgia e Ortopedia
da Faculdade de Odontologia,  observando-se o disposto na Lei  nº 8.112,  de 11 de dezembro de 1990,
combinado com a Lei 8.270, de 17 de dezembro de 1991, por exercer atividades em áreas consideradas
Insalubres conforme Laudo Pericial constante no Processo nº 23078.506010/2016-21, Código SRH n° 22860 e
Código SIAPE 2016002786.
Revogam-se quaisquer outros valores pagos ao servidor a título de insalubridade ou periculosidade
cujo fato gerador difira da presente concessão.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
