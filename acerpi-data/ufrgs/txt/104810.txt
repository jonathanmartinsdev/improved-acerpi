Documento gerado sob autenticação Nº PWP.037.562.T57, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1312                  de  06/02/2020
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  06/02/2020,   referente  ao  interstício  de
06/08/2018 a 05/02/2020, para a servidora LIDIANE ALVES MENDES, ocupante do cargo de Assistente em
Administração - 701200, matrícula SIAPE 3063104,  lotada  no  Instituto de Biociências, passando do Nível de
Classificação/Nível de Capacitação D I, para o Nível de Classificação/Nível de Capacitação D II, em virtude de
ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.500465/2020-10:
Formação Integral de Servidores da UFRGS III  CH: 94 Carga horária utilizada: 70 hora(s) / Carga horária
excedente: 24 hora(s) (08/08/2018 a 19/11/2019)
ENAP - Curso Gestão Estratégica de Pessoas e Planos de Carreira CH: 20 (17/04/2019 a 15/05/2019)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
