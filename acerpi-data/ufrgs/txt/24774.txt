Documento gerado sob autenticação Nº NNJ.775.822.N02, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5691                  de  28/07/2016
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO EM EXERCÍCIO DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7150, de 05 de
dezembro de 2012  , tendo em vista o que consta do Processo Administrativo n° 23078.025975/2015-37, do
Contrato nº 101/PROPLAN/NUDECON/2015, da Lei 10.520/02 e ainda da Lei 8.666/93,
RESOLVE:
 
 
Aplicar a sanção administrativa de ADVERTÊNCIA, prevista no item 'a' da Cláusula
Décima segunda do referido Contrato,  à  Empresa LIDERANÇA LIMPEZA E CONSERVAÇÃO LTDA,  CNPJ
00.482.840/0001-38,  pela falta de reposição do posto de motorista com curso de transporte de cargas
perigosas, conforme atestado pela GERTE às fls. 1/2 e reconhecido pela empresa à fl. 12, bem como pelo
NUDECON à fl. 13 do processo administrativo supracitado.
 
 Registre-se no SICAF, nos termos do art. 38 da IN nº 02/2010, da SLTI/MPOG. 
 
LUIS ROBERTO DA SILVA MACEDO
Pró-Reitor de Planejamento e Administração em exercício
