Documento gerado sob autenticação Nº QBP.346.070.UHK, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8489                  de  23/10/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias , considerando o disposto na
Portaria nº 7620, de 29 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a  partir  de  01/11/2018,  a  ocupante  do  cargo  de  Técnico  em  Assuntos
Educacionais - 701079, do Nível de Classificação EIV, do Quadro de Pessoal desta Universidade, CLAUDETE
LAMPERT GRUGINSKIE, matrícula SIAPE 1756939 da função de Coordenadora do Núcleo de Regulação da
Secretaria de Avaliação Institucional, Código SRH 1481, Código FG-2, para a qual foi designada pela Portaria
nº  4683/2015  de  15/06/2015,  publicada  no  Diário  Oficial  da  União  de  22/06/2015.  Processo  nº
23078.528162/2018-47.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria.
