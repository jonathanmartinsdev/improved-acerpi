Documento gerado sob autenticação Nº MQO.837.024.QL5, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5995                  de  16/07/2019
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
e tendo em vista o que consta do Processo Administrativo n° 23078.006357/13-64, do Contrato de Concessão
de Uso nº 009/PROPLAN/NUDECON/2013 e ainda, da Lei 8.666/93,
 
 
             RESOLVE:
 
 
      Tornar  sem  efe i to  a  Portar ia  4058  de  26/05/2015 ,  cons iderando  o  Despacho  nº
140/2016/INSCRI/SERCOB/PRF4/PGF/AGU (f l .  430) do processo administrativo supracitado.
 
 
 
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
