Documento gerado sob autenticação Nº SVN.097.507.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8947                  de  03/10/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Retificar a Portaria n° 5030/2019, de 12/06/2019, que concedeu autorização para afastamento do
País a SIDINEI ROCHA DE OLIVEIRA, Professor do Magistério Superior, com exercício no Programa de Pós-
Graduação em Administração da Escola de Administração
Onde se lê: no período compreendido entre 26/06/2019 e 30/06/2019, com ônus limitado e do "35th
EGOS Colloquium 2019", em Edimburgo, Escócia, no período compreendido entre 01/07/2019 e 06/07/2019,
com ônus UFRGS (Pró-Reitoria de Pesquisa: diárias),
leia-se: no período compreendido entre 26/06/2019 e 30/06/2019, com CAPES/PROAP e do "35th
EGOS Colloquium 2019", em Edimburgo, Escócia, no período compreendido entre 01/07/2019 e 06/07/2019,
com ônus UFRGS (Pró-Reitoria de Pesquisa: diárias), ficando ratificados os demais termos. Solicitação nº
87681.
RUI VICENTE OPPERMANN
Reitor.
