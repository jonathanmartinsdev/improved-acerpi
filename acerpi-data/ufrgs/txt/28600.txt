Documento gerado sob autenticação Nº ZWX.785.364.5C2, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7884                  de  04/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor GABRIEL DE AVILA OTHERO, matrícula SIAPE n° 1812294, lotado e em exercício no Departamento
de Linguística, Filologia e Teoria Literária, da classe C  de Professor Adjunto, nível 03, para a classe C  de
Professor Adjunto, nível 04, referente ao interstício de 25/08/2014 a 24/08/2016, com vigência financeira a
partir de 25/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações e  a  Decisão nº  197/2006-CONSUN,  alterada pela  Decisão nº  401/2013-CONSUN. Processo nº
23078.506927/2016-26.
JANE FRAGA TUTIKIAN
Vice-Reitora
