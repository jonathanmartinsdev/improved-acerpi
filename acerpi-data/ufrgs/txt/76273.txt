Documento gerado sob autenticação Nº QOF.745.835.VG8, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1255                  de  04/02/2019
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 7684, de 03 de outubro de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  29/01/2019,   referente  ao  interstício  de
25/07/2017 a 28/01/2019, para o servidor CONSTANTINOS GUIMARAES GIANNOULAKIS, ocupante do cargo
de Operador de Estação de Tratamento Água-esgoto - 701449,  matrícula SIAPE 2409342,  lotado  na 
Escola de Educação Física, Fisioterapia e Dança, passando do Nível de Classificação/Nível de Capacitação C I,
para o Nível de Classificação/Nível de Capacitação C II, em virtude de ter realizado o(s) seguinte(s) curso(s),
conforme o Processo nº 23078.502096/2019-66:
Formação Integral de Servidores da UFRGS III  CH: 62 Carga horária utilizada: 60 hora(s) / Carga horária
excedente: 2 hora(s) (30/08/2017 a 16/01/2019)
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
