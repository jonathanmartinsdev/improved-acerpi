Documento gerado sob autenticação Nº GVS.962.600.9F5, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7236                  de  09/08/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de CRISTIANO POLETO, Professor do Magistério Superior, lotado e
em exercício no Departamento de Hidromecânica e Hidrologia do Instituto de Pesquisas Hidráulicas, com a
finalidade de realizar visita à Universidade de Lisboa, Universidade do Algarve e Universidade de Coimbra,
em Lisboa, Faro e Coimbra, Portugal, no período compreendido entre 04/09/2019 e 14/09/2019, incluído
trânsito, com ônus CAPES/PRINT/UFRGS. Solicitação nº 85677.
RUI VICENTE OPPERMANN
Reitor
