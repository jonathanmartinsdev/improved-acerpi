Documento gerado sob autenticação Nº HQW.911.695.68T, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5981                  de  10/08/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Conceder 90 (noventa) dias de licença para capacitação, nos termos do artigo 87 da Lei nº 8.112, de
11 de dezembro de 1990, regulamentado pelo Decreto 5.707, de 23 de fevereiro de 2006, para o servidor
TIAGO D OLIVEIRA SILVA, com exercício na Divisão de Administração de Dados do Centro de Processamento
de Dados, a ser usufruída no período de 22/08/2016 a 19/11/2016, referente ao quinquênio de 11/08/2008 a
10/08/2013, a fim de realizar o trabalho de conclusão do curso de Graduação em Psicologia, conforme
Processo nº 23078.014738/2016-21.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
