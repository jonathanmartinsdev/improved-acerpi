Documento gerado sob autenticação Nº KLL.783.145.M5B, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             3073                  de  26/04/2016
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 44, de 07 de janeiro de 2013
RESOLVE
Lotar na Secretaria de Educação à Distância, com exercício no Coordenação Acadêmica da SEAD,
Silvia de Oliveira Kist, nomeada conforme Portaria Nº 1.388, de 25 de fevereiro de 2016, publicada no Diário
Oficial da União no dia 29 de fevereiro de 2016, em efetivo exercício desde 12 de abril de 2016, ocupante do
cargo de Pedagogo/área: Orientação Educacional, Ambiente Organizacional Ciências Humanas, Jurídicas e
Econômicas, classe E, nível I, padrão I, no Quadro de Pessoal desta Universidade.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
