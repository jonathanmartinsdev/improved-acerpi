Documento gerado sob autenticação Nº GKX.411.385.MH4, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             4823                  de  06/07/2018
  Prorroga  Pró-tempore  o  mandato  do
Coordenador  da  Comissão  Interna  de
Supervisão-CIS do Plano de Carreira dos
Cargos  Técnico-Administrativos  em
Educação.
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE:
Prorrogar Pró-tempore o mandato do Coordenador da Comissão Interna de Supervisão-CIS do
Plano de Carreira  dos  Cargos  Técnico-Administrativos  em Educação,  SILVIO ROBERTO RAMOS CORREA,
designado pela Portaria nº 2687, de 09 de abril de 2015.
RUI VICENTE OPPERMANN,
Reitor.
