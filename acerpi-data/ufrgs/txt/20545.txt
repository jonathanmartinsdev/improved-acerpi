Documento gerado sob autenticação Nº ZWI.319.053.FCN, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2692                  de  12/04/2016
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições, considerando o disposto na Portaria nº 5469, de 04 de outubro de 2012
RESOLVE
Lotar no Instituto de Informática, com exercício no Instituto de Informática, Carlos Yuri da Silva
Negreiros, nomeado conforme Portaria Nº 1387 de 28 de fevereiro de 2016, publicada no Diário Oficial da
União no dia 29 de fevereiro de 2016, em efetivo exercício desde 30 de março de 2016, ocupante do cargo de
Assistente em Administração, Ambiente Organizacional Administrativo, classe D, nível I, padrão I, no Quadro
de Pessoal desta Universidade.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
