Documento gerado sob autenticação Nº OQV.396.895.C9V, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             568                  de  19/01/2017
O SUPERINTENDENTE DE INFRAESTRUTURA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 9764, de 08 de dezembro de 2016
RESOLVE
 
Retificar a Portaria nº 10075 de 21/12/2016, tendo em vista a existência de erro material,
nos seguintes termos:
Onde se lê: "ALINE SANTOS RODRIGUES [...]";
Leia-se: "ALINE RODRIGUES SANTOS [...]".
 
EDY ISAIAS JUNIOR
Superintendente de Infraestrutura
