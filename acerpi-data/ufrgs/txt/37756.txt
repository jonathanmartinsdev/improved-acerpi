Documento gerado sob autenticação Nº GPF.877.501.URP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             3862                  de  04/05/2017
O PRÓ-REITOR DE PLANEJAMENTO E ADMINISTRAÇÃO DA UNIVERSIDADE FEDERAL DO RIO GRANDE
DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 7611, de 29 de setembro de 2016
RESOLVE
Designar os servidores  MAGUINORES FERREIRA PEREIRA, ocupante do cargo de Assistente em
Administração,  lotada na Pró-Reitoria  de Planejamento e  Administração e  com exercício  na Núcleo de
Contratos e Normativas, EDUARDO GONZALEZ GARCIA, ocupante do cargo de Assistente em Administração,
lotado na Pró-Reitoria de Gestão de Pessoas e com exercício na Divisão de Acompanhamento e Execução de
Serviços Terceirizados, MILTON ROGERIO APPEL, ocupante do cargo de Vigilante, lotado e em exercício na
Coordenadoria de Segurança da UFRGS, para comporem comissão com a finalidade de, no prazo de trinta
dias, apurar eventual responsabilidade da empresa contratada para prestação de serviços de vigilância no
caso de furto de material de limpeza, formulários e carimbos médicos ocorrido no Departamento de Atenção
à Saúde da PROGESP/UFRGS.
HELIO HENKIN
Pró-Reitor de Planejamento e Administração
