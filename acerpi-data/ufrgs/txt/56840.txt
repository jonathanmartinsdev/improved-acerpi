Documento gerado sob autenticação Nº KWG.332.953.DFL, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5447                  de  23/07/2018
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias,  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Associado, do Quadro de
Pessoal  desta  Universidade,  ALEX  SANDER  DA  ROSA  ARAUJO,  matrícula  SIAPE  n°  1695539,  lotado  no
Departamento de Fisiologia do Instituto de Ciências Básicas da Saúde, como Coordenador Substituto da
COMGRAD  do  Curso  de  Biomedicina  do  Instituto  de  Ciências  Básicas  da  Saúde,  para  substituir
automaticamente o titular desta função em seus afastamentos ou impedimentos regulamentares no período
de 05/08/2018 até 04/08/2020. Processo nº 23078.516084/2018-38.
JANE FRAGA TUTIKIAN
Vice-Reitora.
