Documento gerado sob autenticação Nº RZE.933.905.5IJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº 3362/2016,
que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1366                  de  07/02/2020
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°52905,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PROFESSOR DO MAGISTÉRIO
SUPERIOR, do Quadro de Pessoal desta Universidade, VALESCA DALL ALBA (Siape: 1457834 ),  para substituir  
VIVIAN CRISTINE LUFT (Siape: 1851851 ), Chefe do Depto de Nutrição da Faculdade de Medicina, Código FG-1,
em seu afastamento por motivo de férias,  no período de 10/02/2020 a 06/03/2020,  com o decorrente
pagamento das vantagens por 26 dias.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
