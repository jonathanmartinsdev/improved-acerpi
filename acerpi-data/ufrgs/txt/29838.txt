Documento gerado sob autenticação Nº QSM.179.184.4RG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8744                  de  26/10/2016
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Dispensar,  a  pedido,  a partir  de 01/08/2016,  a ocupante do cargo de Professor do Magistério
Superior,  classe  Adjunto,  do  Quadro  de  Pessoal  desta  Universidade,  MARCIA  RODRIGUES  NOTARE
MENEGHETTI, matrícula SIAPE n° 2348419, da função de Coordenadora do PPG em Ensino de Matemática,
Código SRH 1122, código FUC, para a qual foi designada pela Portaria 6166/2015, de 10/08/2015, publicada
no Diário Oficial da União do dia 12/08/2015. Processo nº 23078.203554/2016-34.
JANE FRAGA TUTIKIAN
Vice-Reitora
