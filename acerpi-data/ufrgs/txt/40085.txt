Documento gerado sob autenticação Nº VLI.484.152.TGG, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5585                  de  27/06/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, ao
Professor ODALCI JOSE PUSTAI, matrícula SIAPE n° 0357614, lotado e em exercício no Departamento de
Medicina Social da Faculdade de Medicina, da classe C  de Professor Adjunto, nível 03, para a classe C  de
Professor Adjunto, nível 04, referente ao interstício de 31/12/2011 a 10/02/2014, com vigência financeira a
partir de 01/08/2016, de acordo com o que dispõe a Lei nº 12.772, de 28 de dezembro de 2012, com suas
alterações  e  a  Resolução  nº  12/1995-COCEP,  alterada  pela  Decisão  nº  401/2013-CONSUN.  Processo  nº
23078.503305/2017-27.
JANE FRAGA TUTIKIAN
Vice-Reitora.
