Documento gerado sob autenticação Nº XZQ.817.432.T4J, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9149                  de  02/10/2017
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de TALES TIECHER, Professor do Magistério Superior, lotado e em
exercício no Departamento de Solos da Faculdade de Agronomia,  com a finalidade de realizar  visita  à
Université Abdelhamid Ibn Badis, em Mostaganem, Argélia, no período compreendido entre 20/10/2017 e
01/11/2017, incluído trânsito, com ônus limitado. Solicitação nº 31220.
RUI VICENTE OPPERMANN
Reitor
