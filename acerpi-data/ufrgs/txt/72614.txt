Documento gerado sob autenticação Nº HKF.846.831.0C0, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9527                  de  26/11/2018
O PRÓ-REITOR DE ASSUNTOS ESTUDANTIS EM EXERCÍCIO DA UNIVERSIDADE FEDERAL DO RIO
GRANDE DO SUL,  no uso de suas atribuições,  considerando o disposto na Portaria nº  7625,  de 29 de
setembro de 2016
RESOLVE
 
              Designar Suélen Ramón da Rosa, Nutricionista do Restaurante Universitário 4; Márcia Gabriela
D'Elia Bellos, Técnico em Nutrição e Dietética do Restaurante Universitário 3 e Fernanda Fernandes Alves,
Técnico  em  Nutrição  e  Dietética  do  Restaurante  Universitário  1,  para,  sob  a  presidência  da  primeira,
constituírem Comissão  para  realização  de  Inventário  nos  Estoques  dos  Restaurantes  Universitários  da
Divisão de Alimentação do Departamento de Infraestrutura desta Pró-Reitoria de Assuntos Estudantis.
ELTON LUIS BERNARDI CAMPANARO
Pró-Reitor de Assuntos Estudantis em exercício
