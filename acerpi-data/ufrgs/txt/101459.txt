Documento gerado sob autenticação Nº DGI.246.613.K0O, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10636                  de  27/11/2019
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do País de FERNANDO DORNELLES,  Professor do Magistério Superior,
lotado  e  em  exercício  no  Departamento  de  Hidromecânica  e  Hidrologia  do  Instituto  de  Pesquisas
Hidráulicas, com a finalidade de atuar como Professor Visitante junto à University of Texas at San Antonio,
em San Antonio,  Estados Unidos,  no período compreendido entre 01/12/2019 e 29/02/2020,  com ônus
CAPES/PRINT/UFRGS. Processo nº 23078.523629/2019-43.
RUI VICENTE OPPERMANN
Reitor.
