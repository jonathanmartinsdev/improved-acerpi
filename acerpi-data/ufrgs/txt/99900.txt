Documento gerado sob autenticação Nº YAS.457.436.001, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9611                  de  22/10/2019
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar  o  afastamento  do  País  de  ARTUR FRANCISCO SCHUMACHER SCHUH,  Professor  do
Magistério Superior, lotado e em exercício no Departamento de Farmacologia do Instituto de Ciências Básicas
da Saúde, com a finalidade de participar do "14th International Meeting of the Genetic Epidemiology of
Parkinson's  Disease  Consortium",  na  Cidade  do  Cabo,  África  Do  Sul,  no  período  compreendido  entre
29/10/2019 e 04/11/2019, incluído trânsito, com ônus limitado. Solicitação nº 87967.
RUI VICENTE OPPERMANN
Reitor
