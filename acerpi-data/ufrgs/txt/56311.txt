Documento gerado sob autenticação Nº NYN.181.296.VDP, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             5034                  de  12/07/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Dispensar,  a  pedido,  a partir  de 09/07/2018,  o ocupante do cargo de Professor do Magistério
Superior, classe Titular do Quadro de Pessoal desta Universidade, GUIOMAR PEDRO BERGMANN, matrícula
SIAPE n° 0014082, da função de Coordenador do PPG em Alimentos de Origem Animal-Mestrado Profissional,
Código SRH 1485, código FUC, para a qual foi designado pela Portaria 6002/2017, de 07/07/2017, publicada
no Diário Oficial da União do dia 11/07/2017. Processo nº 23078.517126/2018-58.
RUI VICENTE OPPERMANN
Reitor.
