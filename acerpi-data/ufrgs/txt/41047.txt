Documento gerado sob autenticação Nº HWF.078.238.UUG, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6259                  de  13/07/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder Progressão por Mérito Profissional, nos termos do artigo 10-A da Lei nº 11.091, de 12 de
janeiro  de  2005,  e  da  Decisão nº  939,  de  21  de  novembro de  2008,  do  Conselho Universitário  desta
Universidade,  para  a  servidora  STEFÂNIA  OLIVEIRA  DA  COSTA,  matrícula  SIAPE  1479439,  Nível  de
Classificação D , para o padrão de vencimento 09, com vigência a partir de 09 de junho de 2017.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
