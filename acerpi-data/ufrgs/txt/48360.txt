Documento gerado sob autenticação Nº WIT.170.195.A5I, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11387                  de  21/12/2017
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições
legais e estatutárias , considerando o disposto na Portaria nº 7678, de 30 de setembro de 2016
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior, classe Titular, do Quadro de
Pessoal desta Universidade, ROBERTO VERDUM, matrícula SIAPE n° 0357046, lotado no Departamento de
Geografia do Instituto de Geociências, para exercer a função de Coordenador do PPG em Geografia, Código
SRH 1126, código FUC, com vigência a partir da data de publicação no Diário Oficial da União, pelo período de
2 anos, por ter sido reeleito. Processo nº 23078.523985/2017-03.
JANE FRAGA TUTIKIAN
Vice-Reitora.
