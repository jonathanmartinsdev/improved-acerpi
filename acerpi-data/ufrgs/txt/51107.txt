Documento gerado sob autenticação Nº VYK.709.628.TH8, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1509                  de  20/02/2018
O REITOR  DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias
RESOLVE
Autorizar o afastamento do país de ANTONIO CARLOS SCHNEIDER BECK FILHO,  Professor do
Magistério  Superior,  lotado  e  em  exercício  no  Departamento  de  Informática  Aplicada  do  Instituto  de
Informática, com a finalidade de participar do evento "Design, Automation and Test in Europe", em  Dresden,
Alemanha, no período compreendido entre 18/03/2018 e 24/03/2018, incluído trânsito, com ônus UFRGS
(Pró-Reitoria de Pesquisa - diárias). Solicitação nº 34058.
RUI VICENTE OPPERMANN
Reitor
