Documento gerado sob autenticação Nº MDV.534.446.84J, disponível no endereço http://www.ufrgs.br/autenticacao
1/1
PORTARIA Nº             2440                  de  04/04/2016
O VICE-REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 5025, de 26 de setembro de 2012
RESOLVE:
Conceder progressão funcional, por avaliação de desempenho, no Quadro desta Universidade, à
Professora  MILENA DA ROSA SILVA,  matrícula  SIAPE n°  1767024,  com exercício  no Departamento de
Psicanálise e Psicopatologia do Instituto de Psicologia, da classe C  de Professor Adjunto, nível 03, para a
classe C  de Professor Adjunto, nível 04, referente ao interstício de 26/02/2014 a 25/02/2016, com vigência
financeira a partir de 31/03/2016, de acordo com o que dispõe a Resolução nº 12/1995-COCEP, alterada pela
Decisão nº 401/2013-CONSUN. Processo nº 23078.005907/2016-32.
RUI VICENTE OPPERMANN
Vice-Reitor
