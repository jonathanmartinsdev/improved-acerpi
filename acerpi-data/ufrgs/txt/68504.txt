Documento gerado sob autenticação Nº NYU.752.855.1LN, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             6450                  de  20/08/2018
A VICE-PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro de 2016
RESOLVE
Remover, a partir de 20 de agosto de 2018,  de acordo com o artigo 36, parágrafo único, inciso II da
Lei n° 8.112, de 11 de dezembro de 1990, com redação dada pela Lei nº 9.527, de 10 de dezembro de 1997,
ALYNNI LUIZA RICCO AVILA, ocupante do cargo de Assistente em Administração, Ambiente Organizacional
Administrativo,  Código 701200,  Classe D,  Nível  de Capacitação IV,  Padrão de Vencimento 09,  SIAPE nº.
1446236 do Instituto de Geociências para a lotação Secretaria de Educação a Distância, com novo exercício
na Secretaria de Educação a Distância.
VÂNIA CRISTINA SANTOS PEREIRA
Vice-Pró-Reitora de Gestão de Pessoas
