Documento gerado sob autenticação Nº CZR.602.791.LFR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/2
PORTARIA Nº             76                  de  03/01/2020
  Divulga os dias que não haverá expediente
na UFRGS, no ano de 2020.
A VICE-REITORA, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL,
no uso de suas atribuições legais e estatutárias
RESOLVE:
              Art. 1º Divulgar os dias em que não haverá expediente na Universidade, no ano de 2020, conforme a
Portaria n.º 9889 de 01 de novembro de 2019 - Calendário Escolar da UFRGS e Portaria Nº 679 de 30 de
dezembro de 2019, do Ministro de Estado da Economia, publicada no Diário Oficial da União, de 31 de
dezembro de 2019:
 
Janeiro 01 (quarta-feira) - Confraternização Universal
Fevereiro
24 (segunda-feira) - Carnaval
25 (terça-feira) - Carnaval
26 Quarta-feira de cinzas até as 14h
Abril
10 (sexta-feira) - Paixão de Cristo
11 (sábado) - Dia não letivo (exceto no Campus Litoral Norte)
12 (domingo) - Páscoa
21 (terça-feira) - Tiradentes       
Maio 01 (sexta-feira) - Dia Mundial do Trabalho16 (sábado) - Portas Abertas
Junho
11 (quinta-feira) - Corpus Christi
29 (segunda-feira) - São Pedro - Padroeiro de Tramandaí (Dia não letivo apenas no Campus Litoral
Norte)
Setembro 07 (segunda-feira) - Independência do Brasil
Outubro 12 (segunda-feira) - Nossa Senhora Aparecida28 (quarta-feira) - Dia do Servidor Público
Novembro 02 (segunda-feira) - Finados28 (sábado) - Vestibular 2021              
Dezembro
05 (sábado) - Vestibular 2021
24 (quinta-feira) - Véspera de Natal (Ponto facultativo após as 14h)
25 (sexta-feira) - Natal 
31 (quinta-feira) - Véspera de Ano Novo (Ponto Facultativo após as 14h)
 
              Art. 2º O Recesso do Final do Ano será definido posteriormente pelo Ministério da Economia.
 
 
Documento gerado sob autenticação Nº CZR.602.791.LFR, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
2/2
 
              Art. 3º Os dias de guarda dos credos e religiões não relacionados nesta Portaria, poderão ser
compensados na forma do inciso II do Art. 44 da Lei 8.112 de 11/12/90, desde que previamente autorizado
pela Chefia imediata.
 
               Art. 4º Caberá às Direções das Unidades e aos Órgãos da Administração Central a preservação e o
funcionamento dos serviços essenciais afetos às respectivas áreas de competência desta Universidade.
JANE FRAGA TUTIKIAN,
Vice-Reitora, no exercício da Reitoria.
