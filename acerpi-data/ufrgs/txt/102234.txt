Documento gerado sob autenticação Nº YCP.272.633.384, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             11163                  de  16/12/2019
A VICE-REITORA DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições,
considerando o disposto na Portaria nº 7624, de 29 de setembro de 2016
RESOLVE
Nomear, em caráter efetivo, FERNANDA DA SILVA MOMO, em virtude de habilitação em Concurso
Público de Provas e Títulos, conforme Edital Nº 73/2019 de 11 de Outubro de 2019, homologado em 14 de
outubro de 2019 e de acordo com os artigos 9º, item I e X da Lei nº 8.112, de 11 de dezembro de 1990 e Lei
12.772,  de 28 de dezembro de 2012,  com redação dada pela Lei  12.863,  de 24 de setembro de 2013,
publicada  no  Diário  Oficial  da  União  de  25  de  setembro  de  2013,  para  o  cargo  de  PROFESSOR  DO
MAGISTÉRIO SUPERIOR do Plano de Carreiras e Cargos do Magistério Federal, na Classe "A" de Professor
Adjunto A, Nível I,  do Quadro de Pessoal desta Universidade, em regime de trabalho de DE (Dedicação
Exclusiva), junto ao Departamento de Ciências Contábeis e Atuariais da Faculdade de Ciências Econômicas,
em vaga decorrente da aposentadoria da Professora Ana Maria Pellini, código nº 272101, ocorrida em 7 de
Janeiro de 2019, conforme Portaria nº. 119/2019 de 4 de janeiro de 2019, publicada no Diário Oficial da União
de 7 de janeiro de 2019. Processo nº. 23078.508386/2019-13.
JANE FRAGA TUTIKIAN
Vice-Reitora.
