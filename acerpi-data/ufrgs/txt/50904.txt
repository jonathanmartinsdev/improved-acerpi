Documento gerado sob autenticação Nº TCA.090.406.SLJ, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             1335                  de  15/02/2018
A PRÓ-REITORA DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL EM
EXERCÍCIO, no uso de suas atribuições que lhe foram conferidas pela Portaria nº. 8117, de 10 de outubro de
2016, e conforme a Solicitação de Férias n°38639,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, o ocupante do cargo de PROFESSOR DO MAGISTÉRIO DO
ENSINO BÁSICO, TÉCNICO E TECNOLÓGICO, do Quadro de Pessoal desta Universidade, HUBERT AHLERT
(Siape: 6352705 ),  para substituir   PEDRO MONTEIRO NUNES JUNIOR (Siape: 0354873 ), Chefe do Setor de
Capacitação da Gerência Administrativa do CPD, Código FG-5, em seu afastamento por motivo de férias, no
período de 17/02/2018 a 28/02/2018.
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora
