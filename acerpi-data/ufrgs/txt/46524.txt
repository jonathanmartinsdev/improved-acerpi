Documento gerado sob autenticação Nº IHF.137.693.F83, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10069                  de  31/10/2017
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar ROBERTA DA SILVA BUSSAMARA RODRIGUES, matrícula SIAPE n° 3582462, ocupante do
cargo de Professor do Magistério Superior, classe Adjunto A, lotada no Departamento de Química Inorgânica
do Instituto de Química, do Quadro de Pessoal da Universidade, para exercer a função de Coordenadora
Substituta da COMGRAD do Curso de Biotecnologia do Instituto de Biociências, com vigência a partir da data
deste ato até  18/04/2019, a fim de completar o mandato do Professor Mendeli Henning Vainstein, conforme
artigo 92 do Estatuto da mesma Universidade. Processo nº 23078.519290/2017-19.
RUI VICENTE OPPERMANN
Reitor.
