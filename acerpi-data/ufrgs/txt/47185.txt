Documento gerado sob autenticação Nº HKX.641.145.T6L, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             10483                  de  16/11/2017
A  PRÓ-REITORA  DE  GESTÃO  DE  PESSOAS  EM  EXERCÍCIO  DA  UNIVERSIDADE  FEDERAL  DO  RIO
GRANDE DO SUL, no uso de suas atribuições, considerando o disposto na Portaria nº 8117, de 10 de outubro
de 2016
RESOLVE
Conceder  Progressão  por  Capacitação,  a  contar  de  16/11/2017,   referente  ao  interstício  de
16/05/2016 a 15/11/2017, para a servidora NATÁLIA ISABEL MALÜE VIEIRA, ocupante do cargo de Técnico
de Laboratório Área - 701244, matrícula SIAPE 1736042,  lotada  no  Instituto de Geociências, passando do
Nível de Classificação/Nível de Capacitação D III, para o Nível de Classificação/Nível de Capacitação D IV, em
virtude de ter realizado o(s) seguinte(s) curso(s), conforme o Processo nº 23078.518405/2017-58:
Formação Integral de Servidores da UFRGS VI CH: 179 Carga horária utilizada: 150 hora(s) / Carga horária
excedente: 29 hora(s) (29/08/2015 a 27/10/2017)
VÂNIA CRISTINA SANTOS PEREIRA
Pró-Reitora de Gestão de Pessoas em exercício
