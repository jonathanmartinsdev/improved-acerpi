Documento gerado sob autenticação Nº WOG.829.946.96D, disponível no
endereço http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9345                  de  19/11/2018
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias  
RESOLVE
Designar o ocupante do cargo de Professor do Magistério Superior GILBERTO GAGG,  matrícula
SIAPE 1509917, para exercer, em caráter 'pró-tempore', a função de Coordenador Substituto da COMGRAD
de Engenharia Cartográfica do Instituto de Geociências, código SRH 1182, com vigência a partir da data deste
ato até 18/12/2018. Processo nº 23078.531705/2018-11.
RUI VICENTE OPPERMANN
Reitor.
