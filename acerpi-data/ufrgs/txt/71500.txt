Documento gerado sob autenticação Nº RNE.134.050.PLL, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             8710                  de  29/10/2018
O PRÓ-REITOR DE GESTÃO DE PESSOAS DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no
uso de suas atribuições que lhe foram conferidas pela Portaria nº.7684, de 03 de outubro de 2016, do
Magnífico Reitor, e conforme a Solicitação de Afastamento n°60659,
RESOLVE
Designar, temporariamente, nos termos da Lei nº. 8.112, de 11 de dezembro de 1990, com redação
dada pela Lei nº.9.527, de 10 de dezembro de 1997, a ocupante do cargo de PRODUTOR CULTURAL, do
Quadro de Pessoal desta Universidade, LIGIA ANTONELA DA SILVA PETRUCCI (Siape: 0357582 ),   para
substituir   CLAUDIA MARA ESCOVAR ALFARO BOETTCHER (Siape: 0351731 ), Diretor do Departamento de
Difusão Cultural  da PROREXT,  Código CD-4,  em seu afastamento no país,  no período de 30/10/2018 a
01/11/2018, com o decorrente pagamento das vantagens por 3 dias.
MAURÍCIO VIÉGAS DA SILVA
Pró-Reitor de Gestão de Pessoas
