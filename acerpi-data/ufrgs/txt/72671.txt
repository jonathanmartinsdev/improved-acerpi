Documento gerado sob autenticação Nº NXJ.473.071.PUV, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             9587                  de  27/11/2018
O DECANO DO CONSELHO UNIVERSITÁRIO, NO EXERCÍCIO DA REITORIA DA UNIVERSIDADE FEDERAL
DO RIO GRANDE DO SUL, no uso de suas atribuições legais e estatutárias
RESOLVE
Retificar,  por decisão judicial  proferida no processo nº 5054142-27.2018.4.04.7100,  da 10ª Vara
Federal de Porto Alegre, o enquadramento no PCCTAE, homologado pelo anexo I da Portaria n° 1874, de
13/07/2006, da servidora HELENARA ROBALLO UNGARETTI, matrícula SIAPE n° 0356681, aposentada no
cargo de Assistente em Administração - 701200, para o nível IV, conforme o Processo nº 23078.532084/2018-
85.
CELSO GIANNETTI LOUREIRO CHAVES
Decano do Conselho Universitário, no exercício da Reitoria
