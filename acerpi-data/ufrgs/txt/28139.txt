Documento gerado sob autenticação Nº XEG.102.365.L81, disponível no endereço
http://www.ufrgs.br/autenticacao
Documento certificado eletronicamente, conforme Portaria nº
3362/2016, que institui o Sistema de Documentos Eletrônicos da UFRGS.
1/1
PORTARIA Nº             7612                  de  29/09/2016
O REITOR DA UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL, no uso de suas atribuições legais e
estatutárias,  
RESOLVE
Reconduzir  o ocupante do cargo de Administrador,  do Quadro de Pessoal  desta Universidade,
MAURÍCIO VIÉGAS DA SILVA, matrícula SIAPE 0354315, ao cargo de Pró-Reitor de Gestão de Pessoas, Código
SRH 3, código CD-2, com vigência a partir da data de publicação no Diário Oficial da União. Processo nº
23078.022163/2016-11.
RUI VICENTE OPPERMANN
Reitor
