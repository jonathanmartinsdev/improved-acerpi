 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
      
 
 
 
 
 
 
 
02 de abril de 2018 
 
 
 
 
 
 
              
                
     Comissão Responsável pela edição e publicação: 
 
               
Rafaela Padilha 
Lysandra Ramos Tieppo 
Marc Emerim 
Marisol Becker Johann 
          
                                                                           
                                                                                  Portaria Nº 755, de 02 de maio de 2017. 
 
 
 
 
 
 
 
Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
               do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                               de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                    República Federativa do Brasil, Brasília, v. 112, nº 157, 
                                                                p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
Ministro da Educação José Mendonça Bezerra Filho 
 
Secretária de Educação Profissional e Tecnológica Eline Neves Braga Nascimento 
 
Reitor Substituto José Eli Santos dos Santos 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional José Eli Santos dos Santos 
Pró-reitora de Ensino Clarice Monteiro Escott 
Pró-reitora de Extensão Viviane Silva Ramos 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande 
do Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras 
providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 755, de 02 de maio de 2016. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no 
âmbito da reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
   
   
   
   
 
 
 
   
   
   
 
            
      
   
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 424, DE 02 DE ABRIL DE 2018 
 
 
O REITOR SUBSTITUTO DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pela Portaria nº 1128, de 30/06/2017, publicado no D.O.U. de 03/07/2017, tendo 
em vista o disposto nos arts. 143 e 148 da Lei nº 8.112, de 11 de dezembro de 1990, 
RESOLVE:  
 
Art. 1º Converter o julgamento em diligência, nos termos do Parecer Jurídico constante 
nos autos do Processo n° 23419.001116.2013-82; 
 
Art. 2° DESIGNAR os servidores João Helvio Righi de Oliveira, Professor de 3º 
Grau, Matrícula SIAPE nº 380972; Luiz Gustavo Teixeira de Souza, Assistente de 
Laboratório, Matrícula SIAPE nº 1932691; e Leandro Cervo, Auxiliar de Nutrição e Dietética, 
Matrícula SIAPE nº 1095040, todos do Quadro Único de Pessoal da Universidade Federal de 
Santa Maria, para, sob a presidência do primeiro, constituírem Comissão de Processo 
Administrativo Disciplinar, incumbida de concluir, no prazo de 60 (sessenta) dias, os trabalhos 
relativos ao Processo supramencionado. 
 
Art. 3º Esta portaria entra em vigor na data de sua publicação. 
 
 
 
José Eli Santos dos Santos 
Reitor Substituto do IFRS 
 
