 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
      
 
 
 
 
 
 
 
01 de outubro de 2018 
 
 
 
 
 
 
              
                
     Comissão Responsável pela edição e publicação: 
 
               
Marisol Becker Johann 
Marc Emerim 
Rafaela Padilha 
Willian Miranda Rustick 
          
                                                                           
                                                                              Portaria Nº 1208, de 01 de outubro de 2018. 
 
 
 
 
 
 
 
Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
               do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                               de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                    República Federativa do Brasil, Brasília, v. 112, nº 157, 
                                                                p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
Ministro da Educação Rossieli Soares da Silva 
 
Secretária de Educação Profissional e Tecnológica Romero Portella Raposo Filho 
 
Reitor Pro Tempore Júlio Xandro Heck 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional Amilton de Moura Figueiredo 
Pró-reitor de Ensino Lucas Coradini 
Pró-reitora de Extensão Marlova Benedetti 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande 
do Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 606, de 09 de maio de 2018. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no âmbito 
da reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
   
   
   
   
   
 
 
 
   
   
   
 
            
      
   
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
PORTARIAS DE 01 DE OUTUBRO DE 2018 
 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pela Portaria MEC nº 465, de 17/05/2018, publicada no D.O.U. de 18/05/2018, 
RESOLVE: 
 
N° 1225 – REMOVER via Cadastro Permanente de Remoção a servidora QUERUBINA 
AURELIO BEZERRA, ocupante do cargo de Técnico em Assuntos Educacionais, matrícula 
SIAPE n° 1641868, lotada atualmente no Campus Caxias do Sul para o Campus Bento 
Gonçalves do IFRS. 
 
N° 1226 – DESLIGAR do Campus Caxias do Sul a partir de 01 de outubro de 2018, a 
servidora QUERUBINA AURELIO BEZERRA, matrícula SIAPE n° 1641868, ocupante do cargo 
de Técnico em Assuntos Educacionais, em virtude de sua remoção para o Campus Bento 
Gonçalves, conforme portaria 1225//2018.  
 
N° 1227 – LOCALIZAR a servidora QUERUBINA AURELIO BEZERRA, matrícula 
SIAPE n° 1641868, ocupante do cargo de Técnico em Assuntos Educacionais, no Campus 
Bento Gonçalves do IFRS. 
  
 
 
Júlio Xandro Heck  
Reitor pro tempore do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
6 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 1250, DE 1° DE OUTUBRO DE 2018 
 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe 
são conferidas pela Portaria MEC nº 465, de 17/05/2018, publicada no DOU de 18/05/2018, 
tendo em vista o disposto nos arts. 143 e 148 da Lei n° 8.112, de 11 de dezembro de 1990, 
RESOLVE: 
 
Art. 1º DESIGNAR os servidores MARCELO HORTA SANÁBIO, Procurador 
Federal, Matrícula SIAPE nº 1182104; SUELEN DA ROLT, Assistente em Administração, 
Matrícula SIAPE nº 1678978, e JACIRA CASAGRANDE, Telefonista, Matrícula SIAPE nº 
1102290, para, sob a presidência do primeiro, constituírem Comissão de Processo 
Administrativo Disciplinar, incumbida de apurar, no prazo de 60 (sessenta) dias, as possíveis 
irregularidades referentes aos atos e fatos que constam do Processo Administrativo nº 
23367.002194.2015-19, bem como as demais infrações conexas que emergirem no decorrer 
dos trabalhos. 
 
Art. 2º Convalidar todos os atos praticados anteriormente no presente processo. 
 
Art. 3º Esta Portaria entra em vigor na data de sua publicação. 
 
 
 
Júlio Xandro Heck 
Reitor pro tempore do IFRS 
 
