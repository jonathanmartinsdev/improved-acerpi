 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
      
 
 
 
 
 
 
 
Novembro/2018 
 
 
 
 
                                                                               Publicado em 14 de dezembro de 2018. 
 
 
 
              
                
     Comissão Responsável pela edição e publicação: 
 
               
Marisol Becker Johann 
Marc Emerim 
Rafaela Padilha 
Willian Miranda Rustick 
          
                                                                           
                                                                                  Portaria Nº 1208, de 01 de outubro de 2018. 
 
 
 
 
 
 
                               
 
                Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
                 do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                                 de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                      República Federativa do Brasil, Brasília, v. 112, nº 157, 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
                                                                  p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
 
 
 
Ministro da Educação Rossieli Soares da Silva 
 
Secretária de Educação Profissional e Tecnológica Romero Portella Raposo Filho 
 
Reitor Pro Tempore Júlio Xandro Heck 
 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional Amilton de Moura Figueiredo 
Pró-reitor de Ensino Lucas Coradini 
Pró-reitora de Extensão Marlova Benedetti 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do 
Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 606, de 09 de maio de 2018. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no âmbito 
da reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
2 Atestados Médicos  08 
   
   
  
   
              
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 1.343, DE 05 DE NOVEMBRO DE 2018 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pela Portaria MEC nº 465, de 17/05/2018, publicada no D.O.U. de 18/05/2018, 
RESOLVE: 
 
 
Art. 1º CONCEDER ABONO DE PERMANÊNCIA ao servidor LUIZ VICENTE KOCHE 
VIEIRA, ocupante do cargo de Auxiliar em Administração, Matrícula SIAPE nº 1198, do quadro 
de pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul – 
Reitoria, a partir de 03 de novembro de 2018, em razão do cumprimento dos requisitos 
constantes no art. 2º da EC 41/2003, nos termos do processo nº 23419.000986/2018-49. 
 
 
 
 
Júlio Xandro Heck 
Reitor pro tempore do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
6 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 1358, DE 09 DE NOVEMBRO DE 2018 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pelo Portaria nº 740, de 06/06/2018, publicada no D.O.U. de 07/06/2018, RESOLVE: 
 
Art. 1º - DESIGNAR a servidora QUEILA TOMIELO DE CAMARGO, Engenheiro-Área, 
Matrícula SIAPE nº 2172038, substituta eventual da função de Coordenador de Obras, Código 
FG-0002. 
 
 
 
 
 
 
Tatiana Weber 
Reitora substituta do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
7 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 1364, DE 14 DE NOVEMBRO DE 2018 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pela Portaria MEC nº 465, de 17/05/2018, publicada no D.O.U. de 18/05/2018, 
RESOLVE: 
 
Art. 1º NOMEAR a servidora CAROLINE FORNASIER SANCHES, Assistente em 
Administração, Matrícula SIAPE nº 2024236, Substituta da função de Diretora de Licitações e 
Contratos, Código CD-0003, pelo período de 26 de novembro de 2018 a 29 de novembro de 
2018. 
 
 
 
 
 
Júlio Xandro Heck 
Reitor pro tempore do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
8 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
ATESTADOS MÉDICOS 
 
 
Nome do 
Servidor (a) 
Matrícula 
Siape 
Períodos de 
Afastamento 
Quantidade 
de Dias 
Observação 
Marcelo Mejolaro 
Fantin 2141154 
01/11/2018 a 
01/11/2018 
01 dia  
Licença para Tratamento em 
Saúde 
Cintia Tavares 
Pires da Silva 1573513 
05/11/2018 a 
05/11/2018 
01 dia  
Licença para Tratamento em 
Saúde 
Caroline Possoli 
Beltran 
2718108 
05/11/2018 a 
06/11/2018 
02 dias 
Licença para Tratamento em 
Saúde 
Marizete 
Teresinha Fabris 1657048 
08/11/2018 a 
07/12/2018 
30 dias 
Licença para Tratamento em 
Pessoa da Família 
Cintia Tavares 
Pires da Silva 1573513 
21/11/2018 a 
21/11/2018 
01 dia  
Licença para Tratamento em 
Saúde 
Tatiane Dumerqui 
Kuczkowski 2853218 
11/10/2018 a 
09/11/2018 
30 dias 
Licença para Tratamento em 
Saúde 
Tatiane Dumerqui 
Kuczkowski 2853218 
12/11/2018 a 
30/11/2018 
19 dias 
Licença para Tratamento em 
Saúde 
 
