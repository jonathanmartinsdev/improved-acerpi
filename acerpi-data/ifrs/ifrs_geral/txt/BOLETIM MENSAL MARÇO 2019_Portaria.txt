 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
      
 
 
 
 
 
 
 
Março/2019 
 
 
 
 
                                                                                       Publicado em 17 de abril de 2019 
 
 
 
              
                
     Comissão Responsável pela edição e publicação: 
 
               
Marisol Becker Johann 
Marc Emerim 
Rafaela Padilha 
Willian Miranda Rustick 
          
                                                                           
                                                                                  Portaria Nº 1208, de 01 de outubro de 2018. 
 
 
 
 
 
 
                               
 
                Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
                 do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                                 de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                      República Federativa do Brasil, Brasília, v. 112, nº 157, 
                                                                  p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
Ministro da Educação Abraham Weintraub 
 
Secretário de Educação Profissional e Tecnológica Ariosto Antunes Culau 
 
Reitor Pro Tempore Júlio Xandro Heck 
 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional Amilton de Moura Figueiredo 
Pró-reitor de Ensino Lucas Coradini 
Pró-reitora de Extensão Marlova Benedetti 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do 
Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 606, de 09 de maio de 2018. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no âmbito 
da reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
2 Atestados Médicos  9 
   
   
  
   
              
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIAS DE 13 DE MARÇO DE 2019 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pelo Portaria nº 740, de 06/06/2018, publicada no DOU de 07/06/2018, RESOLVE: 
 
Nº 112 - DISPENSAR a servidora KARINE DE OLIVEIRA FONSECA, Assistente em 
Administração, Matrícula SIAPE nº 2054022, de substituta eventual da função de Coordenadora 
de Ingresso, Código FG-0002, a partir de 1º de março de 2019. 
 
Nº 113 - DESIGNAR o servidor WILLIAN MIRANDA RUSTICK, Auxiliar em 
Administração, Matrícula SIAPE nº 2084739, substituto eventual da função de Coordenadora de 
Ingresso, Código FG-0002. 
 
 
 
 
 
 
Tatiana Weber 
Reitora Substituta do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
6 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 123, DE 08 DE MARÇO DE 2019 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA 
E TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são 
conferidas pela Portaria nº 740, de 06/06/2018, publicada no DOU de 07/06/2018, com 
fundamento na Medida Provisória nº 2.174-28, de 24/08/2001 e considerando, ainda o teor do 
Processo nº 23371.000095/2019-67, RESOLVE: 
 
Art. 1º ALTERAR a carga horária da servidora CRISTIANE ANCILA MICHELIN, 
ocupante do cargo de Contador, matrícula SIAPE n° 1808390, lotada no Campus Sertão, de 
8(oito) horas diárias e 40 (vinte) semanais, para 6(seis) horas diárias e 30(trinta)semanais, a 
partir de 01/04/2019. 
 
Art. 2º Esta portaria entra em vigor na data de sua publicação. 
 
 
 
 
Tatiana Weber 
Reitora substituta do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
7 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 141, DE 13 DE MARÇO DE 2019 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pelo Portaria nº 740, de 06/06/2018, publicada no DOU de 07/06/2018, RESOLVE: 
 
Art. 1º NOMEAR a servidora LISIANE BENDER DA SILVEIRA, Técnico em Assuntos 
Educacionais, Matrícula SIAPE nº 2018238, substituta da função de Chefe do Departamento de 
Avaliação Institucional, Código CD-0004, pelo período de 18 de fevereiro de 2019 a 1º de março 
de 2019. 
 
 
 
 
 
 
Tatiana Weber 
Reitora Substituta do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
8 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 150, DE 13 DE MARÇO DE 2019 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pelo Portaria nº 740, de 06/06/2018, publicada no DOU de 07/06/2018, RESOLVE: 
 
Art. 1º INTERROMPER as férias nos termos do artigo 80 da Lei 8112/90, do servidor 
JESUS ROSEMAR BORGES, matrícula SIPE 00087249, a partir do dia 18MAR2019, referente 
ao exercício 2018, parcela 5, iniciada em 14MAR2019. 
 
Art. 2º INTERROMPER as férias nos termos do artigo 80 da Lei 8112/90, do servidor 
SUELEN PATRICIA DOS SANTOS, matrícula SIPE 01779660, a partir do dia 19MAR2019, 
referente ao exercício 2018, parcela 2, iniciada em 18MAR2019. 
 
 
 
 
 
 
Tatiana Weber 
Reitora Substituta do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
9 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
ATESTADOS MÉDICOS 
 
 
 
Nome do 
Servidor (a) 
Matrícula 
Siape 
Períodos de 
Afastamento 
Quantidade 
de Dias 
Observação 
Danner Souza 
Terra 
2711765 
11/03/2019 a 
13/03/2019 
03 dias 
Licença  para Tratamento da 
Própria Saúde 
Marisol Becker 
Johann 
2027298 
11/03/2019 a 
11/03/2019 
01 dia 
Licença  para Tratamento da 
Própria Saúde 
Lysandra Ramos 
Tieppo 
2979692 
13/03/2019 a 
14/03/2019 
02 dias 
Licença  para Tratamento em 
Pessoa da Família 
Mere Luci da Rosa 1946284 
13/03/2019 a 
13/03/2019 
01 dia 
Licença  para Tratamento da 
Própria Saúde 
Zélia Regina da 
Silva 
2073104 
25/03/2019 a 
25/03/2019 
01 dia 
Licença  para Tratamento da 
Própria Saúde 
Angela Marin 2013324 
27/03/2019 a 
29/03/2019 
03 dias 
Licença  para Tratamento da 
Própria Saúde 
Jane Conceicao 
Cardoso Jorge 
2242630 
25/03/2019 a 
27/03/2019 
03 dias 
Licença  para Tratamento da 
Própria Saúde 
Jane Conceicao 
Cardoso Jorge 
2242630 
28/03/2019 a 
29/03/2019 
02 dias 
Licença  para Tratamento da 
Própria Saúde 
Raquel Selbach M. 
Colombo 
3060435 
27/03/2019 a 
05/04/2019 
10 dias 
Licença  para Tratamento da 
Própria Saúde 
Tânia Salete 
Bianchi Carvalho 
1102345 
19/03/2019 a 
21/03/2019 
03 dias 
Licença  para Tratamento em 
Pessoa da Família 
 
