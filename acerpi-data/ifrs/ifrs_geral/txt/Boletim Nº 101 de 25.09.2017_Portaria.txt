 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
      
 
 
 
 
 
 
 
25 de setembro de 2017 
 
 
 
 
 
 
              
                
     Comissão Responsável pela edição e publicação: 
 
               
Rafaela Padilha 
Lysandra Ramos Tieppo 
Marc Emerim 
Marisol Becker Johann 
          
                                                                           
                                                                                  Portaria Nº 755, de 02 de maio de 2017. 
 
 
 
 
 
 
                               
 
                Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
                 do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                                 de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                      República Federativa do Brasil, Brasília, v. 112, nº 157, 
                                                                  p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
Ministro da Educação José Mendonça Bezerra Filho 
 
Secretária de Educação Profissional e Tecnológica Eline Neves Braga Nascimento 
 
Reitor Osvaldo Casares Pinto 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional José Eli Santos dos Santos 
Pró-reitora de Ensino Clarice Monteiro Escott 
Pró-reitora de Extensão Viviane Silva Ramos 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande 
do Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 755, de 02 de maio de 2016. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no âmbito 
da reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
   
   
   
 
 
 
   
   
   
 
             
     
   
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIAS DE 25 DE SETEMBRO DE 2017 
 
 
O REITOR SUBSTITUTO DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições que lhe são conferidas pela 
Portaria nº 1128, de 30 de junho de 2017, publicada no D.O.U. de 03/07/2017, RESOLVE: 
 
Nº 1542 – REMOVER via Cadastro Permanente de Remoção a servidora SILVIA 
RAQUEL SCHIAVO DE AZAMBUJA, ocupante do cargo de Administrador, matrícula SIAPE n° 
1086422, lotada atualmente na Reitoria para o Campus Farroupilha do IFRS. 
 
N° 1543 – DESLIGAR da Reitoria a partir de 25 de setembro de 2017, a servidora 
SILVIA RAQUEL SCHIAVO DE AZAMBUJA, matrícula n° 1086422, ocupante do cargo de 
Administrador, em virtude de sua remoção para o Campus Farroupilha, conforme portaria 
1542/2017. 
 
N° 1544 – LOCALIZAR a servidora SILVIA RAQUEL SCHIAVO DE AZAMBUJA, 
matrícula n° 1086422, ocupante do cargo de Administrador, no Campus Farroupilha do IFRS. 
 
 
 
 
JOSÉ ELI SANTOS DOS SANTOS 
Reitor Substituto do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
