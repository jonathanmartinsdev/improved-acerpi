 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
      
 
 
 
 
 
 
 
08 de outubro de 2019 
 
 
 
 
 
 
              
                
     Comissão Responsável pela edição e publicação: 
 
               
Marisol Becker Johann 
Willian Miranda Rustick  
Suélen Patrícia dos Santos 
          
                                                                           
                                                                              Portaria Nº 303, de 24 de maio de 2019. 
 
 
 
 
 
 
 
Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
               do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                               de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                    República Federativa do Brasil, Brasília, v. 112, nº 157, 
                                                                p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
Ministro da Educação Abraham Weintraub  
 
Secretário de Educação Profissional e Tecnológica Ariosto Antunes Culau  
 
Reitor Pro Tempore Júlio Xandro Heck 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional Amilton de Moura Figueiredo 
Pró-reitor de Ensino Lucas Coradini 
Pró-reitora de Extensão Marlova Benedetti 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do 
Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 606, de 09 de maio de 2018. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no âmbito 
da reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
   
   
   
   
   
 
 
 
   
   
   
 
            
      
   
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 582, de 1º DE OUTUBRO DE 2019 
 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são conferidas pela 
Portaria n° 740 de 06/06/2018, publicada no DOU de 07/06/2018, RESOLVE:  
 
Art. 1º DESIGNAR o Pró-reitor de Pesquisa, Pós-Graduação e Inovação do 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, EDUARDO GIROTTO, 
Matrícula Siape n° 1893215, e a Pró-reitora Adjunta de Pesquisa, Pós-Graduação e Inovação do 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, MARILIA BONZANINI 
BOSSLE, Matrícula Siape n° 2390719 como representantes legais do IFRS, com plenos poderes 
para representar isoladamente a Instituição no âmbito do SisGen, Ministério do Meio 
Ambiente, CGen e IBAMA em todos os temas relacionados à Lei n° 13.123, de 2015 e seus 
regulamento, inclusive no que se refere à regularização a ser efetivada por meio do termo de 
compromisso. 
 
Art. 2º Os efeitos da portaria nº 1074, de 08 de agosto de 2018, continuam 
válidos. 
 
Art. 3º Esta Portaria entra em vigor na data de sua publicação. 
 
 
 
 
TATIANA WEBER 
Reitora substituta do IFRS 
 
