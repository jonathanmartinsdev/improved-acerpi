 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
      
 
 
 
 
 
 
 
10 de setembro de 2018 
 
 
 
 
 
 
              
                
     Comissão Responsável pela edição e publicação: 
 
               
Rafaela Padilha 
Lysandra Ramos Tieppo 
Marc Emerim 
Andrew Chaves Feitosa da Silva 
          
                                                                           
                                                                                  Portaria Nº 606, de 09 de maio de 2018. 
 
 
 
 
 
 
 
Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
               do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                               de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                    República Federativa do Brasil, Brasília, v. 112, nº 157, 
                                                                p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
Ministro da Educação Rossieli Soares da Silva 
 
Secretária de Educação Profissional e Tecnológica Romero Portella Raposo Filho 
 
Reitor Pro Tempore Júlio Xandro Heck 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional Amilton de Moura Figueiredo 
Pró-reitor de Ensino Lucas Coradini 
Pró-reitora de Extensão Marlova Benedetti 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande 
do Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 606, de 09 de maio de 2018. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no âmbito 
da reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
   
   
   
   
   
 
 
 
   
   
   
 
            
      
   
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 1171, DE 10 DE SETEMBRO DE 2018 
 
 
O REITOR EM EXERCÍCIO DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições que lhe são 
conferidas pela Portaria nº 1158, de 06/09/2018, publicada no Boletim de Pessoal do IFRS de 
06/09/2018, pág. 5 e, tendo em vista o disposto no art. 145 da Lei nº 8.112, de 11 de dezembro 
de 1990, e considerando o que consta no Processo nº 23419.000823/2018-66, RESOLVE: 
 
Art. 1º PRORROGAR, por 30 (trinta) dias, o prazo para conclusão dos trabalhos 
da Comissão de Sindicância designada pela Portaria nº 1083, de 10/08/2018, publicada no 
Boletim de Pessoal da Instituição em 13/08/2018. 
 
 
 
 
 
Marc Emerim 
Reitor em exercício do IFRS 
 
