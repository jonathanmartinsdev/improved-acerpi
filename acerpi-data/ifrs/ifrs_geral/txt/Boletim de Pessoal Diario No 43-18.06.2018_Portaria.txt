 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
      
 
 
 
 
 
 
 
18 de junho de 2018 
 
 
 
 
 
 
              
                
     Comissão Responsável pela edição e publicação: 
 
               
Rafaela Padilha 
Lysandra Ramos Tieppo 
Marc Emerim 
Andrew Chaves Feitosa da Silva 
          
                                                                           
                                                                                  Portaria Nº 606, de 09 de maio de 2018. 
 
 
 
 
 
 
 
Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
               do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                               de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                    República Federativa do Brasil, Brasília, v. 112, nº 157, 
                                                                p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
Ministro da Educação Rossiele Soares da Silva 
 
Secretária de Educação Profissional e Tecnológica Eline Neves Braga Nascimento 
 
Reitor Substituto Júlio Xandro Heck 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional Amilton de Moura Figueiredo 
Pró-reitora de Ensino Lucas Coradini 
Pró-reitora de Extensão Marlova Benedetti 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande 
do Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras 
providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 755, de 02 de maio de 2016. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no 
âmbito da reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
   
   
   
   
 
 
 
   
   
   
 
            
      
   
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIAS DE 18 DE JUNHO DE 2018 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso de suas atribuições legais que lhe são 
conferidas pela Portaria MEC nº 465, de 17/05/2018, publicada no D.O.U. de 18/05/2018, 
RESOLVE: 
 
N° 798 – REMOVER via Cadastro Permanente de Remoção a servidora PAULA 
MARIA ZANOTELLI, ocupante do cargo de Pedagogo, matrícula SIAPE n°1244525, lotada 
atualmente na Reitoria para o Campus Alvorada do IFRS. 
 
N° 799– DESLIGAR da Reitoria a partir de 18 de junho de 2018, a servidora PAULA 
MARIA ZANOTELLI matrícula n° 1244525, ocupante do cargo de Pedagogo, em virtude de 
sua remoção para o Campus Alvorada, conforme portaria 798/2018. 
 
N° 800 – LOCALIZAR a servidora PAULA MARIA ZANOTELLI, matrícula n° 1244525, 
ocupante do cargo de Pedagogo no Campus Alvorada do IFRS. 
 
 
 
 
Júlio Xandro Heck  
Reitor pro tempore do IFRS 
 
 
 
 
 
 
 
