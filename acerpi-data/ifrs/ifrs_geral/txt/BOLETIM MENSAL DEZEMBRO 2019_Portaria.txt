 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
1 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
      
 
 
 
 
 
 
DEZEMBRO/2019 
  
 
 
                                                                                       Publicado em 22 de janeiro de 2020 
 
             
                
     Comissão Responsável pela edição e publicação: 
 
               
Marisol Becker Johann 
Suélen Patrícia dos Santos 
Willian Miranda Rustick 
          
                                                                           
                                                                                  Portaria Nº 303, de 24 de maio de 2019. 
 
           
 
                Boletim de Pessoal destinado à publicação dos atos administrativos da Reitoria 
                 do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, 
                                 de acordo com  Lei nº 4.965, de 5 de maio de 1966. Diário Oficial da 
                                                      República Federativa do Brasil, Brasília, v. 112, nº 157, 
                                                              p. 4.971, de 10 de maio de 1966. Seção I, pt. 1. 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
2 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
Ministro da Educação Abraham Weintraub 
 
Secretário de Educação Profissional e Tecnológica Ariosto Antunes Culau 
 
Reitor Pro Tempore Júlio Xandro Heck 
 
Pró-reitora de Administração Tatiana Weber 
Pró-reitor de Desenvolvimento Institucional Amilton de Moura Figueiredo 
Pró-reitor de Ensino Lucas Coradini 
Pró-reitora de Extensão Marlova Benedetti 
Pró-reitor de Pesquisa, Pós Graduação e Inovação Eduardo Girotto 
 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
3 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
O Boletim de Pessoal do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do 
Sul é destinado a dar publicidade aos atos e procedimentos formais da instituição. 
 
 
Referências: 
 
Lei Nº 4.965, de 5 de maio de 1966. 
 
Dispõe sobre a publicação dos atos relativos aos servidores públicos e dá outras providências. 
 
Instrução Normativa Nº 07 de 27 de abril de 2017. 
 
Normatiza o fluxo para edição e publicação do Boletim de Serviço e de Pessoal da reitoria e 
Boletim de Serviço dos campi e revoga a IN 12/2015. 
 
Portaria Nº 606, de 09 de maio de 2018. 
 
Estabelece a Comissão responsável pela edição e publicação do Boletim de Pessoal no âmbito da 
reitoria do IFRS. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
4 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
SUMÁRIO 
 
 
1 Portarias 5 
   
2 Atestados Médicos 11 
   
   
  
   
              
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
5 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIAS DE 29 DE NOVEMBRO DE 2019 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são pela Portaria nº 
465, de 17/05/2018, publicada no DOU de 18/05/2018, RESOLVE: 
 
Nº 697 - EXONERAR a servidora ANELISE FOSCHIERA, Administradora, Matrícula 
SIAPE nº 1982157, de Substituta Eventual da função de Pró-Reitora Adjunta de Desenvolvimento 
Institucional, Código CD-0003. 
 
Nº 698 - NOMEAR o servidor BRUNO DINIZ MACHADO, Administrador, Matrícula 
SIAPE nº 1015823, para Substituto Eventual da função de Pró-Reitor Adjunto de 
Desenvolvimento Institucional, Código CD-0003. 
 
 
  
 
JÚLIO XANDRO HECK 
Reitor pro tempore do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
6 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 710, DE 05 DE DEZEMBRO DE 2019 
 
 
O REITOR EM EXERCÍCIO DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são conferidas pela 
Portaria nº 693, de 29/11/2019, RESOLVE: 
 
Art. 1º NOMEAR a servidora LARISSA BRANDELLI BUCCO, Professora do Ensino 
Básico, Técnico e Tecnológico, Matrícula SIAPE nº 2327276, substituta da função de Pró-Reitora 
Adjunta de Ensino, Código CD-0003, pelo período de 02 de janeiro de 2020 a 10 de fevereiro de 
2020. 
 
 
 
 
LUCAS CORADINI 
Reitor em Exercício do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
7 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
 
PORTARIA Nº 714, DE 06 DE DEZEMBRO DE 2019 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são pela 
Portaria nº 740, de 06/06/2018, RESOLVE: 
 
Art. 1° NOMEAR o servidor JONAS BARONIO, Assistente em Administração, 
Matrícula SIAPE nº 2004803, substituto da função de Diretor de Licitações e Contratos, Código 
CD-0003, pelos períodos de 16 de dezembro de 2019 a 27 de dezembro de 2019 e 27 de janeiro 
de 2020 a 31 de janeiro de 2020. 
 
 
 
TATIANA WEBER 
Reitora Substituta do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
8 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 719, DE 10 DE DEZEMBRO DE 2019 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são conferidas pela 
Portaria nº 465, de 17/05/2018, publicada no DOU de 18/05/2018, RESOLVE: 
 
Art. 1° NOMEAR a servidora SANDRA LIGIA AGNOLIN, Técnica em Assuntos 
Educacionais, Matrícula SIAPE nº 1751941, substituta da função de Diretoria de Ensino, Código 
CD-0003, pelos períodos de 30 de dezembro de 2019 a 11 de janeiro de 2020 e de 13 de janeiro 
de 2020 a 24 de janeiro de 2020. 
 
 
 
 
JÚLIO XANDRO HECK 
Reitor pro tempore do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
9 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
 
PORTARIA Nº 735, DE 19 DE DEZEMBRO DE 2019 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são conferidas pela 
Portaria nº 465, de 17/05/2018, publicada no DOU de 18/05/2018, RESOLVE: 
 
Art. 1º NOMEAR a servidora JAQUELINE MORGAN, Professora do Ensino Básico, 
Técnico e Tecnológico, Matrícula SIAPE nº 2013541, substituta eventual da função de Pró-Reitora 
Adjunta de Pesquisa, Pós-graduação e Inovação, Código CD-0003. 
 
 
 
 
JÚLIO XANDRO HECK 
Reitor pro tempore do IFRS 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
10 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
PORTARIA Nº 736, DE 19 DE DEZEMBRO DE 2019 
 
 
O REITOR PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são conferidas pela 
Portaria nº 465, de 17/05/2018, publicada no DOU de 18/05/2018, RESOLVE: 
 
Art. 1º EXONERAR a servidora LARISSA BRANDELLI BUCCO, Professora do Ensino 
Básico, Técnico e Tecnológico, Matrícula SIAPE nº 2327276, Substituta Eventual da função de 
Diretor Geral Pro Tempore do Campus Avançado Veranópolis, Código CD-0003, a partir de 01 de 
janeiro de 2020. 
 
 
 
 
JÚLIO XANDRO HECK 
Reitor pro tempore do IFRS 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
11 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
PORTARIA N° 746, DE 23 DE DEZEMBRO DE 2019 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são conferidas pela 
Portaria nº 740, de 06/06/2018, publicada no DOU de 07/06/2018, RESOLVE: 
 
Art. 1º CONCEDER Licença para Tratar de Interesses Particulares, pelo prazo de 01 
(um) ano, a partir de 05/02/2020, à servidora CAMILA VANESSA DOBROVOLSKI IBRAHIM, 
ocupante do cargo de Assistente em Administração, matrícula SIAPE n° 1794504, com 
fundamento no art. 91 da Lei 8.112/90 e nos termos do Processo n° 23360.000797/2019-70. 
 
 
 
 
 
                                                         TATIANA WEBER 
                                                 Reitora Substituta do IFRS 
  
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
12 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
 
PORTARIA N° 747, DE 23 DE DEZEMBRO DE 2019 
 
 
A REITORA SUBSTITUTA DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E 
TECNOLOGIA DO RIO GRANDE DO SUL, no uso das atribuições legais que lhe são conferidas pela 
Portaria nº 740, de 06/06/2018, publicada no DOU de 07/06/2018, RESOLVE: 
 
Art. 1º CONCEDER Licença para Tratar de Interesses Particulares, pelo prazo de 01 
(um) ano, a partir de 13/01/2020, ao servidor JEAN DA ROLT JOAQUIM, ocupante do cargo de 
Professor EBTT, matrícula SIAPE n° 1090330, com fundamento no art. 91 da Lei 8.112/90 e nos 
termos do Processo n° 23360.000769/2019-52. 
 
 
 
 
 
                                                         TATIANA WEBER 
                                                 Reitora Substituta do IFRS 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO 
Secretaria de Educação Profissional e Tecnológica 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul 
Diretoria de Gestão de Pessoas 
13 
Rua General Osório, 348 - Centro - Bento Gonçalves/RS - CEP: 95700-086 
 
 
 
ATESTADOS MÉDICOS 
 
 
Nome do 
Servidor (a) 
Matrícula 
Siape 
Períodos de 
Afastamento 
Quantidade 
de Dias 
Observação 
Lysandra Ramos 
Tieppo 2979692 
11/12/2019 a 
14/12/2019 
04 dias 
Licença Para Tratamento da 
Própria Saúde 
Margarete de 
Quevedo 1797058 
09/12/2019 a 
10/12/2019 
02 dias 
Licença Para Tratamento da 
Própria Saúde 
Maria do Carmo 
de Oliveira 1103282 
10/12/2019 a 
10/12/2019 
01 dia 
Licença Para Tratamento da 
Própria Saúde 
Fúlvio Daniel 
Cavalli 1098302 
16/12/2019 a 
20/12/2019 
05 dias 
Licença Para Tratamento da 
Própria Saúde 
 
