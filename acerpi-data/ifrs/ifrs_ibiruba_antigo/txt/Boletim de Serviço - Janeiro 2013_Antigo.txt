1 
 
 
 
 
 
 
 
 
Boletim de Serviço 
Janeiro 
2013 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Campus Avançado Ibirubá 
Rua Nelsi Ribas Fritsch, 1111 - Bairro Esperança 
CEP: 98200-000 – Ibirubá – RS 
 
Comissão Responsável pela elaboração, impressão e distribuição: 
Cristiane Brauner 
Fernando Leão 
Luana Dapper 
Monique Lorenson 
 
 
2 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO – MEC 
Ministro 
Aloízio Mercadante Oliva 
 
SECRETARIA DE EDUCAÇÃO PROFISSIONAL E TECNOLÓGICA – SETEC 
Secretário 
Marco Antônio de Oliveira 
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO 
GRANDE DO SUL 
Reitora 
Claudia Schiedeck Soares de Souza 
 
CAMPUS AVANÇADO IBIRUBÁ 
Diretora-Geral 
Migacir Trindade Duarte Flôres 
 
 
 
 
 
 
 
 
 
 
3 
 
 
 
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA  
DO RIO GRANDE DO SUL – CAMPUS AVANÇADO IBIRUBÁ 
 
 
 
 
ANIVERSARIANTES 
 
 
 
  
 
4 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
5 
 
 
Boletim de Serviço Nº 07/2013 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA  
DO RIO GRANDE DO SUL – CAMPUS AVANÇADO IBIRUBÁ 
Sumário 
I- PORTARIAS................................................................................................................6 
II – ORDENS DE SERVIÇO..........................................................................................12 
III – CONCESSÃO DE DIÁRIAS..................................................................................13 
IV – FÉRIAS...................................................................................................................15 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
6 
 
 
Boletim de Serviço Nº 07/2013 
I – PORTARIAS 
 
PORTARIAS DE 02 DE JANEIRO DE 2013 
 
  A DIRETORA GERAL DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS AVANÇADO 
IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, no uso de suas 
atribuições, RESOLVE: 
 
 Nº 01 - INTERROMPER as férias do servidor MILTON JOSÉ BUSNELLO, 
Técnico em Agropecuária, matrícula Siape nº 1893291, a partir do dia 03 de janeiro de 
2013, para atender as necessidades de relevância desta instituição, conforme o artigo 80 
da lei 8112/90, estabelecendo o reinício para o dia 7 de janeiro de 2013. 
 
 Nº 02 - INTERROMPER as férias do servidor EDUARDO FERNANDES ANTUNES, 
Técnico de Tecnologia da Informação, matrícula Siape nº 1818514, a partir do dia 03 de janeiro 
de 2013, para atender as necessidades de relevância desta instituição, conforme o artigo 80 da 
lei 8112/90, estabelecendo o reinício para o dia 14 de janeiro de 2013. 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
 
PORTARIA Nº 03 DE 07 DE JANEIRO DE 2013 
 
  A DIRETORA GERAL DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS AVANÇADO 
IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, no uso de suas 
atribuições, RESOLVE: 
 
  DESIGNAR o servidor JOVANI JOSÉ ALBERTI, Técnico em 
Mecânica, matrícula Siape nº 995602, para responder como substituto da Direção Geral, 
do IFRS – Campus Avançado de Ibirubá, na ausência do Titular, servidora MIGACIR 
TRINDADE DUAR FLORES, matrícula Siape nº 2104561 nos dias 08 e 09 de janeiro de 
2013. 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
 
  
PORTARIA Nº 04 DE 07 DE JANEIRO DE 2013 
 
  A DIRETORA GERAL  DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS AVANÇADO 
IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, no uso de suas 
atribuições, RESOLVE: 
 
  DESIGNAR os servidores abaixo relacionados para atuarem como fiscais 
no contrato 71/2012, consoante à Prestação de Serviços de Terraplanagem do Campus 
Avançado de Ibirubá. 
 
Fiscal 
Milton José Busnello – Matrícula Siape: 1893291 
Suplente 
André Luiz Marcondes – Matrícula Siape: 1982908  
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
 
PORTARIA Nº 05 DE 07 DE JANEIRO DE 2013 
 
  A DIRETORA GERAL  DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS AVANÇADO 
IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, no uso de suas 
atribuições, RESOLVE: 
 
  DESIGNAR os servidores abaixo relacionados para atuarem como fiscais 
no contrato 62/2012, consoante à Prestação de Serviços de Colheita no Campus 
Avançado de Ibirubá. 
 
Fiscal 
Dagmar Pedro Tamanho – Matrícula Siape: 1916911 
Suplente 
Eduardo Girotto – Matrícula Siape: 1893215  
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore"
8 
 
 
 
PORTARIA Nº 06 DE 07 DE JANEIRO DE 2013 
 
  A DIRETORA GERAL DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de 
suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, 
publicado no DOU de 22 de novembro de 2012, no uso de suas atribuições, RESOLVE: 
 
  ESTABELECER o horário especial de funcionamento do IFRS – Campus 
Ibirubá, no período de 08 de janeiro a 08 de fevereiro de 2013. 
 
I. Nas segundas-feiras o expediente inciará às 12h e se encerrará às 18h, ininterruptamente. 
 
II. Nas terças, quartas e quintas-feiras, os setores funcionarão das 7:45h às 17:15h. 
 
III. Nas sextas-feiras o expediente iniciará às 7:45h e se encerrará às 13:45h, 
ininterruptamente. 
 
IV. O horário de funcionamento dos setores obedecerá às necessidades destes, podendo ser 
alterado, tendo em vista outras disposições indicadas pela chefia respectiva e aprovadas pela 
Direção-Geral. 
 
V. Os serviços terceirizados devem ser mantidos nos horários regulares, conforme contratos. 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
 
PORTARIA Nº 07 DE 10 DE JANEIRO DE 2013 
 
  A DIRETORA GERAL DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de 
suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, 
publicado no DOU de 22 de novembro de 2012, no uso de suas atribuições, RESOLVE: 
  
   INTERROMPER as férias do servidor ANDRÉ RICARDO DIERINGS, 
Professor do Ensino Básico, Técnico e Tecnológico, matrícula Siape nº 1804751, a partir do 
dia 21 de janeiro de 2013, para atender as necessidades de relevância desta instituição, 
conforme o artigo 80 da lei 8112/90, estabelecendo o reinício para o dia 26 de janeiro de 
2013. 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
 
 
 
9 
 
 
 
PORTARIAS DE 11 DE JANEIRO DE 2013 
 
  A DIRETORA GERAL DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de 
suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, 
publicado no DOU de 22 de novembro de 2012, no uso de suas atribuições, RESOLVE: 
 
   Nº 08 - INTERROMPER as férias do servidor MARCOS PAULO LUDWIG, 
Professor do Ensino Básico, Técnico e Tecnológico, matrícula Siape nº 1645508, a partir do 
dia 14 de janeiro de 2013, para atender as necessidades de relevância desta instituição, 
conforme o artigo 80 da lei 8112/90, estabelecendo o reinício para o dia 19 de janeiro de 
2013. 
 
  Nº 09 - INTERROMPER as férias do servidor EDUARDO GIROTTO, 
Professor do Ensino Básico, Técnico e Tecnológico, matrícula Siape nº 1893215, a partir do 
dia 14 de janeiro de 2013, para atender as necessidades de relevância desta instituição, 
conforme o artigo 80 da lei 8112/90, estabelecendo o reinício para o dia 19 de janeiro de 
2013. 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
 
PORTARIA Nº 10 DE 16 DE JANEIRO DE 2013 
 
  A DIRETORA GERAL  DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de 
suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, 
publicado no DOU de 22 de novembro de 2012, no uso de suas atribuições, RESOLVE: 
 
  DESIGNAR os servidores Luís Cláudio Gubert, SIAPE nº 1781712, André 
Marek, SIAPE nº 1982915, Cristiane Brauner, SIAPE nº 1982614, Jovani José Alberti, 
SIAPE nº 0995602, Milton José Busnello, SIAPE nº 1893291, para sob a presidência do 
primeiro comporem a Comissão de Fiscalização da Obra de Construção do Prédio da 
Biblioteca do  IFRS – Campus Ibirubá, conforme processo n° 23366.000180.2012-28. 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
 
 
 
 
 
 
PORTARIA Nº 11 DE 18 DE JANEIRO DE 2013 
10 
 
 
 
 
  A DIRETORA GERAL DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de 
suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, 
publicado no DOU de 22 de novembro de 2012, no uso de suas atribuições, RESOLVE: 
 
  DESIGNAR o servidor FERNANDO DE OLIVEIRA LEÃO, Técnico  em 
Audiovisual, matrícula Siape nº 1982901, para responder como substituto pela Direção Geral 
do  IFRS – Campus Ibirubá nos dias 21, 22 e 23 de janeiro de 2013. 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
 
 
 
RETIFICAÇÃO 
 
 O DIRETOR GERAL SUBSTITUTO DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS  IBIRUBÁ, no uso de 
suas atribuições legais subdelegadas pela Portaria nº 151, de 21 de de dezembro de 2012, 
publicado no Boletim de Serviços de Dezembro do IFRS – Campus Ibirubá, RESOLVE 
RETIFICAR: 
 
 A Portaria nº 11, de 18 de janeiro de 2013, publicada no Boletim de Serviço do IFRS 
Campus Ibirubá, referente à designação do servidor FERNANDO DE OLIVEIRA LEÃO, 
Técnico em Audiovisual, matrícula Siape nº 1982901, como diretor substituto do Campus 
Ibirubá alterando os dias de substituição de 21, 22 e 23 para 21, 24 e 25 de janeiro de 2013, 
visto que a reunião de Diretores que motivou essa designação foi transferida dos dias 22 e 23 
para os dias 24 e 25. 
 
Prof. Marcelo Lima Calixto 
Diretor Geral Substituto 
Port. n° 152/2012 
 
PORTARIA Nº 12 DE 23 DE JANEIRO DE 2013 
 
  O DIRETOR GERAL SUBSTITUTO DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS  
IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 151, de 21 de de 
dezembro de 2012, publicado no Boletim de Serviços de Dezembro do IFRS – Campus 
Ibirubá, RESOLVE: 
 
11 
 
 
 
  DESIGNAR o servidor FERNANDO DE OLIVEIRA LEÃO, Técnico  em 
Audiovisual, matrícula Siape nº 1982901, para responder como substituto pela Direção Geral 
do  IFRS – Campus Ibirubá no dia 28 de janeiro de 2013, devido ao professor Marcelo Lima 
Calixto encontrar-se em Porto Alegre. 
 
Prof. Marcelo Lima Calixto 
Diretor Geral Substituto 
Port. N° 152/2012 
 
PORTARIA Nº 13 DE 23 DE JANEIRO DE 2013 
 
  O DIRETOR GERAL SUBSTITUTO DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS  
IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 151, de 21 de de 
dezembro de 2012, publicado no Boletim de Serviços de Dezembro do IFRS – Campus 
Ibirubá, RESOLVE: 
 
  LOCALIZAR o exercício dos servidores ADILSON BARBOSA, Professor do 
Ensino Básico, Técnico e Tecnológico, DANIEL LONGO ROCKEMBACH, Professor do 
Ensino Básico, Técnico e Tecnológico, IVO MAI, Professor do Ensino Básico, Técnico e 
Tecnológico, IZANDRA ALVES, Professora do Ensino Básico, Técnico e Tecnológico, 
JUCELI DA SILVA, Professora do Ensino Básico, Técnico e Tecnológico, MAIQUEL 
GROMANN, Técnico em Agropecuária, e MARCELE NEUTZLING RICKES, Técnica em 
Assuntos Educacionais. 
 
Prof. Marcelo Lima Calixto 
Diretor Geral Substituto 
Port. N° 152/2012 
 
 
 
 
 
 
 
 
 
 
 
 
 
12 
 
 
 
II - ORDENS DE SERVIÇO 
 
 
ORDEM DE SERVIÇO Nº01/2013 , DE 4 DE JANEIRO DE 2013 
 
 A DIRETORA GERAL PRO TEMPORE DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS  
IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, RESOLVE: 
 
  Autorizar o servidor FERNANDO DE OLIVEIRA LEÃO, CPF n° 027.764.279-56, 
portador da Carteira Nacional de Habilitação nº 531550800, Categoria “B”, registro nº 
02244919760, expedida pelo Departamento Nacional de Trânsito de Santa Catarina, a dirigir 
veículos desta Instituição de Ensino.  
 
Profª. Migacir Trindade Duarte Flôres 
Diretora-Geral "Pro tempore" 
Port. N° 552/2012 
 
ORDEM DE SERVIÇO Nº02/2013 , DE 7 DE JANEIRO DE 2013  
 
 A DIRETORA GERAL PRO TEMPORE DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS  
IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, RESOLVE 
DETERMINAR: 
 - Colocação de medidores de energia nos imóveis utilizados como moradia pelos 
servidores do câmpus; 
 - Pagamento de taxa de luz conforme medições nos imóveis em que já existem 
medidores; 
 - Estipular o valor de R$50,00 (cinquenta reais) por mês para os imóveis em que não 
existem medidores até que os mesmos sejam instalados (conforme a cláusula terceira do 
contrato de locação); 
- A taxa deve ser paga mensalmente até o dia 15 (quinze) do mês subsequente, junto à 
Coordenadoria de Administração Orçamentária e Financeira do câmpus Ibirubá que emitirá 
recibo. 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora-Geral "Pro tempore" 
Port. N° 552/2012 
 
 
 
13 
 
 
 
ORDEM DE SERVIÇO Nº03/2013 , DE 23 DE JANEIRO DE 2013  
 
 O DIRETOR GERAL SUBSTITUTO DO INSTITUTO FEDERAL DE EDUCAÇÃO, 
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS  IBIRUBÁ, no uso de 
suas atribuições legais subdelegadas pela Portaria nº 151, de 21 de de dezembro de 2012, 
publicado no Boletim de Serviços de Dezembro do IFRS – Campus Ibirubá, RESOLVE: 
  Autorizar o servidor MAIQUEL GROMANN, CPF n° 009.237.200-73, portador da 
Carteira Nacional de Habilitação nº 335178535, Categoria “AB”, registro nº 03677216237, 
expedida pelo Departamento Nacional de Trânsito do Rio Grande do Sul, a dirigir veículos 
desta Instituição de Ensino.  
 
Prof. Marcelo Lima Calixto 
Diretor Geral Substituto 
Port. n° 152/2012 
 
 
III – CONCESSÃO DE DIÁRIAS 
 
Gabinete 
Nome: MAURICIO LOPES LIMA  
Função: 
 
Período da Viagem: 15/01/2013    à   15/01/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 000054/13  
Valor das Diárias (R$):  74,68  
Objetivo: Nacional - Convocação  
Descrição: Participação de reunião da CIS.  
 
 
Gabinete 
Nome: ALOISIO CORTEZ  
Função: CD-0004  
Período da 
Viagem: 
16/01/2013    à   16/01/2013  
Destino: Sertão / Ibirubá / Sertão  
Número da 
Requisição: 
000071/13  
Valor das 
Diárias (R$):  
88,80  
Objetivo: Nacional - A Serviço  
Descrição: 
Acompanhamento do início das obras referentes à reforma do ginásio 
poliesportivo e dos banheiros e vestiários do módulo esportivo do Câmpus 
Ibirubá (Tomada de preços 01/2012)  
14 
 
 
 
 
 
Gabinete 
Nome: MIGACIR TRINDADE DUARTE FLORES  
Função: CD-0002  
Período da 
Viagem: 
08/01/2013    à   09/01/2013  
Destino: Ibirubá / Bento Gonçalves / Porto Alegre / Ibirubá  
Número da 
Requisição: 
000009/13  
Valor das 
Diárias (R$):  
310,76  
Objetivo: Nacional - A Serviço  
Descrição: 
Fazer encaminhamentos na Reitoria, participar da Solenidade de Posse dos 
novos Diretores e anúncio dos novos Pró-Reitores do IFRS e participar 
juntamente com o Pró-Reitor de Administração da Reunião junto ao 
Patrimônio da União para tratar do processo de doação da área agrícola para o 
IFRS Câmpus Ibirubá.  
 
 
Gabinete 
Nome: ANDRE RICARDO DIERINGS  
Função: 
 
Período da 
Viagem: 
21/01/2013    à   25/01/2013  
Destino: Ibirubá / Santa Maria / Ibirubá  
Número da 
Requisição: 
000032/13  
Valor das 
Diárias (R$):  
727,41  
Objetivo: Nacional - Treinamento  
Descrição: 
Participação no Programa de Aperfeiçoamento de Professores de Matemática 
do Ensino Médio PAPMEM na Universidade de Santa Maria - Santa Maria - 
RS, conforme cópia de Autorização para participação em evento em anexo.  
 
Gabinete 
Nome: MARCELO LIMA CALIXTO  
Função: CD-0004  
Período da Viagem: 24/01/2013    à   25/01/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 000106/13  
Valor das Diárias (R$):  269,76  
Objetivo: Nacional - A Serviço  
Descrição: Convocação Diretores  
15 
 
 
 
IV - FÉRIAS 
 
MATRÍCULA            NOME DO SERVIDOR                    PERÍODO DE FÉRIAS 
26419-1795569 ALEXANDRE URBANO HOFFMANN 02JAN2013 16JAN2013 
26419-1795569 ALEXANDRE URBANO HOFFMANN 03JUN2013 17JUN2013 
26419-1804751 ANDRE RICARDO DIERINGS 21JAN2013 09FEV2013 
26419-1804751 ANDRE RICARDO DIERINGS 22JUL2013 26JUL2013 
26419-1804751 ANDRE RICARDO DIERINGS 02JAN2014 21JAN2014 
26419-1819147 AURELIO RICARDO BATU MAICA 02JAN2013 31JAN2013 
26419-1796124 BEN HUR COSTA DE CAMPOS 02JAN2013 10FEV2013 
26419-1796124 BEN HUR COSTA DE CAMPOS 22JUL2013 26JUL2013 
26419-1761366 CLAUDIA REGINA COSTA PACHECO 02JAN2013 10FEV2013 
26419-1761366 CLAUDIA REGINA COSTA PACHECO 22JUL2013 26JUL2013 
26419-1823868 DAIANE TOIGO TRENTIN 18JAN2013 11FEV2013 
26419-1823868 DAIANE TOIGO TRENTIN 15JUL2013 19JUL2013 
26419-1823868 DAIANE TOIGO TRENTIN 02JAN2014 16JAN2014 
26419-1804782 EDIMAR MANICA 21JAN2013 08FEV2013 
26419-1804782 EDIMAR MANICA 22JUL2013 26JUL2013 
26419-1804782 EDIMAR MANICA 31DEZ2013 20JAN2014 
26419-1818514 EDUARDO FERNANDES ANTUNES 28JAN2013 01FEV2013 
26419-1818514 EDUARDO FERNANDES ANTUNES 02JAN2014 26JAN2014 
26419-1796015 EDUARDO MATOS MONTEZANO 11JAN2013 10FEV2013 
26419-1796015 EDUARDO MATOS MONTEZANO 22JUL2013 26JUL2013 
26419-1796015 EDUARDO MATOS MONTEZANO 02JAN2014 10JAN2014 
26419-1823897 FERNANDA SCHNEIDER 02JAN2013 10FEV2013 
26419-1823897 FERNANDA SCHNEIDER 22JUL2013 26JUL2013 
26419-1796099 FERNANDO BELTRAME 21JAN2013 08FEV2013 
26419-1796099 FERNANDO BELTRAME 22JUL2013 26JUL2013 
26419-1796099 FERNANDO BELTRAME 31DEZ2013 20JAN2014 
26419-1703241 FRANCISCA BRUM TOLIO 02JAN2013 10FEV2013 
26419-1703241 FRANCISCA BRUM TOLIO 22JUL2013 26JUL2013 
26419-1759468 JULIANO ELESBAO RATHKE 02JAN2013 10FEV2013 
26419-1759468 JULIANO ELESBAO RATHKE 22JUL2013 26JUL2013 
26419-1680548 LAURA GOTLEIB DA ROSA 21JAN2013 08FEV2013 
26419-1680548 LAURA GOTLEIB DA ROSA 03JUN2013 13JUN2013 
26419-1827893 LEANDRO ROGGIA 21JAN2013 08FEV2013 
26419-1827893 LEANDRO ROGGIA 22JUL2013 26JUL2013 
26419-1827893 LEANDRO ROGGIA 31DEZ2013 20JAN2014 
26419-1828061 LISIANE CEZAR DE OLIVEIRA 02JAN2013 10FEV2013 
26419-1828061 LISIANE CEZAR DE OLIVEIRA 22JUL2013 26JUL2013 
26419-1781712 LUIS CLAUDIO GUBERT 19JAN2013 10FEV2013 
26419-1781712 LUIS CLAUDIO GUBERT 22JUL2013 26JUL2013 
26419-1781712 LUIS CLAUDIO GUBERT 02JAN2014 18JAN2014 
26419-1645508 MARCOS PAULO LUDWIG 21JAN2013 08FEV2013 
26419-1645508 MARCOS PAULO LUDWIG 22JUL2013 26JUL2013 
26419-1645508 MARCOS PAULO LUDWIG 02JAN2014 22JAN2014 
26419-2104561 MIGACIR TRINDADE DUARTE FLORES 21JAN2013 09FEV2013 
