1 
 
 
 
 
 
 
 
 
Boletim de Serviço 
Novembro 
2012 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Campus Avançado Ibirubá 
Rua Nelsi Ribas Fritsch, 1111 - Bairro Esperança 
CEP: 98200-000 – Ibirubá – RS 
Comissão Responsável pela elaboração, impressão e distribuição: 
Marcelo Lima Calixto 
Tiago de Paulo Leão 
 
 
2 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO – MEC 
Ministro 
Aloízio Mercadante Oliva 
 
SECRETARIA DE EDUCAÇÃO PROFISSIONAL E TECNOLÓGICA – SETEC 
Secretário 
Marco Antônio de Oliveira 
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO 
GRANDE DO SUL 
Reitora 
Claudia Schiedeck Soares de Souza 
 
CAMPUS AVANÇADO IBIRUBÁ 
Diretora-Geral 
Migacir Trindade Duarte Flôres 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
3 
 
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA  
DO RIO GRANDE DO SUL – CAMPUS AVANÇADO IBIRUBÁ 
 
 
 
 
ANIVERSARIANTES 
 
 
 
  
 
4 
 
 
 
 
 
 
 
 
 
 
 
Boletim de Serviço Nº 05/2012 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA  
DO RIO GRANDE DO SUL – CAMPUS AVANÇADO IBIRUBÁ 
 
 
 
 
 
5 
 
 
Sumário 
I – PORTARIAS.........................................................................................................................6 
II – ORDENS DE SERVIÇO....................................................................................................11 
III – CONCESSÃO DE DIÁRIAS...........................................................................................11 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Boletim de Serviço Nº 05/2012 
I – PORTARIAS 
 
PORTARIA Nº 117, DE 01 DE NOVEMBRO DE 2012 
 
  A DIRETORA GERAL “PRO TEMPORE” DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS 
AVANÇADO IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 285, 
de 01 de abril de 2011, publicado no DOU em 04/04/2011, RESOLVE: 
 
6 
 
 
CONSTITUIR a Subcomissão Permanente de Avaliação – SPA do IFRS Campus 
Avançado de Ibirubá, composta pelos membros abaixo-relacionados: 
 
SEGMENTO DOCENTE: SUZANA FERREIRA DA ROSA, Professora do Ensino Básico, 
Técnico e Tecnológico – Titular; EDUARDO MATOS MONTEZANO, Professor do Ensino 
Básico, Técnico e Tecnológico – Suplente 
 
SEGMENTO TÉCNICO ADMINISTRATIVO: AURÉLIO RICARDO BATU MAICA, 
Técnico em TI – Titular; ALINE TERRA SILVEIRA, Bibliotecária – Suplente 
 
SEGMENTO DISCENTE: RAFAEL ROTTA – Titular; ELVIS GÜNTZEL RUPPENTHAL 
– Suplente 
 
REPRESENTANTES DA COMUNIDADE EXTERNA: JAQUELINE BRIGNONI 
WINSCH – Titular; JOICE BINSFELD – Suplente        
  
Migacir Trindade Duarte Flôres 
Diretora Geral “Pro Tempore” 
Port. N° 285/2011 
PORTARIAS  DE 13 DE NOVEMBRO DE 2012. 
 
 
  A DIRETORA GERAL “PRO TEMPORE” DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS 
AVANÇADO IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 285, 
de 01 de abril de 2011, publicado no DOU em 04/04/2011, RESOLVE: 
 
Nº 118 - DISPENSAR, o servidor, MARCELO LIMA CALIXTO, SIAPE nº 1847670, da 
Função de COORDENADOR DE ENSINO, Código FG-0001, a partir de 13.11.2012 
 
Nº 119 - DISPENSAR, o servidor, JOVANI JOSÉ ALBERTI, SIAPE nº 995602, da Função 
de COORDENADOR DE ADMINISTRAÇÃO E PLANEJAMENTO, Código FG-0001, a 
partir de 13.11.2012 
 
Nº 120 - DISPENSAR, a servidora, RAQUEL LORENSINI ALBERTI, SIAPE nº 1306033, 
da Função de COORDENADORA DE EXTENSÃO, Código FG-0002, a partir de 13.11.2012 
 
Nº 121 - DISPENSAR, o servidor, LUIS CLÁUDIO GUBERT, SIAPE nº 1781712, da 
Função de COORDENADOR DE DESENVOLVIMENTO INSTITUCIONAL, Código FG-
0002, a partir de 13.11.2012 
 
Nº 122 - DISPENSAR, o servidor, BEN-HUR COSTA DE CAMPOS, SIAPE nº 1796124, da 
Função de COORDENADOR DE PESQUISA E INOVAÇÃO, Código FG-0002, a partir de 
13.11.2012 
7 
 
 
 
Nº 123 - DISPENSAR, o servidor, ELIÉZER JOSÉ PEGORARO, SIAPE nº 1686749, da 
Função de COORDENADOR DE PRODUÇÃO AGROPECUÁRIA, Código FG-0004, a 
partir de 13.11.2012 
 
Nº 124 - DISPENSAR, o servidor, TIAGO DE PAULO LEÃO, SIAPE nº 1828218, da 
Função de COORDENADOR DE GESTÃO DE PESSOAS, Código FG-0005, a partir de 
13.11.2012 
 
 
 
Nº 125 - DESIGNAR, o servidor, MARCELO LIMA CALIXTO, SIAPE nº 1847670, para o 
Cargo de DIRETOR DE ENSINO, Código CD-0004, a partir de 13.11.2012 
 
Nº 126 - DESIGNAR, o servidor, JOVANI JOSÉ ALBERTI, SIAPE nº 995602, para o Cargo 
de DIRETOR DE ADMINISTRAÇÃO E PLANEJAMENTO, Código CD-0004, a partir de 
13.11.2012 
 
Nº 127 - DESIGNAR, a servidora, RAQUEL LORENSINI ALBERTI, SIAPE nº 1306033da 
Função de COORDENADOR DE EXTENSÃO, Código FG-0001, a partir de 13.11.2012 
 
Nº 128 - DESIGNAR, o servidor, LUIS CLÁUDIO GUBERT, SIAPE nº 1781712 para 
Função de COORDENADOR DE DESENVOLVIMENTO INSTITUCIONAL, Código FG-
0001, a partir de 13.11.2012 
 
Nº 129 - DESIGNAR, o servidor, BEN-HUR COSTA DE CAMPOS, SIAPE nº 1796124 da 
Função de COORDENADOR DE PESQUISA E INOVAÇÃO, Código FG-0001, a partir de 
13.11.2012 
 
Nº 130 - DESIGNAR, o servidor, ELIÉZER JOSÉ PEGORARO, SIAPE nº 1686749 para 
Função de COORDENADOR DE PRODUÇÃO AGROPECUÁRIA, Código FG-0001, a 
partir de 13.11.2012 
 
Nº 131 - DESIGNAR, a servidora, MARIA INÊS SIMON, SIAPE nº 1868081, para Função 
de COORDENADORA DE ASSISTÊNCIA AO EDUCANDO, Código FG-0002, a partir de 
13.11.2012 
 
Nº 132 - DESIGNAR, o servidor, MILTON JOSÉ BUSNELLO, SIAPE nº1893291, para 
Função de COORDENADOR DE INFRAESTRUTURA, Código FG-0002, a partir de 
13.11.2012 
 
Nº 133 - DESIGNAR, a servidora, MAGÁLI TERESINHA DA SILVA, SIAPE nº 1835765, 
para Função de COORDENADORA DE ADMINISTRAÇÃO E FINANÇAS, Código FG-
0002, a partir de 13.11.2012 
 
8 
 
 
                       
  
 
 
Migacir Trindade Duarte Flôres 
Diretora Geral “Pro Tempore” 
Port. n° 285/2011
9 
 
 
 
 
PORTARIAS DE 21 DE NOVEMBRO DE 2012 
 
 A Diretora Geral “Pro Tempore” do Campus Avançado de Ibirubá, do Instituto 
Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, no uso de suas atribuições 
legais subdelegadas pela Portaria nº 285, de 01 de abril de 2011, publicado no DOU em 04 de 
abril de 2011, RESOLVE: 
 
Nº 134 - DISPENSAR, o servidor, MARCOS PAULO LUDWIG, SIAPE nº 1645508, da 
Função de COORDENADOR DE CURSO, TECNOLOGIA EM PRODUÇÃO DE GRÃOS, 
Código FG-0005 
 
Nº 135 - DISPENSAR, a servidora, MONICA GIACOMINI, SIAPE nº 1827598, da Função 
de COORDENADOR DE CURSO, LICENCIATURA EM MATEMÁTICA, Código FG-
0006 
 
Migacir Trindade Duarte Flôres 
Diretora Geral “Pro Tempore” 
Port. N° 285/2011 
PORTARIA Nº 136, DE 27 DE NOVEMBRO DE 2012. 
 
  A DIRETORA GERAL “PRO TEMPORE” DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS 
AVANÇADO IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 552, 
de 21 de novembro de 2012, publicada no DOU em 22 de novembro de 2012, no uso de suas 
atribuições, RESOLVE: 
 
  
 Designar, o servidor JOVANI JOSÉ ALBERTI, Coordenador de Administração e 
Planejamento, matrícula SIAPE nº 995602, para responder interinamente pela Direção Geral 
do Campus Avançado de Ibirubá, nos dias 28/11/2012 e 29/11/2012 
 
 
 
 
 
 
 
Profª. Migacir Trindade Duarte Flôres 
                                             Diretora Geral “Pro Tempore” 
Port. N° 552/2012 
 
 
10 
 
 
PORTARIA Nº 137, DE 27 DE NOVEMBRO DE 2012. 
 
  A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS 
AVANÇADO IBIRUBÁ, no uso de suas atribuições legais subdelegadas pela Portaria nº 552, 
de 21 de novembro de 2012, publicado no DOU de 22 de novembro de 2012, e de acordo com 
o Edital Institucional de Extensão nº 98, de 21/11/2012, publicado no site do IFRS em 
21/11/2012, RESOLVE: 
 
Art. 1º - CONSTITUIR a Comissão de Seleção de Bolsistas, para a função de Apoio às 
Atividades Acadêmicas e Administrativas dos Cursos do Programa Nacional de Acesso ao 
Ensino Técnico e Emprego – PRONATEC, composta pelos servidores abaixo-relacionados: 
- RAQUEL LORENSINI ALBERTI, Professora do Ensino Básico, Técnico e Tecnológico 
- ANDRÉ RICARDO DIERINGS, Professor do Ensino Básico, Técnico e Tecnológico  
- EDUARDO GIROTTO, Professor do Ensino Básico, Técnico e Tecnológico 
Art. 2º - A presidência da Comissão será exercida pelo servidor EDUARDO GIROTTO; 
Art. 3º - As atribuições da Comissão estão previstas no Edital Institucional de Extensão nº 
98/2012. 
Art. 4º - As atividades da Comissão deverão ser realizadas durante a jornada de trabalho 
regular de seus membros na Instituição. 
 
                       
  
 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral "Pro tempore" 
Port. n° 552/2012
11 
 
 
 
 
PORTARIAS DE 30 DE NOVEMBRO DE 2012 
 
 A Diretora Geral “Pro Tempore” do Campus Avançado de Ibirubá, do Instituto 
Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul, no uso de suas atribuições 
legais subdelegadas pela Portaria nº 552, de 21 de novembro de 2012, publicado no DOU em 
22 de novembro de 2012, RESOLVE: 
 
Nº 138 - Designar, a servidora, DAIANE TOIGO TRENTIN, SIAPE nº 1823868, da Função 
de Coordenadora de Ensino, Código FG-0002 
 
 Nº 139 - Designar, a servidora, MONICA GIACOMINI, SIAPE nº 1827598, da Função de 
Coordenadora dos Cursos Superiores, Código FG-0005 
 
 Nº 140 - Designar, a servidora, LAURA GOTLEIB DA ROSA, SIAPE nº 1680548, da 
Função de Coordenadora de Tecnologia da Informação, Código FG-0002 
 
Migacir Trindade Duarte Flôres 
Diretora Geral “Pro Tempore” 
Portaria nº 552/2012 
 
 
 
 
 
 
 
 
 
 
 
 
 
      
II - ORDENS DE SERVIÇO 
 
 
 
 
 
 
12 
 
 
 
III – CONCESSÃO DE DIÁRIAS 
 
 
Nome: IVAN PAULO CANAL  
Função:  
Período da 
Viagem: 
05/11/2012    à   06/11/2012  
Destino: Ibirubá / Porto Alegre / Ibirubá  
Número da 
Requisição: 
002769/12  
Valor das Diárias 
(R$):  
310,11  
Objetivo: Nacional - Convocação  
Descrição: 
Convocação para Reunião de Trabalho da Comissão Permanente de Pessoal 
Docente (GT-CPPD), no Câmpus Canoas.  
 
 
Nome: DAIANE TOIGO TRENTIN  
Função:  
Período da Viagem: 12/11/2012    à   13/11/2012  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 002848/12  
Valor das Diárias (R$):  237,86  
Objetivo: Nacional - A Serviço  
Descrição: Encontro PROEN - Avaliação de Cursos Superiores.  
 
 
 
Nome: MONICA GIACOMINI  
Função:  
Período da 
Viagem: 
05/11/2012    à   06/11/2012  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
002861/12  
Valor das Diárias 
(R$):  
237,86  
13 
 
 
Objetivo: Nacional - A Serviço  
Descrição: 
Reunião das Licenciaturas em Matemática (05/11/12) e Reunião para Discussão 
das Regulamentações Extraordinárias do IFRS (06/11/12).  
 
 
Nome: MIGACIR TRINDADE DUARTE FLORES  
Função: CD-0003  
Período da 
Viagem: 
06/11/2012    à   07/11/2012  
Destino: Ibirubá / Porto Alegre / Ibirubá  
Número da 
Requisição: 
002886/12  
Valor das 
Diárias (R$):  
353,06  
Objetivo: Nacional - Convocação  
Descrição: 
Atender a convocação para participar da Reunião do CONSUP no Câmpus 
Restinga e do Colégio de Dirigentes juntamente com a COMTI no Câmpus POA.  
 
 
Nome: MARCOS PAULO LUDWIG  
Função:  
Período da Viagem: 06/11/2012    à   06/11/2012  
Destino: Ibirubá / Porto Alegre / Ibirubá  
Número da Requisição: 002900/12  
Valor das Diárias (R$):  92,38  
Objetivo: Nacional - Convocação  
Descrição: Atender a convocação para participar da Reunião do CONSUP.  
 
 
Nome: RAMONE TRAMONTINI  
Função:  
Período da 
Viagem: 
05/11/2012    à   06/11/2012  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
002925/12  
Valor das Diárias 
(R$):  
218,55  
Objetivo: Nacional - A Serviço  
14 
 
 
Descrição: 
Reunião das Licenciaturas em Matemática (05/11/12) e visita ao Laboratório de 
Matemática, PIBIT e PET do Câmpus Bento Gonçalves (06/11/12).  
 
 
Nome: CLAUDIOMIR FEUSTLER RODRIGUES DE SIQUEIRA  
Função:  
Período da 
Viagem: 
15/11/2012    à   18/11/2012  
Destino: Ibirubá / Sertão / Ibirubá  
Número da 
Requisição: 
002944/12  
Valor das 
Diárias 
(R$):  
605,68  
Objetivo: Nacional - Encontro/Seminário  
Descrição: 
A fim de estreitar e confirmar as boas relações entre os Câmpus Ibirubá e Sertão a 
Diretora Geral “Pro tempore” do IFRS – Câmpus Ibirubá Migacir Trindade Duarte 
Flôres confirmou a participação dos alunos acompanhados de professores no XXI 
Encontro Cultural e Tradicionalista dos Institutos Federais da Região Sul do Brasil e 
no II Festival da Canção Cultural e Tradicionalista (FECULT). Justifica-se assim a 
participação desse professor, pois irá acompanhar os alunos da Instituição nesses 04 
dias. Ficando como responsável destes, bem como estará complementando sua 
formação cultural, ao mesmo tempo em que estará representando e divulgando as 
boas ações dessa instituição.  
 
 
 
Nome: JOVANI JOSE ALBERTI  
Função:  
Período da Viagem: 06/11/2012    à   06/11/2012  
Destino: Ibirubá / Porto Alegre / Ibirubá  
Número da Requisição: 002948/12  
Valor das Diárias (R$):  92,38  
Objetivo: Nacional - Convocação  
Descrição: Convocação para a Reunião do CONSUP.  
 
 
Nome: TIAGO DE PAULO LEAO  
Função:  
Período da Viagem: 06/11/2012    à   06/11/2012  
15 
 
 
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 002951/12  
Valor das Diárias (R$):  74,68  
Objetivo: Nacional - A Serviço  
Descrição: Convocação para debate sobre minutas junto ao Campus Bento Gonçalves  
 
 
Nome: FRANCISCA BRUM TOLIO  
Função:  
Período da 
Viagem: 
15/11/2012    à   18/11/2012  
Destino: Ibirubá / Sertão / Ibirubá  
Número da 
Requisição: 
002972/12  
Valor das 
Diárias (R$):  
605,68  
Objetivo: Nacional - Encontro/Seminário  
Descrição: 
Participação juntamente com os alunos no XXI Encontro Cultural e Tradicionalista 
dos Institutos Federais da Região Sul do Brasil e no II Festival da Canção e Cultura 
Tradicionalista, a realizar-se no IFRS - Câmpus Sertão.  
 
 
 
Nome: MAGDA DA SILVA PEREIRA  
Função:  
Período da 
Viagem: 
15/11/2012    à   18/11/2012  
Destino: Ibirubá / Sertão / Ibirubá  
Número da 
Requisição: 
003030/12  
Valor das 
Diárias 
(R$):  
580,55  
Objetivo: Nacional - Encontro/Seminário  
Descrição: 
A fim de estreitar e confirmar as boas relações entre os Câmpus Ibirubá e Sertão a 
Diretora Geral “Pro tempore” do IFRS – Câmpus Ibirubá Migacir Trindade Duarte 
Flôres confirmou a participação dos alunos acompanhados de professores no XXI 
Encontro Cultural e Tradicionalista dos Institutos Federais da Região Sul do Brasil e 
no II Festival da Canção Cultural e Tradicionalista (FECULT). Justifica-se assim a 
participação desse professor, pois irá acompanhar os alunos da Instituição nesses 04 
16 
 
 
dias. Ficando como responsável destes, bem como estará complementando sua 
formação cultural, ao mesmo tempo em que estará representando e divulgando as 
boas ações dessa instituição.  
 
 
 
Nome: LAURA GOTLEIB DA ROSA  
Função:  
Período da 
Viagem: 
06/11/2012    à   07/11/2012  
Destino: Ibirubá / Porto Alegre / Ibirubá  
Número da 
Requisição: 
003028/12  
Valor das 
Diárias (R$):  
264,26  
Objetivo: Nacional - A Serviço  
Descrição: 
Participar como ouvinte da reunião do Consup no Campus Restinga e participar 
da reunião do CD e TI para discutir os rumos da TI no IFRS no Campus Porto 
Alegre  
 
 
 
Nome: MARCELO LIMA CALIXTO  
Função:  
Período da Viagem: 20/11/2012    à   21/11/2012  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 003059/12  
Valor das Diárias (R$):  237,86  
Objetivo: Nacional - Convocação  
Descrição: Convocação COEN.  
 
 
Nome: MIGACIR TRINDADE DUARTE FLORES  
Função: CD-0003  
Período da 
Viagem: 
23/11/2012    à   23/11/2012  
Destino: Ibirubá / Porto Alegre / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
003124/12  
17 
 
 
Valor das 
Diárias (R$):  
91,93  
Objetivo: Nacional - A Serviço  
Descrição: 
Deixar o Professor Eduardo Girotto em POA, para participar de reunião para tratar 
do PRONACAMPO; participar da solenidade de posse dos novos técnicos 
administrativos do IFRS e trazer da reitoria para Ibirubá os porta-banners para a 
Mostra de Pesquisa e Extensão.  
 
 
Nome: MAURICIO LOPES LIMA  
Função:  
Período da Viagem: 27/11/2012    à   28/11/2012  
Destino: Ibirubá / Chapecó / Ibirubá  
Número da 
Requisição: 
003153/12  
Valor das Diárias 
(R$):  
237,86  
Objetivo: Nacional - A Serviço  
Descrição: 
Participação em evento acadêmico com apresentação de trabalho. 
Capacitação para o NEABI  
 
 
 
Nome: SUZANA FERREIRA DA ROSA  
Função:  
Período da Viagem: 29/11/2012    à   29/11/2012  
Destino: Ibirubá / Porto Alegre / Ibirubá  
Número da Requisição: 003317/12  
Valor das Diárias (R$):  92,38  
Objetivo: Nacional - A Serviço  
Descrição: Reunião de Subcomissão Própria de Avaliação institucional  
 
 
Nome: MILTON JOSE BUSNELLO  
Função:  
Período da Viagem: 26/11/2012    à   26/11/2012  
Destino: Ibirubá / Porto Alegre / Ibirubá  
Número da 
Requisição: 
003263/12  
18 
 
 
Valor das Diárias 
(R$):  
92,38  
Objetivo: Nacional - A Serviço  
Descrição: 
Transportar alunos da escola para a Mostra de Pesquisa e Extensão em Porto 
Alegre.  
 
 
Nome: MAURICIO LOPES LIMA  
Função:  
Período da Viagem: 30/11/2012    à   30/11/2012  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 003264/12  
Valor das Diárias (R$):  74,68  
Objetivo: Nacional - Convocação  
Descrição: Convocação para participar de reunião dos NEABIs na reitoria.  
 
 
Nome: EDUARDO GIROTTO  
Função:  
Período da Viagem: 29/11/2012    à   30/11/2012  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 003266/12  
Valor das Diárias (R$):  237,86  
Objetivo: Nacional - Convocação  
Descrição: Convocação para reunião referente ao PRONATEC.  
 
 
