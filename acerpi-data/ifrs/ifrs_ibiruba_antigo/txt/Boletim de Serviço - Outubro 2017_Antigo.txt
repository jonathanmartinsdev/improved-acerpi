 Ministério da Educação
Secretaria de Educação Profissional e Tecnológica
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
Câmpus Ibirubá
Boletim de Serviço 
Outubro/2017
Publicado em 16 de novembro de 2017.
Comissão Responsável pela elaboração, impressão e distribuição:
Cristiane Brauner
Eduardo Fernandes Antunes
Julia Caroline Goulart Blank
Jerry Cenise Marques
Boletim de Serviço destinado à publicação dos atos administrativos do Câmpus Ibirubá do Instituto
Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
1
Ministério da Educação
Secretaria de Educação Profissional e Tecnológica
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
Câmpus Ibirubá
MINISTÉRIO DA EDUCAÇÃO – MEC
Ministro
José Mendonça Bezerra Filho
SECRETARIA DE EDUCAÇÃO PROFISSIONAL E TECNOLÓGICA – SETEC
Secretário 
Marcos Antônio Viegas Filho 
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE
DO SUL
Reitor
Osvaldo Casares Pinto
 
CÂMPUS IBIRUBÁ
Diretora-Geral Pro tempore
Migacir Trindade Duarte Flôres 
2
Boletim de Serviço destinado à publicação dos atos administrativos do Câmpus Ibirubá do 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
ANIVERSARIANTES
JAN FEV        MAR 
02/01 - Alice
02/01 – Maiquel
08/01 – Edimilson
10/01 – Vanessa  Castro
11/01 – Edimar
11/01 – Sônia
14/01 – Rodrigo  Gama
16/01 – Lysy
17/01 – Sandra Meinem
19/01 - Angéli
19/01 - Marcele
20/01 – Dona Ana
23/01 – Ben-Hur
28/01 - Janete
30/01 – Eliezer
30/01 – Juliano  Martins
04/02- Fernanda
09/02 – Gabriel  De Franceschi
13/02 - Miriam
18/02 – Ana Dionéia
20/02 - Elizandro
22/02 – Fabiane
23/02 – Eduardo  Camargo
24/02 – Fernanda Gall
26/02 – Ronaldo Tremarin
27/02 – Gubert
28/02– Girotto
1º/03 - Roberta
06/03 – Anderson
07/03 - Helder
14/03 – Júlia
19/03 - Gilnei
20/03 – Laura 
23/03 – Flávio
        ABR          MAI             JUN
06/04 – Paula
08/04 – Suzana
09/04 – Sabrine
09/04  - Silmar
13/ 04 – Ramone
13/04 – Juceli
17/04 – Gustavo Paulus
19/04 – Karina Doninelli
22/04– André Marcondes
23/04 – Felipe  Brum
26/04 – Dona Marli
26/04 – Daniel  Gonçalves
26/04 – Valmir
27/04 - Cléber
30/04 – Senaldo  Cappi
 1º/05 - Marsoé
03/05 – Ana Paula
05/05 - Lucilene
08/05 – Dionéia
09/05 – Carina Zonim
09/05 – Fabrício 
09/05 – Renata
11/05 – Lilian
12/05 - Miridiane
13/05 – Juliano  Ratke
16/05  - Silvani
19/05 - Edvaldo
19/05 - Marcine
23/05 – Cristiane
26/05 – Jardel 
29/05  - Vanussa 
30/05– Raquel  Alberti
30/05 – Sandra Peringer
30/05 – Tiago 
13/06 – Lucas de  Andrade
15/06 – Aline 
15/06 – Moisés  Hoffmann
15/06 - Roger
19/06 – Adilson
23/06 - Edilisia
27/06 – Magáli
27/06 – Eduardo  Antunes
        JUL         AGO SET
3
01/07 – Grazi
04/07 - Tarsila
10/07 -  Heilande
13/07 – Júlio Cesar
16/07 – Paulo  Ricardo
22/07 - Milton
26/07 – Andréia
27/07 – Lucas  Navarini
 02/08 – Seu Antônio
 04/08 – Gringa
06/08 – Andrey
21/08 - Jason
22/08– André Dierings
27/08 – Adriano
26/08 – Fábio 
31/08 – Dona  Alzira
04/09 – Alair
05/09 - Rodrigo Ludwig
06/09 – Gabriel Begute
08/09 – Mônica  Giacomini
09/09 - Iuri
09/09 – Mônica  dos  Santos
10/09 – Sérgio  Diemer
11/09 – Maria  Inês
13/09 - Matias
14/09 – André  Luis 
14/09 -  Carina Tonieto
16/09 – Tássia
19/09 – Moisés  Cordeiro
20/09 – Cimara
20/09 -  Daniel  P.
21/09 - Dionei
27/09 – Maurício Lopes
30/09 - Bárbara
OUT NOV DEZ
1º/10 – Eduarda
03/10 - Ângela
03/10 – Raquel  Cardoso
04/10 – Felipe  Leite
05/10 – Rafael  Scapini
12/10 – Carlão
13/10 - Lisandro
16/10 - Élvio
24/10 – Daniel  Uhry
02/11 - Nei
06/11 – Marcos  Paulo
09/11- Alessandra
10/11 – Luiz F. Kopper
13/11 – Daniel  Rockenbach 
15/11 – Jovani
18/11 – Eduardo Montezano
18/11 - Ivo
20/11 – Migacir
24/11 – Ronaldo  Serpa
25/11  - Viviane
07/12 – Jerry
07/12 – Lúcia
12/12  - Rafael  Venturini
16/12 – Bruno
20/12 - Odair
22/12 – Maurício Castro
23/12 - Zamith
4
Boletim de Serviço Nº 10/2017
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO 
SUL – CÂMPUS IBIRUBÁ
Sumário
I - PORTARIAS..............................................................................................................06
II – ORDENS DE SERVIÇO....................................................................................... ..22
III – CONCESSÃO DE DIÁRIAS E PASSAGENS......................................................23
IV – FÉRIAS...................................................................................................................28
V – AFASTAMENTOS/LICENÇAS..............................................................................28
VI – ATESTADOS MÉDICOS........................................................................................29
VII – SUBSTITUIÇÃO DE FUNÇÃO...........................................................................29
5
Boletim de Serviço Nº 10/2017
I – PORTARIAS 
PORTARIA DO DIA 02 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº  181  –  AUTORIZAR  a  servidora  SUZANA FERREIRA DA ROSA,  matrícula  SIAPE  N°
2917801, portadora da carteira nacional de habilitação n° 1372335910, categoria “B”, registro nº
04038934337, expedida pelo Departamento Nacional de Trânsito do Rio Grande do Sul, a dirigir
veículos desta instituição de ensino, de acordo com o disposto no art. 1º da lei n° 9.327/96.
Art. 1º da lei 9.327/96: Os servidores públicos federais, dos órgãos e entidades integrantes da
Administração  Pública  Federal  direta,  autárquica  e  fundacional,  no  interesse  do  serviço  e  no
exercício de suas próprias atribuições, quando houver insuficiência de servidores ocupantes do cargo
de Motorista Oficial, poderão dirigir veículos oficiais, de transporte individual de passageiros, desde
que  possuidores  da  Carteira  Nacional  de  Habilitação  e  devidamente  autorizados  pelo  dirigente
máximo do órgão ou entidade a que pertençam.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
6
PORTARIA DO DIA 02 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 182 – DESIGNAR o servidor MARCOS ROBERTO JOST, Assistente em Administração, SIAPE
nº 2307891, para responder como substituto da Coordenadoria de Extensão, código FG-001, do IFRS
– Campus Ibirubá, na ausência do titular da função.
Revogar a portaria nº 34/2015, referente ao mesmo assunto.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 03 DE OUTUBRO DE 2017.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº  183-Alterar  o  posicionamento  da  servidora  SABRINE  DE  OLIVEIRA,  matrícula  SIAPE
n°2169684,Tradutor e Interprete de Linguagem de Sinais, CLASSE: D: Padrão: 202 para CLASSE D
Padrão: 203, por motivo de PROGRESSÃO POR MÉRITO, a partir de 29.09.2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1.849/2016
7
PORTARIA DO DIA 03 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº  184  –  DESIGNAR os  servidores  abaixo  relacionados  para  comporem  a  Comissão  de
Organização e Acompanhamento(COA) do processo de levantamento de necessidades de capacitação
do IFRS-Campus Ibirubá:
REPRESENTANTE INDICADO CPPD
André Luis Demichei SIAPE Nº 2027018
REPRESENTANTE INDICADO CIS
Sonia Margarete Souza de Souza SIAPE Nº 2333087
REPRESENTANTE INDICADO CGP
Patrícia Harter Sampaio Stasiak
(Presidente)
SIAPE Nº 2337781
Revogar a Portaria nº 57/2017 referente ao mesmo assunto.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 04 DE OUTUBRO DE 2017.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
8
Nº 185-Alterar  o  posicionamento  do   servidor  GUSTAVO BATHU PAULUS,  matrícula  SIAPE
n°2168690,Técnico  de  Tecnologia  da  Informação,  CLASSE:  D:  Padrão:  202  para  CLASSE  D
Padrão: 203, por motivo de PROGRESSÃO POR MÉRITO, a partir de 23.09.2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1.849/2016
PORTARIA DO DIA 04 DE OUTUBRO DE 2017.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº  186-Alterar  o  posicionamento  do   servidor  JASON SCALCO PILOTI,  matrícula  SIAPE n°
2296380,Técnico  em Audiovisual,  CLASSE:  D:  Padrão:  101 para  CLASSE D Padrão:  102,  por
motivo de PROGRESSÃO POR MÉRITO, a partir de 29.09.2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1.849/2016
9
PORTARIA DO DIA 06 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 187 – DESIGNAR os servidores LILIAN CLÁUDIA XAVIER CORDEIRO, Professora EBTT,
Matrícula  SIAPE  nº  2022705,  MARCELE  NEUTZLING  RICKES,  Técnica  em  Assuntos
Educacionais,  Matrícula  SIAPE nº  1993197,  MAURICIO LOPES  LIMA,  Técnico  em Assuntos
Educacionais,  Matrícula  SIAPE  nº  17700350,  ANDRÉ  LUIS  DEMICHEI,  Professor  EBTT,
Matrícula SIAPE nº 2027018, ANA PAULA DE ALMEIDA, Assistente Social, Matrícula SIAPE nº
1800168, EDILSON PEREIRA BRITO, Professor EBTT, Matrícula SIAPE nº 2396608, FELIPE
LEITE  SILVA,  Professor  EBTT,  Matrícula  SIAPE  nº  1999902,  EDUARDO  FERNANDES
SARTURI,  Professor  EBTT,  Matrícula  SIAPE  nº  1051137,  MARSOÉ  CRISTINA DAHLKE,
Professora EBTT, Matrícula SIAPE nº 1972400 e ANA PAULA DA CONCEIÇÃO, Assistente de
Alunos, Matrícula SIAPE nº 2398415, para sob a Coordenação da primeira comporem o Núcleo de
Estudos Afro-brasileiros e Indígenas (NEABIS) do IFRS – Campus Ibirubá. 
Revogar a portaria 132/2017 referente ao mesmo assunto.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
10
PORTARIA N °188 DO DIA 06 DE OUTUBRO DE 2017.
Homologa estabilidade de servidor
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Art.1 DECLARAR estável a servidora SABRINE DE OLIVEIRA, Tradutor Interprete de L
inguagens de Sinais , Matrícula Siape nº  2169684, em conformidade com o artigo 41, da
Constituição Federal, com a redação que lhe foi dada pelo artigo 6º da Emenda Constitucional nº 19,
de 04 de junho de 1998, a partir de 26.09.2017.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 1849/2016
PORTARIA DO DIA 11 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 189 – DESIGNAR os membros do Colegiado do Curso Técnico Subsequente em Eletrotécnica,
ficando como membros a partir desta data os servidores abaixo relacionados:
11
Coordenação:  FLÁVIO  ROBERTO  ANDARA,  Professor  do  Ensino  Básico,  Técnico  e
Tecnológico, Matrícula SIAPE nº 1819928.
Representantes  Docentes:  JULIANO ELESBÃO RATHKE, Professor  do Ensino Básico,
Técnico e Tecnológico, Matrícula SIAPE nº 1759468, MOISÉS NIVALDO CORDEIRO, Professora
do  Ensino  Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE  nº  2258011,  Bruno  Conti  Franco,
Professora do Ensino Básico, Técnico e Tecnológico,  Matrícula SIAPE nº 1999869 e IVO MAI,
Professor do Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1995140.
Representante  Técnicos-Administrativos:  IURI  GUISSONI  QUAGLIA,  Técnico  em
Laboratório/área Eletromecânica , Matrícula SIAPE nº 2165103.
Representante Discente: GABRIEL AHLERT, Matrícula n° 07100206.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 11 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 190  –  CONSTITUIR a Comissão Permanente de Avaliação – CPA do IFRS Campus Ibirubá,
composta pelos membros abaixo relacionados:
SEGMENTO DOCENTE: 
Titular:  EDIMILSON  ANTÔNIO  BRAVO  PORTO,  Professor  do  Ensino  Básico,  Técnico  e
Tecnológico, SIAPE n° 1904198.
Suplente: JEFFERSON MORAES GAUTERIO, Professor do Ensino Básico Técnico e Tecnológico,
SIAPE n° 2331430.
SEGMENTO TÉCNICO ADMINISTRATIVO: 
Titular: SANDRA MEINEN DA CRUZ Técnico em Laboratório/Área:Química SIAPE n°2230066.
Suplente: JOVANI JOSÉ ALBERTI Técnico em Laboratório/Área: Mecânica SIAPE n° 995602.
12
SEGMENTO DISCENTE:
Titular: TALITA VIEIRA BROCA, Matrícula n° 07170124
Suplente: BERNARDO ROTA, Matrícula n° 07180100
SEGMENTO MEMBRO EXTERNO:
Titular: VALTER KURZ.
Suplente: JAQUELINE BRIGNONI WINSCH.
Revogar a portaria 155/2015, referente ao mesmo assunto.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 16 DE OUTUBRO DE 2017.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº 191-Alterar  o posicionamento do servidor  MILTON JOSE BUSNELLO, matrícula SIAPE n°
1893291,Técnico em Agropecuária, CLASSE: D: Padrão: 404 para CLASSE D Padrão: 405, por
motivo de PROGRESSÃO POR MÉRITO, a partir de 03.10.2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1.849/2016
13
PORTARIA DO DIA 11 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 192 – DESIGNAR a servidora CRISTIANE BRAUNER, Auxiliar em Administração e Diretora
de Administração e Planejamento, matrícula SIAPE nº 1982614, para responder como substituta da
Direção Geral, do IFRS – Campus Ibirubá, na ausência da Titular, servidora MIGACIR TRINDADE
DUARTE FLÔRES, matrícula SIAPE nº 2104561 nos dias 17 e 18 de outubro de 2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 17 DE OUTUBRO DE 2017.
A DIRETORA GERAL  SUBSTITUTA DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL –  CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 192 de 11 de outubro de 2017, publicado no Boletim
de serviço do mês de outubro,  RESOLVE:
Nº  193 -  INTERROMPER as  férias  do  servidor, Mauricio  Cerutti  de  Castro,  Arquiteto  e
Urbanista, SIAPE n° 2178260, no dia 17/10/2017, devendo retornar as férias em 13/11/2017. 
Cristiane Brauner
   Diretora Geral Substituta
                                                                    Port. Nº 192/2017 
14
PORTARIA DO DIA 19 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 194 – DESIGNAR os membros: EDUARDO FERNANDES ANTUNES, Técnico em Tecnologia
da  Informação,  SIAPE  n°  1818514,  LUIS  CLAUDIO  GUBERT,  Professor  do  Ensino  Básico,
Técnico e Tecnológico, SIAPE nº 1781712 e LISIANE REIPS, Matrícula n° 07190011, para sob a
presidência do primeiro comporem a Comissão Eleitoral Responsável pelo processo de escolha dos
representantes do Campus Ibirubá junto ao CONSUP, contemplando o segmento docente, técnico
administrativo e discente.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA Nº 195, DE 20 DE OUTUBRO DE 2017.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de setembro de 2016, RESOLVE:
Art.1º  DESIGNAR os servidores abaixo relacionados para, sob a coordenação do primeiro
constituírem a  Subcomissão de inventário  para a realização do Inventário Físico Anual de bens
móveis do Campus Ibirubá:
Lucas Jardel José Wohlmuth Alves dos Santos, SIAPE 2328168;
Cristiane Brauner, Auxiliar em Administração, SIAPE nº 1982614;
Milton Jose Busnello, SIAPE nº 1893291;
Gilnei José Hefler, SIAPE 2293657; 
Edson Roberto Bueno, SIAPE 2402137; 
15
Jason Scalco Piloti, SIAPE 2296380;
Iuri Guissoni Quaglia, SIAPE 2165103;
 Sonia Margarete Souza de Souza, SIAPE 2333087;
 Roberto Niederaurer, SIAPE 2318608;
 Jovani José Alberti, SIAPE 995602;
Eliezer José Pegoraro, SIAPE 1686749; 
Jonas Anversa, SIAPE 2384881;
 Daniel Amoretti Gonçalves, SIAPE 2264931;
 Juliano Elesbão Rathke, SIAPE 1759468; 
Vanessa Faria de Souza, SIAPE 1217356;
Art.2º  Os servidores  do campus deverão prestar  tempestivamente as  informações  sobre o
acervo patrimonial que forem solicitadas pela Subcomissão de inventário;
Art.3º O prazo para finalização dos trabalhos é 31 de janeiro de 2018. No período de 01 de
novembro de 2017 até 31 de janeiro de 2018 a movimentação de bens permanentes somente será
permitida mediante prévia comunicação a Subcomissão de Inventário do campus.
Art. 4º Esta portaria entra em vigor nesta data.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 1849/2016
PORTARIA  DO DIA 20 DE  OUTUBRO DE 2017.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
16
Nº 196–CONCEDER  ao servidor  HENRIQUE  LINHATI  BITENCOURT, Cargo  Técnico  em
Laboratório,area:Eletromecânica  , matrícula SIAPE n°2350837, ADICIONAL de
INSALUBRIDADE em grau Médio, devido ao Risco Químico, conforme legislação vigente a partir
de 24 de Janeiro de 2017, nos termos do processo nº 23366.000062.2017-24.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1.849/2016
PORTARIA PUBLICADA NO  BS Nº 10 DO MÊS DE  SETEMBRO DE  2017.
PORTARIA  DO DIA 20 DE  OUTUBRO DE 2017.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº 197–CONCEDER ao servidor JOVANI JOSE ALBERTI, Cargo Técnico em Mecânica , matrícula
SIAPE n°995602, ADICIONAL de INSALUBRIDADE em grau Médio, devido ao Risco Químico,
conforme  legislação  vigente  a  partir  de  10 de  Janeiro de  2017,  nos  termos  do  processo  nº
23366.000030.2017-29.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1.849/2016
17
PORTARIA DO DIA 20 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 198  –  DESIGNAR os membros do Colegiado do Curso Técnico Subsequente em Mecânica,
ficando como membros a partir desta data os servidores abaixo relacionados:
Coordenação:  IVO MAI,  Professor  do Ensino Básico,  Técnico  e  Tecnológico,  Matrícula
SIAPE nº 1995140.
Representantes  Docentes:  FABIANO  JORGE  MACEDO,  Professor  do  Ensino  Básico,
Técnico e Tecnológico, Matrícula SIAPE nº 1827813, FLÁVIO ROBERTO ANDARA, Professor do
Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1819928, ANDERSON DE OLIVEIRA
FRAGA, Professor do Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1256854, FELIPE
LEITE SILVA, Professor do Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1999902 e
FELIPE RODRIGUES DE FREITAS NETO,  Professor do Ensino Básico, Técnico e Tecnológico,
Matrícula SIAPE nº 1993451
Representante Técnicos-Administrativos: HENRIQUE LINHATI BITENCOURT, Técnico
em Laboratório/área Eletromecânica , Matrícula SIAPE nº 2350837.
Representante  Discente:  YASMIN  NATÁLIA  DOS  SANTOS(titular),  Matrícula  n°
07130233 e EMANUELLY DE OLIVEIRA (suplente), Matrícula n° 07130238
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
18
PORTARIA DO DIA 24 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 199 – REVOGAR a portaria nº 99/2014 referente a permissão de dirigir veículos desta instituição
de ensino da servidora Mônica Giacomini, matrícula SIAPE.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
PORTARIA DO DIA 24 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº  200  –  REVOGAR  a  portaria  nº  147/2014 referente  a  permissão  de  dirigir veículos  desta
instituição de ensino do servidor Lucas Navarini, matrícula SIAPE nº 2140840.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
19
PORTARIA DO DIA 26 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 201 – REVOGAR a portaria nº 80/2014 referente a permissão de dirigir veículos desta instituição
de ensino do servidor Eduardo Matos Montezano, matrícula SIAPE nº 1796015.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
PORTARIA DO DIA 26 DE OUTUBRO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 202 –  AUTORIZAR o servidor  ANDERSON DE OLIVEIRA FRAGA, matrícula  SIAPE N°
1256854, portador da carteira nacional de habilitação n° 1362908324, categoria “AB”, registro nº
00981944299, expedida pelo Departamento Nacional de Trânsito do Rio Grande do Sul, a dirigir
veículos desta instituição de ensino, de acordo com o disposto no art. 1º da lei n° 9.327/96.
Art. 1º da lei 9.327/96: Os servidores públicos federais, dos órgãos e entidades integrantes da
Administração  Pública  Federal  direta,  autárquica  e  fundacional,  no  interesse  do  serviço  e  no
exercício de suas próprias atribuições, quando houver insuficiência de servidores ocupantes do cargo
de Motorista Oficial, poderão dirigir veículos oficiais, de transporte individual de passageiros, desde
20
que  possuidores  da  Carteira  Nacional  de  Habilitação  e  devidamente  autorizados  pelo  dirigente
máximo do órgão ou entidade a que pertençam.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 27 DE OUTUBRO DE 2017.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº 203 –  CONCEDER a liberação de 40% da carga horária da jornada de trabalho semanal  para
participação em ação de qualificação do curso de Graduação, para a servidora DANIELI OPPELT
NICOLINI,  Assistente  de  Alunos,  matrícula  SIAPE  n°2331826,  com  fundamento  na  Instrução
Normativa nº 06 de 11/05/2015, e nos termos do Processo n° 23366.000432.2017-23, no período de
16/10/2017 a 19/12/2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
21
II – ORDENS DE SERVIÇO
ORDEM DE SERVIÇO Nº 10/2017
O Instituto Federal de Educação,  Ciência  e  Tecnologia do Rio Grande do Sul –  Campus
Ibirubá, através desta, AUTORIZA a retomada da execução dos serviços da Construção do Prédio de
Laboratórios  do  Campus  Ibirubá,  objeto  do  contrato  n.º  132/2014,  firmado  com  a  empresa  PP
ENGENHARIA E  CONSTRUÇÕES  LTDA,  CNPJ  n.º  72.473.275/0001-53,  tão  somente  para
atender as ações corretivas apontadas na notificação anexa a este documento.  
Fica  resguardado  o  direito  da  fiscalização  do  referido  contrato  determinar  demais  ações
corretivas que se fizerem necessárias neste período de vigência contratual, via nova notificação, ou
futuramente conforme garantia de obra estabelecida em contrato.
Este  documento  entra  em vigência  a  partir  do  recebimento  do  mesmo,  com prazo  para
término das atividades até o dia 23/10/2017, uma vez que o contrato tem sua vigência válida até o dia
24/10/2017.
Ibirubá, 18 de outubro de 2017.
__________________________
Cristiane Brauner
Direção Geral Substituta
Portaria n.º 192/2017
Ciente em: 
__________________________
Paulo Roberto Schäfer
Representante Legal da Empresa
CPF: 666.368.410-87
22
III – CONCESSÃO DE DIÁRIAS E PASSAGENS
Afastamentos a Serviço Número:12/2017
Orgão solicitante: Campus Ibirubá Data de geração: 09/11/2017
Campus Ibirubá
PCDP 002087/17
Nome do Proposto: MIGACIR TRINDADE DUARTE FLORES
CPF do Proposto: 636.854.850-91 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Curso PDI 2019-2023 - Construindo o futuro do IFRS.
Valor das Diárias: 84.93
PCDP 002089/17
Nome do Proposto: RONALDO SERPA DA ROSA
CPF do Proposto: 009.729.700-37 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Treinamento/CURSO PDI 2019-2023
Valor das Diárias: 67.68
PCDP 002107/17
Nome do Proposto: EDIMAR MANICA
CPF do Proposto: 010.303.710-16 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Treinamento
Descrição Motivo: Capacitação Design Thinking 
Valor das Diárias: 223.86
PCDP 002145/17-1C
Nome do Proposto: SANDRA REJANE ZORZO PERINGER
CPF do Proposto: 479.230.320-68 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação reunião GT revisão IN 08/2014 - Solenidade Formatura
23
Ibirubá (02/10/2017) Bento Gonçalves (02/10/2017)
Bento Gonçalves (02/10/2017) Ibirubá (03/10/2017)
Valor das Diárias: 252.37
PCDP 002203/17
Nome do Proposto: BRUNO CONTI FRANCO
CPF do Proposto: 002.522.840-46 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Participar do I Fórum de Ensino em Educação Profissional e Tecnológica EPT, IV Encontro de Avaliação Institucional, II Fórum das
Licenciaturas e II Fórum PROEJA.
Ibirubá (09/10/2017) Bento Gonçalves (11/10/2017)
Bento Gonçalves (11/10/2017) Ibirubá (11/10/2017)
Valor das Diárias: 380.04
Sistema de Concessão de Diárias e Passagens Página 1 de  4
PCDP 002204/17
Nome do Proposto: IVO MAI
CPF do Proposto: 262.408.400-25 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Participar do I Fórum de Ensino em Educação Profissional e Tecnológica EPT, IV Encontro de Avaliação Institucional, II Fórum das
Licenciaturas e II Fórum PROEJA.
Ibirubá (09/10/2017) Bento Gonçalves (11/10/2017)
Bento Gonçalves (11/10/2017) Ibirubá (11/10/2017)
Valor das Diárias: 380.04
PCDP 002220/17
Nome do Proposto: FERNANDA ISABEL ROYER
CPF do Proposto: 021.567.970-99 Cargo ou Função: ASSISTENTE DE ALUNO
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Participar do I Fórum de Ensino em Educação Profissional e Tecnológica (EPT), IV Encontro de Avaliação Institucional, II Fórum das
Licenciaturas e II Fórum Proeja 
Ibirubá (09/10/2017) Bento Gonçalves (11/10/2017)
Bento Gonçalves (11/10/2017) Ibirubá (11/10/2017)
Valor das Diárias: 341.91
PCDP 002230/17
Nome do Proposto: ANDREIA TEIXEIRA INOCENTE
CPF do Proposto: 708.384.290-68 Cargo ou Função: PEDAGOGO-AREA
Motivo da Viagem: Nacional - A Serviço
Descrição Motivo: Participar do I Fórum de Ensino em Educação Profissional e Tecnológica (EPT), IV Encontro de Avaliação Institucional, II Fórum das
Licenciaturas e II Fórum Proeja
Ibirubá (09/10/2017) Bento Gonçalves (11/10/2017)
Bento Gonçalves (11/10/2017) Ibirubá (11/10/2017)
Valor das Diárias: 380.04
PCDP 002231/17
1
Nome do Proposto: HEILANDE FATIMA PEREIRA DA SILVA
CPF do Proposto: 326.973.840-49 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Participar do I Fórum de Ensino em Educação Profissional e Tecnológica (EPT), IV Encontro de Avaliação Institucional, II Fórum das
Licenciaturas e II Fórum Proeja
Ibirubá (09/10/2017) Bento Gonçalves (11/10/2017)
Bento Gonçalves (11/10/2017) Ibirubá (11/10/2017)
Valor das Diárias: 380.04
PCDP 002234/17
Nome do Proposto: VANESSA FARIA DE SOUZA
CPF do Proposto: 065.753.569-90 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Participar do I Fórum de Ensino em Educação Profissional e Tecnológica (EPT), IV Encontro de Avaliação Institucional, II Fórum das
Licenciaturas e II Fórum Proeja
Ibirubá (09/10/2017) Bento Gonçalves (11/10/2017)
Bento Gonçalves (11/10/2017) Ibirubá (11/10/2017)
Valor das Diárias: 380.04
PCDP 002236/17
Nome do Proposto: TIAGO RIOS DA ROCHA
CPF do Proposto: 004.529.450-03 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Encontro/Seminário
Sistema de Concessão de Diárias e Passagens Página 2 de  4
Descrição Motivo: Participar do I Fórum de Ensino em Educação Profissional e Tecnológica (EPT), IV Encontro de Avaliação Institucional, II Fórum das
Licenciaturas e II Fórum Proeja
Ibirubá (09/10/2017) Bento Gonçalves (11/10/2017)
Bento Gonçalves (11/10/2017) Ibirubá (11/10/2017)
Valor das Diárias: 380.04
PCDP 002238/17
Nome do Proposto: DANIEL UHRY
CPF do Proposto: 002.285.070-81 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Participar do I Fórum de Ensino em Educação Profissional e Tecnológica (EPT), IV Encontro de Avaliação Institucional, II Fórum das
Licenciaturas e II Fórum Proeja
Ibirubá (09/10/2017) Bento Gonçalves (11/10/2017)
Bento Gonçalves (11/10/2017) Ibirubá (11/10/2017)
Valor das Diárias: 380.04
PCDP 002347/17
Nome do Proposto: EDUARDO FERNANDES ANTUNES
CPF do Proposto: 018.795.000-86 Cargo ou Função: TECNICO DE TECNOLOGIA DA INFORMACAO
Motivo da Viagem: Nacional - A Serviço
Descrição Motivo: Acompanhar alunos no XXVI Encontro Cultural e Tradicionalista dos IFS da Região Sul do Brasil.
2
Ibirubá (13/10/2017) Sertão (15/10/2017)
Sertão (15/10/2017) Ibirubá (15/10/2017)
Valor das Diárias: 406.68
PCDP 002421/17
Nome do Proposto: EDVALDO FAOUR COUTINHO DA SILVA
CPF do Proposto: 832.573.575-91 Cargo ou Função: OPERADOR DE MAQUINAS AGRICOLAS
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião CONSUP
Ibirubá (17/10/2017) Porto Alegre (17/10/2017)
Porto Alegre (17/10/2017) Ibirubá (18/10/2017)
Valor das Diárias: 132.09
PCDP 002436/17
Nome do Proposto: GUILHERME DE BORTOLLI DO AMARAL
CPF do Proposto: 010.270.520-88 Cargo ou Função:
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: 4ª Reunião Consup
Ibirubá (17/10/2017) Porto Alegre (17/10/2017)
Porto Alegre (17/10/2017) Ibirubá (17/10/2017)
Valor das Diárias: 106.20
PCDP 002482/17
Nome do Proposto: DIONEI SCHWAAB BRANDT
CPF do Proposto: 019.450.300-38 Cargo ou Função: AUX EM ADMINISTRACAO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião processo seletivo 
Ibirubá (24/10/2017) Bento Gonçalves (24/10/2017)
Bento Gonçalves (24/10/2017) Ibirubá (24/10/2017)
Valor das Diárias: 67.68
Sistema de Concessão de Diárias e Passagens Página 3 de  4
PCDP 002492/17
Nome do Proposto: FERNANDA ISABEL ROYER
CPF do Proposto: 021.567.970-99 Cargo ou Função: ASSISTENTE DE ALUNO
Motivo da Viagem: Nacional - A Serviço
Descrição Motivo: Acompanhar os alunos bolsistas do PIBID em Seminário .
Ibirubá (20/10/2017) Porto Alegre (20/10/2017)
Porto Alegre (20/10/2017) Ibirubá (20/10/2017)
Valor das Diárias: 85.38
PCDP 002503/17
3
Nome do Proposto: JULIA CAROLINE GOULART BLANK
CPF do Proposto: 028.724.500-41 Cargo ou Função: JORNALISTA
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião sobre Processo Seletivo
Valor das Diárias: 67.68
4
IV – FÉRIAS
V – AFASTAMENTOS/LICENÇAS
5
Férias
Nome Período Parcela
1680661 05 a 22.12.17 2°
Gustavo Bathu Paulus 2168690 06 à 10.11.17 2°
2050923 20.11 a 27.11.12 2°
Lucas de Andrade 2155228 04 a 11.12.17 3°
Maiquel  Gromann 1993293 28.11 a 08.12.17
2178260 13 à 16.11.17 2°
Milton Jose Busnello 1893291 06 à 14.11.17 3°
2037434 16.10 à 01.11.17 1°
2054049 15.10 à 13.11.17 1°
Matrícula 
SIAPE
Bárbara  Kuntzer 
Schlintwein
Henrique Müller 
Dallmann
Maurício Cerutti de 
Castro
Sandra Rejane Zorzo 
Peringer
Vanessa Soares de 
Castro
Afastamentos/Licenças
Nome do Servidor(a) Matrícula SIAPE Quantidade de Dias Motivo
Karina Doninelli 2058058 1095
2149002 45 DIAS Licença saúde
Raquel Dalla Lana Cardoso 1853821 545 dias
Licença 
Interesse port 
n°136/2017
Paulo Ricardo de Pietro dos 
Santos
Licença 
Interesse port 
n°31/2017
VI – ATESTADOS MÉDICOS
VII – SUBSTITUIÇÕES DE FUNÇÃO 
6
Substituição de Função
Titular Substituto Função Período Motivo
Luiz Felipe Kopper da Silva FG - 002 Port. 084/2017
Ato de 
designação
Simone de Fátima 
Stéffens
26.09 à 
03.10.17
Ferias 
do 
titular.
Atestados médicos
Nome do Servidor(a) Matrícula SIAPE Observação
1870308 05 e 31.10.17 2 dias
1838354   16 e  30.10.17 2 dias
Jerry Cenise Marques 1077922 27.10.17 1 dia
Marcos Paulo Ludwig 1645508 16.10.17 1 dia
2337781 10 e 20.10.17 2 dias
Simone de Fátima  Steffens 2398408 16.10.17 1 dia
Período de 
Afastamento
Quantidade 
de Dias
Eduardo Marques de 
Camargo
Gabriel de Franceschi do 
Santos
Patrícia Harter Sampaio 
Stasiak
