1 
 
 
 
 
 
 
 
 
 
Boletim de Serviço 
Julho 
2013 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Câmpus Ibirubá 
Rua Nelsi Ribas Fritsch, 1111 - Bairro Esperança 
CEP: 98200-000 – Ibirubá – RS 
 
Comissão Responsável pela elaboração, impressão e distribuição: 
Cristiane Brauner 
Fernando Leão 
Luana Dapper 
Monique Lorenson 
 
 
2 
 
 
 
 
 
 
 
 
 
 
MINISTÉRIO DA EDUCAÇÃO – MEC 
Ministro 
Aloízio Mercadante Oliva 
 
SECRETARIA DE EDUCAÇÃO PROFISSIONAL E TECNOLÓGICA – SETEC 
Secretário 
Marco Antônio de Oliveira 
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO 
GRANDE DO SUL 
Reitora 
Claudia Schiedeck Soares de Souza 
 
CÂMPUS IBIRUBÁ 
Diretora-Geral Pro tempore 
Migacir Trindade Duarte Flôres 
 
 
 
 
 
 
 
 
 
 
3 
 
 
 
 
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA  
DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ 
 
 
 
 
ANIVERSARIANTES 
 
 
 
  
 
4 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
5 
 
 
 
Boletim de Serviço Nº 13/2013 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA  
DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ 
Sumário 
I- PORTARIAS...............................................................................................................6 
II – ORDENS DE SERVIÇO..........................................................................................10 
III – CONCESSÃO DE DIÁRIAS..................................................................................10 
IV – FÉRIAS...................................................................................................................15 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
6 
 
 
 
Boletim de Serviço Nº 13/2013 
I – PORTARIAS 
 
PORTARIAS DE 09 DE JULHO DE 2013. 
 
A DIRETORA GERAL PRÓ- TEMPORE DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS 
IBIRUBÁ, no uso da suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, RESOLVE: 
 
Nº 072 – DESIGNAR os servidores, FERNANDA SCHNEIDER, SIAPE nº 1823897, 
DAIANE TOIGO TRENTIN, SIAPE nº 1823868, MARCELE NEUTZLING RICKES, 
SIAPE nº 1993197, LISIANE CÉZAR DE OLIVEIRA, SIAPE nº 1828061, FELIPE LEITE 
SILVA, SIAPE nº 1999902, LUIZ FELIPE KOPPER DA SILVA SIAPE nº 2020887 e os 
alunos, ANA PAULA FRIELINK, matrícula nº 07110004 e WILLIAN SCHNEIDER 
MOREIRA, matrícula nº 07070052, para sob a presidência da primeira comporem a 
Comissão Organizadora da I MOEPEX do IFRS – Câmpus Ibirubá. 
 
Nº 073 – DESIGNAR os servidores abaixo relacionados para atuarem como fiscais do 
contrato nº 35/2013, com a Empresa Perfil Computacional Ltda, referente a aquisição de 
computadores para o IFRS – Câmpus Ibiruba. 
 
FISCAL TÉCNICO DO CONTRATO 
Laura Gotleib da Rosa SIAPE Nº 1680548 
FISCAL ADMINISTRATIVO 
Magali Teresinha da Silva SIAPE Nº 1835765 
FISCAL REQUISITANTE 
Jovani José Alberti SIAPE Nº 995602 
 
 
Nº 074 – DESIGNAR os servidores abaixo relacionados para atuarem como fiscais do 
contrato nº 36/2013, com a Empresa Hewlett-Packard Brasil Ltda, referente a aquisição de 
notebooks para o IFRS – Câmpus Ibiruba. 
 
7 
 
 
 
FISCAL TÉCNICO DO CONTRATO 
Laura Gotleib da Rosa SIAPE Nº 1680548 
FISCAL ADMINISTRATIVO 
Magali Teresinha da Silva SIAPE Nº 1835765 
FISCAL REQUISITANTE 
Jovani José Alberti SIAPE Nº 995602 
 
Nº 075 – Art. 1º - CONSTITUIR a Comissão para a Banca de Seleção de Bolsistas, para o 
cargo de Professor do Programa Nacional de Acesso ao Ensino Técnico e Emprego – 
PRONATEC, conforme edital nº 126/2013, composta pelos servidores abaixo relacionados: 
 
EDUARDO GIROTTO, Professor do Ensino Básico, Técnico e Tecnológico; SIAPE nº 
1893215, RAQUEL LORENSINI ALBERTI, Professora do Ensino Básico, Técnico e 
Tecnológico, SIAPE nº 1306033, ANDRÉ RICARDO DIERINGS, Professor do Ensino 
Básico, Técnico e Tecnológico, SIAPE nº 1804751, MARCOS PAULO LUDWIG, Professor 
do Ensino Básico, Técnico e Tecnológico, SIAPE nº 1645508 e MILENA SILVESTER 
QUADROS, Professora do Ensino Básico, Técnico e Tecnológico, SIAPE nº 1872360. 
 
 
Profº. Marcelo Lima Calixto 
Diretor-Geral Em Exercício 
Port. nº 152/2012 
 
PORTARIA Nº 76, DE 17 DE JULHO DE 2013. 
 
O DIRETOR GERAL EM EXERCÍCIO DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS 
IBIRUBÁ, no uso da suas atribuições legais subdelegadas pela Portaria nº 152 de 21 de 
novembro de 2012, publicado no DOU de 21 de dezembro de 2012, RESOLVE: 
 
DESIGNAR, o servidor MILTON JOSÉ BUSNELLO, SIAPE nº 1893291, como 
Fiscal do Contrato de Motorista e o servidor JOVANI JOSÉ ALBERTI, SIAPE  nº 995602 
8 
 
 
 
como Suplente, referente ao pregão 71/2013, processo nº 2337.1000.519.2013-06, do IFRS – 
Câmpus Ibirubá. 
 
 
Profº. Marcelo Lima Calixto 
Diretora Geral em Exercício 
Port. nº 152/2012 
 
 
PORTARIA Nº 77, DE 17 DE JULHO DE 2013. 
 
O DIRETOR GERAL EM EXERCÍCIO DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS 
IBIRUBÁ, no uso da suas atribuições legais subdelegadas pela Portaria nº 152 de 21 de 
novembro de 2012, publicado no DOU de 21 de dezembro de 2012, RESOLVE: 
 
DESIGNAR, o servidor JULIANO ELESBÃO RATHKE, SIAPE nº 1759468, como 
Fiscal do Contrato de Prestação de Serviços Terceirizados em Manutenção Predial e o 
servidor FERNANDO BELTRAME, SIAPE  nº 1796099 como Suplente, referente ao pregão 
71/2013, processo nº 2337.1000.519.2013-06, do IFRS – Câmpus Ibirubá. 
 
 
Profº. Marcelo Lima Calixto 
Diretora Geral em Exercício 
Port. nº 152/2012 
 
 
PORTARIA DO DIA 25 DE JULHO DE 2013. 
 
A DIRETORA GERAL PRÓ- TEMPORE DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS 
IBIRUBÁ, no uso da suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, RESOLVE: 
 
9 
 
 
 
Nº 78 - DESIGNAR a servidora ALESSANDRA MEDIANEIRA VARGAS DA SILVA, 
Técnica Administrativa em Educação, matrícula SIAPE nº 1828149, para responder como 
substituta da função de Coordenadora de Assistência ao Educando, código FG 02. 
 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral “Pro tempore” 
Port. nº 552/2012 
 
PORTARIA DO DIA 25 DE JULHO DE 2013. 
 
A DIRETORA GERAL PRÓ- TEMPORE DO INSTITUTO FEDERAL DE 
EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS 
IBIRUBÁ, no uso da suas atribuições legais subdelegadas pela Portaria nº 552 de 21 de 
novembro de 2012, publicado no DOU de 22 de novembro de 2012, RESOLVE: 
 
Nº 79 – DESIGNAR os servidores abaixo relacionados para atuarem como fiscais do contrato 
nº 55/2013, referente a Dispensa Emergencial 120/2013, Contratação de Serviço de Vigilância 
Armada para o IFRS – Câmpus Ibiruba. 
 
FISCAL 
Milton José Busnello SIAPE Nº 1893291 
SUPLENTE 
André Luiz Marcondes SIAPE Nº 1982908 
 
 
Profª. Migacir Trindade Duarte Flôres 
Diretora Geral “Pro tempore” 
Port. nº 552/2012 
 
 
 
 
10 
 
 
 
II - ORDENS DE SERVIÇO 
 
Nada consta. 
 
 
III – CONCESSÃO DE DIÁRIAS 
 
Campus Ibirubá 
Período Solicitado : 01/07/2013   à   31/07/2013  
Nome: EDUARDO GIROTTO  
Função: 
 
Período da Viagem: 28/07/2013    à   03/08/2013  
Destino: Ibirubá / Florianópolis / Ibirubá  
Número da Requisição: 001930/13  
Valor das Diárias (R$):  1314,13  
Objetivo: Nacional - Congresso  
Descrição: XXXIV Congresso Brasileiro de Ciência do Solo.  
 
Campus Ibirubá 
Nome: MILTON JOSE BUSNELLO  
Função: 
 
Período da Viagem: 05/07/2013    à   06/07/2013  
Destino: Ibirubá / Santa Maria / Ibirubá  
Número da Requisição: 001737/13  
Valor das Diárias (R$):  248,55  
Objetivo: Nacional - Treinamento  
Descrição: XIV Curso de Poda de plantas frutíferas.  
 
Campus Ibirubá 
Nome: MAIQUEL GROMANN  
Função: 
 
Período da Viagem: 05/07/2013    à   06/07/2013  
Destino: Ibirubá / Santa Maria / Ibirubá  
Número da Requisição: 001738/13  
Valor das Diárias (R$):  248,55  
Objetivo: Nacional - Treinamento  
Descrição: XIV Curso de Poda de plantas frutíferas.  
 
Campus Ibirubá 
Nome: FERNANDO DE OLIVEIRA LEAO  
Função: 
 
Período da Viagem: 08/07/2013    à   09/07/2013  
11 
 
 
 
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 001807/13  
Valor das Diárias (R$):  231,59  
Objetivo: Nacional - Convocação  
Descrição: Convocação para a reunião dos comunicadores do IFRS.  
 
Campus Ibirubá 
Nome: BEN HUR COSTA DE CAMPOS  
Função: 
 
Período da 
Viagem: 
08/07/2013    à   09/07/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
001833/13  
Valor das Diárias 
(R$):  
208,43  
Objetivo: Nacional - Convocação  
Descrição: 
Participação na reunião do COPI nos dias 08 e 09 de julho de 2013, em 
atendimento a convocação 029/2013 da PROPI.  
 
Campus Ibirubá 
Nome: HELDER MADRUGA DE QUADROS  
Função: 
 
Período da 
Viagem: 
08/07/2013    à   08/07/2013  
Destino: Ibirubá / Farroupilha / Ibirubá  
Número da 
Requisição: 
001835/13  
Valor das Diárias 
(R$):  
71,55  
Objetivo: Nacional - Convocação  
Descrição: 
Convocação para a plenária para a discussão e elaboração de proposta de 
regulamento geral da CPPD do IFRS.  
 
Campus Ibirubá 
Nome: JULIANO ELESBAO RATHKE  
Função: 
 
Período da Viagem: 08/07/2013    à   08/07/2013  
Destino: Ibirubá / Farroupilha / Ibirubá  
Número da 
Requisição: 
001837/13  
Valor das Diárias 
(R$):  
71,55  
12 
 
 
 
Objetivo: Nacional - Convocação  
Descrição: 
Segunda reunião plenária para aprovação da proposta de progressão 
docente.  
 
Campus Ibirubá 
Nome: LUIS CLAUDIO GUBERT  
Função: 
 
Período da 
Viagem: 
16/07/2013    à   16/07/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
001870/13  
Valor das 
Diárias (R$):  
71,55  
Objetivo: Nacional - Convocação  
Descrição: 
Reunião dos diretores de desenvolvimento institucional ou ocupantes de 
cargos equivalentes de todos os Câmpus do IFRS, para reunião de trabalho a 
ser realizada no dia 16 de julho de 2013, conforme pauta da convocação.  
 
Campus Ibirubá 
Nome: JOSIANE ROBERTA KREBS  
Função: 
 
Período da 
Viagem: 
12/07/2013    à   12/07/2013  
Destino: Erechim / Ibirubá / Erechim  
Número da 
Requisição: 
001887/13  
Valor das 
Diárias (R$):  
70,36  
Objetivo: Nacional - A Serviço  
Descrição: 
Participar da Banca do Processo Seletivo para a contratação de Professor 
temporário de LIBRAS, no Câmpus Ibirubá, conforme edital nº16 de 18 de 
junho de 2013.  
 
Campus Ibirubá 
Nome: MALCUS CASSIANO KUHN  
Função: 
 
Período da 
Viagem: 
18/07/2013    à   21/07/2013  
Destino: Ibirubá / Curitiba / Ibirubá  
Número da 
Requisição: 
001922/13  
Valor das 
Diárias (R$):  
763,19  
13 
 
 
 
Objetivo: Nacional - Encontro/Seminário  
Descrição: 
Participação no XI Encontro Nacional de Educação Matemática, cujo tema 
será "Educação Matemática: Retrospectivas e perspectivas". Neste evento o 
servidor irá apresentar o trabalho: "O contexto escolar teuto-brasileiro no Rio 
Grande do Sul e o ensino da Matemática".  
 
Campus Ibirubá 
Nome: FELIPE LEITE SILVA  
Função: 
 
Período da 
Viagem: 
16/07/2013    à   16/07/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
001923/13  
Valor das Diárias 
(R$):  
71,55  
Objetivo: Nacional - Convocação  
Descrição: 
Participação na reunião do GT Recursos Externos dia 16 de julho de 2013, 
em atendimento à convocação 030/13 da PROPI.  
 
Campus Ibirubá 
Nome: FABIANO GIACOMAZZI DE ALMEIDA  
Função: 
 
Período da Viagem: 10/07/2013    à   10/07/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da Requisição: 001926/13  
Valor das Diárias (R$):  71,55  
Objetivo: Nacional - Convocação  
Descrição: Participação no treinamento de cadastro SICAF, Reitoria.  
 
Campus Ibirubá 
Nome: BEN HUR COSTA DE CAMPOS  
Função: 
 
Período da 
Viagem: 
15/07/2013    à   16/07/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
001947/13  
Valor das Diárias 
(R$):  
208,43  
Objetivo: Nacional - Convocação  
Descrição: 
Participação na reunião nos dias 15 e 16 de julho de 2013 em atendimento 
à convocação 032/2013 da PROPI.  
 
14 
 
 
 
Campus Ibirubá 
Nome: ANDRE RICARDO DIERINGS  
Função: 
 
Período da 
Viagem: 
18/07/2013    à   18/07/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
001946/13  
Valor das Diárias 
(R$):  
71,55  
Objetivo: Nacional - Convocação  
Descrição: 
Convocação conforme Memorando 150/2013 PROEN/IFRS. Reunião das 
Coperses, conforme anexo.  
 
Campus Ibirubá 
Nome: AURELIO RICARDO BATU MAICA  
Função: 
 
Período da Viagem: 18/07/2013    à   18/07/2013  
Destino: Ibirubá / Bento Gonçalves / Ibirubá  
Número da 
Requisição: 
001961/13  
Valor das Diárias 
(R$):  
50,11  
Objetivo: Nacional - Convocação  
Descrição: 
Reunião da Sucomissão Própria de Avaliação Institucional. Em 
substituição a presidente da SPA.  
 
Campus Ibirubá 
Nome: RAQUEL LORENSINI ALBERTI  
Função: 
 
Período da Viagem: 30/07/2013    à   30/07/2013  
Destino: Ibirubá / Porto Alegre / Ibirubá  
Número da Requisição: 002051/13  
Valor das Diárias (R$):  89,25  
Objetivo: Nacional - Convocação  
Descrição: Convocação Proex - número 87/2013, reunião gestores Mulheres Mil.  
 
 
 
 
 
 
 
 
15 
 
 
 
IV - FÉRIAS 
 
MATRÍCULA            NOME DO SERVIDOR                    PERÍODO DE FÉRIAS 
26419-1795569 ALEXANDRE URBANO HOFFMANN 02JAN2013 16JAN2013 
26419-1795569 ALEXANDRE URBANO HOFFMANN 03JUN2013 17JUN2013 
26419-1804751 ANDRE RICARDO DIERINGS 21JAN2013 09FEV2013 
26419-1804751 ANDRE RICARDO DIERINGS 22JUL2013 26JUL2013 
26419-1804751 ANDRE RICARDO DIERINGS 02JAN2014 21JAN2014 
26419-1819147 AURELIO RICARDO BATU MAICA 02JAN2013 31JAN2013 
26419-1796124 BEN HUR COSTA DE CAMPOS 02JAN2013 10FEV2013 
26419-1796124 BEN HUR COSTA DE CAMPOS 22JUL2013 26JUL2013 
26419-1761366 CLAUDIA REGINA COSTA PACHECO 02JAN2013 10FEV2013 
26419-1761366 CLAUDIA REGINA COSTA PACHECO 22JUL2013 26JUL2013 
26419-1823868 DAIANE TOIGO TRENTIN 18JAN2013 11FEV2013 
26419-1823868 DAIANE TOIGO TRENTIN 15JUL2013 19JUL2013 
26419-1823868 DAIANE TOIGO TRENTIN 02JAN2014 16JAN2014 
26419-1804782 EDIMAR MANICA 21JAN2013 08FEV2013 
26419-1804782 EDIMAR MANICA 22JUL2013 26JUL2013 
26419-1804782 EDIMAR MANICA 31DEZ2013 20JAN2014 
26419-1818514 EDUARDO FERNANDES ANTUNES 28JAN2013 01FEV2013 
26419-1818514 EDUARDO FERNANDES ANTUNES 02JAN2014 26JAN2014 
26419-1796015 EDUARDO MATOS MONTEZANO 11JAN2013 10FEV2013 
26419-1796015 EDUARDO MATOS MONTEZANO 22JUL2013 26JUL2013 
26419-1796015 EDUARDO MATOS MONTEZANO 02JAN2014 10JAN2014 
26419-1823897 FERNANDA SCHNEIDER 02JAN2013 10FEV2013 
26419-1823897 FERNANDA SCHNEIDER 22JUL2013 26JUL2013 
26419-1796099 FERNANDO BELTRAME 21JAN2013 08FEV2013 
26419-1796099 FERNANDO BELTRAME 22JUL2013 26JUL2013 
26419-1796099 FERNANDO BELTRAME 31DEZ2013 20JAN2014 
26419-1703241 FRANCISCA BRUM TOLIO 02JAN2013 10FEV2013 
26419-1703241 FRANCISCA BRUM TOLIO 22JUL2013 26JUL2013 
26419-1759468 JULIANO ELESBAO RATHKE 02JAN2013 10FEV2013 
26419-1759468 JULIANO ELESBAO RATHKE 22JUL2013 26JUL2013 
26419-1680548 LAURA GOTLEIB DA ROSA 21JAN2013 08FEV2013 
26419-1680548 LAURA GOTLEIB DA ROSA 03JUN2013 13JUN2013 
26419-1827893 LEANDRO ROGGIA 21JAN2013 08FEV2013 
26419-1827893 LEANDRO ROGGIA 22JUL2013 26JUL2013 
26419-1827893 LEANDRO ROGGIA 31DEZ2013 20JAN2014 
26419-1828061 LISIANE CEZAR DE OLIVEIRA 02JAN2013 10FEV2013 
26419-1828061 LISIANE CEZAR DE OLIVEIRA 22JUL2013 26JUL2013 
26419-1781712 LUIS CLAUDIO GUBERT 19JAN2013 10FEV2013 
26419-1781712 LUIS CLAUDIO GUBERT 22JUL2013 26JUL2013 
26419-1781712 LUIS CLAUDIO GUBERT 02JAN2014 18JAN2014 
26419-1645508 MARCOS PAULO LUDWIG 21JAN2013 08FEV2013 
26419-1645508 MARCOS PAULO LUDWIG 22JUL2013 26JUL2013 
26419-1645508 MARCOS PAULO LUDWIG 02JAN2014 22JAN2014 
26419-2104561 MIGACIR TRINDADE DUARTE FLORES 21JAN2013 09FEV2013 
