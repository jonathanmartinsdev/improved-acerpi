 Ministério da Educação
Secretaria de Educação Profissional e Tecnológica
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
Câmpus Ibirubá
Boletim de Serviço 
Dezembro/2018
Publicado em 27 de fevereiro de 2018.
Comissão Responsável pela elaboração, impressão e distribuição:
Cristiane Brauner
Eduardo Fernandes Antunes
Julia Caroline Goulart Blank
Gustavo Rangel
Boletim de Serviço destinado à publicação dos atos administrativos do Câmpus Ibirubá do Instituto
Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
1
Ministério da Educação
Secretaria de Educação Profissional e Tecnológica
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
Campus Ibirubá
MINISTÉRIO DA EDUCAÇÃO – MEC
Ministro
Rossieli Soares da Silva
SECRETARIA DE EDUCAÇÃO PROFISSIONAL E TECNOLÓGICA – SETEC
Secretária 
Eline Neves Braga Nascimento
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE
DO SUL
Reitor pro tempore
Júlio Xandro Heck
 
CAMPUS IBIRUBÁ
Diretora-Geral Pro tempore
Migacir Trindade Duarte Flôres 
2
Boletim de Serviço destinado à publicação dos atos administrativos do Câmpus Ibirubá do 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
ANIVERSARIANTES
JAN FEV        MAR 
02/01 - Alice
02/01 – Maiquel
08/01 – Edimilson
10/01 – Vanessa  Castro
11/01 – Edimar
11/01 – Sônia
14/01 – Rodrigo  Gama
16/01 – Lysy
17/01 – Sandra Meinem
19/01 - Angéli
19/01 - Marcele
20/01 – Dona Ana
23/01 – Ben-Hur
28/01 - Janete
30/01 – Eliezer
30/01 – Juliano  Martins
04/02- Fernanda
09/02 – Gabriel  De Franceschi
13/02 - Miriam
18/02 – Ana Dionéia
20/02 - Elizandro
22/02 – Fabiane
23/02 – Eduardo  Camargo
24/02 – Fernanda Gall
26/02 – Ronaldo Tremarin
27/02 – Gubert
28/02– Girotto
1º/03 - Roberta
06/03 – Anderson
07/03 - Helder
14/03 – Júlia
19/03 - Gilnei
20/03 – Laura 
23/03 – Flávio
        ABR          MAI             JUN
06/04 – Paula
08/04 – Suzana
09/04 – Sabrine
09/04  - Silmar
13/ 04 – Ramone
13/04 – Juceli
17/04 – Gustavo Paulus
19/04 – Karina Doninelli
22/04– André Marcondes
23/04 – Felipe  Brum
26/04 – Dona Marli
26/04 – Daniel  Gonçalves
26/04 – Valmir
27/04 - Cléber
30/04 – Senaldo  Cappi
 1º/05 - Marsoé
03/05 – Ana Paula
05/05 - Lucilene
08/05 – Dionéia
09/05 – Carina Zonim
09/05 – Fabrício 
09/05 – Renata
11/05 – Lilian
12/05 - Miridiane
13/05 – Juliano  Ratke
16/05  - Silvani
19/05 - Edvaldo
19/05 - Marcine
23/05 – Cristiane
26/05 – Jardel 
29/05  - Vanussa 
30/05– Raquel  Alberti
30/05 – Sandra Peringer
30/05 – Tiago 
13/06 – Lucas de  Andrade
15/06 – Aline 
15/06 – Moisés  Hoffmann
15/06 - Roger
19/06 – Adilson
23/06 - Edilisia
27/06 – Magáli
27/06 – Eduardo  Antunes
        JUL         AGO SET
3
01/07 – Grazi
04/07 - Tarsila
10/07 -  Heilande
13/07 – Júlio Cesar
16/07 – Paulo  Ricardo
22/07 - Milton
26/07 – Andréia
27/07 – Lucas  Navarini
 02/08 – Seu Antônio
 04/08 – Gringa
06/08 – Andrey
21/08 - Jason
22/08– André Dierings
27/08 – Adriano
26/08 – Fábio 
31/08 – Dona  Alzira
04/09 – Alair
05/09 - Rodrigo Ludwig
06/09 – Gabriel Begute
08/09 – Mônica  Giacomini
09/09 - Iuri
09/09 – Mônica  dos  Santos
10/09 – Sérgio  Diemer
11/09 – Maria  Inês
13/09 - Matias
14/09 – André  Luis 
14/09 -  Carina Tonieto
16/09 – Tássia
19/09 – Moisés  Cordeiro
20/09 – Cimara
20/09 -  Daniel  P.
21/09 - Dionei
27/09 – Maurício Lopes
30/09 - Bárbara
OUT NOV DEZ
1º/10 – Eduarda
03/10 - Ângela
03/10 – Raquel  Cardoso
04/10 – Felipe  Leite
05/10 – Rafael  Scapini
12/10 – Carlão
13/10 - Lisandro
16/10 - Élvio
24/10 – Daniel  Uhry
02/11 - Nei
06/11 – Marcos  Paulo
09/11- Alessandra
10/11 – Luiz F. Kopper
13/11 – Daniel  Rockenbach 
15/11 – Jovani
18/11 – Eduardo Montezano
18/11 - Ivo
20/11 – Migacir
24/11 – Ronaldo  Serpa
25/11  - Viviane
07/12 – Jerry
07/12 – Lúcia
12/12  - Rafael  Venturini
16/12 – Bruno
20/12 - Odair
22/12 – Maurício Castro
23/12 - Zamith
4
Boletim de Serviço Nº 12/2018
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO 
SUL – CÂMPUS IBIRUBÁ
Sumário
I - PORTARIAS..............................................................................................................06
II – ORDENS DE SERVIÇO....................................................................................... ..19
III – CONCESSÃO DE DIÁRIAS E PASSAGENS......................................................20
IV – FÉRIAS...................................................................................................................24
V – AFASTAMENTOS/LICENÇAS..............................................................................26
VI – ATESTADOS MÉDICOS........................................................................................26
VII – SUBSTITUIÇÃO DE FUNÇÃO.............................................................…..........26
5
Boletim de Serviço Nº 12/2018
I – PORTARIAS 
PORTARIA DE 03 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO  TEMPORE  DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 221 – DESIGNAR os docentes, FERNANDA SCHNEIDER, SIAPE nº 1823897 JULIANO
DALCIN MARTINS, SIAPE nº 2146104, LUCILENE BENDER DE SOUSA, SIAPE nº 2212790,
RAQUEL DALLA LANA CARDOSO,  SIAPE nº  1853821,  TARSILA RUBIN BATTISTELLA,
SIAPE  nº  1201120,  para  sob  a  presidência  da  primeira  comporem  a  Comissão  de  Assuntos
Internacionais do IFRS - Campus Ibirubá.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DE 03 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO  TEMPORE  DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
6
Nº  222  –  DESIGNAR os  servidores,  RENATA PORTO ALEGRE GARCIA, Professora  do
Ensino Básico Técnico e Tecnológico, SIAPE nº 1788008, MAURICIO LOPES LIMA, Técnico em
Assuntos Educacionais, SIAPE nº 1770350, PAULA GAIDA WINCH, Professora do Ensino Básico
Técnico  e  Tecnológico,  SIAPE nº  1716767,  ANA DIONÉIA WOUTERS,  Professora  do  Ensino
Básico Técnico e Tecnológico SIAPE nº 1984476, BEN HUR COSTA DE CAMPOS, Professor do
Ensino  Básico  Técnico  e  Tecnológico,  SIAPE  nº  1796124,  para  sob  a  presidência  da  primeira
comporem a Comissão responsável pelo Processo Administrativo Disciplinar do aluno A.V.G.C. com
prazo de 30 dias para apuração.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DE 04 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO  TEMPORE  DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 223 –  DESIGNAR os servidores, DANIEL UHRY, Professor do Ensino Básico Técnico e
Tecnológico, SIAPE nº 1049780, MAURICIO LOPES LIMA, Técnico em Assuntos Educacionais,
SIAPE nº 1770350, PAULA GAIDA WINCH, Professora do Ensino Básico Técnico e Tecnológico,
SIAPE  nº  1716767,  ANA  DIONÉIA  WOUTERS,  Professora  do  Ensino  Básico  Técnico  e
Tecnológico SIAPE nº 1984476, BEN HUR COSTA DE CAMPOS, Professor do Ensino Básico
Técnico  e  Tecnológico,  SIAPE  nº  1796124,  para  sob  a  presidência  do  primeiro  comporem  a
Comissão Disciplinar Estudantil do aluno A.V.G.C. com prazo de 30 dias para apuração.
Revogar a portaria nº 222/2018, referente ao mesmo assunto.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
7
PORTARIA DE 04 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO  TEMPORE  DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 224 – AUTORIZAR o servidor ROBERTO NIEDERAUER, matrícula SIAPE N° 2318608,
portador  da  carteira  nacional  de  habilitação  n°  1732380605,  categoria  “AB”,  registro  nº
00369226187, expedida pelo Departamento Nacional de Trânsito do Rio Grande do Sul, a dirigir
veículos desta instituição de ensino, de acordo com o disposto no art. 1º da lei n° 9.327/96.
Art. 1º da lei 9.327/96: Os servidores públicos federais, dos órgãos e entidades integrantes da
Administração  Pública  Federal  direta,  autárquica  e  fundacional,  no  interesse  do  serviço  e  no
exercício de suas próprias atribuições, quando houver insuficiência de servidores ocupantes do cargo
de Motorista Oficial, poderão dirigir veículos oficiais, de transporte individual de passageiros, desde
que  possuidores  da  Carteira  Nacional  de  Habilitação  e  devidamente  autorizados  pelo  dirigente
máximo do órgão ou entidade a que pertençam.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA Nº 225 DO DIA 06 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO  TEMPORE  DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
I  –  DESIGNAR os  servidores  EDSON  ROBERTO  BUENO,  Técnico  em  Agropecuária,
Matrícula  SIAPE  Nº  2402137,  MARCOS  ROBERTO  JOST,  Assistente  em  Administração,
8
Matrícula SIAPE Nº 2307891, MAGÁLI TERESINHA DA SILVA, Contadora, Matrícula SIAPE
Nº  1835765,  CRISTIANE  BRAUNER,  Auxiliar  em  Administração,  Matrícula  SIAPE  Nº
1982614 para, sob a presidência do primeiro, constituírem a Comissão encarregada de realizar o
levantamento do Inventário Patrimonial Anual dos Bens Imóveis do IFRS – Campus Ibirubá;
II – O prazo para execução do trabalho e envio do processo a Reitoria (DPO) fica a contar desta
data até o dia 17 de dezembro de 2018;
III – A presente portaria entra em vigor a partir desta data.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 10 DE DEZEMBRO DE 2018.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 226  –  DESIGNAR os membros do Colegiado do Curso Técnico Subsequente em Mecânica,
ficando como membros a partir desta data os servidores abaixo relacionados:
Coordenação:  IVO MAI, Professor do Ensino Básico, Técnico e Tecnológico, Matrícula
SIAPE nº 1995140.
Representantes  Docentes:  FABIANO JORGE MACEDO,  Professor  do  Ensino  Básico,
Técnico e Tecnológico, Matrícula SIAPE nº 1827813, FLÁVIO ROBERTO ANDARA, Professor do
Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1819928, ANDERSON DE OLIVEIRA
FRAGA,  Professor  do  Ensino  Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE  nº  1256854,
GIANCARLO  STEFANI  SCHLEDER,  Professor  do  Ensino  Básico,  Técnico  e  Tecnológico,
Matrícula SIAPE nº 1052215 e FELIPE RODRIGUES DE FREITAS NETO,  Professor do Ensino
Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1993451
Representante  Técnicos-Administrativos:  HENRIQUE  LINHATI  BITENCOURT,
Técnico em Laboratório/área Eletromecânica , Matrícula SIAPE nº 2350837.
9
Representante  Discente:  LUCIO  SCHEIBE(titular),  Matrícula  n°  07130268  e
EMANUELLY DE OLIVEIRA (suplente), Matrícula n° 07130238
Revogar a portaria nº 198/2017, referente ao mesmo assunto.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 10 DE DEZEMBRO DE 2018.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº  227  –  DESIGNAR os  membros  do  Colegiado  do  Curso  Integrado  ao  Ensino  Médio  em
Mecânica, ficando como membros a partir desta data os servidores abaixo relacionados:
Coordenação:  IVO MAI, Professor do Ensino Básico, Técnico e Tecnológico, Matrícula
SIAPE nº 1995140.
Representantes  Docentes  Área  Técnica:  FABIANO  JORGE  MACEDO,  Professor  do
Ensino  Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE nº  1827813,  GIANCARLO STEFANI
SCHLEDER, Professor do Ensino Básico,  Técnico e Tecnológico,  Matrícula SIAPE nº 1052215,
JEFFERSON MORAIS GAUTÉRIO, Professor do Ensino Básico, Técnico e Tecnológico, Matrícula
SIAPE nº 2331430.
Representantes Docentes Núcleo Comum: PAULA GAIDA WINCH, Professora do Ensino
Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE nº  1716767,  MARSOÉ CRISTINA DAHLKE,
Professora  do  Ensino  Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE nº  1972400,  MARCUS
VINÍCIUS DA COSTA, Professor do Ensino Básico, Técnico e Tecnológico,  Matrícula SIAPE nº
10
1290853, EDIMILSON ANTÔNIO BRAVO PORTO(suplente), Professor do Ensino Básico, Técnico
e Tecnológico, Matrícula SIAPE nº 1904198.
Representante Técnicos-Administrativos: HENRIQUE LINHATI BITENCOURT (titular),
Técnico  em  Laboratório/área  Eletromecânica,  Matrícula  SIAPE  nº  2350837,  JOVANI  JOSÉ
ALBERTI (suplente), Técnico em Mecânica, Matrícula SIAPE nº 995602.
Representante  Discente:  ANDERSON SIGNORELLI(titular),  Matrícula  n°  07080158  e
MADALENA KLOSTERMAYER (suplente), Matrícula n° 07080200
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 10 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO  TEMPORE  DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 228 –  DESIGNAR  o servidor  EDIMAR MANICA,  Professor  do  Ensino Básico,  Técnico  e
Tecnológico  e  Diretor  de  Desenvolvimento  Institucional,  matrícula  SIAPE  nº  1804782,  para
responder como Diretor Geral em Exercício, do IFRS – Campus Ibirubá, na ausência da Titular,
servidora MIGACIR TRINDADE DUARTE FLÔRES, matrícula SIAPE nº 210456 no dia 11 de
dezembro de 2018.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
11
PORTARIA DE 10 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO  TEMPORE  DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 229 – AUTORIZAR o servidor HELDER MADRUGA DE QUADROS, matrícula SIAPE N°
1828638,  portador  da carteira  nacional  de habilitação n°  1413717975, categoria  “B”, registro nº
04019156151, expedida pelo Departamento Nacional de Trânsito do Rio Grande do Sul, a dirigir
veículos desta instituição de ensino, de acordo com o disposto no art. 1º da lei n° 9.327/96.
Art. 1º da lei 9.327/96: Os servidores públicos federais, dos órgãos e entidades integrantes da
Administração  Pública  Federal  direta,  autárquica  e  fundacional,  no  interesse  do  serviço  e  no
exercício de suas próprias atribuições, quando houver insuficiência de servidores ocupantes do cargo
de Motorista Oficial, poderão dirigir veículos oficiais, de transporte individual de passageiros, desde
que  possuidores  da  Carteira  Nacional  de  Habilitação  e  devidamente  autorizados  pelo  dirigente
máximo do órgão ou entidade a que pertençam.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
PORTARIA DO DIA 13 DE DEZEMBRO DE 2018.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº  230  –  DESIGNAR os  membros  do  Colegiado  do  Curso  Integrado  ao  Ensino  Médio  em
Agropecuária, ficando como membros a partir desta data os servidores abaixo relacionados:
12
Coordenação:  RENATA  PORTO  ALEGRE  GARCIA,  Professora  do  Ensino  Básico,
Técnico e Tecnológico, Matrícula SIAPE nº 1788008.
Representantes Docentes Área Técnica: BEN-HUR COSTA DE CAMPOS, Professor do
Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1796124, DANIEL UHRY, Professor do
Ensino  Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE  nº 1049780,  JULIANO  DALCIN
MARTINS, Professor do Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 2146104.
Representantes  Docentes  Propedêuticas:  ADILSON  BARBOSA,  Professor  do  Ensino
Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE  nº  1995120,  ANA DIONÉIA  WOUTERS,
Professora do Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1984476, IARA CADORE
DALLABRIDA, Professora do Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1237234.
Representante Técnicos-Administrativos: MILTON JOSE BUSNELLO (titular), Técnico
em Agropecuária, Matrícula SIAPE nº 1893291, MAIQUEL GROMANN (suplente), Técnico em
Agropecuária, Matrícula SIAPE nº 1993471.
Representante  Discente:  ANDRESSA  HORBACH  MATTE  (titular),  Matrícula  n°
07090237 e DANIEL LUÍS HÜBNER (suplente), Matrícula n° 07090241. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 14 DE DEZEMBRO DE 2018.
A DIRETORA GERAL SUBSTITUTA DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 047 de 01 de abril de 2015, publicado no DOU de 02
de abril de 2015, RESOLVE:
13
Nº  231  –  DESIGNAR os  membros  do  Colegiado  do  Curso  Integrado  ao  Ensino  Médio  em
Informática, ficando como membros a partir desta data os servidores abaixo relacionados:
Coordenação:  VANESSA FARIA DE SOUZA, Professora  do Ensino Básico,  Técnico  e
Tecnológico, Matrícula SIAPE nº 1217356.
Representantes Docentes Área Técnica: TIAGO RIOS DA ROCHA, Professor do Ensino
Básico,  Técnico e Tecnológico, Matrícula SIAPE nº 2107386, ROGER LUIS HOFF LAVARDA,
Professor  do  Ensino Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE nº  1720535,  RONALDO
SERPA  DA  ROSA,  Professor  do  Ensino  Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE
nº2263571.
Representantes  Docentes  Propedêuticas:  FERNANDA  SCHNEIDER,  Professora  do
Ensino  Básico,  Técnico  e  Tecnológico,  Matrícula  SIAPE  nº  1823897,  TARSILA  RUBIN
BATTISTELLA, Professora do Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE nº 1201120,
RODRIGO FARIAS GAMA, Professor do Ensino Básico, Técnico e Tecnológico, Matrícula SIAPE
nº2088048.
Representante  Técnicos-Administrativos:  FERNANDA  ISABEL  ROYER  (titular),
Assistente de aluno, Matrícula SIAPE nº 2344667, LUCAS JARDEL JOSE WOHLMUTH ALVES
DOS SANTOS (suplente), Técnico de Laboratório área informática, Matrícula SIAPE nº 2328168.
Representante Discente: LUCAS ADAMY LENHARDT (titular), Matrícula n° 07070253 e
LUCIANA BRAATZ (suplente), Matrícula n° 07070252. 
Profª. Sandra Rejane Zorzo Peringer
Diretora Geral Substituta
Port. nº 047/2015
PORTARIA Nº 232, DE 17 DE DEZEMBRO DE 2018.
A DIRETORA GERAL SUBSTITUTA DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
14
atribuições legais subdelegadas pela Portaria nº 047 de 01 de abril de 2015, publicado no DOU de 02
de abril de 2015, RESOLVE:
Art. 1º DESIGNAR os servidores abaixo relacionados para, sob a coordenação do primeiro,
constituírem a comissão especial para seleção e classificação de bens móveis do IFRS Campus
Ibirubá:
Felipe Iop Capeleto, SIAPE 3042375;
Gilnei José Hefler, SIAPE 2293657;
Edson Roberto Bueno, SIAPE 2402137;
Iuri Guissoni Quaglia, SIAPE 2165103;
Henrique Linhati Bitencourt, SIAPE 2350837; 
Aurélio Ricardo Batu Maica, SIAPE 1819147;
Giancarlo Stefani Schleder, SIAPE 1052215.
Art. 2º Os servidores do  campus deverão prestar tempestivamente as informações sobre o
acervo patrimonial que forem solicitadas pela comissão especial;
Art. 3º O prazo para finalização dos trabalhos é 17 de fevereiro de 2019. No período de 17
de dezembro de 2018 até 17 de fevereiro de 2019 a movimentação de bens permanentes somente será
permitida mediante prévia comunicação à comissão de especial do campus;
Art. 4º Esta portaria entra em vigor nesta data.
Profª. Sandra Rejane Zorzo Peringer
Diretora Geral Substituta
Port. nº 047/2015
PORTARIA DO DIA 19 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
15
Nº 233 – DESIGNAR os servidores EDUARDO FERNANDES ANTUNES, Técnico de Tecnologia
da Informação, SIAPE nº 1818514, LUCAS DE ANDRADE, Administrador, SIAPE nº 2155228 e
SANDRA REJANE ZORZO PERINGER, Diretora de Ensino, SIAPE nº 2037434, para compor a
Comissão Especial  de Avaliação do Estágio Probatório do servidor  FELIPE IOP CAPELETO,
SIAPE  n°  3042375,  Técnico  em  audiovisual  no  IFRS  -  Campus  Ibirubá  sob  processo  n°
23366.000343.2018-68.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1.849/2016
PORTARIA DO DIA 19 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº 234 – DESIGNAR os servidores ROGER LUIS HOFF LAVARDA, Professor do Ensino Básico,
Técnico e Tecnológico, SIAPE nº 1720535, TIAGO RIOS DA ROCHA, Professor do Ensino Básico,
Técnico e Tecnológico, SIAPE nº 2107386 e SANDRA REJANE ZORZO PERINGER, Diretora de
Ensino, SIAPE nº 2037434, para compor a Comissão Especial de Avaliação do Estágio Probatório do
servidor  IURI ALBANDES CUNHA GOMES, SIAPE n° 3010746, Professor do Ensino Básico,
Técnico e Tecnológico no IFRS - Campus Ibirubá sob processo n° 23366.000346.2018-00.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1.849/2016
16
PORTARIA DO DIA 19 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº  235  –  DESIGNAR os  servidores  EDIMILSON ANTONIO  BRAVO PORTO,  Professor  do
Ensino Básico, Técnico e Tecnológico, SIAPE nº 1904198, IVO MAI, Professor do Ensino Básico,
Técnico e Tecnológico, SIAPE nº 1995140 e SANDRA REJANE ZORZO PERINGER, Diretora de
Ensino, SIAPE nº 2037434, para compor a Comissão Especial de Avaliação do Estágio Probatório do
servidor GIANCARLO STEFANI SCHLEDER, SIAPE n° 1052215, Professor do Ensino Básico,
Técnico e Tecnológico no IFRS - Campus Ibirubá sob processo n° 23366.000344.2018-11.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1.849/2016
PORTARIA DO DIA 19 DE DEZEMBRO DE 2018.
A DIRETORA GERAL PRO-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
17
Nº 236 – DESIGNAR os  servidores  FERNANDA SCHNEIDER,  Professora  do Ensino Básico,
Técnico  e  Tecnológico,  SIAPE nº  1823897,  IVO MAI,  Professor  do  Ensino  Básico,  Técnico  e
Tecnológico, SIAPE nº 1995140 e SANDRA REJANE ZORZO PERINGER, Diretora de Ensino,
SIAPE  nº  2037434,  para  compor  a  Comissão  Especial  de  Avaliação  do  Estágio  Probatório  do
servidor  IARA CADORE DALLABRIDA,  SIAPE  n°  1237234,  Professora  do  Ensino  Básico,
Técnico e Tecnológico no IFRS - Campus Ibirubá sob processo n° 23366.000345.2018-57.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1.849/2016
PORTARIA DO DIA 20 DE DEZEMBRO DE 2018.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº  237  –  ALTERAR o  posicionamento  do  servidor  EDUARDO  MARQUES  DE
CAMARGO, matrícula SIAPE n° 1870308, Auditor, CLASSE: E: Padrão: 405 para CLASSE: E:
Padrão: 406, por motivo de PROGRESSÃO POR MÉRITO, a partir de 14.12.2018.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
18
II – ORDENS DE SERVIÇO
(nada consta)
19
III – CONCESSÃO DE DIÁRIAS E PASSAGENS
Afastamentos a Serviço Número:1/2019
Orgão solicitante:
Campus Ibirubá Data de geração: 07/01/2019
Campus Ibirubá
PCDP 003965/18
Nome do Proposto: SANDRA REJANE ZORZO PERINGER
CPF do Proposto: 479.230.320-68 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação para a reunião do Comitê de Ensino - Coen
Ibirubá (11/12/2018) Canoas (12/12/2018)
Canoas (12/12/2018) Ibirubá (12/12/2018)
Valor das Diárias: 258.63
PCDP 004065/18
Nome do Proposto: PAULA GAIDA WINCH
CPF do Proposto: 002.844.890-18 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação para reunião NEPGS (convocação em memorando anexo)
Ibirubá (20/12/2018) Bento Gonçalves (20/12/2018)
Bento Gonçalves (20/12/2018) Ibirubá (20/12/2018)
Valor das Diárias: 67.68
PCDP 004066/18
Nome do Proposto: SABRINE DE OLIVEIRA
CPF do Proposto: 012.200.920-70 Cargo ou Função: TRADUTOR INTERPRETE DE LINGUAGEM 
Motivo da Viagem: Nacional - Convocação
20
Descrição Motivo: Convocação para reunião NAPNE-Reitoria
Valor das Diárias: 67.68
PCDP 004067/18
Nome do Proposto: MARCOS ROBERTO JOST
CPF do 564.117.400-97 Cargo ou ASSISTENTE EM ADMINISTRAçãO
Sistema de Concessão de Diárias e Passagens Página 1 de  3
Proposto: Função:
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação para capacitação do Mural de Oportunidades do IFRS.
Valor das Diárias: 67.68
PCDP 004068/18
Nome do Proposto: LUCAS DE ANDRADE
CPF do Proposto: 017.439.160-94 Cargo ou Função: ADMINISTRADOR
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação para Capacitação sobre o Mural de Oportunidades IFRS.
Valor das Diárias: 57.62
PCDP 004069/18
Nome do Proposto: MARCELE NEUTZLING RICKES
CPF do Proposto: 985.384.540-72 Cargo ou Função: TECNICO EM ASSUNTOS EDUCACIONAIS
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação para reunião NAPNE - Reitoria
21
Valor das Diárias: 67.68
PCDP 004070/18
Nome do Proposto: MAURICIO LOPES LIMA
CPF do Proposto: 006.093.430-17 Cargo ou Função: TECNICO EM ASSUNTOS EDUCACIONAIS
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação reunião coordenadores dos NEABIS, 19/12, Bento Gonçalves
Valor das Diárias: 67.68
PCDP 004089/18
Nome do Proposto: HELDER MADRUGA DE QUADROS
Sistema de Concessão de Diárias e Passagens Página 2 de  3
CPF do Proposto: 884.788.230-34 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reuniões do Conselho Superior - 3ª Reunião Especial, 4ª Reunião Especial - 6ª Reunião Ordinária, no IFRS POA
Valor das Diárias: 85.38
PCDP 004110/18
Nome do Proposto: GUSTAVO OLIVEIRA RANGEL
CPF do Proposto: 815.422.750-91 Cargo ou Função: ASSISTENTE EM ADMINISTRAçãO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião Grupo de Trabalho, sistema Férias WEB
22
Valor das Diárias: 223.86
23
IV – FÉRIAS
24
Férias
Nome Matrícula SIAPE Período Parcela
Adriana Rigue Della Méa 1929195 07/01/2019 a 01/02/2019 2º
Aline Sirlei Poersch 3005318 28/01/2019 a 08/02/2019 1º 
Danieli Oppelt Nicolini 2331826 07/01/2019 a 25/01/2019 1º 
Eduardo Fernandes Antunes 1818514 24/01/2019 a 25/01/2019 1º 
Edson Roberto Bueno 2402137 28/01/2019 a 15/02/2019 1º 
Gustavo Bathu Paulus 2168690 28/01/2019 a 08/02/2019 1º 
Gustavo Bathu Paulus 2168690 07/01/2019 a 26/01/2019 1º 
Henrique Linhati Bittencourt 2350837 02/01/2019 a 11/01/2019 1º 
Jovani José Alberti 995602 02/01/2019 a 26/01/2019 1º 
Magali Teresinha da Silva 1835765 21/01/2019 a 25/01/2019 1º 
Maiquel Gromann 1993471 21/01/2019 a 08/02/2019 1º 
Miridiane Wayhs 2034931 15/01/2019 a 08/02/2019 1º 
Moisés Atílio Hoffmann 2785029 17/01/2019 a 31/01/2019 1º 
Mauricio Lopes Lima 1770350 21/01/2019 a 25/01/2019 1º 
Rodrigo Luiz Ludwig 1151545 14/01/2019 a 24/01/2019 3º
Sônia Margarete Souza de Souza 2333087 07/01/2019 a 18/01/2019 1º 
Tássia Michele Schwantes 2190979 07/01/2019 a 27/01/2019 1º 
Cimara Daiana Freddi 2227240 02/01/2019 a 04/01/2019 2º
Aurélio Ricardo Batu Maicá 1819147 26/01/2019 a 09/02/2019 1º 
Adilson Barbosa 1995120 02/01/2019 a 10/02/2019 2º
Ana Dioneia Wouters 1984476 02/01/2019 a 10/02/2019 1º 
Anderson de Oliveira Fraga 1256854 02/01/2019 a 10/02/2019 1º 
Andre Ricardo Dierings 1804751 16/01/2019 a 10/02/2019 1º 
2124439 02/01/2019 a 10/02/2019 1º 
Angeli Cervi Gabbi 2033151 02/01/2019 a 10/02/2019 1º 
Ana Júlian Faccio 1168263 02/01/2019 a 14/01/2019 2º
Ben Hur Costa de Campos 1796124 02/01/2019 a 10/02/2019 2º
Bruno Conti Franco 1999869 02/01/2019 a 10/02/2019 2º
Carina Tonieto 2282286 02/01/2019 a 10/02/2019 2º
Claudia Regina Costa Pacheco 1761366 02/01/2019 a 01/02/2019 1º 
Daniel Uhry 1049780 02/01/2019 a 10/02/2019 1º 
Daniel Vieira Pinto 2249026 02/01/2019 a 10/02/2019 1º 
Dioneia Magda Everling 1323426 02/01/2019 a 10/02/2019 2º
Edimar Manica 1804782 02/01/2019 a 10/02/2019 2º
Edimilson Antonio Bravo Porto 1904198 02/01/2019 a 10/02/2019 1º 
Eduardo Fernandes Sarturi 1051137 02/01/2019 a 10/02/2019 1º 
Eduardo Girotto 1893215 07/01/2019 a 08/02/2019 1º 
Angela Teresinha Woschinski de 
Mamann
25
Eduardo Matos Montezano 1796015 23/01/2019 a 08/02/2019 1º 
Edson Baal 1409609 02/01/2019 a 10/02/2019 1º 
Eliezer Jose Pegoraro 1686749 02/01/2019 a 10/02/2019 2º
Fabiano Jorge Macedo 1827813 02/01/2019 a 10/02/2019 2º
Fabiane Beatriz Sestari 1999837 02/01/2019 a 10/02/2019 2º
Felipe Rodrigues de Freitas Neto 1993451 02/01/2019 a 10/02/2019 1º 
Fernanda Schneider 1823897 02/01/2019 a 10/02/2019 1º 
Flávio Roberto Andara 1819928 02/01/2019 a 10/02/2019 2º
Francinei Rocha Costa 2309189 02/01/2019 a 10/02/2019 1º 
Giancarlo Stefani Schleder 1052215 26/01/2019 a 10/02/2019 1º 
Heilande Fatima Pereira da Silva 2094640 02/01/2019 a 10/02/2019 1º 
Helder Madruga de Quadros 1828638 02/01/2019 a 10/02/2019 1º 
Ivo Mai 1995140 02/01/2019 a 10/02/2019 2º
Jefferson Morais Gauterio 2331430 02/01/2019 a 10/02/2019 1º 
Jonas Anversa 2384881 02/01/2019 a 10/02/2019 1º 
Juliano Dalcin Martins 2146104 02/01/2019 a 10/02/2019 1º 
Juliano Elesbao Rathke 1759468 02/01/2019 a 10/02/2019 1º 
Lisiane Cezar de Oliveira 1828061 02/01/2019 a 15/02/2019 1º 
Lucilene Bender de Sousa 2212790 02/01/2019 a 10/02/2019 2º 
Luis Claudio Gubert 1781712 02/01/2019 a 15/02/2019 1º 
Marcus Vinicius da Costa 1290853 02/01/2019 a 10/02/2019 2º 
Marsoé Cristina Dahlke 1972400 02/01/2019 a 10/02/2019 2º 
Melissa Franceschini 1239265 02/01/2019 a 10/02/2019 2º 
Migacir Trindade Duarte Flores 2104561 17/01/2019 a 25/01/2019 1º 
Moises Nivaldo Cordeiro 2258011 02/01/2019 a 10/02/2019 1º 
Mônica Giacomini 1827598 02/01/2019 a 10/02/2019 1º 
Paula Gaida Winch 1716767 02/01/2019 a 06/02/2019 2º
Rafael Zanatta Scapini 2113322 02/01/2019 a 10/02/2019 2º
Raquel Dalla Lana Cardoso 1853821 31/12/2019 a 08/02/2019 2º
Raquel Lorensini Alberti 1306033 02/01/2019 a 10/02/2019 1º 
Renata Porto Alegre Garcia 1788008 02/01/2019 a 10/02/2019 1º 
Roberta Schmatz 1119051 02/01/2019 a 03/02/2019 2º
Rodrigo Farias Gama 2088048 02/01/2019 a 10/02/2019 2º
Roger Luis Hoff Lavarda 1720535 02/01/2019 a 10/02/2019 2º
Ronaldo Serpa da Rosa 2263571 02/01/2019 a 10/02/2019 1º 
Sandra Rejane Zorzo Peringer 2037434 21/01/2019 a 08/02/2019 1º 
Silvani Lopes Lima 1653220 02/01/2019 a 10/02/2019 1º 
Suzana Ferreira da Rosa 2917801 02/01/2019 a 10/02/2019 2º
Tarsila Rubin Battistella 1201120 02/01/2019 a 10/02/2019 1º 
Tiago Rios da Rocha 2107386 02/01/2019 a 10/02/2019 2º
Vanessa Faria de Souza 1217356 02/01/2019 a 10/02/2019 2º
Vânia Luisa Behnen 2424516 02/01/2019 a 01/02/2019 1º 
Vanussa Gislaine Dobler de Souza 2534931 02/01/2019 a 10/02/2019 2º
Vitor Hugo Machado da Silveira 2327915 02/01/2019 a 10/02/2019 1º 
Marcelo Zart 2369196 02/01/2019 a 26/01/2019 2º
V – AFASTAMENTOS/LICENÇAS
VI – ATESTADOS MÉDICOS
VII – SUBSTITUIÇÕES DE FUNÇÃO 
26
Atestados médicos
Nome do Servidor(a) Matrícula SIAPE Período de Afastamento Quantidade de Dias Observação
Vanussa Gislaine Dobler de Souza 2534931 16.01.2019 1 dia
Jefferson James Cunha de Souza 2331430 16.01.2019 1 dia
Greice Daniela Back 3072235 07.01.2019 1 dia
Gabriel de Franceschi dos  Santos 1838354 07.01.2019 1 dia
Gabriel de Franceschi dos  Santos 1838354 21.01.2019 1 dia
Lucilene Bender de Souza 2212790 23.08.2018; 27.08.2018; 28.08.2018 2 dias
Nome do Servidor(a) Matrícula SIAPE Quantidade de Dias Motivo
Ramone Tramontini 2858717 180 dias Licença- gestante
Ana Julian Faccio 1168263 180 dias Licença- gestante
Substituição de Função
Titular Substituto Função Período Ato de designação Motivo
Cristiane Brauner Luiz Felipe Kopper da Silva CD- 4 10/12/2018 a 14/12/2018 255/2016 Férias da titular
Cristiane Brauner Luiz Felipe Kopper da Silva CD- 4 17/12/2018 a 22/122018 255/2016 Férias da titular
Laura Gotleib da Rosa Lucas Jardel José Wolmuth Alves dos Santos FG-2 16/10/2018 a 01/11/2018 129/2018 Férias da titular
