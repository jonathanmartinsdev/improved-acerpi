 Ministério da Educação
Secretaria de Educação Profissional e Tecnológica
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
Câmpus Ibirubá
Boletim de Serviço 
Março/2017
Publicado em 9 de Maio de 2017.
Comissão Responsável pela elaboração, impressão e distribuição:
Cristiane Brauner
Eduardo Fernandes Antunes
Julia Caroline Goulart Blank
Jerry Cenise Marques
Boletim de Serviço destinado à publicação dos atos administrativos do Câmpus Ibirubá do Instituto
Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
1
Ministério da Educação
Secretaria de Educação Profissional e Tecnológica
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
Câmpus Ibirubá
MINISTÉRIO DA EDUCAÇÃO – MEC
Ministro
José Mendonça Bezerra Filho
SECRETARIA DE EDUCAÇÃO PROFISSIONAL E TECNOLÓGICA – SETEC
Secretário 
Marcos Antônio Viegas Filho 
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE
DO SUL
Reitor
Osvaldo Casares Pinto
 
CÂMPUS IBIRUBÁ
Diretora-Geral Pro tempore
Migacir Trindade Duarte Flôres 
2
Boletim de Serviço destinado à publicação dos atos administrativos do Câmpus Ibirubá do 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
ANIVERSARIANTES
JAN FEV        MAR 
02/01 - Alice
02/01 – Maiquel
08/01 – Edimilson
10/01 – Vanessa  Castro
11/01 – Edimar
11/01 – Sônia
14/01 – Rodrigo  Gama
16/01 – Lysy
17/01 – Sandra Meinem
19/01 - Angéli
19/01 - Marcele
20/01 – Dona Ana
23/01 – Ben-Hur
28/01 - Janete
30/01 – Eliezer
30/01 – Juliano  Martins
04/02- Fernanda
09/02 – Gabriel  De Franceschi
13/02 - Miriam
18/02 – Ana Dionéia
20/02 - Elizandro
22/02 – Fabiane
23/02 – Eduardo  Camargo
24/02 – Fernanda Gall
26/02 – Ronaldo Tremarin
27/02 – Gubert
28/02– Girotto
1º/03 - Roberta
06/03 – Anderson
07/03 - Helder
14/03 – Júlia
19/03 - Gilnei
20/03 – Laura 
23/03 – Flávio
        ABR          MAI             JUN
06/04 – Paula
08/04 – Suzana
09/04 – Sabrine
09/04  - Silmar
13/ 04 – Ramone
13/04 – Juceli
17/04 – Gustavo Paulus
19/04 – Karina Doninelli
22/04– André Marcondes
23/04 – Felipe  Brum
26/04 – Dona Marli
26/04 – Daniel  Gonçalves
26/04 – Valmir
27/04 - Cléber
30/04 – Senaldo  Cappi
 1º/05 - Marsoé
03/05 – Ana Paula
05/05 - Lucilene
08/05 – Dionéia
09/05 – Carina Zonim
09/05 – Fabrício 
09/05 – Renata
11/05 – Lilian
12/05 - Miridiane
13/05 – Juliano  Ratke
16/05  - Silvani
19/05 - Edvaldo
19/05 - Marcine
23/05 – Cristiane
26/05 – Jardel 
29/05  - Vanussa 
30/05– Raquel  Alberti
30/05 – Sandra Peringer
30/05 – Tiago 
13/06 – Lucas de  Andrade
15/06 – Aline 
15/06 – Moisés  Hoffmann
15/06 - Roger
19/06 – Adilson
23/06 - Edilisia
27/06 – Magáli
27/06 – Eduardo  Antunes
        JUL         AGO SET
3
01/07 – Grazi
04/07 - Tarsila
10/07 -  Heilande
13/07 – Júlio Cesar
16/07 – Paulo  Ricardo
22/07 - Milton
26/07 – Andréia
27/07 – Lucas  Navarini
 02/08 – Seu Antônio
 04/08 – Gringa
06/08 – Andrey
21/08 - Jason
22/08– André Dierings
27/08 – Adriano
26/08 – Fábio 
31/08 – Dona  Alzira
04/09 – Alair
05/09 - Rodrigo Ludwig
06/09 – Gabriel Begute
08/09 – Mônica  Giacomini
09/09 - Iuri
09/09 – Mônica  dos  Santos
10/09 – Sérgio  Diemer
11/09 – Maria  Inês
13/09 - Matias
14/09 – André  Luis 
14/09 -  Carina Tonieto
16/09 – Tássia
19/09 – Moisés  Cordeiro
20/09 – Cimara
20/09 -  Daniel  P.
21/09 - Dionei
27/09 – Maurício Lopes
30/09 - Bárbara
OUT NOV DEZ
1º/10 – Eduarda
03/10 - Ângela
03/10 – Raquel  Cardoso
04/10 – Felipe  Leite
05/10 – Rafael  Scapini
12/10 – Carlão
13/10 - Lisandro
16/10 - Élvio
24/10 – Daniel  Uhry
02/11 - Nei
06/11 – Marcos  Paulo
09/11- Alessandra
10/11 – Luiz F. Kopper
13/11 – Daniel  Rockenbach 
15/11 – Jovani
18/11 – Eduardo Montezano
18/11 - Ivo
20/11 – Migacir
24/11 – Ronaldo  Serpa
25/11  - Viviane
07/12 – Jerry
07/12 – Lúcia
12/12  - Rafael  Venturini
16/12 – Bruno
20/12 - Odair
22/12 – Maurício Castro
23/12 - Zamith
4
Boletim de Serviço Nº 3/2017
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO 
SUL – CÂMPUS IBIRUBÁ
Sumário
I - PORTARIAS..............................................................................................................06
II – ORDENS DE SERVIÇO....................................................................................... ..15
III – CONCESSÃO DE DIÁRIAS E PASSAGENS......................................................17
IV – FÉRIAS...................................................................................................................23
V – AFASTAMENTOS/LICENÇAS..............................................................................25
VI – ATESTADOS MÉDICOS........................................................................................26
VII – SUBSTITUIÇÃO DE FUNÇÃO...........................................................................26
5
Boletim de Serviço Nº 3/2017
I – PORTARIAS 
PORTARIA N° 19 DO DIA 02 DE MARÇO DE 2017.
Homologa estabilidade de servidor
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Art.1  DECLARAR  estável  a  servidora   HEILANDE  FÁTIMA PEREIRA DA SILVA,
Professora de Ensino Básico Técnico e Tecnológico , Matrícula Siape nº 2094640, em conformidade
com o artigo 41, da Constituição Federal, com a redação que lhe foi dada pelo artigo 6º da Emenda
Constitucional nº 19, de 04 de junho de 1998, a partir de 19.02.2017.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 1.849/2016
6
PORTARIA N° 20 DO DIA 02 DE MARÇO DE 2017.
Homologa estabilidade de servidor
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Art.1 DECLARAR estável o servidor  RODRIGO FARIAS GAMA, Professor de Ensino
Básico Técnico e Tecnológico , Matrícula Siape nº 2088048, em conformidade com o artigo 41, da
Constituição Federal, com a redação que lhe foi dada pelo artigo 6º da Emenda Constitucional nº 19,
de 04 de junho de 1998, a partir de 07.02.2017.
                                                  Profª. Migacir Trindade Duarte Flôres
 Diretora Geral “Pro tempore”
                                                                 Port. nº 1849/2016
PORTARIA DO DIA 02 DE MARÇO DE 2017.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
7
Nº  021-Alterar  o  posicionamento  do  servidor  GABRIEL  DE  FRANCESCHI  DOS  SANTOS,
matrícula SIAPE n° 1838354, Engenheiro Agrônomo, CLASSE: E: Padrão: 404  para CLASSE E
Padrão: 405, por motivo de PROGRESSÃO POR MÉRITO, a partir de 01 de Fevereiro de 2017.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. Nº 1849/2016
PORTARIA DO DIA 02 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 022 – REVOGAR a portaria nº 137/2014, referente a permissão de dirigir veículos oficiais do 
servidor GABRIEL DE FRANCESCHI DOS SANTOS, matrícula SIAPE nº 1838354, pela situação 
de bloqueado junto ao Detran/RS.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. Nº 1849/2016
8
PORTARIA DO DIA 03 DE MARÇO DE 2017.
A DIRETORA GERAL PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº  023 –  CANCELAR parcela  de  férias,  referentes  ao  exercício  2017,  do  servidor  BEN HUR
COSTA DE CAMPOS , professor de ensino básico, técnico e tenológico, matriculado ao SIAPE sob
o nº 1796124, a partir de 04 de março de 2017, para atender as necessidades de relevância desta
Instituição, nos termos do art. 80 da Lei nº 8.112/90.
O período de férias interrompido terá seu reinício em 24 de julho de 2017. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro Tempore
Port. nº 1849/2016
PORTARIA DO DIA 07 DE MARÇO DE 2017.
A DIRETORA GERAL SUBSTITUTA DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 047 de 01 de abril de 2015, publicado no DOU de 02
de abril de 2015, RESOLVE:
9
Nº 024 –  RENOVAR a concessão da liberação de 40% da carga horária da jornada de trabalho
semanal  para  participação  em  ação  de  qualificação  do  curso  de  Mestrado,  para  o  servidor
EDVALDO FAOUR COUTINHO DA SILVA, Operador de máquinas agrícolas, matrícula SIAPE
n°2159425, com fundamento na Instrução Normativa nº 06 de 11/05/2015, e nos termos do Processo
n° 23666.000099.2016-71, no período de 06/03/2017 a 13/07/2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro Tempore
Port. nº 1849/2016
PORTARIA DO DIA 07 DE MARÇO DE 2017.
A DIRETORA GERAL SUBSTITUTA DO  INSTITUTO  FEDERAL DE  EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 047 de 01 de abril de 2015, publicado no DOU de 02
de abril de 2015, RESOLVE:
Nº 025 – RENOVAR  a concessão da liberação de 20% da carga horária da jornada de trabalho
semanal para participação em ação de qualificação, curso de doutorado, para a servidora SANDRA
10
MEINEN DA CRUZ, Técnica em laboratório área - química, matrícula SIAPE n° 2230066, com
fundamento  na  Instrução  Normativa  nº  06  de  11/05/2015,  e  nos  termos  do  Processo  n°
23666.000295.2015-65, no período de 06/03/2017 a 13/07/2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro Tempore
Port. nº 1849/2016
PORTARIA DO DIA 07 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 026 – DESIGNAR a servidora LUCILENE BENDER DE SOUSA, Professora do Ensino Básico,
Técnico e Tecnológico, matrícula SIAPE nº 2212790, eleita pela área do curso como Coordenadora
do curso de Especialização em Ensino, Linguagens e suas Tecnologias do IFRS – Campus Ibirubá.
Revogar a portaria nº 175/2016, referente ao mesmo assunto, a partir de 01/03/2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
11
PORTARIA DO DIA 07 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 027 – DISPENSAR a servidora KARINA DONINELLI, Auxiliar em Administração, SIAPE nº
2058058,  da  função de Coordenadora  de  Gestão  de Pessoas,  código FG-0002 do IFRS-Campus
Ibirubá, a partir de 06.03.2017.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 08 DE MARÇO DE 2017.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº  028-Alterar  o  posicionamento  da  servidora  ALESSANDRA MEDIANEIRA VARGAS  DA
SILVA, matrícula SIAPE n°1828149,Assistente de Alunos, CLASSE: C: Padrão: 404 para CLASSE
C Padrão: 405, por motivo de PROGRESSÃO POR MÉRITO, a partir de 29 de Novembro  de 2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. Nº 1849/2016
12
PORTARIA DO DIA 08 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 029 – – DESIGNAR os servidores abaixo relacionados para atuarem como fiscais do contrato de
n°12/2017 - VR CONSULTORIA E SERVIÇOS GERAIS EIRELI, que tem como objeto a prestação
de serviço de auxiliar de cozinha para o IFRS - Campus Ibirubá.
FISCAL ADMINISTRATIVO
TÁSSIA MICHELE SCHWAN-
TES
SIAPE Nº 2190979
FISCAL TÉCNICO
BÁRBARA KUNTZER SCH-
LINTWEIN
SIAPE Nº 1680661
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 08 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 030  – DESIGNAR os servidores abaixo relacionados para comporem sobre a presidência da
primeira a comissão para a revisão do Projeto Pedagógico de Curso de Licenciatura em Matemática:
13
VANUSSA GISLAINE DOBLER DE SOUZA; Matrícula SIAPE n° 2534931, RODRIGO FARIAS
GAMA; Matrícula SIAPE n° 2088048, ÂNGELA TERESINHA WOSCHINSKI DA MAMANN;
Matrícula SIAPE n° 2124439, HEILANDE FÁTIMA PEREIRA DA SILVA; Matrícula SIAPE n°
2094640, ANDRÉ RICARDO DIERINGS; Matrícula  SIAPE n° 1804751;  MARSOÉ CRISTINA
DAHLKE; Matrícula SIAPE n° 1972400, RAMONE TRAMONTINI; Matrícula SIAPE n° 2858717;
IVO MAI;  Matrícula  SIAPE n° 1995140 e SANDRA REJANE ZORZO PERINGER; Matrícula
SIAPE n° 2037434.
Revogar a portaria nº 113/2015, referente ao mesmo assunto.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA Nº 31, DE 09 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Art. 1º REVOGAR licença para tratamento de assuntos particulares a servidora RAQUEL DALLA
LANA CARDOSO,  Professora  de  Ensino  Básico,  Técnico  e  Tecnológico,  matrícula  Siape  nº
1853821, no período de 09 de março de 2017 a 29 de junho de 2018,  autorizado pela  Portaria
280/2016, por motivo de férias.
14
Art. 2º  Em razão do estabelecido no artigo supra a servidora terá sua licença para tratamento de
assuntos particulares concedida pelo período de 11 de março de 2017 a 29 de junho de 2018, com
base  no  art.  91  da  Lei  8.112/90,  com  redação  dada  pela  Medida  Provisório  nº  2.225-45,  de
04/09/2001 e nos termos do Processo nº 23366.000629.2016-81.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
PORTARIA DO DIA 09 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 032  – DESIGNAR a servidora FERNANDA ISABEL ROYER, Assistente de aluno, SIAPE nº
2344667,  para  a  função  de  Coordenadora  da  Assistência  Estudantil,  código  FG-0002  do  IFRS-
Campus Ibirubá.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
15
PORTARIA DO DIA 10 DE MARÇO DE 2017.
A DIRETORA GERAL PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2015, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº  033 –  CANCELAR parcela  de férias,  referentes ao exercício 2017, do servidor  MAURICIO
LOPES LIMA, Técnico em Assuntos Educacionais, matriculado ao SIAPE sob o nº 1770350, a partir
de 13 de março de 2017, para atender as necessidades de relevância desta Instituição, nos termos do
art. 80 da Lei nº 8.112/90.
O período de férias cancelado terá seu reinício em 19 de junho de 2017. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro Tempore
Port. Nº 1849/2016
PORTARIA DO DIA 10 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 034 – – DESIGNAR os servidores abaixo relacionados para atuarem como fiscais do Contrato
97/2016, cujo objeto é a contratação de empresa especializada na prestação de serviços terceirizados
16
de  caráter  continuado  de  Recepcionista  para  o  IFRS Campus  Ibirubá,  com a  empresa  Sulbrasil
Serviços Terceirizados Ltda. - ME, CNPJ n.º 06.140.133/0001-40.
FISCAL ADMINISTRATIVO
ROBERTO NIEDERAUER SIAPE Nº 2318608
FISCAL TÉCNICO
CIMARA DAIANA FREDDI SIAPE Nº 2227240
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
PORTARIA DO DIA 13 DE MARÇO DE 2017.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº  035–  HOMOLOGAR  a  avaliação  de  desempenho  da  servidora  ÂNGELA  TERESINHA
WOSCHINSKI DE MAMANN Professora de Ensino Básico Técnico e Tecnológico, Matrícula Siape
nº  2124439,  constante  no  processo  n°23366.000452.2014-51,sem  prejuízo  da  continuidade  de
apurações  dos  fatores  de  assiduidade,  disciplina,  capacidade  de  iniciativa,  produtividade  e
responsabilidade, conforme §1° art.20 lei 8.112/90.
A presente portaria terá validade a partir de 29 de Janeiro de 2017.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. Nº 1849/2016
17
PORTARIA DO DIA 13 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 036 – DESIGNAR os servidores EDUARDO FERNANDES ANTUNES, Técnico em Tecnologia
da Informação, matrícula SIAPE nº 1818514, JASON SCALCO PILOTI, Técnico em Audiovisual,
matrícula  SIAPE nº  2296380, VITOR HUGO MACHADO DA SILVEIRA, Professor do Ensino
Básico, Técnico e Tecnológico, matrícula SIAPE nº 2327915, ADILSON BARBOSA, Professor do
Ensino Básico,  Técnico  e  Tecnológico,  matrícula  SIAPE nº  1995120,  para  sob a  presidência  do
primeiro  comporem  a  Comissão  responsável  pelo  processo  de  escolha  dos  representantes  do
Conselho de Campus.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
PORTARIA DO DIA 13 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
18
Nº 037 –  DESIGNAR A servidora SANDRA MEINEN DA CRUZ, Técnica em laboratório área -
Química, SIAPE nº 2230066, para responder como substituta da função de Coordenação de Pesquisa
e Inovação, código FG-0001, do IFRS – Campus Ibirubá, na ausência do titular.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
PORTARIA DO DIA 14 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 038 – DESIGNAR os servidores abaixo relacionados para atuarem como fiscais para o contrato
de  n°13/2017  -  VR CONSULTORIA E  SERVIÇOS GERAIS  EIRELI,  que  tem como  objeto  a
prestação de serviço de  auxiliar de  de manutenção   Predial para o IFRS- Campus Ibirubá. 
FISCAL ADMINISTRATIVO
MILTON JOSE BUSNELLO SIAPE Nº 1893291
FISCAL TÉCNICO
GILNEI JOSÉ HEFLER SIAPE Nº 2293657
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
19
PORTARIA DO DIA 03 DE MARÇO DE 2017.
A DIRETORA GERAL PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2016, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº  039  –  CANCELAR parcela  de  férias,  referentes  ao  exercício  2016,  da  servidora  SANDRA
REJANE ZORZO PERINGER, professora de ensino básico, técnico e tenológico, matriculado ao
SIAPE sob o nº 2037434, a partir de 18 de março de 2017, para atender as necessidades de relevância
desta Instituição, nos termos do art. 80 da Lei nº 8.112/90.
O período de férias interrompido terá seu reinício em 17 de abril de 2017. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro Tempore
Port. nº 1849/2016
PORTARIA DO DIA 21 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 040 –  DESIGNAR os servidores EDIMAR MANICA, Professor do Ensino Básico, Técnico e
Tecnológico, matrícula SIAPE nº 1804782, LUIS CLAUDIO GUBERT, Professor do Ensino Básico,
20
Técnico e Tecnológico, matrícula SIAPE nº 1781712 e ROBERTO NIEDERAURER, Assistente em
Administração,  matrícula  SIAPE  nº  2318608 para  sob  a  presidência  do  primeiro  comporem  a
Comissão responsável pelo processo eleitoral  da Coordenação do Curso Técnico em Informática
Integrado ao Ensino Médio do IFRS - Campus Ibriubá.
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
PORTARIA DO DIA 27 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 041 – REVOGAR a partir de 13/03/2017 a portaria nº 64/2016, referente a servidora EDUARDA
CORÓ MATTIONI que respondia como substituta da função de Coordenador de Administração e
Execução Orçamentária. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. Nº 1849/2016
21
PORTARIA DO DIA 29 DE MARÇO DE 2017.
A DIRETORA GERAL PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1.849 de 06 de setembro de 2015, publicado no DOU
de 08 de setembro de 2016, RESOLVE:
Nº  042 –  CANCELAR  parcela  de  férias,  referentes  ao  exercício  2016,  do  servidor  RAFAEL
SANCHES VENTURINI, Técnico em Agropecuária, matriculado ao SIAPE sob o nº 2249708, a
partir de 24 de abril de 2017, para atender as necessidades de relevância desta Instituição, nos termos
do art. 80 da Lei nº 8.112/90.
O período de férias cancelado terá seu reinício em 24 de julho de 2017. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro Tempore
Port. nº 1849/2016
PORTARIA DE 30 DE MARÇO DE 2017.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA
E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas atribuições 
legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU em 08 de 
Setembro de 2016, RESOLVE:
Nº 043 - RESCINDIR, a partir de 26 de março de 2017, o contrato do professor substituto 
RAPHAEL SILVANO FERREIRA SILVA, de nº 13/2016, por motivo de retorno do titular às 
22
atividades do cargo, conforme previsão contida na alínea “b” do Inciso I da cláusula sexta do 
contrato. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro Tempore
Port. nº 1849/2016
PORTARIA DO DIA 31 DE MARÇO DE 2017.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 044 –  CONCEDER a liberação de 40% da carga horária da jornada de trabalho semanal para
participação em ação de qualificação do curso de Mestrado, para a servidora VANESSA SOARES
DE CASTRO, Psicóloga, matrícula SIAPE n°2054049, com fundamento na Instrução Normativa nº
06 de 11/05/2015, e nos termos do Processo n° 23366.000129.2017-21, no período de 08/03/2017 a
20/04/2017.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 1849/2016
23
PORTARIA DO DIA 31 DE MARÇO DE 2017.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 045 –  CONCEDER a liberação de 20% da carga horária da jornada de trabalho semanal para
participação em ação de qualificação do curso de Mestrado, para a servidora JULIA CAROLINE
GOULART  BLANK,  Jornalista,  matrícula  SIAPE  n°2151404,  com  fundamento  na  Instrução
Normativa nº 06 de 11/05/2015, e nos termos do Processo n° 23366.000082.2017-03, no período de
31/03/2017 a 21/07/2017.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 1849/2016
PORTARIA DO DIA 31 DE MARÇO DE 2017.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de Setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 046 –  CONCEDER a liberação de 20% da carga horária da jornada de trabalho semanal para
participação em ação de qualificação do  curso de Doutorado, para a servidor  RODRIGO LUIZ
LUDWIG, Técnico em Agropecuária, matrícula SIAPE n°1151545, com fundamento na Instrução
24
Normativa nº 06 de 11/05/2015, e nos termos do Processo n° 23366.000117.2017-04, no período de
06/03/2017 a 13/07/2017.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 1849/2016
PORTARIA DO DIA 31 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 047 –  DESIGNAR os servidores  EDUARDO MATOS MONTEZANO, Professor  do  Ensino
Básico, Técnico e Tecnológico, matrícula SIAPE nº 1796015, SUZANA FERREIRA DA ROSA,
Professora  do  Ensino  Básico,  Técnico  e  Tecnológico,  matrícula  SIAPE  nº  2917801,  SANDRA
REJANE ZORZO PERINGER,  Professora  do  Ensino  Básico,  Técnico  e  Tecnológico,  matrícula
SIAPE nº 2037434, DIONÉI SCHWAAB BRANDT, Auxiliar em Administração, matrícula SIAPE
nº  2050864,  LUCAS DE ANDRADE,  Administrador,  matrícula  SIAPE nº  2155228,  para  sob a
presidência do primeiro, comporem a Comissão Especial com o objetivo de gerir todos os trâmites
relacionados  ao processo  de extinção do Curso Superior  de  Tecnologia  em Produção de  Grãos,
incluindo o acompanhamento a entrega de documentos acadêmicos. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
25
PORTARIA DO DIA 31 DE MARÇO DE 2017.
A DIRETORA GERAL  PRO TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 1849 de 06 de setembro de 2016, publicado no DOU
em 08 de Setembro de 2016, RESOLVE:
Nº 048 – DESIGNAR a servidora CRISTIANE BRAUNER, Auxiliar em Administração e Diretora
de Administração e Planejamento, matrícula SIAPE nº 1982614, para responder como substituta da
Direção Geral, do IFRS – Campus Ibirubá, na ausência da Titular, servidora MIGACIR TRINDADE
DUARTE FLÔRES, matrícula SIAPE nº 2104561 no dia 03 de abril de 2017. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
                                      PORTARIA DE 30 DE MARÇO DE 2017.
A Diretora Geral Pro-Tempore do Campus Ibirubá, no uso das atribuições legais que lhe são 
conferidas pela Portaria nº 1.849/2016 RESOLVE:
Nº 043 - RESCINDIR, a partir de 26 de março de 2017, o contrato do professor substituto 
RAPHAEL SILVANO FERREIRA SILVA, de nº 13/2016, por motivo de retorno do titular às 
atividades do cargo, conforme previsão contida na alínea “b” do Inciso I da cláusula sexta do 
contrato. 
Migacir Trindade Duarte Flôres
Diretora Geral Pro-Tempore
Port. nº 1849/2016
26
II – ORDENS DE SERVIÇO
ORDEM DE SERVIÇO Nº 3 /2017
       Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul – Campus
Ibirubá, AUTORIZA a empresa Sonia Maria Rodrigues - ME, CNPJ n.º 05.611.930/0001-03,
estabelecida Rua Sete de setembro n° 981, centro,  Ibirubá – RS, a prestar os serviços de
Restaurante e de Lanchonete, com fornecimento, preparo e distribuição de refeições para os
alunos, servidores, terceirizados, estagiários e visitantes do IFRS- Campus Ibirubá, contrato
25/2014 processo 23366.000640.2013-07, a partir do dia 03 de março de 2017, de acordo
com o novo cronograma de quantitativo de alunos dos almoços servidos de segunda- feira à
sexta –feira em anexo.  
 
Ibirubá, 01 de Março de 2017. 
_____________________________
Migacir Trindade Duarte Flores
Diretora Geral Pro Tempore
Port. 1849/2016.
Ciente: __________________________
Sonia Maria Rodrigues 
CPF: 010.126.970-61
Data: /___/_______.
27
ORDEM DE EXECUÇÃO DO SERVIÇO 04/2017 
PREGÃO ELETRÔNICO 11/2017
PROCESSO 23366.000467.2016-81
MODALIDADE DE LICITAÇÃO: PREGÃO ELETRÔNICO
TIPO DE LICITAÇÃO: MENOR PREÇO GLOBAL POR ITEM
ORDEM DE EXECUÇÃO DE SERVIÇO 04/2017
À EMPRESA: VR CONSULTORIA E SERVIÇOS GERAIS EIRELI
ENDEREÇO: Rod. BR 316 KM, 08, SALA 703, Edifício Business
CNPJ: 08.573.956/0001-94          TELEFONE/FAX:(91)3255-3226
E-MAIL: gestaovr2006@gmail.com
Autorizamos V.S.ª a executar o serviço adiante discriminado, observadas as especificações e demais
condições constantes do Edital e  Anexo II do Pregão nº. 11/2017, e à sua proposta de 02/03/2017- 
Processo 23366.000467.2016-81.
I - DO OBJETO
Ite
m Descrição do objeto com especificações* Qtd Un.
Valor
Estimado
mensal
Valor
Estimado
anual
1
Contratação de empresa especializada para 
prestação de serviços de Auxiliar de 
Cozinha. Jornada de trabalho de 44 horas 
semanais.
1 Posto R$3.333,33
R$
39.999,97 
II - DA DOTAÇÃO ORÇAMENTÁRIA
1.1. As despesas para eventual contratação de serviços terceirizados de caráter 
continuado, para o IFRS Campus Ibirubá, conforme condições, quantidades e 
exigências estabelecidas neste Edital e seus anexos, para atender ao Instituto Federal 
do Rio Grande do Sul – Campus Ibirubá decorrentes da presente ordem de execução 
de serviço correrão à conta da  Fonte de recurso 112; Elemento de Despesa 339039-46 
do orçamento do órgão requisitante para o exercício de 2017, conforme nota de empenho
nº 2017NE800070.
III – DO PRAZO DE ENTREGA
O prazo para início da prestação de serviços solicitados mediante Ordem de Execução será 
dia 13/03/2017 quando do recebimento deste.
 
IV - DAS DEMAIS CONDIÇÕES
As condições para execução de serviço, bem como de pagamento, obedecerão ao disposto
na Nota de Empenho.
28
________________________________________
Migacir Trindade Duarte Flôres
Diretora-Geral Pro tempore do IFRS –
 Campus Ibirubá
Port. n.º 1.849/2016
Recebi o original desta Ordem de Execução 04/2017 de Serviço e a Nota de Empenho nº 
2017NE800070, ciente das condições estabelecidas.
 Ibirubá- RS, 10 de Março de 2017.
 _______________________________________________
         VR Consultoria e Serviços Gerais EIRELI
ORDEM DE SERVIÇO Nº 05/2017
O Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul –  Campus
Ibirubá, através desta, resolve, PARALISAR a execução do contrato n.º 132/2014, cujo objeto trata
da  construção  do  prédio  de  Laboratórios  do  Campus  Ibirubá,  firmado  com  a  empresa  PP
ENGENHARIA E CONSTRUÇÕES LTDA, CNPJ n.º 72.473.275/0001-53, a partir desta data e por
prazo indeterminado.
A interrupção  da  execução  do  serviço  contratado  visa  oportunizar  a  viabilização  de
orçamento para atender as despesas oriundas do Termo Aditivo n.º 04/2017, que trata de acréscimos
e supressões de itens e valores. Uma vez que o orçamento ora relatado necessita de cadastro de
projeto junto ao MEC, necessitando de tempo para a sua viabilização, paralisa-se a obra para que,
também, não prejudique o prazo contratado de execução, conforme cronograma físico-financeiro. 
Ainda, a análise do pedido de reajuste dos itens remanescentes e não pagos, efetuado pelo
representante da empresa, sr. Paulo Roberto Schäfer, em 01/03/2017, ficará suspensa, uma vez que,
29
para  apuração  da  base  de  cálculo  a  ser  utilizada,  necessita-se  da  correta  formalização  do  TA
04/2017, supramencionado.
Ibirubá, 06 de abril de 2017.
__________________________
Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Portaria n.º 1.849/2016
Ciente em: 
__________________________
Paulo Roberto Schäfer
Representante Legal da Empresa
CPF: 666.368.410-87
30
III – CONCESSÃO DE DIÁRIAS E PASSAGENS
Afastamentos a Serviço Número:4/2017
Orgão solicitante: Campus Ibirubá Data de geração: 12/04/2017
Campus Ibirubá
P
C
000243/17
Nome do 
Proposto:
JASON SCALCO PILOTI
CPF do 
Proposto:
011.683.170-70 Cargo ou Função: TECNICO EM AUDIOVISUAL
Motivo da 
Viagem:
Nacional - Convocação
Descrição 
Motivo:
4ª Reunião do GT Marca e Identidades.
Ibirubá (17/03/2017) Bento Gonçalves (17/03/2017)
Bento Gonçalves (17/03/2017)
Valor das Diárias: 44.57
P
C
000261/17
Nome do 
Proposto:
BARBARA KUNTZER SCHLINTWEIN
CPF do 
Proposto:
009.984.060-07 Cargo ou Função: TECNICO EM ALIMENTOS E LATICINIOS
Motivo da 
Viagem:
Nacional - A Serviço
Descrição 
Motivo:
Treinamento de Combate e Prevenção de Incêndio 
Ibirubá (08/03/2017)
Erechim (08/03/2017)
Valor das Diárias: 67.68
P
C
000266/17
Nome do 
Proposto:
SANDRA MEINEN DA CRUZ
CPF do 
Proposto:
014.544.430-90 Cargo ou Função: TECNICO DE LABORATORIO AREA
Motivo da 
Viagem:
Nacional - A Serviço
Descrição 
Motivo:
Treinamento de combate e prevenção de incêndio, IFRS Campus Erechim. 
Ibirubá (08/03/2017)
Erechim (08/03/2017)
Valor das Diárias: 67.68
P
C
000267/17
Nome do 
Proposto:
IURI GUISSONI QUAGLIA
CPF do 
Proposto:
083.623.079-51 Cargo ou Função: TECNICO DE LABORATORIO AREA
Motivo da 
Viagem:
Nacional - A Serviço
Descrição 
Motivo:
Treinamento de combate e prevenção de incêndio, IFRS Campus Erechim. 
Ibirubá (08/03/2017)
Erechim (08/03/2017)
Valor das Diárias: 67.68
P
C
000268/17
31
Nome do 
Proposto:
GILNEI JOSE HEFLER
CPF do 
Proposto:
787.258.609-68 Cargo ou Função: TECNICO DE LABORATORIO AREA
Motivo da 
Viagem:
Nacional - A Serviço
Descrição 
Motivo:
Treinamento de combate e prevenção de incêndio, IFRS Campus Erechim. 
Valor das Diárias: 56.37
Sistema de Concessão de Diárias e Passagens Página 1 de  
2
PCDP 000300/17
Nome do Proposto: SANDRA REJANE ZORZO PERINGER
CPF do Proposto: 479.230.320-68 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação Comitê de Ensino.
Valor das Diárias: 252.37
PCDP 000323/17
Nome do Proposto: SANDRA MEINEN DA CRUZ
CPF do Proposto: 014.544.430-90 Cargo ou Função: TECNICO DE LABORATORIO AREA
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação PROPI 001/2017-Substituição do titular.
Valor das Diárias: 157.62
PCDP 000416/17
Nome do Proposto: FELIPE MACHADO BRUM
CPF do Proposto: 001.851.580-05 Cargo ou Função: ASSISTENTE EM ADMINISTRAçãO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação Comitê de extensão.
Valor das Diárias: 111.22
PCDP 000452/17
Nome do Proposto: IVO MAI
CPF do Proposto: 262.408.400-25 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação Colegiado CPPD.
Valor das Diárias: 223.86
32
PCDP 000527/17
Nome do Proposto: SANDRA REJANE ZORZO PERINGER
CPF do Proposto: 479.230.320-68 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação PROEN
Valor das Diárias: 73.31
33
IV – FÉRIAS
V – AFASTAMENTOS/LICENÇAS
34
Férias
Nome Período Parcela
Dionei Schwaabt Brandt 2050864 01.04 à 20.04.17 2°
Felipe Machado Brum 2155256 17 à 28.04.17 2°
Jason Scalco Pilot 2296380 17 à 20.04.17 1°
Karina Doninelli 2058058 17 à 20.04.17 2°
2034888 04.04 à 03.05.17 1°
Rodrigo Luis Ludwig 1151545 04.04 à 13.04.17 3°
Matrícula 
SIAPE
Marcine Floriano 
Prediger
Afastamentos/Licenças
Nome do Servidor(a) Matrícula SIAPEQuantidade de DiasMotivo
Roberta Schmatz 1119051 180 dias
Marcele Neutzling Rickes 1993197 180 dias Licença Gestante
Marcine Floriano Prediger 2034888 180 dias
Danieli Oppelt Nicolini 2331826 180 dias
Raquel Dalla Lana Cardoso 1853821 545 dias
Licença 
Gestante
Licença 
Gestante
Licença 
Gestante
Licença 
Interesse port 
n°31/2017
VI – ATESTADOS MÉDICOS
VII – SUBSTITUIÇÕES DE FUNÇÃO 
35
Atestados médicos
Nome do Servidor(a) Matrícula SIAPE Observação
Dioneia Magda Everling 1323426 03 e 08.03.17 2 dias
Eduardo Marques de Camargo 1870308 24.03.17 1 dia
1838354 06 e 20.03.17 2 dias
Jerry Cenise Marques 1077922 17.03.17 1 dia
Juliano Elesbao Rathke 1759468 03, 09 e 10.03.17 3 dias
2328168 13.03.17 1 dia
Marcos Roberto Jost 30 e 31.03.17 2 dias
Rejane Paris Marques 1871596 17.03.17 1 dia
Ronaldo Serpa da Rosa 2263571 10.03.17 1 dia
Suzana Ferreira da Rosa 2917801 20 e 21.03.17 2 dias
Período de 
Afastamento
Quantidade 
de Dias
Gabriel de Franceschi do 
Santos
Lucas Jardel José Wohlmuth 
Alves dos Santos
Substituição de Função
Titular Substituto Função Período Motivo
Milton Jose Busnello FG-002 Port.165/2012
CD-004 Port 256/2017
Cristiane Brauner CD-004 Port 255/2017
Ato de 
designação
André Luiz 
Marcondes
03.03 à 
16.03.17
Ferias 
do 
titular.
Sandra Rejane Zorzo 
Peringer
Paula Gaida 
Winch
13.03 à 
17.03.17
Ferias 
do 
titular.
Luiz Felipe 
Kopper da Silva
01.03 à 
24.03.17
Ferias 
do 
titular.
