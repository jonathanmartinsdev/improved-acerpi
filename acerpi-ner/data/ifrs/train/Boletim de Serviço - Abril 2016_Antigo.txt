 Ministério da Educação
Secretaria de Educação Profissional e Tecnológica
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
Câmpus Ibirubá
Boletim de Serviço 
Abril/2016
Publicado em 10 de maio de 2016.
Comissão Responsável pela elaboração, impressão e distribuição:
Cristiane Brauner
Fernando Leão
Julia Caroline Goulart Blank
Jerry Cenise Marques
Boletim de Serviço destinado à publicação dos atos administrativos do Câmpus Ibirubá do Instituto
Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
1
Ministério da Educação
Secretaria de Educação Profissional e Tecnológica
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
Câmpus Ibirubá
MINISTÉRIO DA EDUCAÇÃO – MEC
Ministro
Aloizio Mercadante
SECRETARIA DE EDUCAÇÃO PROFISSIONAL E TECNOLÓGICA – SETEC
Secretário 
Marcelo Machado Feres
 
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE
DO SUL
Reitor
Osvaldo Casares Pinto
 
CÂMPUS IBIRUBÁ
Diretora-Geral Pro tempore
Migacir Trindade Duarte Flôres 
Boletim de Serviço destinado à publicação dos atos administrativos do Câmpus Ibirubá do 
Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul
2
ANIVERSARIANTES
JAN FEV        MAR 
02/01 - Alice
02/01 – Maiquel
08/01 – Edimilson
10/01 – Vanessa  Castro
11/01 – Edimar
11/01 – Sônia
14/01 – Rodrigo  Gama
16/01 – Lysy
17/01 – Sandra Meinem
19/01 - Angéli
19/01 - Marcele
20/01 – Dona Ana
23/01 – Ben-Hur
28/01 - Janete
30/01 – Eliezer
30/01 – Juliano  Martins
04/02- Fernanda
09/02 – Gabriel  De Franceschi
13/02 - Miriam
18/02 – Ana Dionéia
20/02 - Elizandro
22/02 – Fabiane
23/02 – Eduardo  Camargo
24/02 – Fernanda Gall
26/02 – Ronaldo Tremarin
27/02 – Gubert
28/02– Girotto
1º/03 - Roberta
06/03 – Anderson
07/03 - Helder
14/03 – Júlia
19/03 - Gilnei
20/03 – Laura 
23/03 – Flávio
        ABR          MAI             JUN
06/04 – Paula
08/04 – Suzana
09/04 – Sabrine
09/04  - Silmar
13/ 04 – Ramone
13/04 – Juceli
17/04 – Gustavo Paulus
19/04 – Karina Doninelli
22/04– André Marcondes
23/04 – Felipe  Brum
26/04 – Dona Marli
26/04 – Daniel  Gonçalves
26/04 – Valmir
27/04 - Cléber
30/04 – Senaldo  Cappi
 1º/05 - Marsoé
03/05 – Ana Paula
05/05 - Lucilene
08/05 – Dionéia
09/05 – Carina Zonim
09/05 – Fabrício 
09/05 – Renata
11/05 – Lilian
12/05 - Miridiane
13/05 – Juliano  Ratke
16/05  - Silvani
19/05 - Edvaldo
19/05 - Marcine
23/05 – Cristiane
26/05 – Jardel 
29/05  - Vanussa 
30/05– Raquel  Alberti
30/05 – Sandra Peringer
30/05 – Tiago 
13/06 – Lucas de  Andrade
15/06 – Aline 
15/06 – Moisés  Hoffmann
15/06 - Roger
19/06 – Adilson
23/06 - Edilisia
27/06 – Magáli
27/06 – Eduardo  Antunes
        JUL         AGO SET
3
01/07 – Grazi
04/07 - Tarsila
10/07 -  Heilande
13/07 – Júlio Cesar
16/07 – Paulo  Ricardo
22/07 - Milton
26/07 – Andréia
27/07 – Lucas  Navarini
 02/08 – Seu Antônio
 04/08 – Gringa
06/08 – Andrey
21/08 - Jason
22/08– André Dierings
27/08 – Adriano
26/08 – Fábio 
31/08 – Dona  Alzira
04/09 – Alair
05/09 - Rodrigo Ludwig
06/09 – Gabriel Begute
08/09 – Mônica  Giacomini
09/09 - Iuri
09/09 – Mônica  dos  Santos
10/09 – Sérgio  Diemer
11/09 – Maria  Inês
13/09 - Matias
14/09 – André  Luis 
14/09 -  Carina Tonieto
16/09 – Tássia
19/09 – Moisés  Cordeiro
20/09 – Cimara
20/09 -  Daniel  P.
21/09 - Dionei
27/09 – Maurício Lopes
30/09 - Bárbara
OUT NOV DEZ
1º/10 – Eduarda
03/10 - Ângela
03/10 – Raquel  Cardoso
04/10 – Felipe  Leite
05/10 – Rafael  Scapini
12/10 – Carlão
13/10 - Lisandro
16/10 - Élvio
24/10 – Daniel  Uhry
02/11 - Nei
06/11 – Marcos  Paulo
09/11- Alessandra
10/11 – Luiz F. Kopper
13/11 – Daniel  Rockenbach 
15/11 – Jovani
18/11 – Eduardo Montezano
18/11 - Ivo
20/11 – Migacir
24/11 – Ronaldo  Serpa
25/11  - Viviane
07/12 – Jerry
07/12 – Lúcia
12/12  - Rafael  Venturini
16/12 – Bruno
20/12 - Odair
22/12 – Maurício Castro
23/12 - Zamith
4
Boletim de Serviço Nº 04/2016
INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO 
SUL – CÂMPUS IBIRUBÁ
Sumário
I - PORTARIAS..............................................................................................................06
II – ORDENS DE SERVIÇO....................................................................................... ..26
III – CONCESSÃO DE DIÁRIAS E PASSAGENS......................................................27
IV – FÉRIAS...................................................................................................................30
V – AFASTAMENTOS/LICENÇAS..............................................................................30
VI – ATESTADOS MÉDICOS........................................................................................31
VII – SUBSTITUIÇÃO DE FUNÇÃO...........................................................................31
5
Boletim de Serviço Nº 04/2016
I – PORTARIAS 
PORTARIA DO DIA 04 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 076 – Alterar o posicionamento da servidora SABRINE DE OLIVEIRA, matrícula SIAPE n°
1869684,  Tradutor e Interprete de Linguagens de Sinais, de NÍVEL: NI CLASSE: D NIVEL:  101
para NÍVEL: NI CLASSE: D NÍVEL: 102, por motivo de PROGRESSÃO POR MÉRITO, a partir
de 29 de Março de 2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIA DO DIA 05 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
6
Nº 077 – Designar  os membros FELIPE LEITE SILVA; Siape n°1999902, MOISÉS NIVALDO
CORDEIRO,  Siape  n°2258011,  ANA PAULA DE  ALMEIDA;  Siape  n°1800168,  ANDRÉIA
TEIXEIRA INOCENTE; Siape n°  1808305, RAFAEL ZANATTA SCAPINI;  Siape n°2113322 e
RAQUEL LORENSINI ALBERTI; Siape n°1306033 para sob a presidência do primeiro comporem a
Comissão responsável por conduzir o processo de escolha dos Coordenadores dos Cursos Integrados
em Agropecuária,  Informática e  Mecânica;  Cursos  Subsequentes  em Eletrotécnica  e  Mecânica e
Cursos Superiores de Licenciatura em Matemática, Engenharia Mecânica e Ciência da Computação.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIA DO DIA 07 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ- TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 078 –DISPENSAR o servidor Fernando de Oliveira Leão, SIAPE nº1982901, da função de Chefe
de Gabinete, código FG-0002, do IFRS – Câmpus Ibirubá.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
7
PORTARIA DO DIA 07 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 079 –  Alterar  o  posicionamento  do  servidor  IURI  GUISSONI QUAGLIA,  matrícula  SIAPE
n°2165103,Técnico em Laboratório/Área: Eletromecânica, de NÍVEL: NI CLASSE: D NIVEL: 101
para NÍVEL: NI CLASSE: D NÍVEL: 102, por motivo de PROGRESSÃO POR MÉRITO, a partir
de 18 de Março de 2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIA DO DIA 07 DE MARÇO DE 2016.
A DIRETORA GERAL PRÓ- TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso da suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
8
Nº 080 –  CONCEDER a liberação de 40% da carga horária da jornada de trabalho semanal para
participação em ação de qualificação do  curso de Mestrado, para o servidor  EDVALDO FAOUR
COUTINHO DA SILVA,  Operador  de  máquinas  agrícolas,  matrícula  SIAPE  n°2159425,  com
fundamento  na  Instrução  Normativa  nº  06  de  11/05/2015,  e  nos  termos  do  Processo  n°
23666.000099.2016-71, no período de 07/03/2016 a 19/07/2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIA DO DIA 08 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 081 – DESIGNAR os membros do Núcleo Docente Estruturante do Curso Superior de Licenciatura em
Matemática, ficando este composto pelos servidores: ANDRÉ RICARDO DIERINGS, Professor do Ensino
Básico, Técnico e Tecnológico, Siape nº 1804751, HEILANDE FÁTIMA PEREIRA DA SILVA, Professora
do  Ensino  Básico,  Técnico  e  Tecnológico,  Siape  nº  2094640,  RAMONE TRAMONTINI,  Professora  do
Ensino Básico, Técnico e Tecnológico, Siape nº 1858717, FABIANE BEATRIZ SESTARI, Professora do
Ensino Básico, Técnico e Tecnológico, Siape n° 1999837, IVO MAI, Professor do Ensino Básico, Técnico e
Tecnológico,  Siape  nº  1995140,  RODRIGO  FARIAS  GAMA,  Professor  do  Ensino  Básico,  Técnico  e
Tecnológico, Siape nº 2088048 , VANUSSA GISLAINE DOBLER DE SOUZA, Professora do Ensino Básico,
Técnico e Tecnológico, Siape n° 2534931, MARSOÉ CRISTINA DAHLKE, Professora do Ensino Básico,
Técnico  e  Tecnológico,  Siape  nº  1972400  e  ÂNGELA TERESINHA WOSCHINSKI  DE  MAMANN,
Professora do Ensino Básico, Técnico e Tecnológico, Siape nº 2124439.
Tornar sem efeito a portaria nº 041/2015, referente ao mesmo assunto a partir desta data.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
9
PORTARIA Nº 082 DO DIA 08 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
DESIGNAR  os  membros  do  Colegiado  do  Curso  Superior  de  Licenciatura  em
Matemática, ficando como membros, a partir desta data, os servidores abaixo relacionados:
Coordenação: RODRIGO  FARIAS  GAMA,  Professor  do  Ensino  Básico,  Técnico  e
Tecnológico, Siape nº 2088048..
Representantes  Docentes:  RODRIGO  FARIAS  GAMA,  Professor  do  Ensino  Básico,
Técnico e Tecnológico, Siape nº 2088048, ANDRÉ RICARDO DIERINGS, Professor do Ensino
Básico,  Técnico e Tecnológico,  Siape nº 1804751, HEILANDE FÁTIMA PEREIRA DA SILVA,
Professora do Ensino Básico, Técnico e Tecnológico, Siape nº 2094640, RAMONE TRAMONTINI,
Professora  do  Ensino  Básico,  Técnico  e  Tecnológico,  Siape  n°  1858717,VANUSSA GISLAINE
DOBLER DE SOUZA, Professora do Ensino Básico, Técnico e Tecnológico, Siape nº 2534931.
Suplente Docente: MARSOÉ CRISTINA DAHLKE, Professora do Ensino Básico, Técnico
e Tecnológico, Siape nº 1972400.
Representante Técnicos-Administrativos: MARCELE NEUTZLING RICKES, Técnica em
Assuntos Educacionais, Siape nº 1993197.
Suplente: KARINA DONINELLI, Auxiliar em Administração, Siape nº 2058058.
Representante Discente: LUANA HENRICHSEN, matrícula nº 07110094
Suplente: PAMELA ESTEFANE NUNES, matrícula nº 07110101
Tornar sem efeito a portaria nº 042/2015 referente ao mesmo assunto a partir   desta data.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIA N°083 DO DIA 11 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
HOMOLOGAR o resultado do Estágio Probatório do Servidor abaixo relacionado:
10
MATRÍCULA NOME CARGO PROCESSO RESULTADO
1716767 PAULA GAIDA
WINCH
PROFESSOR DE
ENSINO BÁSICO
TÉCNICO E
TECNOLÓGICO
23366.000614.2013-71 APROVADO
A presente portaria terá validade a partir da data de 06.02.2016
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. Nº 552/2012
PORTARIA N° 084 DO DIA 11 DE ABRIL DE 2016.
Homologa estabilidade de servidor
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Art.1  DECLARAR estável  a  servidora  EDUARDA CORÓ MATTIONI,  Assistente  em
administração ,  Matrícula Siape nº  2022179, em conformidade com o artigo 41, da Constituição
Federal, com a redação que lhe foi dada pelo artigo 6º da Emenda Constitucional nº 19, de 04 de
junho de 1998, a partir de 08 de Abril de 2016.
                                                 Profª. Migacir Trindade Duarte Flôres
 Diretora Geral “Pro tempore”
                                                               Port. nº 552/2012
  
11
PORTARIA DO DIA 15 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 085 – Alterar o posicionamento da servidora EDUARDA CORÓ MATTIONI, matrícula SIAPE
n°2022179, Assistente em Administração, de NÍVEL: NI CLASSE: D NIVEL: 402 para NÍVEL: NI
CLASSE: D NÍVEL: 403, por motivo de PROGRESSÃO POR MÉRITO, a partir de 08 de Abril de
2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIA N°086 DO DIA 15 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
HOMOLOGAR o resultado do Estágio Probatório do Servidor abaixo relacionado:
12
MATRÍCULA NOME CARGO PROCESSO RESULTADO
2037434 SANDRA
REJANE ZORZO
PERINGER
PROFESSOR DE
ENSINO BÁSICO
TÉCNICO E
TECNOLÓGICO
23366.000616.2013-60 APROVADO
A presente portaria terá validade a partir da data de 01.03.2016
 
                                                  Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
                                                              Port. nº 552/2012
PORTARIA Nº 087 DO DIA 15 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ- TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Autorizar  o  servidor  RODRIGO LUIZ  LUDWIG,  matrícula  SIAPE N°  2151545,  portador  da
carteira  nacional  de  habilitação  n°516915401,  categoria  “B”,  registro  nº  03952642463,  expedida  pelo
Departamento Nacional de Trânsito do Rio Grande do Sul, a dirigir veículos desta instituição de ensino, de
acordo com o disposto no art. 1º da lei n° 9.327/96.
Art.  1º  da  lei  9.327/96:  Os  servidores  públicos  federais,  dos  órgãos  e  entidades  integrantes  da
Administração Pública Federal direta, autárquica e fundacional, no interesse do serviço e no exercício de
suas próprias atribuições, quando houver insuficiência de servidores ocupantes do cargo de Motorista
Oficial, poderão dirigir veículos oficiais, de transporte individual de passageiros, desde que possuidores
da Carteira  Nacional  de  Habilitação e  devidamente  autorizados  pelo  dirigente  máximo do órgão ou
entidade a que pertençam.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
 
13
PORTARIAS DO DIA 15 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 088 – DISPENSAR a servidora FABIANE BEATRIZ SESTARI, Professora do Ensino Básico,
Técnico e Tecnológico, SIAPE nº1999837, da função de Coordenador de Extensão, código FG-0001
do IFRS-Campus Ibirubá.
Nº 089 – DESIGNAR o servidor, MOISÉS NIVALDO CORDEIRO, Professor do Ensino Básico,
Técnico e Tecnológico, SIAPE nº2258011, para a função de Coordenador de Extensão, código FG-
0001, do IFRS-Campus Ibirubá.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
 
PORTARIA DO DIA 15 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CAMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
14
Nº 090 – DESIGNAR os servidores, MAURÍCIO CERUTTI DE CASTRO, Arquiteto e Urbanista,
SIAPE nº 2178260, MILTON JOSE BUSNELLO, Técnico em Agropecuária, SIAPE nº 1893291,
MARCOS  PAULO  LUDWIG,  Professor  do  Ensino  Básico,  Técnico  e  Tecnológico,  SIAPE  nº
1645508,  RENATA  PORTO  ALEGRE  GARCIA,  Professora  do  Ensino  Básico,  Técnico  e
Tecnológico,  SIAPE  nº  1788008,  ELIÉZER  JOSÉ  PEGORARO,  Professor  do  Ensino  Básico,
Técnico e Tecnológico, SIAPE nº 1686749, DANIEL UHRY, Professor do Ensino Básico, Técnico e
Tecnológico, SIAPE nº 1049780 e MAIQUEL GROMANN, Técnico em Agropecuária, SIAPE nº
1993471, para compor sob a presidência do primeiro, a Comissão responsável pelo planejamento da
reforma do Bloco F do Campus Ibirubá do IFRS.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIAS DO DIA 15 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
15
Nº 091 – DISPENSAR o servidor, MOISÉS NIVALDO CORDEIRO, Professor do Ensino Básico,
Técnico  e  Tecnológico,  SIAPE  nº2258011,  da  função  de  Coordenador  do  Curso  Técnico  em
Eletrotécnica, código FUC-01, do IFRS-Campus Ibirubá.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 092– HOMOLOGAR a avaliação de desempenho do servidor LUIZ FELIPE    KOPPER DA
SILVA, cargo Assistente em Administração, matrícula Siape n° 2020887,
Constante  no  processo  n°23366.000591.2013-02,sem prejuízo  da  continuidade  de  apurações  dos
fatores  de  assiduidade,  disciplina,  capacidade  de  iniciativa,  produtividade  e  responsabilidade,
conforme §1° art.20 lei 8.112/90.
A presente portaria terá validade a partir  de 02.01.2016,tornando sem efeito a portaria 003/2016
referente ao mesmo assunto.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
16
PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº  093–  HOMOLOGAR a  avaliação  de  desempenho  do  servidor  ANDRÉ  LUIS  DEMICHEI,
Professor  de  Ensino  Básico  Técnico  e  Tecnológico,  matrícula  Siape  n°  2027018,constante  no
processo  n°23366.000599.2013-61,sem  prejuízo  da  continuidade  de  apurações  dos  fatores  de
assiduidade,  disciplina,  capacidade de iniciativa,  produtividade e responsabilidade,  conforme §1°
art.20 lei 8.112/90.
A presente portaria terá validade a partir  de 17.01.2016,tornando sem efeito a portaria 069/2016
referente ao mesmo assunto.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. Nº 552/2012
PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
17
Nº 094– HOMOLOGAR a avaliação de  desempenho da  servidora  MIRIDIANE WAYHS, cargo
Assistente  em  Administração,  matrícula  Siape  n°  2034931,  constante  no  processo  n°
23366.000595.2013-82,sem  prejuízo  da  continuidade  de  apurações  dos  fatores  de  assiduidade,
disciplina,  capacidade  de  iniciativa,  produtividade  e  responsabilidade,  conforme  §1°  art.20  lei
8.112/90.
A presente portaria terá validade a partir  de 13.02.2016,tornando sem efeito a portaria 068/2016
referente ao mesmo assunto.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 095– HOMOLOGAR a avaliação de desempenho da servidora ANGELI CERVI GABBI, cargo:
Professor  de  Ensino  Básico  Técnico  e  Tecnológico,  matrícula  Siape  n°  2033151,constante  no
processo  n°23360.000580.2013-74,sem  prejuízo  da  continuidade  de  apurações  dos  fatores  de
18
assiduidade,  disciplina,  capacidade de iniciativa,  produtividade e responsabilidade,  conforme §1°
art.20 lei 8.112/90.
A presente portaria terá validade a partir  de 10.02.2016,tornando sem efeito a portaria 036/2016
referente ao mesmo assunto.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
 PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 096– HOMOLOGAR a avaliação de desempenho do servidor MOISÉS ATÍLIO HOFFMANN,
cargo  Auxiliar  em  Administração,  matrícula  Siape  n°  2785029,  constante  no  processo  n°
23366.000596.2013-27,sem  prejuízo  da  continuidade  de  apurações  dos  fatores  de  assiduidade,
disciplina,  capacidade  de  iniciativa,  produtividade  e  responsabilidade,  conforme  §1°  art.20  lei
8.112/90.
A presente portaria terá validade a partir  de 02.01.2016,tornando sem efeito a portaria 045/2016
referente ao mesmo assunto.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
19
PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº  097–  HOMOLOGAR  a  avaliação  de  desempenho  da  servidora  MARCINE  FLORIANO
PREDIGER,  cargo:  Assistente  em  Administração,  matrícula  Siape  n°  2034888,  constante  no
processo  n°  23366.000594.2013-38,  sem prejuízo  da  continuidade  de  apurações  dos  fatores  de
assiduidade,  disciplina,  capacidade de iniciativa,  produtividade e responsabilidade,  conforme §1°
art.20 lei 8.112/90.
A presente portaria terá validade a partir  de 13.02.2016,tornando sem efeito a portaria 067/2016
referente ao mesmo assunto.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
20
Nº 098– HOMOLOGAR a avaliação de desempenho da servidora PAULA GAIDA WINCH, cargo:
Professor  de  Ensino  Básico  Técnico  e  Tecnológico,  matrícula  Siape  n°  1716767,constante  no
processo  n°23366.000614.2013-71,sem  prejuízo  da  continuidade  de  apurações  dos  fatores  de
assiduidade,  disciplina,  capacidade de iniciativa,  produtividade e responsabilidade,  conforme §1°
art.20 lei 8.112/90.
A presente portaria terá validade a partir  de 06.02.2016,tornando sem efeito a portaria 083/2016
referente ao mesmo assunto.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº  099– HOMOLOGAR a avaliação de  desempenho da  servidora  SANDRA REJANE ZORZO
PERINGER, cargo: Professor de Ensino Básico Técnico e Tecnológico, matrícula Siape n° 2037434,
constante  no  processo  n°23366.000616.2013-60,sem prejuízo  da  continuidade  de  apurações  dos
21
fatores  de  assiduidade,  disciplina,  capacidade  de  iniciativa,  produtividade  e  responsabilidade,
conforme §1° art.20 lei 8.112/90.
A presente portaria terá validade a partir  de 01.03.2016,tornando sem efeito a portaria 086/2016
referente ao mesmo assunto.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIAS DO DIA 20 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 100– HOMOLOGAR a avaliação de desempenho do servidor ROGER LUIS HOFF LAVARDA,
cargo: Professor de Ensino Básico Técnico e Tecnológico, matrícula Siape n° 1720535, constante no
processo  n°23366.000615.2013-15,sem  prejuízo  da  continuidade  de  apurações  dos  fatores  de
assiduidade,  disciplina,  capacidade de iniciativa,  produtividade e responsabilidade,  conforme §1°
art.20 lei 8.112/90.
A presente portaria terá validade a partir da data de 20.01.2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
22
PORTARIA DO DIA 25 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 101 – Alterar o posicionamento do servidor MILTON JOSÉ BUSNELLO, matrícula SIAPE n°
1893291, Técnico em Agropecuáriade, de  NÍVEL: NI CLASSE: D NIVEL:  403 para NÍVEL: NI
CLASSE: D NÍVEL: 404, por motivo de PROGRESSÃO POR MÉRITO, a partir de 03 de Abril de
2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. Nº 552/2012
PORTARIA DO DIA 25 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
23
Nº  102 –  CANCELAR as  férias  da  servidora SABRINE DE OLIVEIRA,  matrícula  SIAPE n°
2169684, Tradutora interprete de linguagem sinais,  ano de exercício 2015, 5ª parcela, com período
marcado para 30/05/2016 a 08/06/2016 remarcando o período para 08/08/2016 a 17/08/2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. Nº 552/2012 
PORTARIAS DO DIA 26 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso das suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 103– HOMOLOGAR a avaliação de desempenho da servidora LILIAN CLAÚDIA XAVIER
CORDEIRO, cargo: Professor de Ensino Básico Técnico e Tecnológico, matrícula Siape n° 2022705,
constante  no  processo  n°23366.000611.2013-37,sem  prejuízo  da  continuidade  de  apurações  dos
fatores  de  assiduidade,  disciplina,  capacidade  de  iniciativa,  produtividade  e  responsabilidade,
conforme §1° art.20 lei 8.112/90.
A presente portaria terá validade a partir da data de 09.01.2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
24
PORTARIA DO DIA 29 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 104 –  CANCELAR as férias da  servidora LAURA GOTLEIB DA ROSA, matrícula SIAPE n°
1680548, Analista  de tecnologia da informação,  ano de exercício 2016, 2ª  parcela,  com período
marcado para 30/05/2016 a 09/06/2016 remarcando o período para 18/07/2016 a 22/07/2016 e 3ª
parcela, com período marcado para 16/11/2016 a 25/11/2016 remarcando o período para 16/11/2016
a 01/12/2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
PORTARIA DO DIA 29 DE ABRIL DE 2016.
A DIRETORA GERAL PRÓ-TEMPORE DO INSTITUTO FEDERAL DE EDUCAÇÃO,
CIÊNCIA E TECNOLOGIA DO RIO GRANDE DO SUL – CÂMPUS IBIRUBÁ, no uso de suas
atribuições legais subdelegadas pela Portaria nº 552 de 21 de novembro de 2012, publicado no DOU
de 22 de novembro de 2012, RESOLVE:
Nº 105 – Alterar o posicionamento do servidor GUSTAVO BATHU PAULUS, matrícula SIAPE n°
2168690, Técnico  de  tecnologia  da  informação de  NÍVEL:  NI  CLASSE:  D  NIVEL:  101 para
NÍVEL: NI CLASSE: D NÍVEL: 102, por motivo de PROGRESSÃO POR MÉRITO, a partir de 23
de Março de 2016.
Profª. Migacir Trindade Duarte Flôres
Diretora Geral “Pro tempore”
Port. nº 552/2012
25
II – ORDENS DE SERVIÇO
(Nada Consta)
26
III – CONCESSÃO DE DIÁRIAS E PASSAGENS
Afastamentos a Serviço Número:4/2016
Orgão solicitante: Campus Ibirubá Data de geração: 03/05/2016
Campus Ibirubá
PCDP 000525/16
Nome do Proposto: FABIANE BEATRIZ SESTARI
CPF do Proposto: 960.251.820-00 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião CPPD.
Valor das Diárias: 223.86
PCDP 000603/16
Nome do Proposto: JULIA CAROLINE GOULART BLANK
CPF do Proposto: 028.724.500-41 Cargo ou Função: JORNALISTA
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião de comunicadores.
Valor das Diárias: 223.86
PCDP 000610/16
Nome do Proposto: ELIEZER JOSE PEGORARO
CPF do Proposto: 956.371.090-87 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião CEUA - Avaliação e protocolos.
Valor das Diárias: 67.68
PCDP 000618/16
Nome do Proposto: ALINE TERRA SILVEIRA
CPF do Proposto: 011.718.150-18 Cargo ou Função: BIBLIOTECARIO-DOCUMENTALISTA
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião do grupo de bibliotecÁrios do IFRS.
27
Valor das Diárias: 280.53
PCDP 000625/16
Nome do Proposto: MIGACIR TRINDADE DUARTE FLORES
CPF do Proposto: 636.854.850-91 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião de Colégio de dirigentes do IFRS.
Ibirubá (18/04/2016) Bento Gonçalves (19/04/2016)
Bento Gonçalves (19/04/2016) Ibirubá (19/04/2016)
Valor das Diárias: 275.61
Sistema de Concessão de Diárias e Passagens Página 1 de
PCDP 000626/16
Nome do Proposto: MOISES NIVALDO CORDEIRO
CPF do Proposto: 955.438.800-49 Cargo ou Função: PROFESSOR ENS BASICO TECN TECNOLOGICO
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Convocação para reunião do Cômite de extensão(COEX) do IFRS.
Ibirubá (11/04/2016) Bento Gonçalves (12/04/2016)
Bento Gonçalves (12/04/2016) Ibirubá (12/04/2016)
Valor das Diárias: 223.86
PCDP 000688/16
Nome do Proposto: TASSIA MICHELE SCHWANTES
CPF do Proposto: 010.043.180-14 Cargo ou Função: TECNOLOGO FORMACAO
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Encontro com produtores rurais e cooperativas da Agricultura Familiar
Ibirubá (13/04/2016) Farroupilha (13/04/2016)
Farroupilha (13/04/2016) Ibirubá (13/04/2016)
Valor das Diárias: 67.68
PCDP 000689/16
Nome do Proposto: BARBARA KUNTZER SCHLINTWEIN
CPF do Proposto: 009.984.060-07 Cargo ou Função: TECNICO EM ALIMENTOS E LATICINIOS
Motivo da Viagem: Nacional - Encontro/Seminário
Descrição Motivo: Encontro com produtores rurais e cooperativas da Agricultura Familiar.
Ibirubá (13/04/2016) Farroupilha (13/04/2016)
Farroupilha (13/04/2016) Ibirubá (13/04/2016)
Valor das Diárias: 67.68
PCDP 000757/16
Nome do Proposto: JULIA CAROLINE GOULART BLANK
CPF do Proposto: 028.724.500-41 Cargo ou Função: JORNALISTA
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Participação como membro TAE do Conselho Superior do IFRS na 2ª Reunião Ordinária e 3ª Reunião Especial do CONSUP
1
Ibirubá (19/04/2016) Bento Gonçalves (19/04/2016)
Bento Gonçalves (19/04/2016) Ibirubá (19/04/2016)
Valor das Diárias: 67.68
PCDP 000799/16
Nome do Proposto: IURI GUISSONI QUAGLIA
CPF do Proposto: 083.623.079-51 Cargo ou Função: TECNICO DE LABORATORIO AREA
Motivo da Viagem: Nacional - Convocação
Descrição Motivo: Reunião CIS CENTRAL.
Ibirubá (27/04/2016) Bento Gonçalves (27/04/2016)
Bento Gonçalves (27/04/2016) Ibirubá (27/04/2016)
Valor das Diárias: 67.68
Sistema de Concessão de Diárias e Passagens Página 2 de
2
IV – FÉRIAS
V – AFASTAMENTOS/LICENÇAS
3
Férias
Nome Período Parcela
Andréia Teixeira Inocente1808305 09 á 25.05.2016 2°
Eduarda Coro Mattioni 2022179 3º
Gustavo Bathu Paulus 2168690 02.05 à 15.05.16 3°
2020887 23.05 à 25.05.16 1°
Marcele Neutzling Rickes1993197 05.04.à 20.04.16 1°
2149002 04. à 06.05.16 1°
Matrícula 
SIAPE
30.05 á  
18.06.16
Luiz Felipe Kopper 
da Silva
Paulo Ricardo de 
Pietro dos Santos
Afastamentos/Licenças
Nome do Servidor(a) Matrícula SIAPE Motivo
1870308 90 dias
Iuri Guissoni Quaglia 2165103 1 dia Justiça Eleitoral
2113322 1 dia
Jerry Cenise Marques 1077922             1 dia Justiça Eleitoral
Quantidade de 
Dias
Eduardo Marques de 
Camargo
Licença para 
tratamento da  
própria saúde
Rafael Zanatta 
Scapini
Comparecimento 
a Justiça
VI – ATESTADOS MÉDICOS
VII – SUBSTITUIÇÕES DE FUNÇÃO 
4
Atestados médicos
Matrícula SIAPE Observação
Jerry Cenise Marques 1077922 01.04.16 1 dia
Laura Gotleib da Rosa 1680548 06 e 07.04.16 2 dias
Marcos Paulo Ludwig 1645508 04 e 25.04.16 2 dias
Nei Rodrigues de Freitas 22634959 26.04.16 1 dia
Ramone Tramontini 2858717 05 e 06.04.16 2 dias
1853821 01 e15.04.16 2 dias
Rejane Paris Marques 1871596 01.04.16 1 dia
Suzana Ferreira da Rosa 2917801 04 e 25.04.16 2 dias
Tiago Rios da Rocha 2107386 23.04.16 1 dia
Nome do 
Servidor(a)
Período de 
Afastamento
Quantidade de 
Dias
Raquel Dalla Lana 
Cardoso
Substituição de Função
Titular Substituto Função Período Motivo
FG-0002 04 á 06.05 065/2016
Cristiane Brauner FG-0002 28.03 á 15.04.16 064/2016
Ato de 
designação
Paulo Ricardo de 
Pietro dos Santos
Luiz Felipe 
Kopper da Silva
Férias do 
Titular
Eduarda Coró 
Mationi
Férias do 
Titular
