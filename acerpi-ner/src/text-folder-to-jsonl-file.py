import sys
from pathlib import Path
from prodigy.util import write_jsonl


def parse(from_folder: str, to_file: str):
    examples = []

    for file in Path(from_folder).iterdir():
        print(str(file))
        with open(file, 'r') as reader:
            text = reader.read()
            if text.strip() != '':
                task = {'text': text}
                examples.append(task)
        write_jsonl(to_file, examples)


if __name__ == '__main__':
    parse(sys.argv[1], sys.argv[2])
